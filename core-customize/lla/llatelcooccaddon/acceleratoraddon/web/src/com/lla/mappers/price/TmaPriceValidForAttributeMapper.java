/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.mappers.price;

import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.dto.ProductOfferingPriceWsDTO;
import com.lla.dto.TimePeriodWsDTO;
import de.hybris.platform.subscriptionfacades.data.SubscriptionPricePlanData;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import ma.glasnost.orika.MappingContext;


/**
 * TmaPriceValidForAttributeMapper populates value of ValidFor attribute from {@link SubscriptionPricePlanData} to
 * {@link ProductOfferingPriceWsDTO}
 *
 * @since 1907
 */
public class TmaPriceValidForAttributeMapper extends TmaAttributeMapper<SubscriptionPricePlanData, ProductOfferingPriceWsDTO>
{

	@Override
	public void populateTargetAttributeFromSource(final SubscriptionPricePlanData source, final ProductOfferingPriceWsDTO target,
			final MappingContext context)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (StringUtils.isEmpty(source.getName()))
		{
			return;
		}

		final TimePeriodWsDTO timePeriod = new TimePeriodWsDTO();
		if (source.getStartTime() != null)
		{
			timePeriod.setStartDateTime(source.getStartTime());
		}

		if (source.getEndTime() != null)
		{
			timePeriod.setEndDateTime(source.getEndTime());
		}

		target.setValidFor(timePeriod);
	}

}
