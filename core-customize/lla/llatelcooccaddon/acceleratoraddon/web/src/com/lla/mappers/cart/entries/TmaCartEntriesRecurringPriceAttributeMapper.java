/*
 *   Copyright (c) 2019 SAP SE or an SAP affiliate company.  All rights reserved.
 */
package com.lla.mappers.cart.entries;

import com.lla.dto.CartCostWsDTO;
import de.hybris.platform.b2ctelcofacades.data.TmaAbstractOrderPriceData;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercewebservicescommons.dto.order.OrderEntryWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.product.PriceWsDTO;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import java.util.Map;
import java.util.Objects;


/**
 * This attribute Mapper class maps itemPrice attributes between {@link OrderEntryData} and {@link OrderEntryWsDTO}
 *
 * @since 1911
 */
public class TmaCartEntriesRecurringPriceAttributeMapper extends TmaAttributeMapper<OrderEntryData, OrderEntryWsDTO>
{
	private MapperFacade mapperFacade;

	private Map<String, Class<PriceWsDTO>> priceTypeDtoMap;

	@Override
	public void populateTargetAttributeFromSource(final OrderEntryData source, final OrderEntryWsDTO target,
			final MappingContext context)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (source.getPrice() == null)
		{
			return;
		}
		final PriceData priceData=source.getEntryRecurringPrice();
		if(Objects.nonNull(priceData))
		{
			final PriceWsDTO cartPrice = getMapperFacade()
					.map(priceData, priceTypeDtoMap.get(priceData.getClass().getSimpleName()),context);
			target.setEntryRecurringPrice(cartPrice);
		}
	}

	protected MapperFacade getMapperFacade()
	{
		return mapperFacade;
	}

	@Required
	public void setMapperFacade(final MapperFacade mapperFacade)
	{
		this.mapperFacade = mapperFacade;
	}

	protected Map<String, Class<PriceWsDTO>> getPriceTypeDtoMap()
	{
		return priceTypeDtoMap;
	}

	@Required
	public void setPriceTypeDtoMap(final Map<String, Class<PriceWsDTO>> priceTypeDtoMap)
	{
		this.priceTypeDtoMap = priceTypeDtoMap;
	}
}
