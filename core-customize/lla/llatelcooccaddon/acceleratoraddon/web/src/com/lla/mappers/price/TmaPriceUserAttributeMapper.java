/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.mappers.price;

import de.hybris.platform.b2ctelcofacades.data.TmaUserData;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.dto.ProductOfferingPriceWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.UserWsDTO;
import de.hybris.platform.subscriptionfacades.data.SubscriptionPricePlanData;

import org.springframework.util.ObjectUtils;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for user attribute between {@link SubscriptionPricePlanData} and
 * {@link ProductOfferingPriceWsDTO}
 *
 * @since 1907
 */
public class TmaPriceUserAttributeMapper
		extends TmaAttributeMapper<SubscriptionPricePlanData, ProductOfferingPriceWsDTO>
{
	@Override
	public void populateTargetAttributeFromSource(final SubscriptionPricePlanData source, final ProductOfferingPriceWsDTO target,
			final MappingContext context)
	{
		if (ObjectUtils.isEmpty(source.getUser()))
		{
			return;
		}

		final TmaUserData user = source.getUser();
		final UserWsDTO userWsDTO = new UserWsDTO();
		userWsDTO.setCustomerId(user.getUid());
		target.setUser(userWsDTO);
	}
}
