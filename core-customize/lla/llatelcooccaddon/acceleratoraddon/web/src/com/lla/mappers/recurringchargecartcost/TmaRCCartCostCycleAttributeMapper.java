/*
 *   Copyright (c) 2019 SAP SE or an SAP affiliate company.  All rights reserved.
 */

package com.lla.mappers.recurringchargecartcost;


import de.hybris.platform.b2ctelcofacades.data.TmaAbstractOrderRecurringChargePriceData;
import com.lla.dto.CycleWsDTO;
import com.lla.dto.RecurringChargeCartCostWsDTO;
import com.lla.mappers.TmaAttributeMapper;

import org.springframework.util.Assert;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for baseType attribute between {@link TmaAbstractOrderRecurringChargePriceData}
 * and {@link RecurringChargeCartCostWsDTO}
 *
 * @since 1911
 */
public class TmaRCCartCostCycleAttributeMapper
		extends TmaAttributeMapper<TmaAbstractOrderRecurringChargePriceData, RecurringChargeCartCostWsDTO>
{
	@Override
	public void populateTargetAttributeFromSource(final TmaAbstractOrderRecurringChargePriceData source,
			final RecurringChargeCartCostWsDTO target, final MappingContext context)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		final CycleWsDTO cycle = new CycleWsDTO();
		if (source.getCycleStart() != null)
		{
			cycle.setCycleStart(source.getCycleStart());
		}
		if (source.getCycleEnd() != null)
		{
			cycle.setCycleEnd(source.getCycleEnd());
		}
		target.setCycle(cycle);
	}
}
