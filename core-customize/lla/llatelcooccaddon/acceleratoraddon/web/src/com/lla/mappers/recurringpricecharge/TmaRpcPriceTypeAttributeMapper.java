/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.mappers.recurringpricecharge;

import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import com.lla.dto.MoneyWsDTO;
import com.lla.dto.RecurringPriceChargeWsDTO;
import de.hybris.platform.subscriptionfacades.data.RecurringChargeEntryData;
import de.hybris.platform.subscriptionservices.model.RecurringChargeEntryModel;

import java.util.Formatter;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for price type attribute between {@link RecurringChargeEntryData} and
 * {@link RecurringPriceCharge}
 *
 * @since 1907
 */
public class TmaRpcPriceTypeAttributeMapper extends TmaAttributeMapper<RecurringChargeEntryData, RecurringPriceChargeWsDTO>
{
	private static final String RECURRING_CHARGE = "recurring";
	private static final String PRICE_TYPE = "BUY";
	
	@Autowired
	private BaseStoreService baseStoreService;
	@Override
	public void populateTargetAttributeFromSource(final RecurringChargeEntryData source, final RecurringPriceChargeWsDTO target,
			final MappingContext context)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setChargeType(RECURRING_CHARGE);
		
		final BaseStoreModel baseStore = baseStoreService.getCurrentBaseStore();
		String currencySymbol=StringUtils.EMPTY;
		if (Objects.nonNull(baseStore)			
				&& baseStore.getUid().equalsIgnoreCase("puertoricoStore")
				&& Objects.nonNull(baseStore.getDefaultCurrency()))
		{
			currencySymbol = baseStore.getDefaultCurrency().getSymbol();
			appreciationPrice(source, target,currencySymbol);
			signatureWithoutAutoPayprice(source, target,currencySymbol);
			signatureWithAutoPayprice(source, target,currencySymbol);
			withoutAutoPayPrice(source, target,currencySymbol);
		}
		if (source.getPrice() != null || source.getPrice().getValue() != null)
		{
			rpcPrice(source, target,currencySymbol);
		}
	}
	
	
	/**
	 * Displaying Price Value
	 * @param source
	 * @param target
	 */	
	private void rpcPrice(final RecurringChargeEntryData source, final RecurringPriceChargeWsDTO target,String currencySymbol)
	{
		final MoneyWsDTO rpcPrice = new MoneyWsDTO();
		rpcPrice.setCurrencyIso(source.getPrice().getCurrencyIso());
		rpcPrice.setValue(source.getPrice().getValue().toString());
		rpcPrice.setFormattedValue(source.getPrice().getFormattedValue());
		rpcPrice.setPriceType(PRICE_TYPE);
		Formatter priceValue = new Formatter();
		if(Objects.nonNull(source.getRecurringLines())) {
			if(null != source.getTotalPriceForLines() && source.getTotalPriceForLines().getValue().doubleValue() > 0){
				priceValue.format("%.2f", source.getTotalPriceForLines().getValue());
			}else {
				priceValue.format("%.2f", source.getPrice().getValue().doubleValue() * source.getRecurringLines());
			}
	   rpcPrice.setTotalPricevalue(priceValue.toString());
	   rpcPrice.setTotalFormattedvalue(StringUtils.join(new String[]{currencySymbol,priceValue.toString()}));
		}
	   target.setPrice(rpcPrice);
	}
	
	
	/**
	 * Displaying Appreciation Price
	 * @param source
	 * @param target
	 */
	private void appreciationPrice(final RecurringChargeEntryData source, final RecurringPriceChargeWsDTO target,String currencySymbol)
	{
		final MoneyWsDTO apprectiationPrice = new MoneyWsDTO();
		apprectiationPrice.setCurrencyIso(source.getPrice().getCurrencyIso());
		apprectiationPrice.setValue(source.getAppreciationPrice().getValue().toString());
		apprectiationPrice.setFormattedValue(source.getAppreciationPrice().getFormattedValue());
		apprectiationPrice.setPriceType(PRICE_TYPE);
		Formatter apprectiationPriceValue = new Formatter();
		if(Objects.nonNull(source.getRecurringLines())) {
		apprectiationPriceValue.format("%.2f", source.getAppreciationPrice().getValue().doubleValue() * source.getRecurringLines());
		apprectiationPrice.setTotalPricevalue(apprectiationPriceValue.toString());
		apprectiationPrice.setTotalFormattedvalue(StringUtils.join(new String[]{currencySymbol,apprectiationPriceValue.toString()}));
		}
		target.setAppreciatePrice(apprectiationPrice);
	}

	
	/**
	 * Displaying Signature Without AutoPay Price
	 * @param source
	 * @param target
	 */
	private void signatureWithoutAutoPayprice(final RecurringChargeEntryData source, final RecurringPriceChargeWsDTO target,String currencySymbol)
	{
		final MoneyWsDTO signatureWithoutAutoPayprice = new MoneyWsDTO();
		signatureWithoutAutoPayprice.setCurrencyIso(source.getPrice().getCurrencyIso());
		signatureWithoutAutoPayprice.setValue(source.getSignatureWithoutAutoPayPrice().getValue().toString());
		signatureWithoutAutoPayprice.setFormattedValue(source.getSignatureWithoutAutoPayPrice().getFormattedValue());
		signatureWithoutAutoPayprice.setPriceType(PRICE_TYPE);
		Formatter signatureWithoutAutoPaypriceValue = new Formatter();
		if(Objects.nonNull(source.getRecurringLines())) {
		signatureWithoutAutoPaypriceValue.format("%.2f", source.getSignatureWithoutAutoPayPrice().getValue().doubleValue() * source.getRecurringLines());
		signatureWithoutAutoPayprice.setTotalPricevalue(signatureWithoutAutoPaypriceValue.toString());
		signatureWithoutAutoPayprice.setTotalFormattedvalue(StringUtils.join(new String[]{currencySymbol,signatureWithoutAutoPaypriceValue.toString()}));
		}
		target.setSigWithoutAutoPayPrice(signatureWithoutAutoPayprice);
		
	}
	

	/**
	 * Displaying Signature With AutoPay Price
	 * @param source
	 * @param target
	 */
	private void signatureWithAutoPayprice(final RecurringChargeEntryData source, final RecurringPriceChargeWsDTO target,String currencySymbol)
	{
		final MoneyWsDTO signatureWithAutoPayprice = new MoneyWsDTO();
		signatureWithAutoPayprice.setCurrencyIso(source.getPrice().getCurrencyIso());
		signatureWithAutoPayprice.setValue(source.getSignatureWithAutoPayPrice().getValue().toString());
		signatureWithAutoPayprice.setFormattedValue(source.getSignatureWithAutoPayPrice().getFormattedValue());
		signatureWithAutoPayprice.setPriceType(PRICE_TYPE);
		Formatter signatureWithAutoPaypriceValue = new Formatter();
		if(Objects.nonNull(source.getRecurringLines())) {
		signatureWithAutoPaypriceValue.format("%.2f", source.getSignatureWithAutoPayPrice().getValue().doubleValue() * source.getRecurringLines());
		signatureWithAutoPayprice.setTotalPricevalue(signatureWithAutoPaypriceValue.toString());
		signatureWithAutoPayprice.setTotalFormattedvalue(StringUtils.join(new String[]{currencySymbol,signatureWithAutoPaypriceValue.toString()}));
		}
		target.setSigWithAutoPayprice(signatureWithAutoPayprice);
	}
	
	
	/**
	 * Displaying without AutoPay Price
	 * @param source
	 * @param target
	 */
	private void withoutAutoPayPrice(final RecurringChargeEntryData source, final RecurringPriceChargeWsDTO target,String currencySymbol)
	{
		final MoneyWsDTO withoutAutoPayPrice = new MoneyWsDTO();
		withoutAutoPayPrice.setCurrencyIso(source.getPrice().getCurrencyIso());
		withoutAutoPayPrice.setValue(source.getWithoutAutoPayPrice().getValue().toString());
		withoutAutoPayPrice.setFormattedValue(source.getWithoutAutoPayPrice().getFormattedValue());
		withoutAutoPayPrice.setPriceType(PRICE_TYPE);
		Formatter withoutAutoPayPriceValue = new Formatter();
		if(Objects.nonNull(source.getRecurringLines())) {
		withoutAutoPayPriceValue.format("%.2f", source.getWithoutAutoPayPrice().getValue().doubleValue() * source.getRecurringLines());
		withoutAutoPayPrice.setTotalPricevalue(withoutAutoPayPriceValue.toString());
		withoutAutoPayPrice.setTotalFormattedvalue(StringUtils.join(new String[]{currencySymbol,withoutAutoPayPriceValue.toString()}));
		}
		target.setWithOutAutoPayprice(withoutAutoPayPrice);
	}
	
}
