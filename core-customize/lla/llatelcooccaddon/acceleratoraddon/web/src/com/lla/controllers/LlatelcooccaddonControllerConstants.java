/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.controllers;

/**
 * Global class for all Llatelcooccaddon constants. You can add global constants for your extension into this class.
 */
public interface LlatelcooccaddonControllerConstants
{
	public static final String LOCATION = "Location";
	public static final String SLASH = "/";
}
