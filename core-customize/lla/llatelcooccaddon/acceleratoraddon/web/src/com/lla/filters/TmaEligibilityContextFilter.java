/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.filters;

import de.hybris.platform.b2ctelcoservices.compatibility.eligibility.TmaEligibilityContextService;
import de.hybris.platform.b2ctelcoservices.eligibility.data.TmaEligibilityContext;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;


/**
 * Filter responsible for updating the {@link TmaEligibilityContext} for the current user.
 *
 * @since 1907
 */
public class TmaEligibilityContextFilter extends OncePerRequestFilter
{
	private static final String ROLE_TRUSTED_CLIENT = "ROLE_TRUSTED_CLIENT";

	private final TmaEligibilityContextService eligibilityContextService;
	private final String regexp;
	private final String regexpProductApi;


	/**
	 * @param eligibilityContextService
	 * @param regexp
	 * @param regexpProductApi
	 */
	public TmaEligibilityContextFilter(final TmaEligibilityContextService eligibilityContextService, final String regexp,
			final String regexpProductApi)
	{
		super();
		this.eligibilityContextService = eligibilityContextService;
		this.regexp = regexp;
		this.regexpProductApi = regexpProductApi;
	}

	@Override
	protected void doFilterInternal(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse,
			final FilterChain filterChain) throws ServletException, IOException
	{
		if (matchesUrl(httpServletRequest, regexp) || matchesUrl(httpServletRequest, regexpProductApi))
		{
			getEligibilityContextService().updateEligibilityContexts(true);

			getEligibilityContextService().updateApplyEligibilityFlag(!hasAdminRole());
		}

		filterChain.doFilter(httpServletRequest, httpServletResponse);
	}

	protected boolean matchesUrl(final HttpServletRequest request, final String regexp)
	{
		final Matcher matcher = getMatcher(request, regexp);
		if (matcher.find())
		{
			return true;
		}
		return false;
	}

	protected Matcher getMatcher(final HttpServletRequest request, final String regexp)
	{
		final Pattern pattern = Pattern.compile(regexp);
		final String path = request.getPathInfo() != null ? request.getPathInfo() : "";
		return pattern.matcher(path);
	}

	protected boolean hasAdminRole()
	{
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		for (final GrantedAuthority ga : authentication.getAuthorities())
		{
			if (ga.getAuthority().equals(ROLE_TRUSTED_CLIENT))
			{
				return true;
			}
		}
		return false;
	}

	protected TmaEligibilityContextService getEligibilityContextService()
	{
		return eligibilityContextService;
	}


	protected String getRegexp()
	{
		return regexp;
	}

	protected String getRegexpProductApi()
	{
		return regexpProductApi;
	}

}
