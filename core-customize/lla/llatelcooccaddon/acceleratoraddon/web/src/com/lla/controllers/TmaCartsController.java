/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.controllers;

import com.lla.core.util.LLACommonUtil;
import de.hybris.platform.b2ctelcoaddon.dto.PlaceWsDTO;
import de.hybris.platform.b2ctelcoaddon.dto.ProductCharacteristicWsDTO;
import de.hybris.platform.b2ctelcofacades.data.CartActionInput;
import de.hybris.platform.b2ctelcofacades.data.CartDataList;
import de.hybris.platform.b2ctelcofacades.data.TmaPlaceData;
import de.hybris.platform.b2ctelcofacades.order.TmaCartFacade;
import de.hybris.platform.b2ctelcofacades.stock.TmaStockFacade;
import com.lla.dto.RelatedPartyWsDTO;
import com.lla.validator.TmaAnonymousCartValidator;
import com.lla.validator.TmaUserCartValidator;
import de.hybris.platform.b2ctelcoservices.data.TmaProductSpecCharacteristicConfigItem;
import de.hybris.platform.b2ctelcoservices.enums.TmaPlaceRoleType;
import de.hybris.platform.b2ctelcoservices.enums.TmaRelatedPartyRole;
import de.hybris.platform.b2ctelcoservices.enums.TmaSubscribedProductAction;
import de.hybris.platform.b2ctelcoservices.payment.exception.InvalidPaymentInfoException;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.SaveCartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.request.mapping.annotation.RequestMappingOverride;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commercewebservicescommons.dto.order.CartListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.CartModificationWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.CartWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.OrderEntryWsDTO;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.*;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdAndUserIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdUserIdAndCartIdParam;

import java.util.*;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;



/**
 * Controller for cart related requests such as retrieving/delete/update/... a cart
 *
 * @since 1911
 */
@Controller
@RequestMapping(value = "/{baseSiteId}/{lang}/users/{userId}/carts")
@CacheControl(directive = CacheControlDirective.NO_CACHE)
@Api(tags = "Carts")
public class TmaCartsController extends BaseController
{
	private static final Logger LOG = LoggerFactory.getLogger(TmaCartsController.class);
	private static final String DEFAULT_PAGE_SIZE = "20";
	private static final String DEFAULT_CURRENT_PAGE = "0";
	private static final String ANONYMOUS = "anonymous";
	private static final String CURRENT = "current";
	private static final long DEFAULT_PRODUCT_QUANTITY = 1;
	private static final int DEFAULT_GROUP_NUMBER = -1;
	private static final String REGION = "region";

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "tmaWebServicesCartFacade")
	private TmaCartFacade tmaWebServicesCartFacade;

	@Resource(name = "customerFacade")
	private CustomerFacade customerFacade;

	@Resource(name = "saveCartFacade")
	private SaveCartFacade saveCartFacade;

	@Resource(name = "tmaStockFacade")
	private TmaStockFacade tmaStockFacade;

	@Resource(name = "tmaOrderEntryCreateValidator")
	private Validator tmaOrderEntryCreateValidator;

	@Resource(name = "tmaOrderEntryUpdateValidator")
	private Validator tmaOrderEntryUpdateValidator;

	@Resource(name = "anonymousCartValidator")
	private TmaAnonymousCartValidator anonymousCartValidator;

	@Resource(name = "userCartValidator")
	private TmaUserCartValidator userCartValidator;


	@Resource(name="llaCartFacade")
	private com.lla.facades.cart.LLACartFacade llaCartFacade;

	@Resource(name = "llaCommonUtil")
	private LLACommonUtil llaCommonUtil;

	private final String[] DISALLOWED_FIELDS = new String[] {};

	@RequestMapping(method = RequestMethod.GET)
	@RequestMappingOverride(priorityProperty = "llatelcooccaddon.TmaCartsController.getCarts.priority")
	@ResponseBody
	@ApiOperation(value = "Get all customer carts.", notes = "Lists all customer carts.")
	@ApiBaseSiteIdAndUserIdParam
	public CartListWsDTO getCarts(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL") @RequestParam(required = false, defaultValue = DEFAULT_FIELD_SET) final String fields,
			@ApiParam(value = "Language identifier")  @RequestParam(required = false) final String lang,
			@ApiParam(value = "Optional parameter. If the parameter is provided and its value is true, only saved carts are returned.") @RequestParam(defaultValue = "false") final boolean savedCartsOnly,
			@ApiParam(value = "Optional pagination parameter in case of savedCartsOnly == true. Default value 0.") @RequestParam(defaultValue = DEFAULT_CURRENT_PAGE) final int currentPage,
			@ApiParam(value = "Optional {@link PaginationData} parameter in case of savedCartsOnly == true. Default value 20.") @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) final int pageSize,
			@ApiParam(value = "Optional sort criterion in case of savedCartsOnly == true. No default value.") @RequestParam(required = false) final String sort)
	{
		if (userFacade.isAnonymousUser())
		{
			throw new AccessDeniedException("Access is denied");
		}
		final CartDataList cartDataList = new CartDataList();
		final PageableData pageableData = new PageableData();
		pageableData.setCurrentPage(currentPage);
		pageableData.setPageSize(pageSize);
		pageableData.setSort(sort);
		final List<CartData> allCarts = new ArrayList<>(
				saveCartFacade.getSavedCartsForCurrentUser(pageableData, null).getResults());
		if (!savedCartsOnly)
		{
			allCarts.addAll(tmaWebServicesCartFacade.getCartsForCurrentUser());
		}
		cartDataList.setCarts(allCarts);
		return getDataMapper().map(cartDataList, CartListWsDTO.class, fields);
	}

	@RequestMapping(value = "/{cartId}", method = RequestMethod.GET)
	@RequestMappingOverride(priorityProperty = "llatelcooccaddon.TmaCartsController.getCart.priority")
	@ResponseBody
	@ApiOperation(nickname = "getCart", value = "Get a cart with a given identifier.", notes = "Returns the cart with a given identifier.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartWsDTO getCart(
			@ApiParam(value = "Identifier of the Shopping Cart", required = true) @PathVariable("cartId") final String cartId,
			@ApiParam(value = "Language identifier")  @RequestParam(required = false) final String lang,
			@ApiParam(value = "Identifier of the Customer", required = true) @PathVariable("userId") final String userId,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL") @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields)
	{
		Optional<CartData> cartData;

		final String customerId = userId.equalsIgnoreCase(CURRENT) ? customerFacade.getCurrentCustomerUid() : userId;

		if (userId.equalsIgnoreCase(ANONYMOUS))
		{
			cartData = tmaWebServicesCartFacade.getCartForGuid(cartId);
		}
		else
		{
			cartData = tmaWebServicesCartFacade.getCartForCodeAndCustomer(cartId, customerId);
		}

		if (!cartData.isPresent())
		{
			throw new CartException("Cart not found!", CartException.NOT_FOUND);
		}
		return getDataMapper().map(cartData.get(), CartWsDTO.class, fields);
	}

/*	@RequestMapping(value = "/{cartId}/entries", method = RequestMethod.POST, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	@ApiOperation(nickname = "createCartEntry", value = "Adds a product to the cart.", notes = "Applies prices to the Cart entries based on the location of the customer when the attribute region is provided and stores other details like formerSupplier of the customer.")
	@RequestMappingOverride(priorityProperty = "llatelcooccaddon.TmaCartsController.createCartEntry.priority")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartModificationWsDTO createCartEntry(
			@ApiParam(value = "Identifier of the Shopping Cart", required = true) @PathVariable("cartId") final String cartId,
			@ApiParam(value = "Identifier of the Customer", required = true) @PathVariable("userId") final String userId,
			@ApiParam(value = "Base site identifier", required = true) @PathVariable final String baseSiteId,
			@ApiParam(value = "Request body parameter that contains details such as the product code (product.code), BPO Code (rootBpoCode), ProcessType (processType.id), GroupNumber (entryGroupNumbers), the quantity of product (quantity), and the pickup store name (deliveryPointOfService.name).\n\nThe DTO is in XML or .json format.", required = true) @RequestBody final OrderEntryWsDTO entry,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL") @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields)
			throws CommerceCartModificationException
	{
		populateDefaultCartEntryValues(entry);
		validate(entry, "entry", tmaOrderEntryCreateValidator);
		final Integer groupNumber = entry.getEntryGroupNumbers().stream().findFirst().get();
		return addCartEntryInternal(baseSiteId, fields, entry, groupNumber, cartId, userId);
	}*/

	@RequestMapping(method = RequestMethod.POST)
	@RequestMappingOverride(priorityProperty = "llatelcooccaddon.TmaCartsController.createCart.priority")
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "createCart", value = "Creates or restore a cart for a user.", notes = "Creates a new cart or restores an anonymous cart as a user’s cart (if an old Cart Id is given in the request), and also considers region as a distinguishing factor for merging cart entries or adding a new cart entries.")
	@ApiBaseSiteIdAndUserIdParam
	public CartWsDTO createCart(@ApiParam(value = "Anonymous cart GUID.") @RequestParam(required = false) final String oldCartId,
								@ApiParam(value = "Language identifier")  @RequestParam(required = false) final String lang,
								@ApiParam(value = "The GUID of the user's cart that will be merged with the anonymous cart.") @RequestParam(required = false) final String toMergeCartGuid,
			@ApiParam(value = "Identifier of the Customer", required = true) @PathVariable("userId") final String userId,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL") @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields)

	{
		if (StringUtils.isEmpty(oldCartId))
		{
			return getRestoredCart(toMergeCartGuid, userId, fields);
		}
		return mergeCarts(oldCartId, toMergeCartGuid, userId, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/paymentdetails", method = RequestMethod.PUT)
	@RequestMappingOverride(priorityProperty = "llatelcooccaddon.TmaCartsController.replaceCartPaymentDetails.priority")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "replaceCartPaymentDetails", value = "Sets credit card payment details for the cart.", notes = "Sets credit card payment details for the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void replaceCartPaymentDetails(
			@ApiParam(value = "Identifier of the Shopping Cart", required = true) @PathVariable("cartId") final String cartId,
			@ApiParam(value = "Language identifier")  @RequestParam(required = false) final String lang,
			@ApiParam(value = "Identifier of the Customer", required = true) @PathVariable("userId") final String userId,
			@ApiParam(value = "Payment details identifier.", required = true) @RequestParam final String paymentDetailsId)
			throws InvalidPaymentInfoException, CommerceCartModificationException
	{
		Assert.state(!userId.equalsIgnoreCase(ANONYMOUS), "Checkout user must not be the anonymous user");

		final CartActionInput cartActionInput = new CartActionInput();
		cartActionInput.setCartId(cartId);
		cartActionInput.setUserGuid(userId);
		cartActionInput.setPaymentMethodId(paymentDetailsId);

		tmaWebServicesCartFacade.processCartAction(Collections.singletonList(cartActionInput)).get(0);
	}

	@RequestMapping(value = "/{cartId}/entries/{entryNumber}", method = RequestMethod.DELETE)
	@RequestMappingOverride(priorityProperty = "llatelcooccaddon.TmaCartsController.deleteCartEntry.priority")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "removeCartEntry", value = "Deletes cart entry.", notes = "Deletes cart entry.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void removeCartEntry(
			@ApiParam(value = "The entry number. Each entry in a cart has an entry number. Cart entries are numbered in ascending order, starting with zero (0).", required = true) @PathVariable final long entryNumber,
			@ApiParam(value = "Language identifier")  @RequestParam(required = false) final String lang,
			@ApiParam(value = "The ID of the user's cart that will be cloned.", required = true) @PathVariable("cartId") final String cartId,
			@ApiParam(value = "Identifier of the Customer", required = true) @PathVariable("userId") final String userId)
			throws CommerceCartModificationException
	{
		final CartActionInput cartActionInput = new CartActionInput();
		if (tmaWebServicesCartFacade.isAnonymousUserCart(cartId))
		{
			cartActionInput.setToCartGUID(cartId);
		}
		else
		{
			cartActionInput.setCartId(cartId);
		}
		cartActionInput.setUserGuid(userId);
		cartActionInput.setQuantity(Long.valueOf(0));
		cartActionInput.setEntryNumber((int) entryNumber);
		if(llaCommonUtil.isSiteVTR())
		{
			final CartData cart = llaCartFacade.getSessionCart();
			OrderEntryData orderEntryData = getCartEntryForNumber(cart, entryNumber);
			Collection<CategoryData> cat = orderEntryData.getProduct().getCategories();
			if(planFixedProduct(cat))
			{
				llaCartFacade.removeSessionCart();
			}
			else
				tmaWebServicesCartFacade.processCartAction(Collections.singletonList(cartActionInput)).get(0);

		}
		else
		   tmaWebServicesCartFacade.processCartAction(Collections.singletonList(cartActionInput)).get(0);
	}

	private boolean planFixedProduct(Collection<CategoryData> cat) {
		List<String> planBundleCategory = Arrays.asList("devices", "triplepack", "doublepack", "internet", "television", "2PinternetTv", "2PphoneTv", "2PinternetPhone", "");
		return cat.stream().anyMatch(c-> planBundleCategory.contains(c.getCode()));
	}
	protected OrderEntryData getCartEntryForNumber(CartData cart, long number)
	{
		final List<OrderEntryData> entries = cart.getEntries();
		if (entries != null && !entries.isEmpty())
		{
			final Integer requestedEntryNumber = Integer.valueOf((int) number);
			for (final OrderEntryData entry : entries)
			{
				if (entry != null && requestedEntryNumber.equals(entry.getEntryNumber()))
				{
					return entry;
				}
			}
		}
		throw new CartEntryException("Entry not found", CartEntryException.NOT_FOUND, String.valueOf(number));
	}


	@RequestMapping(value = "/{cartId}/entries/{entryNumber}", method = RequestMethod.PATCH, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@RequestMappingOverride(priorityProperty = "llatelcooccaddon.TmaCartsController.updateCartEntry.priority")
	@ResponseBody
	@ApiOperation(nickname = "updateCartEntry", value = "Update quantity and store details of a cart entry.", notes = "Updates the quantity of a single cart entry and the details of the store where the cart entry will be picked up, and also updates the other details  like region, formerSupplier of the customer.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartModificationWsDTO updateCartEntry(
			@ApiParam(value = "Base site identifier.", required = true) @PathVariable final String baseSiteId,
			@ApiParam(value = "Language identifier")  @RequestParam(required = false) final String lang,
			@ApiParam(value = "The entry number. Each entry in a cart has an entry number. Cart entries are numbered in ascending order, starting with zero (0).", required = true) @PathVariable final long entryNumber,
			@ApiParam(value = "Request body parameter that contains details such as the quantity of product (quantity), and the pickup store name (deliveryPointOfService.name)\n\nThe DTO is in XML or .json format.", required = true) @RequestBody final OrderEntryWsDTO entry,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL") @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields,
			@ApiParam(value = "The ID of the user's cart that will be updated", required = true) @PathVariable("cartId") final String cartId,
			@ApiParam(value = "Identifier of the Customer", required = true) @PathVariable("userId") final String userId)
			throws CommerceCartModificationException
	{
		entry.setEntryNumber((int) entryNumber);
		validate(entry, "entry", tmaOrderEntryUpdateValidator);
		final CartActionInput cartActionInput = new CartActionInput();
		if (tmaWebServicesCartFacade.isAnonymousUserCart(cartId))
		{
			cartActionInput.setToCartGUID(cartId);
		}
		else
		{
			cartActionInput.setCartId(cartId);
		}
		cartActionInput.setUserGuid(userId);
		cartActionInput.setQuantity(entry.getQuantity());
		cartActionInput.setEntryNumber((int) entryNumber);
		cartActionInput.setEntryId(String.valueOf(entryNumber));
		setCpiInformation(entry, cartActionInput);
		cartActionInput.setAppointmentId(entry.getAppointment() != null ? entry.getAppointment().getId() : null);
		cartActionInput.setPlaces(getPlaces(entry));
		cartActionInput.setServiceProvider(
				entry.getSubscribedProduct() != null ? getServiceProvider(entry.getSubscribedProduct().getRelatedParty()) : null);
		cartActionInput.setContractStartDate(entry.getContractStartDate());
		setProductCharacteristics(entry, cartActionInput);

		if (entry.getDeliveryPointOfService() != null)
		{
			cartActionInput.setStoreId(entry.getDeliveryPointOfService().getName());
		}

		final CartModificationData cartModificationData = tmaWebServicesCartFacade
				.processCartAction(Collections.singletonList(cartActionInput)).get(0);

		return getDataMapper().map(cartModificationData, CartModificationWsDTO.class, fields);
	}

	/**
	 * Merges anonymous cart with given user cart and returns user cart.
	 *
	 * @param oldCartId
	 *           the old cart id
	 * @param evaluatedToMergeCartGuid
	 *           the evaluated to merge cart guid
	 * @param userId
	 *           the user id
	 * @param fields
	 *           the fields
	 * @return the cart ws DTO
	 * @throws CartException
	 *            the cart exception
	 */
	protected CartWsDTO mergeCarts(final String oldCartId, final String evaluatedToMergeCartGuid, final String userId,
			final String fields) throws CartException
	{
		validate(oldCartId, "String", anonymousCartValidator);
		validate(evaluatedToMergeCartGuid, "String", userCartValidator);
		try
		{
			final CartActionInput cartActionInput = new CartActionInput();
			cartActionInput.setFromCartGUID(oldCartId);
			cartActionInput.setToCartGUID(evaluatedToMergeCartGuid);
			cartActionInput.setUserGuid(userId);
			final List<CartModificationData> cartModificationData = tmaWebServicesCartFacade
					.processCartAction(Collections.singletonList(cartActionInput));
			if (CollectionUtils.isNotEmpty(cartModificationData))
			{
				return getDataMapper().map(
						tmaWebServicesCartFacade.getCartForCodeAndCustomer(cartModificationData.get(0).getCartCode(), userId).get(),
						CartWsDTO.class, fields);
			}
			return getDataMapper().map(tmaWebServicesCartFacade.getSessionCart(), CartWsDTO.class, fields);


		}
		catch (final CommerceCartModificationException e)
		{
			throw new CartException("Couldn't merge cart " + oldCartId + " with " + evaluatedToMergeCartGuid,
					CartException.CANNOT_MERGE, e);
		}
	}

	/**
	 * Gets restored cart or session cart when oldCartId is not provided.
	 *
	 * @param toMergeCartGuid
	 *           the evaluated to merge cart guid
	 * @param userId
	 *           the user id
	 * @param fields
	 *           the fields
	 * @return the restored cart
	 * @throws CartException
	 *            the cart exception
	 */
	protected CartWsDTO getRestoredCart(final String toMergeCartGuid, final String userId, final String fields)
			throws CartException
	{
		if (StringUtils.isEmpty(toMergeCartGuid))
		{
			return getDataMapper().map(tmaWebServicesCartFacade.getSessionCart(), CartWsDTO.class, fields);
		}
		if (!tmaWebServicesCartFacade.isCurrentUserCart(toMergeCartGuid))
		{
			throw new CartException("Cart is not current user's cart", CartException.CANNOT_RESTORE, toMergeCartGuid);
		}
		try
		{
			tmaWebServicesCartFacade.restoreSavedCart(toMergeCartGuid);
			return getDataMapper().map(tmaWebServicesCartFacade.getSessionCart(), CartWsDTO.class, fields);
		}
		catch (final CommerceCartRestorationException e)
		{
			throw new CartException("Couldn't restore cart", CartException.CANNOT_RESTORE, toMergeCartGuid, e);
		}
	}

	protected CartModificationWsDTO addCartEntryInternal(final String baseSiteId, final String fields, final OrderEntryWsDTO entry,
			final Integer cartGroupNo, final String cartId, final String userId) throws CommerceCartModificationException
	{
		CartActionInput cartActionInput;
		final String pickupStore = entry.getDeliveryPointOfService() == null ? null : entry.getDeliveryPointOfService().getName();
		if (pickupStore != null && entry.getRootBpoCode() == null)
		{
			validateIfProductIsInStockInPOS(baseSiteId, entry.getProduct().getCode(), entry.getDeliveryPointOfService().getName(),
					null);
			cartActionInput = createCartItemAction(entry, StringUtils.EMPTY, cartGroupNo, cartId, userId, pickupStore);
		}
		else
		{
			cartActionInput = createCartItemAction(entry, entry.getRootBpoCode(), cartGroupNo, cartId, userId, StringUtils.EMPTY);
		}
		final CartModificationData cartModificationData = tmaWebServicesCartFacade
				.processCartAction(Collections.singletonList(cartActionInput)).get(0);

		return getDataMapper().map(cartModificationData, CartModificationWsDTO.class, fields);
	}

	/**
	 * Creates a {@link CartActionInput} for a {@link OrderEntryWsDTO}.
	 *
	 * @param entry
	 *           the cart entry
	 * @param rootBpoCode
	 *           the bpo Code
	 * @param cartGroupNo
	 *           the group number
	 * @param cartId
	 *           the identifier of the shopping cart
	 * @param userId
	 *           the identifier of the user
	 * @param pickupStore
	 *           the identifier of the pickupStore
	 * @return instance of {@link CartActionInput}
	 */
	protected CartActionInput createCartItemAction(final OrderEntryWsDTO entry, final String rootBpoCode,
			final Integer cartGroupNo, final String cartId, final String userId, final String pickupStore)
	{
		final CartActionInput cartActionInput = new CartActionInput();
		Optional<CartData> cartdata = null;
		if (tmaWebServicesCartFacade.isAnonymousUserCart(cartId))
		{
			cartActionInput.setToCartGUID(cartId);
			cartdata = tmaWebServicesCartFacade.getCartForGuid(cartId);
		}
		else
		{
			cartActionInput.setCartId(cartId);
			cartdata = tmaWebServicesCartFacade.getCartForCodeAndCustomer(cartId, userId);
		}
		cartActionInput.setUserGuid(userId);

		if (entry.getProduct() != null)
		{
			cartActionInput.setProductCode(entry.getProduct().getCode());
		}
		cartActionInput.setQuantity(entry.getQuantity());
		cartActionInput.setProcessType(entry.getProcessType().getId());
		cartActionInput.setRootBpoCode(rootBpoCode);
		cartActionInput.setCartGroupNo(cartGroupNo);
		cartActionInput.setStoreId(pickupStore);
		if (entry.getSubscriptionTerm() != null)
		{
			cartActionInput.setSubscriptionTermId(entry.getSubscriptionTerm().getId());
		}
		setCpiInformation(entry, cartActionInput);
		cartActionInput.setAppointmentId(entry.getAppointment() != null ? entry.getAppointment().getId() : null);
		cartActionInput.setPlaces(getPlaces(entry));
		setProductCharacteristics(entry, cartActionInput);
		cartActionInput.setServiceProvider(
				entry.getSubscribedProduct() != null ? getServiceProvider(entry.getSubscribedProduct().getRelatedParty()) : null);
		cartActionInput.setContractStartDate(entry.getContractStartDate());
		return cartActionInput;
	}

	/**
	 * Sets CPI related information
	 *
	 * @param entry
	 *           the order entry
	 * @param cartActionInput
	 *           the cart action input
	 */
	protected void setCpiInformation(final OrderEntryWsDTO entry, final CartActionInput cartActionInput)
	{
		if (entry.getAction() != null)
		{
			cartActionInput.setAction(TmaSubscribedProductAction.valueOf(entry.getAction().name()));
		}
		cartActionInput
				.setSubscribedProductCode(entry.getSubscribedProduct() != null ? entry.getSubscribedProduct().getId() : null);
	}

	/**
	 * Sets PSCV related data from the entry on the cartActionInput
	 *
	 * @param entry
	 *           the order entry
	 * @param cartActionInput
	 *           the cart action input
	 */
	protected void setProductCharacteristics(final OrderEntryWsDTO entry, final CartActionInput cartActionInput)
	{
		final List<ProductCharacteristicWsDTO> productCharacteristics = getCharacteristics(entry);
		if (CollectionUtils.isNotEmpty(productCharacteristics))
		{
			final Set<TmaProductSpecCharacteristicConfigItem> characteristicConfigItems = productCharacteristics.stream()
					.map(this::getProductSpecCharacteristicConfigItem).collect(Collectors.toSet());
			cartActionInput.setConfigurableProductSpecCharacteristics(characteristicConfigItems);
		}
	}

	protected void populateDefaultCartEntryValues(final OrderEntryWsDTO entry) throws CommerceCartModificationException
	{
		if (entry.getQuantity() == null)
		{
			entry.setQuantity(Long.valueOf(DEFAULT_PRODUCT_QUANTITY));
		}
		if (entry.getProcessType() == null)
		{
			throw new CommerceCartModificationException("Process type cannot be empty");
		}
		if (CollectionUtils.isEmpty(entry.getEntryGroupNumbers()))
		{
			final Collection<Integer> defaultGroupNumber = new ArrayList<>();
			defaultGroupNumber.add(DEFAULT_GROUP_NUMBER);
			entry.setEntryGroupNumbers(defaultGroupNumber);
		}
	}

	/**
	 * @param baseSiteId
	 *           the store identifier
	 * @param productCode
	 *           the product identifier
	 * @param storeName
	 *           the store name
	 * @param entryNumber
	 *           the long value for entry
	 */
	protected void validateIfProductIsInStockInPOS(final String baseSiteId, final String productCode, final String storeName,
			final Long entryNumber)
	{
		if (!tmaStockFacade.isStockSystemEnabled(baseSiteId))
		{
			throw new StockSystemException("Stock system is not enabled on this site", StockSystemException.NOT_ENABLED, baseSiteId);
		}
		final StockData stock = tmaStockFacade.getStockDataForProductAndPointOfService(productCode, storeName);
		if (stock != null && stock.getStockLevelStatus().equals(StockLevelStatus.OUTOFSTOCK))
		{
			if (entryNumber != null)
			{
				throw new LowStockException("Product [" + sanitize(productCode) + "] is currently out of stock", //NOSONAR
						LowStockException.NO_STOCK, String.valueOf(entryNumber));
			}
			else
			{
				throw new ProductLowStockException("Product [" + sanitize(productCode) + "] is currently out of stock",
						LowStockException.NO_STOCK, productCode);
			}
		}
		else if (stock != null && stock.getStockLevelStatus().equals(StockLevelStatus.LOWSTOCK))
		{
			if (entryNumber != null)
			{
				throw new LowStockException("Not enough product in stock", LowStockException.LOW_STOCK, String.valueOf(entryNumber));
			}
			else
			{
				throw new ProductLowStockException("Not enough product in stock", LowStockException.LOW_STOCK, productCode);
			}
		}
	}

	/**
	 * Returns the places from the product in the order entry.
	 *
	 * @param orderEntry
	 *           the order entry
	 * @return The {@link TmaPlaceData}s found.
	 */
	private List<TmaPlaceData> getPlaces(final OrderEntryWsDTO orderEntry)
	{
		final List<TmaPlaceData> tmaPlacesData = new ArrayList<>();
		if (orderEntry.getSubscribedProduct() == null || orderEntry.getSubscribedProduct().getPlace() == null)
		{
			return tmaPlacesData;
		}
		final List<PlaceWsDTO> inputPlaces = orderEntry.getSubscribedProduct().getPlace();
		inputPlaces.forEach(inputPlace -> {
			final TmaPlaceData placeData = new TmaPlaceData();
			placeData.setId(inputPlace.getId());
			placeData.setRole(TmaPlaceRoleType.valueOf(inputPlace.getRole()));
			tmaPlacesData.add(placeData);
		});
		return tmaPlacesData;
	}

	/**
	 * Returns the configured characteristics of the product offering if it is present, else it returns empty list.
	 *
	 * @param cartItem
	 *           the cart item
	 * @return the configured characteristics of the product offering
	 */
	private List<ProductCharacteristicWsDTO> getCharacteristics(final OrderEntryWsDTO cartItem)
	{
		return cartItem.getSubscribedProduct() != null
				&& CollectionUtils.isNotEmpty(cartItem.getSubscribedProduct().getCharacteristic())
						? cartItem.getSubscribedProduct().getCharacteristic()
						: new ArrayList<>();
	}

	private TmaProductSpecCharacteristicConfigItem getProductSpecCharacteristicConfigItem(
			final ProductCharacteristicWsDTO productCharacteristic)
	{
		final TmaProductSpecCharacteristicConfigItem characteristicConfigItem = new TmaProductSpecCharacteristicConfigItem();
		characteristicConfigItem.setName(productCharacteristic.getName());
		characteristicConfigItem.setValue(productCharacteristic.getValue());
		return characteristicConfigItem;
	}

	private String getServiceProvider(final List<RelatedPartyWsDTO> relatedParties)
	{
		if (CollectionUtils.isNotEmpty(relatedParties))
		{
			for (final RelatedPartyWsDTO relatedParty : relatedParties)
			{
				if (StringUtils.equalsIgnoreCase(relatedParty.getRole(), TmaRelatedPartyRole.SERVICE_PROVIDER.name()))
				{
					return relatedParty.getId();
				}

			}
		}
		return null;
	}

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		binder.setDisallowedFields(DISALLOWED_FIELDS);
	}


	@Secured({ "ROLE_CLIENT", "ROLE_CUSTOMERGROUP", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_GUEST" })
	@RequestMapping(value = "/{cartId}/doAutoPay/{autoPay}/{numberOfLines}", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	@ApiOperation(nickname = "doAutoPay", value = "Applies a AutoPay for the cart.", notes = "Applies a AutoPay for the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartWsDTO doAutoPay(
			@ApiParam(value = "Base site identifier.", required = true) @PathVariable final String baseSiteId,
			@ApiParam(value = "Language identifier")  @RequestParam(required = false) final String lang,
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL") @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields,
			@ApiParam(value = "The ID of the user's cart that will be updated", required = true) @PathVariable("cartId") final String cartId,
			@ApiParam(value = "Identifier of the Customer", required = true) @PathVariable("userId") final String userId,
			@ApiParam(value = "Number of Lines", required = true) @PathVariable("numberOfLines") final Integer numberOfLines,
			@ApiParam(value = "AutoPay identifier (boolean)", required = true) @PathVariable final Boolean autoPay) throws CalculationException {
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Applying autopay for cart  : ", autoPay);
		}
		llaCartFacade.calculateAutoPayDetails(autoPay,numberOfLines);
		return getDataMapper().map(tmaWebServicesCartFacade.getSessionCart(), CartWsDTO.class, DEFAULT_FIELD_SET);
	}
}
