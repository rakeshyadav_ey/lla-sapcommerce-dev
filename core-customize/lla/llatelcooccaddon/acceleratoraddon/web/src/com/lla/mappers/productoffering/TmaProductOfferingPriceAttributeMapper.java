package com.lla.mappers.productoffering;

import com.lla.dto.ProductOfferingPriceWsDTO;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercewebservicescommons.dto.product.ProductWsDTO;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

public class TmaProductOfferingPriceAttributeMapper extends TmaAttributeMapper<ProductData, ProductWsDTO>
{
    private MapperFacade mapperFacade;

    @Override
    public void populateTargetAttributeFromSource(final ProductData source, final ProductWsDTO target,
                                                  final MappingContext context)
    {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        if (CollectionUtils.isEmpty(source.getProductOfferingPrices()))
        {
            return;
        }

        final List<ProductOfferingPriceWsDTO> prodList = new ArrayList<>();

        source.getProductOfferingPrices().forEach(addon ->
        {
            final ProductOfferingPriceWsDTO productWsValue = getMapperFacade().map(addon,
                    ProductOfferingPriceWsDTO.class, context);
            prodList.add(productWsValue);
        });
        target.setProductOfferingPrice(prodList);
    }


    public MapperFacade getMapperFacade() {
        return mapperFacade;
    }

    @Required
    public void setMapperFacade(MapperFacade mapperFacade) {
        this.mapperFacade = mapperFacade;
    }

}