/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.exception;

/**
 * Throws TmaInvalidUserException if userID is null
 *
 * @since 1907
 */
public class TmaInvalidUserException extends RuntimeException
{
	public TmaInvalidUserException()
	{
		super("No [User ID] parameter specified for pre authorization");
	}
}
