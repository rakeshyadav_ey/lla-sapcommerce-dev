/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.mappers.price;

import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.dto.ProcessTypeWsDTO;
import com.lla.dto.ProductOfferingPriceWsDTO;
import de.hybris.platform.subscriptionfacades.data.SubscriptionPricePlanData;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;

import ma.glasnost.orika.MappingContext;


/**
 * TmaPriceProcessTypeAttributeMapper populates value of processType attribute from {@link SubscriptionPricePlanData} to
 * {@link ProductOfferingPriceWsDTO}
 *
 * @since 1907
 */
public class TmaPriceProcessTypeAttributeMapper
		extends TmaAttributeMapper<SubscriptionPricePlanData, ProductOfferingPriceWsDTO>
{

	@Override
	public void populateTargetAttributeFromSource(final SubscriptionPricePlanData source, final ProductOfferingPriceWsDTO target,
			final MappingContext context)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (CollectionUtils.isEmpty(source.getProcessTypes()))
		{
			return;
		}

		final List<ProcessTypeWsDTO> processTypes = new ArrayList<>();
		source.getProcessTypes().forEach(processType ->
		{
			final ProcessTypeWsDTO processTypeDto = new ProcessTypeWsDTO();
			processTypeDto.setId(processType.getCode());
			processTypes.add(processTypeDto);

		});

		target.setProcessType(processTypes);
	}

}
