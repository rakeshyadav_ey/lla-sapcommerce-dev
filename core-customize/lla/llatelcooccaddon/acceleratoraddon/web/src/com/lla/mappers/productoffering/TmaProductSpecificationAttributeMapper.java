/*
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 *
 */
package com.lla.mappers.productoffering;

import com.lla.dto.ProdSpecCharValueUseWsDTO;
import com.lla.dto.ProductSpecCharacteristicValueWsDTO;
import de.hybris.platform.addonsupport.controllers.cms.AbstractCMSAddOnComponentController;

import com.lla.dto.*;
import de.hybris.platform.b2ctelcofacades.data.TmaProductSpecCharacteristicValueData;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercewebservicescommons.dto.product.ProductWsDTO;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * TmaProductSpecificationAttributeMapper populates value of productSpecification attribute from {@link ProductData} to
 * {@link ProductWsDTO}
 *
 * @since 1907
 */
public class TmaProductSpecificationAttributeMapper extends TmaAttributeMapper<ProductData, ProductWsDTO>
{
	private MapperFacade mapperFacade;

	@Override
	public void populateTargetAttributeFromSource(final ProductData source, final ProductWsDTO target,
			final MappingContext context)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		
		target.setNewFlag(source.isNewFlag());
		setRequiredFields(source, target);
		setProductSpecificationValueUseData(source, target, context);
		if (source.getProductSpecification() == null)
		{
			return;
		}

		final ProductSpecificationWsDTO productSpecificationRef = getMapperFacade().map(source.getProductSpecification(),
				ProductSpecificationWsDTO.class, context);

		target.setProductSpecification(productSpecificationRef);
	}

	private void setRequiredFields(ProductData source, ProductWsDTO target)
	{
		target.setDescription(source.getDescription());
		target.setIsHomepageProduct(source.isIsHomepageProduct());
		target.setAssociatedProductCode(source.getAssociatedProductCode());
		target.setHomepageProductSequence(source.getHomepageProductSequence());
		target.setPopularFlag(source.isPopularFlag());
		target.setExtensorTag(source.getExtensorTag());
		target.setMostSoldTag(source.getMostSoldTag());
		target.setProductFeatureList(setFeatures(source));
		target.setChannelList(setChannel(source));
		target.setWebPortabilityTag(source.getWebPortabilityTag());
		target.setFfthAddonProductCode(source.getFfthAddonProductCode());
		target.setHfcAddonProductCode(source.getHfcAddonProductCode());
		target.setIncludeDBox(source.getIncludeDBox());
		target.setIncludeExtensor(source.getIncludeExtensor());
		target.setMaxOrderQuantity(source.getMaxOrderQuantity());
		target.setSequenceId(source.getSequenceId());
		target.setColorCode(source.getColorCode());
		target.setMetaDescription(source.getMetaDescription());
		target.setPageTitle(source.getPageTitle());
		target.setDeviceSlug(source.getDeviceSlug());
	}

	private List<ChannelListWsDTO> setChannel(ProductData source)
	{
		List<ChannelListWsDTO> channels = new ArrayList<>();
		if(!CollectionUtils.isEmpty(source.getChannelList()))
		{
			source.getChannelList().forEach(chnl -> {
				ChannelListWsDTO channel = new ChannelListWsDTO();
				channel.setChannelName(chnl.getChannelName());
				channel.setChannelCategory(chnl.getChannelCategory());
				channel.setChannelNumber(chnl.getChannelNumber());
				channel.setChannelImageURL(chnl.getChannelImageURL());
				channel.setChannelType(chnl.getChannelType());
				channel.setImageAltTxt(chnl.getImageAltTxt());
				channels.add(channel);
			});
		}
		return channels;
	}

	private List<LLAProductFeatureWsDTO> setFeatures(ProductData source)
	{
		List<LLAProductFeatureWsDTO> features = new ArrayList<>();
		if(!CollectionUtils.isEmpty(source.getProductFeatureList()))
		{
			source.getProductFeatureList().forEach(f ->{
				LLAProductFeatureWsDTO feature = new LLAProductFeatureWsDTO();
				feature.setCode(f.getCode());
				feature.setFeatureDescription(f.getFeatureDescription());
				feature.setIconName(f.getIconName());
				feature.setFeatureSequence(f.getFeatureSequence());
				features.add(feature);

			});
		}
		return features;
	}

	private void setProductSpecificationValueUseData(ProductData source, ProductWsDTO target, MappingContext context)
	{
		final List<ProdSpecCharValueUseWsDTO> characteristicList = new ArrayList<>();
		if(!CollectionUtils.isEmpty(source.getProductSpecCharValueUses()))
		{
			source.getProductSpecCharValueUses().forEach(characteristic ->
			{
				final ProdSpecCharValueUseWsDTO prodCharacValue = mapperFacade.map(characteristic, ProdSpecCharValueUseWsDTO.class, context);
				final ProductSpecificationWsDTO spec = mapperFacade.map(characteristic.getProductSpecification(), ProductSpecificationWsDTO.class, context);
				if (Objects.nonNull(characteristic.getProductSpecCharacteristicValues())) {
					final List<ProductSpecCharacteristicValueWsDTO> charactersticValues = new ArrayList<>();
					setProductCharactersticValueWsDTO(characteristic.getProductSpecCharacteristicValues(), charactersticValues);
					prodCharacValue.setProductSpecCharacteristicValue(charactersticValues);
				}
				prodCharacValue.setId(characteristic.getId());

				prodCharacValue.setName(characteristic.getName());
				prodCharacValue.setValueType(characteristic.getValueType());
				prodCharacValue.setProductSpecification(spec);
				prodCharacValue.setSequenceId(characteristic.getSequenceId());
				prodCharacValue.setSpecificationType(characteristic.getSpecificationType());
				characteristicList.add(prodCharacValue);
			});
		}
		target.setProductSpecCharValueUses(characteristicList);
	}

	private void setProductCharactersticValueWsDTO(List<TmaProductSpecCharacteristicValueData> valueUse, List<ProductSpecCharacteristicValueWsDTO> value)
	{
		ProductSpecCharacteristicValueWsDTO characValue = new ProductSpecCharacteristicValueWsDTO();
		valueUse.forEach(charactersticValue ->{
			characValue.setUnitOfMeasure(charactersticValue.getUnitOfMeasurment());
			characValue.setValue(charactersticValue.getValue());
			characValue.setId(charactersticValue.getId());
			characValue.setDescription(charactersticValue.getDescription());
			characValue.setCtaSpecDescription(charactersticValue.getCtaSpecDescription());
			value.add(characValue);
		});
	}

	public MapperFacade getMapperFacade()
	{
		return mapperFacade;
	}

	@Required
	public void setMapperFacade(final MapperFacade mapperFacade)
	{
		this.mapperFacade = mapperFacade;
	}

}