/*
 *	Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.formatters;

import java.util.Date;


/**
 * The Date Formatter
 * 
 * @since 2003
 *
 */
public interface TmaWsDateFormatter
{
	Date toDate(String timestamp);

	String toString(Date date);
}
