/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.mappers.price;

import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.dto.ChannelWsDTO;
import com.lla.dto.ProductOfferingPriceWsDTO;
import de.hybris.platform.subscriptionfacades.data.SubscriptionPricePlanData;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;


/**
 * TmaPriceChannelAttributeMapper populates value of channel attribute from {@link SubscriptionPricePlanData} to
 * {@link ProductOfferingPriceWsDTO}
 *
 * @since 1907
 */
class TmaPriceChannelAttributeMapper extends TmaAttributeMapper<SubscriptionPricePlanData, ProductOfferingPriceWsDTO>
{
	private MapperFacade mapperFacade;

	@Override
	public void populateTargetAttributeFromSource(final SubscriptionPricePlanData source, final ProductOfferingPriceWsDTO target,
			final MappingContext context)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (CollectionUtils.isEmpty(source.getDistributionChannels()))
		{
			return;
		}

		final List<ChannelWsDTO> channelList = new ArrayList<>();
		source.getDistributionChannels().forEach(channelData ->
		{
			final ChannelWsDTO channel = getMapperFacade().map(channelData, ChannelWsDTO.class, context);
			channelList.add(channel);
		});

		target.setChannel(channelList);
	}

	public MapperFacade getMapperFacade()
	{
		return mapperFacade;
	}

	@Required
	public void setMapperFacade(final MapperFacade mapperFacade)
	{
		this.mapperFacade = mapperFacade;
	}

}
