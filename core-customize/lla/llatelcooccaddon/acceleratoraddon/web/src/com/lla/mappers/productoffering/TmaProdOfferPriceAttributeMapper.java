/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.mappers.productoffering;

import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;

import com.lla.core.util.LLACommonUtil;
import com.lla.dto.ProductOfferingPriceWsDTO;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.commercewebservicescommons.dto.product.PriceWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.product.ProductWsDTO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.subscriptionfacades.data.OneTimeChargeEntryData;
import de.hybris.platform.subscriptionfacades.data.RecurringChargeEntryData;
import de.hybris.platform.subscriptionfacades.data.SubscriptionPricePlanData;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;


/**
 * TmaProdOfferPriceAttributeMapper populates value of ProductOfferingPrice attribute from {@link ProductData} to
 * {@link ProductWsDTO}
 *
 * @since 1907
 */
public class TmaProdOfferPriceAttributeMapper extends TmaAttributeMapper<ProductData, ProductWsDTO>
{

	public static final String FOREVER_DISCOUNT = "forever";
	public static final String month ="mes";
	public static final String twelve_month ="mensuales x 12 meses";
	private MapperFacade mapperFacade;


	private CommerceCommonI18NService commerceCommonI18NService;
	
	@Autowired
	private LLACommonUtil llaCommonUtil;

	@Override
	public void populateTargetAttributeFromSource(final ProductData source, final ProductWsDTO target,
			final MappingContext context)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (llaCommonUtil.isSiteVTR()  && CollectionUtils.isNotEmpty(source.getDevicePriceList()))
		{
			populateDevicePrice(source, target, context);
			return;
		}
		
		if (CollectionUtils.isEmpty(source.getProductOfferingPrices()))
		{
			return;
		}

		final List<ProductOfferingPriceWsDTO> priceList = new ArrayList<>();
		source.getProductOfferingPrices().forEach(priceData ->
		{
			final ProductOfferingPriceWsDTO price = getMapperFacade().map(priceData, ProductOfferingPriceWsDTO.class, context);
			price.setLifecycleStatus(source.getApprovalStatus());
			priceList.add(price);
		});

		target.setProductOfferingPrice(priceList);
		populateDiscountPrice(source, target, context);
		if (llaCommonUtil.isSitePuertorico()) {
			populatePayNowPrice(source, target, context);
			productReference(source,target,context);
		}
	}

	private void productReference(final ProductData source, final ProductWsDTO target, final MappingContext context)
	{
		if(!CollectionUtils.isEmpty(source.getCompatiblePlans())){
			List<ProductWsDTO> productWsDTOList = new ArrayList<>();
			for(ProductData data : source.getCompatiblePlans()){
				productWsDTOList.add(getMapperFacade().map(data, ProductWsDTO.class, context));
			}
			target.setCompatiblePlans(productWsDTOList);
		}
	}

	private void populateDiscountPrice(ProductData source, ProductWsDTO target, MappingContext context)
	{
		for (SubscriptionPricePlanData price: source.getProductOfferingPrices()) {
			if (null != price && CollectionUtils.isNotEmpty(price.getRecurringChargeEntries())){
				for (RecurringChargeEntryData recurringPrice : price.getRecurringChargeEntries()) {
					if (CollectionUtils.isNotEmpty(recurringPrice.getVtrdiscountedPrice())) {
						List<PriceWsDTO> allDiscountRows = new ArrayList<>();
						recurringPrice.getVtrdiscountedPrice().forEach(discountPrice -> {
							PriceWsDTO discountRow = new PriceWsDTO();
							discountRow.setCurrencyIso(discountPrice.getCurrencyIso());
							discountRow.setFormattedValue(discountPrice.getFormattedValue());
							discountRow.setValue(discountPrice.getValue());
							discountRow.setPlanType(discountPrice.getPlanType());
							discountRow.setDiscountPeriod(discountPrice.getDiscountPeriod());
							discountRow.setDiscountPercentage(discountPrice.getDiscountPercentage());
							//	discountRow.setMonthDuration(StringUtils.equalsIgnoreCase(FOREVER_DISCOUNT,discountPrice.getDiscountPeriod())?month:twelve_month);
							allDiscountRows.add(discountRow);
						});
						target.setDiscountedPrice(allDiscountRows);
					}
				}
			}
		}
	}
	
	
	private void populatePayNowPrice(ProductData source, ProductWsDTO target, MappingContext context)
	{
		for(SubscriptionPricePlanData price: source.getProductOfferingPrices()) {
			if(null != price && CollectionUtils.isNotEmpty(price.getOneTimeChargeEntries())) {
				for (OneTimeChargeEntryData oneTimeCharge : price.getOneTimeChargeEntries())
				{
					List<PriceWsDTO> payNowPrices = new ArrayList<>();
					PriceWsDTO payNowPrice = new PriceWsDTO();
					if(null!=oneTimeCharge.getPrice()) {
					payNowPrice.setCurrencyIso(oneTimeCharge.getPrice().getCurrencyIso());
					payNowPrice.setFormattedValue(oneTimeCharge.getPrice().getFormattedValue());
					payNowPrice.setValue(oneTimeCharge.getPrice().getValue());
					payNowPrice.setPriceType(payNowPrice.getPriceType().BUY);
					payNowPrices.add(payNowPrice);
					  }
					target.setPayNowPrice(payNowPrices);
				 }
			}
		}
	}
	
	
	private void populateDevicePrice(final ProductData source, final ProductWsDTO target, final MappingContext context)
	{
		final List<PriceWsDTO> priceList = new ArrayList<>();
		for (final PriceData price : source.getDevicePriceList())
		{
			if (Objects.nonNull(price))
			{
				final PriceWsDTO devicePriceDto = getMapperFacade().map(price, PriceWsDTO.class, context);
				devicePriceDto.setCurrencyIso(price.getCurrencyIso());
				devicePriceDto.setFormattedValue(price.getFormattedValue());
				devicePriceDto.setValue(price.getValue());
				devicePriceDto.setPlanCode(price.getPlanCode());
				if(Objects.nonNull(price.getMobilePriceType())){
					devicePriceDto.setMobilePriceType(price.getMobilePriceType().getCode());
				}
				priceList.add(devicePriceDto);
			}
		}
		target.setDevicePriceList(priceList);
	}	

	public MapperFacade getMapperFacade()
	{
		return mapperFacade;
	}

	@Required
	public void setMapperFacade(final MapperFacade mapperFacade)
	{
		this.mapperFacade = mapperFacade;
	}

	public CommerceCommonI18NService getCommerceCommonI18NService() {
		return commerceCommonI18NService;
	}

	@Required
	public void setCommerceCommonI18NService(CommerceCommonI18NService commerceCommonI18NService) {
		this.commerceCommonI18NService = commerceCommonI18NService;
	}
}
