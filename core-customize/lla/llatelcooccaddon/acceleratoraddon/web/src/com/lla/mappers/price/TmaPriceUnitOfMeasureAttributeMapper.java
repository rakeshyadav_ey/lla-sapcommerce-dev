/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.mappers.price;

import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.dto.MoneyWsDTO;
import com.lla.dto.ProductOfferingPriceWsDTO;
import de.hybris.platform.subscriptionfacades.data.SubscriptionPricePlanData;

import org.springframework.util.Assert;

import ma.glasnost.orika.MappingContext;


/**
 * TmaPriceUnitOfMeasureAttributeMapper populates value of unitOfMeasure attribute from
 * {@link SubscriptionPricePlanData} to {@link ProductOfferingPriceWsDTO}
 *
 * @since 1907
 */
public class TmaPriceUnitOfMeasureAttributeMapper
		extends TmaAttributeMapper<SubscriptionPricePlanData, ProductOfferingPriceWsDTO>
{

	@Override
	public void populateTargetAttributeFromSource(final SubscriptionPricePlanData source, final ProductOfferingPriceWsDTO target,
			final MappingContext context)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (source.getUnitFactor() == null || source.getUnit() == null)
		{
			return;
		}

		final MoneyWsDTO measurement = new MoneyWsDTO();
		measurement.setCurrencyIso(source.getUnit().getUnitType());
		measurement.setValue(source.getUnitFactor().toString());

		target.setUnitOfMeasure(measurement);
	}

}
