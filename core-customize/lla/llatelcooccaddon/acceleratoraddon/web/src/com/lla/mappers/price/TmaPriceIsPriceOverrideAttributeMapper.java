/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.mappers.price;

import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.dto.ProductOfferingPriceWsDTO;
import de.hybris.platform.subscriptionfacades.data.SubscriptionPricePlanData;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;

import ma.glasnost.orika.MappingContext;


/**
 * TmaPriceIsPriceOverrideAttributeMapper populates value of isPriceOverride attribute from
 * {@link SubscriptionPricePlanData} to {@link ProductOfferingPriceWsDTO}
 *
 * @since 1907
 */
public class TmaPriceIsPriceOverrideAttributeMapper
		extends TmaAttributeMapper<SubscriptionPricePlanData, ProductOfferingPriceWsDTO>
{
	@Override
	public void populateTargetAttributeFromSource(final SubscriptionPricePlanData source, final ProductOfferingPriceWsDTO target,
			final MappingContext context)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		boolean isPriceOverride = false;
		if (source.getAffectedProductOffering() != null)
		{
			isPriceOverride = true;
		}

		if (CollectionUtils.isNotEmpty(source.getRequiredProductOfferings()))
		{
			isPriceOverride = true;
		}

		target.setIsPriceOverride(isPriceOverride);
	}

}
