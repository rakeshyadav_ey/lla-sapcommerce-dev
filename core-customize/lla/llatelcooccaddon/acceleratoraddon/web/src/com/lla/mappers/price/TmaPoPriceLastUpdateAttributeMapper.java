/*
*Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
*/
package com.lla.mappers.price;

import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.dto.ProductOfferingPriceWsDTO;
import de.hybris.platform.subscriptionfacades.data.SubscriptionPricePlanData;

import org.springframework.util.Assert;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for lastUpdate attribute between {@link SubscriptionPricePlanData} and
 * {@link ProductOfferingPriceWsDTO}
 *
 * @since 1907
 */
public class TmaPoPriceLastUpdateAttributeMapper
		extends TmaAttributeMapper<SubscriptionPricePlanData, ProductOfferingPriceWsDTO>
{
	@Override
	public void populateTargetAttributeFromSource(final SubscriptionPricePlanData source, final ProductOfferingPriceWsDTO target,
			final MappingContext context)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (source.getModifiedtime() != null)
		{
			target.setModifiedTime(source.getModifiedtime());
		}
	}
}
