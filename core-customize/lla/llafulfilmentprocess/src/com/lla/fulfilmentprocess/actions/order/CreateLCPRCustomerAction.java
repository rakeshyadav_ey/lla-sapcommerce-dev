package com.lla.fulfilmentprocess.actions.order;

import com.lla.core.enums.SourceEPC;
import com.lla.fulfilmentprocess.util.LLAFulfillmentProcessOrderUtil;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.helper.LlaMulesoftIntegrationHelper;
import com.lla.mulesoft.integration.service.LLAMulesoftCustomerService;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class CreateLCPRCustomerAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
    private static final Logger LOG = Logger.getLogger(CreateLCPRCustomerAction.class);
    @Autowired
    private LLAMulesoftCustomerService llaMulesoftCustomerService;
    @Autowired
    private ConfigurationService configurationService;
    @Autowired
    private LlaMulesoftIntegrationHelper llaMulesoftIntegrationHelper;
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;
    @Autowired
    private LLABusinessProcessRetriesHandler llaBusinessProcessRetriesHandler;
    @Autowired
    private LLAFulfillmentProcessOrderUtil llaFulfillmentProcessOrderUtil;

    @Override
    public Transition executeAction(final OrderProcessModel process) {
        final OrderModel order = process.getOrder();

        if (order == null) {
            LOG.error("Missing the order, exiting the process");
            return Transition.NOK;
        }
       CustomerModel customer=(CustomerModel) order.getUser();
       if(order.getStatus().equals(OrderStatus.MANUAL_CHECK_COMPLETED) ){
            try{
                final String response=llaMulesoftCustomerService.createCustomerInBSS(order,SourceEPC.CSG.getCode());
                LOG.info(String.format("Response Received :: %s",response));
                if(StringUtils.isNotEmpty(response)){
                    llaMulesoftIntegrationHelper.mapBSSAccountToCustomer(response,(CustomerModel)order.getUser(), SourceEPC.CSG.getCode(),order);
                     process.setRetryCount(0);
                    modelService.save(process);
                    return Transition.OK;
                }
            }
            catch(LLAApiException exception)
            {
               if (process.getRetryCount() > llaBusinessProcessRetriesHandler.getMaxRetries()) {
                    LOG.warn(String.format("Customer creation fails for Order ::: %s as connectivity to API got failed %s times",order.getCode(),process.getRetryCount()));
                    return llaBusinessProcessRetriesHandler.handleRetriesFailed(order, process, "Customer Creation Fails !!! Retries attempt exhausted for action");
                }
                LOG.info(String.format("Retry Customer Creation for customer %s in %s minutes" ,customer.getUid(),(llaBusinessProcessRetriesHandler.getRetryDelay(process) / 1000)/60));
                throw llaBusinessProcessRetriesHandler.handleFailedState(process, "Failed to Create Customer in BSS");
            }
       }
      //  LOG.error(String.format("Order :: %s still not manually verified by CS Agent ",order.getCode()));
        return Transition.NOK;
    }

}
