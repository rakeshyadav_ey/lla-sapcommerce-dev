package com.lla.fulfilmentprocess.actions.order;

import com.lla.mulesoft.integration.service.LLAMulesoftPaymentService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.SplitOrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.ResourceAccessException;

public class PostPaymentDetailAction extends AbstractSimpleDecisionAction<SplitOrderProcessModel> {
    private  static final Logger LOG = Logger.getLogger(PostPaymentDetailAction.class);

    @Autowired
    LLAMulesoftPaymentService llaMulesoftPaymentService;

    @Autowired
    private LLABusinessProcessRetriesHandler llaBusinessProcessRetriesHandler;

    @Override
    public Transition executeAction(SplitOrderProcessModel orderProcessModel) throws RetryLaterException, Exception {
        final OrderModel order = orderProcessModel.getOrder();
        String response = null;
        if(order.getPaymentTransactions().isEmpty())
        {
            LOG.info(String.format("Skipping Process Payment API as the total amount is 0$ for order :- %s",order.getCode()));
        }
        else
        {
            LOG.info(String.format("Inside Post Payment Details - pushing to BSS the payment details for order %s ",order.getCode()));
            try {
                response = llaMulesoftPaymentService.processPayment(order, orderProcessModel);
            }
            catch (ResourceAccessException ioexception) {
                if (orderProcessModel.getRetryCount() > llaBusinessProcessRetriesHandler.getMaxRetries()) {
                    LOG.warn(String.format("Post payment fails for Order ::: %s as connectivity to API got failed %s times",order.getCode(),orderProcessModel.getRetryCount()));
                    return llaBusinessProcessRetriesHandler.handleRetriesFailed(order, orderProcessModel, "Post payment Fails !!! Retries attempt exhausted for action");
                }
                LOG.info(String.format("Retry Post payment in : %s minutes" ,(llaBusinessProcessRetriesHandler.getRetryDelay(orderProcessModel) / 1000)/60));
                throw llaBusinessProcessRetriesHandler.handleFailedState(orderProcessModel, "Failed to Post payment in BSS");
            }
            if(StringUtils.isNotEmpty(response))
            {
                LOG.info(String.format("Process Payment API Response :- %s",response));
                orderProcessModel.setRetryCount(0);
                modelService.save(orderProcessModel);
            }else{
               return Transition.NOK;
            }
        }
        return Transition.OK;
    }
}
