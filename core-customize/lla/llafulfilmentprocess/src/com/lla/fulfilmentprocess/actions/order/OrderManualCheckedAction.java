/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.fulfilmentprocess.actions.order;

import com.lla.core.enums.SourceEPC;
import com.lla.core.util.LLACommonUtil;
import com.lla.fulfilmentprocess.util.LLAFulfillmentProcessOrderUtil;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.SplitOrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.task.RetryLaterException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Set;

public class OrderManualCheckedAction extends AbstractSimpleDecisionAction<SplitOrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(OrderManualCheckedAction.class);
	@Autowired
	private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;
	@Autowired
	private LLACommonUtil llaCommonUtil;
	@Autowired
	private LLAFulfillmentProcessOrderUtil llaFulfillmentProcessOrderUtil;

	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}

	@Override
	public AbstractSimpleDecisionAction.Transition executeAction(SplitOrderProcessModel process) throws RetryLaterException, Exception {
		ServicesUtil.validateParameterNotNull(process, "Process cannot be null");

		final OrderModel order = process.getOrder();
		String sourceEPC=StringUtils.EMPTY;
		ServicesUtil.validateParameterNotNull(order, "Order in process cannot be null");
		if(llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())){
			for(AbstractOrderEntryModel entry: order.getEntries()) {
				if(null!=entry.getProductMapper()){
					sourceEPC=entry.getProductMapper().getSourceEPC().getCode();
				}
				final boolean productCat = llaFulfillmentProcessOrderUtil.isProductNotFixedType(entry) || llaFulfillmentProcessOrderUtil.isProductFixedType(entry);
						    
				if (null == entry.getProductMapper() && productCat) {
					LOG.info(String.format("Proceeding to next action as order %s belongs to Mobile/Postpaid or Fixed Category", order.getCode()));
					LOG.info(String.format("Manual Provisioning Required for Order %s ", order.getCode()));
					order.setStatus(OrderStatus.MANUAL_CHECK_COMPLETED);
					modelService.save(order);
					modelService.refresh(order);
					return Transition.OK;
				}
			}
		}

		if(OrderStatus.MANUAL_CHECK_COMPLETED.equals(order.getStatus())){
			AbstractOrderEntryModel entry = order.getEntries().get(0);
			String orderSourceEpc = StringUtils.EMPTY;
			if(StringUtils.isNotEmpty(sourceEPC)){
				orderSourceEpc=sourceEPC;
			}else if (null!=entry.getProductMapper() && SourceEPC.LIBERATE.equals(entry.getProductMapper().getSourceEPC())) {
				orderSourceEpc = SourceEPC.LIBERATE.getCode();
			} else {
				orderSourceEpc = llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())?SourceEPC.CERILLION.getCode():StringUtils.EMPTY;
			}
			Transition transition = StringUtils.isEmpty(orderSourceEpc)? Transition.OK:validateManualReview(order, orderSourceEpc);
			if (transition != null) return transition;
		}else{
			LOG.warn("Incorrect Order Status to Proceed for Provisioning as Manual Review is still pending by CS Agent");
		}
		return Transition.NOK;
	}


	/**
	 * Validate Agent Manual Review
	 * @param order
	 * @param orderSourceEpc
	 * @return
	 */
	private Transition validateManualReview(OrderModel order, String orderSourceEpc) {
		if (llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())) {
			return getNextJamaicaTransitionAction(order, orderSourceEpc);
		} else if(llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())){
			return getNextPanamaTransitionAction(order);
		}
		return Transition.NOK;
	}

	/**
	 * Get Next Transition for Panama
	 * @param order
	 * @return
	 */
	private Transition getNextPanamaTransitionAction(OrderModel order) {
		Transition transition=Transition.NOK;
		if(StringUtils.isNotEmpty(order.getBillStatusArea()) && StringUtils.isNotEmpty(order.getExchangeId_TI())
				&& StringUtils.isNotEmpty(order.getDepttCode()) && StringUtils.isNotEmpty(order.getSiteCode()) &&
				StringUtils.isNotEmpty(order.getExchangeId_TP()) && StringUtils.isNotEmpty(order.getNumberAreaCode_TI())
				&&  StringUtils.isNotEmpty(order.getNumberAreaCode_TP())){
			   if("HFC".equalsIgnoreCase(llaMulesoftIntegrationUtil.getProductType(order)) && StringUtils.isNotEmpty(order.getExchangeId_MO()) && StringUtils.isNotEmpty(order.getNumberAreaCode_MO()))
				   transition=   Transition.OK;
			   if("FTTH".equalsIgnoreCase(llaMulesoftIntegrationUtil.getProductType(order)) && StringUtils.isNotEmpty(order.getExchangeId_DS()) && StringUtils.isNotEmpty(order.getNumberAreaCode_DS()))
				   transition=  Transition.OK;
		}else{
			order.setStatus(OrderStatus.MANUAL_CHECK_PENDING);
			getModelService().save(order);
			transition = Transition.NOK;
		}
		return  transition;
	}

	/**
	 *  Get Next Transition for Jamaica
	 * @param order
	 * @param orderSourceEpc
	 * @return
	 */
	private Transition getNextJamaicaTransitionAction(OrderModel order, String orderSourceEpc) {
		if (SourceEPC.LIBERATE.getCode().equals(orderSourceEpc) &&
				StringUtils.isNotEmpty(order.getDepttCode()) && StringUtils.isNotEmpty(order.getExchangeId())
				&& StringUtils.isNotEmpty(order.getNumberAreaCode()) && StringUtils.isNotEmpty(order.getSiteCode())) {
			return Transition.OK;
		} else if (SourceEPC.CERILLION.getCode().equals(orderSourceEpc) && StringUtils.isNotEmpty(order.getNetworkId())) {
			return Transition.OK;
		} else {
			order.setStatus(OrderStatus.MANUAL_CHECK_PENDING);
			getModelService().save(order);
			return Transition.NOK;
		}
	}
}
