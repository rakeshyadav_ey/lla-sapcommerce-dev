/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.fulfilmentprocess.actions.order;
import com.lla.fulfilmentprocess.service.LLAOrderSplittingService;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.OrderSplittingService;
import de.hybris.platform.ordersplitting.model.SplitOrderProcessModel;
import com.lla.fulfilmentprocess.constants.LlaFulfilmentProcessConstants;
import java.util.List;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;


public class SplitOrderAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(SplitOrderAction.class);
	@Autowired
	private LLAOrderSplittingService llaOrderSplittingService;
	private OrderSplittingService orderSplittingService;
	private BusinessProcessService businessProcessService;

	@Autowired
	private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

	@Override
	public Transition executeAction(final OrderProcessModel process) throws Exception {
		if (LOG.isInfoEnabled()) {
			LOG.info("Process: " + process.getCode() + " in step " + getClass());
		}
		final OrderModel order=process.getOrder();
		final BaseSiteModel siteModel=order.getSite();
		if(llaMulesoftIntegrationUtil.isJamaicaOrder(siteModel)){
			return splitOrderForJamaica(process, order);
		}else if(llaMulesoftIntegrationUtil.isPanamaOrder(siteModel)){

			return splitOrderForPanama(process,order);
		}
		return Transition.OK;
	}

	/**
	 * Split Order For Panama
	 * @param process
	 * @param order
	 * @return
	 */
	private Transition splitOrderForPanama(OrderProcessModel process, OrderModel order) {
		final List<OrderModel> splitOrders = llaOrderSplittingService.splitPanamaOrder(process.getOrder());
		if(CollectionUtils.isNotEmpty(splitOrders)){
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Splitting order into " + splitOrders.size() + " Orders.");
			}
			int index = 0;
			for (final OrderModel item : splitOrders)
			{
				final SplitOrderProcessModel subProcess = getBusinessProcessService().<SplitOrderProcessModel> createProcess(
						process.getCode() + "_" + (++index), LlaFulfilmentProcessConstants.PANAMA_SPLIT_ORDER_SUBPROCESS_NAME);
				subProcess.setParentProcess(process);
				subProcess.setOrder(item);
				save(subProcess);
				getBusinessProcessService().startProcess(subProcess);
			}
			setOrderStatus(process.getOrder(), OrderStatus.ORDER_SPLIT);
			return Transition.OK;
		}
		return Transition.NOK;
	}

	/**
	 * Jamaica Split Order based on Source EPC
	 * @param process
	 * @param order
	 * @return
	 */
	private Transition splitOrderForJamaica(OrderProcessModel process, OrderModel order) {
		if(validateOrderEntriesHasSourceEPC(order)){
			final List<OrderModel> splitOrders = llaOrderSplittingService.splitOrderBySourceEPC(process.getOrder());

			if (LOG.isDebugEnabled())
			{
				LOG.debug("Splitting order into " + splitOrders.size() + " Orders.");
			}

			int index = 0;
			for (final OrderModel item : splitOrders)
			{
				final SplitOrderProcessModel subProcess = getBusinessProcessService().<SplitOrderProcessModel> createProcess(

						process.getCode() + "_" + (++index), LlaFulfilmentProcessConstants.SPLIT_ORDER_SUBPROCESS_NAME);

				subProcess.setParentProcess(process);
				subProcess.setOrder(item);
				save(subProcess);

				getBusinessProcessService().startProcess(subProcess);

			}
			setOrderStatus(process.getOrder(), OrderStatus.ORDER_SPLIT);
			return Transition.OK;
		}
		LOG.info(String.format("Order %s entries is not mapped with Source BSS codes", order.getCode()));
		return Transition.NOK;
	}

	/**
	 *  Validate Order Entries has Source EPC mapped or Not
	 * @param order
	 * @return
	 */
	private boolean validateOrderEntriesHasSourceEPC(OrderModel order) {
		for(AbstractOrderEntryModel item:order.getEntries()){
			if(item.getProductMapper()==null){
				return false;
			}
		}
		return true;
	}


	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}
}
