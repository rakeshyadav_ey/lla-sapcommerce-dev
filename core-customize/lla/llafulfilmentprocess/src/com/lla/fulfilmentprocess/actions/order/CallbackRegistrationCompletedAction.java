/**
 *
 */
package com.lla.fulfilmentprocess.actions.order;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.lla.fulfilmentprocess.util.LLAFulfillmentProcessOrderUtil;


/**
 * @author LU836YG
 *
 */
public class CallbackRegistrationCompletedAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(OrderManualReviewCompletedAction.class);
	@Autowired
	private ConfigurationService configurationService;

	@Autowired
	private LLAFulfillmentProcessOrderUtil llaFulfillmentProcessOrderUtil;

	@Autowired
	private LLABusinessProcessRetriesHandler llaBusinessProcessRetriesHandler;

	@Override
	public Transition executeAction(final OrderProcessModel process)
	{
		final OrderModel order = process.getOrder();
		return proceedToNextTransition(process, order);
	}

	/**
	 * @param process
	 * @param order
	 * @return
	 */
	private Transition proceedToNextTransition(final OrderProcessModel process, final OrderModel order)
	{
		final CustomerModel customer = (CustomerModel) (order.getOwner());

		if (customer != null && CollectionUtils.isNotEmpty(customer.getBillingAccounts())
				&& CollectionUtils.isNotEmpty(customer.getServiceAccounts()))
		{
			process.setState(ProcessState.RUNNING);
			process.setRetryCount(0);
			modelService.save(process);
			return AbstractSimpleDecisionAction.Transition.OK;
		}
		else
		{
			if (process.getRetryCount() > llaBusinessProcessRetriesHandler.getMaxRetries())
			{
				LOG.warn(String.format(
						"Customer is not registered and still the Order Status is Callback Registration Pending, even after %s retries attempt",
						order.getCode(), process.getRetryCount()));
				return llaBusinessProcessRetriesHandler.handleRetriesFailed(order, process, "Retries attempt exhausted for action");
			}
			LOG.warn(String.format("Process will be retried after %s minutes",
					(llaBusinessProcessRetriesHandler.getRetryDelay(process) / 1000) / 60));
			throw llaBusinessProcessRetriesHandler.handleFailedState(process, "Customer is still not registered");
		}
	}
}
