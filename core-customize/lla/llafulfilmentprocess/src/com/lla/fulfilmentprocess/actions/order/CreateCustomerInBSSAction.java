/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.fulfilmentprocess.actions.order;

import com.lla.core.enums.SourceEPC;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.helper.LlaMulesoftIntegrationHelper;
import com.lla.mulesoft.integration.service.LLAMulesoftCustomerService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.SplitOrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * This example action checks the order for required data in the business process. Skipping this action may result in
 * failure in one of the subsequent steps of the process. The relation between the order and the business process is
 * defined in basecommerce extension through item OrderProcess. Therefore if your business process has to access the
 * order (a typical case), it is recommended to use the OrderProcess as a parentClass instead of the plain
 * BusinessProcess.
 */
public class CreateCustomerInBSSAction extends AbstractSimpleDecisionAction<SplitOrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(CreateCustomerInBSSAction.class);
    @Autowired
	private LLAMulesoftCustomerService llaMulesoftCustomerService;
	@Autowired
	private ConfigurationService configurationService;
	@Autowired
	private LlaMulesoftIntegrationHelper llaMulesoftIntegrationHelper;

	@Autowired
	private LLABusinessProcessRetriesHandler llaBusinessProcessRetriesHandler;


	@Override
	public Transition executeAction(final SplitOrderProcessModel process)
	{
		final OrderModel order = process.getOrder();

		if (order == null)
		{
			LOG.error("Missing the order, exiting the process");
			return Transition.NOK;
		}
		String sourceEpc= StringUtils.EMPTY;
		for(AbstractOrderEntryModel entry: order.getEntries()){
			if(null == entry.getProductMapper()){
				LOG.error(String.format("Not proceeding as order %s does not have source EPC configured",order.getCode()));
				return  Transition.NOK;
			}else{
				sourceEpc=entry.getProductMapper().getSourceEPC().getCode();
				break;
			}
		}
		CustomerModel customer=(CustomerModel) order.getUser();
		if(null!=order.getBillingAccount() && null!=order.getBillingAccount().getBillingSystemId() && order.getBillingAccount().getBillingSystemId().equalsIgnoreCase(sourceEpc) &&
				sourceEpc.equalsIgnoreCase(SourceEPC.CERILLION.getCode()) && StringUtils.isEmpty(customer.getCerillionCustomerNo()))
		{
			LOG.warn(String.format("Please associate %s CustomerNo. Before proceeding ahead",sourceEpc));
			return Transition.NOK;
		}
		if(null!=order.getBillingAccount() && null!=order.getBillingAccount().getBillingSystemId() && order.getBillingAccount().getBillingSystemId().equalsIgnoreCase(sourceEpc) &&
				sourceEpc.equalsIgnoreCase(SourceEPC.LIBERATE.getCode()) && StringUtils.isEmpty(customer.getLiberateCustomerNo()))
		{
			LOG.warn(String.format("Please associate %s CustomerNo. Before proceeding ahead",sourceEpc));
			return Transition.NOK;
		}
		if(order.getStatus().equals(OrderStatus.MANUAL_CHECK_COMPLETED) && null==order.getBillingAccount()){
		if(StringUtils.isNotEmpty(sourceEpc)){
			try{
				final String response=llaMulesoftCustomerService.createCustomerInBSS(order,sourceEpc);
				LOG.info(String.format("Response Received :: %s",response));
				if(StringUtils.isNotEmpty(response)){
					llaMulesoftIntegrationHelper.mapBSSAccountToCustomer(response,(CustomerModel)order.getUser(), sourceEpc,order);
					process.setRetryCount(0);
					modelService.save(process);
					return Transition.OK;
				}
			}
			 catch(LLAApiException exception)
			 {
				if (process.getRetryCount() > llaBusinessProcessRetriesHandler.getMaxRetries()) {
					LOG.warn(String.format("Customer creation fails for Order ::: %s as connectivity to API got failed %s times",order.getCode(),process.getRetryCount()));
					return llaBusinessProcessRetriesHandler.handleRetriesFailed(order, process, "Customer Creation Fails !!! Retries attempt exhausted for action");
				}
				LOG.info(String.format("Retry Customer Creation for customer %s in %s minutes" ,customer.getUid(),(llaBusinessProcessRetriesHandler.getRetryDelay(process) / 1000)/60));
				throw llaBusinessProcessRetriesHandler.handleFailedState(process, "Failed to Create Customer in BSS");
			}

		}else{
			LOG.error(String.format("No Source EPC mapped at Order Entry Line Items for Order :::%s",order.getCode()));
			return Transition.NOK;
		}
		}else{
			LOG.info(String.format("Order %s is mapped with customer existing billing account %s for Create Customer API ",order.getCode(),order.getBillingAccount().getBillingAccountId()));
			llaMulesoftIntegrationHelper.mapOrderBillingAccountToCustomer(order, customer);
			LOG.info("Proceeding to next action");
			return  Transition.OK;
		}
		return Transition.NOK;
	}
}
