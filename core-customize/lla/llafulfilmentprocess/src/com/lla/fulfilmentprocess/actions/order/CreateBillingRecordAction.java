package com.lla.fulfilmentprocess.actions.order;

import com.lla.core.enums.SourceEPC;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.service.LLAMulesoftBillingAccountService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.SplitOrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class CreateBillingRecordAction extends AbstractSimpleDecisionAction<SplitOrderProcessModel>
{
    private static final Logger LOG = Logger.getLogger(CreateBillingRecordAction.class);

    @Autowired
    LLAMulesoftBillingAccountService llaMulesoftBillingAccountService;
    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private LLABusinessProcessRetriesHandler llaBusinessProcessRetriesHandler;



    @Override
    public Transition executeAction(SplitOrderProcessModel orderProcessModel)
    {
        final OrderModel order = orderProcessModel.getOrder();
        AbstractOrderEntryModel entry = order.getEntries().get(0);
        if(null == entry.getProductMapper()){
            return Transition.NOK;
        }else if(SourceEPC.LIBERATE.equals(entry.getProductMapper().getSourceEPC())){
            LOG.warn(String.format("Order Entry of order  %s belongs to Liberate Source EPC, Proceeding to Next Action ..",order.getCode()));
            return Transition.OK;
        }else if(SourceEPC.CERILLION.equals(entry.getProductMapper().getSourceEPC()) ){
            if(order.getBillingAccount()==null) {
                try {
                    String billingAccount = llaMulesoftBillingAccountService.createBillingAccount(order);
                    if (StringUtils.isNotEmpty(billingAccount)) {
                        CustomerModel customer = (CustomerModel) order.getUser();
                        List<String> cerellionAccounts = new ArrayList<>();
                        if (CollectionUtils.isNotEmpty(customer.getCerillionAccounts())) {
                            cerellionAccounts = new ArrayList<>(customer.getCerillionAccounts());
                        }
                        cerellionAccounts.remove(billingAccount);
                        cerellionAccounts.add(billingAccount);
                        customer.setCerillionAccounts(cerellionAccounts);
                        orderProcessModel.setRetryCount(0);
                        modelService.save(orderProcessModel);
                        modelService.save(customer);
                        modelService.refresh(customer);
                        LOG.info(String.format("Added Billing Account %s into %s", billingAccount, customer.getUid()));
                    }
                } catch (LLAApiException exception) {
                    if (orderProcessModel.getRetryCount() > llaBusinessProcessRetriesHandler.getMaxRetries()) {
                        LOG.warn(String.format("Create Billing Records fails for Customer ::: %s and  Order ::: %s as connectivity to API got failed %s times", order.getUser().getUid(), order.getCode(), orderProcessModel.getRetryCount()));
                        return llaBusinessProcessRetriesHandler.handleRetriesFailed(order, orderProcessModel, "Customer Billing Record Creation Fails !!! Retries attempt exhausted for action");
                    }
                    LOG.info(String.format("Retry Customer Billing Record Creation for customer %s in  %s minutes", order.getUser().getUid(), (llaBusinessProcessRetriesHandler.getRetryDelay(orderProcessModel) / 1000) / 60));
                    throw llaBusinessProcessRetriesHandler.handleFailedState(orderProcessModel, "Failed to Create Billing Record for Cerillion in BSS");
                }
            } else{
                LOG.info(String.format("Order %s is mapped with customer existing billing account %s for Create billing Account API ",order.getCode(),order.getBillingAccount().getBillingAccountId()));
                LOG.info("Proceeding to next action");
                return  Transition.OK;
            }
         }
        return Transition.OK;
    }
}
