package com.lla.fulfilmentprocess.actions.order;

import com.lla.core.enums.SourceEPC;
import com.lla.fulfilmentprocess.util.LLAFulfillmentProcessOrderUtil;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.helper.LlaMulesoftIntegrationHelper;
import com.lla.mulesoft.integration.service.LLAMulesoftCustomerService;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.SplitOrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class CreatePanamaCustomerInBSS extends AbstractSimpleDecisionAction<SplitOrderProcessModel>
{
    private static final Logger LOG = Logger.getLogger(CreatePanamaCustomerInBSS.class);
    @Autowired
    private LLAMulesoftCustomerService llaMulesoftCustomerService;
    @Autowired
    private ConfigurationService configurationService;
    @Autowired
    private LlaMulesoftIntegrationHelper llaMulesoftIntegrationHelper;
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;
    @Autowired
    private LLABusinessProcessRetriesHandler llaBusinessProcessRetriesHandler;
    @Autowired
    private LLAFulfillmentProcessOrderUtil llaFulfillmentProcessOrderUtil;

    @Override
    public Transition executeAction(final SplitOrderProcessModel process) {
        final OrderModel order = process.getOrder();

        if (order == null) {
            LOG.error("Missing the order, exiting the process");
            return Transition.NOK;
        }
        String sourceEpc = StringUtils.EMPTY;
        for(AbstractOrderEntryModel entry: order.getEntries()){
            if(null!=entry.getProductMapper()){
                sourceEpc=entry.getProductMapper().getSourceEPC().getCode();
                break;
            }
        }
        for (AbstractOrderEntryModel entry : order.getEntries()) {
            final boolean productCat = llaFulfillmentProcessOrderUtil.isProductNotFixedType(entry)|| llaFulfillmentProcessOrderUtil.isProductFixedType(entry);
            if (StringUtils.isEmpty(sourceEpc) && !productCat) {
                LOG.error(String.format("Not proceeding as order %s does not have source EPC configured", order.getCode()));
                return Transition.NOK;
            } else if (null == entry.getProductMapper() && productCat) {
                LOG.info(String.format("Proceeding to next action as order %s belongs to Mobile/Postpaid or Fixed  Category", order.getCode()));
                LOG.info(String.format("Manual Provisioning Required for Order %s ", order.getCode()));
                return Transition.OK;
            }
       }
       CustomerModel customer=(CustomerModel) order.getUser();
       if(null!=order.getBillingAccount() && null!=order.getBillingAccount().getBillingSystemId() && order.getBillingAccount().getBillingSystemId().equalsIgnoreCase(sourceEpc) &&
                sourceEpc.equalsIgnoreCase(SourceEPC.LIBERATE.getCode()) && StringUtils.isEmpty(customer.getLiberateCustomerNo()))
        {
            LOG.warn(String.format("Please associate %s CustomerNo. Before proceeding ahead",sourceEpc));
            return Transition.NOK;
        }
        if(order.getStatus().equals(OrderStatus.MANUAL_CHECK_COMPLETED) && null==order.getBillingAccount()){
            if(StringUtils.isNotEmpty(sourceEpc)){
                try{
                    final String response=llaMulesoftCustomerService.createCustomerInBSS(order,sourceEpc);
                    LOG.info(String.format("Response Received :: %s",response));
                    if(StringUtils.isNotEmpty(response)){
                        llaMulesoftIntegrationHelper.mapBSSAccountToCustomer(response,(CustomerModel)order.getUser(), sourceEpc,order);
                        process.setRetryCount(0);
                        modelService.save(process);
                        return Transition.OK;
                    }
                }
                catch(LLAApiException exception)
                {
                    if (process.getRetryCount() > llaBusinessProcessRetriesHandler.getMaxRetries()) {
                        LOG.warn(String.format("Customer creation fails for Order ::: %s as connectivity to API got failed %s times",order.getCode(),process.getRetryCount()));
                        return llaBusinessProcessRetriesHandler.handleRetriesFailed(order, process, "Customer Creation Fails !!! Retries attempt exhausted for action");
                    }
                    LOG.info(String.format("Retry Customer Creation for customer %s in %s minutes" ,customer.getUid(),(llaBusinessProcessRetriesHandler.getRetryDelay(process) / 1000)/60));
                    throw llaBusinessProcessRetriesHandler.handleFailedState(process, "Failed to Create Customer in BSS");
                }
            }else{
                LOG.error(String.format("No Source EPC mapped at Order Entry Line Items for Order :::%s",order.getCode()));
                return Transition.NOK;
            }
        }else{
            LOG.info(String.format("Order %s is mapped with customer existing billing account %s for Create Customer API ",order.getCode(),order.getBillingAccount().getBillingAccountId()));
            llaMulesoftIntegrationHelper.mapOrderBillingAccountToCustomer(order, customer);
            LOG.info("Proceeding to next action");
            return  Transition.OK;
        }
        return Transition.NOK;
    }
}
