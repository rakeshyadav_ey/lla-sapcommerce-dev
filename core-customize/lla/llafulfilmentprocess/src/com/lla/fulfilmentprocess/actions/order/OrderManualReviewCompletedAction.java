/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.fulfilmentprocess.actions.order;
import com.lla.fulfilmentprocess.util.LLAFulfillmentProcessOrderUtil;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.SplitOrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 */
public class OrderManualReviewCompletedAction extends AbstractSimpleDecisionAction<SplitOrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(OrderManualReviewCompletedAction.class);
	@Autowired
	private ConfigurationService configurationService;

	@Autowired
	private LLAFulfillmentProcessOrderUtil llaFulfillmentProcessOrderUtil;

	@Override
	public Transition executeAction(final SplitOrderProcessModel process) {
		final OrderModel order = process.getOrder();
		return llaFulfillmentProcessOrderUtil.proceedToNextTransition(process,order);
	}


}
