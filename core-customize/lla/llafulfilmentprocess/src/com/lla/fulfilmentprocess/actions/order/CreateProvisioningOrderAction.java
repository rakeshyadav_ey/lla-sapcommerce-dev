package com.lla.fulfilmentprocess.actions.order;

import com.lla.fulfilmentprocess.util.LLAFulfillmentProcessOrderUtil;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import com.lla.mulesoft.integration.service.LLAMulesoftOrderingService;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.SplitOrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

public class CreateProvisioningOrderAction extends AbstractSimpleDecisionAction<SplitOrderProcessModel> {

    @Autowired
    LLAMulesoftOrderingService llaMulesoftOrderingService;
    private LLAMulesoftNotificationService llaMulesoftNotificationService;
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;
    @Autowired
    private LLABusinessProcessRetriesHandler llaBusinessProcessRetriesHandler;
    @Autowired
    private LLAFulfillmentProcessOrderUtil llaFulfillmentProcessOrderUtil;

    private static final Logger LOG = Logger.getLogger(CreateProvisioningOrderAction.class);
    @Override
    public Transition executeAction(SplitOrderProcessModel orderProcessModel){
        LOG.info("Inside the Provisioning Order");
        final OrderModel order = orderProcessModel.getOrder();
        String sourceEpc= StringUtils.EMPTY;
        for(AbstractOrderEntryModel entry: order.getEntries()){
            if(null!=entry.getProductMapper()){
                sourceEpc=entry.getProductMapper().getSourceEPC().getCode();
                break;
            }
        }
        if(llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())){
            for(AbstractOrderEntryModel entry: order.getEntries()){
               final boolean productCat=llaFulfillmentProcessOrderUtil.isProductNotFixedType(entry) || llaFulfillmentProcessOrderUtil.isProductFixedType(entry) ;
                if(StringUtils.isEmpty(sourceEpc) && !productCat){
                    LOG.error(String.format("Not proceeding as order %s does not have source EPC configured",order.getCode()));
                    return  Transition.NOK;
                }else if(null == entry.getProductMapper() && productCat){

                    LOG.info(String.format("Proceeding to next action as order %s belongs to Mobile/Postpaid or Fixed Category",order.getCode()));
                    LOG.info(String.format("Manual Provisioning Required for Order %s ",order.getCode()));
                    return  Transition.OK;
                }
            }
        }else if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())){
            for(AbstractOrderEntryModel entry: order.getEntries()){
                if(null == entry.getProductMapper()){
                    LOG.error(String.format("Not proceeding as order %s does not have source EPC configured",order.getCode()));
                    return  Transition.NOK;
                } else
                    sourceEpc=entry.getProductMapper().getSourceEPC().getCode();
                break;
             }
        }else if(llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())) {
     	   for(AbstractOrderEntryModel entry:  order.getEntries()) {
   	   	if(null == entry.getProductMapper()) {
   	   		LOG.error(String.format("Not proceeding as order %s does not have source EPC configured",order.getCode()));
   	   	} else 
					sourceEpc=entry.getProductMapper().getSourceEPC().getCode();
   	   	break;
   	   }
     }

      try{
        String orderID = llaMulesoftOrderingService.placeOrder(order,sourceEpc);
        if(StringUtils.isNotEmpty(orderID)) {
            order.setBSSOrderId(orderID);
            order.setStatus(OrderStatus.COMPLETED);
            orderProcessModel.setRetryCount(0);
            modelService.save(orderProcessModel);
            modelService.save(order);
            modelService.refresh(order);
            LOG.info("Order Provisioned successfully");
            if(!llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())) {
            LOG.info(String.format("Response to send approval Email",llaMulesoftNotificationService.sendOrderProvisionMail(order)));
            }
            return Transition.OK;
        }
        }catch (LLAApiException exception){
            if (orderProcessModel.getRetryCount() > llaBusinessProcessRetriesHandler.getMaxRetries()) {
                LOG.warn(String.format("Order Provisioning fails for Customer ::: %s and  Order ::: %s as connectivity to API got failed %s times",order.getUser().getUid(),order.getCode(),orderProcessModel.getRetryCount()));
                return llaBusinessProcessRetriesHandler.handleRetriesFailed(order, orderProcessModel, "Order Provisioning Fails  !!! Retries attempt exhausted for action");
            }
            LOG.info(String.format("Retry Order Provisioning for Order :%s in   %s minutes" ,order.getCode(),(llaBusinessProcessRetriesHandler.getRetryDelay(orderProcessModel) / 1000)/60));
            throw llaBusinessProcessRetriesHandler.handleFailedState(orderProcessModel, "Failed to Provision Order in BSS");

        }
        LOG.info("Unable to provision order");
        return Transition.NOK;
    }

    public LLAMulesoftNotificationService getLlaMulesoftNotificationService() {
        return llaMulesoftNotificationService;
    }

    @Required
    public void setLlaMulesoftNotificationService(LLAMulesoftNotificationService llaMulesoftNotificationService) {
        this.llaMulesoftNotificationService = llaMulesoftNotificationService;
    }


}
