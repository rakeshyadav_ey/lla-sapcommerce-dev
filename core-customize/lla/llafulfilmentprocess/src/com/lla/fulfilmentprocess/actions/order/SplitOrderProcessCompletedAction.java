/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.fulfilmentprocess.actions.order;

import de.hybris.platform.core.enums.DeliveryStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.ordersplitting.model.SplitOrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.enums.ProcessState;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.Optional;


/**
 *
 */
public class SplitOrderProcessCompletedAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(SplitOrderProcessCompletedAction.class);

	private static final String PROCESS_MSG = "Process: ";

	@Override
	public Transition executeAction(final OrderProcessModel process)
	{
		LOG.info(PROCESS_MSG + process.getCode() + " in step " + getClass());
		LOG.info(PROCESS_MSG + process.getCode() + " is checking for  " + process.getSplitOrderProcesses().size()
				+ " subprocess results");

		final OrderModel order = process.getOrder();
		final Collection<SplitOrderProcessModel> splitProcesses = process.getSplitOrderProcesses();

		if (CollectionUtils.isNotEmpty(splitProcesses))
		{
			final Optional<SplitOrderProcessModel> atleastOneConsProcessNotDone = splitProcesses.stream()
					.filter(consProcess -> !consProcess.isDone())
					.findFirst();
			final boolean allConsProcessNotDone = splitProcesses.stream().allMatch(consProcess -> !consProcess.isDone());

			if (allConsProcessNotDone)
			{
				LOG.info(PROCESS_MSG + process.getCode() + " found all subprocesses incomplete");
			    save(order);
				return Transition.NOK;
			}
			else if (atleastOneConsProcessNotDone.isPresent())
			{
				LOG.info(PROCESS_MSG + process.getCode() + " found subprocess " + atleastOneConsProcessNotDone.get().getCode()
						+ " incomplete -> wait again!");
			    save(order);
				return Transition.NOK;
			}
		}

		LOG.info(PROCESS_MSG + process.getCode() + " found all subprocesses complete");
		process.setState(ProcessState.SUCCEEDED);
		save(process);
		order.setStatus(OrderStatus.COMPLETED);
		save(order);
		return Transition.OK;
	}
}
