/**
 *
 */
package com.lla.fulfilmentprocess.actions.order;

import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.task.RetryLaterException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;


/**
 * @author HP737SW
 *
 */
public class VTRSendCsaOrderNotification extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(VTRSendCsaOrderNotification.class);

	@Autowired
	private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

	@Autowired
	private FlexibleSearchService flexibleSearchService;
	@Autowired
	private EmailService emailService;
	@Autowired
	private CatalogVersionService catalogVersionService;

	@Autowired
	private BaseSiteService baseSiteService;
	@Autowired
	private UserService userService;
	@Autowired
	private LLAMulesoftNotificationService llaMulesoftNotificationService;

	@Override
	public Transition executeAction(final OrderProcessModel orderProcessModel) throws RetryLaterException, Exception
	{
		final OrderModel orderModel = orderProcessModel.getOrder();
		baseSiteService.setCurrentBaseSite(orderModel.getSite(), false);
		userService.setCurrentUser(orderModel.getUser());
		catalogVersionService.setSessionCatalogVersion(orderModel.getSite().getUid() + "ProductCatalog", "Online");
		catalogVersionService.setSessionCatalogVersion(orderModel.getSite().getUid() + "ProductCatalog", "Staged");
		List<EmployeeModel> csaAgents = new ArrayList<>();
		csaAgents = llaMulesoftIntegrationUtil.getEmployeesInUserGroup("customersupportagentgroup").stream()
				.filter(employee -> employee.getGroups().contains(userService.getUserGroupForUID("chile")))
				.collect(Collectors.toList());

		final List<String> emails = llaMulesoftIntegrationUtil.getEmailAddressesOfEmployees(csaAgents);
		LOG.info(String.format("Triggering CSA Agent Order Notification  %s", orderModel.getCode()));
		final String response = llaMulesoftNotificationService.triggerCSA_Notification(orderModel, emails, StringUtils.EMPTY);
		Transition returnTransition = null;
		if (StringUtils.isNotEmpty(response))
		{
			orderModel.setStatus(OrderStatus.COMPLETED);
			modelService.save(orderModel);
			modelService.refresh(orderModel);
			LOG.info(String.format("CSA Agent Order Notification sent successfully %s", orderModel.getCode()));
			returnTransition = Transition.OK;
		}
		else
		{
			returnTransition = Transition.NOK;
		}
		return returnTransition;
	}
}
