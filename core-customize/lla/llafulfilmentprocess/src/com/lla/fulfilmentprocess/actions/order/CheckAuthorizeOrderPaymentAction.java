/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.fulfilmentprocess.actions.order;

import com.lla.core.util.LLACommonUtil;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.*;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * This action implements payment authorization using {@link CreditCardPaymentInfoModel}. Any other payment model could
 * be implemented here, or in a separate action, if the process flow differs.
 */
public class CheckAuthorizeOrderPaymentAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	@Autowired
	private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

	@Autowired
	private LLACommonUtil llaCommonUtil;

	@Autowired
	private CatalogVersionService catalogVersionService;


	@Override
	public Transition executeAction(final OrderProcessModel process)
	{
		final OrderModel order = process.getOrder();
		final BaseSiteModel siteModel=order.getSite();
		if (order != null)
		{
			PaymentInfoModel paymentInfoModel=order.getPaymentInfo();
			if(llaMulesoftIntegrationUtil.isPanamaOrder(siteModel)){
				catalogVersionService.setSessionCatalogVersion(order.getSite().getUid()+"ProductCatalog","Online");
				catalogVersionService.setSessionCatalogVersion(order.getSite().getUid()+"ProductCatalog","Staged");

				if(paymentInfoModel == null && !llaCommonUtil.hidePaymentMethod(order)){
					return Transition.OK;
				}else if(paymentInfoModel!=null && (paymentInfoModel instanceof PayInStorePaymentInfoModel || paymentInfoModel instanceof CashOnDeliveryPaymentInfoModel || paymentInfoModel instanceof OnlinePaymentInfoModel)){
					return Transition.OK;
				}else{
					return  Transition.NOK;
				}
			}
			else
			{
				return assignStatusForOrder(order);
			}
		}
		return Transition.NOK;
	}

	/**
	 * Sets the status for given order in case on of its {@link PaymentTransactionEntryModel} matches proper
	 * {@link PaymentTransactionType} and {@link TransactionStatus}.
	 * @param order {@link OrderModel}
	 * @return {@link Transition}
	 */
	protected Transition assignStatusForOrder(final OrderModel order) {
		if(order.getTotalPrice().doubleValue()==0.0){

			order.setStatus(OrderStatus.PAYMENT_AUTHORIZED);
			modelService.save(order);
			return Transition.OK;
		}
		for (final PaymentTransactionModel transaction : order.getPaymentTransactions())
		{
			for (final PaymentTransactionEntryModel entry : transaction.getEntries())
			{
				if (entry.getType().equals(PaymentTransactionType.AUTHORIZATION)
						&& TransactionStatus.ACCEPTED.name().equals(entry.getTransactionStatus()))
				{
					order.setStatus(OrderStatus.PAYMENT_AUTHORIZED);
					modelService.save(order);
					return Transition.OK;
				}
			}
		}
		return Transition.NOK;
	}
}
