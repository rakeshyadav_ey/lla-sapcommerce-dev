package com.lla.fulfilmentprocess.actions.order;

import com.lla.fulfilmentprocess.constants.LlaFulfilmentProcessConstants;
import com.lla.mulesoft.integration.exception.LLAApiException;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.jalo.JaloConnection;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.SplitOrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.task.RetryLaterException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.llapayment.service.LLAPaymentCaptureService;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * The TakePayment step captures the payment transaction.
 */
public class TakePaymentAction extends AbstractSimpleDecisionAction<SplitOrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(TakePaymentAction.class);
	@Autowired
	private LLAPaymentCaptureService llaPaymentCaptureService;

	@Autowired
	private CatalogVersionService catalogVersionService;

    @Autowired
	private BaseSiteService baseSiteService;
    
    @Autowired
	private ConfigurationService configurationService;

    @Autowired
	private UserService userService;

	@Autowired
	private LLABusinessProcessRetriesHandler llaBusinessProcessRetriesHandler;

	@Override
	public Transition executeAction(final SplitOrderProcessModel process)
	{
		OrderModel order = process.getOrder();
		if(order.getTotalPrice().doubleValue()==0.0 || CollectionUtils.isEmpty(order.getPaymentTransactions())){
			LOG.info(String.format("No Payment Capture required for Order ::: %s  as Order total is  0 ",order.getCode()));
		    return Transition.OK;
		}

		if(null==order.getSite()){
			LOG.info(String.format("No Site Mapped at Order :%s",order.getCode()));
			return Transition.NOK;
	    }

		baseSiteService.setCurrentBaseSite(order.getSite(),false);
		userService.setCurrentUser(order.getUser());
		catalogVersionService.setSessionCatalogVersion(order.getSite().getUid()+"ProductCatalog","Online");
		catalogVersionService.setSessionCatalogVersion(order.getSite().getUid()+"ProductCatalog","Staged");
		try {

			if(llaPaymentCaptureService.capturePayment(order)) {
				order.setStatus(OrderStatus.PAYMENT_CAPTURED);
				process.setRetryCount(0);
				modelService.save(process);
				modelService.save(order);
				modelService.refresh(order);
				return Transition.OK;
			}else{
				LOG.info(String.format("Payment capture unsuccessful for Order : %s", order.getCode()));
				order.setStatus(OrderStatus.PAYMENT_NOT_CAPTURED);
				modelService.save(order);
				modelService.refresh(order);
				return  Transition.NOK;
			}
		}catch (LLAApiException lla){
			if (process.getRetryCount() > llaBusinessProcessRetriesHandler.getMaxRetries()) {
				LOG.warn(String.format("Payment Capture Call for ::: %s as connectivity to API got failed %s times",order.getCode(),process.getRetryCount()));
				LOG.error("Capturing payment failed for order due to connectivity issue ::: "+order.getCode(),lla);
				order.setStatus(OrderStatus.PAYMENT_NOT_CAPTURED);
				modelService.save(order);
				modelService.refresh(order);
				return llaBusinessProcessRetriesHandler.handleRetriesFailed(order, process, "Payment Capture Fails !!! Retries attempt exhausted for action");
			}
			LOG.info(String.format("Retry Payment Capture Call for Order %s in %s minutes" ,order.getCode(),(llaBusinessProcessRetriesHandler.getRetryDelay(process) / 1000)/60));
			throw llaBusinessProcessRetriesHandler.handleFailedState(process, "Failed to Create FAC Payment Capture Call ");
		} catch (Exception exception){
			LOG.error("Error in Capturing Payment for Order ::: "+order.getCode(),exception);
			order.setStatus(OrderStatus.PAYMENT_NOT_CAPTURED);
			modelService.save(order);
			modelService.refresh(order);
			return Transition.NOK;
		}
	}
}
