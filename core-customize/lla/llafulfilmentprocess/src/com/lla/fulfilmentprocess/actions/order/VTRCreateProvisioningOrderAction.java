/**
 *
 */
package com.lla.fulfilmentprocess.actions.order;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.task.RetryLaterException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.lla.core.enums.SourceEPC;
import com.lla.mulesoft.integration.service.LLAMulesoftOrderingService;


/**
 * @author HP737SW
 *
 */
public class VTRCreateProvisioningOrderAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(VTRCreateProvisioningOrderAction.class);
	private static final String VTR_ORDER_PROVISIONING_ENABLED="llamulesoftintegration.vtr.order.provisioning.enabled";


	@Autowired
	private LLAMulesoftOrderingService llaMulesoftOrderingService;

	@Autowired
	private ConfigurationService configurationService;

	@Override
	public Transition executeAction(final OrderProcessModel orderProcessModel) throws RetryLaterException, Exception {
		if (configurationService.getConfiguration().getBoolean(VTR_ORDER_PROVISIONING_ENABLED))
		{
			LOG.info("Inside the Provisioning Order");
			final OrderModel order = orderProcessModel.getOrder();
			final CustomerModel customer = (CustomerModel) order.getUser();
			if (null != customer.getCustomerID() && CollectionUtils.isNotEmpty(customer.getBillingAccounts())) {
				final String orderID = llaMulesoftOrderingService.placeOrder(order, SourceEPC.SIEBEL.getCode());
				if (StringUtils.isNotEmpty(orderID)) {
					order.setStatus(OrderStatus.ORDER_PREPARATION_IN_PROGRESS);
					order.setBSSOrderId(orderID);
					orderProcessModel.setRetryCount(0);
					modelService.save(orderProcessModel);
					modelService.save(order);
					modelService.refresh(order);
					LOG.info("Order Provisioned successfully");
					return Transition.OK;
				} else {
					return Transition.NOK;
				}
			}
			else
				{
				LOG.info(String.format("Customer ID or Customer Billing Account is not existing for the Order::: %s", order.getCode()));
				return Transition.NOK;
			}
		}
		else
		{
			LOG.info(String.format("Skipping order provisioning API call and moving onto CSR notification for order no. ::: %s", orderProcessModel.getOrder().getCode()));
			return Transition.OK;
		}
	}

}
