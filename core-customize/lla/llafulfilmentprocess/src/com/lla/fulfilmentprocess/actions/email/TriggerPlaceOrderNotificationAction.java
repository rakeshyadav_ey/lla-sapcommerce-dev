package com.lla.fulfilmentprocess.actions.email;

import com.lla.fulfilmentprocess.actions.order.LLABusinessProcessRetriesHandler;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.NotificationEmailProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class TriggerPlaceOrderNotificationAction  extends AbstractSimpleDecisionAction<NotificationEmailProcessModel> {
    private static final Logger LOG = Logger.getLogger(TriggerPlaceOrderNotificationAction.class);
    @Autowired
    private LLABusinessProcessRetriesHandler llaBusinessProcessRetriesHandler;
    @Autowired
    private LLAMulesoftNotificationService llaMulesoftNotificationService;

    @Override
    public Transition executeAction(NotificationEmailProcessModel notificationEmailProcessModel) throws RetryLaterException, Exception {
        OrderModel order = notificationEmailProcessModel.getOrder();
        Transition returnTransition = null;
        if (!Boolean.valueOf(notificationEmailProcessModel.isPlaceOrderNotificationSent())) {
            try {
                String response = llaMulesoftNotificationService.generateOrderNotification(order);
                if (StringUtils.isNotEmpty(response)) {
                    notificationEmailProcessModel.setPlaceOrderNotificationSent(Boolean.TRUE);
                    notificationEmailProcessModel.setRetryCount(0);
                    returnTransition = Transition.OK;
                    modelService.save(notificationEmailProcessModel);
                    modelService.refresh(notificationEmailProcessModel);
                }
            } catch (LLAApiException llaApiException) {

                if (notificationEmailProcessModel.getRetryCount() > llaBusinessProcessRetriesHandler.getMaxRetries()) {
                    LOG.warn(String.format("Customer Notification failed for Order ::: %s as connectivity to API got failed %s times", order.getCode(), notificationEmailProcessModel.getRetryCount()));
                    return llaBusinessProcessRetriesHandler.handleRetriesFailed(order, notificationEmailProcessModel, "Customer Notification Fails !!! Retries attempt exhausted for action");
                }
                LOG.info(String.format("Retry Customer Notification for Order %s in %s minutes", order.getCode(), (llaBusinessProcessRetriesHandler.getRetryDelay(notificationEmailProcessModel) / 1000) / 60));
                throw llaBusinessProcessRetriesHandler.handleFailedState(notificationEmailProcessModel, "Customer Notification");
            }

        } else {
            LOG.info("Place Order Notification email already sent for Order ::: "+ order.getCode());
            return Transition.OK;
        }
        return returnTransition;
    }
}
