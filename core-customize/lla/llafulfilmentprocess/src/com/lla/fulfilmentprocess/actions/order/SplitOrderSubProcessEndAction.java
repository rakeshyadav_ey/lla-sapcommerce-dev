package com.lla.fulfilmentprocess.actions.order;

import com.lla.fulfilmentprocess.actions.consignment.SubprocessEndAction;
import com.lla.fulfilmentprocess.constants.LlaFulfilmentProcessConstants;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.SplitOrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class SplitOrderSubProcessEndAction extends AbstractProceduralAction<SplitOrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(SplitOrderSubProcessEndAction.class);

    private static final String PROCESS_MSG = "Process: ";

    @Autowired
    BusinessProcessService businessProcessService;
    @Override
    public void executeAction(SplitOrderProcessModel process) throws RetryLaterException, Exception {
       LOG.info(PROCESS_MSG + process.getCode() + " in step " + getClass());

        try
        {
            // simulate different ending times
            Thread.sleep((long) (Math.random() * 2000));
        }
        catch (final InterruptedException e)
        {
            // can't help it
        }

        process.setDone(true);

        save(process);
        LOG.info(PROCESS_MSG + process.getCode() + " wrote DONE marker");
        final OrderModel order=process.getOrder();
        order.setStatus(OrderStatus.COMPLETED);
        modelService.save(order);
        businessProcessService.triggerEvent(
                process.getParentProcess().getCode() + "_"
                        + LlaFulfilmentProcessConstants.SPLIT_ORDER_SUBPROCESS_END_EVENT_NAME);
        LOG.info(PROCESS_MSG + process.getCode() + " fired event "
                + LlaFulfilmentProcessConstants.SPLIT_ORDER_SUBPROCESS_END_EVENT_NAME);
    }
}
