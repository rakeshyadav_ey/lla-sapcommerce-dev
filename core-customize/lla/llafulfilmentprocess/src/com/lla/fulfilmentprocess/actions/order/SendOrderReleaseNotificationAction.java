/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.fulfilmentprocess.actions.order;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;


public class SendOrderReleaseNotificationAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(SendOrderReleaseNotificationAction.class);
	@Autowired
	private LLAMulesoftNotificationService llaMulesoftNotificationService;
	@Autowired
	private CatalogVersionService catalogVersionService;

	@Autowired
	private BaseSiteService baseSiteService;
	@Autowired
	private UserService userService;
	@Override
	public Transition executeAction(final OrderProcessModel process)
	{

		OrderModel order=process.getOrder();
		if (order == null || null == order.getEntries())
		{
			LOG.error(String.format("Missing the order, exiting the process for Order ::: %s",order.getCode()));
			return Transition.NOK;
		}
		String response = StringUtils.EMPTY;
		try {
			response = llaMulesoftNotificationService.sendOrderProvisionMail(order);
		} catch (LLAApiException exception) {
			LOG.error(String.format("Exception in making call Order Release Notification email fails for order %s",order.getCode()));
		}
		if(StringUtils.isNotEmpty(response)){
			return Transition.OK;
		}
		return Transition.NOK;

	}
}
