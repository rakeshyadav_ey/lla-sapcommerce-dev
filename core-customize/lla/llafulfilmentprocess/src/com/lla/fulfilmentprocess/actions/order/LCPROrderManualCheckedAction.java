package com.lla.fulfilmentprocess.actions.order;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class LCPROrderManualCheckedAction extends AbstractSimpleDecisionAction<OrderProcessModel> {


    private static final Logger LOG = Logger.getLogger(LCPROrderManualCheckedAction.class);

    /**
     *
     * @param process
     * @return
     */
    @Override
    public Transition executeAction(final OrderProcessModel process)
    {
        final OrderModel order = process.getOrder();

        if (order == null)
        {
            LOG.error("Missing the order, exiting the process");
            return Transition.NOK;
        }

        if(OrderStatus.MANUAL_CHECK_COMPLETED.equals(order.getStatus())){
            return validateManualReview(order);
        }else{
            order.setStatus(OrderStatus.MANUAL_CHECK_PENDING);
            LOG.info(String.format("Order %s is still not manually verified",order.getCode()));
            getModelService().save(order);
            return  Transition.NOK;
        }


    }

    /**
     *
     * @param order
     * @return
     */
    private Transition validateManualReview(OrderModel order) {
        AddressModel addressModel=order.getDeliveryAddress();

        if(order.getInstallationType()!=null && StringUtils.length(addressModel.getPostalcode())==9 && Boolean.valueOf(order.getServiceabilityCheck()) && Boolean.valueOf(order.getCreditCheck()) && Boolean.valueOf(order.getDuplicateCustomerCheck())){

            return Transition.OK;
        }
        else{
            LOG.info(String.format("Order %s is not manually verified",order.getCode()));
            order.setStatus(OrderStatus.MANUAL_CHECK_PENDING);
            getModelService().save(order);
            return Transition.NOK;
        }
     }
}
