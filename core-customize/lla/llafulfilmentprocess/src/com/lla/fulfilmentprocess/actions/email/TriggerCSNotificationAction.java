package com.lla.fulfilmentprocess.actions.email;

import com.google.common.collect.Lists;
import com.lla.fulfilmentprocess.actions.order.LLABusinessProcessRetriesHandler;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.core.util.LLAEncryptionUtil;
import com.lla.fulfilmentprocess.actions.order.CheckOrderAction;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.ordersplitting.model.NotificationEmailProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.task.RetryLaterException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TriggerCSNotificationAction extends AbstractSimpleDecisionAction<NotificationEmailProcessModel>{

    private static final Logger LOG = Logger.getLogger(TriggerCSNotificationAction.class);
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

    @Autowired
    private FlexibleSearchService flexibleSearchService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private CatalogVersionService catalogVersionService;
    @Autowired
    private LLABusinessProcessRetriesHandler llaBusinessProcessRetriesHandler;
    @Autowired
    private BaseSiteService baseSiteService;
    @Autowired
    private UserService userService;
    @Autowired
    private LLAMulesoftNotificationService llaMulesoftNotificationService;
    @Autowired
   	private LLAEncryptionUtil llaEncryptionUtil;
    @Override
    public Transition executeAction(NotificationEmailProcessModel notificationEmailProcessModel) throws RetryLaterException, Exception {
        Transition returnTransition=null;
        if(!Boolean.valueOf(notificationEmailProcessModel.isAgentNotificationSent())){
            OrderModel orderModel=notificationEmailProcessModel.getOrder();
            baseSiteService.setCurrentBaseSite(orderModel.getSite(),false);
            userService.setCurrentUser(orderModel.getUser());
            catalogVersionService.setSessionCatalogVersion(orderModel.getSite().getUid()+"ProductCatalog","Online");
            catalogVersionService.setSessionCatalogVersion(orderModel.getSite().getUid()+"ProductCatalog","Staged");

            List<EmployeeModel> csaAgents = new ArrayList<>();
            if(llaMulesoftIntegrationUtil.isPanamaOrder(orderModel.getSite())){
                csaAgents=  llaMulesoftIntegrationUtil.getEmployeesInUserGroup("backofficeworkflowadmingroup").stream().filter(employee -> employee.getGroups().contains(userService.getUserGroupForUID("panama"))).collect(Collectors.toList());
            }else if(llaMulesoftIntegrationUtil.isJamaicaOrder(orderModel.getSite())){
                csaAgents=  llaMulesoftIntegrationUtil.getEmployeesInUserGroup("backofficeworkflowadmingroup").stream().filter(employee -> employee.getGroups().contains(userService.getUserGroupForUID("jamaica"))).collect(Collectors.toList());
            }else if(llaMulesoftIntegrationUtil.isPuertoricoOrder(orderModel.getSite())){
                csaAgents= llaMulesoftIntegrationUtil.getEmployeesInUserGroup("backofficeworkflowadmingroup").stream().filter(employee -> employee.getGroups().contains(userService.getUserGroupForUID("puertorico"))).collect(Collectors.toList());
            }else  if(llaMulesoftIntegrationUtil.isCableticaOrder(orderModel.getSite())){
                csaAgents= llaMulesoftIntegrationUtil.getEmployeesInUserGroup("backofficeworkflowadmingroup").stream().filter(employee -> employee.getGroups().contains(userService.getUserGroupForUID("cabletica"))).collect(Collectors.toList());
            }
            else  if(llaMulesoftIntegrationUtil.isVtrOrder(orderModel.getSite())){
               csaAgents= llaMulesoftIntegrationUtil.getEmployeesInUserGroup("customersupportagentgroup").stream().filter(employee -> employee.getGroups().contains(userService.getUserGroupForUID("chile"))).collect(Collectors.toList());
           }

            List<String> emails = llaMulesoftIntegrationUtil.getEmailAddressesOfEmployees(csaAgents);

            try{
            	final CustomerModel customerModel = (CustomerModel) orderModel.getUser();
            	final String ssnValue = llaMulesoftIntegrationUtil.formatSSNNumber(customerModel.getCustomerSSN());
                String response=llaMulesoftNotificationService.triggerCSA_Notification(orderModel, emails, ssnValue);
                if(StringUtils.isNotEmpty(response)) {
                    notificationEmailProcessModel.setAgentNotificationSent(Boolean.TRUE);
                    notificationEmailProcessModel.setRetryCount(0);
                    returnTransition = Transition.OK;
                    modelService.save(notificationEmailProcessModel);
                    modelService.refresh(notificationEmailProcessModel);
                }
            }catch(LLAApiException exception)
            {
                if (notificationEmailProcessModel.getRetryCount() > llaBusinessProcessRetriesHandler.getMaxRetries()) {
                    LOG.warn(String.format("CS Agent Notification failed for Order ::: %s as connectivity to API got failed %s times",orderModel.getCode(),notificationEmailProcessModel.getRetryCount()));
                    return llaBusinessProcessRetriesHandler.handleRetriesFailed(orderModel, notificationEmailProcessModel, "CS Agent Notification Fails !!! Retries attempt exhausted for action");
                }
                LOG.info(String.format("Retry CS Agent Notification for Order %s in %s minutes" ,orderModel.getCode(),(llaBusinessProcessRetriesHandler.getRetryDelay(notificationEmailProcessModel) / 1000)/60));
                throw llaBusinessProcessRetriesHandler.handleFailedState(notificationEmailProcessModel, "Failed to Create CS Agent Notification");
            }

        }else{
            LOG.info(String.format("CS Agent Notification email already sent for Order ::%s",notificationEmailProcessModel.getOrder().getCode()));
            return  Transition.OK;
        }
        return returnTransition;
    }
}