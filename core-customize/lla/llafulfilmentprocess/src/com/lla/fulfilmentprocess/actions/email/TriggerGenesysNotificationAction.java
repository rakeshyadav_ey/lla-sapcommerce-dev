package com.lla.fulfilmentprocess.actions.email;

import com.google.common.collect.Lists;
import com.lla.core.service.order.LLACommerceCheckoutService;
import com.lla.fulfilmentprocess.actions.order.LLABusinessProcessRetriesHandler;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import com.lla.telcotmfwebservices.v2.dto.Customer;
import de.hybris.platform.acceleratorcms.model.components.ClickToCallModel;
import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.*;
import de.hybris.platform.ordersplitting.model.NotificationEmailProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.task.RetryLaterException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TriggerGenesysNotificationAction extends AbstractSimpleDecisionAction<NotificationEmailProcessModel>{

    private static final Logger LOG = Logger.getLogger(TriggerGenesysNotificationAction.class);
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

    @Autowired
    private FlexibleSearchService flexibleSearchService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private CatalogVersionService catalogVersionService;
    @Autowired
    private LLABusinessProcessRetriesHandler llaBusinessProcessRetriesHandler;
    @Autowired
    private BaseSiteService baseSiteService;
    @Autowired
    private UserService userService;
    @Autowired
    private CommonI18NService commonI18NService;
    @Autowired
    private LLACommerceCheckoutService llaCommerceCheckoutService;
    @Override
    public Transition executeAction(NotificationEmailProcessModel notificationEmailProcessModel) throws RetryLaterException, Exception {
        OrderModel order=notificationEmailProcessModel.getOrder();
        baseSiteService.setCurrentBaseSite(order.getSite(),false);
        commonI18NService.setCurrentLanguage(order.getLanguage());
        userService.setCurrentUser(order.getUser());
        catalogVersionService.setSessionCatalogVersion(order.getSite().getUid()+"ProductCatalog","Online");
        catalogVersionService.setSessionCatalogVersion(order.getSite().getUid()+"ProductCatalog","Staged");
        if(!llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite())){
            LOG.info("Genesys Notification not required for Order :::"+ order.getCode()+"as  it doesn't belong to Cabletica ");
            return Transition.NOK;
        }
        Transition returnTransition = null;
        if(llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite()) && !Boolean.valueOf(notificationEmailProcessModel.isC2cNotificationSent())){
            try {
                ClickToCallModel response=null;
                CustomerModel customerModel=(CustomerModel) order.getUser();
                ClickToCallModel c2cItem = getClickToCallModelForOrder(order);
                if(null!=c2cItem) {
                    response = llaCommerceCheckoutService.executeC2CRequest(order, c2cItem);
                }else{
                    response = llaCommerceCheckoutService.executeCustomerCallBackRequest(order, null);
                }
                if (BooleanUtils.isTrue(response.getGenesysNotificationSent())) {
                    notificationEmailProcessModel.setC2cNotificationSent(Boolean.TRUE);
                    notificationEmailProcessModel.setRetryCount(0);
                    returnTransition = Transition.OK;
                    modelService.save(notificationEmailProcessModel);
                    modelService.refresh(notificationEmailProcessModel);
                }else{
                    throw new LLAApiException("Exception in making Api Call");
                }
            }catch(LLAApiException llaApiException){

                if (notificationEmailProcessModel.getRetryCount() > llaBusinessProcessRetriesHandler.getMaxRetries()) {
                    LOG.warn(String.format("Genesys Notification failed for Order ::: %s as connectivity to API got failed %s times",order.getCode(),notificationEmailProcessModel.getRetryCount()));
                    return llaBusinessProcessRetriesHandler.handleRetriesFailed(order, notificationEmailProcessModel, "Genesys Notification Fails !!! Retries attempt exhausted for action");
                }
                LOG.info(String.format("Retry Genesys Notification for Order %s in %s minutes" ,order.getCode(),(llaBusinessProcessRetriesHandler.getRetryDelay(notificationEmailProcessModel) / 1000)/60));
                throw llaBusinessProcessRetriesHandler.handleFailedState(notificationEmailProcessModel, "Failed to Create C2C Notification in Genesys");
            }
        }else {
            LOG.info(String.format("Genesys Notification  already sent for Order :::%s ",order.getCode()));
            return  Transition.OK;
        }
        return returnTransition;
    }

    /**
     * Return Click to Call Associated for Order
     * @param order
     * @return
     */
    private ClickToCallModel getClickToCallModelForOrder(OrderModel order) {
        ClickToCallModel clickToCallModel=new ClickToCallModel();
        clickToCallModel.setOrder(order);
        try {

            List<ClickToCallModel> c2cItemList = flexibleSearchService.getModelsByExample(clickToCallModel);
            if(CollectionUtils.isNotEmpty(c2cItemList)){
                return  c2cItemList.get(0);
            }else{
                return null;
            }
        }catch (ModelNotFoundException notFoundException){
            return null;
        }
    }

}
