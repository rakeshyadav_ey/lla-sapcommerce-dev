package com.lla.fulfilmentprocess.actions.order;

import com.lla.fulfilmentprocess.constants.LlaFulfilmentProcessConstants;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.NotificationEmailProcessModel;
import de.hybris.platform.ordersplitting.model.SplitOrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import javax.annotation.Resource;

public class LLABusinessProcessRetriesHandler {
    @Resource
    private ModelService modelService;

    @Resource
    private ConfigurationService configurationService;

    /**
     * @param errorReason
     *
     */
    public RetryLaterException handleFailedState(final BusinessProcessModel process, final String errorReason)
    {
        incrementRetryCount(process);
        return getRetryLaterException(process, errorReason);
    }
    private RetryLaterException getRetryLaterException(BusinessProcessModel process, String errorReason) {
        process.setEndMessage(errorReason);
        modelService.save(process);
        final RetryLaterException expRetry = new RetryLaterException(errorReason);
        expRetry.setMethod(RetryLaterException.Method.LINEAR);
        expRetry.setRollBack(false);
        expRetry.setDelay(getRetryDelay(process));
        return expRetry;
    }

    public long getRetryDelay(BusinessProcessModel process) {
        if (process instanceof SplitOrderProcessModel || process instanceof OrderProcessModel)
        {
             return  configurationService.getConfiguration().getLong(LlaFulfilmentProcessConstants.RETRYTIME);
        }
        else{
            return  configurationService.getConfiguration().getLong(LlaFulfilmentProcessConstants.NOTIFICATION_RETRY_TIME);
        }
    }

    private void incrementRetryCount(final BusinessProcessModel process)
    {
        if (process instanceof SplitOrderProcessModel || process instanceof OrderProcessModel || process instanceof NotificationEmailProcessModel)
        {
            process.setRetryCount(getRetryCount(process) + 1);
        }

    }

    private int getRetryCount(final BusinessProcessModel process)
    {
        if (process instanceof SplitOrderProcessModel || process instanceof OrderProcessModel || process instanceof NotificationEmailProcessModel)
        {
            return process.getRetryCount();
        }
        return getMaxRetries();
    }


    @SuppressWarnings("javadoc")
    public int getMaxRetries()
    {
        return configurationService.getConfiguration().getInt(LlaFulfilmentProcessConstants.MAX_RETRY_COUNT);
    }


    public AbstractSimpleDecisionAction.Transition handleRetriesFailed(final OrderModel order, final BusinessProcessModel process, final String failureReason)
    {
        process.setState(ProcessState.FAILED);
        process.setEndMessage(failureReason);
        modelService.save(process);
        return  AbstractSimpleDecisionAction.Transition.NOK;


    }
}
