/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.fulfilmentprocess.actions.order;

import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


public class SendOrderPlacedNotificationAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(SendOrderPlacedNotificationAction.class);

	private LLAMulesoftNotificationService llaMulesoftNotificationService;

	private Converter<OrderModel, OrderData> orderConverter;

	public LLAMulesoftNotificationService getLlaMulesoftNotificationService() {
		return llaMulesoftNotificationService;
	}

	public void setLlaMulesoftNotificationService(LLAMulesoftNotificationService llaMulesoftNotificationService) {
		this.llaMulesoftNotificationService = llaMulesoftNotificationService;
	}


	public Converter<OrderModel, OrderData> getOrderConverter() {
		return orderConverter;
	}

	public void setOrderConverter(Converter<OrderModel, OrderData> orderConverter) {
		this.orderConverter = orderConverter;
	}
	@Override
	public Transition executeAction(final OrderProcessModel process)
	{

		OrderModel order=process.getOrder();
		if (order == null || null == order.getEntries())
		{
			LOG.error(String.format("Missing the order, exiting the process for Order ::: %s",order.getCode()));
			return Transition.NOK;
		}
		String response = StringUtils.EMPTY;
		try {
			response = llaMulesoftNotificationService.generateOrderNotification(order);
		} catch (LLAApiException exception) {
			LOG.error(String.format("Error in making API Call.. Order Confirmation Notification Fails for Order %s",order.getCode()));
		}
		if(!StringUtils.startsWith(response,"error:")){
			LOG.info(String.format("Order confirmation Notification Response %s for Order :::  %s" ,response,order.getCode()));
			getModelService().save(order);
			return Transition.OK;
		}else{
			return Transition.NOK;
		}



	}

}
