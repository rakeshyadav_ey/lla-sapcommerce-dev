package com.lla.fulfilmentprocess.actions.order;

import com.lla.core.enums.SourceEPC;
import com.lla.fulfilmentprocess.util.LLAFulfillmentProcessOrderUtil;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import com.lla.mulesoft.integration.service.LLAMulesoftOrderingService;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.SplitOrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class LCPRCreateProvisioningOrderAction extends AbstractSimpleDecisionAction<OrderProcessModel> {
    private static final Logger LOG = Logger.getLogger(LCPRCreateProvisioningOrderAction.class);

    @Autowired
    private LLAMulesoftOrderingService llaMulesoftOrderingService;
    @Autowired
    private LLAMulesoftNotificationService llaMulesoftNotificationService;
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;
    @Autowired
    private LLABusinessProcessRetriesHandler llaBusinessProcessRetriesHandler;
    @Autowired
    private LLAFulfillmentProcessOrderUtil llaFulfillmentProcessOrderUtil;

    @Override
    public Transition executeAction(OrderProcessModel orderProcessModel) throws RetryLaterException, Exception {
        LOG.info("Inside the Provisioning Order");
        final OrderModel order = orderProcessModel.getOrder();
        if(null!=order.getJobType() && null!=order.getDropType() && null!=order.getBusinessUnit() && null!=order.getLocationId()){

            String orderID =llaMulesoftOrderingService.lcprPlaceOrder(order, SourceEPC.CSG.getCode());
            if(StringUtils.isNotEmpty(orderID)) {
                order.setBSSOrderId(orderID);
                order.setStatus(OrderStatus.ORDER_PREPARATION_IN_PROGRESS);
                orderProcessModel.setRetryCount(0);
                modelService.save(orderProcessModel);
                modelService.save(order);
                modelService.refresh(order);
                LOG.info("Order Provisioned successfully");
                return Transition.OK;
            }else
               return Transition.NOK;
        }else{
            LOG.info(String.format("LocationId or JobType or DropType or BusinessUnit  is not mapped for the Order ::: %s",order.getCode()));
            return Transition.NOK;
        }

    }
}
