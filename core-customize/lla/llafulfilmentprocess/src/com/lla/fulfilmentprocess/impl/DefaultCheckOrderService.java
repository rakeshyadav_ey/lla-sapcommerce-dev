/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.fulfilmentprocess.impl;

import com.lla.core.util.LLACommonUtil;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import com.lla.fulfilmentprocess.CheckOrderService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Default implementation of {@link CheckOrderService}
 */
public class DefaultCheckOrderService implements CheckOrderService
{
	@Autowired
	private ConfigurationService configurationService;
	@Autowired
	private LLACommonUtil llaCommonUtil;
	@Autowired
	private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

	@Autowired
	private CatalogVersionService catalogVersionService;


	@Override
	public boolean check(final OrderModel order)
	{
		if (!order.getCalculated().booleanValue())
		{
			// Order must be calculated
			return false;
		}
		if (order.getEntries().isEmpty())
		{
			// Order must have some lines
			return false;
		}
	/*	else if (order.getPaymentInfo() == null)
		{
			// Order must have some payment info to use in the process
			return false;
		}*/
		else
		{
			// Order delivery options must be valid
			return checkDeliveryOptions(order);
		}
	}

	protected boolean checkDeliveryOptions(final OrderModel order)
	{
		final BaseSiteModel siteModel=order.getSite();

		if(llaMulesoftIntegrationUtil.isPanamaOrder(siteModel)){
			catalogVersionService.setSessionCatalogVersion(order.getSite().getUid()+"ProductCatalog","Online");
			catalogVersionService.setSessionCatalogVersion(order.getSite().getUid()+"ProductCatalog","Staged");
			if (order.getDeliveryMode() == null && llaCommonUtil.hideDeliveryMethod(order))
			{
				return true;
			}
	     	if (order.getDeliveryAddress() == null)
			{
				for (final AbstractOrderEntryModel entry : order.getEntries())
				{
					if (entry.getDeliveryPointOfService() == null && entry.getDeliveryAddress() == null)
					{
						// Order and Entry have no delivery address and some entries are not for pickup
						return false;
					}
				}
			}

		}

		return true;
	}
}
