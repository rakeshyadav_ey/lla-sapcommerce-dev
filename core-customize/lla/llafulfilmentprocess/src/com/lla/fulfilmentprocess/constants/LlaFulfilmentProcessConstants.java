/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.fulfilmentprocess.constants;

@SuppressWarnings("deprecation")
public final class LlaFulfilmentProcessConstants extends GeneratedLlaFulfilmentProcessConstants
{
	public static final String CONSIGNMENT_SUBPROCESS_END_EVENT_NAME = "ConsignmentSubprocessEnd";
	public static final String SPLIT_ORDER_SUBPROCESS_END_EVENT_NAME = "SplitOrderProcessEnd";
	public static final String ORDER_PROCESS_NAME = "order-process";
	public static final String CONSIGNMENT_SUBPROCESS_NAME = "consignment-process";
	public static final String SPLIT_ORDER_SUBPROCESS_NAME = "split-order-process";
	public static final String PANAMA_SPLIT_ORDER_SUBPROCESS_NAME = "panama-split-order-process";
	public static final String WAIT_FOR_WAREHOUSE = "WaitForWarehouse";
	public static final String WAIT_FOR_SPLITORDER = "WaitForSplitOrder";
	public static final String CONSIGNMENT_PICKUP = "ConsignmentPickup";
	public static final String CONSIGNMENT_COUNTER = "CONSIGNMENT_COUNTER";
	public static final String PARENT_PROCESS = "PARENT_PROCESS";
	public static final String CUSTOMER = "CUSTOMER";
	public static final String BASE_STORE = "BASE_STORE";
	public static final String RETRYTIME = "service.retry.dealy.time";
	public static final String NOTIFICATION_RETRY_TIME = "notification.service.retry.delay.time";
	public static final String MAX_RETRY_COUNT="service.max.retry.count";
}
