package com.lla.fulfilmentprocess.service;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.OrderSplittingService;

import java.util.List;

public interface LLAOrderSplittingService extends OrderSplittingService {
    List<OrderModel> splitOrderBySourceEPC(final OrderModel parentOrder);

    List<OrderModel> splitPanamaOrder(OrderModel order);
}
