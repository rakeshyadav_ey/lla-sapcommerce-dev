package com.lla.fulfilmentprocess.service.impl;

import com.lla.core.enums.DevicePriceType;
import com.lla.core.enums.SourceEPC;
import com.lla.core.util.LLACommonUtil;
import com.lla.fulfilmentprocess.service.LLAOrderSplittingService;
import com.lla.fulfilmentprocess.util.LLAFulfillmentProcessOrderUtil;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.pricing.services.TmaCalculationService;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.order.OrderService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.ordercloning.CloneAbstractOrderStrategy;
import de.hybris.platform.ordersplitting.impl.DefaultOrderSplittingService;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.subscriptionservices.model.RecurringChargeEntryModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionPricePlanModel;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.function.Predicate;

public class LLAOrderSplittingServiceImpl extends DefaultOrderSplittingService implements LLAOrderSplittingService {
    public static final String THREEP_BUNDLES = "3Pbundles";
    public static final String TWOP_INTERNET_TV = "2PInternetTv";
    public static final String TWOP_INTERNET_TELEPHONE = "2PInternetTelephone";
    private static final Logger LOG = Logger.getLogger(LLAOrderSplittingServiceImpl.class);
    @Autowired
    private CloneAbstractOrderStrategy cloneAbstractOrderStrategy;
    @Autowired
    private CatalogVersionService catalogVersionService;
    private ComposedTypeModel orderType;
    private ComposedTypeModel orderEntryType;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private PersistentKeyGenerator orderCodeGenerator;

    @Autowired
    BaseStoreService baseStoreService;

    @Resource(name="calculationService")
    private TmaCalculationService calculationService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private BaseSiteService baseSiteService;

    @Autowired
    private ModelService modelService;
    @Autowired
    private TypeService typeService;

    @Autowired
    private LLACommonUtil llaCommonUtil;
    @Autowired
    private LLAFulfillmentProcessOrderUtil llaFulfillmentProcessOrderUtil;

    @Override
    public List<OrderModel> splitOrderBySourceEPC(final OrderModel parentOrder) {
        catalogVersionService.setSessionCatalogVersion(parentOrder.getSite().getUid() + "ProductCatalog", "Online");
        catalogVersionService.setSessionCatalogVersion(parentOrder.getSite().getUid() + "ProductCatalog", "Staged");
        LOG.info(null != parentOrder.getSite() ? parentOrder.getSite().getUid() : "No Site Mapped");
        baseSiteService.setCurrentBaseSite(parentOrder.getSite(),false);
        final List<AbstractOrderEntryModel> liberateEntries=new ArrayList();
        final List<AbstractOrderEntryModel> cerillionEntries=new ArrayList();
        for(AbstractOrderEntryModel entry:parentOrder.getEntries())
        {
            if(SourceEPC.CERILLION.getCode().equalsIgnoreCase(entry.getProductMapper().getSourceEPC().getCode())) {
                cerillionEntries.add(entry);
             }
            if(SourceEPC.LIBERATE.getCode().equalsIgnoreCase(entry.getProductMapper().getSourceEPC().getCode())) {
                liberateEntries.add(entry);
            }
        }
        try {
           return splitParentOrderBasedOnSourceEpc(parentOrder,liberateEntries,cerillionEntries);

        } catch (CalculationException exception) {
            LOG.error("Error in Child Order Calculation for Parent Order :::" +parentOrder.getCode(),  exception);
        }

        return null;

     }

    /**
     * Split Panama Parent Order
     * @param parentOrder
     * @return
     */
    @Override
    public List<OrderModel> splitPanamaOrder(OrderModel parentOrder) {
        catalogVersionService.setSessionCatalogVersion(parentOrder.getSite().getUid() + "ProductCatalog", "Online");
        catalogVersionService.setSessionCatalogVersion(parentOrder.getSite().getUid() + "ProductCatalog", "Staged");
        LOG.info(null != parentOrder.getSite() ? parentOrder.getSite().getUid() : "No Site Mapped");
        baseSiteService.setCurrentBaseSite(parentOrder.getSite(),false);
        final Map<TmaSimpleProductOfferingModel, Integer> liberateEntriesMap=new HashMap();
        final Map<TmaSimpleProductOfferingModel,Integer>  ushaCommEntriesMap=new HashMap();
        CategoryModel threepBundleCategory=categoryService.getCategoryForCode(THREEP_BUNDLES);
        CategoryModel twpPInternetTvCategory=categoryService.getCategoryForCode(TWOP_INTERNET_TV);
        CategoryModel twoPInternetTelephoneCategory=categoryService.getCategoryForCode(TWOP_INTERNET_TELEPHONE);
        CategoryModel addonCategory=categoryService.getCategoryForCode("addon");

        UnitModel unitModel=null;
        for(AbstractOrderEntryModel item:parentOrder.getEntries()){
            unitModel=item.getUnit();
            if(validateOrderEntriesHasFMCProducts(item)){
                final Collection<ProductModel> fmcChildProductsList=((TmaSimpleProductOfferingModel) item.getProduct()).getFmcChildProductList();
                for(ProductModel fmcChildProduct:fmcChildProductsList){
                    if(fmcChildProduct instanceof  TmaSimpleProductOfferingModel){
                        TmaSimpleProductOfferingModel product=(TmaSimpleProductOfferingModel) fmcChildProduct;
                       if (validateProductCategories(threepBundleCategory.getProducts(),product.getCode()) || validateProductCategories(twpPInternetTvCategory.getProducts(),product.getCode()) || validateProductCategories(twoPInternetTelephoneCategory.getProducts(),product.getCode()))
                        {
                           liberateEntriesMap.put((TmaSimpleProductOfferingModel)fmcChildProduct,1);
                        }else{
                           ushaCommEntriesMap.put((TmaSimpleProductOfferingModel)fmcChildProduct,item.getQuantity().intValue());
                        }
                    }
                }
            }else if(validateProductCategories(threepBundleCategory.getProducts(),item.getProduct().getCode())|| validateProductCategories(twpPInternetTvCategory.getProducts(),item.getProduct().getCode()) || validateProductCategories(twoPInternetTelephoneCategory.getProducts(),item.getProduct().getCode())||validateProductCategories(addonCategory.getProducts(), item.getProduct().getCode())){
                liberateEntriesMap.put((TmaSimpleProductOfferingModel)item.getProduct(),item.getQuantity().intValue());
            }else{
                ushaCommEntriesMap.put((TmaSimpleProductOfferingModel)item.getProduct(),item.getQuantity().intValue());
            }
        }
        try {
            return splitPanamaParentOrder(parentOrder,liberateEntriesMap,ushaCommEntriesMap,unitModel);

        } catch (CalculationException exception) {
            LOG.error("Error in Child Order Calculation for Parent Order :::" +parentOrder.getCode(),  exception);
        }

        return null;
    }

    private boolean validateProductCategories(final List<ProductModel> allProducts,final String code) {

        Predicate<ProductModel> p1 = s -> s.getCode().equalsIgnoreCase(code);
        return allProducts.stream().anyMatch(p1)?true:false;
    }
     /**
     *  Create Child Order For Panama Parent Order
     * @param parentOrder
     * @param liberateEntries
     * @param ushaCommEntries
     * @param unitModel
     * @return
     * @throws CalculationException
     */
    private List<OrderModel> splitPanamaParentOrder(OrderModel parentOrder, Map<TmaSimpleProductOfferingModel,Integer>  liberateEntries, Map<TmaSimpleProductOfferingModel,Integer>  ushaCommEntries,UnitModel unitModel) throws CalculationException {

        final List<OrderModel> childOrders = new ArrayList();
        final boolean isFMC=llaFulfillmentProcessOrderUtil.checkOrderType(parentOrder,"4Pbundles");
        final boolean isPostPaid=llaFulfillmentProcessOrderUtil.checkOrderType(parentOrder,"postpaid");
        if(MapUtils.isNotEmpty(liberateEntries)){
            final OrderModel liberateOrder=createPanamaChildOrder(parentOrder,liberateEntries,unitModel,isFMC,isPostPaid);
            childOrders.add(liberateOrder);
        }
        if(MapUtils.isNotEmpty(ushaCommEntries)){
            OrderModel uniconnOrder=createPanamaChildOrder(parentOrder,ushaCommEntries,unitModel,isFMC,isPostPaid);
            for(AbstractOrderEntryModel entry:uniconnOrder.getEntries()){
              final TmaSimpleProductOfferingModel entryProduct= (TmaSimpleProductOfferingModel) entry.getProduct();
              if(llaFulfillmentProcessOrderUtil.isProductDevice(entry)){
                  if(isFMC){
                      fetchDeviceExpectedPrice(isFMC,entryProduct,entry,DevicePriceType.FMC);
                  }else if(isPostPaid){
                      fetchDeviceExpectedPrice(isPostPaid,entryProduct,entry,DevicePriceType.POSTPAID);
                  }
              }
            }
            uniconnOrder.setCalculated(Boolean.FALSE);
            calculationService.recalculate(uniconnOrder);
            childOrders.add(uniconnOrder);
        }
        parentOrder.setChildOrders(childOrders);
        modelService.save(parentOrder);
        modelService.refresh(parentOrder);
        return childOrders;
    }

    /**
     *  Create Child Order For Panama
     * @param parentOrder
     * @param childProducts
     * @param unitModel
     * @return
     * @throws CalculationException
     */
    private OrderModel createPanamaChildOrder(OrderModel parentOrder, Map<TmaSimpleProductOfferingModel,Integer>  childProducts,UnitModel unitModel, final boolean isFMC, final boolean isPostPaid) throws CalculationException {
        orderEntryType = typeService.getComposedTypeForCode("OrderEntry");
        orderType = typeService.getComposedTypeForCode("Order");
        final OrderModel order = cloneAbstractOrderStrategy.clone(orderType, orderEntryType, parentOrder, orderCodeGenerator.generate().toString(),
                OrderModel.class, AbstractOrderEntryModel.class);

        order.setEntries(null);
        order.setParentOrder(parentOrder);
        order.setPaymentInfo(parentOrder.getPaymentInfo());
        order.setStatus(OrderStatus.MANUAL_CHECK_PENDING);
        modelService.save(order);
        modelService.refresh(order);
        int index=0;
        for (Map.Entry<TmaSimpleProductOfferingModel,Integer> entry : childProducts.entrySet()){
            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
            orderService.addNewEntry(order,entry.getKey(), entry.getValue(),unitModel,index,false);
            final OrderEntryModel newEntry= orderService.getEntryForNumber(order,index);
            index++;
        }
        order.setCalculated(Boolean.FALSE);
        modelService.save(order);
        calculationService.recalculate(order);
        LOG.info(String.format("New Child Order %s Created  for Parent Order %s" , order.getCode(),parentOrder.getCode()));
        return order;
    }

    /**
     * Fetch Expected Device Price for Child Order
     * @param orderType
     * @param childEntryProduct
     * @param newEntry
     * @param fmc
     */
    private void fetchDeviceExpectedPrice(boolean orderType, TmaSimpleProductOfferingModel childEntryProduct, AbstractOrderEntryModel newEntry, DevicePriceType fmc) {
        if (orderType && llaFulfillmentProcessOrderUtil.checkOrderType(newEntry.getOrder(), "devices")) {
            Collection<PriceRowModel> priceRowCollection = childEntryProduct.getOwnEurope1Prices();
            for (PriceRowModel pricePlanModel : priceRowCollection) {
                if (pricePlanModel instanceof PriceRowModel && fmc.equals(pricePlanModel.getPriceType())) {
                    newEntry.setBasePrice(pricePlanModel.getPrice());
                    newEntry.setTotalPrice(pricePlanModel.getPrice());
                    newEntry.setCalculated(Boolean.FALSE);
                    modelService.save(newEntry);
                    modelService.refresh(newEntry);
                    modelService.save(newEntry.getOrder());
                    modelService.refresh(newEntry.getOrder());
                    break;
                }
            }
        }
    }

    /**
     * Validate Order Contains FMC Product Or Not
     * @param entry
     * @return
     */
    private boolean validateOrderEntriesHasFMCProducts(AbstractOrderEntryModel entry){
       if(entry.getProduct() instanceof TmaSimpleProductOfferingModel &&null!=((TmaSimpleProductOfferingModel) entry.getProduct()).getFmcProductFlag()&& ((TmaSimpleProductOfferingModel) entry.getProduct()).getFmcProductFlag().booleanValue()){
            return true;
        }
      return false;
    }

    /**
     * Split Jamaica Order Based on Source EPC
     * @param parentOrder
     * @param liberateEntries
     * @param cerillionEntries
     * @return
     * @throws CalculationException
     */
    private  List<OrderModel> splitParentOrderBasedOnSourceEpc(final OrderModel parentOrder, final List<AbstractOrderEntryModel> liberateEntries,
                                                  final List<AbstractOrderEntryModel> cerillionEntries) throws CalculationException  {
       final List<OrderModel> childOrders = new ArrayList();
       if(CollectionUtils.isNotEmpty(liberateEntries)){
           childOrders.add(splitOrder(parentOrder,liberateEntries));
       }
       if(CollectionUtils.isNotEmpty(cerillionEntries)){
           childOrders.add(splitOrder(parentOrder,cerillionEntries));
        }
       parentOrder.setChildOrders(childOrders);
       modelService.save(parentOrder);
       modelService.refresh(parentOrder);
       return childOrders;
    }

    /**
     * Create Child Order for Jamaica on SourceEPC Entries
     * @param parentOrder
     * @param sourceEpcEntries
     * @return
     * @throws CalculationException
     */
    private OrderModel splitOrder(final OrderModel parentOrder, List<AbstractOrderEntryModel> sourceEpcEntries) throws CalculationException {
        orderEntryType = typeService.getComposedTypeForCode("OrderEntry");
        orderType = typeService.getComposedTypeForCode("Order");
        final OrderModel order = cloneAbstractOrderStrategy.clone(orderType, orderEntryType, parentOrder, orderCodeGenerator.generate().toString(),
                OrderModel.class, AbstractOrderEntryModel.class);

        order.setEntries(null);
       // order.setAddressUID(order.getCode());
        order.setParentOrder(parentOrder);
        order.setStatus(OrderStatus.MANUAL_CHECK_PENDING);
        modelService.save(order);
        modelService.refresh(order);
        int index=0;
        for(AbstractOrderEntryModel item:sourceEpcEntries){
          orderService.addNewEntry(order, item.getProduct(), item.getQuantity(), item.getUnit(),index,false);
           final OrderEntryModel newEntry= orderService.getEntryForNumber(order,index);
           newEntry.setProductMapper(item.getProductMapper());
           newEntry.setSimCardSerialPrefix(((OrderEntryModel)item).getSimCardSerialPrefix());
           newEntry.setSimNumber(((OrderEntryModel)item).getSimNumber());
           index++;
        }
        updatePaymentTransactionAmount(order,sourceEpcEntries);
        order.setCalculated(Boolean.FALSE);
        modelService.save(order);
        calculationService.calculate(order);
        modelService.save(order);
        modelService.refresh(order);

        LOG.info(String.format("New Child Order %s Created  for Parent Order %s" , order.getCode(),parentOrder.getCode()));
        return order;

    }

    /**
     * Update Payment Transaction Amount for BSS Specific Order
     * @param splitOrder
     * @param sourceEpcEntries
     */
    private void updatePaymentTransactionAmount(final OrderModel splitOrder,  List<AbstractOrderEntryModel> sourceEpcEntries){
       double ptPlannedAmount=0.0;
       for(AbstractOrderEntryModel entry:sourceEpcEntries){
           ptPlannedAmount=+ entry.getTotalPrice();
       }

       for(PaymentTransactionModel childPt:splitOrder.getPaymentTransactions()){
            childPt.setPlannedAmount(BigDecimal.valueOf(ptPlannedAmount));
            for(PaymentTransactionEntryModel childPtEntry:childPt.getEntries())
            {
               childPtEntry.setAmount(BigDecimal.valueOf(ptPlannedAmount));
            }
        }
       modelService.save(splitOrder);
    }

}
