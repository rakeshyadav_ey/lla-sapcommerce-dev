package com.lla.fulfilmentprocess.util;

import com.lla.fulfilmentprocess.actions.order.LLABusinessProcessRetriesHandler;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.function.Predicate;

public class LLAFulfillmentProcessOrderUtil {
    private static final Logger LOG = Logger.getLogger(LLAFulfillmentProcessOrderUtil.class);

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CatalogVersionService catalogVersionService;

    @Autowired
    private ProductService productService;
    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private LLABusinessProcessRetriesHandler llaBusinessProcessRetriesHandler;

    @Autowired
    private ModelService modelService;


    /**
     * Validate Product code is part of Product list
     * @param allProducts
     * @param code
     * @return
     */
    public boolean validateProductCategories(final List<ProductModel> allProducts, final String code) {

        Predicate<ProductModel> p1 = s -> s.getCode().equalsIgnoreCase(code);
        return allProducts.stream().anyMatch(p1)?true:false;
    }

    public boolean isProductDevice(AbstractOrderEntryModel entry){
        catalogVersionService.setSessionCatalogVersion(entry.getOrder().getSite().getUid()+"ProductCatalog","Online");
        catalogVersionService.setSessionCatalogVersion(entry.getOrder().getSite().getUid()+"ProductCatalog","Staged");

        return validateProductCategories(categoryService.getCategoryForCode("devices").getProducts(), entry.getProduct().getCode());

    }

    public boolean isProductNotFixedType(AbstractOrderEntryModel entry) {
        catalogVersionService.setSessionCatalogVersion(entry.getOrder().getSite().getUid()+"ProductCatalog","Online");
        catalogVersionService.setSessionCatalogVersion(entry.getOrder().getSite().getUid()+"ProductCatalog","Staged");

        return validateProductCategories(categoryService.getCategoryForCode("mobile").getProducts(), entry.getProduct().getCode()) || validateProductCategories(categoryService.getCategoryForCode("postpaid").getProducts(), entry.getProduct().getCode())
                || validateProductCategories(categoryService.getCategoryForCode("phones").getProducts(), entry.getProduct().getCode())
                || validateProductCategories(categoryService.getCategoryForCode("prepaid").getProducts(), entry.getProduct().getCode()) || validateProductCategories(categoryService.getCategoryForCode("devices").getProducts(), entry.getProduct().getCode());
    }

    public boolean isProductFixedType(AbstractOrderEntryModel entry) {
        if(configurationService.getConfiguration().getBoolean("panama.manual.provisioning.inactive")){
            catalogVersionService.setSessionCatalogVersion(entry.getOrder().getSite().getUid()+"ProductCatalog","Online");
            catalogVersionService.setSessionCatalogVersion(entry.getOrder().getSite().getUid()+"ProductCatalog","Staged");

            return validateProductCategories(categoryService.getCategoryForCode("3Pbundles").getProducts(), entry.getProduct().getCode()) || validateProductCategories(categoryService.getCategoryForCode("4Pbundles").getProducts(), entry.getProduct().getCode())
                    || validateProductCategories(categoryService.getCategoryForCode("2PInternetTv").getProducts(), entry.getProduct().getCode())
                    || validateProductCategories(categoryService.getCategoryForCode("2PInternetTelephone").getProducts(), entry.getProduct().getCode())
                    || validateProductCategories(categoryService.getCategoryForCode("addon").getProducts(), entry.getProduct().getCode());
        }else{
            return false;
        }

    }

    public boolean checkOrderType(AbstractOrderModel order, final String categoryCode) {

        catalogVersionService.setSessionCatalogVersion(order.getSite().getUid()+"ProductCatalog","Online");
        catalogVersionService.setSessionCatalogVersion(order.getSite().getUid()+"ProductCatalog","Staged");
        Boolean returnFlag=Boolean.FALSE;
        for(AbstractOrderEntryModel entry:order.getEntries()){
            if(validateProductCategories(categoryService.getCategoryForCode(categoryCode).getProducts(), entry.getProduct().getCode())){
                returnFlag=Boolean.TRUE;
                break;
            }
        }
        return returnFlag;
    }



    public AbstractSimpleDecisionAction.Transition proceedToNextTransition(BusinessProcessModel process, OrderModel order) {
        if (order.getStatus().equals(OrderStatus.MANUAL_CHECK_COMPLETED)) {
            process.setState(ProcessState.RUNNING);
            process.setRetryCount(0);
            modelService.save(process);
            return AbstractSimpleDecisionAction.Transition.OK;
        } else {
            if (process.getRetryCount() > llaBusinessProcessRetriesHandler.getMaxRetries()) {
                LOG.warn(String.format("Agent didn't reviewed the Order %s and still the Order Status is set to  Manual Check Pending, even after %s retries attempt", order.getCode(), process.getRetryCount()));
                return llaBusinessProcessRetriesHandler.handleRetriesFailed(order, process, "Retries attempt exhausted for action");
            }
            LOG.warn(String.format("Process will be retried after %s minutes",(llaBusinessProcessRetriesHandler.getRetryDelay(process)/1000)/60));
            throw llaBusinessProcessRetriesHandler.handleFailedState(process, "Order is still not Reviewed by Agent");
        }
    }
}
