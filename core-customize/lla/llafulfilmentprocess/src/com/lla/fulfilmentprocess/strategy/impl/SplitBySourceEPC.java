package com.lla.fulfilmentprocess.strategy.impl;

import com.lla.core.enums.SourceEPC;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.strategy.AbstractSplittingStrategy;

public class SplitBySourceEPC extends AbstractSplittingStrategy {

    @Override
    public Object getGroupingObject(AbstractOrderEntryModel abstractOrderEntryModel) {
        return abstractOrderEntryModel.getProductMapper().getSourceEPC();
    }

    @Override
    public void afterSplitting(Object groupingObject, ConsignmentModel consignmentModel) {
        consignmentModel.setSourceEPC((SourceEPC) groupingObject);
    }
}
