/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.fulfilmentprocess.test.actions;

/**
 * Test counterpart for
 * {@link com.lla.fulfilmentprocess.actions.order.PrepareOrderForManualCheckAction}
 */
public class PrepareOrderForManualCheck extends TestActionTemp
{
	//EMPTY
}
