ACC.checkout = {

	_autoload: [
		"bindCheckO",
		"bindForms",
		"bindSavedPayments"
	],


	bindForms:function(){

		$(document).on("click","#addressSubmit",function(e){
			e.preventDefault();
			$('#addressForm').submit();	
		})
		
		$(document).on("click","#deliveryMethodSubmit",function(e){
			e.preventDefault();
			$('#selectDeliveryMethodForm').submit();	
		})

	},

	bindSavedPayments:function(){
		$(document).on("click",".js-saved-payments",function(e){
			e.preventDefault();

			var title = $("#savedpaymentstitle").html();

			$.colorbox({
				href: "#savedpaymentsbody",
				inline:true,
				maxWidth:"100%",
				opacity:0.7,
				//width:"320px",
				title: title,
				close:'<span class="glyphicon glyphicon-remove"></span>',
				onComplete: function(){
				}
			});
		})
	},

	bindCheckO: function ()
	{
		var cartEntriesError = false;
		
		// Alternative checkout flows options
		$('.doFlowSelectedChange').change(function ()
		{
			if ('multistep-pci' == $('#selectAltCheckoutFlow').val())
			{
				$('#selectPciOption').show();
			}
			else
			{
				$('#selectPciOption').hide();

			}
		});



		$('.js-continue-shopping-button').click(function ()
		{
			var checkoutUrl = $(this).data("continueShoppingUrl");
			window.location = checkoutUrl;
		});
		
		$('.js-create-quote-button').click(function ()
		{
			$(this).prop("disabled", true);
			var createQuoteUrl = $(this).data("createQuoteUrl");
			window.location = createQuoteUrl;
		});

		
		$('.expressCheckoutButton').click(function()
				{
					document.getElementById("expressCheckoutCheckbox").checked = true;
		});
		
		$(document).on("input",".confirmGuestEmail,.guestEmail",function(){
			  
			  var orginalEmail = $(".guestEmail").val();
			  var confirmationEmail = $(".confirmGuestEmail").val();
			  
			  if(orginalEmail === confirmationEmail){
			    $(".guestCheckoutBtn").removeAttr("disabled");
			  }else{
			     $(".guestCheckoutBtn").attr("disabled","disabled");
			  }
		});
		
		$('.js-continue-checkout-button').click(function ()
		{
			var checkoutUrl = $(this).data("checkoutUrl");
			
			cartEntriesError = ACC.pickupinstore.validatePickupinStoreCartEntires();
			if (!cartEntriesError)
			{
				var expressCheckoutObject = $('.express-checkout-checkbox');
				if(expressCheckoutObject.is(":checked"))
				{
					window.location = expressCheckoutObject.data("expressCheckoutUrl");
				}
				else
				{
					var flow = $('#selectAltCheckoutFlow').val();
					var customerLoggedIn=false;
                     if ( $('#userInSession').val() == "true" ) {
                          customerLoggedIn=true;
                     }
                    if ( (flow == undefined || flow == '' || flow == 'select-checkout') && customerLoggedIn == false )
                    {
                        var popupHeadLine =  $("#login-popup").prev(".headline").html();
                        $.colorbox({
                            inline:true,
                            scrolling:true,
                            href:"#login-popup",
                            maxWidth:"100%",
                            width:"600px",
                            //href: $(this).attr("href"),
                            close:'<span class="close-popup"></span>',
                            title: popupHeadLine,
                            onComplete: function() {
                                $("#cboxTitle").css({
                                    "padding": "0"
                                });
                                $("#cboxLoadedContent").css({
                                    "margin-top": "50px"
                                });
                                $(".headline-text").css({
                                    "background-color": "#ffb81c",
                                    "color": "#fff",
                                    "font-family": "MontserratLight",
                                    "padding": "15px",
                                    "font-weight": "bold",
                                    "font-size": "16px",
                                    "margin": "0"
                                });
                                ACC.common.refreshScreenReaderBuffer();
                            },
                            onClosed: function() {
                                ACC.common.refreshScreenReaderBuffer();
                            }
                        });
                        $(".login-popup-close").on("click", function() {
                            $.colorbox.close();
                        })
                    }
                    else
                    {
                      window.location = checkoutUrl;
                    }
				}
			}
			return false;
		});

	}

};

$(document).ready(function () {
	
	$("#addressForm").on('keypress',"input[name='firstName'], input[name='lastName'], input[name='townCity']", function(e){
		var regex = /^[a-zA-Z ]*$/;
		return regex.test(e.key )
	});
	$("#addressForm").on('keypress',"input[name='postcode'], input[name='phone']", function(e){
		const pattern = /^[0-9]$/;
		return pattern.test(e.key )
	});
	if ($("#address_validation_error").length) {
		var firstName_error = $("#firstName_error").val();
		var lastName_error = $("#lastName_error").val();
		var line1_error = $("#line1_error").val();
		var line2_error = $("#line2_error").val();
		var area_error = $("#area_error").val();
		var townCity_error = $("#townCity_error").val();
		var postcode_error = $("#postcode_error").val();
	}
	$("#addressForm").validate({
		rules: {
			"titleCode": {
				required: true,
			},
			"firstName": {
				required: true,
			},
			"lastName": {
				required: true,
			},
			"line1": {
				required: true,
			},
			"line2": {
				required: true,
			},
			"area": {
                        required: true,
            },
			"townCity": {
				required: true,
			},
			"postcode": {
				required: true,
			},
			"phone": {
				required: true,
			}	   
		},
		messages: {
			firstName: firstName_error,
			lastName: lastName_error,
			line1: line1_error,
			line2: line2_error,
			area: area_error,
			townCity: townCity_error,
			postcode: postcode_error
		},
		invalidHandler: function() {
        	var errorMsg = $("#gtmAddressFormError").val();
        	 window.dataLayer.push({
        		 'event': 'error',
        		 'error': errorMsg,
        		 'error_category': 'User error',
        	 });
        }
	});

	/* Gtm for Checkout Steps and Purchase pages Starts */
        /* Chekcout Page Starts */
        if($('body').hasClass('page-multiStepCheckoutSummaryPage')){
            var vpPath = '';
            var actionField = {};
            var step = $(".js-checkout-step.active").prop("id");
            if(step == 'step1') {
                vpPath = 'CheckOut - Step 1 Installation Location';
                actionField = {'step': vpPath};
            }else if(step == 'step2') {
                vpPath = 'CheckOut - Step 2 Payment and Billing Address';
                actionField = {'step': vpPath};
            }else if(step == 'step3') {
                vpPath = 'CheckOut - Step 3 Personal Details';
                actionField = {'step': vpPath, 'option': $(".productGtmDataPaymentMethod").attr('data-attr-cardtype')};
            }else if(step == 'step4') {
                vpPath = 'CheckOut - Step 4 Final Review';
                actionField = {'step': vpPath, 'option': $(".productGtmDataPaymentMethod").attr('data-attr-cardtype')};
            }
            window.dataLayer.push({
                'vpPath' : vpPath,
            });
            var products = [];
            $(".productGtmData").each(function(){
                products.push({
                    'name' : $(this).attr('data-attr-name'),
                    'id' : $(this).attr('data-attr-id'),
                    'price' : $(this).attr('data-attr-price').replace(/,/g, ""),
                    'brand' : $(this).attr('data-attr-brand'),
                    'category' : $(this).attr('data-attr-category'),
                    'category_2' : $(this).attr('data-attr-category_2'),
                    'quantity' : $(this).attr('data-attr-quantity')
                });
            });
            window.dataLayer.push({
                'event': 'checkout',
                'ecommerce': {
                    'checkout': {
                        'actionField': actionField,
                        'products': products
                    }
                }
            });
        }
        var errorCount = 0;
        $(".help-block span").each(function (){
        	errorCount++;
        });
        if(errorCount > 0){
        	var errorMsg = $("#gtmPersonalFormError").val();
        	window.dataLayer.push({
        		'event': 'error',
        		'error': errorMsg,
        		'error_category': 'User error',
        	});
        }
        /* Chekcout Page Ends */
        /* Order Confirmation Page Starts */
        if($('body').hasClass('page-orderConfirmationPage')){
            window.dataLayer.push({
                'vpPath' : 'Purchase',
            });
            var productsOrderConfirmationPage = [];
            $(".productGtmDataOrderConfirmationPage").each(function(){
                productsOrderConfirmationPage.push({
                    'name' : $(this).attr('data-attr-name'),
                    'id' : $(this).attr('data-attr-id'),
                    'price' : $(this).attr('data-attr-price').replace(/,/g, ""),
                    'brand' : $(this).attr('data-attr-brand'),
                    'category' : $(this).attr('data-attr-category'),
                    'category_2' : $(this).attr('data-attr-category_2'),
                    'quantity' : $(this).attr('data-attr-quantity')
                });
            });
            var actionField =  {'id': $("#orderNumber").attr('data-attr-order-id'), // Transaction ID.
                                 'affiliation': 'Hybris',
                                 'revenue': $("#orderTotalRevenue").attr('data-attr-orderTotalRevenue'), // Total transaction value
                               };
            window.dataLayer.push ({
                'event': 'purchase',
                'payment_method': $(".productGtmDataPaymentMethod").attr('data-attr-cardtype'),
                'currencyCode': $("#orderCurrencyType").attr('data-attr-orderCurrencyType'),
                'ecommerce': {
                    'purchase': {
                        'actionField': actionField,
                        'products': productsOrderConfirmationPage
                    }
                }
            });
        }
        /* Order Confirmation Page Ends */
        /* Page not Found Code Starts*/
        if($('body').hasClass('page-notFound')){
            var errorMsg = $("#gtmNotFoundPageError").val();
            window.dataLayer.push({
                'event': 'error',
                'error': errorMsg,
                'error_category': 'User error',
            });
        }
        /* Page not Found Code Ends*/
    /* Gtm for Checkout Steps and Purchase pages Ends */

    /* Adobe Analyetics Code for cart checkout button */
    $(".page-cartPage .js-continue-checkout-button").click(function (){
        var products = [];
        $(".gtmProductData").each(function(){
            var productInfo = {}, category = {};
            productInfo.productName = $(this).attr('data-attr-name');
            productInfo.price = $(this).attr('data-attr-price');
            productInfo.quantity = $(this).attr('data-attr-initialquantity');
            category.primaryCategory = $(this).attr('data-attr-category_2');
            category.subcategory = $(this).attr('data-attr-category');
            products.push({
                'productInfo': productInfo,
                'category': category
            });
        });
        _satellite.track('evClicCheckoutCartPage', { product: products });
    });

});
