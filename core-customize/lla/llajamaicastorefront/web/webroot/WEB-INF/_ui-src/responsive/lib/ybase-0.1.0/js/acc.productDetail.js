ACC.productDetail = {

    _autoload: [
        "initPageEvents",
        "bindVariantOptions",
        "bindDeviceProductPlans"
    ],


    checkQtySelector: function (self, mode) {
    	var $qtySelector = $(document).find(self).parents(".js-qty-selector");
        var input = $qtySelector.find(".js-qty-selector-input");
        var inputVal = parseInt(input.val());
        var max = input.data("max");
        var minusBtn = $qtySelector.find(".js-qty-selector-minus");
        var plusBtn = $qtySelector.find(".js-qty-selector-plus");

        $qtySelector.find(".btn").removeAttr("disabled");

        if (mode == "minus") {
            if (inputVal != 1) {
                ACC.productDetail.updateQtyValue(self, inputVal - 1)
                if (inputVal - 1 == 1) {
                    minusBtn.attr("disabled", "disabled")
                }

            } else {
                minusBtn.attr("disabled", "disabled")
            }
        } else if (mode == "reset") {
            ACC.productDetail.updateQtyValue(self, 1)

        } else if (mode == "plus") {
        	if(max == "FORCE_IN_STOCK") {
        		ACC.productDetail.updateQtyValue(self, inputVal + 1)
        	} else if (inputVal <= max) {
                ACC.productDetail.updateQtyValue(self, inputVal + 1)
                if (inputVal + 1 == max) {
                    plusBtn.attr("disabled", "disabled")
                }
            } else {
                plusBtn.attr("disabled", "disabled")
            }
        } else if (mode == "input") {
            if (inputVal == 1) {
                minusBtn.attr("disabled", "disabled")
            } else if(max == "FORCE_IN_STOCK" && inputVal > 0) {
            	ACC.productDetail.updateQtyValue(self, inputVal)
            } else if (inputVal == max) {
                plusBtn.attr("disabled", "disabled")
            } else if (inputVal < 1) {
                ACC.productDetail.updateQtyValue(self, 1)
                minusBtn.attr("disabled", "disabled")
            } else if (inputVal > max) {
                ACC.productDetail.updateQtyValue(self, max)
                plusBtn.attr("disabled", "disabled")
            }
        } else if (mode == "focusout") {
        	if (isNaN(inputVal)){
                ACC.productDetail.updateQtyValue(self, 1);
                minusBtn.attr("disabled", "disabled");
        	} else if(inputVal >= max) {
                plusBtn.attr("disabled", "disabled");
            }
        }

    },

    updateQtyValue: function (self, value) {
        var input = $(document).find(self).parents(".js-qty-selector").find(".js-qty-selector-input");
        var addtocartQty = $(document).find(self).parents(".addtocart-component").find("#addToCartForm").find(".js-qty-selector-input");
        var configureQty = $(document).find(self).parents(".addtocart-component").find("#configureForm").find(".js-qty-selector-input");
        input.val(value);
        addtocartQty.val(value);
        configureQty.val(value);
    },

    initPageEvents: function () {
        $(document).on("click", '.js-qty-selector .js-qty-selector-minus', function () {
            ACC.productDetail.checkQtySelector(this, "minus");
        })

        $(document).on("click", '.js-qty-selector .js-qty-selector-plus', function () {
            ACC.productDetail.checkQtySelector(this, "plus");
        })

        $(document).on("keydown", '.js-qty-selector .js-qty-selector-input', function (e) {

            if (($(this).val() != " " && ((e.which >= 48 && e.which <= 57 ) || (e.which >= 96 && e.which <= 105 ))  ) || e.which == 8 || e.which == 46 || e.which == 37 || e.which == 39 || e.which == 9) {
            }
            else if (e.which == 38) {
                ACC.productDetail.checkQtySelector(this, "plus");
            }
            else if (e.which == 40) {
                ACC.productDetail.checkQtySelector(this, "minus");
            }
            else {
                e.preventDefault();
            }
        })

        $(document).on("keyup", '.js-qty-selector .js-qty-selector-input', function (e) {
            ACC.productDetail.checkQtySelector(this, "input");
            ACC.productDetail.updateQtyValue(this, $(this).val());

        })
        
        $(document).on("focusout", '.js-qty-selector .js-qty-selector-input', function (e) {
            ACC.productDetail.checkQtySelector(this, "focusout");
            ACC.productDetail.updateQtyValue(this, $(this).val());
        })

        $("#Size").change(function () {
            changeOnVariantOptionSelection($("#Size option:selected"));
        });

        $("#variant").change(function () {
            changeOnVariantOptionSelection($("#variant option:selected"));
        });

        $(".selectPriority").change(function () {
            window.location.href = $(this[this.selectedIndex]).val();
        });

        function changeOnVariantOptionSelection(optionSelected) {
            window.location.href = optionSelected.attr('value');
        }
    },

    bindVariantOptions: function () {
        ACC.productDetail.bindCurrentStyle();
        ACC.productDetail.bindCurrentSize();
        ACC.productDetail.bindCurrentType();
    },

    bindCurrentStyle: function () {
        var currentStyle = $("#currentStyleValue").data("styleValue");
        var styleSpan = $(".styleName");
        if (currentStyle != null) {
            styleSpan.text(": " + currentStyle);
        }
    },

    bindCurrentSize: function () {
        var currentSize = $("#currentSizeValue").data("sizeValue");
        var sizeSpan = $(".sizeName");
        if (currentSize != null) {
            sizeSpan.text(": " + currentSize);
        }
    },

    bindCurrentType: function () {
        var currentSize = $("#currentTypeValue").data("typeValue");
        var sizeSpan = $(".typeName");
        if (currentSize != null) {
            sizeSpan.text(": " + currentSize);
        }
    },
    bindDeviceProductPlans:function(){
         $("input:radio[name='deviceprice_radio']").click(function(){
          var categoryCode = ($(this).val().toLowerCase());
          if(categoryCode !== "nooffer"){
          $("#addToCartFormWithOffer input[id=plantype]").val(categoryCode);
            $.ajax({
                    url: "./categoryProductJsonData/"+categoryCode,
                    cache: true,
                    type: 'GET',
                    dataType: 'json',
                    success: function(jsonData){
                       renderPage(jsonData);
                    },
                    failure: function(error){
                        console.error("error: "+error);
                    }
                });
                const renderPage = function(jsonData)
                {
                     var planHeadline = $("#planHeadline_text").data("localizedText");
                     let productGrid = '<h3>'+planHeadline+'</h3>';
                     jsonData.forEach(function(data){
                        productGrid += '<div class="plan-info clearfix"><label class="col-xs-4"><input type="radio" name="plans_radio">';

                         if(data.productOfferingPrices !== null && Object.keys(data.productOfferingPrices).length && data.productOfferingPrices !== "undefined"){
                            if(data.productOfferingPrices[0].recurringChargeEntries !== null && Object.keys(data.productOfferingPrices[0].recurringChargeEntries).length && data.productOfferingPrices.recurringChargeEntries !== "undefined"){
                                productGrid += data.productOfferingPrices[0].recurringChargeEntries[0].price.formattedValue;
                            }
                        }
                        productGrid += '<input type="hidden" name="planProductCode" id="planProductCode" value='+data.code+'></label><div class="col-xs-8">'+data.name+'</div></div>';
                     });
                     $('#plans_details').html(productGrid);
                }
            }});
            $('input:radio[name="deviceprice_radio"]').change(function(){
                if ($(this).is(':checked')) {
                    var radioValue = $(this).val();
                    if(radioValue == "NOOFFER") {
                        $("#addToCartFormWithoutOffer .buynow-btn").show();
                        $("#addToCartFormWithOffer .buynow-btn").hide();
                        $("#plans_details").empty();
                    }
                    else {
                        $(".buynow-btn").hide();
                    }
                }
            });
            $("#plans_details").on("change", "input[name=plans_radio]:radio", function(){
                if ($(this).is(':checked')) {
                    var plan = $(this).next("input").val();
                    $("#addToCartFormWithOffer input[id=plan]").val(plan);
                    $("#addToCartFormWithOffer .buynow-btn").show();
                }
            });
          }
  }