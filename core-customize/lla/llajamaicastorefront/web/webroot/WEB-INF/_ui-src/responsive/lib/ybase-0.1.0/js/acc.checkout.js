ACC.checkout = {

	_autoload: [
		"bindCheckO",
		"bindForms",
		"bindSavedPayments"
	],


	bindForms:function(){

		$(document).on("click","#addressSubmit",function(e){
			e.preventDefault();
			localStorage.setItem("jamaica-line-1", $('[id="address.townCity"]').val());
            localStorage.setItem("jamaica-line-2", $('[id="address.line2"]').val());
            localStorage.setItem("jamaica-line-3", $('[id="address.line1"]').val());
            localStorage.setItem("jamaica-line-4", $('select[id^="address.area"] option:selected').val());
			$('#addressForm').submit();
		})

		$(document).on("click","#deliveryMethodSubmit",function(e){
			e.preventDefault();
			$('#selectDeliveryMethodForm').submit();
		})

	},

	bindSavedPayments:function(){
		$(document).on("click",".js-saved-payments",function(e){
			e.preventDefault();

			var title = $("#savedpaymentstitle").html();

			$.colorbox({
				href: "#savedpaymentsbody",
				inline:true,
				maxWidth:"100%",
				opacity:0.7,
				//width:"320px",
				title: title,
				close:'<span class="glyphicon glyphicon-remove"></span>',
				onComplete: function(){
				}
			});
		})
	},

	bindCheckO: function ()
	{
		var cartEntriesError = false;

		// Alternative checkout flows options
		$('.doFlowSelectedChange').change(function ()
		{
			if ('multistep-pci' == $('#selectAltCheckoutFlow').val())
			{
				$('#selectPciOption').show();
			}
			else
			{
				$('#selectPciOption').hide();

			}
		});



		$('.js-continue-shopping-button').click(function ()
		{
			var checkoutUrl = $(this).data("continueShoppingUrl");
			window.location = checkoutUrl;
		});

		$('.js-create-quote-button').click(function ()
		{
			$(this).prop("disabled", true);
			var createQuoteUrl = $(this).data("createQuoteUrl");
			window.location = createQuoteUrl;
		});


		$('.expressCheckoutButton').click(function()
				{
					document.getElementById("expressCheckoutCheckbox").checked = true;
		});

		$(document).on("input",".confirmGuestEmail,.guestEmail",function(){

			  var orginalEmail = $(".guestEmail").val();
			  var confirmationEmail = $(".confirmGuestEmail").val();

			  if(orginalEmail === confirmationEmail){
			    $(".guestCheckoutBtn").removeAttr("disabled");
			  }else{
			     $(".guestCheckoutBtn").attr("disabled","disabled");
			  }
		});

		$('.js-continue-checkout-button').click(function ()
		{
			var checkoutUrl = $(this).data("checkoutUrl");

			cartEntriesError = ACC.pickupinstore.validatePickupinStoreCartEntires();
			if (!cartEntriesError)
			{
				var expressCheckoutObject = $('.express-checkout-checkbox');
				if(expressCheckoutObject.is(":checked"))
				{
					window.location = expressCheckoutObject.data("expressCheckoutUrl");
				}
				else
				{
					var flow = $('#selectAltCheckoutFlow').val();
					var customerLoggedIn=false;
                     if ( $('#userInSession').val() == "true" ) {
                          customerLoggedIn=true;
                     }
                    if ( (flow == undefined || flow == '' || flow == 'select-checkout') && customerLoggedIn == false )
                    {
                        var popupHeadLine =  $("#login-popup").prev(".headline").html();
                        $.colorbox({
                            inline:true,
                            scrolling:true,
                            href:"#login-popup",
                            maxWidth:"100%",
                            width:"600px",
                            //href: $(this).attr("href"),
                            close:'<span class="close-popup"></span>',
                            title: popupHeadLine,
                            onComplete: function() {
                                $("#cboxTitle").css({
                                    "padding": "0"
                                });
                                $("#cboxLoadedContent").css({
                                    "margin-top": "50px"
                                });
                                ACC.common.refreshScreenReaderBuffer();
                            },
                            onClosed: function() {
                                ACC.common.refreshScreenReaderBuffer();
                            }
                        });
                        $(".login-popup-close").on("click", function() {
                            $.colorbox.close();
                        })
                    }
                    else
                    {
                      window.location = checkoutUrl;
                    }
				}
			}
			return false;
		});

	}

};

$(document).ready(function () {

	
        localStorage.setItem("jamaica-checkoutEmail", $("#guestEmail").val());
		localStorage.setItem("jamaica-checkoutPhonenumber", $("#mobilePhone").val());
        if (localStorage.getItem('jamaica-userAccountNumber') !== null ) {
            $('[id="jamaica.register.accountNumber"]').val(localStorage.getItem('jamaica-userAccountNumber'));
        }
    

    if($('.page-multiStepCheckoutSummaryPage').length > 0 && $(".js-checkout-step.active").prop("id") == 'step2') {
       if (localStorage.getItem('jamaica-line-1') !== null ) {
           $('[id="address.townCity"]').val(localStorage.getItem('jamaica-line-1'));
           $('[id="address.line2"]').val(localStorage.getItem('jamaica-line-2'));
           $('[id="address.line1"]').val(localStorage.getItem('jamaica-line-3'));
           $('select[name^="area"] option:selected').attr("selected",null);
           $('select[name^="area"] option[value='+localStorage.getItem("jamaica-line-4")+']').attr("selected","selected");
       }
   }

   $("#customerProfileForm .checkout-next").click(function(){
       localStorage.setItem("jamaica-userAccountNumber", $('[id="jamaica.register.accountNumber"]').val());
   });

   $("#placeOrder").click(function(){
        if( $("#Terms1").is(':checked') && $("#Terms2").is(':checked')){
            localStorage.removeItem('jamaica-line-1');
            localStorage.removeItem('jamaica-line-2');
            localStorage.removeItem('jamaica-line-3');
            localStorage.removeItem('jamaica-line-4');
            localStorage.removeItem('jamaica-checkoutEmail');
            localStorage.removeItem('jamaica-checkoutPhonenumber');
            localStorage.removeItem('jamaica-userAccountNumber');
        }
   });

	$("#customerProfileForm").on('keypress',"input[name='firstName'], input[name='lastName']", function(e){
		var regex = /^[a-zA-Z ]*$/;
		return regex.test(e.key )
	});
	$("#customerProfileForm").on('keypress',"input[name='trnNumber'],input[name='fixedPhone']", function(e){
		const pattern = /^[0-9]$/;
		return pattern.test(e.key )
	});
	$("input[name='trnNumber']").on('keydown', function() {
		$(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{3})+$/, "$1-$2-$3"));
	});
	if ($("#customerProfileForm_error").length) {
		var mobilePhone_error = $("#mobilePhone_error").val();
		var firstName_error = $("#firstName_error").val();
		var lastName_error = $("#lastName_error").val();
		var trnNumber_error = $("#trnNumber_error").val();
	}
	$("#customerProfileForm").validate({
		rules: {
			"firstName": {
				required: true,
			},
			"lastName": {
				required: true,
			},
			"trnNumber": {
				required: true,
			}
		},
		messages: {
			firstName: firstName_error,
			lastName: lastName_error,
			trnNumber: trnNumber_error,
		},
		invalidHandler: function() {
        	 window.dataLayer.push({
        		 'event': 'error',
        		 'description': "User needs to fill all the required fields in Checkout Step 1 Personal Details form",
        	 });
        }
	});

    if ($("#address_validation_error").length) {
		var townCity_error = $("#townCity_error").val();
		var area_error = $("#area_error").val();
		var line2_error = $("#line2_error").val();
		var line1_error = $("#line1_error").val();
	}
    $("#addressForm").validate({
		rules: {
			"townCity" : {
				required: true,
			},
			"area" : {
				required: true,
			},
			"line2": {
				required: true,
			},
			"line1": {
				required: true,
			}
		},
		messages: {
			townCity: townCity_error,
			area: area_error,
			line2: line2_error,
			line1: line1_error,
		},
		invalidHandler: function() {
        	var errorMsg = $("#gtmAddressFormError").val();
        	 window.dataLayer.push({
        		 'event': 'error',
        		 'description': "User needs to fill all the required fields in Checkout Step 2 Address form",
        	 });
        }
	});

/*	if ($("#placeOrderForm1").length) {
		var termsCheck2_error = $("#termsCheck2_error").val();
	}
	$("#placeOrderForm1").validate({
		rules: {
			"termsCheck1" : {
				required: true,
			},
			"termsCheck2" : {
				required: true,
			}
		},
		messages: {
			termsCheck2: termsCheck2_error,
		}
	});*/

    /* Adobe Analyetics Code for cart checkout button */
    $(".page-cartPage .js-continue-checkout-button").click(function (){
        var products = [];
        $(".gtmProductData").each(function(){
            var productInfo = {}, category = {};
            productInfo.productName = $(this).attr('data-attr-name');
            productInfo.price = $(this).attr('data-attr-price');
            productInfo.quantity = $(this).attr('data-attr-initialquantity');
            category.primaryCategory = $(this).attr('data-attr-category_2');
            category.subcategory = $(this).attr('data-attr-category');
            products.push({
                'productInfo': productInfo,
                'category': category
            });
        });
        _satellite.track('evClicCheckoutCartPage', { product: products });
    });

});

$(document).ready(function(){
	//Mobile number format display in personal details
	$("input[name='phone'], input[name='phone2']").on('keyup', function() {
		$(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d)+$/, "($1)$2-$3"));
	});

	$(window).on('load', function(){
		$("input[name='phone'], input[name='phone2']").each(function() {
			$(this).val($(this).val().replace(/(\d{3})(\d{3})(\d{4})/, "($1)$2-$3"));
		});
	});

	//Guest checkout fields validation error messages handle in cart page.
	if ($("#guestCheckout").length) {
        $("#guestCheckout").on('keypress',"input[name='mobilePhone']", function(e){
            const pattern = /^[0-9]$/;
            return pattern.test(e.key )
        });
        var guestEmail_error = $("#guestEmail_error").val();
		var guestEmail_valid_error = $("#guestEmail_valid_error").val();
        var guestPhone_error = $("#guestPhone_error").val();
		var guestPhone_length_error = $("#guestPhone_length_error").val();
        $.validator.addMethod("regx", function(value, element, regexpr) {
            if( regexpr.test(value)){
              return true;
            }
            else{
              return false;
            }
        }, guestEmail_error);
        $("#guestCheckout").validate({
            rules: {
                "guestEmail": {
                    required: true,
                    regx: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
					email: true
                },
                "mobilePhone": {
                    required: true,
					minlength: 14
                }
            },
            messages: {
                guestEmail:{
					required:guestEmail_error,
					email:guestEmail_valid_error
				},
                mobilePhone:{
					required:guestPhone_error,
					minlength:guestPhone_length_error
				},
            },
            invalidHandler: function() {
                if (typeof googleAnalyticsTagManager.loginFormGtmError === "function") {
                    googleAnalyticsTagManager.loginFormGtmError();
                }
            }
        });
    }


    $("input[name='fixedPhone']").on('keyup input', function() {
		var number = $(this).val().replace(/[^\d]/g, '')
	    if(number.length == 7) {
	      number = number.replace(/(\d{3})(\d{4})/, "$1 $2");
	    } else if (number.length == 10) {
	      number = number.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2 $3");
    	}
		$(this).val(number)
	}).attr('maxlength','14').attr('minlength','14');

});
