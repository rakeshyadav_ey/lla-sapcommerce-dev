ACC.payment = {
		
		activateSavedPaymentButton: function(){

			$(document).on("click",".js-saved-payments",function(e){
				e.preventDefault();
				
				var titleHtml = $("#savedpaymentstitle").html();
				
				$.colorbox({
					href: "#savedpaymentsbody",
					inline:true,
					maxWidth:"100%",
					opacity:0.7,
					width:"320px",
					title: titleHtml,
					close:'<span class="glyphicon glyphicon-remove"></span>',
					onComplete: function(){
					}
				});
			})
		},
		bindPaymentCardTypeSelect: function ()
		{
			ACC.payment.filterCardInformationDisplayed();
			$("#card_cardType").change(function ()
			{
				var cardType = $(this).val();
				if (cardType == '024')
				{
					$('#startDate, #issueNum').show();
				}
				else
				{
					$('#startDate, #issueNum').hide();
				}
			});
		},
		filterCardInformationDisplayed: function ()
		{
			var cardType = $('#card_cardType').val();
			if (cardType == '024')
			{
				$('#startDate, #issueNum').show();
			}
			else
			{
				$('#startDate, #issueNum').hide();
			}
		},
		clickCapturePayment:function (){
		 $(document).on('click','#capture-payment', function (e){
         					var paymentCaptureUrl=$('#capture-payment').val();
                	     	var orderCode=$('#order-code').val();
						    var options = {
								'order': orderCode
			
							};
                			var capturePaymentUrl=paymentCaptureUrl;
                			$.ajax({
                			url: capturePaymentUrl,
                			cache: true,
                			type: 'GET',
							data:options,
							dataType: 'json',
                			success: function(jsonData){

                				alert("Payment Captured");
                			}
							
                		});
         		});

		},
		saveCVVInputForSavedCards:function(){
		 $('.page-multiStepCheckoutSummaryPage').on('keyup', '.saved_card_cvn', function(event) {
                    event.preventDefault();
                    var cvvNumber = $(this).val();
                    var cardType = $('#cardType').val();
                    if (cardType == 'amex' || cardType == 'Amex') {
                        $('.saved_card_cvn').attr("maxlength", "4");
                      } else {
                        $('.saved_card_cvn').attr("maxlength", "3");

                    }
                    if ((cvvNumber.length == 4 && cardType == 'Amex') || (cvvNumber.length == 3 && cardType != 'Amex')) {
                        ACC.payment.saveCVVIntoCart(cvvNumber);
                    }
                });
		},
		saveCVVIntoCart: function(cvvNumber, callback) {
                $.ajax({
                    url: ACC.config.encodedContextPath + '/checkout/multi/payment-method/saveCVV',
                    async: true,
                    type: "POST",
                    dataType: "html",
                    data: {
                        cvvNumber: cvvNumber
                    },
                    success: function(data) {}
                })
                return false;
            }
}

$(document).ready(function () {
	ACC.payment.activateSavedPaymentButton();
	ACC.payment.bindPaymentCardTypeSelect();
	ACC.payment.clickCapturePayment();
	ACC.payment.saveCVVInputForSavedCards();
	
	//payment method validation
	$('#card_accountNumber').attr('maxlength','19');
	$('#card_cvNumber').attr({'minlength':'3','maxlength':'3'});
	$('#card_accountNumber, #card_cvNumber').on('keypress',function(e){
		const pattern = /^[0-9]$/;
        return pattern.test(e.key )
	});
	
	if ($("#silentOrderPostForm_error").length) { 
		var card_cardType_error = $("#card_cardType_error").val();
		var card_accountNumber_error = $("#card_accountNumber_error").val();
		var card_cvNumber_error = $("#card_cvNumber_error").val();
		var card_expirationMonth_error = $("#card_expirationMonth_error").val();
		var card_expirationYear_error = $("#card_expirationYear_error").val();
	}
	$("#silentOrderPostForm").validate({
		rules: {
			/*"card_cardType": {
                required: true,
			},*/
			"card_accountNumber": {
				required: true,
				minlength: 18,
				maxlength: 19
			},
			"card_cvNumber": {
				required: true,
				minlength: 3
			},
			"card_expirationMonth": {
        		required: true,
        	},
        	"card_expirationYear": {
                required: true,
			}
		},
		messages: {
			card_cardType: card_cardType_error,
			card_accountNumber: card_accountNumber_error,
			card_cvNumber: card_cvNumber_error,
			card_expirationMonth: card_expirationMonth_error,
			card_expirationYear: card_expirationYear_error,
		},
		invalidHandler: function() {
            window.dataLayer.push({
                'event': 'error',
                'description': "User needs to fill all the required fields in Checkout Step 3 Payment form",
            });
        }
	});

	/* CVV number validation on popup */
	$(".checkout-card-cvv-error").hide();
    $(".saved-card-button").click(function(e){
        e.preventDefault();
        var cvv = $(this).parent().find(".saved_card_cvn").val();
        if(cvv == '' || cvv.length != 3){
            $(this).parent().find("#checkout-card-cvv-error").show();
        }else{
            $(this).closest('form').submit();
        }
    });
    $(".saved_card_cvn").keyup(function(){
      $(this).closest('form').find("#checkout-card-cvv-error").hide();
    });

    $(".saved_card_cvn").on('keypress', function(e){
        const pattern = /^[0-9]$/;
        return pattern.test(e.key )
    });

	//credit card number validation	
	$('#card_accountNumber').on('keypress change blur', function () {
	  $(this).val(function (index, value) {
		
	    return value.replace(/[^0-9]+/gi, '').replace(/(.{4})/g, '$1 ').trim();
	  });
	  $('#card_cvNumber').val('');
	});
	
	$('#card_accountNumber').on('copy cut paste', function () {
	  setTimeout(function () {
	    $('#card_accountNumber').trigger("change");
	  });
	});

	function getCCLabel(ccNumberMask) {
		
	    var creditCardsRegex = {
	        visa: /^4[0-9]{4}(?:[0-9]{11})?/,
	        master: /^5[1-5][0-9]{3}/,
	        //amex: /^3[47][0-9]{4}/,
	        //discover: /^6(?:011|5[0-9]{2})[0-9]{4}/,
	        //maestro: /^(5018|5081|5044|5020|5038|603845|6304|6759|676[1-3]|6799|6220|504834|504817|504645)[0-9]{8,15}$/
	    },
	        creditCards = [
	            'visa',
	            'master',
	            //'amex',
	            //'discover',
	            //'maestro'   
	        ],
	        regexes = [],
	        ccNumber = ccNumberMask.replace(/\s+|-/g, '');
	
	    creditCards.map((cc) => {
	        var ccRegex = {
	            regex: creditCardsRegex[cc],
	            label: cc
	        };
	        regexes.push(ccRegex);
	    });
	
	    for (var i = 0; i < regexes.length; i++) {
	        if (regexes[i].regex.test(ccNumber)) return regexes[i].label;
	    }
	    return false;
	}
	
	(function ($) {
		var card_cvNumber_error = $("#card_cvNumber_error").val();
	    $.cccheck = function cccheck(settings) {
	        var config = {
	            // Config local
	            'inputCCNumber': '#card_accountNumber',
	            'elementShowLabel': '#show-cc-label'
	        },
	        nodeNumber = $(config.inputCCNumber),
	        nodeLabel = $(config.elementShowLabel);
	
	        if (settings) { 
	            settings = jQuery.extend(config, settings); 
	        }
			
			nodeLabel.html('<i class="i-icon-payment"></i>');
	        nodeNumber.keyup(function (e) {
	            var cardLabel = getCCLabel(nodeNumber.val());
	            nodeLabel.html('<i class="i-icon-payment i-icon-card-' + cardLabel + '"></i>');
				$('#card_cardType').val(cardLabel);
				$('#card_cvNumber').attr({'maxlength':'3'});
				$("input[name='card_cvNumber']").rules('add', {
					minlength:3,
					messages: {
						minlength: card_cvNumber_error
					}
	            });
	        });
	
	        return this;
	    };
	})(jQuery);

	$.cccheck();

	$('#card_accountNumber').on('keyup change', function () {
	  $(this).val(function (index, value) {
	    return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ').trim();
	  });
	});

});
	
	
	
