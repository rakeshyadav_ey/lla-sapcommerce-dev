ACC.cartitem = {

	_autoload: [
		"bindCartItem"
	],

	submitTriggered: false,

	bindCartItem: function ()
	{

		$('.js-execute-entry-action-button').on("click", function ()
		{
			var entryAction = $(this).data("entryAction");
			var entryActionUrl =  $(this).data("entryActionUrl");
			var entryProductCode =  $(this).data("entryProductCode");
			var entryInitialQuantity =  $(this).data("entryInitialQuantity");
			var actionEntryNumbers =  $(this).data("actionEntryNumbers");

			if(entryAction == 'REMOVE')
			{
				ACC.track.trackRemoveFromCart(entryProductCode, entryInitialQuantity);
			}

			var cartEntryActionForm = $("#cartEntryActionForm");
			var entryNumbers = actionEntryNumbers.toString().split(';');
			entryNumbers.forEach(function(entryNumber) {
				var entryNumbersInput = $("<input>").attr("type", "hidden").attr("name", "entryNumbers").val(entryNumber);
				cartEntryActionForm.append($(entryNumbersInput));
			});
			cartEntryActionForm.attr('action', entryActionUrl).submit();
		});

		$('.qtyplus').on("click", function (e)
		{
		    var product = [];
            product.push({
                'name' : $(this).parent().find('.gtmProductData').attr('data-attr-name'),
                'id' : $(this).parent().find('.gtmProductData').attr('data-attr-id'),
                'price' : $(this).parent().find('.gtmProductData').attr('data-attr-price').replace(/,/g, ""),
                'brand' : $(this).parent().find('.gtmProductData').attr('data-attr-brand'),
                'category' : $(this).parent().find('.gtmProductData').attr('data-attr-category'),
                'category_2' : $(this).parent().find('.gtmProductData').attr('data-attr-category_2'),
                'quantity' : parseInt($(this).parent().find('.gtmProductData').attr('data-attr-initialQuantity')) + 1
            });
            var currencyCode = $(this).parent().find('.gtmProductData').attr('data-attr-currencyCode');
            window.dataLayer.push({
                'event': 'addToCart',
                'ecommerce': {
                    'currencyCode': currencyCode,
                    'add': {
                        'products': product
                    }
                }
            });
			ACC.cartitem.handleUpdateQuantityPlus(this, e);
		});

		$('.qtyminus').on("click", function (e)
		{
		    var qty = parseInt($(this).parent().find('.gtmProductData').attr('data-attr-initialQuantity')) - 1;
            if(qty == 0) { qty = 1; }
            var product = [];
            product.push({
                'name' : $(this).parent().find('.gtmProductData').attr('data-attr-name'),
                'id' : $(this).parent().find('.gtmProductData').attr('data-attr-id'),
                'price' : $(this).parent().find('.gtmProductData').attr('data-attr-price').replace(/,/g, ""),
                'brand' : $(this).parent().find('.gtmProductData').attr('data-attr-brand'),
                'category' : $(this).parent().find('.gtmProductData').attr('data-attr-category'),
                'category_2' : $(this).parent().find('.gtmProductData').attr('data-attr-category_2'),
                'quantity' : qty
            });
            window.dataLayer.push({
                'event': 'removeFromCart',
                'ecommerce': {
                    'remove': {
                        'products': product
                    }
                }
            });
			ACC.cartitem.handleUpdateQuantityMinus(this, e);
		});

		$('.js-update-entry-quantity-input').on("blur", function (e){
			ACC.cartitem.handleUpdateQuantity(this, e);
		}).on("keyup", function (e){
			return ACC.cartitem.handleKeyEvent(this, e);
		}).on("keydown", function (e){
			return ACC.cartitem.handleKeyEvent(this, e);
		});
	},

	handleKeyEvent: function (elementRef, event)
	{
		//console.log("key event (type|value): " + event.type + "|" + event.which);

		if (event.which == 13 && !ACC.cartitem.submitTriggered)
		{
			ACC.cartitem.submitTriggered = ACC.cartitem.handleUpdateQuantity(elementRef, event);
			return false;
		}
		else 
		{
			// Ignore all key events once submit was triggered
			if (ACC.cartitem.submitTriggered)
			{
				return false;
			}
		}

		return true;
	},
	
	handleUpdateQuantity: function (elementRef, event)
	{

		var form = $(elementRef).closest('form');
		
		var productCode = form.find('input[name=productCode]').val();
		var initialCartQuantity = form.find('input[name=initialQuantity]').val();
		var newCartQuantity = form.find('input[name=quantity]').val();

		if(initialCartQuantity != newCartQuantity)
		{
			ACC.track.trackUpdateCart(productCode, initialCartQuantity, newCartQuantity);
			form.submit();

			return true;
		}

		return false;
	},
	
	handleUpdateQuantityPlus: function (elementRef, event)
	{

		var form = $(elementRef).closest('form');
		
		var productCode = form.find('input[name=productCode]').val();
		var initialCartQuantity = parseInt(form.find('input[name=initialQuantity]').val());
		var currentVal = parseInt(form.find('input[name=quantity]').val());
		var newCartQuantity = parseInt(form.find('input[name=quantity]').val(currentVal+1));

		if(initialCartQuantity != newCartQuantity)
		{
			ACC.track.trackUpdateCart(productCode, initialCartQuantity, newCartQuantity);
			form.submit();

			return true;
		}

		return false;
	},
	
	handleUpdateQuantityMinus: function (elementRef, event)
	{

		var form = $(elementRef).closest('form');
		
		var productCode = form.find('input[name=productCode]').val();
		var initialCartQuantity = parseInt(form.find('input[name=initialQuantity]').val());
		var currentVal = parseInt(form.find('input[name=quantity]').val());
		var newCartQuantity = parseInt(form.find('input[name=quantity]').val(currentVal-1));
		
		if(initialCartQuantity != newCartQuantity)
		{
			ACC.track.trackUpdateCart(productCode, initialCartQuantity, newCartQuantity);
			form.submit();

			return true;
		}

		return false;
	}
};

$(document).ready(function() {
    //enable comment for this item only
    $('.js-entry-comment-button').click(function(event) {
        event.preventDefault();
        var linkID = $(this).attr('href');
        $( linkID ).toggleClass('in');
        $( thisDetailGroup ).removeClass('open');
    });
});
