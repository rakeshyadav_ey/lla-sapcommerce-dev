googleAnalyticsTagManager = {

    pageLoadGoogleAnalyticsData:function(){
        window.dataLayer = window.dataLayer || [];

        var pageName = $("#adobeAnalyticsPageInfo").attr('data-attr-pagename');
        if(pageName == '' || pageName == undefined){ pageName = ''; }

        var breadCrumbs = $("#adobeAnalyticsPageInfo").attr('data-attr-breadcrumbsdata');
        if($('body').hasClass('page-orderConfirmationPage')){
            breadCrumbs = $("#adobeAnalyticsPageInfo").attr('data-attr-pageName');
        }
        if(breadCrumbs == '' || breadCrumbs == undefined || breadCrumbs == 'N/A'){ breadCrumbs = ''; } else { breadCrumbs = breadCrumbs.split(","); }

        var primaryCategory = $("#adobeAnalyticsPageInfo").attr('data-attr-pagename');
        if(primaryCategory == '' || primaryCategory == undefined){ primaryCategory = ''; }

        var subCategory = "";

        if($('body').hasClass('page-cartPage') || $('body').hasClass('page-multiStepCheckoutSummaryPage') || $('body').hasClass('page-orderConfirmationPage')){
            primaryCategory = $("#adobeAnalyticsPageInfo").attr('data-attr-pagename');
        }

        if(pageName == 'PLP'){
            pageName = window.location.pathname+"/"+subCategory;
        }else {
            pageName = window.location.pathname;
        }

        var statusLogged = 'Logged';
        var mail = "true";
        var profileID = $("#adobeAnalyticsPageInfo").attr('data-attr-userid');
        if(profileID == '' || profileID == undefined){ profileID = ''; statusLogged = 'Not Logged'; mail = "flase"; }

        var encryptedEmail1 = '';
        var email = $("#adobeAnalyticsPageInfo").attr('data-attr-email');
        if(email != '' && email != undefined && email != 'anonymous'){ encryptedEmail1 = sha256Converter(email); }

        var pageType = '';
        if($('body').hasClass('pageLabel-residential')){
            pageType = 'PLP';
        }else if($('body').hasClass('page-cartPage')) {
            pageType = 'Cart';
        }else if ($('body').hasClass('page-multiStepCheckoutSummaryPage')){
            pageType = 'Checkout';
        }else if ($('body').hasClass('page-orderConfirmationPage')){
            pageType = 'OrderConfirmation';
        }else if($('body').hasClass('page-doItBundlePage')){
            pageType = 'PLP';
            primaryCategory = 'FMC';
            subCategory = 'Internet-TV-Telephone-PlanOnly';
            breadCrumbs = '';
        }else if($('body').hasClass('page-mobilePage')){
            pageType = 'PLP';
            primaryCategory = 'Mobile';
            subCategory = 'Sim Only';
            breadCrumbs = '';
        }else if($('body').hasClass('page-storefinderPage')){
            pageType = 'Find Store';
            primaryCategory = '';
            subCategory = '';
        }else if($('body').hasClass('page-bundle')){
            pageType = 'PLP';
            primaryCategory = 'Fixed';
            subCategory = 'Internet-TV-Telephone-PlanOnly';
        }

        if($('body').hasClass('page-productDetails') && $("#adobeAnalyticsPageInfo").attr('data-attr-vpPath') == 'Mobile Services'){
            pageType = 'PDP';
            primaryCategory = 'Mobile';
            subCategory = 'Sim Only';
        }else if ($('body').hasClass('page-productDetails') && $("#adobeAnalyticsPageInfo").attr('data-attr-vpPath') == 'bundles') {
            pageType = 'PDP';
            primaryCategory = 'FMC';
            subCategory = 'Internet-TV-Telephone-PlanOnly';
        }

        var userType = $("#adobeAnalyticsPageInfo").attr('data-attr-customerType');
        if(userType == '' || userType == undefined){ userType = '';}

        var phone = $("#adobeAnalyticsPageInfo").attr('data-attr-mobilephone');
        var encryptedPhone = '';
        if(phone != '' && phone != undefined){ encryptedPhone = sha256Converter(phone); }

        var categoryInfo={}, profileInfo={}, socialInfo={};
        var productPage= '';//dynamic value, but this will be product name in PDP, remaining pages will be NA
        if($("#adobeAnalyticsPageInfo").attr('data-attr-pagename') == 'PRODUCT'){
            productPage = $("#adobeAnalyticsPageInfo").attr('data-attr-name');
        }

        if(!$('body').hasClass('page-orderConfirmationPage')){
            window.dataLayer.push({
                'event': 'vpv',
                'page': {
                    'pageName': pageName,
                    'breadCrumbs': breadCrumbs,
                    'pageType': pageType,
                    'country': 'Jamaica',
                    'channel': 'Ecommerce',
                    'productPage': productPage
                },
                'category':{
                    'businessCategory': 'Residential',
                    'primaryCategory': primaryCategory,
                    'subCategory': subCategory
                },
                'user':{
                    'statusLogged': statusLogged,//'logged status from the user', //Dynamic Value
                    'userType': userType,//'guest or client', //Dynamic Value
                    'profileInfo': {
                        'profileID': profileID,
                        'email': encryptedEmail1,
                        'phone': encryptedPhone
                    },
                    'social': {
                        'mail': mail,
                        'facebook': 'false',
                        'other': 'false'
                    }
                },
             });
         }

        /* Chekcout Page Starts */
        if($('body').hasClass('page-multiStepCheckoutSummaryPage')){
            var prodGtmData = $(".productGtmData");
        	var actionField = {};
            var stepInfo = '', option='';
            var step = $(".js-checkout-step.active").prop("id");
            if(step == 'step1') {
                stepInfo = 1;
                option="Personal Information";
            }else if(step == 'step2') {
                stepInfo = 2;
                option="Address";
            }else if(step == 'step3') {
                stepInfo = 3;
                option="Secure Payment";
            }else if(step == 'step4') {
                stepInfo = 4;
                option="Detail";
                prodGtmData = $(".checkout-order-summary.monthly-details").find(".productGtmData");
            }
            var products = [];
            prodGtmData.each(function(){
                var productId = $(this).attr('data-attr-id');
                var category = getPrimarySecondaryCategory(productId);
                products.push({
                    'name' : $(this).attr('data-attr-name'),
                    'id' : $(this).attr('data-attr-id'),
                    'price' : $(this).attr('data-attr-price').replace(/,/g, ""),
                    'brand' : 'DiscoverFlow',
                    'category' : category,
                    'variant': '',
                    'quantity' : $(this).attr('data-attr-quantity')
                });
            });
            window.dataLayer.push({
                'event': 'checkout',
                'ecommerce': {
                    'checkout': {
                        'actionField': {'step': stepInfo, 'option': option},
                        'products': products
                    }
                }
            });
            var errorCount = 0;
            $(".help-block span").each(function (){
                errorCount++;
            });
            if(errorCount > 0){
                var errorMsg = '';
                if($(".js-checkout-step.active").prop("id") == 'step1'){ errorMsg = 'User needs to fill all the required fields in Checkout Step 1 Personal Details form'; }
                if($(".js-checkout-step.active").prop("id") == 'step2'){ errorMsg = 'User needs to fill all the required fields in Checkout Step 2 Address form'; }
                if($(".js-checkout-step.active").prop("id") == 'step3'){ errorMsg = 'User needs to fill valid card deatils in Checkout Step 3 Payment form'; }
                window.dataLayer.push({
                    'event': 'error',
                    'description': errorMsg,
                });
            }
        }
        /* Chekcout Page Ends */

        /* Order Confirmation Page Starts */
        if($('body').hasClass('page-orderConfirmationPage')){
            var productsOrderConfirmationPage = [];           
            $(".productsGtmDataOrderConf").each(function(){
                var productId = $(this).attr('data-attr-id');
                var category = getPrimarySecondaryCategory(productId);
                productsOrderConfirmationPage.push({
                    'name' : $(this).attr('data-attr-name'),
                    'id' : $(this).attr('data-attr-id'),
                    'price' : $(this).attr('data-attr-price').replace(/,/g, ""),
                    'brand' : 'DiscoverFlow',
                    'category' : category,
                    'variant': '',
                    'quantity' : $(this).attr('data-attr-quantity')
                });
            });
            var actionField =  {
                'id': $("#orderNumber").attr('data-attr-order-id'), // Transaction ID.
                 'affiliation': 'Jamaica Ecommerce',
                 'revenue': $("#orderTotalRevenue").attr('data-attr-orderTotalRevenue'), // Total transaction value
                 'tax': $("#orderConfirmationPagePriceInfo").attr('data-attr-taxrate').replace(/,/g, ""), //Dynamic Value Ex. 4.90
                 'shipping': '', //Dynamic Value Ex. 1.90
                 'coupon': '' //Dynamic Value Ex. 0.50
           };
           var address = [];
           address.province = $("#checkoutConfirMsg").attr('data-attr-province'); //dynamic value
           address.district = ""; //dynamic value
           address.town = $("#checkoutConfirMsg").attr('data-attr-town'); //dynamic value
           address.neighbourhood = "" //dynamic value
           address.street = $("#checkoutConfirMsg").attr('data-attr-street'); //dynamic value
           address.country = "Jamaica"; //dynamic value

            window.dataLayer.push ({
                'event': 'vpv',
                'page': {
                    'pageName': pageName,
                    'breadCrumbs': breadCrumbs,
                    'pageType': pageType,
                    'country': 'Jamaica',
                    'channel': 'Ecommerce',
                    'productPage': productPage
                },
                'category':{
                    'businessCategory': 'Residential',
                    'primaryCategory': primaryCategory,
                    'subCategory': subCategory
                },
                'user':{
                    'statusLogged': statusLogged,//'logged status from the user', //Dynamic Value
                    'userType': userType,//'guest or client', //Dynamic Value
                    'profileInfo': {
                        'profileID': profileID,
                        'email': encryptedEmail1,
                        'phone': encryptedPhone
                    },
                    'social': {
                        'mail': mail,
                        'facebook': 'false',
                        'other': 'false'
                    }
                },
                'address': address,
                //'payment_method': $(".productGtmDataPaymentMethod").attr('data-attr-cardtype'),
                'ecommerce': {
                    'currencyCode': $("#orderCurrencyType").attr('data-attr-orderCurrencyType'),
                    'purchase': {
                        'actionField': actionField,
                        'products': productsOrderConfirmationPage
                    }
                }
            });
        }
        /* Order Confirmation Page Ends */
        /* Page not Found Code Starts*/
        if($('body').hasClass('page-notFound')){
            var errorMsg = $("#gtmNotFoundPageError").val();
            window.dataLayer.push({
                'event': 'error',
                'description': errorMsg,
            });
        }
        /* Page not Found Code Ends*/
        var errorGlobalAlert = 0;
        var errorMsg = '', errorMsgAll = '';
        if($(".js-checkout-step.active").prop("id") == 'step3' || $(".js-checkout-step.active").prop("id") == 'step4'){
            $(".global-alerts .alert-danger").each(function (){
                errorGlobalAlert++;
                errorMsg = $(this).text();
                errorMsg = errorMsg.replace( /[\r\n\t]+/gm, "" ).substring(1);
                errorMsgAll = errorMsgAll+ ' ' +errorMsg;
            });
        }
        if(errorGlobalAlert > 0){
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                'event': 'error',
                'description': errorMsgAll
            });
        }
    },
    loginFormGtmError: function (){
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
            'event': 'error',
            'description': 'User entered invalid email address or phone number on cart page login form'
        });
    }
}

$(window).on('load', function(){
    let gtmCallInterval = setInterval(function(){
        // when window.google_tag_manager is defined and true, stop the function
        if (!!window.google_tag_manager) {
            clearInterval(gtmCallInterval);
            googleAnalyticsTagManager.pageLoadGoogleAnalyticsData();
        }
    }, 1000);
});

$(document).ready(function(){

	//Add product on PLP
	$(document).on("click",".page-bundle #addToCartForm , .page-bundle #plpBundleAddToCartForm",function(e){
	    var cartCheck = $(".nav__links--account .updateqty .mini-cart-link .updatedcartitem").text();
        if(cartCheck == 0) {
            let products = {};
            let _this = $(this);
            let id = _this.find("input[name='productCodePost']").val();
            let category = getPrimarySecondaryCategory(id);
            products = ({
                'name' : _this.find('.pageProductsPlp').attr('data-attr-name'),
                'id' : id,
                'price' : _this.parent().parent().find('.pageProductsPlpPrice').attr('data-attr-price').substring(1).replace(/,/g, ""),
                'brand': 'DiscoverFlow',
                'category': category,
                'variant': '',
                'quantity': _this.find("input[name='qty']").val(),
                });

            window.dataLayer.push({
                'event': 'addToCart',
                'ecommerce': {
                    'currencyCode': $("main").attr('data-currency-iso-code'),
                    'add': {
                        'products': [products]
                    }
                }
            });
        }
    });
	
	//Add a product to cart from PDP
    $(document).on("click",".page-productDetails #addToCartButton",function(e){
        var products = {};
        $("#productPageGtmData").each(function(){
            var productId = $(this).attr('data-attr-id');
            var category = getPrimarySecondaryCategory(productId);
            products = ({
                'name' : $(this).attr('data-attr-name'),
                'id' : $(this).attr('data-attr-id'),
                'price' : $(this).attr('data-attr-price').substring(1).replace(/,/g, ""),
                'brand': 'DiscoverFlow',
                'category': category,
                'variant': '',
                'quantity': $("#pdpAddtoCartInput").val(),
            });
        });
        window.dataLayer.push({
            'event': 'addToCart',
            'ecommerce': {
                'currencyCode': $("#productPageGtmData").attr('data-attr-currency'),
                'add': {
                    'products': [products]
                }
            }
        });
    });

    $(document).on("click",".page-mobilePage #addToCartButton",function(e){
        var products = [];
        var addonInfo = {};
        $(".productPageGtmData").each(function(){
            var productId = $(this).attr('data-attr-id');
            if(productId != undefined) {
                var category = getPrimarySecondaryCategory(productId);
                addonInfo = ({
                    'name' : $(this).attr('data-attr-name'),
                    'id' : $(this).attr('data-attr-id'),
                    'price' : $(this).attr('data-attr-price').substring(1).replace(/,/g, ""),
                    'brand': 'DiscoverFlow',
                    'category': category,
                    'variant': '',
                    'quantity': 1,
                });
                products.push(addonInfo);
            }
        });
        window.dataLayer.push({
            'event': 'addToCart',
            'ecommerce': {
                'currencyCode': $(".productPageGtmData").attr('data-attr-currency'),
                'add': {
                    'products': products
                }
            }
        });
    });

    //Remove a product from cart page
    $('.js-cartItemDetailBtn').click(function(event) {
    	event.stopPropagation();
    	var fetchData = $(this).parent().parent().parent().find('.item__quantity .quantity-box .gtmProductData');
    	var product = [];
    	var productId = fetchData.attr('data-attr-id');
        var category = getPrimarySecondaryCategory(productId);
    	product.push({
    		'name' : fetchData.attr('data-attr-name'),
    		'id' : fetchData.attr('data-attr-id'),
    		'price' : fetchData.attr('data-attr-price').replace(/,/g, ""),
    		'brand' : 'DiscoverFlow',
    		'category' : category,
    		'variant': '',
    		'quantity' : fetchData.attr('data-attr-initialQuantity')
    	});
    	window.dataLayer.push({
    		'event': 'removeFromCart',
    		'ecommerce': {
    			'remove': {
    				'products': product
    			}
    		}
    	});
    });
});

function getPrimarySecondaryCategory(id){
    var primaryCategory = '', subCategory = '';
    switch(id) {
          case 'doit':
          case 'doitnow':
              primaryCategory = 'Fixed'; subCategory = '2P-Internet-Telephone';
              break;
          case 'doitmore':
          case 'doitall':
              primaryCategory = 'Fixed'; subCategory = '3P-Internet-TV-Telephone';
              break;
          case 'flow3200Plan':
          case 'flow4900Plan':
          case 'flow2200Plan':
              primaryCategory = 'Mobile'; subCategory = 'Sim Only';
              break;
          case 'torchbundle':
          case 'HBOGOwithTV':
              primaryCategory = 'FMC'; subCategory = '2P-Internet-Telephone';
              break;
          case 'blazebundle':
              primaryCategory = 'FMC'; subCategory = '3P-Internet-TV-Telephone';
              break;
          default:
            primaryCategory = $(this).attr('data-attr-category');
            subCategory = $(this).attr('data-attr-primarycategory');
    }
    var category = "Residential/"+primaryCategory+"/"+subCategory;
    return category;
}

function sha256Converter(ascii) {
	function rightRotate(value, amount) {
		return (value>>>amount) | (value<<(32 - amount));
	};

	var mathPow = Math.pow;
	var maxWord = mathPow(2, 32);
	var lengthProperty = 'length'
	var i, j; // Used as a counter across the whole file
	var result = ''

	var words = [];
	var asciiBitLength = ascii[lengthProperty]*8;

	//* caching results is optional - remove/add slash from front of this line to toggle
	// Initial hash value: first 32 bits of the fractional parts of the square roots of the first 8 primes
	// (we actually calculate the first 64, but extra values are just ignored)
	var hash = sha256.h = sha256.h || [];
	// Round constants: first 32 bits of the fractional parts of the cube roots of the first 64 primes
	var k = sha256.k = sha256.k || [];
	var primeCounter = k[lengthProperty];

	var isComposite = {};
	for (var candidate = 2; primeCounter < 64; candidate++) {
		if (!isComposite[candidate]) {
			for (i = 0; i < 313; i += candidate) {
				isComposite[i] = candidate;
			}
			hash[primeCounter] = (mathPow(candidate, .5)*maxWord)|0;
			k[primeCounter++] = (mathPow(candidate, 1/3)*maxWord)|0;
		}
	}

	ascii += '\x80' // Append Ƈ' bit (plus zero padding)
	while (ascii[lengthProperty]%64 - 56) ascii += '\x00' // More zero padding
	for (i = 0; i < ascii[lengthProperty]; i++) {
		j = ascii.charCodeAt(i);
		if (j>>8) return; // ASCII check: only accept characters in range 0-255
		words[i>>2] |= j << ((3 - i)%4)*8;
	}
	words[words[lengthProperty]] = ((asciiBitLength/maxWord)|0);
	words[words[lengthProperty]] = (asciiBitLength)

	// process each chunk
	for (j = 0; j < words[lengthProperty];) {
		var w = words.slice(j, j += 16); // The message is expanded into 64 words as part of the iteration
		var oldHash = hash;
		// This is now the undefinedworking hash", often labelled as variables a...g
		// (we have to truncate as well, otherwise extra entries at the end accumulate
		hash = hash.slice(0, 8);

		for (i = 0; i < 64; i++) {
			var i2 = i + j;
			// Expand the message into 64 words
			// Used below if
			var w15 = w[i - 15], w2 = w[i - 2];

			// Iterate
			var a = hash[0], e = hash[4];
			var temp1 = hash[7]
				+ (rightRotate(e, 6) ^ rightRotate(e, 11) ^ rightRotate(e, 25)) // S1
				+ ((e&hash[5])^((~e)&hash[6])) // ch
				+ k[i]
				// Expand the message schedule if needed
				+ (w[i] = (i < 16) ? w[i] : (
						w[i - 16]
						+ (rightRotate(w15, 7) ^ rightRotate(w15, 18) ^ (w15>>>3)) // s0
						+ w[i - 7]
						+ (rightRotate(w2, 17) ^ rightRotate(w2, 19) ^ (w2>>>10)) // s1
					)|0
				);
			// This is only used once, so *could* be moved below, but it only saves 4 bytes and makes things unreadble
			var temp2 = (rightRotate(a, 2) ^ rightRotate(a, 13) ^ rightRotate(a, 22)) // S0
				+ ((a&hash[1])^(a&hash[2])^(hash[1]&hash[2])); // maj

			hash = [(temp1 + temp2)|0].concat(hash); // We don't bother trimming off the extra ones, they're harmless as long as we're truncating when we do the slice()
			hash[4] = (hash[4] + temp1)|0;
		}

		for (i = 0; i < 8; i++) {
			hash[i] = (hash[i] + oldHash[i])|0;
		}
	}

	for (i = 0; i < 8; i++) {
		for (j = 3; j + 1; j--) {
			var b = (hash[i]>>(j*8))&255;
			result += ((b < 16) ? 0 : '') + b.toString(16);
		}
	}
	return result;
};
