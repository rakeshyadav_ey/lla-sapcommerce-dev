adobeAnalytics = {

	pageLoadAdobeAnalyticsData:function(){
	    window.digitalData = window.digitalData || {};

        var referringURL = document.referrer;
        if(referringURL == ''){ referringURL = 'NA'; }

        var primaryCategory = $("#adobeAnalyticsPageInfo").attr('data-attr-vpPath');
        var pageTypeValue = $("#adobeAnalyticsPageInfo").attr('data-attr-pageName');
        var breadCrumbs = $("#adobeAnalyticsPageInfo").attr('data-attr-breadcrumbsdata');
        var profileID = $("#adobeAnalyticsPageInfo").attr('data-attr-userid');
        var email = $("#adobeAnalyticsPageInfo").attr('data-attr-email');
        var statusLogged = 'Logged';
        var mail = "true";
        var pageName = $("#adobeAnalyticsPageInfo").attr('data-attr-pageName');
        var userType = $("#adobeAnalyticsPageInfo").attr('data-attr-customerType');
        var encryptedEmail1 = 'NA';
        var pageType = '';
        var productPage = "NA";
        var subCategory = '';
        //Breadcrumb start here
        if(breadCrumbs == '' || breadCrumbs == undefined){ breadCrumbs = 'N/A'; } else { breadCrumbs = breadCrumbs.split(","); }
        if(breadCrumbs == 'N/A'){
            breadCrumbs = $("#adobeAnalyticsPageInfo").attr('data-attr-pageName');
        }
        if($('body').hasClass('page-orderConfirmationPage')) {
            breadCrumbs = $("#adobeAnalyticsPageInfo").attr('data-attr-pageName');
        }

        //primaryCategory start here
        /*if($('body').hasClass('page-cartPage') || $('body').hasClass('page-multiStepCheckoutSummaryPage') || $('body').hasClass('page-orderConfirmationPage')) {
            primaryCategory = $("#adobeAnalyticsPageInfo").attr('data-attr-vpPath');
        }*/
        if(primaryCategory === 'Mobile Services') {
            primaryCategory = "Mobile";
            subCategory = "Postpaid";
        }
        else if(primaryCategory === 'bundles') {
            primaryCategory = "Bundles";
            subCategory = "Bundles";
        }
        else if(pageTypeValue === 'HOME' || pageTypeValue === 'CART' || pageTypeValue === 'CHECKOUT' || pageTypeValue === 'ORDERCONFIRMATION') {
            primaryCategory = pageTypeValue;
            subCategory = "NA";
        }

        //profile id and email start here
        if($('body').hasClass('page-productDetailsForDevices') || $('body').hasClass('page-cartPage') || $('body').hasClass('page-multiStepCheckoutSummaryPage') || $('body').hasClass('page-orderConfirmationPage')) {
            profileID = $("#adobeAnalyticsPageInfo").attr('data-attr-userid');
            email = $("#adobeAnalyticsPageInfo").attr('data-attr-email');
        }

        if(profileID == '' || profileID == undefined) { profileID = 'N/A'; statusLogged = 'Not Logged'; mail = "flase"; }

        if(email != '' && email != undefined && email != 'anonymous') {
            encryptedEmail1 = sha256(email);
        }


        if($("#adobeAnalyticsPageInfo").attr('data-attr-pageName') == 'HOME') {
            pageType = "Homepage";
        }
        else if($('body').hasClass('page-productDetails') || $('body').hasClass('page-productDetailsForDevices')) {
            pageType = "Listed Page";
        }
        else {
            pageType = "pageTypeValue";
        }


        if($('body').hasClass('page-productDetails') || $('body').hasClass('page-productDetailsForDevices')){
            productPage = $("#pdpAdobeAnalyticsData").attr('data-attr-name');
        }

        /*if($('body').hasClass('page-productDetails') && $("#pdpAdobeAnalyticsData").attr('data-attr-category') == '3Pbundles'){
            primaryCategory = "Bundles";
            subCategory = "Bundles";
        }*/


        /*if($('body').hasClass('page-productDetailsForDevices')){
            primaryCategory = "Mobile";
            subCategory = "Postpaid";
            pageName = $("#adobeAnalyticsPageInfo").attr('data-attr-pageName');
        }*/

        if($('body').hasClass('page-productDetails') || $('body').hasClass('page-productDetailsForDevices') || $('body').hasClass('page-cartPage') || $('body').hasClass('page-multiStepCheckoutSummaryPage') || $('body').hasClass('page-orderConfirmationPage')){
            pageName = $("#adobeAnalyticsPageInfo").attr('data-attr-pageName');
        }

        if(userType == '' || userType == undefined){ userType = 'NA';}

        window.digitalData = window.digitalData || {};
        window.digitalData = {
            page: {
                pageInfo: {
                    hostName: location.hostname, //"https://cwpanama.com", //dynamic value
                    pageName: pageName, //"Bundle - Complete Package 300", //dynamic value
                    currentURL: window.location.href, //"https://cwpanama.com/home", //dynamic value
                    referringURL: referringURL, //"https://example.com/referrer ", //dynamic value
                    breadCrumbs: breadCrumbs, //["Online Store","Mobile Pospaid Plans"], //dynamic value
                },
                category: {
                    primaryCategory: primaryCategory, //"Movil", //dynamic value // this is for all page
                    subCategory: subCategory, //"Postpago", //dynamic value
                    productPage: productPage,//"Data Without Limits", //dynamic value
                    pageType: pageTypeValue, //"Listed Page", //dynamic value
                    channel: "ecommerce_jamaica", //static value
                },
                attributes: {
                    country: "Jamaica", //static value
                }
            },
            user: [{
                profile: [{
                    statusLogged: statusLogged, //"Logged", //dynamic value
                    userType: userType, //"Guest", //dynamic value
                    profileInfo: {
                        profileID: profileID, //"aq123123", //dynamic value
                        email: encryptedEmail1, //"aiadifaoidfasdfsadfs", //dynamic value
                        phone: "NA", //dynamic value
                    },
                    social: {
                        facebook: "false", //dynamic value
                        mail: mail
                    }
                }]
            }],
            version: "1.0",
        }

        if($('body').hasClass('page-cartPage')){
            onCartPageLoad();
        }
        if($('body').hasClass('page-orderConfirmationPage')){
            onCartPageLoad();
            onpageloadOCP();
            var transaction = [], profile = [], address = [];
            transaction.transactionID =  $("#order-code").attr('data-attr-order-id'); //dynamic value
            transaction.affiliation =  "Ecommerce Jamaica"; //static value
            //transaction.shippingMethod = $("#addressInfo").attr('data-attr-shippingMethod'); //dynamic value
            address.province = $("#checkoutConfirMsg").attr('data-attr-province'); //dynamic value
            //address.district = $("#addressInfo").attr('data-attr-district'); //dynamic value
            address.town = $("#checkoutConfirMsg").attr('data-attr-town'); //dynamic value
            //address.neighbourhood = $("#addressInfo").attr('data-attr-neighbourhood'); //dynamic value
            address.street = $("#checkoutConfirMsg").attr('data-attr-street'); //dynamic value
            address.country = "Jamaica"; //dynamic value

            profile.address = address;
            transaction.profile = profile;
            window.digitalData.transaction = transaction;
        }
        if($('body').hasClass('page-productDetails')){
            onpageloadpdp();
        }
	}
}

function onpageloadOCP() {
    var products = [];
    $(".productGtmData").each(function(){
        var productInfo = {}, category = {};
        productInfo.productName = $(this).attr('data-attr-name');
        productInfo.price = $(this).attr('data-attr-price').replace(/[^0-9\.]+/g, "");
        productInfo.brand = $(this).attr('data-attr-brand');
        productInfo.quantity = 1;
        var primaryCategory = $(this).attr('data-attr-category_2');
        if(primaryCategory === 'Mobile Services') {
            category.primaryCategory = 'Postpaid';
            category.subcategory = 'Mobile Services';
        }
        else if(primaryCategory === 'Bundles' || primaryCategory === 'bundles') {
            category.primaryCategory = 'Bundles';
            category.subcategory = 'Bundles';
        }
        products.push({
            'productInfo': productInfo,
            'category': category
        });
    });
    window.digitalData.product = products;
}

function onpageloadpdp() {
    var products = [];
    $("#adobeAnalyticsPageInfo").each(function(){
        var productInfo = {}, category = {};
        var pageNameValue = $(this).attr('data-attr-pagename');
        productInfo.productName = $(this).attr('data-attr-name');
        productInfo.price = $(this).attr('data-attr-price').replace(/[^0-9\.]+/g, "").substring(1);
        productInfo.brand = "NA";
        productInfo.quantity = 1;
        var primaryCategory = $(this).attr('data-attr-vpPath');
        if(primaryCategory === 'Mobile Services') {
            category.primaryCategory = 'Postpaid';
            category.subcategory = 'Mobile Services';
        }
        else if(primaryCategory === 'Bundles') {
            category.primaryCategory = 'Bundles';
            category.subcategory = 'Bundles';
        }
        //category.primaryCategory = $(this).attr('data-attr-primarycategoryName');
        //category.subcategory = $(this).attr('data-attr-primarycategoryName');
        products.push({
            'productInfo': productInfo,
            'category': category
        });
    });
    window.digitalData.product = products;
}

$(document).ready(function(){
    adobeAnalytics.pageLoadAdobeAnalyticsData();

    //Setting cookie for if user is logged in
	loginSuccessSetCookie();

	//Logout passing data and remvoing cookie
    $(".nav__links li.js-logout a").click(function(e){
        var encryptedEmailData = 'NA', encryptedUserPhoneData = 'NA';
        var email = $("#adobeAnalyticsPageInfo").attr('data-attr-email');
        if(email != '' && email != undefined && email != 'anonymous'){ encryptedEmailData = sha256(email); }
        var userPhone =  $("#adobeAnalyticsPageInfo").attr('data-attr-mobilePhone');
        if(userPhone != '' && userPhone != undefined){ encryptedUserPhoneData = sha256(userPhone); }
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
            'event': 'logout',
            'userType': $("#adobeAnalyticsPageInfo").attr('data-attr-customerType'),
            'profileID': $("#adobeAnalyticsPageInfo").attr('data-attr-userid'),
            'email': encryptedEmailData,
            'phone': encryptedUserPhoneData,
            'accountid': encryptedUserPhoneData
        });
        document.cookie = "loginSuccessCount=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    });

	//Removing cookie after guest user completes his order
	if($("body").hasClass('page-orderConfirmationPage')){
        var userType = $("#adobeAnalyticsPageInfo").attr('data-attr-customerType');
        if(userType == 'GUEST' || userType == 'guest'){
            document.cookie = "loginSuccessCount=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        }
    }

	//Guest checkout
    $('.page-cartPage').on('click', '.guestCheckoutButton ', function(event) {
        _satellite.track("evCheckoutPopUpOptions",{
            action: "GuestGO"
        });
    });

    $('#guestCheckout #guestEmail').on('keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            if($("#guestCheckout").valid()){
                _satellite.track("evCheckoutPopUpOptions",{
                    action: "GuestGO"
                });
            }
        }
    });

    $(document).on("click","#login-popup .btn-blue",function() {
        _satellite.track("evCheckoutPopUpOptions",{
            action: "Login"
        });
    });

    $(document).on("click",".login-popup-close",function() {
        _satellite.track("evCheckoutPopUpOptions",{
            action: "Cancel"
        });
    });
});

function sha256(ascii) {
	function rightRotate(value, amount) {
		return (value>>>amount) | (value<<(32 - amount));
	};

	var mathPow = Math.pow;
	var maxWord = mathPow(2, 32);
	var lengthProperty = 'length'
	var i, j; // Used as a counter across the whole file
	var result = ''

	var words = [];
	var asciiBitLength = ascii[lengthProperty]*8;

	//* caching results is optional - remove/add slash from front of this line to toggle
	// Initial hash value: first 32 bits of the fractional parts of the square roots of the first 8 primes
	// (we actually calculate the first 64, but extra values are just ignored)
	var hash = sha256.h = sha256.h || [];
	// Round constants: first 32 bits of the fractional parts of the cube roots of the first 64 primes
	var k = sha256.k = sha256.k || [];
	var primeCounter = k[lengthProperty];

	var isComposite = {};
	for (var candidate = 2; primeCounter < 64; candidate++) {
		if (!isComposite[candidate]) {
			for (i = 0; i < 313; i += candidate) {
				isComposite[i] = candidate;
			}
			hash[primeCounter] = (mathPow(candidate, .5)*maxWord)|0;
			k[primeCounter++] = (mathPow(candidate, 1/3)*maxWord)|0;
		}
	}

	ascii += '\x80' // Append Ƈ' bit (plus zero padding)
	while (ascii[lengthProperty]%64 - 56) ascii += '\x00' // More zero padding
	for (i = 0; i < ascii[lengthProperty]; i++) {
		j = ascii.charCodeAt(i);
		if (j>>8) return; // ASCII check: only accept characters in range 0-255
		words[i>>2] |= j << ((3 - i)%4)*8;
	}
	words[words[lengthProperty]] = ((asciiBitLength/maxWord)|0);
	words[words[lengthProperty]] = (asciiBitLength)

	// process each chunk
	for (j = 0; j < words[lengthProperty];) {
		var w = words.slice(j, j += 16); // The message is expanded into 64 words as part of the iteration
		var oldHash = hash;
		// This is now the undefinedworking hash", often labelled as variables a...g
		// (we have to truncate as well, otherwise extra entries at the end accumulate
		hash = hash.slice(0, 8);

		for (i = 0; i < 64; i++) {
			var i2 = i + j;
			// Expand the message into 64 words
			// Used below if
			var w15 = w[i - 15], w2 = w[i - 2];

			// Iterate
			var a = hash[0], e = hash[4];
			var temp1 = hash[7]
				+ (rightRotate(e, 6) ^ rightRotate(e, 11) ^ rightRotate(e, 25)) // S1
				+ ((e&hash[5])^((~e)&hash[6])) // ch
				+ k[i]
				// Expand the message schedule if needed
				+ (w[i] = (i < 16) ? w[i] : (
						w[i - 16]
						+ (rightRotate(w15, 7) ^ rightRotate(w15, 18) ^ (w15>>>3)) // s0
						+ w[i - 7]
						+ (rightRotate(w2, 17) ^ rightRotate(w2, 19) ^ (w2>>>10)) // s1
					)|0
				);
			// This is only used once, so *could* be moved below, but it only saves 4 bytes and makes things unreadble
			var temp2 = (rightRotate(a, 2) ^ rightRotate(a, 13) ^ rightRotate(a, 22)) // S0
				+ ((a&hash[1])^(a&hash[2])^(hash[1]&hash[2])); // maj

			hash = [(temp1 + temp2)|0].concat(hash); // We don't bother trimming off the extra ones, they're harmless as long as we're truncating when we do the slice()
			hash[4] = (hash[4] + temp1)|0;
		}

		for (i = 0; i < 8; i++) {
			hash[i] = (hash[i] + oldHash[i])|0;
		}
	}

	for (i = 0; i < 8; i++) {
		for (j = 3; j + 1; j--) {
			var b = (hash[i]>>(j*8))&255;
			result += ((b < 16) ? 0 : '') + b.toString(16);
		}
	}
	return result;
};

//Passing data to datalayer when user login successfully.
function loginSuccessSetCookie(){
    var email = $("#adobeAnalyticsPageInfo").attr('data-attr-email');
    if(email != '' && email != undefined && email != 'anonymous'){
        if(document.cookie.indexOf('loginSuccessCount=') == -1){
            var date = new Date();
            date.setTime(date.getTime() + (1*24*60*60*1000));
            document.cookie="loginSuccessCount=1;expires="+date.toUTCString()+";path=/";
            var encryptedEmailData = 'NA', encryptedUserPhoneData = 'NA';
            var userType = $("#adobeAnalyticsPageInfo").attr('data-attr-customerType');
            if(email != '' && email != undefined && email != 'anonymous'){
                if(userType == 'GUEST' || userType == 'guest'){
                    var emaildata = email.split("|");
                    email = emaildata[1];
                }
                encryptedEmailData = sha256(email);
            }
            var userPhone =  $("#adobeAnalyticsPageInfo").attr('data-attr-mobilePhone');
            if(userPhone != '' && userPhone != undefined){ encryptedUserPhoneData = sha256(userPhone); }
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                'event': 'login',
                'userType': userType,
                'profileID': $("#adobeAnalyticsPageInfo").attr('data-attr-userid'),
                'email': encryptedEmailData,
                'phone': encryptedUserPhoneData,
                'accountid': encryptedUserPhoneData
            });
        }
    }
}
function onCartPageLoad(){
    var cart = {};
    var price = {};
    var className = $("#cartPagePriceInfo");
    if($('body').hasClass('page-orderConfirmationPage')){
        className = $("#orderConfirmationPagePriceInfo");
    }

    var subtotal = 0;
    if(typeof className.attr('data-attr-subtotal') !== 'undefined') {
        subtotal = className.attr('data-attr-subtotal').replace(/[^0-9\.]+/g, "");
    }
    price.subtotal = subtotal; //dynamic value

    var savings = 0;
    if(typeof className.attr('data-attr-savings') !== 'undefined') {
        savings = className.attr('data-attr-savings').replace(/[^0-9\.]+/g, "");
    }
    price.savings = savings;//dynamic value

    var currency = '';
    if(typeof className.attr('data-attr-currency') !== 'undefined') {
        currency = className.attr('data-attr-currency');
    }
    price.currency = currency; //dynamic value

    var taxRate = 0;
    if(typeof className.attr('data-attr-taxRate') !== 'undefined') {
        taxRate = className.attr('data-attr-taxRate').replace(/[^0-9\.]+/g, "");
    }
    price.taxRate = taxRate; //dynamic value

    var cartTotal = 0;
    if(typeof className.attr('data-attr-cartTotal') !== 'undefined') {
        cartTotal = className.attr('data-attr-cartTotal').replace(/[^0-9\.]+/g, "");
    }
    price.cartTotal = cartTotal; //dynamic value
    cart.price = price;
    window.digitalData.cart = cart;

}

