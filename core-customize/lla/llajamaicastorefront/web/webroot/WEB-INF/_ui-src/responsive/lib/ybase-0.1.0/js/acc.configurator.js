
if($('.step1ConfigForm').length) {	
	$('.step1ConfigForm').submit(function() {	
		$(this).parents('.ctabox').addClass('selected');
		return true; // return false to cancel form action
	});	
}

if($('.step2ConfigForm').length) {	
	$('.step2ConfigForm').submit(function() {
		$(this).parents('.ctabox').addClass('selected');
		return true; // return false to cancel form action
	});

	enquire.register("screen and (max-width:" + 767 + "px" + ")", { 
		match: function () {
			if($(".ctabox").hasClass("selected")) {
				$(window).scrollTop($('.selected').offset().top);				
			}
		}
	});
}

if($('.step2ConfigFormNext').length) {	
	$('.step2ConfigFormNext').submit(function() {		
		if($('.selected').length) {
			var selectedMobile = $('.selected').find('#selectedMobile').val();
			$(".next-button #selectedMobile").val(selectedMobile);
		}
		return true; // return false to cancel form action
	});
}