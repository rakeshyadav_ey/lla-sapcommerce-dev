<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="tab-details">
	<ycommerce:testId code="productDetails_content_label">
	<div class="tab-channel-list">
		<div class="channel-list">
			<ul>
				<li>HBO <span class="special">*</span></li>
				<li>HBO 3 Pack </li>
				<li>HBO More </li>
				<li>In the Know</li> 
				<li>More Enternainment</li>
				<li>Fox Premium Movies <span class="special">*</span></li>
				<li>Spanish Pack</li>
				<li>Sports Fan</li>
				<li>The Maxpack</li>
				<li>A la Carte (per channel)</li>
				<li>Adult</li>
				<li>Adult Plus</li>
				<li>Flow Sports Pack <span class="special">*</span></li>
			</ul>
		</div>
	</div>
	</ycommerce:testId>
</div>