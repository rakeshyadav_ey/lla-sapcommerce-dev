<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:url value="/plp/international-phone" var="plpUrl" />
<div class="tab-details">
	<ycommerce:testId code="productDetails_content_label">
		<div class="tab-content-details">
            <p class="tab-data-head">World Pak</p>
            <span class="tab-data-desc">WorldPak Destinations USA, Canada, India, China and landlines in UK & Spain</span>
        </div>
        <div class="tab-content-details">
        	<p class="tab-data-head">30 days</p>
            <span class="tab-data-desc">Duration</span>
		</div>
		<div class="tab-content-details">
		     <p class="tab-data-head">660</p>
             <span class="tab-data-desc">Minutes</span>
		</div>
		<div class="tab-content-details">
		     <p class="tab-data-head">$ 900 + GCT</p>
		     <span class="tab-data-desc">Price</span>
             <p><a href="${plpUrl}" class="down-link-android">Get It Now</a></p>
		</div>
		<div class="tab-content-details">
		     <p class="tab-data-head">WorldPak Lite</p>
             <span class="tab-data-desc">WorldPak Lite Destinations USA, Canada, UK Landlines</span>
		</div>
		<div class="tab-content-details">
		     <p class="tab-data-head">30 days</p>
             <span class="tab-data-desc">Duration</span>
		</div>
		<div class="tab-content-details">
		     <p class="tab-data-head">300</p>
             <span class="tab-data-desc">Minutes</span>
		</div>
		<div class="tab-content-details">
		     <p class="tab-data-head">$ 575 + GCT</p>
		     <span class="tab-data-desc">Price</span>
             <p><a href="${plpUrl}" class="down-link-android">Get It Now</a></p>
		</div>
	</ycommerce:testId>
</div>