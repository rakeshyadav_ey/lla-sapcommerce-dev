<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:url value="/plp/roaming" var="plpurl"/>

<div class="tab-details">
	<ycommerce:testId code="productDetails_content_label">
		<div class="product-classifications">
        	<div class="tab-content-details">
			    <p class="tab-data-head"><spring:theme code="pdp.roaming.tab.headtext1" text="Traveling overseas?"/></p>
				<span class="tab-data-desc"><spring:theme code="pdp.roaming.tab.text1" text="Traveling overseas?"/></span>
			</div>
			<div class="tab-content-details">
			    <p class="tab-data-head"><spring:theme code="pdp.roaming.tab.headtext2" text="TravelPass Combo"/></p>
			</div>
			<div class="tab-content-details">
			    <p class="tab-data-head"><spring:theme code="pdp.roaming.tab.headtext3" text="Free"/></p>
			    <span class="tab-data-desc"><spring:theme code="pdp.roaming.tab.text3" text="Free"/></span>
			</div>
			<div class="tab-content-details">
			    <p class="tab-data-head"><spring:theme code="pdp.roaming.tab.headtext4" text="30 minutes"/></p>
			    <span class="tab-data-desc"><spring:theme code="pdp.roaming.tab.text4" text="30 minutes"/></span>
			</div>
			<div class="tab-content-details">
			    <p class="tab-data-head"><spring:theme code="pdp.roaming.tab.headtext5" text="100mb"/></p>
			    <span class="tab-data-desc"><spring:theme code="pdp.roaming.tab.text5" text="100mb"/></span>
			</div>
			<div class="tab-content-details">
			    <p class="tab-data-head"><spring:theme code="pdp.roaming.tab.headtext6" text="7 days"/></p>
			    <span class="tab-data-desc"><spring:theme code="pdp.roaming.tab.text6" text="7 days"/></span>
			    <p><a href="${plpurl}" class="down-link-android"><spring:theme code="pdp.roaming.tab.active" text="Activate now"/></a></p>
			    <p class="tab-data-desc"><i><spring:theme code="pdp.roaming.tab.text7" text="7 days"/></i></p>
			</div>
        </div>
	</ycommerce:testId>
</div>