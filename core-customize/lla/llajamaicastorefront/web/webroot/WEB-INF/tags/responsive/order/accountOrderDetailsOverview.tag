<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="order-detail-overview">
    <h3 class="summary-head"><spring:theme code="order.multi.order.summary" /></h3>
    <div class="order-detail-overview-list">
    <div class="row summary-details">
        <c:if test="${not empty orderData.statusDisplay}">
            <div class="col-xs-12">
                <div class="item-group">
                    <ycommerce:testId code="orderDetail_overviewOrderStatus_label">
                        <span class="item-label"><spring:theme code="text.account.orderHistory.orderStatus"/></span>
                        <span class="item-value textPrimaryColor"><spring:theme code="text.account.order.status.display.${orderData.statusDisplay}"/></span>
                    </ycommerce:testId>
                </div>
            </div>
        </c:if>
        <div class="col-xs-12">
            <div class="item-group">
                <ycommerce:testId code="orderDetail_overviewOrderID_label">
                    <span class="item-label"><spring:theme code="text.account.orderHistory.orderNumber"/></span>
                    <span class="item-value">${fn:escapeXml(orderData.code)}</span>
                </ycommerce:testId>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="item-group">
                <ycommerce:testId code="orderDetail_overviewStatusDate_label">
                    <span class="item-label"><spring:theme code="text.account.order.datePurchased"/></span>
                    <span class="item-value"><fmt:formatDate value="${order.created}"  pattern="dd/MM/yyyy HH:mm"  type="both"/>&nbsp;<spring:theme code="text.account.order.datePurchased.hours"/></span>
                </ycommerce:testId>
            </div>
        </div>
        <div class="col-xs-12 discount-pay">
            <div class="item-group">
                <ycommerce:testId code="orderDetail_overviewOrderTotal_label">
                    <c:if test="${ cmsSite.uid eq 'jamaica' and order.entries[0].discountFlag eq 'true'}">
                    <span class="item-label"><spring:theme code="text.order.discounted.monthly"/>
                        <span class="sm-label-text"><spring:theme code="text.order.first"/> &nbsp;${order.discountNumber}&nbsp;${order.discountPeriod.code}</span>
                    </span> 
                    <span class="item-value"><format:price priceData="${orderData.monthlyTotalPrice}"/></span>
                    </c:if>
                </ycommerce:testId>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="item-group">
                <ycommerce:testId code="orderDetail_overviewOrderTotal_label">
                    <span class="item-label"><spring:theme code="text.account.order.total.monthly"/></span>
                    <span class="item-value textPrimaryColor"><format:price priceData="${orderData.monthlyTotalPrice}"/></span>
                </ycommerce:testId>
            </div>
        </div>        

        <input type="hidden" id="orderNumber" data-attr-order-id="${fn:escapeXml(orderData.code)}" >
        <input type="hidden" id="orderTotalRevenue" data-attr-orderTotalRevenue="${order.totalPriceWithTax.value}" >
        <input type="hidden" id="orderCurrencyType" data-attr-orderCurrencyType="${order.totalPriceWithTax.currencyIso}" >
        <c:if test="${orderData.quoteCode ne null}">
			  <div class="col-sm-3">
			  	  <div class="item-group">
					  <spring:url htmlEscape="false" value="/my-account/my-quotes/{/quoteCode}" var="quoteDetailUrl">
					  <spring:param name="quoteCode"  value="${orderData.quoteCode}"/>
					  </spring:url>
		              <ycommerce:testId code="orderDetail_overviewQuoteId_label">
							  <span class="item-label"><spring:theme code="text.account.quote.code"/></span>
							  <span class="item-value">
								  <a href="${fn:escapeXml(quoteDetailUrl)}" >
								  	  ${fn:escapeXml(orderData.quoteCode)}
								  </a>
							  </span>
		              </ycommerce:testId>
					</div>
				</div>
			</c:if>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="billing-address contact">
                <div class="value-order">
                    <order:addressItem address="${order.installationAddress}"/>
                    <span>${order.placedBy}</span>
                </div> 
            </div>
        </div>
    </div>
    <div class="row">
         <div class="col-sm-12">
            <div class="billing-address">
                <h4><spring:theme code="text.payment.billing.information"/></h4>
                <order:billingAddressItem order="${orderData}"/>
                
                <c:if test="${not empty orderData.paymentInfo}">
                    <div class="paymentcard-details">
                        <div class="label-order textPrimaryColor"><spring:theme code="text.order.payment.method.headline"/></div>
                        <spring:theme code="text.order.payment.card.type"/><br>
                        ${orderData.paymentInfo.cardType}:
                        ${orderData.paymentInfo.cardNumber}
                    </div>
                </c:if>
                
                <%-- --%>
    <c:forEach items="${orderData.entries}" var="entryGroup" varStatus="count" >
          <input type="hidden" class="productsGtmDataOrderConf"
           <c:if test="${entryGroup.product.name ne null}"> data-attr-name="${entryGroup.product.name}" </c:if>
           <c:if test="${entryGroup.product.code ne null}"> data-attr-id="${entryGroup.product.code}" </c:if>
           <c:if test="${entryGroup.orderEntryPrices[0].basePrice.value ne null}"> data-attr-price="${entryGroup.orderEntryPrices[0].basePrice.value}" </c:if>
           <c:if test="${entryGroup.quantity ne null}"> data-attr-quantity="${entryGroup.quantity}" </c:if>
           <c:if test="${entryGroup.product.categories[0].name ne null}"> data-attr-category="${entryGroup.product.categories[0].name}" </c:if>
           <c:if test="${entryGroup.product.categories[0].parentCategoryName ne null}"> data-attr-primarycategory="${entryGroup.product.categories[0].parentCategoryName}" </c:if>
       />
    </c:forEach>
                <%-- --%>
            </div>
        </div>
    </div>
    </div>
</div>
