<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="tab-details">
	<ycommerce:testId code="productDetails_content_label">
		<div class="product-classifications">
		<div class="tab-content-details">
                <span class="tab-data-desc">
	                <spring:theme code="pdp.switch.tab.text.default" text="Unlimited calls"/>
                </span>
           	</div>
			<div class="tab-content-details">
                <p class="tab-data-head"><spring:theme code="pdp.switch.tab.headtext1" text="DO MORE WITH UNLIMITED EVERYTHING."/></p>
                <span class="tab-data-desc">
	                <spring:theme code="pdp.switch.tab.text1" text="Say hello to better"/>
                </span>
           	</div>
           	<div class="tab-content-details">
                <p class="tab-data-head"><spring:theme code="pdp.switch.tab.headtext2" text="What is number portability?"/></p>
                <span class="tab-data-desc">
                	<spring:theme code="pdp.switch.tab.text2" text="What is number portability?"/>
                </span>
            </div>
            <div class="tab-content-details">
                <p class="tab-data-head"><spring:theme code="pdp.switch.tab.headtext3" text="Why should I switch to FLOW?"/></p>
                <span class="tab-data-desc">
                	<spring:theme code="pdp.switch.tab.text3" text="Why should I switch to FLOW?"/>
                </span>
            </div>
            <div class="tab-content-details">
                <p class="tab-data-head"><spring:theme code="pdp.switch.tab.headtext4" text="We give back in a big way."/></p>
                <span class="tab-data-desc">
                	<spring:theme code="pdp.switch.tab.text4" text="We give back in a big way."/>
            	</span>
            </div>
            <div class="tab-content-details">
            	<p class="tab-data-head"><spring:theme code="pdp.switch.tab.headtext5" text="Does FLOW offer free texts, free calls or free nights?"/></p>
            	<span class="tab-data-desc">
            		<spring:theme code="pdp.switch.tab.text5" text="Does FLOW offer free texts, free calls or free nights?"/>
            	</span>
            </div>
            
	        <div class="tab-content-details">
	            <p class="tab-data-head"><spring:theme code="pdp.switch.tab.headtext6" text="Roaming (USA, Canada, UK)"/></p>
	            <span class="tab-data-desc">
	            	<spring:theme code="pdp.switch.tab.text6" text="Roaming (USA, Canada, UK)"/>
	        	</span>
	        </div>
	        <div class="tab-content-details">
	            <p class="tab-data-head"><spring:theme code="pdp.switch.tab.headtext7" text="Social Media Benefits"/></p>
	            <span class="tab-data-desc">
	            	<spring:theme code="pdp.switch.tab.text7" text="Social Media Benefits"/>
	        	</span>
	        </div>
	        <div class="tab-content-details">
	            <p class="tab-data-head"><spring:theme code="pdp.switch.tab.headtext8" text="Voice Benefit"/></p>
	            <span class="tab-data-desc">
	            	<spring:theme code="pdp.switch.tab.text8" text="Voice Benefit"/>
	        	</span>
	        </div>
	        <div class="tab-content-details">
	            <p class="tab-data-head"><spring:theme code="pdp.switch.tab.headtext9" text="SWITCH TO BETTER. MOVE TO FLOW."/></p>
	        </div>
        	<a href="https://discoverflow.co/jamaica/mobile/switch-flow" class="btn-custom" type="button">&nbsp;&nbsp;<spring:theme code="pdp.switch.tab.btn" text="Switch Now"/>&nbsp;&nbsp;</a>
      	</div>
	</ycommerce:testId>
</div>