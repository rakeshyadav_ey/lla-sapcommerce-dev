<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="secureFooter" tagdir="/WEB-INF/tags/responsive/common/footer"%>

<c:url value="/configureProduct/saveMobilePlan" var="getItNow"/>
<c:url value="/configureProduct/addToCart" var="nextStep"/>
<c:url value="/configureProduct/deleteMobile" var="deleteItem"/>
<c:set var="categoryName" value="${categoryName}"/>
<div class="configurator-body">
    <c:choose>
		<c:when test="${not empty selectedBundle}">
			<div class="container">
				<div class="step2-infobar">
					<div class="info-msg">
						<div class="alert-l-icon hidden-xs">
                            <!--this below icon is tick icon-->
                            <svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M18 33.75C22.1772 33.75 26.1832 32.0906 29.1369 29.1369C32.0906 26.1832 33.75 22.1772 33.75 18C33.75 13.8228 32.0906 9.81677 29.1369 6.86307C26.1832 3.90937 22.1772 2.25 18 2.25C13.8228 2.25 9.81677 3.90937 6.86307 6.86307C3.90937 9.81677 2.25 13.8228 2.25 18C2.25 22.1772 3.90937 26.1832 6.86307 29.1369C9.81677 32.0906 13.8228 33.75 18 33.75ZM18 36C22.7739 36 27.3523 34.1036 30.7279 30.7279C34.1036 27.3523 36 22.7739 36 18C36 13.2261 34.1036 8.64773 30.7279 5.27208C27.3523 1.89642 22.7739 0 18 0C13.2261 0 8.64773 1.89642 5.27208 5.27208C1.89642 8.64773 0 13.2261 0 18C0 22.7739 1.89642 27.3523 5.27208 30.7279C8.64773 34.1036 13.2261 36 18 36Z" fill="#0085CA"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M24.6825 11.1825C24.9974 10.8704 25.4225 10.6947 25.8658 10.6935C26.3092 10.6922 26.7353 10.8656 27.0519 11.176C27.3685 11.4863 27.5502 11.9089 27.5577 12.3522C27.5653 12.7955 27.3981 13.224 27.0922 13.545L18.1102 24.7725C17.9559 24.9388 17.7696 25.0722 17.5624 25.1648C17.3553 25.2574 17.1316 25.3073 16.9048 25.3115C16.678 25.3157 16.4526 25.2741 16.2422 25.1893C16.0318 25.1044 15.8407 24.9779 15.6802 24.8175L9.72899 18.864C9.56319 18.7095 9.43021 18.5232 9.33798 18.3162C9.24575 18.1092 9.19615 17.8857 9.19216 17.6592C9.18816 17.4326 9.22984 17.2075 9.31471 16.9974C9.39958 16.7873 9.52591 16.5964 9.68615 16.4362C9.84639 16.2759 10.0373 16.1496 10.2474 16.0647C10.4575 15.9798 10.6826 15.9382 10.9092 15.9422C11.1357 15.9462 11.3592 15.9958 11.5662 16.088C11.7732 16.1802 11.9595 16.3132 12.114 16.479L16.8255 21.1882L24.6397 11.232C24.6537 11.2146 24.6687 11.198 24.6847 11.1825H24.6825Z" fill="#0085CA"/>
                            </svg>
                        </div>
						<div class="col-xs-12 col-sm-10">

                            <c:forEach var="offeringPrices" items="${selectedBundle.productOfferingPrices}" varStatus="productIndex">
                                <c:if test="${null ne offeringPrices.recurringChargeEntries}">
                                    <c:set var="recurringCharges" value="${offeringPrices.recurringChargeEntries[0]}"/>
                                </c:if>
                            </c:forEach>

                            <c:choose>
                                <c:when test="${not empty recurringCharges.discountedPrice}">
                                    <c:set var="formatedPrice">${recurringCharges.discountedPrice.formattedValue}</c:set>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="formatedPrice">${recurringCharges.price.formattedValue}</c:set>
                                </c:otherwise>
                            </c:choose>
                            <c:set var="categoryName" value="${fn:escapeXml(productCategories[productCategories.size()-2].name)}"/>
                            <input type="hidden" class="mobilePageGtmData"
                            <c:if test="${categoryName ne null}"> data-attr-vpPath="${categoryName}" </c:if>
                            <c:if test="${selectedBundle.name ne null}"> data-attr-name="${fn:escapeXml(selectedBundle.name)}" </c:if>
                            <c:if test="${selectedBundle.code ne null}"> data-attr-id="${selectedBundle.code}" </c:if>
                            <c:if test="${selectedBundle.price.formattedValue ne null}"> data-attr-price="${selectedBundle.price.formattedValue}" </c:if>
                            <c:if test="${selectedBundle.categories[0].name ne null}"> data-attr-brand="${selectedBundle.categories[0].name}" data-attr-category="${selectedBundle.categories[0].name}" </c:if>
                            <c:if test="${selectedBundle.price.currencyIso ne null}"> data-attr-currency="${selectedBundle.price.currencyIso}" </c:if>
                            >

							<spring:theme code="configurator.step2.infobar.withdoit.text" argumentSeparator=";" arguments="${selectedBundle.name};${formatedPrice} / month" htmlEscape="false"/>
						</div>
						
						<div class="col-sm-2 next-button">
							<form:form action="${nextStep}" method="post" class="step2ConfigFormNext">
                                <input type="hidden" name="selectedMobile" id="selectedMobile" value="${selectedMobile.code}"/>
                                <input type="hidden" value="ACQUISITION" id="processType" name="processType" />
								<button id="addToCartButton" type="submit" class="btn btn-next"><spring:theme code="configurator.addtocart.button.text" /></button>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div class="step1-infobar">
				<div class="container">
					<div class="info-msg">
						<div class="col-xs-12 col-sm-10">
							<spring:theme code="configurator.step2.infobar.text" htmlEscape="false" />
						</div>
						<div class="col-sm-2 next-button">
							<form:form action="${nextStep}" method="post" class="step2ConfigFormNext">
                                <input type="hidden" name="selectedMobile" id="selectedMobile" value="${item.code}"/>
								<button id="addToCartButton" type="submit" class="btn btn-next"><spring:theme code="configurator.addtocart.button.text" /></button>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</c:otherwise>
	</c:choose>


	<div class="container">
        <div class="ctabox-container row">
            <c:forEach var="item" items="${productList}" varStatus="productIndex">
                <div class="col-sm-4">
                    <div class="ctabox step2 <c:if test="${selectedMobile.code == item.code}">selected</c:if>">
                        <c:if test="${selectedMobile.code == item.code}">
                            <input type="hidden" class="mobilePageGtmData"
                            <c:if test="${categoryName ne null}"> data-attr-vpPath="${categoryName}" </c:if>
                            <c:if test="${item.name ne null}"> data-attr-name="${fn:escapeXml(item.name)}" </c:if>
                            <c:if test="${item.code ne null}"> data-attr-id="${item.code}" </c:if>
                            <c:if test="${item.price.formattedValue ne null}"> data-attr-price="${item.price.formattedValue}" </c:if>
                            <c:if test="${item.categories[0].name ne null}"> data-attr-brand="${item.categories[0].name}" data-attr-category="${item.categories[0].name}" </c:if>
                            <c:if test="${item.price.currencyIso ne null}"> data-attr-currency="${item.price.currencyIso}" </c:if>
                            >
                        </c:if>
                        <div class="ctabox__selected--blur"></div>
                        <div class="content">
                            <div class="bundle">
                                <p class="bundleName">${item.name}</p>
								
								<c:forEach var="offeringPrices" items="${item.productOfferingPrices}" varStatus="productIndex">
                                    <c:if test="${null ne offeringPrices.recurringChargeEntries}">
                                        <c:set var="recurringCharges" value="${offeringPrices.recurringChargeEntries[0]}"/>
                                    </c:if>
                                </c:forEach>
                               
                                       <p class="oldPrice"><span class="linethrough">Was ${currentCurrency.symbol}<fmt:formatNumber type = "number" maxFractionDigits = "0" value = "${recurringCharges.price.value}" /> + <spring:theme code="configurator.cgt" /></span> <span class="off"><spring:theme code="configurator.off" /></span></p>
                                       <p class="newPrice">Now ${recurringCharges.discountedPrice.formattedValue} + <spring:theme code="configurator.cgt" />&#42;</p>
                                
                            </div>
                            <hr>
                        </div>
                        <div class="specification">
                            <c:forEach var="specCharValue" items="${item.productSpecCharValueUses}" varStatus="index">
                                <p class="internet-spec ${specCharValue.productSpecCharacteristicValues[0].id}">
                                    ${specCharValue.description}
                                </p>
                            </c:forEach>
                        </div>
                        <form:form action="${getItNow}" method="post" class="step2ConfigForm ctabox-want-form">
                            <input type="hidden" name="selectedMobile" id="selectedMobile" value="${item.code}"/>
                            <input type="hidden" value="ACQUISITION" id="processType" name="processType" />
                            <div class="ctabox-btn ctabox-wantbtn--js">
                                <button type="submit" class="btn btn-primary"><spring:theme code="configurator.select.button.text" /></button>
                            </div>
                        </form:form>

                        <!--Delete button in below form-->
                        <form:form action="${deleteItem}" method="post" class="step2ConfigForm ctabox-dlt-form dsp-none">
                            <input type="hidden" name="selectedMobile" id="selectedMobile" value="${item.code}"/>
                            <input type="hidden" value="ACQUISITION" id="processType" name="processType" />
                            <div class="ctabox-btn ctabox-dltbtn--js">
                                <button type="submit" class="btn btn-primary text-uppercase"><spring:theme code="configurator.iconCartRemove" /></button>
                            </div>
                        </form:form>
                    </div>
                </div>
            </c:forEach>
        </div>
        <div class="visible-xs step2-infobar">
            <c:choose>
                <c:when test="${not empty selectedBundle}">
                    <div class="col-sm-2 next-button">
                        <form:form action="${nextStep}" method="post" class="step2ConfigFormNext">
                            <input type="hidden" name="selectedMobile" id="selectedMobile" value="${selectedMobile.code}"/>
                            <input type="hidden" value="ACQUISITION" id="processType" name="processType" />
                            <button id="addToCartButton" type="submit" class="btn btn-next"><spring:theme code="configurator.addtocart.button.text" /></button>
                        </form:form>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="col-sm-2 next-button">
                        <form:form action="${nextStep}" method="post" class="step2ConfigFormNext">
                            <input type="hidden" name="selectedMobile" id="selectedMobile" value="${item.code}"/>
                            <button id="addToCartButton" type="submit" class="btn btn-next"><spring:theme code="configurator.addtocart.button.text" /></button>
                        </form:form>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
        
        <div class="policy-text">
            <spring:theme code="usage.policy.text" htmlEscape="false" />
        </div>
        <secureFooter:footerSecuresiteLogo />
    </div>

</div>

<input type="hidden" class="productPageGtmData"
   <c:if test="${selectedBundle.name ne null}"> data-attr-name="${selectedBundle.name}" </c:if>
   <c:if test="${selectedBundle.code ne null}"> data-attr-id="${selectedBundle.code}" </c:if>
   <c:choose>
        <c:when test="${selectedBundle.productOfferingPrices[0].recurringChargeEntries[0].discountedPrice.formattedValue ne null}">
            data-attr-price="${selectedBundle.productOfferingPrices[0].recurringChargeEntries[0].discountedPrice.formattedValue}"
        </c:when>
        <c:otherwise>
            data-attr-price="${selectedBundle.price.formattedValue}"
        </c:otherwise>
   </c:choose>
   <c:if test="${selectedBundle.categories[0].name ne null}"> data-attr-brand="${selectedBundle.categories[0].name}" data-attr-category="${selectedBundle.categories[0].name}" </c:if>
   <c:if test="${selectedBundle.categories[0].parentCategoryName ne null}"> data-attr-primarycategory="${selectedBundle.categories[0].parentCategoryName}" </c:if>
   <c:if test="${selectedBundle.price.currencyIso ne null}"> data-attr-currency="${selectedBundle.price.currencyIso}" </c:if>
/>
<input type="hidden" class="productPageGtmData"
   <c:if test="${selectedMobile.name ne null}"> data-attr-name="${selectedMobile.name}" </c:if>
   <c:if test="${selectedMobile.code ne null}"> data-attr-id="${selectedMobile.code}" </c:if>
   <c:choose>
       <c:when test="${selectedMobile.productOfferingPrices[0].recurringChargeEntries[0].discountedPrice.formattedValue ne null}">
           data-attr-price="${selectedMobile.productOfferingPrices[0].recurringChargeEntries[0].discountedPrice.formattedValue}"
       </c:when>
       <c:otherwise>
           data-attr-price="${selectedMobile.price.formattedValue}"
       </c:otherwise>
  </c:choose>
   <c:if test="${selectedMobile.categories[0].name ne null}"> data-attr-brand="${selectedBundle.categories[0].name}" data-attr-category="${selectedMobile.categories[0].name}" </c:if>
   <c:if test="${selectedMobile.categories[0].parentCategoryName ne null}"> data-attr-primarycategory="${selectedMobile.categories[0].parentCategoryName}" </c:if>
   <c:if test="${selectedMobile.price.currencyIso ne null}"> data-attr-currency="${selectedMobile.price.currencyIso}" </c:if>
/>