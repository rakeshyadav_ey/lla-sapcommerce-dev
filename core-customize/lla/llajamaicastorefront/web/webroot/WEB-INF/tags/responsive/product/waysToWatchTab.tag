<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="tab-details">
	<ycommerce:testId code="productDetails_content_label">
		<div class="tab-content-details">
			<p class="tab-data-head">Flow to Go:</p>
	        <span class="tab-data-desc">Flow ToGo is an innovative mobile application, designed to enhance your entertainment experience.You'll get the ability to watch popular Live TV shows and access specialized programming, all from a Web Browser,Smartphone or Tablet.
			</span>
		</div>
		<div class="tab-content-details">
		    <p class="tab-data-head">Flow on Demand:</p>
            <span class="tab-data-desc">With Flow On Demand, you can make a selection from a huge library of movies, music and television shows any time you want.
			</span>
	   </div>
	   <div class="tab-content-details">
		     <p class="tab-data-head">HBO Go:</p>
             <span class="tab-data-desc">Features and benefits</span>
       </div>
	</ycommerce:testId>
</div>