<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="tab-details">
	<ycommerce:testId code="productDetails_content_label">
		<div class="tab-channel-list">
            <h3 class="channel-head">Channel list</h3>
            <div class="channel-list">
              <ul>
	              <c:forEach items="${product.channelList}" var="channel" varStatus="i">
	                  <div class="col-xs-12 col-sm-4">
	                  	<li><span class="channel-num">${channel.channelNumber}</span> ${channel.channelName}</li>
	                  </div>
	              </c:forEach>
              </ul>
            </div>
        </div>
	</ycommerce:testId>
</div>