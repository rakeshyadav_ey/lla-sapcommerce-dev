<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<div class="tab-details">
	<ycommerce:testId code="productDetails_content_label">
		<div class="product-classifications">
        	<div class="tab-channel-list">
	        	<div class="channel-list">
	                <div class="col-xs-4 col-sm-3">
		                <ul>
		                	<li>Albania<li>
		                	<li>Anguilla</li>
		                	<li>Antigua</li>
		                	<li>Australia</li>
		                	<li>Bahamas</li>
		                	<li>Barbados</li>
		                	<li>Belize</li>
		                	<li>Bonaire</li>
		                	<li>BVI</li>
		                	<li>Canada</li>
		                	<li>Cayman Islands</li>
		                	<li>Costa Rica</li>
		                	</li>Curacao</li>
		                	<li>Czech Republic</li>
		                	<li>Dominica</li>
		                	<li>French Guiana</li>
		                </ul>
	                </div>
	                <div class="col-xs-4 col-sm-3">
		                <ul>
		                	<li>Germany<li>
		                	<li>Ghana</li>
		                	<li>Grenada</li>
		                	<li>Guadeloupe</li>
		                	<li>Guyana</li>
		                	<li>Ireland</li>
		                	<li>Italy</li>
		                	<li>Malta</li>
		                	<li>Martinique</li>
		                	<li>Montserrat</li>
		                	<li>Netherlands</li>
		                	<li>New Zealand</li>
		                	<li>Panama</li>
		                	<li>Puerto Rico</li>
		                	</li>Romania</li>
		                	<li>Saba</li>
		                </ul>
	                </div>
	                <div class="col-xs-4 col-sm-3">
		                <ul>
		                	<li>Spain<li>
		                	<li>St.kitts</li>
		                	<li>St.Lucia</li>
		                	<li>St.Vincent</li>
		                	<li>St.Barthelemy</li>
		                	<li>St.Eustatius (Statia)</li>
		                	<li>St.Maarten (Dutch)</li>
		                	<li>St.Martin (French)</li>
		                	<li>Trinidad</li>
		                	<li>Turkey</li>
		                	<li>Turks & Caicos</li>
		                	<li>UK</li>
		                	<li>USVI</li>
		                	<li>USA</li>
		                </ul>
	                </div>
	            </div>
        	</div>
        </div>
        </div>
	</ycommerce:testId>
</div>