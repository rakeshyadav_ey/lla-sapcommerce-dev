<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>


<div class="secureSiteFooter">	
	<div class="securesite-text">
		<span><spring:theme code="secure.site.text" htmlEscape="false" /></span>
		<span class="securesite-logo">
		    <img src="${contextPath}/_ui/responsive/theme-jamaica/images/atlantic_commerce_logo.svg" alt="">
		    <img src="${contextPath}/_ui/responsive/theme-jamaica/images/dssl_logo.svg" alt="">
	    </span>
	</div>
</div>
