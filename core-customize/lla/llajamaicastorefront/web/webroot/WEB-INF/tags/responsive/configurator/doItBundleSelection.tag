<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="secureFooter" tagdir="/WEB-INF/tags/responsive/common/footer"%>

<c:url value="/configureProduct/saveBundle" var="nextStep"/>
<c:url value="/configureProduct/mobilePlan" var="nextStepWithoutSelection"/>
<c:url value="/configureProduct/deleteBundle" var="deleteItem"/>
<c:set var="categoryName" value="${categoryName}"/>

<div class="configurator-body">
    <div class="step1-infobar">
        <div class="container">
            <div class="info-msg">
                <div class="col-xs-12 col-sm-10">
                    <p class="wcText"><spring:theme code="configurator.step1.infobar.text" htmlEscape="false" /></p>
                </div>
                <!--<div class="next-button">
                    <form:form action="${nextStepWithoutSelection}" method="get">
                        <button type="submit" class="btn btn-next">
                            <spring:theme code="configurator.next.button.text" />
                        </button>
                    </form:form>
                </div>-->
            </div>
        </div>
    </div>

    <div class="container">
        <div class="ctabox-container">
            <c:forEach var="item" items="${productList}" varStatus="productIndex">
                <div class="col-xs-12 col-sm-6 col-md-3 cta-card" data-productcode="${item.code}" data-productname="${item.name}" data-noofchannels="${item.channelList.size()}">
                    <div class="ctabox <c:if test="${selectedBundle == item.code}">selected</c:if>" >
                        <div class="ctabox__selected--blur"></div>
                        <c:if test="${item.popularFlag}">
                            <div class="popular text-capitalize"><spring:theme code="configurator.popular.best.value" /></div>
                        </c:if>
                        <div class="content">
                            <div class="bundle">
                                <p class="bundleName">${item.name}</p>
                                <c:forEach var="offeringPrices" items="${item.productOfferingPrices}" varStatus="productIndex">
                                    <c:if test="${null ne offeringPrices.recurringChargeEntries}">
                                        <c:set var="recurringCharges" value="${offeringPrices.recurringChargeEntries[0]}"/>
                                    </c:if>
                                </c:forEach>
                                
                                    <c:choose>
                                    	<c:when test="${not empty recurringCharges.discountedPrice}">
                                    		 <p class="oldPrice"><span class="linethrough">Was ${currentCurrency.symbol}<fmt:formatNumber type = "number" maxFractionDigits = "0" value = "${recurringCharges.price.value}" /> + <spring:theme code="configurator.cgt" /></span></p>
                                             <p class="newPrice">Now  ${recurringCharges.discountedPrice.formattedValue} + <spring:theme code="configurator.cgt" />&#42;</p>
                                    	</c:when>
                                    	<c:otherwise>
                                             <p class="newPrice"> ${recurringCharges.price.formattedValue} + <spring:theme code="configurator.cgt" />&#42;</p>
                                    	</c:otherwise>
                                    </c:choose>
                                
                            </div>
                            <hr>
                        </div>
                        <div class="specification">
                            <c:forEach var="specCharValue" items="${item.productSpecCharValueUses}" varStatus="index">
                                <p class="internet-spec ${specCharValue.productSpecCharacteristicValues[0].id}">
                                    ${specCharValue.description}
                                </p>
                            </c:forEach>
                        </div>
                        <c:url value="/p/${item.code}" var="pdpUrl"/>

                        <!--I want it button in below form-->
                        <form:form action="${nextStep}" method="post" class="step1ConfigForm ctabox-want-form">
                            <input type="hidden" name="selectedBundle" id="selectedBundle" value="${item.code}">
                            <div class="ctabox-btn ctabox-wantbtn--js">
                                <button type="submit" class="btn btn-primary"><spring:theme code="configurator.select.button.text" /></button>
                            </div>
                        </form:form>
                        <!--Delete button in below form-->
                        <form:form action="${deleteItem}" method="post" class="step1ConfigForm ctabox-dlt-form dsp-none">
                            <input type="hidden" name="selectedBundle" id="selectedBundle" value="{item.code}">
                            <input type="hidden" name="stepId" id="stepId" value="1">
                            <div class="ctabox-btn ctabox-dltbtn--js">
                                <button type="submit" class="btn btn-primary text-uppercase"><spring:theme code="configurator.iconCartRemove" /></button>
                            </div>
                        </form:form>
                    </div>
                </div>
            </c:forEach>
        </div>

        <div class="policy-text">
            <spring:theme code="usage.policy.text" htmlEscape="false" />
        </div>
        <secureFooter:footerSecuresiteLogo />
    </div>

    <div class="hidden">
		<div class="headline">
			<div class="channel-popup-title">
				<div class="bundleName"></div>
				<div class="no-of-channels"><b></b> <spring:theme code="text.bundle.channels"/></div>
			</div>
		</div>
		<div id="channels-popup" class="channels-popup">
			<div class="channels-list"></div>
			<br>
			<button class="btn btn-primary btn-block js-channels-btn"><spring:theme code="text.buyIt.now.button"/></button>
		</div>
	</div>

</div>

