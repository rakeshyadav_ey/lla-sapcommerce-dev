<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="address" required="true" type="de.hybris.platform.commercefacades.user.data.AddressData" %>
<%@ attribute name="storeAddress" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not storeAddress }">
	<div class="label-order label-contact"><spring:theme code="text.account.order.contact"/></div>
	<c:if test="${not empty address.title}">
	    ${fn:escapeXml(address.title)}&nbsp;
	</c:if>
	${fn:escapeXml(address.firstName)}&nbsp;${fn:escapeXml(address.lastName)}
	<br>
</c:if>
<div class="label-order label-contact"><spring:theme code="text.account.order.address"/></div>
${fn:escapeXml(address.line1)}
<c:if test="${not empty address.line2}">
	<br>
	${fn:escapeXml(address.line2)}
</c:if>
<c:if test="${not empty address.area}">
	<br>
	${fn:escapeXml(address.area)}
</c:if>
<br>
${fn:escapeXml(address.town)}&nbsp;${fn:escapeXml(address.region.name)}
<br>
${fn:escapeXml(address.country.name)}&nbsp;${fn:escapeXml(address.postalCode)}
<br/>
${fn:escapeXml(address.phone)}

<div class="label-order label-contact">
	<spring:theme code="text.account.order.contactEmail"/><br>
</div>

