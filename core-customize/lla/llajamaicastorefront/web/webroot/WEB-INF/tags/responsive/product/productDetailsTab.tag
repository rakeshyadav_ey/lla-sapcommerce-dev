<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="tab-details">
	<ycommerce:testId code="productDetails_content_label">
		<div class="product-classifications">
        	<c:if test="${not empty product.productSpecCharValueUses}">
        		<c:forEach items="${product.productSpecCharValueUses}" var="productSpecCharValueUse">
					<c:forEach items="${productSpecCharValueUse.productSpecCharacteristicValues}" var="productSpecCharacteristicValue">
						<div class="tab-content-details">
						    <p class="tab-data-head">${fn:escapeXml(productSpecCharacteristicValue.ctaSpecDescription)}
						    </p>
							<span class="tab-data-desc">${productSpecCharValueUse.description}</span>
						</div>
					</c:forEach>
        		</c:forEach>
        		
			  <c:if test="${productCategories[0].code == 'postpaid'}" var="booleanValue">
				<div class="tab-content-details">
				    <p class="tab-data-head"><spring:theme code="pdp.postpaid.tab.headtext1" text="Fair Usage Policy (FUP)"/></p>
					<span class="tab-data-desc"><spring:theme code="pdp.postpaid.tab.text1" text="Fair Usage Policy (FUP)"/>&nbsp;&nbsp;<a href="https://discoverflow.co/jamaica/terms-and-conditions/fair-usage-policy">Click Here...</a></span>
				</div>
			  </c:if>
        	</c:if>
        	<c:if test="${productCategories[0].code == 'broadband'}" var="booleanValue">
        	    <div class="tab-content-details">
	        	    <p class="tab-data-head"><spring:theme code="pdp.broadband.tab.headtext1" text="This is what you get"/></p>
	        	    <span class="tab-data-desc"><spring:theme code="${product.summary}"/></span>
        	    </div>
        	</c:if>
        	<c:if test="${productCategories[0].code == '3Pbundles' && product.code == 'doitnow'}" var="booleanValue">
                <div class="tab-content-details">
                    <p class="tab-data-head"><a href="https://discoverflow.co/jamaica/do-it-now-terms-and-conditions"><spring:theme code="pdp.bundles.tab.termsandconditions" text="Terms and Conditions apply"/></a></p>
                </div>
            </c:if>
            <c:if test="${productCategories[0].code == '3Pbundles' && product.code == 'doitmore'}" var="booleanValue">
                <div class="tab-content-details">
                    <p class="tab-data-head"><a href="https://discoverflow.co/jamaica/do-it-more-terms-and-conditions"><spring:theme code="pdp.bundles.tab.termsandconditions" text="Terms and Conditions apply"/></a></p>
                </div>
            </c:if>
            <c:if test="${productCategories[0].code == '3Pbundles' && product.code == 'doitall'}" var="booleanValue">
                <div class="tab-content-details">
                    <p class="tab-data-head"><a href="https://discoverflow.co/jamaica/do-it-all-terms-and-conditions"><spring:theme code="pdp.bundles.tab.termsandconditions" text="Terms and Conditions apply"/></a></p>
                </div>
            </c:if>
            <c:if test="${productCategories[0].code == '3Pbundles' && product.code == 'doit'}" var="booleanValue">
                <div class="tab-content-details">
                    <p class="tab-data-head"><a href="https://discoverflow.co/jamaica/do-it-terms-and-condition"><spring:theme code="pdp.bundles.tab.termsandconditions" text="Terms and Conditions apply"/></a></p>
                </div>
            </c:if>
        </div>
	</ycommerce:testId>
</div>