<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="categoryName" value="${fn:escapeXml(productCategories[productCategories.size()-2].name)}"/>
<input type="hidden" id="productPageGtmData"
    <c:if test="${categoryName ne null}"> data-attr-vpPath="${categoryName}" </c:if>
    <c:if test="${product.name ne null}"> data-attr-name="${fn:escapeXml(product.name)}" </c:if>
    <c:if test="${product.code ne null}"> data-attr-id="${product.code}" </c:if>
    <c:if test="${product.price.formattedValue ne null}"> data-attr-price="${product.price.formattedValue}" </c:if>
    <c:if test="${product.categories[0].name ne null}"> data-attr-brand="${product.categories[0].name}" data-attr-category="${product.categories[0].name}" </c:if>
    <c:if test="${product.price.currencyIso ne null}"> data-attr-currency="${product.price.currencyIso}" </c:if>
>

<div class="pdp-custom">
<div class="product-details page-title">
	<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
        <c:if test="${productCategories.size() > 2}">
        	    <div class="name">${fn:escapeXml(productCategories[productCategories.size()-2].name)}</div>
        	</c:if>
	</ycommerce:testId>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-6 col-lg-4">
		<product:productImagePanel galleryImages="${galleryImages}" />
	</div>
	<div class="clearfix hidden-sm hidden-md hidden-lg"></div>
	<div class="col-sm-6 col-lg-8">
		<div class="product-main-info">
			<div class="row">
				<div class="col-lg-7">
					<div class="product-details">
					    <div class="name">${fn:escapeXml(product.name)}</div>
						<div class="description">${ycommerce:sanitizeHTML(product.summary)}</div>
						<product:productPromotionSection product="${product}"/>
						<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
							<product:productPricePanel product="${product}" />
						</ycommerce:testId>	
						<cms:pageSlot position="AddToCart" var="component" element="div" class="page-details-variants-select">
							<cms:component component="${component}" element="div" class="yComponentWrapper page-details-add-to-cart-component"/>
						</cms:pageSlot>					
					</div>
				</div>

				<div class="col-sm-12 col-md-9 col-lg-5">
					<cms:pageSlot position="VariantSelector" var="component" element="div" class="page-details-variants-select">
						<cms:component component="${component}" element="div" class="yComponentWrapper page-details-variants-select-component"/>
					</cms:pageSlot>
					
				</div>
			</div>
		</div>

	</div>
</div>
</div>