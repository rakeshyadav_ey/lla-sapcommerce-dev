<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="tab-details">
	<ycommerce:testId code="productDetails_content_label">
	<div class="tab-content-details">
		<table class="table table-responsive tab-plan-rates">
			<thead>
				<tr>
					<th>Destination</th>
					<th>Rate per<br>minute + GCT</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Local Rates</td>
					<td>$1.05</td>
				</tr>
				<tr>
					<td>FLOW Home Phone (formerly Columbus Communications' Flow)</td>
					<td>$1.05</td>
				</tr>
				<tr>
					<td>FLOW Home Phone (formerly LIME)</td>
					<td>$1.05</td>
				</tr>
				<tr>
					<td>FLOW Home Phone (formerly LIME)</td>
					<td>$1.05</td>
				</tr>
				<tr>
					<td>FLOW Home Phone (NEW)</td>
					<td>$1.05</td>
				</tr>
				<tr>
					<td>FLOW Home Phone (NEW)</td>
					<td>$1.05</td>
				</tr>
				<tr>
					<td>Digicel Fixed Line</td>
					<td>$2.55</td>
				</tr>
				<tr>
					<td>Other Local Operators (OLO) Fixed Line</td>
					<td>$2.55</td>
				</tr>
				<tr>
					<td>Other Local Operators (OLO) Fixed Line</td>
					<td>$2.55</td>
				</tr>
				<tr>
					<td>FLOW (formerly LIME) Mobile</td>
					<td>$2.85</td>
				</tr>
				<tr>
					<td>Digicel Mobile</td>
					<td>$2.85</td>
				</tr>
				<tr>
					<td>Other Local Operators (OLO) Mobile</td>
					<td>$2.85</td>
				</tr>
				<tr>
					<td>Other Local Operators (OLO) Mobile</td>
					<td>$2.85</td>
				</tr>
				<tr>
					<td class="subhead" colspan="2">International Rates</td>
				</tr>
				<tr>
					<td>USA, Canada and UK (landline)</td>
					<td>$11.00</td>
				</tr>
				<tr>
					<td>Caribbean</td>
					<td>$25/$35</td>
				</tr>
				<tr>
					<td>Caribbean</td>
					<td>$25/$35</td>
				</tr>
			</tbody>
		</table> 
		</div>
	</ycommerce:testId>
</div>
