<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true" %>
<%@ attribute name="metaDescription" required="false" %>
<%@ attribute name="metaKeywords" required="false" %>
<%@ attribute name="pageCss" required="false" fragment="true" %>
<%@ attribute name="pageScripts" required="false" fragment="true" %>

<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="analytics" tagdir="/WEB-INF/tags/shared/analytics" %>
<%@ taglib prefix="addonScripts" tagdir="/WEB-INF/tags/responsive/common/header" %>
<%@ taglib prefix="generatedVariables" tagdir="/WEB-INF/tags/shared/variables" %>
<%@ taglib prefix="debug" tagdir="/WEB-INF/tags/shared/debug" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="htmlmeta" uri="http://hybris.com/tld/htmlmeta"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<!DOCTYPE html>
<html lang="${fn:escapeXml(currentLanguage.isocode)}">
<head>
	<title>
		${not empty pageTitle ? pageTitle : not empty cmsPage.title ? fn:escapeXml(cmsPage.title) : 'Accelerator Title'}
	</title>

	<%-- Meta Content --%>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="referrer" content="no-referrer-when-downgrade" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

	<%-- Additional meta tags --%>
	<htmlmeta:meta items="${metatags}"/>

	<%-- Favourite Icon --%>
	<spring:theme code="img.favIcon" text="/" var="favIconPath"/>
	
	<c:choose>
		<%-- if empty webroot, skip originalContextPath, simply use favIconPath --%>
		<c:when test="${fn:length(originalContextPath) eq 1}" >
			<link rel="shortcut icon" type="image/x-icon" media="all" href="${favIconPath}" />
		</c:when>
		<c:otherwise>
			<link rel="shortcut icon" type="image/x-icon" media="all" href="${originalContextPath}${favIconPath}" />
		</c:otherwise>
	</c:choose>

	<%-- CSS Files Are Loaded First as they can be downloaded in parallel --%>
	<template:styleSheets/>
    <analytics:googleTagManagerContainer/>
	<%-- Inject any additional CSS required by the page --%>
	<jsp:invoke fragment="pageCss"/>
	<analytics:analytics/>
	<generatedVariables:generatedVariables/>
</head>

<body class="${pageBodyCssClasses} ${cmsPageRequestContextData.liveEdit ? ' yCmsLiveEdit' : ''} language-${fn:escapeXml(currentLanguage.isocode)}">
	<!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TJ9HQ3W" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

	<%-- Inject the page body here --%>
	<jsp:doBody/>


	<form name="accessiblityForm">
		<input type="hidden" id="accesibility_refreshScreenReaderBufferField" name="accesibility_refreshScreenReaderBufferField" value=""/>
	</form>
	<div id="ariaStatusMsg" class="skip" role="status" aria-relevant="text" aria-live="polite"></div>

	<%-- Load JavaScript required by the site --%>
	<template:javaScript/>
	
	<%-- Inject any additional JavaScript required by the page --%>
	<jsp:invoke fragment="pageScripts"/>

	<%-- Inject CMS Components from addons using the placeholder slot--%>
	<addonScripts:addonScripts/>
	<c:if test="${cmsSite.uid eq 'jamaica'}">
    <c:choose>
      <c:when test="${not empty breadcrumbs}">
        <c:set var="breadcrumbinfo"><spring:theme code="breadcrumb.home.jamaica" />,</c:set>
        <c:forEach items="${breadcrumbs}" var="breadcrumb" varStatus="status">
            <c:set var="breadcrumbinfo">${breadcrumbinfo}${breadcrumb.name}<c:if test="${not status.last}">, </c:if></c:set>
        </c:forEach>
      </c:when>
      <c:otherwise>
        <c:set var="breadcrumbinfo" value="N/A"/>
      </c:otherwise>
    </c:choose>
	<c:set var="categoryName" value="${fn:escapeXml(productCategories[productCategories.size()-2].name)}"/>
    <input type="hidden" id="adobeAnalyticsPageInfo"
        <c:if test="${pageType ne null}"> data-attr-pageName="${pageType}" </c:if>
		<c:if test="${product.name ne null}"> data-attr-name="${fn:escapeXml(product.name)}" </c:if>
        <c:if test="${breadcrumbinfo ne null}"> data-attr-breadcrumbsdata="${breadcrumbinfo}" </c:if>
        <c:if test="${user.customerId ne null}"> data-attr-userid="${user.customerId}" </c:if>
        <c:if test="${user.additionalEmail ne null}"> data-attr-email="${user.additionalEmail}" </c:if>
		<c:if test="${categoryName ne null}"> data-attr-vpPath="${categoryName}" </c:if>
		<c:if test="${user.mobilePhone ne null && user.mobilePhone ne '' }"> data-attr-mobilePhone="${user.mobilePhone}" </c:if>
		<c:if test="${user.customerType ne null && user.customerType ne ''}"> data-attr-customerType="${user.customerType}" </c:if>
        <c:if test="${product.price.formattedValue ne null}"> data-attr-price="${product.price.formattedValue}" </c:if>
    	<c:if test="${product.categories[0].name ne null}"> data-attr-brand="${product.categories[0].name}" data-attr-category="${product.categories[0].name}" </c:if>
    	<c:if test="${product.price.currencyIso ne null}"> data-attr-currency="${product.price.currencyIso}" </c:if>
    />
</c:if>

</body>

<debug:debugFooter/>

</html>
