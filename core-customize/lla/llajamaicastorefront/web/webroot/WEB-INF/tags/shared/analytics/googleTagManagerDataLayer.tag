<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<!-- Google Tag Manager Data Layer -->
<script type="text/javascript">
<c:set var="path" value="" />
<c:forEach items="${breadcrumbs}" var="breadcrumb" varStatus="status">
    <c:set var="path">${path}${breadcrumb.name}<c:if test="${not status.last}">/</c:if></c:set>
</c:forEach>
window.dataLayer = window.dataLayer || [];
<c:if test="${not empty user.customerId}">
    window.dataLayer.push({
        'userId': '${user.customerId}'
    });
</c:if>
<c:choose>
    <c:when test="${pageType == 'PRODUCTSEARCH' || pageType == 'CATEGORY'}">
        window.dataLayer.push({
              'ecommerce': {
                'currencyCode': '${currentCurrency.isocode}',
                'impressions': [
                        <c:forEach items="${searchPageData.results}" var="product" varStatus="loop">
                            {
                              'id': '${product.code}',
                              'name': '${product.name}',
                              'price': '${product.price.value}',<c:if test="${pageType == 'CATEGORY'}">
                              'category': '${path}',</c:if>
                              'list': '${path}',
                              'position': '${loop.index + 1}'
                            }<c:if test="${!loop.last}">,</c:if>
                        </c:forEach>
                    ]
                }
        });
    </c:when>
    <%--<c:when test="${pageType == 'PRODUCT'}">
        <window.dataLayer.push({
              'ecommerce': {
                'currencyCode': '${currentCurrency.isocode}',
                'detail': {
                  'actionField': {'list': '${path}'}, 
                  'products': [{
                      'id': '${product.code}',
                      'name': '${product.name}',
                      'price': '${product.price.value}',
                      'brand':     <c:choose>
                                        <c:when test="${not empty product.categories}">
                                            '${ycommerce:encodeJavaScript(product.categories[fn:length(product.categories) - 1].name)}'
                                        </c:when>
                                        <c:otherwise>
                                            ''
                                        </c:otherwise>
                                    </c:choose>
                   }]
                 }
               }
        });
    </c:when>--%>
    <c:when test="${pageType == 'CART' || pageType == 'CHECKOUT'}">
        window.mediator.subscribe('onCheckout_gtm', function(data) {
            if (data.checkoutStep)
            {
                trackOnCheckout_gtm(data.checkoutStep, data.option);
            }
        });
        function trackOnCheckout_gtm(checkoutStep, option) {
            window.dataLayer.push({
                'event': 'checkout',
                'ecommerce': {
                  'checkout': {
                    'products': [
                           <c:forEach items="${cartData.entries}" var="entry" varStatus="loop">
                        {
                            'id': '${ycommerce:encodeJavaScript(entry.product.code)}',
                            'name': '${ycommerce:encodeJavaScript(entry.product.name)}', 
                            'price': Number(Math.round((parseFloat('${entry.totalPrice.value}')/parseFloat('${entry.quantity}'))+'e2')+'e-2'),
                            'brand': <c:choose>
                                             <c:when test="${not empty entry.product.categories}">
                                                 '${ycommerce:encodeJavaScript(entry.product.categories[fn:length(entry.product.categories) - 1].name)}',
                                             </c:when>
                                             <c:otherwise>
                                                 '',
                                              </c:otherwise>
                                         </c:choose>
                            'quantity': '${entry.quantity}'
                        }<c:if test="${not loop.last}">,</c:if>
                        </c:forEach> 
                      ]
                 }
               }
             });
        }
    </c:when>
    <c:when test="${pageType == 'ORDERCONFIRMATION'}">
        <c:if test="${not empty couponCodes}">
            <c:set var="coupons" value="" />
            <c:forEach items="${couponCodes}" var="couponCode" varStatus="loop">
                <c:set var="coupons">${couponCode}<c:if test="${!loop.last}">|</c:if></c:set>
            </c:forEach>
        </c:if>
        window.dataLayer.push({
              'ecommerce': {
                'purchase': {
                  'actionField': {
                    'id': '${ycommerce:encodeJavaScript(orderData.code)}',
                    'affiliation': '${ycommerce:encodeJavaScript(siteName)}',
                    'revenue': '${orderData.totalPrice.value}',
                    'tax':'${orderData.totalTax.value}',
                    'shipping': '${orderData.deliveryCost.value}',
                    'coupon': '${coupons}'
                  },
                  'products': [
                           <c:forEach items="${orderData.entries}" var="entry" varStatus="loop">
                        {
                            'id': '${ycommerce:encodeJavaScript(entry.product.code)}',
                            'name': '${ycommerce:encodeJavaScript(entry.product.name)}', 
                            'price': Number(Math.round((parseFloat('${entry.totalPrice.value}')/parseFloat('${entry.quantity}'))+'e2')+'e-2'),
                            'brand': <c:choose>
                                            <c:when test="${not empty entry.product.categories}">
                                                '${ycommerce:encodeJavaScript(entry.product.categories[fn:length(entry.product.categories) - 1].name)}',
                                            </c:when>
                                            <c:otherwise>
                                                '',
                                             </c:otherwise>
                                        </c:choose>
                            'quantity': '${entry.quantity}'
                        }<c:if test="${not loop.last}">,</c:if>
                        </c:forEach> 
                  ]
                }
              }
            });
    </c:when>
</c:choose>
window.mediator.subscribe('productClick_gtm', function(data) {
    if (data.productCode && data.productName && data.productPrice && data.productUrl)
    {
        trackProductClick_gtm(data.productCode, data.productName, data.productPrice, data.productUrl);
    }
});
function trackProductClick_gtm(productCode, productName, productPrice, productUrl) {
    window.dataLayer.push({
        'event': 'productClick',
            'ecommerce': {
              'click': {
                'actionField': {'list': '${path}'},
                    'products': [{
                          'id': productCode,
                          'name': productName,
                          'price': productPrice
                      }]
               }
          },
        'eventCallback': function() {
            if (productUrl) {
                document.location = productUrl;
             }
          }
    });
}
window.mediator.subscribe('addToCart_gtm', function(data) {
 
        trackAddToCart_gtm(data.productCode, data.productName, data.productPrice, data.quantityAdded);
    
});
function trackAddToCart_gtm(productCode, productName, productPrice, quantityAdded) {
    window.dataLayer.push({
        'event': 'addToCart',
        'ecommerce': {
            'currencyCode': '${currentCurrency.isocode}',
            'add': {                            
                'products': [{
                      'id': productCode,
                      'name': productName,
                      'price': productPrice,
                     'quantity': Number(quantityAdded)
                 }]
            }
        }
    });
}
window.mediator.subscribe('removeFromCart_gtm', function(data) {
    if (data.productCode && data.productName && data.productPrice && data.quantityRemoved)
    {
        trackRemoveFromCart_gtm(data.productCode, data.productName, data.productPrice, data.quantityRemoved);
    }
});
function trackRemoveFromCart_gtm(productCode, productName, productPrice, quantityRemoved) {
    window.dataLayer.push({
        'event': 'removeFromCart',
          'ecommerce': {
            'currencyCode': '${currentCurrency.isocode}',
            'remove': {                            
                  'products': [{
                      'id': productCode,
                      'name': productName,
                      'price': productPrice,
                      'quantity': Number(quantityRemoved)
                   }]
            }
          }
    });
}
window.mediator.subscribe('onBannerClick_gtm', function(data) {
    if (data.bannerId && data.bannerName && data.bannerCreative && data.bannerPos && data.bannerUrl)
    {
        trackOnBannerClick_gtm(data.bannerId, data.bannerName, data.bannerCreative, data.bannerPos, data.bannerUrl);
    }
});
function trackOnBannerClick_gtm(bannerId, bannerName, bannerCreative, bannerPos, bannerUrl) {
    window.dataLayer.push({
        'event': 'promotionClick',
        'ecommerce': {
           'promoClick': {
             'promotions': [
              {
                'id': bannerId, 
                'name': bannerName,
                'creative': bannerCreative,
                'position': bannerPos
              }]
           }
        },
        'eventCallback': function() {
             if (bannerUrl) {
                 document.location = bannerUrl;
             }
        }
    });
}
</script>
<!-- End Google Tag Manager Data Layer -->