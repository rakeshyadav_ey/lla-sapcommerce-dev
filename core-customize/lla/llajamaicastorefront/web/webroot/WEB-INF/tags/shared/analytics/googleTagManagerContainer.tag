<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- Google Optimize -->
<script src="https://www.googleoptimize.com/optimize.js?id=OPT-PLZMDBV"></script>
<!-- End Google Optimize -->

<!-- Google Tag Manager -->
    <script>
    var googleTagManagerTrackingId = '${ycommerce:encodeJavaScript(googleTagManagerTrackingId)}';
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore
    (j,f);
    })(window,document,'script','dataLayer', googleTagManagerTrackingId);</script>
    <!-- End Google Tag Manager -->
     <c:choose>
    	<c:when test="${isProductionSite}">
    	  	<script src="//assets.adobedtm.com/cafe14ecc71b/bba3e3806851/launch-12c1aa1a43b3.min.js" async></script>
    	</c:when>
    	<c:otherwise>
    	    <script src="//assets.adobedtm.com/cafe14ecc71b/bba3e3806851/launch-fad97666f467-development.min.js" async></script>
    	</c:otherwise>
    </c:choose>
<!-- End Google Tag Manager -->