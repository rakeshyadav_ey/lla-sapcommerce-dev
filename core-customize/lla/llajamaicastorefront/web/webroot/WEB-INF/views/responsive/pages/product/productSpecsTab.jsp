<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="tabhead">
	<a href="">${fn:escapeXml(title)}</a> <span class="glyphicon"></span>
</div>
<div class="tabbody">
	<div class="container-lg">
		<div class="row">
			<div class="col-sm-10 col-lg-6 col-sm-offset-1 col-md-offset-3 col-lg-offset-3">
				<div class="tab-container">
					<product:productDetailsClassifications product="${product}" />
				</div>
			</div>
		</div>
	</div>
</div>

