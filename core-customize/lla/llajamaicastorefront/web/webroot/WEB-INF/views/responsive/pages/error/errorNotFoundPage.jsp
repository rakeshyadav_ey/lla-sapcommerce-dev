<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<template:errorpage>
	
	<div class="error-content">
		<div class="error-message text-center">
			<h1>
				<spring:theme code="error.404.page.message" text="Sorry, we can not find the page you are looking for" />
			</h1>
			<h4 class="error-description">
				<spring:theme code="error.404.page.message1" text="You might be interested in below offerings. Please check for it" />
			</h4>
			<ul class="site-category">
				<li>
					<a href="https://discoverflow.co/jamaica/tv/flow-evo">
						<img alt="tv" src="https://discoverflow.co/sites/discoverflow.co/files/tv.svg" style="width: 60px;">
						<span><spring:theme code="error.404.page.TV" text="TV" /></span>
					</a>
				</li>
				<li>
					<a href="https://discoverflow.co/jamaica/mobile/overview">
						<img alt="mobile" src="https://discoverflow.co/sites/discoverflow.co/files/mobile.svg" style="width: 60px;">
						<span><spring:theme code="error.404.page.mobile" text="Mobile" /></span>
					</a>
				</li>
				<li>
					<a href="https://discoverflow.co/jamaica/broadband/overview">
						<img alt="broadband" src="https://discoverflow.co/sites/discoverflow.co/files/broadband.svg" style="width: 60px;">
						<span><spring:theme code="error.404.page.broadband" text="Broadband" /></span>
					</a>					
				</li>
				<li>
					<a href="https://discoverflow.co/jamaica/home-phone/overview">
						<img alt="home_phone" src="https://discoverflow.co/sites/discoverflow.co/files/home_phone.svg" style="width: 60px;">
						<span><spring:theme code="error.404.page.homePhone" text="homePhone" /></span>
					</a>
				</li>
				<li>
					<a href="https://discoverflow.co/jamaica/bundles/overview">
						<img alt="bundles" src="https://discoverflow.co/sites/discoverflow.co/files/bundles.svg" style="width: 60px;">
						<span><spring:theme code="error.404.page.bundles" text="bundles" /></span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	
<input type="hidden" id="gtmNotFoundPageError" value="<spring:theme code="gtm.pagenotfound.msg"/>" >
</template:errorpage>