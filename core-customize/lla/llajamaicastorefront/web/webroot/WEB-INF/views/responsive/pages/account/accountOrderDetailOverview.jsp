<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/cart"%>
<%@ taglib prefix="multi-checkout-telco" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="order-confirmation-details"> 
	<div class="row">
	    <div class="col-xs-12 col-sm-6">
		    <ycommerce:testId code="orderDetail_overview_section">
		        <order:accountOrderDetailsOverview order="${orderData}"/>
		    </ycommerce:testId>
	    </div>
	    <div class="col-xs-12 col-sm-6">
			<multi-checkout-telco:deliveryOrderItems cartData="${cartData}" showDeliveryAddress="${showDeliveryAddress}" />
	    </div>
    </div>
</div>

<!-- <c:if test="${not empty orderData.placedBy}">
	<div class="alert alert-info order-placedby">
	<c:choose>
		<c:when test="${not empty agent}">
			<spring:theme code="text.account.order.placedBy" arguments="${orderData.placedBy}"/>
		</c:when>
		<c:otherwise>
			<spring:theme code="text.account.order.placedByText"/>
		</c:otherwise>
	</c:choose>
	</div>
</c:if> -->
