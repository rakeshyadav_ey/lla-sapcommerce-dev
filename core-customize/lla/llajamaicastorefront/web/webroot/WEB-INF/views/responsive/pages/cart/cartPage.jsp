<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="secureFooter" tagdir="/WEB-INF/tags/responsive/common/footer"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<template:page pageTitle="${pageTitle}">

	<cart:cartValidation/>
	<cart:cartPickupValidation/>
	
<div class="container">
    <c:if test="${not empty cartData.rootGroups}">
  <c:if test="${hideProgressBar eq 'false'}">
	    <div class="process-selection-bar">
		    <ul class="selection-state clearfix">
			<li class="previous col-xs-4"><p class="level"><a href="./plp/bundle"><span class="process-state">1</span><span class="process-info"><spring:theme code="selection.level1.text"/></span></a></p></li>
			<li class="active col-xs-4"><p class="level"><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></p></li>
			<li class="col-xs-4"><p class="level"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level3.text"/></span></p></li>
		    </ul>
		</div>
	</c:if>
		<!--<div class="cart-header">
		     <h1><spring:theme code="text.cart" text="My Shopping Cart" /></h1>
		     <p><spring:theme code="text.cart.message.jamaica" text="Products added to you cart" /></p>
        </div>-->
        <!-- <div class="cart-alert-info">
            <div class="alert alert-info">
                <div class="heading"><spring:theme code="text.cart.payment.info" /></div>
                <spring:theme code="text.cart.paymen.info.description" htmlEscape="false" />

            </div>
        </div> -->
    </c:if>
	<!-- <div class="cart-top-bar">
        <div class="text-right">
            <spring:theme var="textHelpHtml" code="text.help" />
            <a href="" class="help js-cart-help" data-help="${fn:escapeXml(textHelpHtml)}">${textHelpHtml}
                <span class="glyphicon glyphicon-info-sign"></span>
            </a>
            <div class="help-popup-content-holder js-help-popup-content">
                <div class="help-popup-content">
                    <strong>${fn:escapeXml(cartData.code)}</strong>
                    <spring:theme var="cartHelpContentVar" code="basket.page.cartHelpContent" htmlEscape="false" />
                    <c:set var="cartHelpContentVarSanitized" value="${ycommerce:sanitizeHTML(cartHelpContentVar)}" />
                    <div>${cartHelpContentVarSanitized}</div>
                </div>
            </div>
		</div>
	</div> -->

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-6">
            <cms:pageSlot position="TopContent" var="feature">
                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
            </cms:pageSlot>
		</div>

	   <c:if test="${not empty cartData.rootGroups}">
           <cms:pageSlot position="CenterLeftContentSlot" var="feature">
                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
           </cms:pageSlot>
        </c:if>
		<div class="col-xs-12 col-sm-12 col-md-6">
		 <c:if test="${not empty cartData.rootGroups}">
            <cms:pageSlot position="CenterRightContentSlot" var="feature">
                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
            </cms:pageSlot>
            <cms:pageSlot position="BottomContentSlot" var="feature">
                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
            </cms:pageSlot>
		</c:if>
        </div>

        <div class="col-xs-12">
            <c:if test="${not empty cartData.rootGroups}">
                <secureFooter:footerSecuresiteLogo />
            </c:if>
        </div>

    </div>

    <div class="row">
		<c:if test="${empty cartData.rootGroups}">
            <cms:pageSlot position="EmptyCartMiddleContent" var="feature">
                <cms:component component="${feature}" element="div" class="yComponentWrapper content__empty"/>
            </cms:pageSlot>
		</c:if>
	</div>

    
    

</div>
</template:page>
