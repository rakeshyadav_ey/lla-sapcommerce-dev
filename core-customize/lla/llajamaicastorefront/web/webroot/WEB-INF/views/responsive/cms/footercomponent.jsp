<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/responsive/common/footer"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<div class="support-section">
    <div class="container">
        <div class="col-xs-12 col-sm-6">
            <div class="headline">
                ${footerTopHeadlineText}
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 icons">
            <c:forEach items="${footerTopLinksList}" var="items">
                <div class="col">
                    <a href="${items.url}"> 
                        <img class="img-responsive" src="${items.media.url}" alt="">
                        <h3>${items.text}</h3>
                        <p>${items.description}</p>
                    </a>
                </div>
            </c:forEach>
        </div>
	</div>
</div>

<div class="footer__top__contact">
    <div class="container">
        <div class="footer_callus"><spring:theme code="footer.callus.text1" htmlEscape="false"/></div>
        <div class="footer_questions"><spring:theme code="footer.callus.text2" htmlEscape="false" /></div>
        <div class="footer_Store"><spring:theme code="footer.flow.store" htmlEscape="false" /></div>
    </div>
</div>
<div class="container-fluid">
    <div class="footer__top">
        <div class="row">
            <div class="footer__left col-xs-12 col-sm-12 col-md-12">
                <div class="footer-grid">
                    <c:forEach items="${navigationNodes}" var="node">
                        <c:if test="${node.visible}">
                            <c:forEach items="${node.links}" step="${component.wrapAfter}"
                                       varStatus="i">

                                <div class="footer__nav--container">
                                    <c:if test="${component.wrapAfter > i.index}">
                                        <div class="title">${fn:escapeXml(node.title)}</div>
                                    </c:if>
                                    <ul class="footer__nav--links">
                                        <c:forEach items="${node.links}" var="childlink"
                                                   begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
                                            <cms:component component="${childlink}"
                                                           evaluateRestriction="true" element="li" class="footer__link"/>
                                        </c:forEach>
                                    </ul>
                                </div>
                            </c:forEach>
                        </c:if>
                    </c:forEach>
                </div>
            </div>
        <%--     <div class="footer__right col-xs-12 col-md-3">
                <c:if test="${showLanguageCurrency}">
                    <div class="row">
                        <div class="col-xs-6 col-md-6 footer__dropdown">
                            <footer:languageSelector languages="${languages}"
                                                     currentLanguage="${currentLanguage}" />
                        </div>
                        <div class="col-xs-6 col-md-6 footer__dropdown">
                            <footer:currencySelector currencies="${currencies}
                                 currentCurrency="${currentCurrency}" />
                        </div>
                    </div>
                </c:if>
            </div> --%>
        </div>
    </div>
</div>

<div class="footer__bottom">
	<!--   Static html designed for LLA Demo start -->
	<div class="footer__middle clearfix">
	    <div class="col-lg-12">
	        <ul class="menu nav">
	            <c:forEach items="${footerBottomLinksList}" var="cmslink">
	             <li class="leaf"><a href="${cmslink.url}">${cmslink.linkName}</a>
	            </c:forEach>
	        </ul>
	    </div>
	</div>
    <div class="footer__copyright">
        <div class="container">${fn:escapeXml(notice)}</div>
    </div>
</div>
