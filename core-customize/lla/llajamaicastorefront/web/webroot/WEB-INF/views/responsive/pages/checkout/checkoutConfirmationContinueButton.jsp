<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<spring:url value="/plp/bundle" var="continueShoppingUrl" scope="session" htmlEscape="false"/>
<spring:url value="/my-account/orders" var="orderUrl" scope="session" htmlEscape="false"/>
<spring:url value="/checkout/multi/sop/capture-payment" var="captureUrl" scope="session" htmlEscape="false"/>

<div class="row">
   <%-- <div class="pull-right col-xs-12 col-sm-6 col-md-3 col-lg-3">
            <div class="continue__shopping">
                <button type="button" id="capture-payment" value="${captureUrl}"> Capture Payment </button>
              </div>
        </div> --%>
    <!-- <div class="pull-right col-xs-12 col-sm-6 col-md-3 col-lg-3">
        <div class="continue__shopping">
            <button class="btn btn-primary btn-block btn--continue-shopping js-continue-shopping-button" data-continue-shopping-url="${fn:escapeXml(orderUrl)}">
                <spring:theme code="checkout.orderConfirmation.gotoOrders"/>
            </button>
        </div>
    </div> -->
    </br>
        <div class="continue__shopping">
            <button class="btn btn--continue-shopping js-continue-shopping-button btn-white primaryBtnWhiteBg" data-continue-shopping-url="${fn:escapeXml(continueShoppingUrl)}">
                <spring:theme code="checkout.orderConfirmation.continueShopping" />
            </button>
        </div>
    
</div>
