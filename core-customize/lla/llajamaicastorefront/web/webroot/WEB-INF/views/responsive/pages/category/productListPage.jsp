<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<template:page pageTitle="${pageTitle}">
	<div class="row">
	    <cms:pageSlot position="BannerSlot" var="feature">
		    <cms:component component="${feature}" />
	    </cms:pageSlot>
	</div>
	<div class="row">
    <cms:pageSlot position="SectionBanner" var="component" element="div" class="container">
        <cms:component component="${component}" />
    </cms:pageSlot>
    </div>
	 <div class="category-head-section">
        	<h3 class="subheadline" align="center"> <spring:theme code="mobile.banner.text"/></h3>
    	</div>
	
	<div class="row">
		<cms:pageSlot position="Section1" var="feature" element="div" class="product-list-section1-slot">
			<cms:component component="${feature}" element="div" class="col-xs-12 yComponentWrapper product-list-section1-component"/>
		</cms:pageSlot>
	</div>
	
	<div class="row">
		<div class="col-xs-3 col-sm-offset-1">
		<div class="empty-design-filter"></div>
			<cms:pageSlot position="ProductLeftRefinements" var="feature" element="div" class="product-list-left-refinements-slot">
				<cms:component component="${feature}" element="div" class="yComponentWrapper product-list-left-refinements-component"/>
			</cms:pageSlot>
		</div>
		<div class="col-sm-12 col-md-7">
			<cms:pageSlot position="ProductListSlot" var="feature" element="div" class="product-list-right-slot">
				<cms:component component="${feature}" element="div" class="product__list--wrapper yComponentWrapper product-list-right-component product-list"/>
			</cms:pageSlot>
		</div>
	</div>

</template:page>
