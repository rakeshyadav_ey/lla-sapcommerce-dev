<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">
    <!-- Static design for home page blocks start -->
    <div class="home-banner">
        <cms:pageSlot position="Section1" var="feature">
            <cms:component component="${feature}" />
        </cms:pageSlot>
        <div class="banner-text">
            <h1>
                <spring:theme code="text.home.banner.text.headline1"/>
            </h1>
            <h3>
                <spring:theme code="text.home.banner.text.headline2"/>
            </h3>
            <span>
                <spring:theme code="text.home.banner.text.headline3"/>
            </span>            
            <a href="plp/postpaid" class="banner-button">
                <spring:theme code="text.home.banner.button"/>
            </a>
        </div>
    </div>

   <cms:pageSlot position="Section5" var="feature" element="div">
   		<cms:component component="${feature}" element="div" class="yComponentWrapper" />
   	</cms:pageSlot>
    <div class="experience-section row">
        <cms:pageSlot position="Section3" var="feature" element="div" class="headline">
            <cms:component component="${feature}" element="div" class="yComponentWrapper" />
        </cms:pageSlot>
        <cms:pageSlot position="Section4" var="feature" element="div">
            <cms:component component="${feature}" element="div" class="experience-col yComponentWrapper"/>
        </cms:pageSlot>
    </div>


	<cms:pageSlot position="Section6" var="feature" element="div">
		<cms:component component="${feature}" element="div" class="yComponentWrapper" />
	</cms:pageSlot>

	<div class="call-contact">
        <div class="container">
            <h2>
                <spring:theme code="call.text" text="Call" />
                <a href="tel:1-800-804-2994"><spring:theme code="call.contact.number" text="1-800-804-2994" /></a>
            </h2>
            <p>
                <spring:theme code="visit.text" text="Visit a" />&nbsp;
                <a href="store-finder"><spring:theme code="flow.store.text" text="Flow Store." /></a>
                <spring:theme code="call.contact.description" text="Or talk to your local sales rep." /> 
            </p>
        </div>
    </div>

</template:page>
