<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
    <div class="customer-additional-details">
       <h4><spring:theme code="additional.profile.details" text="Share your additional personal details" /></h4>
       <spring:url value="/checkout/customer/update/personaldetails/${orderCode}" var="customerProfileUrl" scope="session" htmlEscape="false"/>
       <form:form id="newCustomerAdditionalDetailsForm" name="newCustomerProfileForm" modelAttribute="customerProfileForm" action="${customerProfileUrl}" method="POST">
          <input type="hidden" name="orderCode" path="orderCode" value="${orderCode}" />
          <formElement:formSelectBoxDefaultEnabled idKey="register.title"    labelKey="register.title"   path="titleCode" mandatory="true" skipBlank="false"
               items="${titles}"   tabindex="1" />

          <formElement:formSelectBoxDefaultEnabled idKey="customer.gender"    labelKey="customer.gender"   path="gender" mandatory="true" skipBlank="false"
                     items="${genders}"  tabindex="2"  />

          <formElement:formInputBox idKey="register.lastName" labelKey="register.lastName" path="lastName" inputCSS="form-control" tabindex="3" mandatory="true" />

           <formElement:formInputBox idKey="dob" labelKey="customer.dob" path="dob" inputCSS="form-control"  mandatory="true"  tabindex="4" />
           <button type="submit" class="btn btn-primary">
                <spring:theme code="text.account.profile.update" text="Complete Profile" />
            </button>

        </form:form>
    </div>
