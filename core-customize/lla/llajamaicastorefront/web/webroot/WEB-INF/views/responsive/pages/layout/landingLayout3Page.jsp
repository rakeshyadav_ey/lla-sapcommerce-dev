<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="configurator" tagdir="/WEB-INF/tags/responsive/configurator" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:choose>
   <c:when test="${currentStepID == 2}">
      <c:url value="/configureProduct/selectDoItBundle" var="prevStep"/>
   </c:when>
</c:choose>

<template:page pageTitle="${pageTitle}">
    <div class="container configurator-banner">
       <cms:pageSlot position="Section3" var="feature" element="div" class="row no-margin" >
          <cms:component component="${feature}" element="div" class="no-space col-xs-12 yComponentWrapper"/>
       </cms:pageSlot>
    </div>

    <div class="configurator-steps">
        <div class="container">
            <div class="row">
                <div class="configurator-steps-wrapper">
                    <a href="selectDoItBundle" class="configurator-step <c:if test="${currentStepID == 1}">active</c:if>">
                        <spring:theme code="configurator.step1.headline.text" htmlEscape="false" />
                        <c:if test="${currentStepID == 2}">
                            <c:if test="${not empty selectedBundle}">
                                <div class="name">+${selectedBundle.name}</div>
                            </c:if>
                         </c:if>

                          <c:if test="${currentStepID == 1}">
                             <c:if test="${not empty selectedBundleName}">
                               <div class="name">+${selectedBundleName}</div>
                             </c:if>
                          </c:if>

                    </a>
                    <a href="mobilePlan" class="configurator-step <c:if test="${currentStepID == 2}">active</c:if>">
                        <spring:theme code="configurator.step2.headline.text" htmlEscape="false" />
                    </a>
                    <a href="${prevStep}" class="previous-step hidden-xs <c:if test="${currentStepID != 1}">active</c:if> <c:if test="${currentStepID == 1}">hide</c:if>">
                        <span>
                            <svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M27.0061 13.0674C27.1627 13.2236 27.2869 13.4092 27.3716 13.6135C27.4564 13.8178 27.5 14.0368 27.5 14.258C27.5 14.4792 27.4564 14.6982 27.3716 14.9025C27.2869 15.1068 27.1627 15.2924 27.0061 15.4487L20.4534 22.0013L27.0061 28.554C27.3219 28.8698 27.4993 29.2981 27.4993 29.7446C27.4993 30.1912 27.3219 30.6195 27.0061 30.9353C26.6903 31.251 26.2621 31.4284 25.8155 31.4284C25.3689 31.4284 24.9406 31.251 24.6249 30.9353L16.8731 23.1835C16.7165 23.0273 16.5923 22.8417 16.5076 22.6374C16.4228 22.4331 16.3792 22.2141 16.3792 21.9929C16.3792 21.7717 16.4228 21.5527 16.5076 21.3484C16.5923 21.1441 16.7165 20.9585 16.8731 20.8023L24.6249 13.0505C25.2666 12.4088 26.3475 12.4088 27.0061 13.0674Z" fill="#C4C4C4"/>
                                <circle cx="22" cy="22" r="20.5" stroke="#C4C4C4" stroke-width="3"/>
                            </svg>
                        </span>
                    </a>
                </div>
                <a href="${prevStep}" class="previous-step visible-xs <c:if test="${currentStepID != 1}">active</c:if> <c:if test="${currentStepID == 1}">hide</c:if>">
                    <span>
                        <svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M27.0061 13.0674C27.1627 13.2236 27.2869 13.4092 27.3716 13.6135C27.4564 13.8178 27.5 14.0368 27.5 14.258C27.5 14.4792 27.4564 14.6982 27.3716 14.9025C27.2869 15.1068 27.1627 15.2924 27.0061 15.4487L20.4534 22.0013L27.0061 28.554C27.3219 28.8698 27.4993 29.2981 27.4993 29.7446C27.4993 30.1912 27.3219 30.6195 27.0061 30.9353C26.6903 31.251 26.2621 31.4284 25.8155 31.4284C25.3689 31.4284 24.9406 31.251 24.6249 30.9353L16.8731 23.1835C16.7165 23.0273 16.5923 22.8417 16.5076 22.6374C16.4228 22.4331 16.3792 22.2141 16.3792 21.9929C16.3792 21.7717 16.4228 21.5527 16.5076 21.3484C16.5923 21.1441 16.7165 20.9585 16.8731 20.8023L24.6249 13.0505C25.2666 12.4088 26.3475 12.4088 27.0061 13.0674Z" fill="#C4C4C4"/>
                            <circle cx="22" cy="22" r="20.5" stroke="#C4C4C4" stroke-width="3"/>
                        </svg>
                    </span>
                </a>
            </div>
        </div>
    </div>


   <c:if test="${currentStepID eq '1'}">
      <configurator:doItBundleSelection/>
   </c:if>
   <c:if test="${currentStepID eq '2'}">
      <configurator:mobileSelection/>
   </c:if>

	<div class="container configurator-banner">
		<cms:pageSlot position="Section4" var="feature" element="div"
			class="row no-margin">
			<cms:component component="${feature}" element="div" class="no-space col-xs-12 yComponentWrapper" />
		</cms:pageSlot>
	</div>

</template:page>
