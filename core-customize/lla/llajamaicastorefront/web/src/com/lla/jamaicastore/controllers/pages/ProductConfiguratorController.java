package com.lla.jamaicastore.controllers.pages;

import com.lla.common.addon.idp.strategy.LLALoginStrategy;
import com.lla.facades.channel.data.ChannelData;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import com.lla.core.util.LLACommonUtil;
import com.lla.facades.cart.LLACartFacade;
import com.lla.facades.product.impl.LLATmaProductFacadeImpl;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.subscriptionfacades.data.SubscriptionPricePlanData;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/configureProduct")
public class ProductConfiguratorController extends AbstractPageController {
    private static final Logger LOG = Logger.getLogger(ProductConfiguratorController.class);
    public static final String PRODUCT_LIST = "productList";
    private static final String DOITBUNDLEPAGELABEL="configureProduct/selectDoItBundle";
    private static final String POSTPAIDPAGELABEL="configureProduct/mobilePlan";
    private static final String DOITBUNDLECATEGORY="3Pbundles";
    private static final String POSTPAIDCATEGORY="postpaid";
    private static final String REDIRECT_MOBILE_URL = REDIRECT_PREFIX + "/configureProduct/mobilePlan";
    private static final String REDIRECT_BUNDLE_URL = REDIRECT_PREFIX + "/configureProduct/selectDoItBundle";
    public static final String SELECTED_BUNDLE = "selectedBundle";
    public static final String SELECTED_BUNDLE_NAME = "selectedBundleName";
    public static final String SELECTED_MOBILE = "selectedMobile";
    @Resource(name = "llaCartFacade")
    private LLACartFacade llaCartFacade;
    @Resource(name = "simpleBreadcrumbBuilder")
    private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;
    @Autowired
    private SessionService sessionService;

    @Resource(name = "llaTmaProductFacade")
    private LLATmaProductFacadeImpl llaTmaProductFacade;
    @Resource(name = "llaLoginStrategy")
    private LLALoginStrategy llaLoginStrategy;
    
  	@Resource(name = "llaCommonUtil")
  	private LLACommonUtil llaCommonUtil;

    @ModelAttribute("authUrl")
    public String getAuthorizationUrl(final HttpServletRequest request)
    {
        request.getSession().setAttribute("llaRefererUrl",request.getRequestURL());
        return llaLoginStrategy.authUrlConstructor(request, "auth");
    }
    
    @RequestMapping(value = "/selectDoItBundle", method = RequestMethod.GET)
    public String getDoItBundle(final Model model, final HttpServletRequest request, final HttpServletResponse response)
            throws CMSItemNotFoundException {

        model.addAttribute(PRODUCT_LIST, llaCommonUtil.sortProductOnSequenceId(llaTmaProductFacade.getProductDataForCategory(DOITBUNDLECATEGORY)));
        model.addAttribute("categoryName",DOITBUNDLECATEGORY);
        String selectedDoItPlan= sessionService.getAttribute(SELECTED_BUNDLE);
        if(StringUtils.isNotEmpty(selectedDoItPlan))
        {
            ProductData product = llaTmaProductFacade.getProductForCodeAndOptions(selectedDoItPlan, Arrays.asList(ProductOption.BASIC, ProductOption.CATEGORIES));
            model.addAttribute(SELECTED_BUNDLE_NAME,product.getName());
        }
        model.addAttribute(SELECTED_BUNDLE, sessionService.getAttribute(SELECTED_BUNDLE));
        model.addAttribute("currentStepID", "1");
        model.addAttribute("pageType", PageType.SELECT_POSTPAID_PLAN.name());
        final ContentPageModel contentPage = getContentPageForLabelOrId(DOITBUNDLEPAGELABEL);
        setCMSPageData(model, contentPage);
        return getViewForPage(model);
    }

    @RequestMapping(value="/saveBundle", method=RequestMethod.POST)
    public String saveBundle(@RequestParam(SELECTED_BUNDLE) final String selectedBundle, final Model model, final RedirectAttributes redirectModel)
    {
        sessionService.setAttribute(SELECTED_BUNDLE, selectedBundle);
        return REDIRECT_MOBILE_URL;
    }

    @RequestMapping(value="/deleteBundle", method=RequestMethod.POST)
    public String deleteBundle(@RequestParam(SELECTED_BUNDLE) final String selectedBundle, final Model model, final RedirectAttributes redirectModel)
    {
        sessionService.removeAttribute(SELECTED_BUNDLE);
        sessionService.removeAttribute(SELECTED_MOBILE);
        return REDIRECT_BUNDLE_URL;
    }
    @RequestMapping(value = "/mobilePlan", method = RequestMethod.GET)
    public String getMobile(final Model model, final HttpServletRequest request, final HttpServletResponse response)
            throws CMSItemNotFoundException {
        model.addAttribute(PRODUCT_LIST, llaCommonUtil.sortProductOnSequenceId(llaTmaProductFacade.getProductDataForCategory(POSTPAIDCATEGORY)));
        String selectedDoItPlan= sessionService.getAttribute(SELECTED_BUNDLE);
        model.addAttribute("categoryName",POSTPAIDCATEGORY);
        if(StringUtils.isNotEmpty(selectedDoItPlan))
        {
           model.addAttribute(SELECTED_BUNDLE, llaTmaProductFacade.getProductForCodeAndOptions(selectedDoItPlan, Arrays.asList(ProductOption.BASIC, ProductOption.CATEGORIES, ProductOption.PRICE, ProductOption.PRODUCT_OFFERING_PRICES)));
        }
        String selectedMobile= sessionService.getAttribute(SELECTED_MOBILE);
        if(StringUtils.isNotEmpty(selectedMobile)){
            model.addAttribute(SELECTED_MOBILE, llaTmaProductFacade.getProductForCodeAndOptions(selectedMobile, Arrays.asList(ProductOption.BASIC,  ProductOption.CATEGORIES, ProductOption.PRICE, ProductOption.PRODUCT_OFFERING_PRICES)));
        }
        model.addAttribute("currentStepID", "2");
        model.addAttribute("pageType", PageType.SELECT_MOBILE_PLAN.name());
        final ContentPageModel contentPage = getContentPageForLabelOrId(POSTPAIDPAGELABEL);
        setCMSPageData(model, contentPage);
        return getViewForPage(model);
    }

    /**
     * Gets channels data.
     *
     * @param productCode the product code
     * @return the channels data
     */
    @RequestMapping(value = "/getChannelData", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,List<ChannelData>> getChannelsData(@RequestParam("itemCode") final String productCode)
    {
         return llaTmaProductFacade.fetchChannelData(productCode);
    }

    @RequestMapping(value = "/saveMobilePlan", method = RequestMethod.POST)
    public String saveMobilePlan(@RequestParam(SELECTED_MOBILE) final String selectedMobile, final Model model, final RedirectAttributes redirectModel)
    {
        sessionService.setAttribute(SELECTED_MOBILE, selectedMobile);
        return REDIRECT_MOBILE_URL;
    }

    @RequestMapping(value="/deleteMobile", method=RequestMethod.POST)
    public String deleteMobile(@RequestParam(SELECTED_MOBILE) final String selectedMobile, final Model model, final RedirectAttributes redirectModel)
    {
        sessionService.removeAttribute(SELECTED_MOBILE);
        return REDIRECT_MOBILE_URL;
    }

    @RequestMapping(value = "/addToCart", method = RequestMethod.POST)
    public String addToCart(@RequestParam("selectedMobile") final String selectedMobilePlan,
                                 @RequestParam(value = "processType", required = false) final String processType,
                                 @RequestParam(value = "plan", required = false) final String plan,
                                 @RequestParam(value = "quantity", required = false, defaultValue = "1") final long quantity,
                                 @RequestParam(value = "rootBpoCode", required = false) final String rootBpoCode,
                                 @RequestParam(value = "cartGroupNo", required = false, defaultValue = "-1") final int cartGroupNo,
                                 @RequestParam(value = "subscriptionTermId", required = false) final String subscriptionTermId,
                                 @RequestParam(value = "subscriberId", required = false) final String subscriberId,
                                 @RequestParam(value = "subscriberBillingId", required = false) final String subscriberBillingId,
                                 final Model model, final RedirectAttributes redirectModel) throws CommerceCartModificationException {
        final String selectedBundle = sessionService.getAttribute(SELECTED_BUNDLE);
        if (llaCartFacade.hasSessionCart() && llaCartFacade.getSessionCart().getEntries().size() > 0) {
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.error.multiple.bundle");
            return REDIRECT_MOBILE_URL;
        }
        if (StringUtils.isEmpty(selectedBundle) || StringUtils.isEmpty(selectedMobilePlan)) {
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.error.mandetory.doitbundle");
            return REDIRECT_MOBILE_URL;
        }
        llaCartFacade.addProductOfferingToCart(selectedMobilePlan, quantity, processType, rootBpoCode, cartGroupNo,
              subscriptionTermId, subscriberId, subscriberBillingId);
        llaCartFacade.addProductOfferingToCart(selectedBundle, quantity, processType, rootBpoCode, cartGroupNo,
                subscriptionTermId, subscriberId, subscriberBillingId);
        sessionService.removeAttribute(SELECTED_MOBILE);
        sessionService.removeAttribute(SELECTED_BUNDLE);
        return REDIRECT_PREFIX + "/cart";
    }
    /**
     * @param model
     * @param contentPage
     */
    private void setCMSPageData(final Model model, final ContentPageModel contentPage)
    {
        storeCmsPageInModel(model, contentPage);
        setUpMetaDataForContentPage(model, contentPage);
        updatePageTitle(model, contentPage);
//        model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("product.configurator.create.breadcrumb"));
    }
    protected void updatePageTitle(final Model model, final AbstractPageModel cmsPage)
    {
        storeContentPageTitleInModel(model, getPageTitleResolver().resolveContentPageTitle(cmsPage.getTitle()));
    }
}
