package com.lla.jamaicastore.interceptors.beforeview;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AdobeAnalyticsBeforeViewHandler implements BeforeViewHandler {
    private static final String CURRENT_SITE = "jamaicastorefront.productionsite";
    @Resource(name = "configurationService")
    private ConfigurationService configurationService;
    @Override
    public void beforeView(HttpServletRequest request, HttpServletResponse response, ModelAndView modelAndView) throws Exception {
        modelAndView.addObject("isProductionSite", configurationService.getConfiguration().getString(CURRENT_SITE));
    }
}
