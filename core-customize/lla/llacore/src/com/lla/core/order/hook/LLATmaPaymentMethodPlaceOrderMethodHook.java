/**
 *
 */
package com.lla.core.order.hook;

import de.hybris.platform.b2ctelcoservices.order.hook.TmaPaymentMethodPlaceOrderMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.subscriptionservices.subscription.BillingTimeService;

import javax.annotation.Resource;

import com.lla.core.util.LLACommonUtil;


/**
 * @author GF986ZE
 *
 */
public class LLATmaPaymentMethodPlaceOrderMethodHook extends TmaPaymentMethodPlaceOrderMethodHook
{

	/**
	 * @param billingTimeService
	 * @param configurationService
	 */
	public LLATmaPaymentMethodPlaceOrderMethodHook(final BillingTimeService billingTimeService,
			final ConfigurationService configurationService)
	{
		super(billingTimeService, configurationService);

	}

	@Resource(name = "llaCommonUtil")
	private LLACommonUtil llaCommonUtil;

	@Override
	public void beforePlaceOrder(final CommerceCheckoutParameter parameter) throws InvalidCartException
	{
		if (!llaCommonUtil.isSiteJamaica() && !llaCommonUtil.isSitePanama() && !llaCommonUtil.isSiteVTR() && parameter.getCart() != null
				&& isPaymentMethodRequired(parameter.getCart())
				&& parameter.getCart().getPaymentInfo() == null)
		{
			throw new InvalidCartException(
					"Cannot place order because cart '" + parameter.getCart().getCode() + "' does not have payment method.");
		}
	}
}
