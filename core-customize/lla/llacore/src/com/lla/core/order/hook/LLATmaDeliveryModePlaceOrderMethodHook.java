package com.lla.core.order.hook;

import de.hybris.platform.b2ctelcoservices.order.hook.TmaDeliveryModePlaceOrderMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.order.InvalidCartException;

import javax.annotation.Resource;

import com.lla.core.util.LLACommonUtil;

public class LLATmaDeliveryModePlaceOrderMethodHook extends TmaDeliveryModePlaceOrderMethodHook {

	@Resource(name = "llaCommonUtil")
	private LLACommonUtil llaCommonUtil;

    @Override
    public void beforePlaceOrder(final CommerceCheckoutParameter parameter) throws InvalidCartException
    {
		if (!llaCommonUtil.isSiteJamaica() && !llaCommonUtil.isSitePuertorico() && !llaCommonUtil.isSitePanama() && !llaCommonUtil.isSiteVTR() && !llaCommonUtil.isSiteCabletica() && parameter.getCart().getDeliveryAddress() != null
				&& parameter.getCart().getDeliveryMode() == null)
        {
            throw new InvalidCartException(
                    "Cannot place order because cart '" + parameter.getCart().getCode() + "' does not have delivery mode.");
        }
    }
}
