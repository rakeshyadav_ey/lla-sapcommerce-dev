package com.lla.core.order.interceptors;

import com.lla.core.enums.InstallationType;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.interceptors.DefaultOrderPrepareInterceptor;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.internal.model.impl.ModelValueHistory;
import de.hybris.platform.servicelayer.model.ItemModelContextImpl;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class LLAOrderPrepareInterceptor extends DefaultOrderPrepareInterceptor {
    private static final Logger LOG = Logger.getLogger(LLAOrderPrepareInterceptor.class);

    @Autowired
    private ConfigurationService configurationService;
    @Override
    public void onPrepare(final Object model, final InterceptorContext ctx) throws InterceptorException {
        super.onPrepare(model, ctx);

         if (model instanceof OrderModel && ((OrderModel)model).getSite().getUid().equalsIgnoreCase("puertorico")) {
            final OrderModel order = (OrderModel) model;
            final ItemModelContextImpl context = (ItemModelContextImpl) order.getItemModelContext();
            final ModelValueHistory history = context.getValueHistory();
            InstallationType installationType = order.getInstallationType();
            if(installationType==null){
                LOG.error(String.format("Please select valid Installation Type for Order ",order.getCode()));
                throw new InterceptorException("Please select valid Installation Type for Order");
            }
            if(ctx.isNew(order)){
                populateInstallationChargeAndCode(installationType,order);
            }else if (ctx.isModified(order, OrderModel.INSTALLATIONTYPE)) {
                populateInstallationChargeAndCode(installationType,order);
            }

        }
    }

    /**
     * Populate Installation Charge and Code for Order
     * @param installationType
     * @param order
     */
    private void populateInstallationChargeAndCode(InstallationType installationType,OrderModel order) {
         StringBuffer installationServiceCode=new StringBuffer("installation.servicecode.");
         StringBuffer installationCost=new StringBuffer("installation.cost.");
        if (InstallationType.TIER1.equals(installationType)) {
            updateCostAndServiceCode(installationType, order, installationServiceCode, installationCost);

        } else if (InstallationType.TIER2.equals(installationType)) {
            updateCostAndServiceCode(installationType, order, installationServiceCode, installationCost);


        } else if (InstallationType.TIER3.equals(installationType)) {
            updateCostAndServiceCode(installationType, order, installationServiceCode, installationCost);

        } else if (InstallationType.TIER4.equals(installationType)) {
            updateCostAndServiceCode(installationType, order, installationServiceCode, installationCost);

        } else if (InstallationType.TIER5.equals(installationType)) {
            updateCostAndServiceCode(installationType, order, installationServiceCode, installationCost);
        }
    }

    /**
     * Update Installation Cost and ServiceCode
     * @param installationType
     * @param order
     * @param installationServiceCode
     * @param installationCost
     */
    private void updateCostAndServiceCode(InstallationType installationType, OrderModel order, StringBuffer installationServiceCode, StringBuffer installationCost) {
        order.setInstallationCharge(Double.valueOf(configurationService.getConfiguration().getString(installationCost.append(installationType.getCode().toLowerCase()).toString())));
        order.setInstallationServiceCode(configurationService.getConfiguration().getString(installationServiceCode.append(installationType.getCode().toLowerCase()).toString()));
    }
}

