/**
 *
 */
package com.lla.core.services.strategy.impl;

import de.hybris.platform.commerceservices.delivery.dao.CountryZoneDeliveryModeDao;
import de.hybris.platform.commerceservices.delivery.dao.PickupDeliveryModeDao;
import de.hybris.platform.commerceservices.strategies.impl.DefaultDeliveryModeLookupStrategy;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.lla.core.services.strategy.LLADeliveryModeLookupStrategy;


/**
 * @author GF986ZE
 *
 */
public class LLADeliveryModeLookupStrategyImpl extends DefaultDeliveryModeLookupStrategy implements LLADeliveryModeLookupStrategy
{

	private CountryZoneDeliveryModeDao countryZoneDeliveryModeDao;
	private PickupDeliveryModeDao pickupDeliveryModeDao;



	@Override
	public List<DeliveryModeModel> getSelectableDeliveryModesForOrder(final AbstractOrderModel abstractOrderModel)
	{
		final List<DeliveryModeModel> deliveryModes = new ArrayList<DeliveryModeModel>();
		if (null != abstractOrderModel)
		{
			final AddressModel deliveryAddress = abstractOrderModel.getDeliveryAddress();
			final CurrencyModel currency = abstractOrderModel.getCurrency();
			if (currency != null && deliveryAddress != null && deliveryAddress.getCountry() != null)
			{
				final List<DeliveryModeModel> pickModes = getPickupDeliveryModeDao()
						.findPickupDeliveryModesForAbstractOrder(abstractOrderModel);
				if (CollectionUtils.isNotEmpty(pickModes))
				{
					deliveryModes.add(pickModes.get(0));
				}
				deliveryModes.addAll(getCountryZoneDeliveryModeDao().findDeliveryModes(abstractOrderModel));
			}

		}
		return deliveryModes;

	}


	protected PickupDeliveryModeDao getPickupDeliveryModeDao()
	{
		return pickupDeliveryModeDao;
	}

	public void setPickupDeliveryModeDao(final PickupDeliveryModeDao pickupDeliveryModeDao)
	{
		this.pickupDeliveryModeDao = pickupDeliveryModeDao;
	}

	protected CountryZoneDeliveryModeDao getCountryZoneDeliveryModeDao()
	{
		return countryZoneDeliveryModeDao;
	}

	public void setCountryZoneDeliveryModeDao(final CountryZoneDeliveryModeDao countryZoneDeliveryModeDao)
	{
		this.countryZoneDeliveryModeDao = countryZoneDeliveryModeDao;
	}

}
