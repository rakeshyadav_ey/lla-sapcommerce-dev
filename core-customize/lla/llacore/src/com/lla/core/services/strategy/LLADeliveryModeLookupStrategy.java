package com.lla.core.services.strategy;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;

import java.util.List;


/**
 * @author GF986ZE
 *
 */
public interface LLADeliveryModeLookupStrategy
{

	List<DeliveryModeModel> getSelectableDeliveryModesForOrder(AbstractOrderModel abstractOrderModel);
}
