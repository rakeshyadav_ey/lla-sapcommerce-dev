/**
 *
 */
package com.lla.core.price.strategy;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.delivery.DeliveryMode;
import de.hybris.platform.order.strategies.calculation.impl.DefaultFindDeliveryCostStrategy;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.PriceValue;

import org.apache.log4j.Logger;


/**
 * @author GZ132VA
 *
 */
public class LLAFindDeliveryCostStrategy extends DefaultFindDeliveryCostStrategy
{
	private static final Logger LOG = Logger.getLogger(LLAFindDeliveryCostStrategy.class);

	//step 1 : delegate to jalo
	@Override
	public PriceValue getDeliveryCost(final AbstractOrderModel order)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("order", order);
		try
		{
			final DeliveryModeModel deliveryMode = order.getDeliveryMode();
			if (order.getSite().getUid().equals("jamaica"))
			{
				return new PriceValue(order.getCurrency().getIsocode(), 0.0, order.getNet().booleanValue());
			}
			if (deliveryMode != null)
			{
				getModelService().save(order);
				final AbstractOrder orderItem = getModelService().getSource(order);
				final DeliveryMode dModeJalo = getModelService().getSource(deliveryMode);
				return dModeJalo.getCost(orderItem);
			}
			else
			{
				return new PriceValue(order.getCurrency().getIsocode(), 0.0, order.getNet().booleanValue());
			}
		}
		catch (final Exception e)
		{
			LOG.warn("Could not find deliveryCost for order [" + order.getCode() + "] due to : " + e + "... skipping!");
			return new PriceValue(order.getCurrency().getIsocode(), 0.0, order.getNet().booleanValue());
		}
	}
}
