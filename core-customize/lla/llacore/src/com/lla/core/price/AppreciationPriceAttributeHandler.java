package com.lla.core.price;
import com.lla.core.util.LLACommonUtil;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.subscriptionservices.model.RecurringChargeEntryModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;


public class AppreciationPriceAttributeHandler implements DynamicAttributeHandler<Double, RecurringChargeEntryModel>, Serializable
{
    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private LLACommonUtil llaCommonUtil;

    @Override
    public Double get(final RecurringChargeEntryModel model)
    {
        if(llaCommonUtil.isSitePuertorico() && llaCommonUtil.containsSecondLevelCategoryWithCode(model.getSubscriptionPricePlanRecurring().getProduct(), "postpaidPlans")) {
            Double configPercentage = Double.valueOf(configurationService.getConfiguration().getDouble("appreciation.discount.value"));
            return model.getPrice() * configPercentage;
        }
        return 0.0D;
    }

    @Override
    public void set(final RecurringChargeEntryModel model, final Double value)
    {
        throw new UnsupportedOperationException();
    }
}
