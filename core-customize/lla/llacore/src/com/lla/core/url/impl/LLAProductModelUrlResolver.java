package com.lla.core.url.impl;

import com.lla.core.util.LLACommonUtil;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.DefaultProductModelUrlResolver;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class LLAProductModelUrlResolver  extends DefaultProductModelUrlResolver {
    @Autowired
    private LLACommonUtil llaCommonUtil;
    @Override
    protected String buildPathString(final List<CategoryModel> path)
    {
        if (path == null || path.isEmpty())
        {
            return "c"; // Default category part of path when missing category
        }

        final StringBuilder result = new StringBuilder();
        int categoryPathSize=path.size();
        if(llaCommonUtil.isSiteJamaica()){
            categoryPathSize=categoryPathSize-1;
        }
        for (int i = 0; i < categoryPathSize; i++)
        {
            if (i != 0)
            {
                result.append('/');
            }
            result.append(urlSafe(path.get(i).getName()));
        }

        return result.toString();
    }
}
