package com.lla.core.cutstomer.interceptor;

import com.lla.core.util.LLAEncryptionUtil;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.LoadInterceptor;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;

public class LLACustomerLoadInterceptor implements LoadInterceptor {
    private static final Logger LOG = Logger.getLogger(LLACustomerLoadInterceptor.class);
    @Autowired
    private LLAEncryptionUtil llaEncryptionUtil;
    @Autowired
    private ModelService modelService;
    @Override
    public void onLoad(Object object, InterceptorContext interceptorContext) throws InterceptorException {
        final CustomerModel customer=(CustomerModel)object;
        if (object instanceof CustomerModel &&( interceptorContext.isModified(object) || interceptorContext.isNew(object)))
            if(StringUtils.isNotEmpty(customer.getEncryptedSSN())){
                customer.setCustomerSSN(llaEncryptionUtil.doMasking(llaEncryptionUtil.doDecryption(customer.getEncryptedSSN())));

            }
        }
    }

