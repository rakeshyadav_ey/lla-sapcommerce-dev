package com.lla.core.cutstomer.interceptor;

import com.lla.core.util.LLAEncryptionUtil;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


public class LLACustomerPrepareInterceptor implements PrepareInterceptor {
    private static final Logger LOG = Logger.getLogger(LLACustomerPrepareInterceptor.class);
    @Autowired
    private LLAEncryptionUtil llaEncryptionUtil;
    @Autowired
    private ModelService modelService;
    @Override
    public void onPrepare(final Object model, final InterceptorContext ctx) throws InterceptorException
    {
        if (model instanceof CustomerModel &&( ctx.isModified(model) || ctx.isNew(model)))
        {
            CustomerModel customer=(CustomerModel)model;
            final String customerSSN=customer.getCustomerSSN();
            if(!StringUtils.contains(customerSSN,'*')){
                if(customerSSN.matches("[0-9]+") && customerSSN.length()==9)
                {
                    String encryptedText=llaEncryptionUtil.doEncryption(customerSSN);
                    customer.setCustomerSSN(llaEncryptionUtil.doMasking(customerSSN));
                    customer.setEncryptedSSN(encryptedText);
                }else{
                    LOG.error("SSN must be numeric only and must have length of 9");
                    throw new InterceptorException("SSN must be numeric only and must have length of 9", this);
                }
            }else{
                LOG.info("No action required !!! ");
            }

        }

    }
}
