/**
 *
 */
package com.lla.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import com.lla.core.util.LLACommonUtil;
import de.hybris.platform.commerceservices.consent.dao.impl.DefaultConsentDao;
import de.hybris.platform.commerceservices.model.consent.ConsentModel;
import de.hybris.platform.commerceservices.model.consent.ConsentTemplateModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.lla.core.dao.LLAConsentDao;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author Rohit25.Gupta
 *
 */
public class DefaultLLAConsentDao extends DefaultConsentDao implements LLAConsentDao
{
	@Autowired
	private LLACommonUtil llaCommonUtil;

	private static final String FIND_CONSENT_BY_CUSTOMER_AND_TEMPLATE = "SELECT {uc:" + ConsentModel.PK + "} FROM {"
			+ ConsentModel._TYPECODE + " as uc} WHERE {uc:" + ConsentModel.CONSENTTEMPLATE + "} = ?" + ConsentModel.CONSENTTEMPLATE
			+ " AND {" + ConsentModel.CUSTOMER + "} = ?" + ConsentModel.CUSTOMER;

	private static final String ORDER_BY_CONSENT_GIVEN_DATE_ASC = " ORDER BY {uc:" + ConsentModel.CONSENTGIVENDATE + "} ASC";

	@Override
	public ConsentModel findConsentByCustomerAndConsentTemplate(final CustomerModel customer,
			final ConsentTemplateModel consentTemplate)
	{
		if(llaCommonUtil.isSiteJamaica()) {
			validateParameterNotNullStandardMessage("customer", customer);
			validateParameterNotNullStandardMessage("consentTemplate", consentTemplate);

			final Map<String, Object> queryParams = populateBasicQueryParams(customer, consentTemplate);

			final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(
					FIND_CONSENT_BY_CUSTOMER_AND_TEMPLATE + ORDER_BY_CONSENT_GIVEN_DATE_ASC);
			flexibleSearchQuery.getQueryParameters().putAll(queryParams);
			flexibleSearchQuery.setCount(1);

			final List<ConsentModel> consents = getFlexibleSearchService().<ConsentModel>search(flexibleSearchQuery).getResult();
			return CollectionUtils.isNotEmpty(consents) ? consents.get(0) : null;
		}else
			return super.findConsentByCustomerAndConsentTemplate(customer, consentTemplate);
	}

	@Override
	protected Map<String, Object> populateBasicQueryParams(final CustomerModel customer,
			final ConsentTemplateModel consentTemplate)
	{
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(ConsentModel.CUSTOMER, customer);
		queryParams.put(ConsentModel.CONSENTTEMPLATE, consentTemplate);
		return queryParams;
	}
}
