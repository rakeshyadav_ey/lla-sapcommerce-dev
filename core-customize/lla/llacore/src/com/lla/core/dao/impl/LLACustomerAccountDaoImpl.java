/**
 *
 */
package com.lla.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.customer.dao.impl.DefaultCustomerAccountDao;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.lla.core.dao.LLACustomerAccountDao;


/**
 * @author GZ132VA
 *
 */
public class LLACustomerAccountDaoImpl extends DefaultCustomerAccountDao implements LLACustomerAccountDao
{

	private static final String FIND_ADDRESS_BOOK_INSTALLATION_ENTRIES = "SELECT {address:" + AddressModel.PK + "} FROM {"
			+ AddressModel._TYPECODE + " AS address LEFT JOIN " + CustomerModel._TYPECODE + " AS customer ON {address:"
			+ AddressModel.OWNER + "}={customer:" + CustomerModel.PK + "}} WHERE {customer:" + CustomerModel.PK
			+ "} = ?customer AND {address:" + AddressModel.INSTALLATIONADDRESS + "} = ?installationAddress AND {address:"
			+ AddressModel.VISIBLEINADDRESSBOOK + "} = ?visibleInAddressBook AND {address:" + AddressModel.COUNTRY
			+ "} IN (?installationCountries)";

	private static final String FIND_CUSTOMER_FOR_CORRELATION_ID = "SELECT {" + CustomerModel.PK + "} FROM {"
			+ CustomerModel._TYPECODE + "} WHERE {" + CustomerModel.CORRELATIONID + "} = ?correlationId ";

	@Override
	public Collection<? extends AddressModel> findAddressBookInstallationEntriesForCustomer(final CustomerModel user,
			final Collection<CountryModel> installationCountries)
	{
		validateParameterNotNull(user, "Customer must not be null");
		final Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("customer", user);
		queryParams.put("installationAddress", Boolean.TRUE);
		queryParams.put("visibleInAddressBook", Boolean.TRUE);
		queryParams.put("installationCountries", installationCountries);
		final SearchResult<AddressModel> result = getFlexibleSearchService().search(FIND_ADDRESS_BOOK_INSTALLATION_ENTRIES,
				queryParams);
		return result.getResult();

	}

	@Override
	public SearchPageData<OrderModel> findOrdersByCustomerAndStore(final CustomerModel customerModel, final BaseStoreModel store,
			final OrderStatus[] status, final PageableData pageableData)
	{
		final SearchPageData<OrderModel> searchResult = super.findOrdersByCustomerAndStore(customerModel, store, status,
				pageableData);
		final SearchPageData<OrderModel> refinedSearchResult = new SearchPageData<>();
		final List<OrderModel> filterOrder = new ArrayList();
		if (null != searchResult && CollectionUtils.isNotEmpty(searchResult.getResults()))
		{
			for (final OrderModel order : searchResult.getResults())
			{
				if (null == order.getParentOrder())
				{
					filterOrder.add(order);
				}
			}
		}
		refinedSearchResult.setResults(filterOrder);
		final PaginationData paginationData = searchResult.getPagination();
		paginationData.setTotalNumberOfResults(filterOrder.size());
		paginationData.setPageSize(searchResult.getPagination().getPageSize());
		paginationData.setSort(searchResult.getPagination().getSort());
		paginationData.setCurrentPage(searchResult.getPagination().getCurrentPage());
		refinedSearchResult.setPagination(paginationData);
		refinedSearchResult.setSorts(searchResult.getSorts());

		return refinedSearchResult;
	}

	@Override
	public CustomerModel findCustomerForCorrelationID(final String correlationId)
	{
		validateParameterNotNull(correlationId, "Customer correlationId must not be null");
		final Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("correlationId", correlationId);
		final SearchResult<CustomerModel> result = getFlexibleSearchService().search(FIND_CUSTOMER_FOR_CORRELATION_ID, queryParams);
		return result.getCount() > 0 ? result.getResult().get(0) : null;
	}
}
