package com.lla.core.dao;

import com.lla.core.enums.ContractType;
import com.lla.core.model.CreditScoreModel;

public interface LLACreditScoreDao {
	
	/**
	 * Gets the creditScore by creditScoreRange value, contractStatus and pack
	 *
	 * @param creditScore
	 * @param contractStatus
	 * @param pack
	 * @return the found creditScore
	 */
    CreditScoreModel fetchCreditScore(int creditScore, ContractType contractStatus,String pack);
}
