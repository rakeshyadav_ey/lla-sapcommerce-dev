/**
 *
 */
package com.lla.core.dao;

import de.hybris.platform.commerceservices.consent.dao.ConsentDao;
import de.hybris.platform.commerceservices.model.consent.ConsentModel;
import de.hybris.platform.commerceservices.model.consent.ConsentTemplateModel;
import de.hybris.platform.core.model.user.CustomerModel;


/**
 * @author Rohit25.Gupta
 *
 */
public interface LLAConsentDao extends ConsentDao
{
	ConsentModel findConsentByCustomerAndConsentTemplate(final CustomerModel customer, final ConsentTemplateModel consentTemplate);
}
