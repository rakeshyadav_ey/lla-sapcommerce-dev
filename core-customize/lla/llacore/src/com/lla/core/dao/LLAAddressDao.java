/**
 *
 */
package com.lla.core.dao;

import java.util.List;

import com.lla.core.model.LLAPanamaAddressMappingModel;
import com.lla.core.model.ProvinceModel;


/**
 * @author GF986ZE
 *
 */
public interface LLAAddressDao
{

	List<LLAPanamaAddressMappingModel> getAddressMappingForGivenText(String text);

	List<ProvinceModel> getProvinces();

	<T> List<T> getAddressValues(final String prevNode, final String nextNode);

}
