package com.lla.core.dao;

import de.hybris.platform.processengine.model.BusinessProcessModel;

import java.util.Date;
import java.util.List;

public interface NotificationEmailProcessDao {
    /**
     * Get Failed Notification email Process
     * @param siteId
     * @param filesDaysOld
     * @return List<BusinessProcessModel>
     */
    List<BusinessProcessModel> getFailedNotificationProcess(final String siteId, Date filesDaysOld);

    /**
     * Get Failed Fallout Process
     * @param siteId
     * @param numHours
     * @return
     */
    List<BusinessProcessModel> getFailedFalloutProcess(final String siteId, int numHours);
}

