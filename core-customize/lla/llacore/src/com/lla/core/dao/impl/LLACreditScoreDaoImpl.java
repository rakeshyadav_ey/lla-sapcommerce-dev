package com.lla.core.dao.impl;

import com.lla.core.dao.LLACreditScoreDao;
import com.lla.core.enums.ContractType;
import com.lla.core.model.CreditScoreModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import java.util.List;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

public class LLACreditScoreDaoImpl implements LLACreditScoreDao {

    private static final Logger LOG = Logger.getLogger(LLACreditScoreDaoImpl.class);

    private FlexibleSearchService flexibleSearchService;

    private final static String CREDIT_SCORE_QUERY = "SELECT {score:" + CreditScoreModel.PK + "} FROM {"
 			+ CreditScoreModel._TYPECODE + " AS score JOIN " + ContractType._TYPECODE + " AS contract ON {score:"
 			+ CreditScoreModel.CONTRACTSTATUS + "} = {contract:pk}} WHERE {contract:code}=?contractStatus AND {score:" + CreditScoreModel.PACK + "}=?pack";

    @Override
 	public CreditScoreModel fetchCreditScore(final int creditScore, final ContractType contractStatus, final String pack)
 	{
 		validateParameterNotNullStandardMessage("creditScore", creditScore);
 		validateParameterNotNullStandardMessage("contractStatus", contractStatus.getCode());
 		validateParameterNotNullStandardMessage("pack", pack);

 		final FlexibleSearchQuery query = new FlexibleSearchQuery(CREDIT_SCORE_QUERY);
 		query.addQueryParameter("contractStatus", contractStatus.getCode());
 		query.addQueryParameter("pack", pack);
 		final SearchResult<CreditScoreModel> creditScoreResult = getFlexibleSearchService().search(query);
 		LOG.info("Credit Score Result" + creditScoreResult.getResult().size());
 		if (null != creditScoreResult && null != creditScoreResult.getResult() && CollectionUtils.isNotEmpty(creditScoreResult.getResult()))
 		{
 			for(CreditScoreModel creditScoreModel : creditScoreResult.getResult()) {
 				if(creditScore >= (creditScoreModel.getMinScoreRange().intValue()) 
 						&& creditScore <= (creditScoreModel.getMaxScoreRange().intValue())) {
 					return creditScoreModel;
 				}
 				
 			}
 		}
 		return null;
 	}

    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}
