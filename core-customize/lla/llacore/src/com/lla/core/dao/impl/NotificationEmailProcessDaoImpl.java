package com.lla.core.dao.impl;

import com.lla.core.dao.NotificationEmailProcessDao;
import com.lla.mulesoft.integration.service.impl.LLAMulesoftCallBackRequestServiceImpl;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class NotificationEmailProcessDaoImpl  implements NotificationEmailProcessDao {

    private static final Logger LOG = Logger.getLogger(NotificationEmailProcessDaoImpl.class);
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-mm-dd HH:mm:ss";
    private static final String GET_NOTIFICATION_PROCESS="SELECT {np:pk} FROM {NotificationEmailProcess AS np join Order as o  " +
            "on {np.order}={o.pk} join BaseSite as bs on {o.site}={bs.pk}} where {bs.uid}=?site and {np.creationtime}>= ?validDate";

    private static final String GET_INCOMPLETED_FALLOUT_PROCESS="SELECT {fp:pk} FROM {FalloutProcess AS fp join Cart as o  " +
            "on {fp.order}={o.pk} join BaseSite as bs on {o.site}={bs.pk}} where {bs.uid}=?site and {fp.agentNotificationSent}=0 or {fp.genesysNotificationSent} = 0";

    @Autowired
    private FlexibleSearchService flexibleSearchService;
    /**
     * Get Failed Notification email Process
     * @param siteId
     * @param validDate
     * @return List<BusinessProcessModel>
     */
    @Override
    public List<BusinessProcessModel> getFailedNotificationProcess(final String siteId, Date validDate) {
         final Map<String, Object> params = new HashMap<>();
         params.put("site", siteId);
         params.put("validDate", validDate);
         FlexibleSearchQuery query=new FlexibleSearchQuery(GET_NOTIFICATION_PROCESS,params);
         SearchResult<BusinessProcessModel> failedProcess= flexibleSearchService.search(query);
         List<BusinessProcessModel> failedProcessResult=failedProcess.getResult();
         if(CollectionUtils.isNotEmpty(failedProcessResult)){
             return failedProcessResult;
         }
         return null;
    }

    @Override
    public List<BusinessProcessModel> getFailedFalloutProcess(final String siteId, int numHours) {
        final Map<String, Object> params = new HashMap<>();
        params.put("site", siteId);
        Date endTime=new Date();
        Date startTime=new Date(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(numHours));
        FlexibleSearchQuery query=new FlexibleSearchQuery(GET_INCOMPLETED_FALLOUT_PROCESS,params);
        SearchResult<BusinessProcessModel> failedProcess= flexibleSearchService.search(query);
        List<BusinessProcessModel> failedProcessResult=failedProcess.getResult();
        if(CollectionUtils.isNotEmpty(failedProcessResult)) {
            List<BusinessProcessModel> filteredProcessResult=failedProcessResult.stream().filter(result -> result.getCreationtime().after(startTime) && result.getCreationtime().before(endTime)).collect(Collectors.toList());
            LOG.info(String.format("Retrieved ::: %s FalloutProcess for Repair in last ::%s hours for Market ::%s", filteredProcessResult.size(), numHours, siteId));
            return filteredProcessResult;
        }
        LOG.info(String.format("Retrieved ::: %s FalloutProcess for Repair in last ::%s hours for Market ::%s",failedProcessResult.size(),numHours,siteId));
        return ListUtils.EMPTY_LIST;
    }
}
