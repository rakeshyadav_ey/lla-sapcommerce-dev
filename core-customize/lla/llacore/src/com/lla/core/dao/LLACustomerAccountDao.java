/**
 *
 */
package com.lla.core.dao;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Collection;


/**
 * @author GZ132VA
 *
 */
public interface LLACustomerAccountDao
{

	/**
	 * @param user
	 * @param allCountries
	 * @return
	 */
	Collection<? extends AddressModel> findAddressBookInstallationEntriesForCustomer(CustomerModel user,
			Collection<CountryModel> allCountries);

	/**
	 * @param correlationId
	 * @return
	 */
	CustomerModel findCustomerForCorrelationID(String correlationId);

}
