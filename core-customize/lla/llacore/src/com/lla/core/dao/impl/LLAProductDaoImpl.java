package com.lla.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.b2ctelcoservices.daos.impl.DefaultTmaProductDao;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.lla.core.dao.LLAProductDao;
import com.lla.core.model.LLABundleProductMappingModel;
import com.lla.core.model.LLAConfiguratorMessageModel;

public class LLAProductDaoImpl extends DefaultTmaProductDao implements LLAProductDao {

    private static final Logger LOG = Logger.getLogger(LLAProductDaoImpl.class);
    private static final String GET_COMMERCE_PRODUCT_USING_BSSCODE="SELECT {p:product} FROM {LLAProductMapper AS p JOIN SourceEPC AS s ON {p.sourceEPC}={s.pk}} WHERE {s.code} = ?sourceEPC AND  {p.sourceSystemCode}=?sourceSystemCode";
    private static final String GET_BUNDLE_PRODUCT_MAPPING="SELECT {p:pk} FROM {LLABundleProductMapping AS p}";
    private static final String GET_BUNDLE_PRODUCT_CONFIGURATION="SELECT {p:pk} FROM {LLAConfiguratorMessage AS p} where {p.packageId} LIKE ?packageId";
    public LLAProductDaoImpl(final String typecode)
    {
        super(typecode);
    }
    /**
     * Get Commerce Product Model  Using BSS Code
     *
     * @param bssCode
     * @return
     * @throws ModelNotFoundException
     */
    @Override
    public TmaProductOfferingModel findCommerceProductCodeUsingBSSCode(final String bssCode, final String sourceEPC) throws ModelNotFoundException {
        validateParameterNotNull(bssCode, "No Source System Code specified");
        final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_COMMERCE_PRODUCT_USING_BSSCODE.toString());
        query.addQueryParameter("sourceSystemCode",bssCode);
        query.addQueryParameter("sourceEPC", sourceEPC);
        final SearchResult<TmaProductOfferingModel> productMapperResult=getFlexibleSearchService().search(query);
        LOG.info("Product Mapper Result" +productMapperResult.getResult().size());
        if(null!=productMapperResult && null!=productMapperResult.getResult()){
         final List<TmaProductOfferingModel> itemList= productMapperResult.getResult();
          return CollectionUtils.isNotEmpty(itemList)?itemList.get(0):null;
        }
        return null;
    }

    @Override
    public List<LLABundleProductMappingModel> getAllBundleProdutMapping() {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_BUNDLE_PRODUCT_MAPPING);
        final SearchResult<LLABundleProductMappingModel> productMapperResult=getFlexibleSearchService().search(query);
        return productMapperResult.getResult();
    }

    @Override
    public LLAConfiguratorMessageModel getConfiguratorMessageForPackage(final String packageId) {
        validateParameterNotNull(packageId, "No packageId provided");
        final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_BUNDLE_PRODUCT_CONFIGURATION.toString());
        query.addQueryParameter("packageId","%" + packageId + "%");
        final SearchResult<LLAConfiguratorMessageModel> configurationMessageResult=getFlexibleSearchService().search(query);
        if(null!=configurationMessageResult && null!=configurationMessageResult.getResult()){
            final List<LLAConfiguratorMessageModel> itemList= configurationMessageResult.getResult();
            return CollectionUtils.isNotEmpty(itemList)?itemList.get(0):null;
        }
        return null;
    }
}
