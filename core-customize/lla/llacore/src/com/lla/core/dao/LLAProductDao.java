package com.lla.core.dao;

import com.lla.core.model.LLABundleProductMappingModel;
import com.lla.core.model.LLAConfiguratorMessageModel;
import de.hybris.platform.b2ctelcoservices.daos.TmaProductDao;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;

import java.util.List;

public interface LLAProductDao extends TmaProductDao {
  /**
   * Get Commerce Product Model  Using BSS Code
   * @param bssCode
   * @param  sourceEPC
   * @return
   * @throws ModelNotFoundException
   */
  TmaProductOfferingModel findCommerceProductCodeUsingBSSCode(final String bssCode, final String sourceEPC) throws ModelNotFoundException;

  List<LLABundleProductMappingModel> getAllBundleProdutMapping();

  LLAConfiguratorMessageModel getConfiguratorMessageForPackage(String packageId);
}
