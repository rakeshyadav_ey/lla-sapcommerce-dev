/**
 *
 */
package com.lla.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import com.lla.core.jalo.District;
import com.lla.core.model.*;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.lla.core.dao.LLAAddressDao;


/**
 * @author GF986ZE
 *
 */
public class LLAAddressDaoImpl implements LLAAddressDao
{
	private static final Logger LOG = Logger.getLogger(LLAAddressDaoImpl.class);

	private static final int SEARCH_RESULTS_COUNT = 10;
	private static final String PERCENTAGE_SYMBOL = "%";
	private static final String GET_ADDRESS_MAPPINGS_FOR_GIVEN_TEXT = "select {l.pk} from {LLAPanamaAddressMapping as l} WHERE LOWER({l.identifier}) LIKE ?searchTerm";
	private static final String GET_PROVINCE = "select {p.pk} from {Province as p}";

	@Resource
	private FlexibleSearchService flexibleSearchService;


	@Override
	public List<LLAPanamaAddressMappingModel> getAddressMappingForGivenText(final String text)
	{
		validateParameterNotNullStandardMessage("text", text);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ADDRESS_MAPPINGS_FOR_GIVEN_TEXT);
		final StringBuilder searchTerm = new StringBuilder();
		searchTerm.append(PERCENTAGE_SYMBOL);
		searchTerm.append(text.toLowerCase());
		searchTerm.append(PERCENTAGE_SYMBOL);
		query.addQueryParameter("searchTerm", searchTerm.toString());
		query.setCount(SEARCH_RESULTS_COUNT);
		final SearchResult<LLAPanamaAddressMappingModel> results = getFlexibleSearchService().search(query);
		if (null != results)
		{
			final List<LLAPanamaAddressMappingModel> addressMappingResults = results.getResult();
			return CollectionUtils.isNotEmpty(addressMappingResults) ? addressMappingResults : null;
		}
		return null;
	}

	@Override
	public List<ProvinceModel> getProvinces() {
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_PROVINCE);
		final SearchResult<ProvinceModel> results = getFlexibleSearchService().search(query);
		if (null != results)
		{
			final List<ProvinceModel> provinceResults = results.getResult();
			return CollectionUtils.isNotEmpty(provinceResults) ? provinceResults : null;
		}
		return null;
	}

	@Override
	public <T> List<T> getAddressValues(String prevNode, String nextNode) {
		StringBuilder queryBuilder=new StringBuilder("SELECT ");
		switch (nextNode){
			case "Canton":
				queryBuilder.append("{type:pk} from {").append(CantonModel._TYPECODE).append(" as type} where {type:postalCode}='").append(prevNode).append("'");
				break;
			case "District":
				queryBuilder.append("{type:pk} from {").append(DistrictModel._TYPECODE).append(" as type} where {type:postalCode}='").append(prevNode).append("'");
				break;
			case "NeighbourHood":
				queryBuilder.append("{type:pk} from {").append(NeighbourhoodModel._TYPECODE).append(" as type} where {type:postalCode}='").append(prevNode).append("'");
				break;
		}
		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryBuilder.toString());
		final SearchResult<T> results = getFlexibleSearchService().search(query);
		if (null != results)
		{
			final List<T> addressResults = results.getResult();
			return CollectionUtils.isNotEmpty(addressResults) ? addressResults : null;
		}
		return null;
	}

	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}


	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

}
