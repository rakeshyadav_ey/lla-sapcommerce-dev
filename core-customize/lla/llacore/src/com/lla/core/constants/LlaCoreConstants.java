/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.core.constants;

/**
 * Global class for all LlaCore constants. You can add global constants for your extension into this class.
 */
public final class LlaCoreConstants extends GeneratedLlaCoreConstants
{
	public static final String EXTENSIONNAME = "llacore";


	private LlaCoreConstants()
	{
		//empty
	}

	// implement here constants used by this extension
	public static final String QUOTE_BUYER_PROCESS = "quote-buyer-process";
	public static final String QUOTE_SALES_REP_PROCESS = "quote-salesrep-process";
	public static final String QUOTE_USER_TYPE = "QUOTE_USER_TYPE";
	public static final String QUOTE_SELLER_APPROVER_PROCESS = "quote-seller-approval-process";
	public static final String QUOTE_TO_EXPIRE_SOON_EMAIL_PROCESS = "quote-to-expire-soon-email-process";
	public static final String QUOTE_EXPIRED_EMAIL_PROCESS = "quote-expired-email-process";
	public static final String QUOTE_POST_CANCELLATION_PROCESS = "quote-post-cancellation-process";
	public static final String SESSION_REGION = "region";
	public static final String FIXED_LINE_CATEGORIES = "fixedLineCategories.config";
	public static final String FMC_PROMOTION_ALLOWED_CATEGORY="fmc.promotion.allowed.category";
	public static final String FMC_PRODUCT_BUNDLE="fmc.product.superCategory.bundle";
	public static final String FMC_PRODUCT_POSTPAID="fmc.product.superCategory.postpaid";
	public static final String FMC_PROMOTION_APPLICABLE_PRODUCTS="fmc.promotion.product";
	public static final String SUCCESS = "success";
	public static final String USER_CALLBACK_EVENT_NAME="CallbackRegistrationCheck";
	public static final String UTM_FIELD="UTMZ";
	public static final String SITE_CABLETICA = "cabletica";
	public static final String CREDIT_SCORE_M = "M";
	public static final String CREDIT_SCORE_L = "L";
	public static final String CREDIT_SCORE_K = "K";
	public static final String CREDIT_SCORE_MONTHLY_PAYMENT = "creditscore.monthly.payment";
	public static final String SITE_PUERTORICO = "puertorico";
	public static final String DEVICE_CATEGORY = "device.category.config";

}
