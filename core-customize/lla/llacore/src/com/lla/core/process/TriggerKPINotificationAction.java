package com.lla.core.process;

import com.lla.core.enums.ClickToCallReasonEnum;
import com.lla.core.enums.OriginSource;
import com.lla.core.service.order.LLACommerceCheckoutService;
import com.lla.core.util.LLACommonUtil;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.falloutprocess.model.FalloutProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.task.RetryLaterException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class TriggerKPINotificationAction extends AbstractSimpleDecisionAction {
    private static final Logger LOG = LoggerFactory.getLogger(TriggerKPINotificationAction.class);

    @Autowired
    private LLACommerceCheckoutService llaCommerceCheckoutService;
    @Autowired
    private LLAMulesoftNotificationService llamulesoftNotificationService;
    @Autowired
    private ConfigurationService configurationService;
    @Autowired
    private CatalogVersionService catalogVersionService;

    @Autowired
    private BaseSiteService baseSiteService;
    @Autowired
    private UserService userService;
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

    @Override
    public Transition executeAction(BusinessProcessModel businessProcessModel) throws RetryLaterException, Exception {
        if (businessProcessModel instanceof FalloutProcessModel) {
            if (llaMulesoftIntegrationUtil.isCableticaOrder(((FalloutProcessModel) businessProcessModel).getSite()))
            {
                FalloutProcessModel processModel = (FalloutProcessModel) businessProcessModel;
                if (OriginSource.PLP.equals(processModel.getOriginSource())) {
                    LOG.info(String.format("Notification to Agent is not required for Servicability Failure on PLP  for cart ::: %s  customer  ::: %s having Identity Number :::: %s", ((FalloutProcessModel) businessProcessModel).getOrder().getCode(), processModel.getPlpC2CForm().getEmail(), processModel.getPlpC2CForm().getIdentityNumber()));
                    processModel.setAgentNotificationSent(Boolean.FALSE);
                    modelService.save(processModel);
                    return Transition.OK;
                } else if (!processModel.isAgentNotificationSent())
                    return triggerEmailAndProceedToNextAction((FalloutProcessModel) businessProcessModel, processModel);
                else {
                    LOG.info(String.format("Notification to Agent is already triggered for Cart ::: %s", ((FalloutProcessModel) businessProcessModel).getOrder().getCode()));
                    return Transition.OK;
                }
            }
            else if(llaMulesoftIntegrationUtil.isPuertoricoOrder(((FalloutProcessModel) businessProcessModel).getSite()))
            {
                FalloutProcessModel processModel = (FalloutProcessModel) businessProcessModel;
                if(!Boolean.valueOf(processModel.isAgentNotificationSent())){
                    catalogVersionService.setSessionCatalogVersion(processModel.getOrder().getSite().getUid()+"ProductCatalog","Online");
                    catalogVersionService.setSessionCatalogVersion(processModel.getOrder().getSite().getUid()+"ProductCatalog","Staged");
                    String response = llamulesoftNotificationService.generateFreshdeskFalloutNotifications((CartModel) processModel.getOrder(),
                            "FRESHDESKFALLOUT", processModel.getNotificationType(), llaMulesoftIntegrationUtil.formatSSNNumber(processModel.getSsnValue()),processModel.getHouseKey());
                    if(StringUtils.isNotEmpty(response))
                    {
                        processModel.setAgentNotificationSent(Boolean.TRUE);
                        modelService.save(processModel);
                        LOG.info(String.format("CS Agent mail triggered for fallout type ::: %s for cart ::: %s for Customer ::: %s",processModel.getNotificationType(),processModel.getOrder().getCode(),((CustomerModel)processModel.getUser()).getUid()));
                        return Transition.OK;
                    }
                    else
                    {
                        LOG.info(String.format("Error sending notification to CS agent for Fallout Type ::: %s for Cart ::: %s  for Customer::: %s",processModel.getNotificationType(), processModel.getOrder().getCode(), ((CustomerModel)processModel.getUser()).getUid()));
                        return Transition.NOK;
                    }
                }else{
                    LOG.info(String.format("Notification to Agent is already triggered for Cart ::: %s Customer Id::: %s ", ((FalloutProcessModel) businessProcessModel).getOrder().getCode(),((CustomerModel)processModel.getUser()).getUid()));
                    return Transition.OK;
                }
            }
        }
        LOG.error("Business Process is not related to Fallout Process");
        return Transition.NOK;
    }

    /***
     * Trigger Email and Proceed to Next Action
     * @param businessProcessModel
     * @param processModel
     * @return
     */
    private Transition triggerEmailAndProceedToNextAction(FalloutProcessModel businessProcessModel, FalloutProcessModel processModel) {
        baseSiteService.setCurrentBaseSite(processModel.getOrder().getSite(), false);
        userService.setCurrentUser(processModel.getOrder().getUser());
        catalogVersionService.setSessionCatalogVersion(processModel.getOrder().getSite().getUid() + "ProductCatalog", "Online");
        catalogVersionService.setSessionCatalogVersion(processModel.getOrder().getSite().getUid() + "ProductCatalog", "Staged");
        try {
            final StringBuffer notificationType = new StringBuffer("llamulesoftintegration.kpi.notification.").append(processModel.getNotificationType().getCode().toLowerCase());
            String emailResponse = llamulesoftNotificationService.generateKPINotifications(processModel.getOrder(), configurationService.getConfiguration().getString(notificationType.toString()), processModel.getNotificationType());
            return getTransitionForKPIEmail(businessProcessModel, processModel, emailResponse);

        } catch (LLAApiException exception) {
            LOG.error(String.format("Exception received : Email not Triggered for  Fallout Process ::"+ processModel.getCode(), exception.getMessage()));
            return Transition.NOK;
        }
    }

    /**
     * Find out next transition for KPI Email
     * @param businessProcessModel
     * @param processModel
     * @param emailResponse
     * @return
     */
    private Transition getTransitionForKPIEmail(FalloutProcessModel businessProcessModel, FalloutProcessModel processModel, String emailResponse) {
        if (StringUtils.isNotEmpty(emailResponse)) {
            CustomerModel customerModel = (CustomerModel) processModel.getOrder().getUser();
            processModel.setAgentNotificationSent(Boolean.TRUE);
            modelService.save(processModel);
            modelService.refresh(processModel);
            LOG.info(String.format("Notification to Agent triggered for Fallout Process :::%s  for Cart  ::: %s", processModel.getCode(), businessProcessModel.getOrder().getCode()));
            return Transition.OK;
        }else{
            LOG.info(String.format("Notification to Agent  not triggered for Fallout Process :::%s  for Cart  ::: %s", processModel.getCode(), businessProcessModel.getOrder().getCode()));
            return Transition.NOK;
        }
    }
}
