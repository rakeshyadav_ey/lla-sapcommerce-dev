package com.lla.core.process;

import com.lla.core.model.PLPC2CFormModel;
import com.lla.core.service.order.LLACommerceCheckoutService;
import com.lla.mulesoft.integration.dto.ClickCallServicablity;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.acceleratorcms.model.components.ClickToCallModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.falloutprocess.model.FalloutProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.task.RetryLaterException;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class TriggerFalloutGenesysNotificationAction extends AbstractSimpleDecisionAction {
    private static final Logger LOG = LoggerFactory.getLogger(TriggerFalloutGenesysNotificationAction.class);

    @Autowired
    private LLACommerceCheckoutService llaCommerceCheckoutService;
    @Autowired
    private CatalogVersionService catalogVersionService;

    @Autowired
    private BaseSiteService baseSiteService;

    @Autowired
    private ConfigurationService configurationService;
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

    @Autowired
    private UserService userService;
    @Override
    public Transition executeAction(BusinessProcessModel businessProcessModel) throws RetryLaterException, Exception {

        if(businessProcessModel instanceof FalloutProcessModel){
            FalloutProcessModel processModel=(FalloutProcessModel)businessProcessModel;
            if(llaMulesoftIntegrationUtil.isPuertoricoOrder(((FalloutProcessModel) businessProcessModel).getSite()))
            {
                processModel.setGenesysNotificationSent(Boolean.TRUE);
                LOG.info("Notification to Genesys is not required for site puertorico.");
                modelService.save(processModel);
                return Transition.OK;
            }
            if(!processModel.isGenesysNotificationSent()){
                baseSiteService.setCurrentBaseSite(processModel.getOrder().getSite(),false);
                userService.setCurrentUser(processModel.getOrder().getUser());
                catalogVersionService.setSessionCatalogVersion(processModel.getOrder().getSite().getUid()+"ProductCatalog","Online");
                catalogVersionService.setSessionCatalogVersion(processModel.getOrder().getSite().getUid()+"ProductCatalog","Staged");
                ClickToCallModel c2cModel=null;
                if(null!=processModel.getPlpC2CForm()){
                    ClickCallServicablity clickCallServicablity = getClickCallServicablity(processModel.getPlpC2CForm());
                    c2cModel=llaCommerceCheckoutService.executeCustomerCallBackRequest(processModel.getOrder(),clickCallServicablity,processModel.getNotificationType());
                }else{
                    c2cModel=llaCommerceCheckoutService.executeCustomerCallBackRequest(processModel.getOrder(),processModel.getNotificationType());
                }
                if(c2cModel.getGenesysNotificationSent()){
                    LOG.info(String.format("Click To Call triggered for Cart %s and Fallout Type :%s",processModel.getOrder().getCode(),processModel.getNotificationType().getCode()));
                    updateC2CRequested(processModel, c2cModel);
                    return Transition.OK;
                }else{
                    LOG.warn(String.format("Click To Call not triggered for Cart %s and Fallout Type :%s",processModel.getOrder().getCode(),processModel.getNotificationType().getCode()));
                    updateC2CRequested(processModel, c2cModel);
                    return Transition.NOK;
                }
            }else{
                LOG.info(String.format("Notification to Genesys is already triggered for Cart ::: %s",((FalloutProcessModel)businessProcessModel).getOrder().getCode()));
                return Transition.OK;
            }
        }
        LOG.error("Business Process is not related to Fallout Process");
        return Transition.NOK;
    }

    /**
     * Update C2C Requested
     * @param processModel
     * @param c2cModel
     */
    private void updateC2CRequested(FalloutProcessModel processModel, ClickToCallModel c2cModel) {
        final List<ClickToCallModel> previousCalls= processModel.getClickToCallsRequested();
        final List<ClickToCallModel> updatedCalls=new ArrayList<>(previousCalls);
        updatedCalls.add(previousCalls.size(), c2cModel);
        processModel.setClickToCallsRequested(updatedCalls);
        processModel.setGenesysNotificationSent(c2cModel.getGenesysNotificationSent());
        modelService.save(processModel);
        modelService.refresh(processModel);
    }

    /***
     *  Populate C2C Servicability Data for Call
     * @param form
     * @return
     */
    private ClickCallServicablity getClickCallServicablity(PLPC2CFormModel form) {
        ClickCallServicablity clickCallServicablity=new ClickCallServicablity();
        clickCallServicablity.setEmail(form.getEmail());
        clickCallServicablity.setIdentityNumber(form.getIdentityNumber());
        clickCallServicablity.setPhoneNumber(form.getPhoneNumber());
        clickCallServicablity.setProduct(form.getProduct());
        return clickCallServicablity;
    }
}
