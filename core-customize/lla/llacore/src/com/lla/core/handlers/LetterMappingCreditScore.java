/**
 *
 */
package com.lla.core.handlers;

import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import com.lla.core.model.CreditScoreModel;
import org.apache.commons.lang.StringUtils;

/**
 * The type Letter mapping credit score.
 *
 * @author US927CX
 */
public class LetterMappingCreditScore implements DynamicAttributeHandler<String, CreditScoreModel> {

	/**
	 * The constant LETTER_MAPPING_M.
	 */
	protected static final String LETTER_MAPPING_M = "M";
	/**
	 * The constant LETTER_MAPPING_L.
	 */
	protected static final String LETTER_MAPPING_L = "L";
	/**
	 * The constant LETTER_MAPPING_K.
	 */
	protected static final String LETTER_MAPPING_K = "K";
	/**
	 * The constant LETTER_MAPPING_B.
	 */
	protected static final String LETTER_MAPPING_B = "B";
	/**
	 * The constant LETTER_MAPPING_A.
	 */
	protected static final String LETTER_MAPPING_A = "A";

	/**
	 * get letter mapping
	 * @param creditScore
	 * @return
	 */
	@Override
    public String get(final CreditScoreModel creditScore) {

        if (null != creditScore) {

            if (0 < (creditScore.getMinScoreRange()) && 0 <(creditScore.getMaxScoreRange())) {

                final int minScoreRange = creditScore.getMinScoreRange().intValue();
                final int maxScoreRange = creditScore.getMaxScoreRange().intValue();

                if (between(1, 501, minScoreRange, maxScoreRange)) {
               	 return LETTER_MAPPING_M;
                }

                if (between(501, 574, minScoreRange, maxScoreRange)) {
               	 return LETTER_MAPPING_L;
                }

                if (between(575, 699, minScoreRange, maxScoreRange)) {
               	 return LETTER_MAPPING_K;
                }

                if (between(700, 899, minScoreRange, maxScoreRange)) {
               	 return  LETTER_MAPPING_B;
                }

                if (between(900, 1000, minScoreRange, maxScoreRange)) {
               	 return LETTER_MAPPING_A;
                }

            }
        }
        return null;
    }

    @Override
    public void set(final CreditScoreModel creditScore, final String value) {
        throw new UnsupportedOperationException();

    }
	/**
	 * between
	 * @param minValueInclusive
	 * @param maxValueInclusive
	 * @param minScoreRange
	 * @param maxScoreRange
	 * @return
	 */
    public static boolean between(int minValueInclusive, int maxValueInclusive, int minScoreRange, int maxScoreRange) {
        if (minScoreRange >= minValueInclusive && maxScoreRange <= maxValueInclusive)
            return true;
        else
            return false;
    }

}
