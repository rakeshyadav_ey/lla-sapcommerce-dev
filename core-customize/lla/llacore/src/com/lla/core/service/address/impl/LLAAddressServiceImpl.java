/**
 *
 */
package com.lla.core.service.address.impl;

import java.util.List;

import javax.annotation.Resource;

import com.lla.core.dao.LLAAddressDao;
import com.lla.core.jalo.Neighbourhood;
import com.lla.core.model.*;
import com.lla.core.service.address.LLAAddressService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author GF986ZE
 *
 */
public class LLAAddressServiceImpl implements LLAAddressService
{
	@Resource
	private LLAAddressDao llaAddressDao;

	@Autowired
	private FlexibleSearchService flexibleSearchService;



	@Override
	public List<LLAPanamaAddressMappingModel> getAddressMappingForGivenText(final String text)
	{

		final List<LLAPanamaAddressMappingModel> addressMappings = getLlaAddressDao().getAddressMappingForGivenText(text);
		return addressMappings;

	}
	@Override
	public List<ProvinceModel> getAllProvince()
	{
		final List<ProvinceModel> addressMappings = getLlaAddressDao().getProvinces();
		return addressMappings;
	}

	@Override
	public <T> List<T> getAddressValues(String prevNode, String nextNode) {
		List<T> addressResults=getLlaAddressDao().getAddressValues(prevNode,nextNode);
		return addressResults;
	}

	@Override
	public String getNeighbourHoodByUID(final String uid) {
		NeighbourhoodModel exampleModel=new NeighbourhoodModel();
		exampleModel.setUid(returnToken(uid));
		NeighbourhoodModel retrievedModel=flexibleSearchService.getModelByExample(exampleModel);
		return null!=retrievedModel?retrievedModel.getLabel(): StringUtils.EMPTY;
	}

	@Override
	public String getDistrictByUID(final String uid) {
		DistrictModel exampleModel=new DistrictModel();
		exampleModel.setUid(returnToken(uid));
		DistrictModel retrievedModel=flexibleSearchService.getModelByExample(exampleModel);
		return null!=retrievedModel?retrievedModel.getLabel(): StringUtils.EMPTY;
	}

	@Override
	public String getProvinceByUID(final String uid) {
		ProvinceModel exampleModel=new ProvinceModel();
		exampleModel.setUid(returnToken(uid));
		ProvinceModel retrievedModel=flexibleSearchService.getModelByExample(exampleModel);
		return null!=retrievedModel?retrievedModel.getLabel(): StringUtils.EMPTY;
	}

	@Override
	public String getCantonByUID(final String uid) {
		CantonModel exampleModel=new CantonModel();
		exampleModel.setUid(returnToken(uid));
		CantonModel retrievedModel=flexibleSearchService.getModelByExample(exampleModel);
		return null!=retrievedModel?retrievedModel.getLabel(): StringUtils.EMPTY;
	}
//TODO Temp code to fix blocker have to identify root cause
	private String returnToken(String uid){
		if(uid.contains(",")){
			return uid.substring(0,uid.indexOf(","));
		}
		return uid;
	}
	public LLAAddressDao getLlaAddressDao()
	{
		return llaAddressDao;
	}


	public void setLlaAddressDao(final LLAAddressDao llaAddressDao)
	{
		this.llaAddressDao = llaAddressDao;
	}

}
