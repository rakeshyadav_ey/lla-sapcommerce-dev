package com.lla.core.service.order.impl;

import de.hybris.platform.b2ctelcoservices.order.TmaCommerceCartResourceService;
import de.hybris.platform.b2ctelcoservices.order.impl.TmaCommerceAddToCartStrategy;
import de.hybris.platform.b2ctelcoservices.services.TmaPoService;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.fest.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;

import com.lla.core.util.LLACommonUtil;


public class LLACommerceAddToCartStrategy extends TmaCommerceAddToCartStrategy {


	@Autowired
	private LLACommonUtil llaCommonUtil;

	@Autowired
	private TmaPoService tmaPoService;

	@Autowired
	private ModelService modelService;

	@Autowired
	private SessionService sessionService;


    public LLACommerceAddToCartStrategy(final TmaCommerceCartResourceService tmaCommerceCartResourceService) {
        super(tmaCommerceCartResourceService);
    }

    @Override
    protected CartEntryModel addCartEntry(final CommerceCartParameter parameter, final long actualAllowedQuantityChange)
            throws CommerceCartModificationException
    {
        if (parameter.getUnit() == null)
        {
            parameter.setUnit(getUnit(parameter));
        }

        final CartEntryModel cartEntryModel = getCartService().addNewEntry(parameter.getCart(), parameter.getProduct(),
                actualAllowedQuantityChange, parameter.getUnit(), APPEND_AS_LAST, checkAddToPresent(parameter));
        cartEntryModel.setDeliveryPointOfService(parameter.getPointOfService());

        return cartEntryModel;
    }

    @Override
 	public CommerceCartModification addToCart(final CommerceCartParameter parameter) throws CommerceCartModificationException
 	{
 		validateApplicableEligiblity(parameter);
 		final CommerceCartModification modification = doAddToCart(parameter);
 		getCommerceCartCalculationStrategy().calculateCart(parameter);
		if (modification.getEntry() != null && modification.getEntry().getBpo() != null)
		{
			parameter.setBpoCode(modification.getEntry().getBpo().getCode());
		}
 		afterAddToCart(parameter, modification);
 		if (modification.getQuantityAdded() > 0 && modification.getEntry() != null)
 		{
 			getTmaCommerceCartResourceService().updateResources(parameter, modification);
 		}
		parameter.setCreateNewEntry(!checkAddToPresent(parameter));
 		mergeEntry(modification, parameter);
		if (llaCommonUtil.isSitePanama() && llaCommonUtil
				.containsFirstLevelCategoryWithCode(tmaPoService.getPoForCode(parameter.getProduct().getCode()), "devices"))
		{

			associateDeviceEntryToPlanEntry(parameter);

		}
 		getCommerceCartCalculationStrategy().calculateCart(parameter);

 		return modification;
 	}

    private boolean checkAddToPresent(final CommerceCartParameter parameter) {

		if (llaCommonUtil.isSitePanama() && (llaCommonUtil.containsFirstLevelCategoryWithCode(parameter.getProduct(), "devices")
				|| llaCommonUtil.containsFirstLevelCategoryWithCode(parameter.getProduct(), "postpaid")))
		{

			return false;
   	    }
		if(llaCommonUtil.isSiteJamaica()){
			return false;
		}

        return !(Collections.isEmpty( getCartService().getEntriesForProduct(parameter.getCart(), parameter.getProduct())));
    }

	private void associateDeviceEntryToPlanEntry(final CommerceCartParameter parameter)
	{

		final String deviceCode = parameter.getProduct().getCode();
		final String planCode = (String) sessionService.getAttribute("lastAddedPlanCode");
		final CartModel cartModel = parameter.getCart();

		if (null != cartModel && !StringUtils.isEmpty(planCode))
		{
			final AbstractOrderEntryModel masterEntry = getMasterEntry(deviceCode, planCode, cartModel);
			final List<AbstractOrderEntryModel> childEntryList = new ArrayList<>();
			for (final AbstractOrderEntryModel cartEntry : cartModel.getEntries())
			{
				if (deviceCode.equalsIgnoreCase(cartEntry.getProduct().getCode()) && null != masterEntry)
				{
					if (cartEntry.getMasterEntry() == null)
					{
						cartEntry.setMasterEntry(masterEntry);
						cartEntry.setCalculated(false);
						if (CollectionUtils.isEmpty(masterEntry.getChildEntries()))
						{
							childEntryList.add(cartEntry);
						}
						else
						{
							final Collection<AbstractOrderEntryModel> entries = new ArrayList<AbstractOrderEntryModel>(
									masterEntry.getChildEntries());
							childEntryList.addAll(entries);
							childEntryList.add(cartEntry);
						}
					}
				}
			}
			masterEntry.setChildEntries(childEntryList);
			masterEntry.setCalculated(false);
		}
		cartModel.setCalculated(false);
		modelService.saveAll();
		parameter.setEnableHooks(true);
		parameter.setCart(cartModel);

	}

	private AbstractOrderEntryModel getMasterEntry(final String deviceCode, final String planCode, final CartModel cartModel)
	{

		final AbstractOrderEntryModel masterEntry = null;
		for (final AbstractOrderEntryModel entry : cartModel.getEntries())
		{
			if (entry.getProduct().getCode().equalsIgnoreCase(planCode))
			{
				if (CollectionUtils.isEmpty(entry.getChildEntries())
						|| llaCommonUtil.containsFirstLevelCategoryWithCode(tmaPoService.getPoForCode(planCode), "4Pbundles"))
				{
					return entry;
				}

			}

		}
		return masterEntry;

	}
}
