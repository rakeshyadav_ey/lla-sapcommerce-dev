package com.lla.core.service.customer.impl;

import com.lla.core.service.customer.LLACustomerService;
import com.lla.core.service.product.LLAProductService;
import com.lla.core.util.LLACommonUtil;
import com.lla.mulesoft.integration.dto.PlanServices;
import com.lla.mulesoft.integration.dto.ServiceProducts;
import com.lla.mulesoft.integration.dto.ViewPlanResponse;
import de.hybris.platform.b2ctelcoservices.enums.TmaAccessType;
import de.hybris.platform.b2ctelcoservices.enums.TmaServiceType;
import de.hybris.platform.b2ctelcoservices.model.TmaBillingAccountModel;
import de.hybris.platform.b2ctelcoservices.model.TmaSubscribedProductModel;
import de.hybris.platform.b2ctelcoservices.model.TmaSubscriptionAccessModel;
import de.hybris.platform.b2ctelcoservices.model.TmaSubscriptionBaseModel;
import de.hybris.platform.b2ctelcoservices.services.TmaBillingAccountService;
import de.hybris.platform.b2ctelcoservices.services.TmaSubscribedProductService;
import de.hybris.platform.b2ctelcoservices.services.TmaSubscriptionAccessService;
import de.hybris.platform.b2ctelcoservices.services.TmaSubscriptionBaseService;
import de.hybris.platform.commerceservices.customer.dao.CustomerDao;
import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.SetUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.bouncycastle.math.raw.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.lla.core.dao.LLACustomerAccountDao;
import com.lla.core.model.TmaServiceAccountModel;
import com.lla.core.service.customer.LLACustomerService;
import com.lla.core.service.product.LLAProductService;
import com.lla.mulesoft.integration.dto.PlanServices;
import com.lla.mulesoft.integration.dto.ServiceProducts;
import com.lla.mulesoft.integration.dto.ViewPlanResponse;
import io.swagger.models.Model;


public class LLACustomerServiceImpl extends DefaultCustomerService implements LLACustomerService
{

	private static final Logger LOG = Logger.getLogger(LLACustomerServiceImpl.class);
	private static final String ACCOUNT_ADDED_SUCCESSFULLY="success";

	@Autowired
	private TmaSubscriptionBaseService tmaSubscriptionBaseService;

	@Autowired
	private TmaSubscriptionAccessService tmaSubscriptionAccessService;

	@Autowired
	private TmaBillingAccountService tmaBillingAccountService;

	@Autowired
	private ModelService modelService;

	@Autowired
	private UserService userService;

	@Autowired
	private ProductService productService;

	@Autowired
	private LLAProductService llaProductService;

	@Autowired
   private CartService cartService;

    @Autowired
    private LLACommonUtil llaCommonUtil;

    @Autowired
	private TmaSubscribedProductService tmaSubscribedProductService;

	@Resource(name = "customerAccountDao")
	private LLACustomerAccountDao llaCustomerAccountDao;

	private Map<String, String> statusMap;

	/**
	 * Service constructor
	 *
	 * @param customerDao
	 *           DAO for retrieving customer
	 * @param regexp
	 */
	public LLACustomerServiceImpl(final CustomerDao customerDao, final String regexp)
	{
		super(customerDao, regexp);
	}

	/**
	 * Creating Subscription Plan Retrieved from Mulesoft for Customer
	 *
	 * @param viewPlanResponseList
	 * @param customerId
	 */
	@Override
	public void createCustomerSubscriptionPlans(final List<ViewPlanResponse> viewPlanResponseList, final String customerId)
	{

		removeOlderSubscriptions(customerId);
		if (CollectionUtils.isNotEmpty(viewPlanResponseList))
		{

			for (final ViewPlanResponse viewPlanResponse : viewPlanResponseList)
			{
				if (Integer.valueOf(viewPlanResponse.getNoOfServices()) != 0)
				{
					final List<PlanServices> servicePlans = viewPlanResponse.getServices();
					for (final PlanServices planService : servicePlans)
					{

						setSubscriptionPlan(planService, viewPlanResponse, customerId);

					}
				}
				else
				{
					LOG.info(String.format("No Services associated with Account Id ::: %s ", viewPlanResponse.getAccountNo()));
				}
			}

		}
		else
		{
			LOG.info(String.format("No new subscription is fetched from Service call for Customer ::: %s ", customerId));
		}

	}

  @Override
    public void resetDetailsForAnonymousCart() {
       if(userService.isAnonymousUser(userService.getCurrentUser())){
           CartModel cartModel=cartService.getSessionCart();
           CustomerModel customerModel= (CustomerModel) cartModel.getUser();
          /* if(!("cabletica".equalsIgnoreCase(cartModel.getSite().getUid()) && "cableticaStore".equalsIgnoreCase(cartModel.getStore().getUid()))){
               customerModel.setClickToCallRequest(null);
           } */
           customerModel.setMobilePhone(StringUtils.EMPTY);
           customerModel.setAdditionalEmail(StringUtils.EMPTY);
           customerModel.setFixedPhone(StringUtils.EMPTY);
           modelService.save(customerModel);
       }

    }
	private void setSubscriptionPlan(final PlanServices planService, final ViewPlanResponse viewPlanResponse,
			final String customerId)
	{

		final Set<TmaSubscriptionBaseModel> baseModelSet = new HashSet<>();
		TmaBillingAccountModel prevBillingAccount = null;
		TmaBillingAccountModel billingAccountModel = null;

		final String billingSystemId = planService.getSourceEPC();
		final String subscriberIdentity = planService.getUniqueServiceId();

		if (CollectionUtils.isNotEmpty(planService.getProducts()))
		{
			final List<ServiceProducts> serviceProductsList = planService.getProducts();
			final String subscribedProductCode = null != serviceProductsList.get(0) ? serviceProductsList.get(0).getProductCode()
					: StringUtils.EMPTY;
			try
			{
				final String commerceProductCode = llaProductService.fetchCommerceProductCodeUsingBSSCode(subscribedProductCode,
						billingSystemId);
				LOG.info(String.format("Commerce Product Code %s against Source System code %s  ", commerceProductCode,
						subscribedProductCode));
				if (StringUtils.isNotEmpty(commerceProductCode))
				{
					TmaSubscribedProductModel subscribedProductModel = null;
					try
					{
						prevBillingAccount = tmaBillingAccountService.findBillingAccount(billingSystemId,
								viewPlanResponse.getAccountNo());
					}
					catch (final ModelNotFoundException modelNotFoundException)
					{
						prevBillingAccount = null;
						LOG.error(String.format("No Previous Billing Account with id %s", viewPlanResponse.getAccountNo()));
					}
					billingAccountModel = getTmaBillingAccountModel(customerId, prevBillingAccount, viewPlanResponse, billingSystemId);
					final TmaSubscriptionBaseModel subscriptionBaseModel = mapSubscriptionBases(billingAccountModel, viewPlanResponse,
							baseModelSet, billingSystemId, subscriberIdentity);
					final TmaSubscriptionAccessModel accessModel = mapCustomerSubscriptionAccess(customerId, billingSystemId,
							subscriberIdentity, subscriptionBaseModel);
					subscribedProductModel = generateTmaSubscribedProducts(planService, billingSystemId, commerceProductCode,
							subscriptionBaseModel);

					modelService.saveAll(subscribedProductModel, subscriptionBaseModel, accessModel);
					modelService.refresh(subscribedProductModel);
					modelService.refresh(subscriptionBaseModel);
					modelService.refresh(accessModel);
					billingAccountModel.setSubscriptionBases(!baseModelSet.isEmpty() ? baseModelSet : SetUtils.emptySet());
					modelService.save(billingAccountModel);
					modelService.refresh(billingAccountModel);
				}
			}
			catch (ModelNotFoundException | UnknownIdentifierException | AmbiguousIdentifierException
					| IllegalArgumentException exception)
			{
				LOG.info(String.format("Subscribed Product Code :::  %s is not available in Commerce  ", subscribedProductCode));
			}

		}

	}


	private void removeOlderSubscriptions(final String customerId)
	{
		modelService.removeAll(tmaSubscriptionAccessService.getSubscriptionAccessesByPrincipalUid(customerId));
		modelService.removeAll(tmaSubscriptionBaseService.getSubscriptionBases(customerId));
	}

	/**
	 *
	 * @param planService
	 * @param billingSystemId
	 * @param commerceProductCode
	 * @param subscriptionBaseModel
	 * @return
	 */
	private TmaSubscribedProductModel generateTmaSubscribedProducts(final PlanServices planService, final String billingSystemId,
			final String commerceProductCode, final TmaSubscriptionBaseModel subscriptionBaseModel)
	{
		TmaSubscribedProductModel subscribedProductModel;
		final Set<TmaSubscribedProductModel> subscribedProductModelSet = new HashSet();
		final String serviceStatus = getStatusMap().get(planService.getServiceStatusCode());
		subscribedProductModel = tmaSubscribedProductService.createNewSubscribedProduct(new Date(), billingSystemId, serviceStatus);
		subscribedProductModel.setProductCode(commerceProductCode);
		subscribedProductModel.setServiceType(TmaServiceType.TARIFF_PLAN);
		subscribedProductModelSet.add(subscribedProductModel);
		subscriptionBaseModel.setSubscribedProducts(subscribedProductModelSet);
		return subscribedProductModel;
	}

	/**
	 *
	 * @param customerId
	 * @param prevBillingAccount
	 * @param viewPlanResponse
	 * @param billingSystemId
	 * @return
	 */
	private TmaBillingAccountModel getTmaBillingAccountModel(final String customerId,
			final TmaBillingAccountModel prevBillingAccount, final ViewPlanResponse viewPlanResponse, final String billingSystemId)
	{
		TmaBillingAccountModel billingAccountModel;
		if (prevBillingAccount == null)
		{
			billingAccountModel = tmaBillingAccountService.createBillingAccount(billingSystemId, viewPlanResponse.getAccountNo());
			billingAccountModel.setCustomer((CustomerModel) userService.getUserForUID(customerId));
		}
		else
		{
			billingAccountModel = prevBillingAccount;
		}
		return billingAccountModel;
	}

	/**
	 *
	 * @param customerId
	 * @param billingSystemId
	 * @param subsriberIdentity
	 * @param subscriptionBaseModel
	 * @return
	 */
	private TmaSubscriptionAccessModel mapCustomerSubscriptionAccess(final String customerId, final String billingSystemId,
			final String subsriberIdentity, final TmaSubscriptionBaseModel subscriptionBaseModel)
	{
		TmaSubscriptionAccessModel accessModel = null;
		final Set<TmaSubscriptionAccessModel> accessModelSet = new HashSet();
		try
		{
			accessModel = tmaSubscriptionAccessService.getSubscriptionAccessByPrincipalAndSubscriptionBase(customerId,
					billingSystemId, subsriberIdentity);
			LOG.info("Try success");
		}
		catch (ModelNotFoundException | AmbiguousIdentifierException exception)
		{
			accessModel = tmaSubscriptionAccessService.createSubscriptionAccessModel(customerId, billingSystemId,
					subscriptionBaseModel, TmaAccessType.OWNER);
			LOG.info("After Catch");
		}
		accessModelSet.add(accessModel);
		subscriptionBaseModel.setSubscriptionAccesses(accessModelSet);
		modelService.save(subscriptionBaseModel);
		modelService.save(accessModel);
		return accessModel;
	}

	/**
	 *
	 * @param billingAccountModel
	 * @param viewPlanResponse
	 * @param baseModelSet
	 * @param billingSystemId
	 * @param subsriberIdentity
	 * @return
	 */
	private TmaSubscriptionBaseModel mapSubscriptionBases(final TmaBillingAccountModel billingAccountModel,
			final ViewPlanResponse viewPlanResponse, final Set<TmaSubscriptionBaseModel> baseModelSet, final String billingSystemId,
			final String subsriberIdentity)
	{
		TmaSubscriptionBaseModel subscriptionBaseModel;
		try
		{
			subscriptionBaseModel = tmaSubscriptionBaseService.getSubscriptionBaseByIdentity(subsriberIdentity);
			LOG.info("In Try" + subscriptionBaseModel + "  " + subscriptionBaseModel.getSubscriberIdentity());

		}
		catch (final ModelNotFoundException exception)
		{
			subscriptionBaseModel = tmaSubscriptionBaseService.createSubscriptionBase(subsriberIdentity, billingSystemId,
					viewPlanResponse.getAccountNo());
			LOG.info("In catch" + subscriptionBaseModel + "  " + subscriptionBaseModel.getSubscriberIdentity());
		}

		baseModelSet.add(subscriptionBaseModel);
		subscriptionBaseModel.setBillingAccount(billingAccountModel);
		modelService.save(subscriptionBaseModel);
		modelService.refresh(subscriptionBaseModel);
		LOG.info("subscriptionBaseModel " + subscriptionBaseModel);
		return subscriptionBaseModel;
	}

	public Map<String, String> getStatusMap()
	{
		return statusMap;
	}

	@Required
	public void setStatusMap(final Map<String, String> statusMap)
	{
		this.statusMap = statusMap;
	}

	@Override
	public String updateCustomerBillingAccount(final CustomerModel customer, final String accountId, final String systemId)
	{
		final TmaBillingAccountModel billingAccount = modelService.create(TmaBillingAccountModel.class);
		final Set<TmaBillingAccountModel> tmaBillingAccount = new HashSet<TmaBillingAccountModel>();
		billingAccount.setBillingAccountId(accountId);
		billingAccount.setBillingSystemId(systemId);
		tmaBillingAccount.add(billingAccount);
		customer.setBillingAccounts(tmaBillingAccount);
		modelService.saveAll(billingAccount, customer);
		modelService.refresh(billingAccount);
		modelService.refresh(customer);
		LOG.info("CustomerModel " + customer);
		return ACCOUNT_ADDED_SUCCESSFULLY;
	}

	@Override
	public String updateCustomerServiceAccount(final CustomerModel customer, final String accountId, final String systemId)
	{
		final TmaServiceAccountModel serviceAccount = modelService.create(TmaServiceAccountModel.class);
		final Set<TmaServiceAccountModel> tmaServiceAccount = new HashSet<TmaServiceAccountModel>();
		serviceAccount.setServiceAccountId(accountId);
		serviceAccount.setServiceSystemId(systemId);
		tmaServiceAccount.add(serviceAccount);
		customer.setServiceAccounts(tmaServiceAccount);
		modelService.saveAll(serviceAccount, customer);
		modelService.refresh(serviceAccount);
		modelService.refresh(customer);
		LOG.info("CustomerModel " + customer);
		return ACCOUNT_ADDED_SUCCESSFULLY;
	}

	@Override
	public CustomerModel getCutsomerForCorrelationID(final String correlationId)
	{
		return llaCustomerAccountDao.findCustomerForCorrelationID(correlationId);

	}
}
