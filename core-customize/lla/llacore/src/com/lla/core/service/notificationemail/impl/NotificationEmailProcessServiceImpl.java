package com.lla.core.service.notificationemail.impl;

import com.lla.core.dao.NotificationEmailProcessDao;
import com.lla.core.service.notificationemail.NotificationEmailProcessService;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.falloutprocess.model.FalloutProcessModel;
import de.hybris.platform.ordersplitting.model.NotificationEmailProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NotificationEmailProcessServiceImpl implements NotificationEmailProcessService{
    private static final String START_NODE = "notification.process.start.node";
    private static final String CSA_START_NODE = "csa.notification.process.start.node";
    private static final String FALLOUT_START_NODE = "fallout.process.start.node";
    private static final Logger LOG = Logger.getLogger(NotificationEmailProcessServiceImpl.class);
    @Autowired
    private NotificationEmailProcessDao notificationEmailProcessDao;
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;
    @Autowired
    private ConfigurationService configurationService;
    @Autowired
    private BusinessProcessService businessProcessService;

    /**
     * Fetch Failed Notification email Process
     * @param siteId
     * @param filesDaysOld
     * @return
     */
    @Override
    public int  fetchFailedNotificationProcess(final String siteId, int filesDaysOld) {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, -filesDaysOld);
        final Date validDate = cal.getTime();
        List<BusinessProcessModel> list=notificationEmailProcessDao.getFailedNotificationProcess(siteId,validDate);
        if(null!=list){
            for(BusinessProcessModel process:list){
                if(process instanceof  NotificationEmailProcessModel){
                    NotificationEmailProcessModel item=(NotificationEmailProcessModel)process;
                    OrderModel order=(OrderModel) item.getOrder();
                    if(null!=order){
                        if(llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite())){
                            repairActionForCabletica(process, item);
                        }else {
                            repairActionForNonCabletica(process, item);
                        }
                    }else{
                        LOG.error(String.format("No Order Mapped with Process %s",item.getCode()));
                        return 0;
                    }

                }
            }
            return list.size();
        }else{
            LOG.info(String.format("No Pending Notification found::: Finishing job..."));
            return 0;
        }
    }

    /**
     * Repair action for Cabletica
     * @param process
     * @param item
     */
    private void repairActionForCabletica(BusinessProcessModel process, NotificationEmailProcessModel item) {
        if(!item.isAgentNotificationSent() || !item.isC2cNotificationSent() || !item.isPlaceOrderNotificationSent()){
            LOG.info(String.format("Restarting process for ::: %s ", process.getCode()));
            businessProcessService.restartProcess(process,configurationService.getConfiguration().getString(START_NODE));
        }else{
            LOG.info(String.format("All notification are already triggered for Process ::: %s", process.getCode()));
        }
    }

    /**
     *  Repair Fallout Action for Cabletica
     * @param process
     * @param item
     */
    private void repairFalloutActionForCabletica(BusinessProcessModel process, FalloutProcessModel item) {
        if(!item.isAgentNotificationSent() || !item.isGenesysNotificationSent()){
            LOG.info(String.format("Restarting process for ::: %s ", process.getCode()));
            businessProcessService.restartProcess(process,configurationService.getConfiguration().getString(FALLOUT_START_NODE));
        }else{
            LOG.info(String.format("All notification are already triggered for Process ::: %s", process.getCode()));
        }
    }

    /**
     * Repair Action for Non cabletica
     * @param process
     * @param item
     */
    private void repairActionForNonCabletica(BusinessProcessModel process, NotificationEmailProcessModel item) {
        if(!item.isAgentNotificationSent()  || !item.isPlaceOrderNotificationSent()){
            LOG.info(String.format("Restarting process for ::: %s ", process.getCode()));
            businessProcessService.restartProcess(process,configurationService.getConfiguration().getString(CSA_START_NODE));
       }else{
            LOG.info(String.format("All notification are already triggered for Process ::: %s", process.getCode()));
        }
    }

    /**
     * Fetch Failed Notification email Process
     * @param siteId
     * @param numHours
     * @return
     */
    @Override
    public int  fetchFailedFalloutProcess(final String siteId, int numHours) {
        List<BusinessProcessModel> list=notificationEmailProcessDao.getFailedFalloutProcess(siteId,numHours);
        if(CollectionUtils.isNotEmpty(list)){
            for(BusinessProcessModel process:list){
                if(process instanceof FalloutProcessModel){
                    FalloutProcessModel item=(FalloutProcessModel)process;
                    AbstractOrderModel order=(AbstractOrderModel) item.getOrder();
                    if(null!=order){
                        if(llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite()) || llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite())) {
                            repairFalloutActionForCabletica(process, item);
                        }

                    }else{
                        LOG.error(String.format("No Order Mapped with Process %s",item.getCode()));
                        return 0;
                    }

                }
            }
            return list.size();
        }else{
            LOG.info(String.format("No Pending Notification found::: Finishing job..."));
            return 0;
        }
    }
}
