/**
 *
 */
package com.lla.core.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import com.lla.core.util.LLACommonUtil;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.commerceservices.consent.impl.DefaultCommerceConsentService;
import de.hybris.platform.commerceservices.event.AbstractConsentEvent;
import de.hybris.platform.commerceservices.event.ConsentGivenEvent;
import de.hybris.platform.commerceservices.model.consent.ConsentModel;
import de.hybris.platform.commerceservices.model.consent.ConsentTemplateModel;
import de.hybris.platform.core.model.user.CustomerModel;

import com.lla.core.service.LLACommerceConsentService;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author Rohit25.Gupta
 *
 */
public class DefaultLLACommerceConsentService extends DefaultCommerceConsentService implements LLACommerceConsentService
{
	@Autowired
	private LLACommonUtil llaCommonUtil;
	@Override
	public void giveConsent(final CustomerModel customer, final ConsentTemplateModel consentTemplate)
	{
		if(llaCommonUtil.isSiteJamaica()) {
			giveConsentJamaica(customer, consentTemplate);
		}
		else{
			super.giveConsent(customer, consentTemplate);
		}
	}

	/**
	 * @param customer
	 * @param consentTemplate
	 */
	protected void giveConsentPuertorico(CustomerModel customer, ConsentTemplateModel consentTemplate)
	{
		validateParameterNotNullStandardMessage("customer", customer);
		validateParameterNotNullStandardMessage("consentTemplate", consentTemplate);
		final ConsentModel consent = createConsentModel(customer, consentTemplate);
		consent.setConsentGivenDate(getTimeService().getCurrentTime());
		getModelService().save(consent);
		getEventService().publishEvent(initializeEvent(new ConsentGivenEvent(), consent));
		
	}

	protected void giveConsentJamaica(CustomerModel customer, ConsentTemplateModel consentTemplate) {
		validateParameterNotNullStandardMessage("customer", customer);
		validateParameterNotNullStandardMessage("consentTemplate", consentTemplate);

		final ConsentModel consent = createConsentModel(customer, consentTemplate);
		consent.setConsentGivenDate(getTimeService().getCurrentTime());
		getModelService().save(consent);
		getEventService().publishEvent(initializeEvent(new ConsentGivenEvent(), consent));
	}

	@Override
	protected ConsentModel createConsentModel(final CustomerModel customer, final ConsentTemplateModel consentTemplate)
	{
		final ConsentModel consent = getModelService().create(ConsentModel._TYPECODE);
		consent.setConsentTemplate(consentTemplate);
		consent.setCustomer(customer);
		return consent;
	}

	@Override
	protected AbstractConsentEvent initializeEvent(final AbstractConsentEvent event, final ConsentModel consent)
	{
		event.setConsent(consent);
		return event;
	}
}
