package com.lla.core.service.order.impl;

import de.hybris.platform.b2ctelcoservices.enums.TmaAbstractOrderPriceType;
import de.hybris.platform.b2ctelcoservices.model.TmaAbstractOrderChargePriceModel;
import de.hybris.platform.b2ctelcoservices.model.TmaAbstractOrderCompositePriceModel;
import de.hybris.platform.b2ctelcoservices.model.TmaAbstractOrderPriceModel;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.pricing.services.impl.DefaultTmaCalculationService;
import de.hybris.platform.b2ctelcoservices.services.TmaRegionService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.OrderRequiresCalculationStrategy;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.subscriptionservices.model.RecurringChargeEntryModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionPricePlanModel;
import de.hybris.platform.util.TaxValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.lla.core.service.order.LLACalculationService;
import com.lla.core.util.LLACommonUtil;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import org.apache.log4j.Logger;

public class LLACalculationServiceImpl extends DefaultTmaCalculationService implements LLACalculationService {

    private static final Logger LOG = Logger.getLogger(LLACalculationServiceImpl.class);
    private static final String PLAN_TYPE = "plantype";
    private static final String MOBILE_PRICE_TYPE = "mobilepricetype";
    private static final String PRICE_PLAN = "priceplan";
    private static final String PLAN_CODE = "plancode";

    private static final String TELEVISIONPLANCATEGORY = "tvcabletica";
    private static final String TRIPLEPLAY = "3PInternetTvTelephone";
    private static final String DOBLEPLAY = "2PInternetTv";
    private static final String INTERNET = "internetcabletica";
    private static final String ADDONS = "channelsaddoncabletica";
    private static final String PLANS_CATEGORY = "plans";
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;
    @Autowired
    private ProductService productService;
    @Autowired
    private OrderRequiresCalculationStrategy orderRequiresCalculationStrategy;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private SessionService sessionService;
    @Autowired
    private CommonI18NService commonI18NService;
    @Resource(name = "commerceCategoryService")
    private CommerceCategoryService commerceCategoryService;
    @Autowired
	 private LLACommonUtil llaCommonUtil;

	 @Autowired
	 private TmaRegionService tmaRegionService;


	 @Override
    public void calculateEntries(final AbstractOrderModel order, final boolean forceRecalculate) throws CalculationException
    {
        double subtotal = 0.0;
        if(llaCommonUtil.isSiteJamaica() || llaCommonUtil.isSitePanama() || llaCommonUtil.isSiteVTR() && null != order){
      	  calculateMonthlyPrice(order);
        }
        for (final AbstractOrderEntryModel e : order.getEntries())
        {
            if(!(order instanceof OrderModel && llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())  && ((OrderModel) order).getParentOrder()!=null)){
                recalculateOrderEntryIfNeeded(e, forceRecalculate);
            }
            subtotal += e.getTotalPrice().doubleValue();
        }
        order.setTotalPrice(Double.valueOf(subtotal));

        if (llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite()) && configurationService.getConfiguration().getBoolean("lcpr.postpaid.plans.active")) {
            for (final AbstractOrderEntryModel entry: order.getEntries()) {
                Double entryRecurringPrice = Double.valueOf(0.00D);
                final Collection < PriceRowModel > priceRowModelCollection = entry.getProduct().getEurope1Prices();
                final Collection < CategoryModel > categoryModelCollection = entry.getProduct().getSupercategories();
                for (final PriceRowModel pricePlanModel: priceRowModelCollection) {
                    if (pricePlanModel instanceof SubscriptionPricePlanModel) {
                        final Collection < RecurringChargeEntryModel > recurringChargeEntries = ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
                        for (final RecurringChargeEntryModel recurringChargeEntry: recurringChargeEntries) {
                            if (categoryModelCollection.contains(commerceCategoryService.getCategoryForCode("unlimitedplans"))) {
                                if (recurringChargeEntry.getQty() == order.getNumberOfLines()) {
                                    if (BooleanUtils.isNotTrue(order.getAutoPayActive())) {
                                        entryRecurringPrice += recurringChargeEntry.getWithoutAutoPayPrice();

                                    } else {
                                        entryRecurringPrice += recurringChargeEntry.getPrice().doubleValue();
                                    }
                                    break;
                                }
                            } else {
                                entryRecurringPrice += recurringChargeEntry.getPrice().doubleValue();
                            }
                        }
                        break;
                    }
                }

                entry.setEntryRecurringPrice(entryRecurringPrice);
                getModelService().save(entry);
            }
        }
    }

    private void calculateMonthlyPrice(final AbstractOrderModel order) {
        for (final AbstractOrderEntryModel entry: order.getEntries()) {
            if (llaCommonUtil.isSiteVTR() && llaCommonUtil.containsFirstLevelCategoryWithCode(entry.getProduct(),"devices"))
            {
                setDevicePrices(entry);
            }
            final Collection < PriceRowModel > priceRowModelCollection = entry.getProduct().getOwnEurope1Prices();
            for (final PriceRowModel pricePlanModel: priceRowModelCollection) {
                if (pricePlanModel instanceof SubscriptionPricePlanModel) {
                    final Collection < RecurringChargeEntryModel > recurringChargeEntries = ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
                    for (final RecurringChargeEntryModel recurringChargeEntry: recurringChargeEntries) {
                        final TmaProductOfferingModel product = (TmaProductOfferingModel) recurringChargeEntry.getSubscriptionPricePlanRecurring().getProduct();
                        final Double totalPrice = recurringChargeEntry.getPrice().doubleValue() * entry.getQuantity();
//        	               if ((llaCommonUtil.isSiteJamaica() && CollectionUtils.isNotEmpty(product.getEurope1Discounts()) && BooleanUtils.isTrue(llaCommonUtil.is3PBundlePresent(order))) || (llaCommonUtil.isSitePanama() && CollectionUtils.isNotEmpty(product.getEurope1Discounts()))) {
        	               if ((llaCommonUtil.isSiteJamaica() && CollectionUtils.isNotEmpty(product.getEurope1Discounts())) || (llaCommonUtil.isSitePanama() && CollectionUtils.isNotEmpty(product.getEurope1Discounts()))) {
                        for (final DiscountRowModel discount: product.getEurope1Discounts()) {
                                Double priceData;
                                Boolean discountFlag;
                                final Double actualPrice = recurringChargeEntry.getPrice().doubleValue() * entry.getQuantity();
                                if (discount.getAbsolute())
                                {
                                    final double discountedValue = BigDecimal.valueOf(recurringChargeEntry.getPrice().doubleValue() - discount.getValue()).setScale(3).doubleValue();
                                    priceData = discount.getValue() * entry.getQuantity();
                                    entry.setMonthlyPrice(priceData);
                                    entry.setDiscountedValue(discountedValue);
                                    LOG.info(String.format("Discounted price is :: %s, Discounted value is:: %s",entry.getMonthlyPrice(), entry.getDiscountedValue()));
                                }
                                else {
                                    priceData = BigDecimal.valueOf(totalPrice * (discount.getValue() / 100)).setScale(3).doubleValue();
                                    double discountedPrice=totalPrice-priceData;
                                    entry.setMonthlyPrice(discountedPrice);
                                    entry.setDiscountedValue(priceData);
                                    LOG.info(String.format("Discounted price is :: %s, Discounted value is:: %s",entry.getMonthlyPrice(), entry.getDiscountedValue()));
                                }
                                if (discount.getValue() != null)
                                {
                                    discountFlag = true;
                                }
                                else
                                {
                                    discountFlag = false;
                                }
                                entry.setDiscountFlag(discountFlag);
                                entry.setActualPrices(actualPrice);
                                LOG.info(String.format("actual price %s, discountflag %s",entry.getActualPrices(), entry.getDiscountFlag()));
                            }
                        }
                        else if (llaCommonUtil.isSiteVTR())
                        {
                            if(product.getEurope1Discounts().isEmpty())
                            {
                                entry.setBasePrice(recurringChargeEntry.getPrice());
                                entry.setMonthlyPrice(totalPrice);
                                entry.setTotalPrice(totalPrice);
                            }
                           else if (validCategory(entry) && !product.getEurope1Discounts().isEmpty())
                            {
                                calculateDiscount(entry);
                            }
                            else
                                {
                                for (final DiscountRowModel discount: product.getEurope1Discounts())
                                 {

                                    Double priceData;
                                    if (discount.getAbsolute()) {
                                    	final double discountedValue = BigDecimal.valueOf(recurringChargeEntry.getPrice().doubleValue() - discount.getValue()).setScale(3).doubleValue();
                                        priceData = BigDecimal.valueOf(discount.getValue() * entry.getQuantity()).setScale(3).doubleValue();
                                        entry.setMonthlyPrice(priceData);
                                        entry.setTotalPrice(priceData);
                                        entry.setDiscountedValue(discountedValue);

                                    }
                                    else
                                        {
                                        priceData = BigDecimal.valueOf(totalPrice * (discount.getValue() / 100)).setScale(3).doubleValue();
                                        final double price = BigDecimal.valueOf((totalPrice - priceData) * entry.getQuantity()).setScale(3).doubleValue();
                                        entry.setDiscountedValue(priceData);
                                        entry.setDiscountPercentage(discount.getValue().toString());
                                        entry.setMonthlyPrice(price);
                                        entry.setTotalPrice(price);

                                    }
                                    entry.setDiscountPeriod(discount.getDiscountPeriod());
                                    entry.setBasePrice(BigDecimal.valueOf(recurringChargeEntry.getPrice()).setScale(3).doubleValue());
                                }
                            }
                            entry.setCalculated(true);
                            order.setCalculated(true);
                        }
                        else {
                            entry.setBasePrice(recurringChargeEntry.getPrice());
                            entry.setActualPrices(recurringChargeEntry.getPrice());
                            entry.setMonthlyPrice(totalPrice);
                            entry.setTotalPrice(totalPrice);
                        }
                        //TSE-127 : setting discount period and number.
                        if(llaCommonUtil.isSiteJamaica() && Objects.nonNull(entry.getProduct()) && entry.getProduct() instanceof TmaProductOfferingModel)
                        {
                            final TmaProductOfferingModel tmaProductOffering = ((TmaProductOfferingModel) entry.getProduct());
                            if(BooleanUtils.isTrue(tmaProductOffering.getPromoFlag()))
                            {
                                if(Objects.nonNull(tmaProductOffering.getDiscountPeriod())){
                                    entry.setDiscountPeriodEnum(tmaProductOffering.getDiscountPeriod());
                                }
                                if(Objects.nonNull(tmaProductOffering.getDiscountNumber())){
                                    entry.setDiscountNumber(tmaProductOffering.getDiscountNumber());
                                }
                            }
                        }
                        getModelService().save(entry);
                        getModelService().save(order);
                    }
                }
            }
        }

    }

    private void setDevicePrices(final AbstractOrderEntryModel entry) {
        final AbstractOrderModel cart = entry.getOrder();
        if (Objects.nonNull(cart) && StringUtils.isEmpty(cart.getDevicePriceType())) {
            final Map < String, String > plan = sessionService.getAttribute(PRICE_PLAN);
            if (Objects.nonNull(plan) && plan.containsKey(MOBILE_PRICE_TYPE)) {
                cart.setDevicePriceType(plan.get(MOBILE_PRICE_TYPE));
                cart.setDevicePlanCode(plan.get(PLAN_CODE));
                cart.setPlanType(plan.get(PLAN_TYPE));
            }
        }
        final Collection < PriceRowModel > priceRowModelCollection = entry.getProduct().getOwnEurope1Prices();
        for (final PriceRowModel pricePlanModel: priceRowModelCollection) {
            //compra
            if ("compra".equalsIgnoreCase(cart.getDevicePriceType()) && pricePlanModel.getMobilePriceType().getCode().equalsIgnoreCase(cart.getDevicePriceType())) {
                final double price = BigDecimal.valueOf(pricePlanModel.getPrice()).setScale(3).doubleValue();
                entry.setBasePrice(price);
                entry.setTotalPrice(price);

            }

            //arriendo prices
            if ("ARRIENDO".equalsIgnoreCase(cart.getDevicePriceType()) && pricePlanModel.getMobilePriceType().getCode().equalsIgnoreCase(cart.getDevicePriceType()) &&
                    cart.getDevicePlanCode().equalsIgnoreCase(pricePlanModel.getPlanCode())) {
                final double price = BigDecimal.valueOf(pricePlanModel.getPrice()).setScale(3).doubleValue();
                entry.setBasePrice(price);
                entry.setTotalPrice(price);
            }
        }
        entry.setCalculated(true);
        cart.setCalculated(true);
        getModelService().saveAll();
        getModelService().refresh(entry.getOrder());

    }

    private void calculateDiscount(final AbstractOrderEntryModel entryModel) {

        final AbstractOrderModel cart = entryModel.getOrder();
        final Collection < PriceRowModel > priceRowModelCollection = entryModel.getProduct().getOwnEurope1Prices();
        for (final PriceRowModel pricePlanModel: priceRowModelCollection) {
            if (pricePlanModel instanceof SubscriptionPricePlanModel) {
                final Collection < RecurringChargeEntryModel > recurringChargeEntries = ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
                for (final RecurringChargeEntryModel recurringChargeEntry: recurringChargeEntries) {
                    final TmaProductOfferingModel product = (TmaProductOfferingModel) recurringChargeEntry.getSubscriptionPricePlanRecurring().getProduct();
                    final Double totalPrice = recurringChargeEntry.getPrice().doubleValue() * entryModel.getQuantity();
                    final Map < String, String > sessionPlan = sessionService.getAttribute(PRICE_PLAN);
                    if (StringUtils.isEmpty(cart.getPlanType()) && Objects.nonNull(sessionPlan) && Objects.nonNull(sessionPlan.get(PLAN_TYPE))) {
                        cart.setPlanType(sessionPlan.get(PLAN_TYPE));
                    }
                    for (final DiscountRowModel discount: product.getEurope1Discounts()) {
                        if (StringUtils.isNotEmpty(cart.getPlanType()) && cart.getPlanType().equalsIgnoreCase(discount.getPlanType().getCode())) {
                            Double priceData;
                            if (discount.getAbsolute()) {
                           	 final double discountedValue = BigDecimal.valueOf(recurringChargeEntry.getPrice().doubleValue() - discount.getValue()).setScale(3).doubleValue();
                                priceData = BigDecimal.valueOf(discount.getValue() * entryModel.getQuantity()).setScale(3).doubleValue();
                                entryModel.setMonthlyPrice(priceData);
                                entryModel.setTotalPrice(priceData);
                                entryModel.setDiscountedValue(discountedValue);

                            } else {
                                priceData = BigDecimal.valueOf(totalPrice * (discount.getValue() / 100)).setScale(3).doubleValue();
                                final double price = BigDecimal.valueOf((totalPrice - priceData) * entryModel.getQuantity()).setScale(3).doubleValue();
                                entryModel.setDiscountedValue(priceData);
                                entryModel.setDiscountPercentage(discount.getValue().toString());
                                entryModel.setMonthlyPrice(price);
                                entryModel.setTotalPrice(price);

                            }
                            entryModel.setDiscountPeriod(discount.getDiscountPeriod());
                            entryModel.setBasePrice(BigDecimal.valueOf(recurringChargeEntry.getPrice()).setScale(3).doubleValue());
                        }

                    }
                }
            }
        }

        entryModel.setCalculated(true);
        getModelService().save(entryModel);
        getModelService().save(entryModel.getOrder());
    }

    @Override
    public void calculateTotals(final AbstractOrderEntryModel entry, final boolean recalculate) {
        super.calculateTotals(entry, recalculate);
        if (llaCommonUtil.isSiteJamaica() || (llaCommonUtil.isSiteVTR() && validCategory(entry)) || (llaCommonUtil.isSitePanama())) {
            if (recalculate || orderRequiresCalculationStrategy.requiresCalculation(entry)) {
                final AbstractOrderModel order = entry.getOrder();
                final CurrencyModel curr = order.getCurrency();
                final int digits = curr.getDigits().intValue();
                final double totalPriceWithoutDiscount = commonI18NService
                        .roundCurrency(entry.getBasePrice().doubleValue() * entry.getQuantity().longValue(), digits);
                final double quantity = entry.getQuantity().doubleValue();
                /*
                 * apply discounts (will be rounded each) convert absolute discount values in case their currency doesn't match the
                 * order currency
                 */
                //Commented below lines to avoid the discount calculation for jamaica as discount is specific to recurring charges
        /*final List appliedDiscounts = DiscountValue.apply(quantity, totalPriceWithoutDiscount, digits,
                convertDiscountValues(order, entry.getDiscountValues()),
                curr.getIsocode());
        entry.setDiscountValues(appliedDiscounts);*/
                final double totalPrice = totalPriceWithoutDiscount;
        /*for (final Iterator it = appliedDiscounts.iterator(); it.hasNext(); )
        {
            totalPrice -= ((DiscountValue) it.next()).getAppliedValue();
        }*/
                // set total price
                entry.setTotalPrice(Double.valueOf(totalPrice));
                // apply tax values too
                calculateTotalTaxValues(entry);
                setCalculatedStatus(entry);

                if (hasJaloStrategies()) {
                    getModelService().save(entry);
                }
            }

            boolean requiresDiscountRecalculation = false;
            if (recalculate || Boolean.FALSE.equals(entry.getCalculated())) {
                requiresDiscountRecalculation = true;
            }
            if (!requiresDiscountRecalculation) {
                return;
            }

            if (CollectionUtils.isEmpty(entry.getDiscountValues())) {
                return;
            }

            final TmaAbstractOrderCompositePriceModel entryPrice = (TmaAbstractOrderCompositePriceModel) entry.getPrice();
            if (entryPrice == null || CollectionUtils.isEmpty(entryPrice.getChildPrices())) {
                return;
            }

            final Set < TmaAbstractOrderPriceModel > orderEntryChildPrices = new HashSet < > ();

            orderEntryChildPrices.addAll(entryPrice.getChildPrices().stream()
                    .filter(price -> ((price instanceof TmaAbstractOrderCompositePriceModel) || (
                            price instanceof TmaAbstractOrderChargePriceModel && !((TmaAbstractOrderChargePriceModel) price)
                                    .getPriceType().equals(TmaAbstractOrderPriceType.DISCOUNT)))).collect(Collectors.toList()));

            //Commented below lines to avoid the discount calculation
      /*entry.getDiscountValues().forEach(discountValue -> {
          TmaAbstractOrderChargePriceModel promotionCartPrice = createCartPriceForDiscountValue(entry, discountValue);
          orderEntryChildPrices.add(promotionCartPrice);
      });
      */
            if (entryPrice.getChildPrices().size() >= orderEntryChildPrices.size()) {
                return;
            }

            entryPrice.setChildPrices(orderEntryChildPrices);
            getModelService().save(entryPrice);
        }
    }

    private boolean validCategory(final AbstractOrderEntryModel entries) {
        return llaCommonUtil.containsSecondLevelCategoryWithCode(entries.getProduct(),PLANS_CATEGORY);
    }

    protected void calculateTotals(final AbstractOrderModel order, final boolean recalculate,
                                   final Map < TaxValue, Map < Set < TaxValue > , Double >> taxValueMap) throws CalculationException {
        super.calculateTotals(order, recalculate, taxValueMap);
        calculateTotals(order, recalculate);
        if (llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite())) {
            calculateCartTotalAndEntryRecurringPrices(order);
        }
    }

    /**
     * Calculate Cart Total and Entry Recurring Price
     * @param order
     */
    private void calculateCartTotalAndEntryRecurringPrices(final AbstractOrderModel order) {
        double cartTotalRecurringPrice = 0.0;
        for (final AbstractOrderEntryModel e: order.getEntries()) {
            cartTotalRecurringPrice += e.getEntryRecurringPrice();
        }
        order.setTotalRecurringPrice(cartTotalRecurringPrice != 0.0 ? cartTotalRecurringPrice : order.getTotalRecurringPrice());
        getModelService().save(order);
        getModelService().refresh(order);
    }

    public void calculateTotals(final AbstractOrderModel order, final boolean recalculate) throws CalculationException {
        if (llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite())) {
                double subtotal = 0.0;
                double addontotal = 0.0;
                double channelsAddontatal = 0.0;
                double smartProtectPrice = 0.0;
                double cartTotalRecurringPrice = 0.0;
                for (final AbstractOrderEntryModel e: order.getEntries()) {
                    final CategoryModel cat = e.getProduct().getSupercategories().iterator().next();
                    if (cat.getCode().equalsIgnoreCase("tv") || cat.getCode().equalsIgnoreCase("internet") || cat.getCode().equalsIgnoreCase("phone")) {

                            subtotal = getBundlePackagePrice(order, e);
                            order.setBundlePrice(subtotal);
                    } else if ((cat.getCode().equalsIgnoreCase("tvaddon") || cat.getCode().equalsIgnoreCase("wifiaddon")) && !(e.getProduct().getCode().equalsIgnoreCase("DVR_BOX"))) {
                        addontotal += getSubscriptonPriceforProduct(e.getProduct(), e.getQuantity().intValue());
                    } else if (cat.getCode().equalsIgnoreCase("channeladdon")) {
                        channelsAddontatal += getSubscriptonPriceforProduct(e.getProduct(), e.getQuantity().intValue());
                    } else if (cat.getCode().equalsIgnoreCase("smartproduct")) {
                        smartProtectPrice += getSubscriptonPriceforProduct(e.getProduct(), e.getQuantity().intValue());
                    }
                    cartTotalRecurringPrice += e.getEntryRecurringPrice() * e.getQuantity();
                    getModelService().save(e);
                }
                order.setSmartProtectPrice(smartProtectPrice);
                order.setAddonProductsTotalPrice(addontotal);
                order.setChannelsAddonTotalPrice(channelsAddontatal);
                order.setTotalRecurringPrice(cartTotalRecurringPrice);
                order.setTotalPrice((order.getBundlePrice() + order.getSmartProtectPrice() + order.getAddonProductsTotalPrice() + order.getChannelsAddonTotalPrice()));
                getModelService().save(order);
        }
        if (llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())) {
           double price = 0.0;
           double deviceprice = 0.0;
           calculateMonthlyPrice(order);
           for (final AbstractOrderEntryModel e: order.getEntries()) {
               final CategoryModel cat = e.getProduct().getSupercategories().iterator().next();
               if ("devices".equalsIgnoreCase(cat.getCode())) {
                   deviceprice = e.getTotalPrice();
               } else {
                   if (Objects.nonNull(e.getMonthlyPrice())) {
                       price += e.getMonthlyPrice();
                   }

               }
           }
           order.setTotalPrice(deviceprice != 0.0 ? deviceprice : price);
           getModelService().save(order);
       }

        if(llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite())){
      	  double totalprice = 0.0;
      	  double monthlyPrice = 0.0;
      	  double tax13Price = 0.0;
      	  double tax911 = 0.0;
           for (final AbstractOrderEntryModel e : order.getEntries()) {
               final CategoryModel cat = e.getProduct().getSupercategories().iterator().next();
               if (cat.getCode().equalsIgnoreCase(TRIPLEPLAY) || cat.getCode().equalsIgnoreCase(DOBLEPLAY) || cat.getCode().equalsIgnoreCase(INTERNET)) {
               	e.setBasePrice(getSubscriptonPriceforCableticaProduct(e.getProduct()));
   					e.setTotalPrice(getSubscriptonPriceforCableticaProduct(e.getProduct())*e.getQuantity());
   					e.setTax13Price(getSubscriptionTax13PriceforProduct(e.getProduct())*e.getQuantity());
   					e.setTax911Price(getSubscriptionTax911PriceforProduct(e.getProduct())*e.getQuantity());
   					e.setMonthlyPrice(getSubscriptionforMonthlyProduct(e.getProduct()));
   					totalprice += e.getTotalPrice();
   					monthlyPrice += e.getMonthlyPrice()+e.getTax13Price()+e.getTax911Price();
   					tax13Price += e.getTax13Price();
   					tax911 += e.getTax911Price();
               }else if(cat.getCode().equalsIgnoreCase(ADDONS)){
               	 e.setBasePrice(getSubscriptonPriceforCableticaProduct(e.getProduct()));
    					e.setTotalPrice(getSubscriptonPriceforCableticaProduct(e.getProduct())*e.getQuantity());
    					e.setTax13Price(getSubscriptionTax13PriceforProduct(e.getProduct())*e.getQuantity());
    					e.setTax911Price(getSubscriptionTax911PriceforProduct(e.getProduct())*e.getQuantity());
    					e.setMonthlyPrice(getSubscriptionforMonthlyProduct(e.getProduct()));
    					totalprice += e.getTotalPrice();
    					monthlyPrice += e.getMonthlyPrice()+e.getTax13Price()+e.getTax911Price();
    					tax13Price += e.getTax13Price();
    					tax911 += e.getTax911Price();
               }
               else if (cat.getCode().equalsIgnoreCase(TELEVISIONPLANCATEGORY)) {
               	e.setBasePrice(getSubscriptonTvPriceforCableticaProduct(e.getProduct()));
   					e.setTotalPrice(getSubscriptonTvPriceforCableticaProduct(e.getProduct())*e.getQuantity());
   					e.setTax13Price(getSubscriptionTvTax13PriceforProduct(e.getProduct())*e.getQuantity());
   					e.setTax911Price(getSubscriptionTvTax911PriceforProduct(e.getProduct())*e.getQuantity());

   					e.setMonthlyPrice(getSubscriptionTVMonthlyProduct(e.getProduct()));
   					totalprice += e.getTotalPrice();
   					monthlyPrice += e.getMonthlyPrice()+e.getTax13Price()+e.getTax911Price();
   					tax13Price += e.getTax13Price();
   					tax911 += e.getTax911Price();
               }
               getModelService().save(e);
           }
           order.setCalculated(Boolean.TRUE);
           order.setTotalPrice(totalprice);
           order.setOrderMonthlyPrice(totalprice);
           order.setTax13Price(tax13Price);
           order.setTax911Price(tax911);
           getModelService().save(order);
       }

        if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())){
            double orderMonthlyPrice = 0.0;
            double discountedValue=0.0;
            //calculateMonthlyPrice(order);
            for (final AbstractOrderEntryModel e : order.getEntries()) {
                if(null != e.getMonthlyPrice()) {
                    orderMonthlyPrice += e.getMonthlyPrice();
                }
                if(null != e.getDiscountedValue()) {
                       discountedValue += e.getDiscountedValue();
                   }
                if(Objects.nonNull(e.getDiscountPeriodEnum())){
                    order.setDiscountPeriod(e.getDiscountPeriodEnum());
                }
                if(Objects.nonNull(e.getDiscountNumber())){
                    order.setDiscountNumber(e.getDiscountNumber());
                }
            }
            order.setOrderMonthlyPrice(orderMonthlyPrice);
            order.setTotalDiscounts(discountedValue);
            getModelService().save(order);
        }

    }
    private Double getBundlePackagePrice(final AbstractOrderModel source, final AbstractOrderEntryModel e) {
        if(!CollectionUtils.isEmpty(source.getBundleProductMapping()))
		{
			return source.getBundleProductMapping().get(0).getBundlePrice();
		}
		else
		{
			return getBpoGroupPrice(source);
		}
    }
    /**
	 * @param product
	 * @return
	 */
	private Double getSubscriptionTVMonthlyProduct(final ProductModel product)
	{
		double subtotal = 0.0;
		final Collection<PriceRowModel> priceRowModelCollection=product.getOwnEurope1Prices();
		final CategoryModel category = commerceCategoryService.getCategoryForCode(TELEVISIONPLANCATEGORY);
		  for(final PriceRowModel pricePlanModel:priceRowModelCollection){
			if(pricePlanModel instanceof SubscriptionPricePlanModel){
		    Collection<RecurringChargeEntryModel> recurringChargeEntries = new ArrayList<RecurringChargeEntryModel>();
   		  if(llaCommonUtil.isSiteCabletica() && product.getSupercategories().contains(category)) {
   			final String llaregion = sessionService.getAttribute("llaregion");
   			final RegionModel regionModel = tmaRegionService.findRegionByIsocode(llaregion);
   			if(null != regionModel && ((SubscriptionPricePlanModel) pricePlanModel).getRegions().contains(regionModel)) {
      			recurringChargeEntries =  ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
      			for (final RecurringChargeEntryModel recurringChargeEntry : recurringChargeEntries)
					{
   						subtotal += recurringChargeEntry.getPrice().doubleValue();
   					}
					}
   			}
   		 }
      }
	return subtotal;
   }



	/**
	 * @param product
	 * @return
	 */
	private Double getSubscriptionforMonthlyProduct(final ProductModel product)
	{
		double subtotal = 0.0;
	   final Collection<PriceRowModel> priceRowModelCollection=product.getOwnEurope1Prices();
	   for(final PriceRowModel pricePlanModel:priceRowModelCollection){
	       if(pricePlanModel instanceof SubscriptionPricePlanModel){
	           final Collection<RecurringChargeEntryModel> recurringChargeEntries= ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
	           for(final RecurringChargeEntryModel recurringChargeEntry:recurringChargeEntries){
						{
							subtotal += recurringChargeEntry.getPrice().doubleValue();
						}
	           }
	       }
	   }
	   return subtotal;
	   }

	/**
	 * @param product
	 * @return
	 */
	private Double getSubscriptionTvTax911PriceforProduct(final ProductModel product)
	{
		double subtotal = 0.0;
		final Collection<PriceRowModel> priceRowModelCollection=product.getOwnEurope1Prices();
		final CategoryModel category = commerceCategoryService.getCategoryForCode(TELEVISIONPLANCATEGORY);
		  for(final PriceRowModel pricePlanModel:priceRowModelCollection){
			if(pricePlanModel instanceof SubscriptionPricePlanModel){
		    Collection<RecurringChargeEntryModel> recurringChargeEntries = new ArrayList<RecurringChargeEntryModel>();
   		  if(llaCommonUtil.isSiteCabletica() && product.getSupercategories().contains(category)) {
   			final String llaregion = sessionService.getAttribute("llaregion");
   			final RegionModel regionModel = tmaRegionService.findRegionByIsocode(llaregion);
   			if(null != regionModel && ((SubscriptionPricePlanModel) pricePlanModel).getRegions().contains(regionModel)) {
      			recurringChargeEntries =  ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
      			for (final RecurringChargeEntryModel recurringChargeEntry : recurringChargeEntries)
					{
      				final Double tax911 = recurringChargeEntry.getTax911();
   					if (null != tax911)
   					{
   						subtotal += recurringChargeEntry.getTax911().doubleValue();
   				    }
					}
   			}
   		 }
      }
   }

   return subtotal;
 }

	/**
	 * @param product
	 * @return
	 */
	private Double getSubscriptionTvTax13PriceforProduct(final ProductModel product)
	{
		double subtotal = 0.0;
		final Collection<PriceRowModel> priceRowModelCollection=product.getOwnEurope1Prices();
		final CategoryModel category = commerceCategoryService.getCategoryForCode(TELEVISIONPLANCATEGORY);
		  for(final PriceRowModel pricePlanModel:priceRowModelCollection){
			if(pricePlanModel instanceof SubscriptionPricePlanModel){
		    Collection<RecurringChargeEntryModel> recurringChargeEntries = new ArrayList<RecurringChargeEntryModel>();
   		  if(llaCommonUtil.isSiteCabletica() && product.getSupercategories().contains(category)) {
   			final String llaregion = sessionService.getAttribute("llaregion");
   			final RegionModel regionModel = tmaRegionService.findRegionByIsocode(llaregion);
   			if(null != regionModel && ((SubscriptionPricePlanModel) pricePlanModel).getRegions().contains(regionModel)) {
      			recurringChargeEntries =  ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
      			for (final RecurringChargeEntryModel recurringChargeEntry : recurringChargeEntries)
					{
   					final Double tax13 = recurringChargeEntry.getTax13Percent();
   					if (null != tax13)
   					{
   						subtotal += recurringChargeEntry.getTax13Percent().doubleValue();
   					}
					}
   			}
   		 }
      }
   }

   return subtotal;
 }

	/**
	 * @param product
	 * @return
	 */
	private Double getSubscriptionTax911PriceforProduct(final ProductModel product)
	{
		double subtotal = 0.0;
	   final Collection<PriceRowModel> priceRowModelCollection=product.getOwnEurope1Prices();
	   for(final PriceRowModel pricePlanModel:priceRowModelCollection){
	       if(pricePlanModel instanceof SubscriptionPricePlanModel){
	           final Collection<RecurringChargeEntryModel> recurringChargeEntries= ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
	           for(final RecurringChargeEntryModel recurringChargeEntry:recurringChargeEntries){
	         	  final Double tax911 = recurringChargeEntry.getTax911();
						if (null != tax911)
						{
							subtotal += recurringChargeEntry.getTax911().doubleValue();
						}
	           }
	       }
	   }
	   return subtotal;
	  }

	/**
	 * @param product
	 * @return
	 */
	private Double getSubscriptionTax13PriceforProduct(final ProductModel product)
	{
		double subtotal = 0.0;
	   final Collection<PriceRowModel> priceRowModelCollection=product.getOwnEurope1Prices();
	   for(final PriceRowModel pricePlanModel:priceRowModelCollection){
	       if(pricePlanModel instanceof SubscriptionPricePlanModel){
	           final Collection<RecurringChargeEntryModel> recurringChargeEntries= ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
	           for(final RecurringChargeEntryModel recurringChargeEntry:recurringChargeEntries){
						final Double tax13 = recurringChargeEntry.getTax13Percent();
						if (null != tax13)
						{
							subtotal += recurringChargeEntry.getTax13Percent().doubleValue();
						}
	           }
	       }
	   }
	   return subtotal;
	  }

	/**
	 * @param product
	 * @return
	 */
	private Double getSubscriptonTvPriceforCableticaProduct(final ProductModel product)
	{
		double subtotal = 0.0;
		final Collection<PriceRowModel> priceRowModelCollection=product.getOwnEurope1Prices();
		final CategoryModel category = commerceCategoryService.getCategoryForCode(TELEVISIONPLANCATEGORY);
		  for(final PriceRowModel pricePlanModel:priceRowModelCollection){
			if(pricePlanModel instanceof SubscriptionPricePlanModel){
		    Collection<RecurringChargeEntryModel> recurringChargeEntries = new ArrayList<RecurringChargeEntryModel>();
   		  if(llaCommonUtil.isSiteCabletica() && product.getSupercategories().contains(category)) {
   			final String llaregion = sessionService.getAttribute("llaregion");
   			final RegionModel regionModel = tmaRegionService.findRegionByIsocode(llaregion);
   			if(null != regionModel && ((SubscriptionPricePlanModel) pricePlanModel).getRegions().contains(regionModel)) {
      			recurringChargeEntries =  ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
      			for (final RecurringChargeEntryModel recurringChargeEntry : recurringChargeEntries)
					{
      				final Double tax911 = recurringChargeEntry.getTax911();
   					final Double tax13 = recurringChargeEntry.getTax13Percent();
   					if (null != tax911 && null != tax13)
   					{
   						subtotal += recurringChargeEntry.getPrice().doubleValue() + recurringChargeEntry.getTax911().doubleValue()
   								+ recurringChargeEntry.getTax13Percent().doubleValue();
   					}
					}
   			}
   		 }
      }
   }

   return subtotal;
 }

	/**
	 * @param product
	 * @return
	 */
	private Double getSubscriptonPriceforCableticaProduct(final ProductModel product)
	{
	double subtotal = 0.0;
	double tax13Price = 0.0;
	double tax911Price = 0.0;
   final Collection<PriceRowModel> priceRowModelCollection=product.getOwnEurope1Prices();
   for(final PriceRowModel pricePlanModel:priceRowModelCollection){
       if(pricePlanModel instanceof SubscriptionPricePlanModel){
           final Collection<RecurringChargeEntryModel> recurringChargeEntries= ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
           for(final RecurringChargeEntryModel recurringChargeEntry:recurringChargeEntries){
         		if(null != recurringChargeEntry.getTax911())
					{
						tax911Price = recurringChargeEntry.getTax911().doubleValue();
					}
					if(null != recurringChargeEntry.getTax13Percent())
					{
						tax13Price = recurringChargeEntry.getTax13Percent().doubleValue();
					}

				    subtotal += recurringChargeEntry.getPrice().doubleValue() + tax911Price + tax13Price;
           }
       }
   }
   return subtotal;
   }

	private Double getBpoGroupPrice(final AbstractOrderModel source)
	{
		if (!CollectionUtils.isEmpty(source.getEntryGroups()))
		{
			return getSubscriptonPriceforProduct(
					productService.getProductForCode(source.getEntryGroups().get(0).getExternalReferenceId()), 1);
		}
		return 0.0;
	}

    private Double getSubscriptonPriceforProduct(final ProductModel product, final int qty) {
        double subtotal = 0.0;
        final Collection < PriceRowModel > priceRowModelCollection = product.getOwnEurope1Prices();
        for (final PriceRowModel pricePlanModel: priceRowModelCollection) {
            if (pricePlanModel instanceof SubscriptionPricePlanModel) {
                final Collection < RecurringChargeEntryModel > recurringChargeEntries = ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
                for (final RecurringChargeEntryModel recurringChargeEntry: recurringChargeEntries) {
                    if (qty == 1 && null == recurringChargeEntry.getQty()) {
                        subtotal += recurringChargeEntry.getPrice().doubleValue() * qty;
                        break;
                    } else if (null != recurringChargeEntry.getQty() && recurringChargeEntry.getQty().equals(qty)) {
                        subtotal += recurringChargeEntry.getPrice().doubleValue() * qty;
                        break;
                    }
                }
            }
        }
        return subtotal;
    }

}
