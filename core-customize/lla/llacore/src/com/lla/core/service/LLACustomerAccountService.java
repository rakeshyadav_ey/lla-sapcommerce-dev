/**
 *
 */
package com.lla.core.service;

import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Collection;


/**
 * @author GZ132VA
 *
 */
public interface LLACustomerAccountService extends CustomerAccountService
{

	/**
	 * @param user
	 * @return
	 */
	Collection<? extends AddressModel> getAddressBookInstallationEntries(CustomerModel user);

}
