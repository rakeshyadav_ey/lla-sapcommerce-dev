/**
 *
 */
package com.lla.core.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.List;


/**
 * @author GZ132VA
 *
 */
public interface LLAInstallationAddressService
{
	public List<AddressModel> getSupportedInstallationAddressesForOrder(final AbstractOrderModel abstractOrder,
			final boolean visibleAddressesOnly);
}
