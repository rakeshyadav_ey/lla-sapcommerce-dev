package com.lla.core.service.creditscore;

import com.lla.core.enums.ContractType;
import com.lla.core.model.CreditScoreModel;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.response.LLAApiResponse;
import de.hybris.platform.order.exceptions.CalculationException;

public interface LLACreditScoreService {

    LLAApiResponse checkCreditScore() throws  LLAApiException;
    CreditScoreModel getCreditScore(int creditScore, ContractType contractStatus,String pack);
}
