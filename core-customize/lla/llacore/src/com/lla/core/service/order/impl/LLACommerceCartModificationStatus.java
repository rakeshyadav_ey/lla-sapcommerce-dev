/**
 * 
 */
package com.lla.core.service.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;

/**
 * @author Rohit25.Gupta
 *
 */
public enum LLACommerceCartModificationStatus implements CommerceCartModificationStatus
{
    /**
     * Indicates a problem between cart entries compatibility policies.
     */
    DEVICE_COMPATIBILITY_ERROR("deviceCompatibilityError");

    private String code;

    private LLACommerceCartModificationStatus(final String code)
    {
        this.code = code;
    }

    @SuppressWarnings("javadoc")
    public String getCode()
    {
        return code;
    }
}
