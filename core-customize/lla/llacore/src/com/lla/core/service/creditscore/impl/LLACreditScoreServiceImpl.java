package com.lla.core.service.creditscore.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Objects;
import com.lla.core.constants.LlaCoreConstants;
import com.lla.core.dao.LLACreditScoreDao;
import com.lla.core.enums.BundleTypeEnum;
import com.lla.core.enums.ContractType;
import com.lla.core.model.CreditScoreModel;
import com.lla.core.service.creditscore.LLACreditScoreService;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.response.LLAApiResponse;
import com.lla.mulesoft.integration.service.LLAMulesoftCreditCheckService;


public class LLACreditScoreServiceImpl implements LLACreditScoreService
{

	private static final Logger LOG = LoggerFactory.getLogger(LLACreditScoreServiceImpl.class);
	protected static final String SSN_ENCRYPTED_VALUE = "ssn_encrypted";
	@Resource(name = "llaMulesoftCreditCheckService")
	private LLAMulesoftCreditCheckService llaMulesoftCreditCheckService;

	@Resource(name = "llaCreditScoreDao")
	private LLACreditScoreDao llaCreditScoreDao;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "calculationService")
	private CalculationService calculationService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Autowired
	private ConfigurationService configurationService;
	@Autowired
	private SessionService sessionService;
	@Override
	public LLAApiResponse checkCreditScore() throws  LLAApiException
	{

		final CartModel cart = cartService.getSessionCart();
		LLAApiResponse creditCheckResponse = null;
		String pack = StringUtils.EMPTY;
		if (cart != null)
		{
			creditCheckResponse = llaMulesoftCreditCheckService.customerCreditCheck((AbstractOrderModel) cart,sessionService.getAttribute(SSN_ENCRYPTED_VALUE));
		}
		if(null!=creditCheckResponse && (StringUtils.isNotEmpty(creditCheckResponse.getErrorCode()) || StringUtils.isNotEmpty(creditCheckResponse.getDescription()) || StringUtils.isNotEmpty(creditCheckResponse.getMessage()))){
			LOG.info(String.format("Error in retrieving Credit Check Response for customer :::%s having cart code :::%s for Market ::%s",cart.getUser().getUid(),cart.getCode(),cart.getSite().getUid()));
			return  creditCheckResponse;
		}else if (null != creditCheckResponse && 0 < (creditCheckResponse.getCreditScore()))
		{

			final ContractType contractType = cart.getContractType();
			if (CollectionUtils.isNotEmpty(cart.getBundleProductMapping()))
			{
				pack = cart.getBundleProductMapping().get(0).getBundleType().toString();
			}
			final CreditScoreModel creditScoreModel = getCreditScore(creditCheckResponse.getCreditScore(), contractType, pack);
			if (creditScoreModel != null)
			{
				if (StringUtils.isNotBlank(creditScoreModel.getLetterMapping()))
				{
					final String letterMapping = creditScoreModel.getLetterMapping();
					if (letterMapping.equals(LlaCoreConstants.CREDIT_SCORE_M) || letterMapping.equals(LlaCoreConstants.CREDIT_SCORE_L))
						updateCreditScoreDetailsIntoCart(creditScoreModel);
				}
				updateCartAmountAfterCreditScoreEvaluation(cart, creditScoreModel);
				modelService.save(creditScoreModel);
				modelService.refresh(creditScoreModel);
				cart.setCreditScoreLevel(creditScoreModel);
				modelService.save(cart);
			}
		}else{
			LOG.info(String.format("Exception in retrieving Credit Check Response for customer :::%s having cart code :::%s for Market ::%s",cart.getUser().getUid(),cart.getCode(),cart.getSite().getUid()));
		    throw  new LLAApiException(String.format("Exception in retrieving Credit Check Response for customer ::%s",cart.getUser().getUid()));
		}
		return creditCheckResponse;
	}

	private void updateCartAmountAfterCreditScoreEvaluation(CartModel cart, CreditScoreModel creditScoreModel) {
		if(Objects.nonNull(creditScoreModel.getInstallationFee()) && Objects.nonNull(creditScoreModel.getDvrDeposit())) {
			cart.setAmountDueToday(creditScoreModel.getInstallationFee() + creditScoreModel.getDvrDeposit());
		}
		if(Objects.nonNull(creditScoreModel.getOnetimeCharges())) {
			cart.setAmountDueFirstBill(creditScoreModel.getOnetimeCharges());
		}
		if(Objects.nonNull(creditScoreModel.getPaperBilling())){
			cart.setAmountDueFirstBill((Objects.nonNull(cart.getAmountDueFirstBill())?cart.getAmountDueFirstBill():Double.valueOf(0.0)) + creditScoreModel.getPaperBilling());
		}
		if (null != creditScoreModel.getUpfrontPayment() && creditScoreModel.getUpfrontPayment().equalsIgnoreCase(
				configurationService.getConfiguration().getString(LlaCoreConstants.CREDIT_SCORE_MONTHLY_PAYMENT)))
		{
			cart.setAmountDueToday(cart.getTotalPrice() + cart.getAmountDueToday());
			if(null!=cart.getAmountDueToday() && cart.getAmountDueToday()>Double.valueOf(0.0))
			{
				cart.setPackAmountDueToday(calculateTotals(cart));
			}


		}
		else if (null != creditScoreModel.getMonthlyPayment() && creditScoreModel.getMonthlyPayment().equalsIgnoreCase(
				configurationService.getConfiguration().getString(LlaCoreConstants.CREDIT_SCORE_MONTHLY_PAYMENT)))
		{
			cart.setAmountDueFirstBill(cart.getTotalPrice() + cart.getAmountDueFirstBill());
			if(null!=cart.getAmountDueFirstBill() && cart.getAmountDueFirstBill()>Double.valueOf(0.0))
			{
				cart.setPackAmountDueFirstBill(calculateTotals(cart));
			}
		}



	}

	private Double calculateTotals(CartModel cart)
	{
		BigDecimal bundleMonthlyPrice= BigDecimal.valueOf(null!=cart.getBundlePrice()?cart.getBundlePrice():0.0);
		BigDecimal smartProtectMonthlyPrice= BigDecimal.valueOf(null!=cart.getSmartProtectPrice()?cart.getSmartProtectPrice():0.0);
		BigDecimal channelAddonOnMonthlyPrice= BigDecimal.valueOf(null!=cart.getChannelsAddonTotalPrice()?cart.getChannelsAddonTotalPrice():0.0);
		BigDecimal addonOnMonthlyPrice= BigDecimal.valueOf(null!=cart.getAddonProductsTotalPrice()?cart.getAddonProductsTotalPrice():0.0);

		BigDecimal totalMonthlyPrice=bundleMonthlyPrice.add(smartProtectMonthlyPrice).add(channelAddonOnMonthlyPrice).add(addonOnMonthlyPrice);
		return Double.valueOf(totalMonthlyPrice.doubleValue());
	}

	private void updateCreditScoreDetailsIntoCart(CreditScoreModel creditScoreModel) {
		if (creditScoreModel.getContractStatus() == (ContractType.WITHCONTRACT))
		{
			if ((BundleTypeEnum.ONEP.toString()).equals(creditScoreModel.getPack())
					|| (BundleTypeEnum.TWOP.toString()).equals(creditScoreModel.getPack()))
			{
				creditScoreModel.setDvrDeposit(100.0);
				modelService.save(creditScoreModel);
			}
		}
		else if ((ContractType.WITHOUTCONTRACT) == creditScoreModel.getContractStatus())
		{
			if ((BundleTypeEnum.ONEP.toString()).equals(creditScoreModel.getPack())
					|| (BundleTypeEnum.TWOP.toString()).equals(creditScoreModel.getPack())
					|| (BundleTypeEnum.THREEP.toString()).equals(creditScoreModel.getPack()))
			{
				creditScoreModel.setDvrDeposit(100.0);
				modelService.save(creditScoreModel);
			}

		}
	}

	@Override
	public CreditScoreModel getCreditScore(final int creditScore, final ContractType contractStatus, final String pack)
	{
		return llaCreditScoreDao.fetchCreditScore(creditScore, contractStatus, pack);
	}

}
