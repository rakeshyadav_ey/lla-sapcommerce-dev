package com.lla.core.service.product.impl;

import com.lla.core.service.product.LLATmaPoVariantService;
import de.hybris.platform.b2ctelcoservices.model.TmaPoVariantModel;
import de.hybris.platform.b2ctelcoservices.services.impl.DefaultTmaPoVariantService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.springframework.beans.factory.annotation.Autowired;

public class LLATmaPoVariantServiceImpl  extends DefaultTmaPoVariantService implements LLATmaPoVariantService{
    @Autowired
    private FlexibleSearchService flexibleSearchService;
    @Override
    public TmaPoVariantModel getTmaPoVariant(String code) {
        TmaPoVariantModel sampleModel= new TmaPoVariantModel();
        sampleModel.setCode(code);
        TmaPoVariantModel retrievedData=flexibleSearchService.getModelsByExample(sampleModel).get(0);
        return retrievedData;
    }
}
