package com.lla.core.service.product;

import de.hybris.platform.b2ctelcoservices.model.TmaPoVariantModel;
import de.hybris.platform.b2ctelcoservices.services.TmaPoVariantService;

public interface LLATmaPoVariantService extends TmaPoVariantService {

   TmaPoVariantModel getTmaPoVariant(final String code);
}
