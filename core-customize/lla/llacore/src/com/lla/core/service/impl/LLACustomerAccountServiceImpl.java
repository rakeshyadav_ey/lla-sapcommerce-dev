/**
 *
 */
package com.lla.core.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import com.lla.core.constants.LlaCoreConstants;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerAccountService;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import com.lla.core.dao.LLACustomerAccountDao;
import com.lla.core.service.LLACustomerAccountService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author GZ132VA
 *
 */
public class LLACustomerAccountServiceImpl extends DefaultCustomerAccountService implements LLACustomerAccountService
{

	@Resource(name = "customerAccountDao")
	private LLACustomerAccountDao llaCustomerAccountDao;

	@Autowired
	private CMSSiteService cmsSiteService;

	@Override
	public Collection<? extends AddressModel> getAddressBookInstallationEntries(final CustomerModel user)
	{
		validateParameterNotNull(user, "Customer model cannot be null");
		return llaCustomerAccountDao.findAddressBookInstallationEntriesForCustomer(user,
				getCommerceCommonI18NService().getAllCountries());
	}

	@Override
	public void setDefaultAddressEntry(final CustomerModel customerModel, final AddressModel addressModel)
	{
		super.setDefaultAddressEntry(customerModel,addressModel);
	    if(cmsSiteService.getCurrentSite().getUid().equals("jamaica")){
			if (customerModel.getAddresses().contains(addressModel))
			{
			   customerModel.setDefaultInstallationAddress(addressModel);
			}
			else
			{
				final AddressModel clone = getModelService().clone(addressModel);
				clone.setOwner(customerModel);
				getModelService().save(clone);
				final List<AddressModel> customerAddresses = new ArrayList<AddressModel>();
				customerAddresses.addAll(customerModel.getAddresses());
				customerAddresses.add(clone);
				customerModel.setAddresses(customerAddresses);
				customerModel.setDefaultInstallationAddress(clone);
			}
			getModelService().save(customerModel);
			getModelService().refresh(customerModel);
	    }
	}

}
