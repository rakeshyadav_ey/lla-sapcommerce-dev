package com.lla.core.service.c2c.impl;

import com.lla.core.constants.LlaCoreConstants;
import com.lla.core.enums.ClickToCallStatus;
import com.lla.core.service.c2c.ClickToCallService;
import com.lla.core.service.order.LLACommerceCheckoutService;
import com.lla.core.service.product.LLATmaPoVariantService;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import de.hybris.platform.acceleratorcms.model.components.ClickToCallModel;
import de.hybris.platform.b2ctelcoservices.model.TmaPoVariantModel;
import de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.i18n.LanguageResolver;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import com.lla.mulesoft.integration.exception.LLAApiException;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;


public class ClickToCallServiceImpl implements ClickToCallService {
    private static final Logger LOG = Logger.getLogger(ClickToCallServiceImpl.class);
    @Autowired
    private FlexibleSearchService flexibleSearchService;
    @Autowired
    private LLACommerceCheckoutService llaCommerceCheckoutService;
    @Autowired
    private LLAMulesoftNotificationService llaMulesoftNotificationService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ModelService modelService;
    @Autowired
    private BaseSiteService baseSiteService;
    @Autowired
    private PersistentKeyGenerator c2cRequestNumberGenerator;
    @Autowired
    private LLATmaPoVariantService llaTmaPoVariantService;
    @Autowired
    private CatalogVersionService catalogVersionService;
    @Autowired
    private ConfigurationService configurationService;
    @Autowired
    private LanguageResolver languageResolver;

    @Override
    public Boolean  getGenesysClickToCalls(final BaseSiteModel siteUid) {
       ClickToCallModel clickToCall_Error= new ClickToCallModel();
       clickToCall_Error.setStatus(ClickToCallStatus.ERROR);
       clickToCall_Error.setBaseSite(siteUid);
       List<ClickToCallModel> clickToCallErrorList=flexibleSearchService.getModelsByExample(clickToCall_Error);
       if(CollectionUtils.isNotEmpty(clickToCallErrorList)) {
           for (ClickToCallModel item : clickToCallErrorList) {
               String colorCode=StringUtils.EMPTY;
               String capacityCode=StringUtils.EMPTY;
               if(StringUtils.isNotEmpty(item.getDeviceProduct().getCode())){
                   TmaPoVariantModel poVariant= llaTmaPoVariantService.getTmaPoVariant(item.getDeviceProduct().getCode());
                   final List<CategoryModel> categoryList= (List<CategoryModel>) poVariant.getSupercategories();
                   for(CategoryModel category:categoryList){
                       if(category.getSupercategories().get(0).getCode().equals("color")){
                           colorCode=category.getName();
                       }
                       if(category.getSupercategories().get(0).getCode().equals("capacity")){
                           capacityCode=category.getName();
                       }
                   }
               }

               try {
                   triggerC2CEmailToAgent_Customer(item.getRequestNo(),item,item.getCustomerPreferredLanguage().getIsocode(),siteUid.getUid(),item.getPlanProduct().getCode(),item.getNumberOfLines(),item.getDeviceProduct().getCode(),
                           item.getLastName(),item.getFirstName(),item.getPhoneNumber(),item.getEmail(),colorCode,capacityCode);
               } catch (LLAApiException exception) {
                   LOG.info(String.format("Error in Sending out  Email to Agent & Customer %s ",item.getEmail()));
                   LOG.error(exception);
               }
               LOG.info(String.format("Click to Call Record Id %s updated with Status ", item.getUid(), item.getStatus().getCode()));
               modelService.save(item);
               modelService.refresh(item);
           }
           return Boolean.TRUE;
       }else {
           LOG.info("No records found with status as Error");
           return Boolean.FALSE;
       }
   }

    @Override
    public Boolean sendOutCommunicationToAgent(BaseSiteModel baseSite) {
        ClickToCallModel clickToCall_Error= new ClickToCallModel();
        clickToCall_Error.setC2cAgentNotificationSent(Boolean.FALSE);
        clickToCall_Error.setBaseSite(baseSite);
        List<ClickToCallModel> clickToCallErrorList=flexibleSearchService.getModelsByExample(clickToCall_Error);
        if(CollectionUtils.isNotEmpty(clickToCallErrorList)) {
            for (ClickToCallModel item : clickToCallErrorList) {
                AbstractOrderModel order = item.getOrder();
                  String notificationType= StringUtils.EMPTY;
                  String reasonCode=item.getReason().getCode();
                  notificationType = getKPINotificationType(notificationType, reasonCode);
                  triggerKPINotification(item, order, notificationType);
            }
            return Boolean.TRUE;
           }else {
            LOG.info("No records found with status as Error");
            return Boolean.FALSE;
        }
    }

    @Override
   public ClickToCallModel triggerC2CEmailToAgent_Customer(String requestNum,ClickToCallModel item,final String lang, final String baseSite, final String planCode, final Integer numberOfLines, final String deviceCode,
                                                           final String customerLastName, final String customerName, final String phoneNumber, final String emailAddress, String colorCode, String capacityCode) throws LLAApiException {

        final String requestNumber=StringUtils.isNotEmpty(requestNum)?requestNum:String.valueOf(c2cRequestNumberGenerator.generate());
        String customerResponse=StringUtils.EMPTY;
        String agentResponse=StringUtils.EMPTY;
        ClickToCallModel clickToCallModel=item;
        try{
            agentResponse = llaMulesoftNotificationService.triggerC2C_AgentNotification("es",baseSite,requestNumber,customerName,planCode,numberOfLines,deviceCode,customerLastName,phoneNumber,emailAddress,colorCode,capacityCode);
            LOG.info(String.format("Agent Email Triggered for Customer %s",emailAddress));
        }catch (LLAApiException apiException){
            LOG.info(String.format("Error in Sending out Agent Email for Customer %s -- Email Not Triggered for NEW C2C Agent",emailAddress));
            LOG.error("Exception in making call");
        }finally {
            clickToCallModel= persistDataInCommerce(lang,clickToCallModel,requestNumber,baseSite,planCode,numberOfLines,deviceCode,customerLastName,customerName,phoneNumber,emailAddress,agentResponse,customerResponse);
        }

        try{
            customerResponse =llaMulesoftNotificationService.triggerC2C_CustomerNotification(lang,baseSite,requestNumber,customerName,planCode,numberOfLines,deviceCode,customerLastName,phoneNumber,emailAddress,colorCode,capacityCode);
            LOG.info(String.format("Email Triggered for NEW C2C Customer %s",emailAddress));
        }catch (LLAApiException apiException){
            LOG.info(String.format("Error in Sending out Email for Customer  %s -- Email Not Triggered for NEW C2C Customer",emailAddress));
            LOG.error("Exception in making call");
        }finally {
            clickToCallModel= persistDataInCommerce(lang,clickToCallModel,requestNumber,baseSite,planCode,numberOfLines,deviceCode,customerLastName,customerName,phoneNumber,emailAddress,agentResponse,customerResponse);
        }
        return clickToCallModel;
    }

    /**
     * Persist C2C Data to Commerce
     *
     * @param lang
     * @param clickToCallModel
     * @param requestNumber
     * @param baseSite
     * @param planCode
     * @param numberOfLines
     * @param deviceCode
     * @param customerLastName
     * @param customerName
     * @param phoneNumber
     * @param emailAddress
     * @param agentResponseStatus
     * @param customerResponseStatus
     * @return
     */
    private ClickToCallModel persistDataInCommerce(String lang, ClickToCallModel clickToCallModel, final String requestNumber, final String baseSite, final String planCode, final Integer numberOfLines, final String deviceCode,
                                                   final String customerLastName, final String customerName, final String phoneNumber, final String emailAddress, final String agentResponseStatus, final String customerResponseStatus) {

        ClickToCallModel c2cData=null;
        if(null!=clickToCallModel){
             c2cData=clickToCallModel;
        }else{
              c2cData=modelService.create(ClickToCallModel.class);
              c2cData= setC2CRequestData(lang,requestNumber, baseSite, planCode, numberOfLines, deviceCode, customerLastName, customerName, phoneNumber, emailAddress, c2cData);
        }
        c2cData.setC2cAgentNotificationSent(StringUtils.isNotEmpty(agentResponseStatus)?Boolean.TRUE:Boolean.FALSE);
        c2cData.setC2cCustomerNotificationSent(StringUtils.isNotEmpty(customerResponseStatus)?Boolean.TRUE:Boolean.FALSE);

        if(c2cData.getC2cAgentNotificationSent() && c2cData.getC2cCustomerNotificationSent()){
            c2cData.setStatus(ClickToCallStatus.SUCCESS);
        }else{
            c2cData.setStatus(ClickToCallStatus.ERROR);
        }
        modelService.save(c2cData);
        modelService.refresh(c2cData);
        return c2cData;

    }

    /***
     * Set C2C Request Data
     *
     * @param lang
     * @param requestNumber
     * @param baseSite
     * @param planCode
     * @param numberOfLines
     * @param deviceCode
     * @param customerLastName
     * @param customerName
     * @param phoneNumber
     * @param emailAddress
     * @param c2cData
     * @return
     */
    private ClickToCallModel setC2CRequestData(String lang, String requestNumber, String baseSite, String planCode, Integer numberOfLines, String deviceCode, String customerLastName, String customerName, String phoneNumber, String emailAddress, ClickToCallModel c2cData) {
        c2cData.setRequestNo(requestNumber);
        c2cData.setBaseSite(baseSiteService.getBaseSiteForUID(baseSite));
        c2cData.setPhoneNumber(phoneNumber);
        c2cData.setFirstName(customerName);
        c2cData.setLastName(customerLastName);
        c2cData.setEmail(emailAddress);
        c2cData.setPlanProduct((TmaSimpleProductOfferingModel)productService.getProductForCode(catalogVersionService.getSessionCatalogVersionForCatalog(baseSite+"ProductCatalog"),planCode));
        c2cData.setDeviceProduct(StringUtils.isNotEmpty(deviceCode)?(TmaSimpleProductOfferingModel)productService.getProductForCode(catalogVersionService.getSessionCatalogVersionForCatalog(baseSite+"ProductCatalog"),deviceCode):null);
        c2cData.setNumberOfLines(numberOfLines);
        c2cData.setCustomerPreferredLanguage(languageResolver.getLanguage(lang));
        UUID uuid = UUID.randomUUID();
        c2cData.setUid(uuid.toString());
        c2cData.setProduct(planCode);
        return c2cData;
    }
    /**
     * Trigger KPI Notifications
     * @param item
     * @param order
     * @param notificationType
     */
    private void triggerKPINotification(ClickToCallModel item, AbstractOrderModel order, String notificationType) {
        try {
            String response=llaMulesoftNotificationService.generateKPINotifications(order, notificationType, item.getReason());
            if(StringUtils.isNotEmpty(response)){
                item.setC2cAgentNotificationSent(Boolean.TRUE);
                LOG.info(String.format("Click to Call Record Id %s updated with Status ", item.getUid(), item.getStatus().getCode()));
                modelService.save(item);
                modelService.refresh(item);
            }
        } catch (LLAApiException exception) {
            LOG.error(String.format("Error in Sending Out Communication to Agent for Cart ::: %s due to error ::: %s", item.getOrder().getCode(),exception.getMessage()));
        }
    }

    /**
     * Get KPI Notification Type
     * @param notificationType
     * @param reasonCode
     * @return
     */
    private String getKPINotificationType(String notificationType, String reasonCode) {
        switch (reasonCode){
            case "DEBT": notificationType =configurationService.getConfiguration().getString(new StringBuffer("llamulesoftintegration.kpi.notification").append(reasonCode.toLowerCase()).toString());
                          break;
            case "CURRENT_CUSTOMER": notificationType =configurationService.getConfiguration().getString(new StringBuffer("llamulesoftintegration.kpi.notification").append(reasonCode.toLowerCase()).toString());
                break;
            case "NOT_SERVICEABLE": notificationType =configurationService.getConfiguration().getString(new StringBuffer("llamulesoftintegration.kpi.notification").append(reasonCode.toLowerCase()).toString());
                break;
        }
        return notificationType;
    }


}
