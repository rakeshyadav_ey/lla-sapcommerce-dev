package com.lla.core.service;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceUpdateCartEntryStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;

public interface LLACommerceUpdateCartEntryStrategy extends CommerceUpdateCartEntryStrategy {

    CommerceCartModification updateQuantityForCartEntry(final CommerceCartParameter parameters)
            throws CommerceCartModificationException;
}
