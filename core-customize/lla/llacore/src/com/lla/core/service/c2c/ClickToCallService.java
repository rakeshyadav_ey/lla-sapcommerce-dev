package com.lla.core.service.c2c;

import com.lla.mulesoft.integration.exception.LLAApiException;
import de.hybris.platform.acceleratorcms.model.components.ClickToCallModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;

public interface ClickToCallService {

   /**
    * Get all Failed Click To Call
    * @param baseSite
    * @return
    */
   Boolean  getGenesysClickToCalls(BaseSiteModel baseSite);

    /**
     *  Trigger C2C email to Agent And Customer
     * @param lang
     * @param baseSite
     * @param planCode
     * @param numberOfLines
     * @param deviceCode
     * @param customerLastName
     * @param customerName
     * @param phoneNumber
     * @param emailAddress
     * @param colorCode
     * @param capacityCode
     * @throws LLAApiException
     * @return
     */
    ClickToCallModel triggerC2CEmailToAgent_Customer(final String requestNum,ClickToCallModel c2cItem,final String lang, final String baseSite, final String planCode, final Integer numberOfLines, final String deviceCode,
                                                     final String customerLastName, final String customerName, final String phoneNumber, final String emailAddress, String colorCode, String capacityCode) throws LLAApiException;

    /**
     * Re-Trigger C2C Failed Notifications
     * @param baseSite
     * @return
     */
    Boolean sendOutCommunicationToAgent(BaseSiteModel baseSite);

}
