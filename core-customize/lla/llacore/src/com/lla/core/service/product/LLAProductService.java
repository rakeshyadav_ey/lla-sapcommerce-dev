package com.lla.core.service.product;

import com.lla.core.model.LLABundleProductMappingModel;
import com.lla.core.model.LLAConfiguratorMessageModel;
import de.hybris.platform.product.ProductService;

import java.util.List;

public interface LLAProductService  extends ProductService {
    /**
     *  Retrieving Commerce Product Code Using BSS Product Code
     * @param bssProductCode
     *  @param sourceEPC
     * @return
     */
  String  fetchCommerceProductCodeUsingBSSCode(final String bssProductCode,final String sourceEPC);

  List<LLABundleProductMappingModel> getAllBundleMapping();

    LLAConfiguratorMessageModel getBundleMessageForPackage(LLABundleProductMappingModel llaBundleProductMappingModel);
}
