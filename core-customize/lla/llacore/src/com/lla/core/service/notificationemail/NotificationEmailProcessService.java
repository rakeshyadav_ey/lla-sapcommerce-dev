package com.lla.core.service.notificationemail;

public interface NotificationEmailProcessService {
    /**
     *
     * Fetch Failed Notification email Process
     * @param siteId
     * @param filesDaysOld
     * @return
     */
    int  fetchFailedNotificationProcess(final String siteId, int filesDaysOld);

    /**
     * Fetch Failed Fallout Process
     * @param siteId
     * @param numHours
     * @return
     */
        int  fetchFailedFalloutProcess(final String siteId, int numHours) ;
}
