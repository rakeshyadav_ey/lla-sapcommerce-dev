package com.lla.core.service.helper;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.helper.ProductAndCategoryHelper;

public class LLAProductAndCategoryHelper extends ProductAndCategoryHelper {

    @Override
    public boolean isValidProductCategory(final CategoryModel category)
    {
        if (category == null)
        {
            return false;
        }
        for (final Class filteredCategory : getProductCategoryBlacklist())
        {
            if (filteredCategory.isInstance(category) || category.getCode().startsWith("brand_"))
            {
                return false;
            }
        }
        return true;
    }
}
