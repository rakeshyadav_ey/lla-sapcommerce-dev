package com.lla.core.service.customer;

import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;

import com.lla.mulesoft.integration.dto.ViewPlanResponse;


public interface LLACustomerService
{
	/**
	 * Creating Subscription Plan Retrieved from Mulesoft for Customer
	 *
	 * @param viewPlanResponseList
	 * @param customerId
	 */
	void createCustomerSubscriptionPlans(List<ViewPlanResponse> viewPlanResponseList, final String customerId);

	/**
	 * @param customer
	 * @param accountId
	 * @param systemId
	 * @return
	 */
	String updateCustomerBillingAccount(CustomerModel customer, String accountId, String systemId);

	/**
	 * @param customer
	 * @param accountId
	 * @param systemId
	 * @return
	 */
	String updateCustomerServiceAccount(CustomerModel customer, String accountId, String systemId);

	/**
	 * @param correlationId
	 */
	CustomerModel getCutsomerForCorrelationID(String correlationId);

	/**
	 *
	 */
	void resetDetailsForAnonymousCart();
}
