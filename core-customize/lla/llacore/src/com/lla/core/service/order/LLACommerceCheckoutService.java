/**
 *
 */
package com.lla.core.service.order;

import com.lla.core.enums.ClickToCallReasonEnum;
import com.lla.mulesoft.integration.dto.ClickCallServicablity;
import com.lla.mulesoft.integration.exception.LLAApiException;
import de.hybris.platform.acceleratorcms.model.components.ClickToCallModel;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Collection;


/**
 * @author GZ132VA
 *
 */
public interface LLACommerceCheckoutService extends CommerceCheckoutService
{
    /**
     * Set Installtion Address
     * @param parameter
     * @return
     */
    boolean setInstallationAddress(final CommerceCheckoutParameter parameter);

    /**
     * Execute C2C Request as Part of Customer Journey
     * @param abstractOrderModel
     * @return
     */
    ClickToCallModel executeCustomerCallBackRequest(final AbstractOrderModel abstractOrderModel, final ClickToCallReasonEnum clickToCallReasonEnum) throws LLAApiException;

    /**
     * Execute Failed C2C Request from CronJob
     * @param abstractOrderModel
     * @param c2cItem
     * @return
     */
    ClickToCallModel executeC2CRequest(final AbstractOrderModel abstractOrderModel, final ClickToCallModel c2cItem);

    /**
     *
     * @param abstractOrderModel
     * @param c2cItem
     * @param callReason
     * @return
     */
    ClickToCallModel executeC2CRequest(final AbstractOrderModel abstractOrderModel, final ClickToCallModel c2cItem, final ClickToCallReasonEnum callReason);
    /**
     * Execute C2C Request For Anonymous
     * @param cartModel
     * @param clickCallServicablity
     * @return
     * @throws LLAApiException
     */
    ClickToCallModel executeAnonymousCallBackRequest(AbstractOrderModel cartModel, ClickCallServicablity clickCallServicablity) throws LLAApiException;


    /**
     * Execute C2C Request For Anonymous
     * @param abstractOrderModel
     * @param clickCallServicablity
     * @param clickToCallReasonEnum
     * @return
     * @throws LLAApiException
     */
    ClickToCallModel executeCustomerCallBackRequest(final AbstractOrderModel abstractOrderModel, final ClickCallServicablity clickCallServicablity,final ClickToCallReasonEnum clickToCallReasonEnum) throws LLAApiException;
    }
