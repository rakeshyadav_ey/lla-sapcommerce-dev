/**
 *
 */
package com.lla.core.service.impl;

import de.hybris.platform.b2ctelcoservices.model.TmaBundledProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.pricing.context.TmaPriceContext;
import de.hybris.platform.b2ctelcoservices.pricing.context.impl.TmaPriceContextBuilder;
import de.hybris.platform.b2ctelcoservices.pricing.services.impl.DefaultTmaCommercePriceService;
import de.hybris.platform.b2ctelcoservices.services.TmaPoService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.subscriptionservices.model.SubscriptionPricePlanModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.lla.core.enums.DevicePriceType;
import com.lla.core.util.LLACommonUtil;


/**
 * @author GF986ZE
 *
 */
public class DefaultLLACommercePriceService extends DefaultTmaCommercePriceService
{

	@Autowired
	private LLACommonUtil llaCommonUtil;

	@Resource(name = "cartService")
	private CartService cartService;

	@Autowired
	private SessionService sessionService;

	@Autowired
	private TmaPoService tmaPoService;

	private static final String COMPRA_CATEGORY ="compra";
	private static final String ARRIENDO_CATEGORY ="arriendo";

	@Override
	public PriceRowModel getMinimumPrice(final TmaPriceContext priceContext)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("priceContext cannot be null", priceContext);

		final Set<PriceRowModel> applicablePrices = new HashSet<>();
		if (productIsBpo(priceContext.getProduct()))
		{
			applicablePrices.addAll(getPriceOverrides(priceContext));
			// create a new TmaPriceContext to retrieve also standalone prices
			final TmaPriceContext standalonePriceContext = TmaPriceContextBuilder.newTmaPriceContextBuilder()
					.withProduct(getProduct(priceContext)).withProcessTypes(priceContext.getProcessTypes())
					.withSubscriptionTerms(priceContext.getSubscriptionTerms()).withRegions(priceContext.getRegions()).build();
			applicablePrices.addAll(getStandalonePrices(standalonePriceContext));
		}
		else
		{
			applicablePrices.addAll(getStandalonePrices(priceContext));
		}
		if (!CollectionUtils.isEmpty(applicablePrices) && llaCommonUtil.isSitePanama() && llaCommonUtil.containsFirstLevelCategoryWithCode(priceContext.getProduct(), "devices"))
		{
			String planCode = StringUtils.EMPTY;
				if (priceContext.getEntry() != null && priceContext.getEntry().getMasterEntry() != null)
				{

					planCode = priceContext.getEntry().getMasterEntry().getProduct().getCode();
				}
				else
				{
				planCode = getLastProductCodeAddedToCart(priceContext.getProduct().getCode());
				}

				for (final PriceRowModel priceRow : applicablePrices)
				{
				if (StringUtils.isNotEmpty(priceRow.getPlanCode()) && priceRow.getPlanCode().equalsIgnoreCase(planCode))
				{
					return priceRow;
				}

			}
			return applicablePrices.stream().filter(priceRow -> priceRow.getPriceType()!=null &&  priceRow.getPriceType().getCode()!=null)
					.filter(priceRow -> priceRow.getPriceType().getCode().equalsIgnoreCase(DevicePriceType.NOOFFER.getCode()))
					.collect(Collectors.toList()).get(0);
			}
		if (llaCommonUtil.isSiteJamaica() && null != priceContext.getEntry())
		{

			final TmaProductOfferingModel productModel = tmaPoService.getPoForCode(priceContext.getEntry().getProduct().getCode());
			if (llaCommonUtil.containsFirstLevelCategoryWithCode(productModel, "3Pbundles")
					|| llaCommonUtil.containsFirstLevelCategoryWithCode(productModel, "postpaid"))
			{

				if (BooleanUtils.isNotTrue(priceContext.getEntry().getOrder().getIsCustomerExisting()))
				{
					for (final PriceRowModel priceRow : applicablePrices)
					{
						if (priceRow instanceof SubscriptionPricePlanModel
								&& CollectionUtils.isNotEmpty(((SubscriptionPricePlanModel) priceRow).getProcessTypes())
								&& ((SubscriptionPricePlanModel) priceRow).getProcessTypes().iterator().next().getCode()
										.equalsIgnoreCase("ACQUISITION"))
						{
							return priceRow;
						}
					}
				}
				else
				{
					return getPriceForExistingCustomer(applicablePrices);
				}

			}
		}
		if(llaCommonUtil.isSiteVTR())
		{
			CategoryModel category = priceContext.getProduct().getSupercategories().iterator().next();
			if (("devices").equalsIgnoreCase(category.getCode()))
			{
				CartModel cart = cartService.getSessionCart();
				if(Objects.nonNull(cart) && StringUtils.isNotEmpty(cart.getDevicePlanCode()) && StringUtils.isNotEmpty(cart.getDevicePriceType()))
				{
					Collection<PriceRowModel> priceRowModelCollection = priceContext.getProduct().getEurope1Prices();
					for (PriceRowModel pricePlanModel : priceRowModelCollection)
					{

						if (COMPRA_CATEGORY.equalsIgnoreCase(cart.getDevicePriceType()))
						{
							return pricePlanModel;
						}
						if (ARRIENDO_CATEGORY.equalsIgnoreCase(cart.getDevicePriceType()) &&
								cart.getDevicePlanCode().equalsIgnoreCase(pricePlanModel.getPlanCode()))
						{
							return pricePlanModel;
						}
					}
				}

				}
		}
		return getMinimumPriceRowFromCollection(applicablePrices);
	}
	
	/**
	 * @param applicablePrices
	 * @return
	 */
	private PriceRowModel getPriceForExistingCustomer(final Set<PriceRowModel> applicablePrices)
	{
		for (final PriceRowModel priceRow : applicablePrices)
		{
			if (priceRow instanceof SubscriptionPricePlanModel
					&& CollectionUtils.isNotEmpty(((SubscriptionPricePlanModel) priceRow).getProcessTypes())
					&& ((SubscriptionPricePlanModel) priceRow).getProcessTypes().iterator().next().getCode()
					.equalsIgnoreCase("UPSELL"))
			{
				return priceRow;
			}
		}
		return applicablePrices.iterator().next();
	}



	private boolean productIsBpo(final ProductModel poModel)
	{
		return poModel instanceof TmaBundledProductOfferingModel;
	}

	private PriceRowModel getMinimumPriceRowFromCollection(final Set<PriceRowModel> prices)
	{
		return CollectionUtils.isNotEmpty(prices) ? Collections.min(prices, getPriceRowComparator()) : null;
	}

	private String getLastProductCodeAddedToCart(final String productCode)
	{
		final CartModel cartModel = cartService.getSessionCart();
		if (null != cartModel)
		{
			final List<AbstractOrderEntryModel> entries = new ArrayList<AbstractOrderEntryModel>(cartModel.getEntries());
			Collections.reverse(entries);
			for (final AbstractOrderEntryModel entry : entries)
			{
				if (entry.getProduct().getCode().equalsIgnoreCase(productCode) && null != entry.getMasterEntry())
				{
				   return entry.getMasterEntry().getProduct().getCode();

				   }
		  }
		  }

		return StringUtils.EMPTY;
	}

	@Override
	public PriceRowModel getMinimumPrice(final AbstractOrderEntryModel orderEntryModel)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("orderEntry cannot be null", orderEntryModel);

		final TmaPriceContext priceContext = createPriceContext(orderEntryModel);
		priceContext.setEntry(orderEntryModel);
		return getMinimumPrice(priceContext);
	}
}
