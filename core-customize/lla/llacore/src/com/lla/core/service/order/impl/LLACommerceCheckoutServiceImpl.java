package com.lla.core.service.order.impl;

import com.lla.core.enums.ClickToCallReasonEnum;
import com.lla.core.enums.ClickToCallStatus;
import com.lla.core.service.LLAInstallationAddressService;
import com.lla.core.service.order.LLACommerceCheckoutService;
import com.lla.mulesoft.integration.constants.LlamulesoftintegrationConstants;
import com.lla.mulesoft.integration.dto.ClickCallServicablity;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.service.LLAMulesoftCallBackRequestService;

import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;

import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.text.SimpleDateFormat;

import javax.annotation.Resource;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorcms.model.components.ClickToCallModel;
import de.hybris.platform.category.model.CategoryModel;

public class LLACommerceCheckoutServiceImpl extends DefaultCommerceCheckoutService implements LLACommerceCheckoutService {
    private static final Logger LOG = Logger.getLogger(LLACommerceCheckoutServiceImpl.class);
    @Autowired
   private LLAInstallationAddressService llaInstallationAddressService;
   @Resource(name="llaMulesoftCallBackRequestService")
	private LLAMulesoftCallBackRequestService llaMulesoftCallBackRequestService;
   @Resource(name = "commerceCategoryService")
   private CommerceCategoryService commerceCategoryService;
   @Autowired
   private ConfigurationService configurationService;
    @Autowired
    private BaseSiteService baseSiteService;
    @Autowired
    private UserService userService;
    @Autowired
    private CommonI18NService commonI18NService;
    @Autowired
    private ProductService productService;
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

    @Autowired
    private EnumerationService enumerationService;
    @Autowired
    private CatalogVersionService catalogVersionService;
    @Override
    public boolean setInstallationAddress(CommerceCheckoutParameter parameter) {
        final CartModel cartModel = parameter.getCart();
        final AddressModel addressModel = parameter.getAddress();
        final boolean flagAsInstallationAddress = parameter.isIsInstallationAddress();

        validateParameterNotNull(cartModel, "Cart model cannot be null");
        getModelService().refresh(cartModel);

        final UserModel user = cartModel.getUser();
        getModelService().refresh(user);

        cartModel.setInstallationAddress(addressModel);

        // Check that the address model belongs to the same user as the cart
        if (isValidInstallationAddress(cartModel, addressModel))
        {
            getModelService().save(cartModel);

            if (addressModel != null && flagAsInstallationAddress && !Boolean.TRUE.equals(addressModel.getInstallationAddress()))
            {
                // Flag the address as a delivery address
                addressModel.setInstallationAddress(Boolean.TRUE);
                getModelService().save(addressModel);
            }
            final CommerceCartParameter calculateCartParameter = new CommerceCartParameter();
            calculateCartParameter.setEnableHooks(true);
            calculateCartParameter.setCart(cartModel);
            getCommerceCartCalculationStrategy().calculateCart(calculateCartParameter);
            getModelService().refresh(cartModel);

            return true;
        }

        return false;
    }

    protected boolean isValidInstallationAddress(final CartModel cartModel, final AddressModel addressModel)
    {
        if (addressModel != null)
        {
            final List<AddressModel> supportedAddresses = llaInstallationAddressService.getSupportedInstallationAddressesForOrder(cartModel,
                    false);
            return supportedAddresses != null && supportedAddresses.contains(addressModel);
        }
        else
        {
            return true;
        }
    }
    
    @Override
 	public ClickToCallModel executeCustomerCallBackRequest(final AbstractOrderModel abstractOrderModel, final ClickToCallReasonEnum clickToCallReasonEnum) throws LLAApiException
 	{
        return serviceCallToGenesys(abstractOrderModel, null, null,clickToCallReasonEnum);

    }

    @Override
    public ClickToCallModel executeCustomerCallBackRequest(final AbstractOrderModel abstractOrderModel, final ClickCallServicablity clickCallServicablity,final ClickToCallReasonEnum clickToCallReasonEnum) throws LLAApiException
    {
        return serviceCallToGenesys(abstractOrderModel, null, clickCallServicablity,clickToCallReasonEnum);
    }
    private ClickToCallModel serviceCallToGenesys(AbstractOrderModel abstractOrderModel, ClickToCallModel c2cItem, ClickCallServicablity clickCallServicablity, ClickToCallReasonEnum clickToCallReasonEnum) {
        boolean isCallRequestPlaced = false;
        ClickToCallModel clickToCallModel=null;
        try {

            String response = llaMulesoftCallBackRequestService.createCallBackRequest(abstractOrderModel,clickCallServicablity,clickToCallReasonEnum);
            if (!StringUtils.isEmpty(response)) {
                isCallRequestPlaced = true;
            }
        } catch (Exception e) {
            LOG.error("Exception in making call");
        } finally {
            clickToCallModel= saveCallRequestsInCustomerDetails(abstractOrderModel, isCallRequestPlaced, c2cItem,clickCallServicablity,clickToCallReasonEnum);
        }
        return clickToCallModel;
    }

    @Override
    public ClickToCallModel executeAnonymousCallBackRequest(final AbstractOrderModel abstractOrderModel, final ClickCallServicablity clickCallServicablity) throws LLAApiException
    {
        return serviceCallToGenesys(abstractOrderModel, null,clickCallServicablity,ClickToCallReasonEnum.LEAD);

    }

     @Override
    public ClickToCallModel executeC2CRequest(final AbstractOrderModel abstractOrderModel, final ClickToCallModel c2cItem)
    {
        return serviceCallToGenesys(abstractOrderModel, c2cItem, null, null);

    }

    @Override
    public ClickToCallModel executeC2CRequest(final AbstractOrderModel abstractOrderModel, final ClickToCallModel c2cItem, final ClickToCallReasonEnum callReason)
    {
        return serviceCallToGenesys(abstractOrderModel, c2cItem, null, callReason);

    }

   private ClickToCallModel saveCallRequestsInCustomerDetails(final AbstractOrderModel abstractOrderModel, boolean isCallRequestPlaced, ClickToCallModel c2cItem,ClickCallServicablity clickCallServicablity, ClickToCallReasonEnum clickToCallReasonEnum) {
        if (null != c2cItem && isCallRequestPlaced) {
            c2cItem.setStatus(ClickToCallStatus.SUCCESS);
            c2cItem.setGenesysNotificationSent(Boolean.TRUE);
            getModelService().save(c2cItem);
            getModelService().refresh(c2cItem);
            return c2cItem;
        } else if(null!=c2cItem && !isCallRequestPlaced){
            c2cItem.setStatus(ClickToCallStatus.ERROR);
            c2cItem.setGenesysNotificationSent(Boolean.FALSE);
            getModelService().save(c2cItem);
            getModelService().refresh(c2cItem);
            return c2cItem;
        }else{
            final CustomerModel customer = (CustomerModel) abstractOrderModel.getUser();
           // final CategoryModel category = commerceCategoryService.getCategoryForCode("channelsaddoncabletica");
            final List<ClickToCallModel> calls = new ArrayList<>();
            final ClickToCallModel call = generateClickToCallData(abstractOrderModel, isCallRequestPlaced, clickCallServicablity, clickToCallReasonEnum, customer);
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();
            call.setRequestedDate(date);
            call.setBaseSite(abstractOrderModel.getSite());
            call.setGenesysNotificationSent(isCallRequestPlaced);
            getModelService().save(call);
            calls.add(call);
            final List<ClickToCallModel> previousCalls=customer.getClickToCallRequest();
            final List<ClickToCallModel> updatedCalls=new ArrayList<>(previousCalls);
            updatedCalls.add(previousCalls.size(),call);
            customer.setClickToCallRequest(updatedCalls);
            getModelService().save(customer);
            return call;
        }
    }

    /**
     * Generate Click TO Call For Persistence
     * @param abstractOrderModel
     * @param isCallRequestPlaced
     * @param clickCallServicablity
     * @param clickToCallReasonEnum
     * @param customer
     * @return
     */
    private ClickToCallModel generateClickToCallData(AbstractOrderModel abstractOrderModel, boolean isCallRequestPlaced, ClickCallServicablity clickCallServicablity, ClickToCallReasonEnum clickToCallReasonEnum, CustomerModel customer) {
        final ClickToCallModel call = getModelService().create(ClickToCallModel.class);
        final CategoryModel category = commerceCategoryService.getCategoryForCode(LlamulesoftintegrationConstants.ADDONS);
        UUID uuid = UUID.randomUUID();
        call.setUid(uuid.toString());
        call.setFirstName(StringUtils.isNotEmpty(customer.getFirstName())? customer.getFirstName().trim():StringUtils.EMPTY);
        call.setLastName(StringUtils.isNotEmpty(customer.getLastName())? customer.getLastName().trim():StringUtils.EMPTY);
        userService.setCurrentUser(abstractOrderModel.getUser());
        catalogVersionService.setSessionCatalogVersion(abstractOrderModel.getSite().getUid()+"ProductCatalog","Online");
        baseSiteService.setCurrentBaseSite(abstractOrderModel.getSite(),Boolean.TRUE);
        if(null!= clickCallServicablity){

            TmaSimpleProductOfferingModel poSpo=(TmaSimpleProductOfferingModel)productService.getProductForCode(catalogVersionService.getSessionCatalogVersionForCatalog(abstractOrderModel.getSite().getUid()+"ProductCatalog"),clickCallServicablity.getProduct());
            call.setPhoneNumber(clickCallServicablity.getPhoneNumber());
            call.setIdentityNumber(clickCallServicablity.getIdentityNumber());
            call.setEmail(clickCallServicablity.getEmail());
            call.setProduct(new StringBuffer(poSpo.getName(getCurrentLocale(abstractOrderModel))).append("_").append(enumerationService.getEnumerationName(clickToCallReasonEnum, getCurrentLocale(abstractOrderModel))).toString());
        }else{
            call.setEmail(StringUtils.substringAfter(customer.getUid(), "|"));
            call.setPhoneNumber(customer.getMobilePhone());
            call.setIdentityNumber(customer.getDocumentNumber());
            for (final AbstractOrderEntryModel entry : abstractOrderModel.getEntries())
            {
                TmaSimpleProductOfferingModel poSpo=(TmaSimpleProductOfferingModel)productService.getProductForCode(catalogVersionService.getSessionCatalogVersionForCatalog(abstractOrderModel.getSite().getUid()+"ProductCatalog"),entry.getProduct().getCode());
                if (!poSpo.getSupercategories().contains(category))
                {
                    if (abstractOrderModel instanceof CartModel)
                    {
                       if(null != clickToCallReasonEnum)
                          //  call.setProduct(new StringBuffer(entry.getProduct().getName(new Locale("es"))).append("_").append(clickToCallReasonEnum.toString()).toString());
                            call.setProduct(new StringBuffer(entry.getProduct().getName(getCurrentLocale(abstractOrderModel))).append("_").append(
                                enumerationService.getEnumerationName(clickToCallReasonEnum, getCurrentLocale(abstractOrderModel))).toString());
                        else
                            call.setProduct(new StringBuffer(entry.getProduct().getName(getCurrentLocale(abstractOrderModel))).append("_Lead").toString());
                    }
                    else
                    {
                        call.setProduct(new StringBuffer(entry.getProduct().getName(getCurrentLocale(abstractOrderModel))).append("_").append(entry.getOrder().getCode()).toString());
                    }
                    break;
                }
            }
        }
        call.setOrder(abstractOrderModel);
        call.setIdentityType(customer.getDocumentType());
        call.setStatus(isCallRequestPlaced ? ClickToCallStatus.SUCCESS : ClickToCallStatus.ERROR);
        call.setReason(clickToCallReasonEnum);
        return call;
    }

    /**
     * Get Current locale
     * @return  locale
     */
    private Locale getCurrentLocale(final AbstractOrderModel orderModel){
        BaseSiteModel baseSite =orderModel.getSite();
        Locale locale = commonI18NService.getLocaleForLanguage(baseSite.getDefaultLanguage());
        return locale;
    }
}
