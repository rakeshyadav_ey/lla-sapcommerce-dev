/**
 *
 */
package com.lla.core.service.impl;

import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.lla.core.service.LLACustomerAccountService;
import com.lla.core.service.LLAInstallationAddressService;

/**
 * @author GZ132VA
 *
 */
public class LLAInstallationAddressServiceImpl implements LLAInstallationAddressService
{

	@Resource(name = "customerAccountService")
	private LLACustomerAccountService llaCustomerAccountService;
	@Resource(name = "checkoutCustomerStrategy")
	private CheckoutCustomerStrategy checkoutCustomerStrategy;
	@Override
	public List<AddressModel> getSupportedInstallationAddressesForOrder(final AbstractOrderModel abstractOrder,
			final boolean visibleAddressesOnly)
	{
		final List<AddressModel> addressesForOrder = new ArrayList<AddressModel>();
		if (abstractOrder != null)
		{
			final UserModel user = abstractOrder.getUser();
			if (user instanceof CustomerModel)
			{
				if (visibleAddressesOnly)
				{
					addressesForOrder.addAll(llaCustomerAccountService.getAddressBookInstallationEntries((CustomerModel) user));
				}
				else
				{
					addressesForOrder.addAll(llaCustomerAccountService.getAllAddressEntries((CustomerModel) user));
				}
				// If the user had no addresses, check the order for an address in case it's a guest checkout.
				if (checkoutCustomerStrategy.isAnonymousCheckout() && addressesForOrder.isEmpty()
						&& abstractOrder.getDeliveryAddress() != null)
				{
					addressesForOrder.add(abstractOrder.getDeliveryAddress());
				}
			}
		}
		return addressesForOrder;
	}

}
