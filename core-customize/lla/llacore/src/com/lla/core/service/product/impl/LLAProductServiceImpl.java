package com.lla.core.service.product.impl;

import com.lla.core.dao.LLAProductDao;
import com.lla.core.model.LLABundleProductMappingModel;
import com.lla.core.model.LLAConfiguratorMessageModel;
import com.lla.core.service.product.LLAProductService;
import com.lla.mulesoft.integration.service.impl.LLAMulesoftViewPlanServiceImpl;
import de.hybris.platform.b2ctelcoservices.jalo.TmaSimpleProductOffering;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.impl.DefaultProductService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import java.util.List;

public class LLAProductServiceImpl  extends DefaultProductService implements LLAProductService {

    private static final Logger LOG = Logger.getLogger(LLAProductServiceImpl.class);
    @Resource(name="productDao")
    private LLAProductDao productDao;
    /**
     * Retrieving Commerce Product Code Using BSS Product Code
     *
     * @param bssProductCode
     * @param  sourceEPC
     * @return
     */
    @Override
    public String fetchCommerceProductCodeUsingBSSCode(final String bssProductCode, final String sourceEPC) {
        final TmaProductOfferingModel productModel=productDao.findCommerceProductCodeUsingBSSCode(bssProductCode,sourceEPC);
        if(null!=productModel){
            return productModel.getCode();
        }else{
            throw new ModelNotFoundException("No Product exist in Commerce for BSS Code" + bssProductCode);
        }
    }

    @Override
    public List<LLABundleProductMappingModel> getAllBundleMapping() {
      return productDao.getAllBundleProdutMapping();
    }

    @Override
    public LLAConfiguratorMessageModel getBundleMessageForPackage(LLABundleProductMappingModel llaBundleProductMappingModel) {
        return productDao.getConfiguratorMessageForPackage(llaBundleProductMappingModel.getPackageId());
    }
}
