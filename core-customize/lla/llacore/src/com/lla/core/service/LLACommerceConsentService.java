/**
 *
 */
package com.lla.core.service;

import de.hybris.platform.commerceservices.consent.CommerceConsentService;
import de.hybris.platform.commerceservices.model.consent.ConsentTemplateModel;
import de.hybris.platform.core.model.user.CustomerModel;


/**
 * @author Rohit25.Gupta
 *
 */
public interface LLACommerceConsentService extends CommerceConsentService
{
	void giveConsent(final CustomerModel customer, final ConsentTemplateModel consentTemplate);
}
