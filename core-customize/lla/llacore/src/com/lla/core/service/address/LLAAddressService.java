/**
 *
 */
package com.lla.core.service.address;

import java.util.List;

import com.lla.core.model.*;


/**
 * @author GF986ZE
 *
 */
public interface LLAAddressService
{


	List<LLAPanamaAddressMappingModel> getAddressMappingForGivenText(final String text);

	/**
	 * Return all province values
	 * @return
	 */
	List<ProvinceModel> getAllProvince();

	<T> List<T> getAddressValues(final String prevNode, final String nextNode);

	String getNeighbourHoodByUID(final String uid);

	String getDistrictByUID(final String uid);

	String getProvinceByUID(final String uid);

	String getCantonByUID(final String uid);
}
