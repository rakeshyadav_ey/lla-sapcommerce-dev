/*
 *   Copyright (c) 2019 SAP SE or an SAP affiliate company.  All rights reserved.
 */
package com.lla.core.job;

import com.lla.core.model.AbandonedCartExtractionCronJobModel;
import com.lla.service.LLAGenerateDocumentService;
import com.lla.service.LLAS3StroageUploadService;
import de.hybris.platform.commerceservices.order.dao.CommerceCartDao;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.util.Config;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Required;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * The Abandoned Cart Extraction Job Performable.
 */
public class AbandonedCartExtractionJobPerformable extends AbstractJobPerformable<AbandonedCartExtractionCronJobModel> {

    private static final Logger LOG = Logger.getLogger(AbandonedCartExtractionJobPerformable.class);

    private static final String FILE_NAME = Config.getParameter("lla.s3.csv.file.name");
    private static final String DEFAULT_CART_EXTRACT_AGE =Config.getParameter("lla.abandoned.cart.extract.age");
    private static final String ANONYMOUS = "anonymous";
    private LLAGenerateDocumentService llaGenerateDocumentService;
    private LLAS3StroageUploadService llaS3StroageUploadService;
    private ConfigurationService configurationService;
    private CommerceCartDao commerceCartDao;
    private TimeService timeService;

    /**
     * @param job
     * @return performResult
     **/
    @Override
    public PerformResult perform(final AbandonedCartExtractionCronJobModel job)
    {
        List<CartModel> abandonedCartList = new ArrayList<>();
        try {
            if (StringUtils.isEmpty(String.valueOf(job.getSite()))) {
                LOG.warn("There is no sites defined for " + job.getCode());
                return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
            }
            final int cartExtractAge = job.getCartExtractionAge() != null ? job.getCartExtractionAge().intValue() :
                    Integer.parseInt(DEFAULT_CART_EXTRACT_AGE);
            final String exportFileName = StringUtils.isNotBlank(job.getExportFileName()) ? job.getExportFileName() : FILE_NAME;
            final String MAX_AGE= String.valueOf(TimeUnit.SECONDS.toDays(cartExtractAge));
            if (job.getSite().getUid().equalsIgnoreCase(getConfigurationService().getConfiguration().getString("website.jamaica"))) {
                for (final CartModel oldCart : getCommerceCartDao().getCartsForRemovalForSiteAndUser(
                        new DateTime(getTimeService().getCurrentTime()).minusSeconds(cartExtractAge).toDate(), job.getSite(), null)) {
                    CustomerModel customer = (CustomerModel) oldCart.getUser();
                    if (null != customer && StringUtils.isNotEmpty(customer.getUid())) {
                        if (isNotAnonymous(customer.getUid()) && !(oldCart.getIsCartExported()!= null ? oldCart.getIsCartExported() : Boolean.FALSE)) {
                            abandonedCartList.add(oldCart);
                            oldCart.setIsCartExported(Boolean.TRUE);
                            modelService.save(oldCart);
                            modelService.refresh(oldCart);
                        }
                    }
                }
                if (CollectionUtils.isNotEmpty(abandonedCartList)){
                    exportDocument(abandonedCartList, exportFileName);
                }
                else {
                    LOG.info(String.format("Not found any abandoned cart older than %s  days and Abandoned cart List size ::%s ",MAX_AGE ,abandonedCartList.size()));
                }
            }
            return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
        } catch (final Exception e) {
            LOG.error("Exception occurred during extract abdandoned cart job [" + e.getMessage() + "]");
            return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
        }
    }

    private void exportDocument(List<CartModel> cartList, String fileName) {
        try {
                getLlaGenerateDocumentService().generateDocument(cartList, fileName);
                getLlaS3StroageUploadService().uploadFile();
        } catch (IOException e) {
            LOG.error("Error during exporting document to s3 storage:[" + e.getMessage() + "]");
        }
    }
    /**
     * @param userId
     * @return boolean
     */
    public boolean isNotAnonymous(String userId) {
        return !ANONYMOUS.equalsIgnoreCase(userId);
    }

    public LLAGenerateDocumentService getLlaGenerateDocumentService() {
        return llaGenerateDocumentService;
    }

    @Required
    public void setLlaGenerateDocumentService(LLAGenerateDocumentService llaGenerateDocumentService) {
        this.llaGenerateDocumentService = llaGenerateDocumentService;
    }

    public LLAS3StroageUploadService getLlaS3StroageUploadService() {
        return llaS3StroageUploadService;
    }

    protected CommerceCartDao getCommerceCartDao()
    {
        return commerceCartDao;
    }

    @Required
    public void setCommerceCartDao(final CommerceCartDao commerceCartDao)
    {
        this.commerceCartDao = commerceCartDao;
    }

    protected TimeService getTimeService()
    {
        return timeService;
    }

    @Required
    public void setTimeService(final TimeService timeService)
    {
        this.timeService = timeService;
    }
    @Required
    public void setLlaS3StroageUploadService(LLAS3StroageUploadService llaS3StroageUploadService) {
        this.llaS3StroageUploadService = llaS3StroageUploadService;
    }
    public ConfigurationService getConfigurationService() {
        return configurationService;
    }
    @Required
    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }
}
