/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.core.job;

import com.lla.core.model.FalloutProcessCronJobModel;
import com.lla.core.model.NotificationEmailProcessCronJobModel;
import com.lla.core.service.notificationemail.NotificationEmailProcessService;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class FalloutProcessJobPerformable extends AbstractJobPerformable<FalloutProcessCronJobModel>
{
	private static final Logger LOG = Logger.getLogger(FalloutProcessJobPerformable.class);
	@Autowired
	private NotificationEmailProcessService notificationEmailProcessService;

	@Override
	public PerformResult perform(FalloutProcessCronJobModel falloutProcess) {
		int numHours=falloutProcess.getNumHours();
		int processCount=notificationEmailProcessService.fetchFailedFalloutProcess(falloutProcess.getSite(),  numHours);
		LOG.info(String.format("Restarted Fallout Notification Process for %s ",processCount));
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}
	@Override
	public boolean isAbortable(){
		return  true;
	}

}
