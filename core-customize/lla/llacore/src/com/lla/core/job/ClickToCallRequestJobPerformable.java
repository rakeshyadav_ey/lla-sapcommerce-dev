/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.core.job;

import com.lla.core.constants.LlaCoreConstants;
import com.lla.core.model.ClickToCallRequestCronJobModel;
import com.lla.core.service.c2c.ClickToCallService;
import com.lla.core.util.LLACommonUtil;
import de.hybris.platform.acceleratorcms.model.components.ClickToCallModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import org.apache.log4j.Logger;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;

public class ClickToCallRequestJobPerformable extends AbstractJobPerformable<ClickToCallRequestCronJobModel>
{
	private static final Logger LOG = Logger.getLogger(ClickToCallRequestJobPerformable.class);
	@Autowired
	private ClickToCallService clickToCallService;
	@Autowired
	private BaseSiteService baseSiteService;
	@Autowired
	private UserService userService;
	@Autowired
	private CatalogVersionService catalogVersionService;

	@Override
	public PerformResult perform(ClickToCallRequestCronJobModel clickToCallModel) {
		baseSiteService.setCurrentBaseSite(clickToCallModel.getBaseSite(),Boolean.FALSE);
		userService.setCurrentUser(userService.getAnonymousUser());
		catalogVersionService.setSessionCatalogVersion(clickToCallModel.getBaseSite().getUid()+"ProductCatalog","Online");
		catalogVersionService.setSessionCatalogVersion(clickToCallModel.getBaseSite().getUid()+"ProductCatalog","Staged");
		if(null!=clickToCallModel.getBaseSite()){
			if(LlaCoreConstants.SITE_PUERTORICO.equalsIgnoreCase(clickToCallModel.getBaseSite().getUid())){
				clickToCallService.getGenesysClickToCalls(clickToCallModel.getBaseSite());
		  } 
		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	@Override
	public boolean isAbortable(){
		return  true;
	}

}
