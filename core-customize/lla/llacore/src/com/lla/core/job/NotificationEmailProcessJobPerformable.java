/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.core.job;

import com.lla.core.model.NotificationEmailProcessCronJobModel;
import com.lla.core.service.notificationemail.NotificationEmailProcessService;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class NotificationEmailProcessJobPerformable extends AbstractJobPerformable<NotificationEmailProcessCronJobModel>
{
	private static final Logger LOG = Logger.getLogger(NotificationEmailProcessJobPerformable.class);
	@Autowired
	private NotificationEmailProcessService notificationEmailProcessService;

	@Override
	public PerformResult perform(NotificationEmailProcessCronJobModel notificationProcess) {
		int processCount=notificationEmailProcessService.fetchFailedNotificationProcess(notificationProcess.getSite(),notificationProcess.getFilesDaysOld());
		LOG.info(String.format("Restarted Notification Process for %s Orders",processCount));
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}
	@Override
	public boolean isAbortable(){
		return  true;
	}

}
