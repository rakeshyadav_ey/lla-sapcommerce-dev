/**
 *
 */
package com.lla.core.job.strategy;

import com.lla.core.util.LLADateUtils;
import de.hybris.platform.catalog.model.ItemSyncTimestampModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.hmc.jalo.ConfigConstants;
import de.hybris.platform.jobs.maintenance.MaintenanceCleanupStrategy;
import de.hybris.platform.servicelayer.internal.model.MaintenanceCleanupJobModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

public class LLACleanupItemSyncStampsStrategy implements MaintenanceCleanupStrategy<ItemModel, CronJobModel>
{

	private final static Logger LOG = Logger.getLogger(LLACleanupItemSyncStampsStrategy.class.getName());

	//static bean properties
	private FlexibleSearchService flexibleSearchService;
	private ModelService modelService;
	//dynamic job properties
	private Integer maxAllowedValues = Integer.valueOf(ConfigConstants.getInstance().STORING_MODIFIEDVALUES_SIZE);

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.jobs.maintenance.MaintenanceCleanupStrategy#createFetchQuery(de.hybris.platform.cronjob.model.
	 * CronJobModel)
	 */
	@Override
	public FlexibleSearchQuery createFetchQuery(final CronJobModel cjm)
	{
		if (cjm.getJob() instanceof MaintenanceCleanupJobModel
				&& ((MaintenanceCleanupJobModel) cjm.getJob()).getThreshold() != null)
		{
			setThreshold(((MaintenanceCleanupJobModel) cjm.getJob()).getThreshold());
		}

		final String validDate = LLADateUtils
				.formatDateToYYYY_MM_DD_HH_MM_SS(LLADateUtils.getPreviousDate(maxAllowedValues));
		final StringBuilder builder = new StringBuilder();
		builder.append("SELECT {" + ItemSyncTimestampModel.PK + "} FROM {" + ItemSyncTimestampModel._TYPECODE + "}");
		builder.append(" where {" + ItemSyncTimestampModel.MODIFIEDTIME + "}  <=?threshold");

		final FlexibleSearchQuery query = new FlexibleSearchQuery(builder.toString(),
				Collections.singletonMap("threshold", validDate));
		query.setResultClassList(Arrays.asList(ItemModel.class));

		return query;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.jobs.maintenance.MaintenanceCleanupStrategy#process(java.util.List)
	 */
	@Override
	public void process(final List<ItemModel> elements)
	{

		LOG.info("Found " + elements.size() + " items old ItemSyncTimestamp");

		for (final Iterator<ItemModel> iter = elements.iterator(); iter.hasNext();)
		{
			final ItemModel model = iter.next();

			final String query = "select {pk} from {" + ItemSyncTimestampModel._TYPECODE + "} " + //
					"where {" + ItemSyncTimestampModel.PK + "} = ?item ";

			final SearchResult<ItemSyncTimestampModel> searchresult = flexibleSearchService.search(query,
					Collections.singletonMap("item", model.getPk()));

			final List<ItemSyncTimestampModel> items = searchresult.getResult();
			modelService.remove(items.get(0));
		}
		LOG.info(String.format("LLACleanupItemSyncTimestamp Job Finished Successfully and it removed  %s SyncLogs :::",
				elements.size()));
	}

	@Required
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	private void setThreshold(final Integer maxAllowedValues)
	{
		if (maxAllowedValues.intValue() < 0)
		{
			throw new IllegalArgumentException("maxAllowedValues cannot be negative.");
		}
		this.maxAllowedValues = maxAllowedValues;
	}

}
