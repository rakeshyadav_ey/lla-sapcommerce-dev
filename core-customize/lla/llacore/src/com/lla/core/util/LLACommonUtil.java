/**
 *
 */
package com.lla.core.util;
import com.lla.core.enums.*;
import com.lla.core.model.PLPC2CFormModel;
import com.lla.facades.address.LLAEnumTypeData;
import com.lla.facades.product.data.LLAProductFeatureData;
import com.lla.telcotmfwebservices.v2.dto.NormalizedAddress;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.model.TmaSubscribedProductModel;
import de.hybris.platform.b2ctelcoservices.model.TmaSubscriptionBaseModel;
import de.hybris.platform.b2ctelcoservices.services.TmaSubscriptionBaseService;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.falloutprocess.model.FalloutProcessModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.lla.core.constants.LlaCoreConstants;
import com.lla.facades.customer.data.DocumentTypeData;
import com.lla.facades.customer.data.DriverLicenceStateData;
import com.lla.facades.order.data.ContractTypeData;
import com.lla.facades.product.data.ProductRegionData;
import com.lla.facades.product.data.PlanTypeData;
import com.lla.facades.product.data.DevicePaymentOptionData;

public class LLACommonUtil
{
	private static final Logger LOGGER = Logger.getLogger(LLACommonUtil.class);
	protected static final String PANAMA_SITE_ID = "website.baseSite.uid.panama";
	protected static final String CHILE_SITE_ID = "website.vtr";
	protected static final String CABLETICA_SITE_ID = "website.baseSite.uid.cabletica";
	private static String SHOW_BILL_MESSAGE = "show_bill_message";
	public static final String PHONE_CATEGORY_NAME = "phone.category.name";
	public static final String PHONE_CATEGORY_NAME_ES = "phone.category.name.es";
	public static final String TV_CATEGORY_NAME = "tv.category.name";
	public static final String TV_CATEGORY_NAME_ES = "tv.category.name.es";
	public static final String INTERNET_CATEGORY_NAME = "internet.category.name";
	public static final String INTERNET_CATEGORY_NAME_ES = "internet.category.name.es";
	public static final String PHONE_CATEGORY_CODE = "phone";
	public static final String TV_CATEGORY_CODE = "tv";
	public static final String INTERNET_CATEGORY_CODE = "internet";
	public static final String PLUS = " + ";
	public static final String MONTHLY_PRICE = "price.per.month";
	public static final String MONTHLY_PRICE_ES = "price.per.month.es";
	public static final String ISOCODE = "en";
	public static final String INTERNET_CODE = "Internet";
	@Autowired
	private CategoryService categoryService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Autowired
	private ConfigurationService configurationService;

	@Resource(name = "productService")
	private ProductService productService;

	@Autowired
		private TmaSubscriptionBaseService  tmaSubscriptionBaseService;

	@Autowired
	private UserService userService;

	@Autowired
	private BaseStoreService baseStoreService;

	@Autowired
	private CustomerAccountService customerAccountService;


	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Autowired
	private EnumerationService enumerationService;
	@Autowired
	private Converter<ProductRegion, ProductRegionData> productRegionConverter;

	@Autowired
	private Converter<PlanTypes, PlanTypeData> planTypeConverter;

	@Autowired
	private Converter<DevicePaymentOption, DevicePaymentOptionData> devicePaymentOptionConverter;

	@Autowired
	private Converter<DocumentType, DocumentTypeData> documentTypeConverter;

	@Autowired
	private Converter<DriverLicenceState, DriverLicenceStateData> driverLicenceStateConverter;

	@Autowired
	private Converter<ContractType, ContractTypeData> contractTypeConverter;

	@Autowired
	private Converter<UrbanResidenceEnum, LLAEnumTypeData> urbanResidenceEnumConverter;

	@Autowired
	private Converter<RuralResidenceEnum, LLAEnumTypeData> ruralResidenceEnumConverter;

	@Autowired
	private CMSSiteService cmsSiteService;
	@Autowired
	private BusinessProcessService businessProcessService;
	@Autowired
	private ModelService modelService;
	@Resource(name = "commerceCategoryService")
   private CommerceCategoryService commerceCategoryService;

	public boolean containsOnlyMobileProducts(final AbstractOrderModel cartModel)
	{
		boolean onlyMobileProducts = false;
		if (cartModel.getEntries() != null && !cartModel.getEntries().isEmpty())
		{
			for (final AbstractOrderEntryModel entry : cartModel.getEntries())
			{
				if (containsSecondLevelCategoryWithCode(entry.getProduct(), "mobile"))
				{
					onlyMobileProducts = true;
				}
				else
				{
					onlyMobileProducts = false;
					break;
				}
			}
		}
		return onlyMobileProducts;
	}

	public boolean containsSecondLevelCategoryWithCode(final ProductModel product, final String categoryCode)
	{
		final Collection<CategoryModel> allCategories = product.getSupercategories();
		final CategoryModel category = categoryService.getCategoryForCode(categoryCode);
		for (final CategoryModel cat : allCategories)
		{
			final Collection<CategoryModel> superCat = cat.getAllSupercategories();
			if (superCat.contains(category))
			{
				return true;
			}
		}
		return false;
	}

	public boolean containsFirstLevelCategoryWithCode(final ProductModel product, final String categoryCode)
	{
		final Collection<CategoryModel> allCategories = product.getSupercategories();
		final CategoryModel category = categoryService.getCategoryForCode(categoryCode);
		if (allCategories.contains(category))
		{
			return true;
		}
		return false;
	}

	public boolean checkFixedLineProduct(final String productCode)
	{
		final String configuredFixedLineProductsCategories = configurationService.getConfiguration()
				.getString(LlaCoreConstants.FIXED_LINE_CATEGORIES, "");
		final List<String> listOfCategories = Stream.of(configuredFixedLineProductsCategories.split(",", -1))
				.collect(Collectors.toList());
		final ProductModel product = productService.getProductForCode(productCode);
		return categoryCheck(product, listOfCategories);
	}
	
	public boolean checkDeviceProduct(final String productCode)
	{
		final String deviceCategory = configurationService.getConfiguration()
				.getString(LlaCoreConstants.DEVICE_CATEGORY, "");
		final ProductModel product = productService.getProductForCode(productCode);
		return product.getSupercategories().stream().anyMatch(r->deviceCategory.contains(r.getCode()));
	}

	private boolean categoryCheck(final ProductModel product, final List<String> listOfCategories)
	{
		for (final String category : listOfCategories)
		{
			if (containsSecondLevelCategoryWithCode(product, category))
			{
				return true;
			}
		}
		return false;
	}

	private List<String> getProductCategoriesOfCart(final CartModel cartModel){
		final List<String> catCodes=new ArrayList();
		for(final AbstractOrderEntryModel entry:cartModel.getEntries()){
			final ProductModel product = productService.getProductForCode(entry.getProduct().getCode());
			final Collection<CategoryModel> allCategories = product.getSupercategories();
			for(final CategoryModel item:allCategories){
				catCodes.add(item.getCode());
			}
		}
		return catCodes;
	 }

	 public boolean compareFMCCategoriesWithCartCategories(){
		 final CartModel cartModel=cartService.getSessionCart();
		 if(validateCartContainsValidFMCProducts(cartModel)){

		 }
		 final List<String> listOfCategories = getFMCCategories(LlaCoreConstants.FMC_PROMOTION_ALLOWED_CATEGORY);
		 final List<String> cartCategories=getProductCategoriesOfCart(cartModel);
		 if(cartCategories.containsAll(listOfCategories)){
		 	return true;
		 }
		return false;
	 }

	private List<String> getFMCCategories(final String fmcPromotionAllowedCategory) {
		final String fmcAllowedCategories = configurationService.getConfiguration()
				.getString(fmcPromotionAllowedCategory, "");
		return Stream.of(fmcAllowedCategories.split(",", -1))
				.collect(Collectors.toList());
	}

	public Boolean validateCartContainsValidFMCProducts(final CartModel cartModel){
		final String configValueFMCAllowedProducts= configurationService.getConfiguration().getString(LlaCoreConstants.FMC_PROMOTION_APPLICABLE_PRODUCTS);
		final List<String> fmcAllowedProducts= Stream.of(configValueFMCAllowedProducts.split(",", -1))
				.collect(Collectors.toList());
		for (final AbstractOrderEntryModel entryModel:cartModel.getEntries()){
			if(fmcAllowedProducts.contains(entryModel.getProduct().getCode())){
				LOGGER.info("Cart Contains FMC Product "+entryModel.getProduct().getCode());
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	/**
	 * Is cart contains fmc products boolean.
	 *
	 * @param cartModel the cart model
	 * @return the boolean
	 */
	public Boolean isCartContainsFMCProducts(final CartModel cartModel){
	 boolean isPostpaid = false;
	 boolean isBundle = false;
	 final String BUNDLE = configurationService.getConfiguration().getString(LlaCoreConstants.FMC_PRODUCT_BUNDLE);
	 final String POSTPAID = configurationService.getConfiguration().getString(LlaCoreConstants.FMC_PRODUCT_POSTPAID);
	  if(null!=cartModel) {
			for (final AbstractOrderEntryModel entryModel : cartModel.getEntries()) {
				if (containsFirstLevelCategoryWithCode(entryModel.getProduct(), POSTPAID)) {
					isPostpaid = true;
				}
				if (containsSecondLevelCategoryWithCode(entryModel.getProduct(), BUNDLE)) {
					isBundle = true;
				}
			}
		}
	 return isPostpaid && isBundle;
 }
	public Boolean retrieveCustomerSubscription(final CartModel cartModel) {
		final List<String> categoryList=getProductCategoriesOfCart(cartModel);
		final CustomerModel customer=(CustomerModel) userService.getCurrentUser();
		final List<TmaSubscriptionBaseModel> subscriptionBaseModelList=tmaSubscriptionBaseService.getSubscriptionBases(customer.getUid());
		for(final TmaSubscriptionBaseModel base:subscriptionBaseModelList){
			final Set<TmaSubscribedProductModel> subscribedProductModelSet=base.getSubscribedProducts();
			for(final TmaSubscribedProductModel subscribedProduct:subscribedProductModelSet){
				final String productCode=subscribedProduct.getProductCode();
				final ProductModel product = productService.getProductForCode(productCode);
				final Collection<CategoryModel> allCategories = product.getSupercategories();
				for(final CategoryModel item:allCategories){
					categoryList.add(item.getCode());
				}
			}
		}
		if(categoryList.containsAll(getFMCCategories(LlaCoreConstants.FMC_PROMOTION_ALLOWED_CATEGORY))){
			return true;
		}return false;
  }

	public List<ProductModel> retrieveCustomerSubscriptionForCompatibility(final CartModel cartModel) {
		final List<ProductModel> productList = new ArrayList<>();
		final CustomerModel customer=(CustomerModel) userService.getCurrentUser();
		final List<TmaSubscriptionBaseModel> subscriptionBaseModelList=tmaSubscriptionBaseService.getSubscriptionBases(customer.getUid());
		for(final TmaSubscriptionBaseModel base:subscriptionBaseModelList){
			final Set<TmaSubscribedProductModel> subscribedProductModelSet=base.getSubscribedProducts();
			for(final TmaSubscribedProductModel subscribedProduct:subscribedProductModelSet){
				final String productCode=subscribedProduct.getProductCode();
				final ProductModel product = productService.getProductForCode(productCode);
				productList.add(product);
			}
		}
		return productList;
  }

	public Boolean hideInstallationAddressForOrder(final String orderCode) {
		return Boolean.FALSE;
	}

	public List<ProductRegionData>  getJamaicaRegions(){
		final List<ProductRegionData> areaList = new ArrayList<>();
		final List<HybrisEnumValue> productRegionList=enumerationService.getEnumerationValues("ProductRegion");
		for(final HybrisEnumValue item:productRegionList){
			areaList.add(productRegionConverter.convert(ProductRegion.valueOf(item.getCode())));
		}
		return areaList;
	}

	public List<PlanTypeData>  getPlanTypes(){
		final List<PlanTypeData> planList = new ArrayList<>();
		final List<HybrisEnumValue> planTypeList=enumerationService.getEnumerationValues("PlanTypes");
		for(final HybrisEnumValue item:planTypeList){
			planList.add(planTypeConverter.convert(PlanTypes.valueOf(item.getCode())));
		}
		return planList;
	}

	public List<DevicePaymentOptionData>  getDevicePaymentOptions(){
		final List<DevicePaymentOptionData> devicePaymentList = new ArrayList<>();
		final List<HybrisEnumValue> devicePaymentOptionList=enumerationService.getEnumerationValues("DevicePaymentOption");
		for(final HybrisEnumValue item:devicePaymentOptionList){
			devicePaymentList.add(devicePaymentOptionConverter.convert(DevicePaymentOption.valueOf(item.getCode())));
		}
		return devicePaymentList;
	}

	public Boolean isSiteJamaica(){
		return null!=cmsSiteService.getCurrentSite() && cmsSiteService.getCurrentSite().getUid().equalsIgnoreCase(configurationService.getConfiguration().getString("website.jamaica"))?Boolean.TRUE:Boolean.FALSE;

	}

	public Boolean isSitePuertorico(){
		return null!=cmsSiteService.getCurrentSite() && cmsSiteService.getCurrentSite().getUid().equalsIgnoreCase(configurationService.getConfiguration().getString("website.puertorico"))?Boolean.TRUE:Boolean.FALSE;

	}
	public Boolean isSitePanama() {
		return null != cmsSiteService.getCurrentSite() && cmsSiteService.getCurrentSite().getUid()
				.equalsIgnoreCase(configurationService.getConfiguration().getString(PANAMA_SITE_ID)) ? Boolean.TRUE : Boolean.FALSE;
	}

	public Boolean isSiteVTR()
	{
		return null != cmsSiteService.getCurrentSite() && cmsSiteService.getCurrentSite().getUid()
				.equalsIgnoreCase(configurationService.getConfiguration().getString(CHILE_SITE_ID)) ? Boolean.TRUE : Boolean.FALSE;
	}
	public Boolean isSiteCabletica() {
       return null != cmsSiteService.getCurrentSite() && cmsSiteService.getCurrentSite().getUid()
               .equalsIgnoreCase(configurationService.getConfiguration().getString(CABLETICA_SITE_ID)) ? Boolean.TRUE : Boolean.FALSE;
   }
	/**
	 * Get all possible contract type
	 * @return
	 */
	public List<ContractTypeData>  getPossibleContractTypes(){
		final List<ContractTypeData> contractTypes = new ArrayList<>();
		final List<HybrisEnumValue> contractTypeList=enumerationService.getEnumerationValues("ContractType");
		for(final HybrisEnumValue item:contractTypeList){
			contractTypes.add(contractTypeConverter.convert(ContractType.valueOf(item.getCode())));
		}
		return contractTypes;
	}

	/**
	 * Get all possible document type
	 * @return
	 */
	public List<DocumentTypeData>  getCustomerDocumentTypes(){
		final List<DocumentTypeData> documentTypes = new ArrayList<>();
		final List<HybrisEnumValue> documentTypeList=enumerationService.getEnumerationValues("DocumentType");
		for(final HybrisEnumValue item:documentTypeList){
			documentTypes.add(documentTypeConverter.convert(DocumentType.valueOf(item.getCode())));
		}
		return documentTypes;
	}

	/**
	 * Get possible residence types list.
	 *
	 * @param residenceTypeEnum the residence type enum
	 * @return the list
	 */
	public List<LLAEnumTypeData>  getPossibleResidenceTypes(String residenceTypeEnum){
		final List<LLAEnumTypeData> residenceTypes = new ArrayList<>();
		if(residenceTypeEnum.equalsIgnoreCase("UrbanResidenceEnum")){
			final List<HybrisEnumValue> urbanResidenceTypeList=enumerationService.getEnumerationValues("UrbanResidenceEnum");
			for(final HybrisEnumValue item:urbanResidenceTypeList){
				residenceTypes.add(urbanResidenceEnumConverter.convert(UrbanResidenceEnum.valueOf(item.getCode())));
			}
			return residenceTypes;
		}
		if(residenceTypeEnum.equalsIgnoreCase("RuralResidenceEnum")){
			final List<HybrisEnumValue> ruralResidenceTypesList=enumerationService.getEnumerationValues("RuralResidenceEnum");
			for(final HybrisEnumValue item:ruralResidenceTypesList){
				residenceTypes.add(ruralResidenceEnumConverter.convert(RuralResidenceEnum.valueOf(item.getCode())));
			}
			return residenceTypes;
		}

        return residenceTypes;
	}

	/*
	 * Method returns boolean value based on categories of products present in cart.If Cart only contains products belonging to
	 * 3Pbundles category, it returns true
	 */
	public boolean hideDeliveryMethod(final AbstractOrderModel cartModel)
	{
		boolean hideDeliverySection = false;
		final String categoriesToHideDeliveryMethod =configurationService.getConfiguration().getString("categories.to.hide.delivery.method.panama");
		final List<String> CategoryList= Stream.of(categoriesToHideDeliveryMethod.split(",", -1))
				.collect(Collectors.toList());

		if (null != cartModel && !CollectionUtils.isEmpty(cartModel.getEntries()))
		{
			for (final AbstractOrderEntryModel entry : cartModel.getEntries())
			{
				hideDeliverySection = false;
				for (final String categoryCode : CategoryList)
				{
					if (containsFirstLevelCategoryWithCode(entry.getProduct(), categoryCode)
							|| containsSecondLevelCategoryWithCode(entry.getProduct(), categoryCode))
					{
						hideDeliverySection = true;
						break;

					}

				}
				if (!hideDeliverySection)
				{
					break;
				}
			}
		}
		return hideDeliverySection;
	}

	/*
	 * Method returns boolean value based on categories of products present in cart.If Cart only contains products from categories such as
	 * postpaid,3Pbundles,4Pbundles it returns true
	 */
	public boolean hidePaymentMethod(final AbstractOrderModel cartModel)
	{
		boolean hidePaymentSection = false;
		final String categoriesToHidePaymentMethod =configurationService.getConfiguration().getString("categories.to.hide.payment.method.panama");
		final List<String> CategoryList= Stream.of(categoriesToHidePaymentMethod.split(",", -1))
				.collect(Collectors.toList());

		if (null != cartModel && !CollectionUtils.isEmpty(cartModel.getEntries()))
		{
			for (final AbstractOrderEntryModel entry : cartModel.getEntries())
			{
				hidePaymentSection = false;
				for (final String categoryCode : CategoryList)
				{
					if (containsFirstLevelCategoryWithCode(entry.getProduct(), categoryCode)
							|| containsSecondLevelCategoryWithCode(entry.getProduct(), categoryCode))
					{
						hidePaymentSection = true;
						break;

					}

				}
				if (!hidePaymentSection)
				{
					break;
				}
			}
		}
		return hidePaymentSection;
	}

	public boolean isFreeInstallationAvailable(final AbstractOrderModel cartModel)	{

		boolean isFreeInstallationAvailable = false;
		final String categoriesToEnableFreeInstallation =configurationService.getConfiguration().getString("free.installation.check.categories.panama");
		final List<String> CategoryList= Stream.of(categoriesToEnableFreeInstallation.split(",", -1))
				.collect(Collectors.toList());

		if (null != cartModel && !CollectionUtils.isEmpty(cartModel.getEntries()))
		{
			for (final AbstractOrderEntryModel entry : cartModel.getEntries())
			{
				for (final String categoryCode : CategoryList)
				{
					if (containsFirstLevelCategoryWithCode(entry.getProduct(), categoryCode)
							|| containsSecondLevelCategoryWithCode(entry.getProduct(), categoryCode))
					{
						isFreeInstallationAvailable = true;
						break;

					}
				}
				if (isFreeInstallationAvailable)
				{
					break;
				}
			}
		}
		return isFreeInstallationAvailable;
	}

	public OrderModel getOrderForCode(final String orderCode)
	{

		final BaseStoreModel baseStoreModel = baseStoreService.getCurrentBaseStore();
		return customerAccountService.getOrderForCode((CustomerModel) userService.getCurrentUser(), orderCode, baseStoreModel);

	}

	public String getCartCalculationMessage(final CartData sessionCart)
	{
		for (final OrderEntryData entryData : sessionCart.getEntries())
		{
			for (final CategoryData categoryData : entryData.getProduct().getCategories())
			{
				if(categoryData.getCode().equalsIgnoreCase(configurationService.getConfiguration().getString("category.prepaid.name.panama", "prepaid"))){
					return null;
				}
			}
		}
		return SHOW_BILL_MESSAGE;
	}

	public boolean isPrepaidOrder(final OrderData sessionOrder)
	{
		for (final OrderEntryData entryData : sessionOrder.getEntries())
		{
			for (final CategoryData categoryData : entryData.getProduct().getCategories())
			{
				if(categoryData.getCode().equalsIgnoreCase(configurationService.getConfiguration().getString("category.prepaid.name.panama", "prepaid"))){
					return true;
				}
			}
		}
		return false;
	}

	public boolean isPostpaidOrder(final OrderData sessionOrder)
	{
		for (final OrderEntryData entryData : sessionOrder.getEntries())
		{
			for (final CategoryData categoryData : entryData.getProduct().getCategories())
			{
				if(categoryData.getCode().equalsIgnoreCase(configurationService.getConfiguration().getString("category.postpaid.name.panama", "postpaid"))){
					return true;
				}
			}
		}
		return false;
	}

	public boolean isPrepaidCart(){

		final List<String> categories = getProductCategoriesOfCart(cartService.getSessionCart());
		if(categories.contains(configurationService.getConfiguration().getString("category.prepaid.name.panama", "prepaid")))
		{
			return true;
		}
		return false;
	}

	public boolean isContainsOnlyPrepaidProducts(){
		final List<String> categories = getProductCategoriesOfCart(cartService.getSessionCart()).stream().distinct()
				.collect(Collectors.toList());
		if(categories.size()==1 && configurationService.getConfiguration().getString("category.prepaid.name.panama", "prepaid").equalsIgnoreCase(categories.get(0)))
		{
			return true;
		}
		return false;
	}

	public Long getCartAddedProductCount(){
		return cartService.getSessionCart().getEntries().stream().mapToLong(entry -> Math.toIntExact(entry.getQuantity())).sum();
	}

	public boolean hidePaymentSectionForJamaica() {

		final CartModel cart = cartService.getSessionCart();
		if(cart != null && cart.getTotalPrice() != 0)
		{
			return false;
		}
		return true;
	}


	public void setBundleProductPriceLable(final AbstractOrderModel source, final AbstractOrderData target)
	{
		target.setBundleProductPriceLabel(getPriceLabel(source, target));
	}

	private String getPriceLabel(final AbstractOrderModel source, final AbstractOrderData target)
	{
		final LanguageModel currentLanguage = commerceCommonI18NService.getCurrentLanguage();
		final StringBuilder str = new StringBuilder();
		final LinkedHashSet<String> productNameList= new LinkedHashSet<>();

		for (final OrderEntryData entry : target.getEntries())
		{
			for (final CategoryData cat : entry.getProduct().getCategories())
			{
				switch (cat.getCode()){
					case INTERNET_CATEGORY_CODE:
						productNameList.add(INTERNET_CODE + " " + entry.getProduct().getName());
						if (currentLanguage.getIsocode().equals(ISOCODE)) {
							str.append(configurationService.getConfiguration().getString(INTERNET_CATEGORY_NAME));
						}
						else {
							str.append(configurationService.getConfiguration().getString(INTERNET_CATEGORY_NAME_ES));
						}
						break;
					case TV_CATEGORY_CODE:
						productNameList.add(entry.getProduct().getName());
						if(StringUtils.isNotEmpty(str.toString()))
						{
							if (currentLanguage.getIsocode().equals(ISOCODE)) {
								str.append(PLUS).append(configurationService.getConfiguration().getString(TV_CATEGORY_NAME));
							}
							else {
								str.append(PLUS).append(configurationService.getConfiguration().getString(TV_CATEGORY_NAME_ES));
							}
						}
                        if(StringUtils.isEmpty(str.toString()))
						{
							if (currentLanguage.getIsocode().equals(ISOCODE)) {
								str.append(configurationService.getConfiguration().getString(TV_CATEGORY_NAME));
							}
							else {
								str.append(configurationService.getConfiguration().getString(TV_CATEGORY_NAME_ES));
							}
						}
						break;
					case PHONE_CATEGORY_CODE:
						productNameList.add(entry.getProduct().getName());
						if(StringUtils.isNotEmpty(str.toString()))
						{
							if (currentLanguage.getIsocode().equals(ISOCODE)) {
								str.append(PLUS).append(configurationService.getConfiguration().getString(PHONE_CATEGORY_NAME));
							} else {
								str.append(PLUS).append(configurationService.getConfiguration().getString(PHONE_CATEGORY_NAME_ES));
							}
						}
						if(StringUtils.isEmpty(str.toString()))
						{
							if (currentLanguage.getIsocode().equals(ISOCODE)) {
								str.append(configurationService.getConfiguration().getString(PHONE_CATEGORY_NAME));
							} else {
								str.append(configurationService.getConfiguration().getString(PHONE_CATEGORY_NAME_ES));
							}
						}
						break;
				}
			}
		}
		target.setSelectedProductName(productNameList);
		if (currentLanguage.getIsocode().equals(ISOCODE))
		{
		str.append(configurationService.getConfiguration().getString(MONTHLY_PRICE));
		}
		else
		{
			str.append(configurationService.getConfiguration().getString(MONTHLY_PRICE_ES));
		}
		return str.toString();
	}

    public List<LLAProductFeatureData> setProductFeatureList(TmaProductOfferingModel productModel)
	{
		List<LLAProductFeatureData> features= new ArrayList<>();
		productModel.getProductFeatureList().forEach(p -> {
			LLAProductFeatureData feature = new LLAProductFeatureData();
			feature.setCode(p.getCode());
			feature.setFeatureDescription(p.getFeatureDescription());
			feature.setIconName(p.getIconName());
			feature.setFeatureSequence(p.getSequence());
			features.add(feature);
		});
		return features;
	}


	public <T> Predicate<T> distinctByKey(final Function<? super T, Object> keyExtractor) {
		final Map<Object, Boolean> seen = new ConcurrentHashMap<>();
		return t -> {
			final Object key = keyExtractor.apply(t);
			if (key == null) {
				return false;
			}
			return seen.putIfAbsent(key, Boolean.TRUE) == null;
		};
	}
	/**
	 * Cart Contains product of Different Category
	 * @param cartData
	 * @return
	 */
	public boolean cartContainsEntiresOfDistinctCategory(final CartData cartData) {
		final List<String> catCodeList=new ArrayList<>();
		if(cartData.getEntries().size() >= 2) {

			for(final OrderEntryData entryData:cartData.getEntries()){
				final String categoryCode=entryData.getProduct().getCategories().iterator().next().getCode();
				catCodeList.add(categoryCode);
			}
		}
		final Set<String> set = new HashSet<>();
		final List<String> updatedCodeList = catCodeList.stream().distinct().collect(Collectors.toList());
		return updatedCodeList.size()==catCodeList.size();
	}

	/**
	 * Cart Contains duplicate product or not
	 * @param cartData
	 * @return
	 */
	public boolean cartContainsSameProduct(final CartData cartData) {
		final Set<String> productDataList=new HashSet<>();
		for(final OrderEntryData entryData:cartData.getEntries()){
			final String productCode=entryData.getProduct().getCode();
			if(!productDataList.contains(productCode))
			{
				productDataList.add(productCode);
			}
			else
			{
				return true;
			}
		  }
		return false;
	}

	 public boolean is3PBundlePresent(final AbstractOrderModel order) {
		 boolean is3PBundlePresent =  false;
		 final Collection<CategoryModel> categories = new ArrayList<>();
   	 final CategoryModel category = commerceCategoryService.getCategoryForCode("3Pbundles");	
   	 for (final AbstractOrderEntryModel entry : order.getEntries()){
			 categories.addAll(entry.getProduct().getSupercategories());
			 }
		 if(categories.contains(category)) {
			 is3PBundlePresent = true;
		 }
   	 return is3PBundlePresent;
    }
	 public boolean isTelephoneFree(final AbstractOrderModel cartModel)
	{

		if (configurationService.getConfiguration().getBoolean("telephone.free.message") && CollectionUtils.isNotEmpty(cartModel.getBundleProductMapping())
				&& cartModel.getBundleProductMapping().get(0).getBundleType().equals(BundleTypeEnum.THREEP))
		{
			return true;
		}

		return false;

	}
	/**
	 * @param productDataForCategory
	 * @return
	 */
	 public List<ProductData> sortProductOnSequenceId(final List<ProductData> productDataForCategory)
		{
		 if (isSiteVTR())
			{
			 return productDataForCategory.stream()
						.sorted(Comparator.comparing((ProductData::getSequenceId), Comparator.nullsLast(Comparator.reverseOrder())))
						.collect(Collectors.toList());
			}
			else
			{
				return productDataForCategory.stream()
						.sorted(Comparator.comparing((ProductData::getSequenceId), Comparator.nullsLast(Comparator.naturalOrder())))
						.collect(Collectors.toList());		
			}
		}

	/**
	 * Gets normalized addresses.
	 *
	 * @param normalizedAddressList the normalized address list
	 * @return the normalized addresses
	 */
	public List<String> getNormalizedAddresses(List<NormalizedAddress> normalizedAddressList)
	{
		LinkedList<String> dataList = new LinkedList<>();
			for (NormalizedAddress address:normalizedAddressList) {
				dataList.add(String.join(",",address.getStREETUnderscoreNAME(),address.getNAME(),address.getCITY(),address.getStATEUnderscoreORUnderscorePROVINCE(),address.getPOSTCODE(),address.getHoUSEUnderscoreID()));
			}
		return CollectionUtils.isNotEmpty(dataList) ? dataList : Collections.EMPTY_LIST;
	}
	public boolean isFixedOrder(AbstractOrderModel order) {
		return  order.getEntries().stream().filter(entry -> isFixedCategory(entry.getProduct().getSupercategories())).findAny().isPresent();
	}

	private boolean isFixedCategory(Collection<CategoryModel> supercategories) {

	 	return supercategories.contains(commerceCategoryService.getCategoryForCode("unlimitedplans")) || supercategories.contains(commerceCategoryService.getCategoryForCode("mifiPostpaid"));
	}


	/**
	 * Launch Cabletica Fallout Process
	 * @param cart
	 * @param notificationType
	 * @param source
	 * @param plpc2CForm
	 */
	public void launchFalloutProcess(final CartModel cart, final ClickToCallReasonEnum notificationType, OriginSource source, final PLPC2CFormModel plpc2CForm, final String houseNo, final String ssnValue) {
		final FalloutProcessModel falloutProcessModel = (FalloutProcessModel) businessProcessService.createProcess(
				"FalloutProcess-" + cart.getSite().getUid()+"-" + cart.getCode() + "-" +notificationType.getCode()+"-"+ System.currentTimeMillis(),
				"fallout-process");

		falloutProcessModel.setNotificationType(notificationType);
		falloutProcessModel.setOrder(cart);
		falloutProcessModel.setUser(cart.getUser());
		falloutProcessModel.setSite(cart.getSite());
		falloutProcessModel.setOriginSource(source);
		 if(Objects.nonNull(plpc2CForm))
		 {
			 falloutProcessModel.setPlpC2CForm(plpc2CForm);
		 }
		 if(StringUtils.isNotEmpty(ssnValue))
		 {
		 	falloutProcessModel.setSsnValue(ssnValue);
		 }
		 if(StringUtils.isNotEmpty(houseNo))
		 {
		 	falloutProcessModel.setHouseKey(houseNo);
		 }
		modelService.save(falloutProcessModel);
		modelService.refresh(falloutProcessModel);
		businessProcessService.startProcess(falloutProcessModel);
		LOGGER.info(String.format("Launching Fallout Process ::%s for Site::%s having Cart :: %s  FalloutType :: %s  Originated at :: %s ",falloutProcessModel.getCode(),cart.getSite().getUid(),cart.getCode(), notificationType.getCode(),source.getCode()));
	}




	public List<DriverLicenceStateData>  getDriverLicenceStates(){
		final List<DriverLicenceStateData> driverLicenceStates = new ArrayList<>();
		final List<HybrisEnumValue> driverLicenceStateList=enumerationService.getEnumerationValues("DriverLicenceState");
		for(final HybrisEnumValue item:driverLicenceStateList){
			driverLicenceStates.add(driverLicenceStateConverter.convert(DriverLicenceState.valueOf(item.getCode())));
		}
		return driverLicenceStates;
	}

    /**
     * Update credit score details.
     *
     * @param cartModel the cart model
     */
    public void updateCreditScoreDetails(CartModel cartModel)
	{
		LOGGER.info(String.format("Inside update credit score details for cart: %s",cartModel.getCode()));
	    if(Objects.nonNull(cartModel.getCreditScoreLevel()))
		{
			try {
				cartModel.setCreditScoreLevel(null);
				cartModel.setAmountDueFirstBill(Double.valueOf(0));
				cartModel.setAmountDueToday(Double.valueOf(0));
				cartModel.setPackAmountDueFirstBill(Double.valueOf(0));
				cartModel.setPackAmountDueToday(Double.valueOf(0));
				modelService.save(cartModel);
				modelService.refresh(cartModel);
			}
			catch (Exception ex){
				LOGGER.error(String.format("Exception in updating new Credit Score Model for Cart: %s and exception message is %s ",cartModel.getCode(),ex.getMessage()));
			}

		}
	}

}
