package com.lla.core.util;

import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class LLADateUtils {
    private static final Logger LOG = Logger.getLogger(LLADateUtils.class);
    final static String YYYY_MM_DD_HH_MM_SS_Format = "yyyy-MM-dd HH:mm:ss";
    public static String formatDateToYYYY_MM_DD_HH_MM_SS(final Date date)
    {
        final SimpleDateFormat formatter = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS_Format);
        return formatter.format(date);
    }
    public static Date getPreviousDate(final int days)
    {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, -days);
        final Date validDate = cal.getTime();
        return validDate;
    }
}
