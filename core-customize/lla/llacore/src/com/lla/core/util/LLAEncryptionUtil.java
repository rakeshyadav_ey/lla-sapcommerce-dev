package com.lla.core.util;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
public class LLAEncryptionUtil {
    private static final Logger LOG = Logger.getLogger(LLAEncryptionUtil.class);
    public static final String ENCRYPTION_ALGORITHM = "AES/ECB/PKCS5PADDING";
    public static final String AES = "AES";

    @Autowired
    private ConfigurationService configurationService;

    public String doEncryption(String plainText)
    {
        try
        {
            final Key key = getKey();
            return encryptWithAESKey(plainText, key.getEncoded());
        }
        catch (final Exception e)
        {
          LOG.error(String.format("Error in Encryption for text %s due to %s",plainText,e));
        }
          return StringUtils.EMPTY;

    }

    private static String encryptWithAESKey(final String data, final byte[] key) throws NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException
    {
        final SecretKey secKey = new SecretKeySpec(key, AES);

        final Cipher cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);

        cipher.init(Cipher.ENCRYPT_MODE, secKey);
        final byte[] newData = cipher.doFinal(data.getBytes());

        return Base64.encodeBase64String(newData);
    }

    private static String decryptWithAESKey(final String inputData, final byte[] key) throws NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
    {
        final Cipher cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
        final SecretKey secKey = new SecretKeySpec(key, AES);

        cipher.init(Cipher.DECRYPT_MODE, secKey);
        final byte[] newData = cipher.doFinal(Base64.decodeBase64(inputData.getBytes()));
        return new String(newData);

    }
    public String doDecryption(String encryptedText) {
        try
        {
            final Key key = getKey();
            return decryptWithAESKey(encryptedText, key.getEncoded());
        }
        catch (final Exception e)
        {
           LOG.error(String.format("Unable to decrypt text %s due to error %s", encryptedText,e));

        }
        return encryptedText;
    }

    private Key getKey() throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException {
        final KeyStore keyStore = KeyStore.getInstance("JCEKS");
        final InputStream inputStream = getClass().getResourceAsStream(configurationService.getConfiguration().getString("ssn.keystore.location"));
        final String password = configurationService.getConfiguration().getString("ssn.keystore.password");
        keyStore.load(inputStream, password.toCharArray());
        return keyStore.getKey(configurationService.getConfiguration().getString("ssn.keystore.alias"), password.toCharArray());
    }

    public String doMasking(String text) {
        if (StringUtils.isNotEmpty(text) && text.trim().length() > 4) {
            String maskString = StringUtils.repeat("*", text.trim().length() - 4);
            return StringUtils.overlay(text, maskString, 0, text.trim().length() - 4);

        }
        return text;
    }
}
