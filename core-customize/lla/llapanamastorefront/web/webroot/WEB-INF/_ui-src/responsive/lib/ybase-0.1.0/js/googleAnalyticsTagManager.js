googleAnalyticsTagManager = {
	productsSelected : [],
	currency : '',
	
	pageLoadGoogleAnalyticsData:function(){
		googleAnalyticsTagManager.currency = $("#panamaAnalyticsPageInfo").attr('data-attr-currencyCode');
		window.dataLayer = window.dataLayer || [];       
		var pageType = "", pageName= "";
		pageName = window.location.pathname;
         /*Categories*/
        var primaryCategory = $("#panamaAdobeAnalyticsPageInfo").attr('data-attr-pagename');
        var subCategory = primaryCategory ;
        
        /*Postpaid*/
        if($('body').hasClass('page-plansAndDevices')){ 
        	primaryCategory = 'Mobile';
        	subCategory = $(".js-tab-head.active").attr('data-content-id').replace(/([a-zA-Z])(?=[A-Z])/g, '$1-') == 'postpaid' ? '1P-PlanOnly' : '1P-HandsetPlan';        
            pageType = "PLP"; 
        	}
        /*Prepaid*/
        if(primaryCategory != '' && primaryCategory != undefined && $("#panamaAdobeAnalyticsPageInfo").attr('data-attr-pagename') == 'PREPAID_HOME'){
        	primaryCategory = "Mobile";
            subCategory = "1P-SimOnly";            
        }
        
        if($('body').hasClass('page-productDetailsForDevices')){ 
        	primaryCategory = 'Mobile';
        	subCategory = '1P-HandsetPlan';
        	}
       
        if(primaryCategory != '' && primaryCategory != undefined && (primaryCategory == 'POSTPAID_HOME' || primaryCategory == 'PREPAID_HOME') ){
            primaryCategory = "Mobile";
        }else if(primaryCategory != '' && primaryCategory != undefined && primaryCategory == 'BUNDLE_HOME'){
            primaryCategory = "Bundles";
        }
        
        
      if(primaryCategory != '' && primaryCategory != undefined && primaryCategory == 'Bundles'){
            subCategory = "Bundles";
        }
       
        /*BreadCrumbs*/
        var breadCrumbs = $("#panamaAnalyticsPageInfo").attr('data-attr-breadcrumbsdata');
        if(breadCrumbs == undefined){
            var breadCrumbs = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-breadcrumbsdata');
        }
        if(breadCrumbs == '' || breadCrumbs == undefined){ breadCrumbs = 'N/A'; } else { breadCrumbs = breadCrumbs.split(","); }
        if(breadCrumbs == 'N/A'){breadCrumbs = $("#panamaAdobeAnalyticsPageInfo").attr('data-attr-pageName');}

        /*OrderConfirmation Page*/
        if($('body').hasClass('page-orderConfirmationPage')){
            breadCrumbs = '';
            pageType = "OrderConfirmation";
            primaryCategory = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-pagename');
            subCategory = '';
        }

        /*Cart Page*/
        if($('body').hasClass('page-cartPage')){
            primaryCategory = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-pagename');
            pageType = "Cart";
            subCategory = '';            
        }
        /*Checkout Page*/
        if($("body").hasClass('page-multiStepCheckoutSummaryPage')){
            breadCrumbs = '';
        	primaryCategory = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-pagename');            
            pageType = "Checkout";
            subCategory = '';
        }
        //Home
        if($("#panamaAdobeAnalyticsPageInfo").attr('data-attr-pageName') == 'HOME'){ pageType = "Home"; }               

        var productPage = "";
        //PDP
        if($('body').hasClass('page-productDetails') || $('body').hasClass('page-productDetailsForDevices')){
            productPage = $("#pdpAdobeAnalyticsData").attr('data-attr-name');            
            pageType = "PDP"; 
        }
        //PDP-3p
        if($('body').hasClass('page-productDetails')){            
            subCategory = $("#pdpAdobeAnalyticsData").attr('data-attr-category');
            subCategory = (subCategory == '3Pbundles' ? '3P-Internet-TV-Telephone' : subCategory == '2PInternetTelephone' ? '1P-Internet' :subCategory == '2PInternetTv' ? '2P-Internet-TV': subCategory == '4Pbundles' ? '4P-Internet-TV-Telephone-PlanOnly':'');
            primaryCategory = subCategory.includes('4P') ? "FMC" : "Fixed";
        }
           
        if($('body').hasClass('page-productDetailsForDevices')){
            primaryCategory = "Mobile";
            subCategory = "1P-HandsetPlan";            
        }
       
        //PLP
        if($('body').hasClass('pageLabel-residential')){    
        	pageType = "PLP";        	
        	subCategory = $(".js-tab-head.active").attr('data-content-id').replace(/([a-zA-Z])(?=[A-Z])/g, '$1-');
        	if(subCategory.includes('3P')){
        		subCategory = '3P-Internet-TV-Telephone';
        	}
        	else if(subCategory.includes('4P')){
        		subCategory = '4P-Internet-TV-Telephone-PlanOnly';
        	}
        	else if(subCategory.includes('2PInternetTelephone')||subCategory.includes('2P-Internet-Telephone')){
        		subCategory = '1P-Internet';
        	}
        	primaryCategory = subCategory.includes('4P') ? 'FMC' : 'Fixed';
          	}
        if($('body').hasClass('page-prepaid')){ pageType = "PLP"; }
        /*User Info*/
        var userType = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-customerType');
        if(userType == '' || userType == undefined){ userType = '';}

        var profileID = $("#panamaAnalyticsPageInfo").attr('data-attr-userid');
        var email = $("#panamaAnalyticsPageInfo").attr('data-attr-email');
        if($('body').hasClass('page-productDetailsForDevices') || $('body').hasClass('page-cartPage') || $('body').hasClass('page-multiStepCheckoutSummaryPage') || $('body').hasClass('page-orderConfirmationPage')){
            profileID = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-userid');
            email = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-email');
        }
        var statusLogged = 'Logged';
        var mail = "true";
        if(profileID == '' || profileID == undefined){ profileID = 'N/A'; statusLogged = 'Not Logged'; mail = "false"; }

        var encryptedEmail1 = '';
        if(email != '' && email != undefined && email != 'anonymous'){ encryptedEmail1 = sha256(email); }
        var phone = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-mobilephone');
        var encryptedPhone = '';
        if(phone != '' && phone != undefined){ encryptedPhone = sha256(phone); }

        //Page Load
        var pageInfo = {} , categoryInfo={}, profileInfo={}, socialInfo={};
        pageInfo.pageName= pageName;//'business name of the page', //Dynamic value
        pageInfo.breadCrumbs= breadCrumbs;//['Online Store', 'Mobile Postpaid Plans'], //Dynamic Value
        pageInfo.pageType= pageType;//'type of page from the business', //Dynamic Value
        pageInfo.country= 'Panama';//static value
        pageInfo.channel= 'Ecommerce';//static value
        pageInfo.productPage= productPage;//dynamic value, but this will be product name in PDP, remaining pages will be NA

        categoryInfo.businessCategory='Residential';
        categoryInfo.primaryCategory= primaryCategory;//'main category from the products', //Dynamic Value
        categoryInfo.subCategory=subCategory;//'bundle category' //Dynamic Value

        profileInfo.profileID=profileID;//'encripted business id for the user', //Dynamic Value
        profileInfo.email=encryptedEmail1;//'encripted email SHA256', //Dynamic Value
        profileInfo.phone=encryptedPhone;//'encripted phone number SHA256' //Dynamic Value
        socialInfo.mail=mail;//'boolean', //Dynamic Value
        socialInfo.facebook='false';//boolean', //Dynamic Value
        socialInfo.other='false';//'boolean' //Dynamic Value
        
        /*Every Page Load- Except Order Confirmation*/
        
        if(!($("body").hasClass('page-orderConfirmationPage'))){
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
         	'event': 'vpv',
         	'page': pageInfo,
         	'category':categoryInfo,
         	'user':{
         	'statusLogged': statusLogged,//'logged status from the user', //Dynamic Value
         	'userType': userType,//'guest or client', //Dynamic Value
         	'profileInfo': profileInfo,
         	'social': socialInfo
         	}
         	});        
        }      
        if($('body').hasClass('page-productDetails') || $('body').hasClass('page-productDetailsForDevices')){
            onGtmpageloadpdp();
        }

        if($("body").hasClass('page-multiStepCheckoutSummaryPage')){        	
            var step = $(".js-checkout-step.active").prop("id");
            var stepInfo = '', option='';
            if(step == 'step1') {
                stepInfo = 1;
                option="Personal Information";
            }else if(step == 'step2') {
                stepInfo = 2;
                option="Address";
            }else if(step == 'step3') {
                stepInfo = 3;
                option="Detail";
            }else{
                stepInfo = '';
            }
            if(stepInfo != ''){                                           
                $('.productGtmData').each(function(){
                    var _this=$(this);                                        
                    selectedProducts(_this);                              
                });
                           	
            	window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({
                	'event': 'checkout',
                	  'ecommerce': {
                		  'checkout': {
                			  'actionField': {'step': stepInfo, 'option': option},
                			  'products': googleAnalyticsTagManager.productsSelected
                			  }
                         }
                    });                
            }           

            var errorCount = 0;
            $(".help-block span").each(function (){
                errorCount++;
            });
            if(errorCount > 0){                
                var errorMsg = $("#gtmPersonalFormError").val();
                handleGtmErrors(errorMsg);
            }
        }

        if($("body").hasClass('page-orderConfirmationPage')){

        	var subtotal = 0, price =0, qty = 1;
        	 $('.productGtmDataOrderConfirmationPage').each(function(){
                 var _this=$(this);    
                 price = Number(_this.attr("data-attr-price") == parseInt(0) ? _this.attr("data-attr-price2") : _this.attr("data-attr-price"));
                 qty = Number(_this.attr("data-attr-quantity") == undefined ? 1 : _this.attr("data-attr-quantity") == 0 ? 1 : _this.attr("data-attr-quantity"));
                 _this[0].attributes['data-attr-price'].value = price == 0 ? 0 : Number(price * qty).toFixed(2);                 
                 subtotal = Number(subtotal) + Number(price * qty);        	     
                 selectedProducts(_this);               
             });
        	 subtotal= Number(subtotal).toFixed(2);
        	 var transaction = {}, address = {};
            //onCartPageLoad();
                     
            transaction.id =  $("#orderNumber").attr('data-attr-order-id'); //dynamic value
            transaction.affiliation =  "Panamá Ecommerce"; //static value
            transaction.revenue= subtotal;
            transaction.tax=0;
            transaction.coupon=0;
            transaction.shipping=0;                                  

            if(typeof $("#cartPagePriceInfo").attr('data-attr-taxRate') !== 'undefined') {
            	 transaction.tax = $("#cartPagePriceInfo").attr('data-attr-taxRate').replace(/[^0-9\.]+/g, "");
            	 transaction.revenue = Number(Number(transaction.tax) + Number(subtotal)).toFixed(2)
            }                       
        
            transaction.coupon = Number(transaction.coupon).toFixed(2);
            transaction.shipping = Number(transaction.shipping).toFixed(2);
            
            address.province = $("#addressInfo").attr('data-attr-province'); //dynamic value
            address.district = $("#addressInfo").attr('data-attr-district'); //dynamic value
            address.town = $("#addressInfo").attr('data-attr-town'); //dynamic value
            address.neighbourhood = $("#addressInfo").attr('data-attr-neighbourhood'); //dynamic value
            address.street = $("#addressInfo").attr('data-attr-street'); //dynamic value
            address.country = $("#addressInfo").attr('data-attr-country'); //dynamic value         
            
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            	'event': 'vpv',
            	'page': pageInfo,
            	'category':categoryInfo,
            	'user':{
            	'statusLogged': statusLogged,//'logged status from the user', //Dynamic Value
            	'userType': userType,//'guest or client', //Dynamic Value
            	'profileInfo': profileInfo,
            	'social': socialInfo
            	      },
            	    'address': address,
            	     'ecommerce': {
            	    	 'currencyCode': googleAnalyticsTagManager.currency, //Dynamic Value Ex. USD
            	    	 'purchase': {
            	    		 'actionField': transaction,
            	    		 'products': googleAnalyticsTagManager.productsSelected
            	    		 }
            	          }
            	          });            
        }
        /* Page not Found Code-Start*/
        if($('body').hasClass('page-notFound')){
        	handleGtmErrors($("#gtmNotFoundPageError").val());          
        }
        /* Page not Found Code-End*/
	},
	/*CartPage-Login Form Error-Start*/
     loginFormGtmError: function (){
	   handleGtmErrors($("#gtmCheckoutFormError").val());
	   }
	/*CartPage-Login Form Error-End*/
}

function onGtmpageloadpdp(){
    var products = [];
    $("#pdpAdobeAnalyticsData").each(function(){
        var _this=$(this);      
//        	_this.attr('data-attr-primarycategoryName') = _this.attr('data-attr-category').includes('4P') ? 'FMC' : 'Fixed';
        selectedProducts(_this);                    
    });   
    /* Add on products */
    $(".addonproductdatapdp").each(function(){
        var addonProdInfo = {}, _this=$(this);
        addonProdInfo.name = _this.parent().find("input[name='productName']").val();
        addonProdInfo.id = _this.parent().find("input[name='productCodePostList']").val();
        addonProdInfo.price = _this.val();
        addonProdInfo.brand =  '+ Movil';//'phone brand or Opco Name'//Dynamic Value *Only for Cell Phones, For other products use + Movil
        addonProdInfo.variant = '';       
        addonProdInfo.quantity = parseInt(_this.parent().find(".js-qty-selector-input").val());               
        addonProdInfo.category = 'Residential'+'/'+'SVA'+'/'+ (addonProdInfo.id.includes('EXT') ? 'Extender' : 'Decoder');      
        googleAnalyticsTagManager.productsSelected.push(addonProdInfo);
    });
    /* Devices on PDP */
    $(".productListForGtmData").each(function(){       
        var _this=$(this);
        _this[0].attributes['data-attr-category_2'].value = 'Mobile';
        _this[0].attributes['data-attr-category'].value = '1P-HandsetPlan';
        _this[0].attributes['data-attr-brand'].value = _this.attr('data-attr-devicebrand')!= undefined &&  _this.attr('data-attr-devicebrand')!='' ? _this.attr('data-attr-devicebrand') : _this.attr('data-attr-brand');
        selectedProducts(_this);

    });

    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
    	'event': 'productView',
    	'ecommerce': {
        	'detail': { // 'add' actionFieldObjectmeasures.
        		'products': googleAnalyticsTagManager.productsSelected
                   }
                 }
    });
}

$(window).on('load', function(){
    let gtmCallInterval = setInterval(function(){
    	 // when window.google_tag_manager is defined and true, stop the function
    	 if (!!window.google_tag_manager) {
            clearInterval(gtmCallInterval);
            googleAnalyticsTagManager.pageLoadGoogleAnalyticsData();
        }
    }, 1000);
});

$(document).ready(function(){

	//PLP Tab change
	$(document).on("click", ".js-tab-head", function (e) {//PLP Tab change
        setTimeout(function(){
        	googleAnalyticsTagManager.pageLoadGoogleAnalyticsData();
        }, 1000);
    });
	//Add to cart
    $(".addToCartFmc3pbundle, .fmc-onlyBundle-btn, .fmc-device-mobile, .device-add-to-cart, #addToCartForm #addToCartButton").on("click", function(e) {
    	if(!$(this).hasClass('addToCartFmcPopupGTM')) {
    	    var isPlanExist = $("#isPlanExist").val();
            if(isPlanExist == "true") {
                popupAddRemoveProductdata("remove", ".existingCartProductPDP");
            }
    		addtocartpdpanalytics();
        }
    });

    $(document).on("click", ".product__listing .add_to_cart_form .add-cart-btn", function(e){
        //FMC-Add Device
        var productCode = $(this).parent().find("input[name='productCodePost']").val();
        var processType = $(this).parent().find("input[name='processType']").val();
        googleAnalyticsTagManager.productsSelected = [];
        $(".productListForGtmData").each(function(){
            if($(this).attr('data-attr-id') == productCode) {
               var _this=$(this);
           	  _this[0].attributes['data-attr-category_2'].value = 'Mobile';
              _this[0].attributes['data-attr-category'].value = '1P-HandsetPlan';
              _this[0].attributes['data-attr-brand'].value = _this.attr('data-attr-devicebrand')!= undefined &&  _this.attr('data-attr-devicebrand')!='' ? _this.attr('data-attr-devicebrand') : _this.attr('data-attr-brand');
               selectedProducts(_this);
               window.dataLayer.push({
               	'event': 'addToCart',
               	'ecommerce': {
                   	'currencyCode': googleAnalyticsTagManager.currency,
                   	'add': { // 'add' actionFieldObjectmeasures.
                   		'products': googleAnalyticsTagManager.productsSelected
                              }
                            }
               });
           }
        });
    });

    $(document).on("click", ".devices-plp .product__listing .add_to_cart_form .add-cart-btn", function(e){
        //Postpaid PLP or Bundle PDP Device add to cart
        var productCode = $(this).parent().find("input[name='productCodePost']").val();
        var planType = $(this).parent().find("input[name='plan']").val();
        var addedProdInfo = [];
        addedProdInfo.name = $('a.btn.add-cart-btn[data-device-code=' + productCode + ']').attr('data-device-name');
        addedProdInfo.id = productCode;
        addedProdInfo.price = $('span.actual-price.'+ planType).attr('data-price');
        addedProdInfo.price = addedProdInfo.price.replace(/[^0-9\.]+/g, "").substring(1, (addedProdInfo.price.length));
        addedProdInfo.brand = $('a.btn.add-cart-btn[data-device-code=' + productCode + ']').attr('data-device-brand');
        addedProdInfo.quantity = 1;
        addedProdInfo.primaryCategory = "Mobile";
        addedProdInfo.subcategory = "1P-HandsetPlan";
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
        	'event': 'addToCart',
        	'ecommerce': {
            	'currencyCode': googleAnalyticsTagManager.currency,
            	'add': { // 'add' actionFieldObjectmeasures.
            		'products': addedProdInfo
                       }
                     }
        });
    });

  //Prepaid PLP Add To Cart
    $(document).on("click", ".prepaid-addcart-btn", function(e) {
    	var _this=$(this);
    	_this[0].attributes['data-attr-category_2'].value = 'Mobile';
    	_this[0].attributes['data-attr-category'].value = '1P-SimOnly';
    	_this[0].attributes['data-attr-brand'].value = '+ Movil';
        selectedProducts(_this);
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
        	'event': 'addToCart',
        	'ecommerce': {
            	'currencyCode': googleAnalyticsTagManager.currency,
            	'add': { // 'add' actionFieldObjectmeasures.
            		'products': googleAnalyticsTagManager.productsSelected
                       }
                     }
        });
    });

  //Postpaid PLP Add To Cart
    $(document).on("click", ".plp-onlyPlan-btn, .btn-plan-mobile", function(e) {
    	var productCodePost = $("#addToCartFormPostPaidPlp input[name='productCodePost']").val();
        $(".addcart-btn").each(function() {
            if($(this).attr('data-attr-item-code') == productCodePost){
            	var _this=$(this);
            	_this[0].attributes['data-attr-id'].value = _this.attr('data-attr-item-code');
            	_this[0].attributes['data-attr-category_2'].value = 'Mobile';
            	_this[0].attributes['data-attr-category'].value = _this.attr('data-attr-category') == 'postpaid' ? '1P-PlanOnly' : '1P-HandsetPlan';
                selectedProducts(_this);
                window.dataLayer.push({
                	'event': 'addToCart',
                	'ecommerce': {
                    	'currencyCode': googleAnalyticsTagManager.currency,
                    	'add': { // 'add' actionFieldObjectmeasures.
                    		'products': googleAnalyticsTagManager.productsSelected
                               }
                             }
                });

            }
        });
    });
  //Addons add/remove
    $('.addonqtyplus, .qtyplus').click(function (e) {//Addon Add to cart-Add quantity
        var productCode = $(this).parent().parent().find("input[name='productCode']").val() || $(this).parent().parent().find("input[name='productCodePost']").val();
        var quantity = parseInt($(this).parent().parent().find("input[name='initialQuantity']").val()) + 1;
        cartpageAddRemoveQty("add", productCode, quantity);
    });

    $('.addonqtyminus, .qtyminus').click(function (e) {//Addon Remove to cart-Remove quantity
    	var productCode = $(this).parent().parent().find("input[name='productCode']").val()|| $(this).parent().parent().find("input[name='productCodePost']").val();
        var quantity = parseInt($(this).parent().parent().find("input[name='initialQuantity']").val()) - 1;
        if(quantity < 0 ){ quantity = 0}
        cartpageAddRemoveQty("remove", productCode, quantity);
    });

    $('.js-cartItemDetailBtn').click(function(event) {//Remove from cart
        var fetchData = $(this).parent().parent().parent().find('.item__quantity .quantity-box .gtmProductData');
        var removedProdInfo = [], category = '', brand = '';
        brand = fetchData.attr('data-attr-brand') == undefined ? '+ Movil' : fetchData.attr('data-attr-brand');
        var primarycat = '', subcat = 'bundles';
        //var primarycat = removedProdInfo.id.includes('fixed') ? 'Fixed' : removedProdInfo.id.includes('FMC') ? 'FMC':'Mobile', subcat = 'bundles';
        var id = fetchData.attr('data-attr-id');
        if(id.includes('fixed')){
            primarycat = 'Fixed';
        }else if(id.includes('FMC')){
            primarycat = 'FMC';
        }else{
            primarycat = 'Mobile';
        }
        if (primarycat =='Fixed' || primarycat == 'FMC'){
            brand='+ Movil';
        }
        if(fetchData.attr('data-attr-category_2') == 'Mobile Services') {
           if(fetchData.attr('data-attr-category') == 'postpaid' || fetchData.attr('data-attr-category') == 'prepaid') {
               brand = "+ Movil";
               subcat = fetchData.attr('data-attr-category') == 'prepaid' ? '1P-SimOnly' : '1P-PlanOnly';
            }
           else{
               subcat = '1P-HandsetPlan';
           }
        }
        if(fetchData.attr('data-attr-category') == 'Add on') {
            primarycat = 'SVA';
            if(id.includes('EXT')){
                subcat = 'Extender';
            }else{
                subcat = 'Decoder';
            }
            brand = '+ Movil';
        }
        category = 'Residential'+'/'+primarycat+'/'+subcat;
        removedProdInfo.push({
            'name' : fetchData.attr('data-attr-name'),
            'id' : fetchData.attr('data-attr-id'),
            'price' : fetchData.attr('data-attr-price').replace(/[^0-9\.]+/g, ""),
            'brand' : brand,//'phone brand or Opco Name'//Dynamic Value *Only for Cell Phones, For other products use + Movil
            'category' : category,
            'variant': '',
            'quantity' : fetchData.attr('data-attr-initialQuantity')
        });

        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
        	'event': 'removeFromCart',
        	'ecommerce': {
            	'remove': { // 'remove' actionFieldObjectmeasures.
            		'products': removedProdInfo
                       }
                     }
        });
    });  

});

$(document).on("click", ".page-internettvtelephone .cartItemRemoval-popup .updateCartProduct", function(e) {
    popupAddRemoveProductdata("remove", ".cartItemRemoval-popup .existingCartProduct");
    popupAddRemoveProductdata("add", ".page-internettvtelephone .cartItemRemoval-popup .updateCartProduct");
});

function popupAddRemoveProductdata(type, fetchData){
    var productInfo = [];
    $(fetchData).each(function(){
        var category = '', brand = '';
        brand = $(this).attr('data-attr-brand') == undefined ? '+ Movil' : $(this).attr('data-attr-brand');
        category = getPrimarySecondaryCategory($(this).attr('data-attr-id'));
        productInfo.push({
            'name' : $(this).attr('data-attr-name'),
            'id' : $(this).attr('data-attr-id'),
            'price' : $(this).attr('data-attr-price').replace(/[^0-9\.]+/g, ""),
            'brand' : brand,
            'category' : category,
            'variant': '',
            'quantity' : 1
        });
    });

    if(type == 'add') {
       window.dataLayer = window.dataLayer || [];
       window.dataLayer.push({
        'event': 'addToCart',
        'ecommerce': {
            'currencyCode': googleAnalyticsTagManager.currency,
            'add': { // 'add' actionFieldObjectmeasures.
                'products': productInfo
                      }
                    }
        });
    }
    if(type == 'remove') {
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
         'event': 'removeFromCart',
         'ecommerce': {
             'remove': { // 'remove' actionFieldObjectmeasures.
                 'products': productInfo
                       }
                     }
        });
    }
}

function getPrimarySecondaryCategory(id){
    var primaryCategory = '', subCategory = '';
    switch(id) {
          case 'FMC_400':
          case 'FMC_600':
              primaryCategory = 'FMC'; subCategory = '4P-Internet-TV-Telephone-PlanOnly';
              break;
          case 'fixed_250':
          case 'fixed_400':
          case 'fixed_600':
          case 'fixed_1000':
              primaryCategory = 'Fixed'; subCategory = '3P-Internet-TV-Telephone';
              break;
          case 'Panama_250':
          case 'Panama_400':
          case 'Panama_1000':
              primaryCategory = 'Fixed'; subCategory = '2P-Internet-TV';
              break;
          case 'Panama1_250':
          case 'Panama1_400':
          case 'Panama1_1000':
              primaryCategory = 'Fixed'; subCategory = '1P-Internet';
              break;
          case 'WIFI_EXT':
              primaryCategory='SVA'; subCategory = 'Extender';
              break;
          case 'SETTOP_BOX':
              primaryCategory='SVA'; subCategory = 'Decoder';
              break;
          case 'Mobile_20':
          case 'Mobile_25':
          case 'Mobile_35':
          case 'Mobile_40':
              primaryCategory='Mobile'; subCategory = '1P-SimOnly';
          default:
            primaryCategory = "Mobile";
            subCategory = "1P-HandsetPlan";
    }
    var category = "Residential/"+primaryCategory+"/"+subCategory;
    return category;
}

function cartpageAddRemoveQty(type, productCode, quantity){
	googleAnalyticsTagManager.productsSelected = [];

    $(".gtmProductData").each(function(){
      if($(this).attr('data-attr-id') == productCode) {
    	  var _this=$(this);
          _this[0].attributes['data-attr-brand'].value = _this.attr('data-attr-devicebrand') == undefined ? '+Movil' : _this.attr('data-attr-devicebrand');
          var primarycat = 'bundles', subcat = 'bundles';
          if(_this.attr('data-attr-category_2') == 'Mobile Services') {
              if(_this.attr('data-attr-category') == 'postpaid') {
                   primarycat = 'postpaid'; subcat = 'Mobile Services';
               }
              if(_this.attr('data-attr-category') == 'prepaid') {
                   primarycat = 'prepaid'; subcat = 'Mobile Services';
              }
          }
          if(_this.attr('data-attr-category') == 'Add on' || _this.attr('data-attr-category_2') == 'SVA') {
        	  primarycat = 'SVA';

        	  $(".gtmProductDataForAddon").each(function(){
        		  if(_this.attr('data-attr-id') == productCode) {
        			  _this[0].attributes['data-attr-price'].value=$(this).attr('data-attr-price');
        			  subcat= _this.attr('data-attr-id').includes('EXT') ? 'Extender' : 'Decoder';
        			  _this[0].attributes['data-attr-brand'].value = '+ Movil';
        			  }
        	  });
          }

          _this[0].attributes['data-attr-category_2'].value = primarycat;
          _this[0].attributes['data-attr-category'].value = subcat;
          selectedProducts(_this);

           if(type == 'add') {
               window.dataLayer = window.dataLayer || [];
               window.dataLayer.push({
               	'event': 'addToCart',
               	'ecommerce': {
                   	'currencyCode': googleAnalyticsTagManager.currency,
                   	'add': { // 'add' actionFieldObjectmeasures.
                   		'products': googleAnalyticsTagManager.productsSelected
                              }
                            }
               });
           }
           if(type == 'remove') {
        	   window.dataLayer = window.dataLayer || [];
               window.dataLayer.push({
               	'event': 'removeFromCart',
               	'ecommerce': {
                   	'remove': { // 'remove' actionFieldObjectmeasures.
                   		'products': googleAnalyticsTagManager.productsSelected
                              }
                            }
               });
          }
      }
    });
    $("#addOnZeroAddon").each(function(){
        if($(this).attr('data-attr-id') == productCode) {
      	  var _this=$(this);
      	  var primarycat = 'SVA', subcat= _this.attr('data-attr-id').includes('EXT') ? 'Extender' : 'Decoder';
      	  _this[0].attributes['data-attr-brand'].value ='+Movil';
      	  _this[0].attributes['data-attr-price'].value=$(this).attr('data-attr-price');
      	  _this[0].attributes['data-attr-id'].value=$(this).attr('data-attr-id');
      	  _this[0].attributes['data-attr-name'].value=$(this).attr('data-attr-name');
      	  selectedProducts(_this);
          var event = type == 'add' ? 'addToCart' : 'removeFromCart';
          var ecom =  type == 'add' ? {
        	  'currencyCode': googleAnalyticsTagManager.currency,
        	  'add' : { // 'add' actionFieldObjectmeasures.
        		  'products': googleAnalyticsTagManager.productsSelected
        		  }
          } :
          {
        	  'remove' : { // 'add' actionFieldObjectmeasures.
        		  'products': googleAnalyticsTagManager.productsSelected
        		  }
          } ;
          window.dataLayer = window.dataLayer || [];
          window.dataLayer.push({
        	  'event': event,
        	  'ecommerce': ecom
        	  });
          }
        }
    );
}

function addtocartpdpanalytics(){
	googleAnalyticsTagManager.productsSelected = [];
    $("#pdpAdobeAnalyticsData").each(function(){//PDP Add to cart-step2
    	var _this=$(this);    	
          selectedProducts(_this);
    });

    $(".addonproductdatapdp").each(function(){//Addons on PDP
    	var productInfo = {} ;
    	var _this=$(this);
        productInfo.name = _this.parent().find("input[name='productName']").val();
        productInfo.id = _this.parent().find("input[name='productCodePostList']").val();
        productInfo.price = _this.val();
        productInfo.brand = _this.attr('data-attr-brand') == undefined ? '+ Movil' : _this.attr('data-attr-brand');//'phone brand or Opco Name'//Dynamic Value *Only for Cell Phones, For other products use + Movil
        var input = _this.parent().find(".js-qty-selector-input");
        var inputVal = parseInt(input.val());
        productInfo.quantity = inputVal;
        productInfo.category = 'Residential'+'/'+'SVA'+'/'+ (productInfo.id.includes('EXT') ? 'Extender' : 'Decoder') ;
        productInfo.variant = '';
        googleAnalyticsTagManager.productsSelected.push(productInfo);
    });

    if($(".plan-type.selected").length > 0) {
        var id = $(".plan-type.selected").attr('id');
        var productInfo = {};
        productInfo.name = $('#' + id + ' .plan-name').html();
        productInfo.id = id;
        productInfo.price = $('#' + id + ' .price label').html();
        productInfo.price = productInfo.price.replace(/[^0-9\.]+/g, "").substring(1, (productInfo.price.length));
        productInfo.brand = "+ Movil";
        productInfo.variant = '';
        productInfo.quantity = 1;
        productInfo.category = 'Residential'+'/'+'Mobile'+'/'+'1P-PlanOnly';
        googleAnalyticsTagManager.productsSelected.push(productInfo);
    }
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
    	'event': 'addToCart',
    	'ecommerce': {
        	'currencyCode': googleAnalyticsTagManager.currency,
        	'add': { // 'add' actionFieldObjectmeasures.
        		'products': googleAnalyticsTagManager.productsSelected
                   }
                     }
    });
}

function selectedProducts(selectedItem){
    var addonInfo = {};
    var primaryCategory = selectedItem.attr('data-attr-primarycategoryName') || selectedItem.attr('data-attr-category_2');
    if(primaryCategory == '' || primaryCategory == undefined) { primaryCategory = ''; }
    var subcategory = selectedItem.attr('data-attr-category');
    if(subcategory == '' || subcategory == undefined) { subcategory = ''; }
    if(subcategory == '3Pbundles'){
    	primaryCategory='Fixed';
    	subcategory= '3P-Internet-TV-Telephone';
    }
    if(subcategory == '2PInternetTelephone'){
    	primaryCategory='Fixed';
    	subcategory= '1P-Internet';
    }
    if(subcategory == '2PInternetTv'){
    	primaryCategory='Fixed';
    	subcategory= '2P-Internet-TV';
    }
    if(subcategory == '4Pbundles'){
    	primaryCategory='FMC';
    	subcategory= '4P-Internet-TV-Telephone-PlanOnly';
    }
    if(subcategory == 'Add on'){//Addons
    	primaryCategory='SVA';
    	subcategory= selectedItem.attr('data-attr-id').includes('EXT') ? 'Extender' : 'Decoder';
    	}
    addonInfo.name = selectedItem.attr('data-attr-name') || selectedItem.attr('data-arrt-name');
    addonInfo.id = selectedItem.attr('data-attr-id');
    addonInfo.price =  selectedItem.attr('data-attr-price').includes('B/') ? Number(selectedItem.attr('data-attr-price').replace(/[^0-9\.]+/g, "").substring(1)).toFixed(2) :selectedItem.attr('data-attr-price');//$(this).attr('data-attr-price').replace(/[^0-9\.]+/g, "").substring(1);
    addonInfo.brand = selectedItem.attr('data-attr-brand') == undefined ? '+ Movil' : selectedItem.attr('data-attr-brand');//'phone brand or Opco Name'//Dynamic Value *Only for Cell Phones, For other products use + Movil
    addonInfo.category = 'Residential'+'/'+primaryCategory+'/'+subcategory;
    addonInfo.variant = '';
    addonInfo.quantity = Number(selectedItem.attr('data-attr-quantity') == undefined ? 1 : selectedItem.attr('data-attr-quantity')) ;
    if($('body').hasClass('page-productDetailsForDevices')){ //DevicePDP Add To Cart
        var data = $("#pdpAdobeAnalyticsData");
        if(data.attr('data-attr-primarycategoryName') == 'Mobile Services'){
        	primaryCategory = 'Mobile';
        	subcategory = (subcategory == 'devices' ? '1P-HandsetPlan' : subcategory == 'postpaid' ? '1P-PlanOnly' : '1P-SimOnly');
        }
        else{
        	primaryCategory = data.attr('data-attr-category').includes('4P') ? 'FMC' : 'Fixed';
        	subcategory = data.attr('data-attr-primarycategoryName');
        }
        addonInfo.category = 'Residential'+'/'+primaryCategory+'/'+subcategory;
        addonInfo.brand = data.attr('data-attr-devicebrand');
    }
    if($("body").hasClass('page-orderConfirmationPage') || $("body").hasClass('page-multiStepCheckoutSummaryPage')){
    	addonInfo.price = Number(addonInfo.price).toFixed(2);
    	 var primaryCat = selectedItem.attr('data-attr-category_2'), subCat = selectedItem.attr('data-attr-category'), deviceBrand = selectedItem.attr('data-attr-devicebrand') != undefined && selectedItem.attr('data-attr-devicebrand') !='' ? selectedItem.attr('data-attr-devicebrand') : '+Movil', name=selectedItem.attr('data-attr-name');

         if(subCat == 'Add on') {
        	 primaryCat = 'SVA';
        	 subCat= selectedItem.attr('data-attr-id').includes('EXT') ? 'Extender' : 'Decoder';
        	 addonInfo.brand = '+ Movil';
        	 }
         if(primaryCat == 'Mobile Services'|| primaryCat == 'bundles') {
         	primaryCat = selectedItem.attr('data-attr-id').includes('fixed') ? 'Fixed' :selectedItem.attr('data-attr-id').includes('FMC') ? 'FMC':'Mobile';
         	subCat = (subCat == 'prepaid' ? '1P-SimOnly' : subCat == 'postpaid'? '1P-PlanOnly' : subCat == 'devices'? '1P-HandsetPlan' : primaryCat == 'FMC'? '4P-Internet-TV-Telephone-PlanOnly' : name.includes('Telefon')? '1P-Internet' : name.includes('TV')? '2P-Internet-TV' : 'bundle');
         	primaryCat = subCat.includes('2P') ? 'Fixed' : primaryCat;         	
		subCat = primaryCat.includes('Fixed') && subCat.includes('bundle') ? '3P-Internet-TV-Telephone' : primaryCat.includes('Mobile') && subCat.includes('bundle') ? '1P-HandsetPlan' : subCat;
         }
         let planType = (selectedItem.attr('data-attr-planType') == undefined && selectedItem.attr('data-attr-planType') == null) ? '' : selectedItem.attr('data-attr-planType').toLowerCase();
         let devicePaymentOption = (selectedItem.attr('data-attr-devicePaymentOption') == undefined && selectedItem.attr('data-attr-devicePaymentOption') == null) ? '' : selectedItem.attr('data-attr-devicePaymentOption').toLowerCase();
         planType = planType.includes('portability') ? 'portability' : planType.includes('new') ? 'new' : planType ;
         devicePaymentOption = devicePaymentOption.includes('cash') ? 'cash' : devicePaymentOption.includes('fees') ? 'installments' : devicePaymentOption ;
         if((planType != undefined && planType != null && planType != '') && (devicePaymentOption != undefined && devicePaymentOption != null && devicePaymentOption != '')){
        	 addonInfo.variant = planType + '|' + devicePaymentOption;
         }else{
        	 if(planType != undefined && planType != null && planType != ''){
            	 addonInfo.variant = planType;
             } 
             if(devicePaymentOption != undefined && devicePaymentOption != null && devicePaymentOption != ''){
            	 addonInfo.variant = devicePaymentOption ;
             } 
         }  
         
         addonInfo.brand = deviceBrand;
         addonInfo.category = 'Residential'+'/'+primaryCat+'/'+subCat;
    }
    googleAnalyticsTagManager.productsSelected.push(addonInfo);
  }

function handleGtmErrors(errorMsg){   
  window.dataLayer = window.dataLayer || [];
  window.dataLayer.push({
	'event': 'error',
	'description': errorMsg
   });
}
function sha256(ascii) {
	function rightRotate(value, amount) {
		return (value>>>amount) | (value<<(32 - amount));
	};

	var mathPow = Math.pow;
	var maxWord = mathPow(2, 32);
	var lengthProperty = 'length'
	var i, j; // Used as a counter across the whole file
	var result = ''

	var words = [];
	var asciiBitLength = ascii[lengthProperty]*8;

	//* caching results is optional - remove/add slash from front of this line to toggle
	// Initial hash value: first 32 bits of the fractional parts of the square roots of the first 8 primes
	// (we actually calculate the first 64, but extra values are just ignored)
	var hash = sha256.h = sha256.h || [];
	// Round constants: first 32 bits of the fractional parts of the cube roots of the first 64 primes
	var k = sha256.k = sha256.k || [];
	var primeCounter = k[lengthProperty];

	var isComposite = {};
	for (var candidate = 2; primeCounter < 64; candidate++) {
		if (!isComposite[candidate]) {
			for (i = 0; i < 313; i += candidate) {
				isComposite[i] = candidate;
			}
			hash[primeCounter] = (mathPow(candidate, .5)*maxWord)|0;
			k[primeCounter++] = (mathPow(candidate, 1/3)*maxWord)|0;
		}
	}

	ascii += '\x80' // Append Ƈ' bit (plus zero padding)
	while (ascii[lengthProperty]%64 - 56) ascii += '\x00' // More zero padding
	for (i = 0; i < ascii[lengthProperty]; i++) {
		j = ascii.charCodeAt(i);
		if (j>>8) return; // ASCII check: only accept characters in range 0-255
		words[i>>2] |= j << ((3 - i)%4)*8;
	}
	words[words[lengthProperty]] = ((asciiBitLength/maxWord)|0);
	words[words[lengthProperty]] = (asciiBitLength)

	// process each chunk
	for (j = 0; j < words[lengthProperty];) {
		var w = words.slice(j, j += 16); // The message is expanded into 64 words as part of the iteration
		var oldHash = hash;
		// This is now the undefinedworking hash", often labelled as variables a...g
		// (we have to truncate as well, otherwise extra entries at the end accumulate
		hash = hash.slice(0, 8);

		for (i = 0; i < 64; i++) {
			var i2 = i + j;
			// Expand the message into 64 words
			// Used below if
			var w15 = w[i - 15], w2 = w[i - 2];

			// Iterate
			var a = hash[0], e = hash[4];
			var temp1 = hash[7]
				+ (rightRotate(e, 6) ^ rightRotate(e, 11) ^ rightRotate(e, 25)) // S1
				+ ((e&hash[5])^((~e)&hash[6])) // ch
				+ k[i]
				// Expand the message schedule if needed
				+ (w[i] = (i < 16) ? w[i] : (
						w[i - 16]
						+ (rightRotate(w15, 7) ^ rightRotate(w15, 18) ^ (w15>>>3)) // s0
						+ w[i - 7]
						+ (rightRotate(w2, 17) ^ rightRotate(w2, 19) ^ (w2>>>10)) // s1
					)|0
				);
			// This is only used once, so *could* be moved below, but it only saves 4 bytes and makes things unreadble
			var temp2 = (rightRotate(a, 2) ^ rightRotate(a, 13) ^ rightRotate(a, 22)) // S0
				+ ((a&hash[1])^(a&hash[2])^(hash[1]&hash[2])); // maj

			hash = [(temp1 + temp2)|0].concat(hash); // We don't bother trimming off the extra ones, they're harmless as long as we're truncating when we do the slice()
			hash[4] = (hash[4] + temp1)|0;
		}

		for (i = 0; i < 8; i++) {
			hash[i] = (hash[i] + oldHash[i])|0;
		}
	}

	for (i = 0; i < 8; i++) {
		for (j = 3; j + 1; j--) {
			var b = (hash[i]>>(j*8))&255;
			result += ((b < 16) ? 0 : '') + b.toString(16);
		}
	}
	return result;
};

