ACC.track = {
	trackAddToCart: function (productCode, quantity, cartData)
	{		
		try{
			window.mediator.publish('trackAddToCart',{
				productCode: productCode,
				quantity: quantity,
				cartData: cartData
			});
			}
		catch(error)
		{
		    return true;
		}
	},
	trackRemoveFromCart: function(productCode, initialCartQuantity)
	{		
		try{
			window.mediator.publish('trackRemoveFromCart',{
				productCode: productCode,
				initialCartQuantity: initialCartQuantity
			});
			}
		catch(error)
		{
		    return true;
		}
	},

	trackUpdateCart: function(productCode, initialCartQuantity, newCartQuantity)
	{
		try{
			window.mediator.publish('trackUpdateCart',{
				productCode: productCode,
				initialCartQuantity: initialCartQuantity,
				newCartQuantity: newCartQuantity
			});
			}
		catch(error)
		{
		    return true;
		}
	},

    trackShowReviewClick: function(productCode)
    {
    	try{
    		window.mediator.publish('trackShowReviewClick',{});
			}
		catch(error)
		{
		    return true;
		}
    }

};