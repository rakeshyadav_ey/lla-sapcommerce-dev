ACC.checkout = {

	_autoload: [
		"bindCheckO",
		"bindForms",
		"bindSavedPayments",
		"bindAddressSearchResults",
		"bindPaymentMethodMessage",
		"bindDeliveryMethodMessage",
		"bindPlaceOrderSingleClick"
	],

	bindPlaceOrderSingleClick:function(){

		$(document).ready(function (){
			$("#placeOrderForm1").submit(function() {
				// submit more than once return false
				localStorage.removeItem('line-1');
                localStorage.removeItem('line-2');
                localStorage.removeItem('line-3');
                localStorage.removeItem('line-4');
                localStorage.removeItem('line-5');
                localStorage.removeItem('line-6');
				$(this).submit(function() {
					return false;
				});
				// submit once return true
				return true;
			});
		});

	},

	bindForms:function(){

		$(document).on("click","#addressSubmit",function(e){
			e.preventDefault();
			$('#addressForm').submit();	
		})
		
		$(document).on("click","#deliveryMethodSubmit",function(e){
			e.preventDefault();
			$('#selectDeliveryMethodForm').submit();	
		})

	},

	bindSavedPayments:function(){
		$(document).on("click",".js-saved-payments",function(e){
			e.preventDefault();

			var title = $("#savedpaymentstitle").html();

			$.colorbox({
				href: "#savedpaymentsbody",
				inline:true,
				maxWidth:"100%",
				opacity:0.7,
				//width:"320px",
				title: title,
				close:'<span class="glyphicon glyphicon-remove"></span>',
				onComplete: function(){
				}
			});
		})
	},

	bindCheckO: function ()
	{
		var cartEntriesError = false;
		
		// Alternative checkout flows options
		$('.doFlowSelectedChange').change(function ()
		{
			if ('multistep-pci' == $('#selectAltCheckoutFlow').val())
			{
				$('#selectPciOption').show();
			}
			else
			{
				$('#selectPciOption').hide();

			}
		});



		$('.js-continue-shopping-button').click(function ()
		{
			var checkoutUrl = $(this).data("continueShoppingUrl");
			window.location = checkoutUrl;
		});
		
		$('.js-create-quote-button').click(function ()
		{
			$(this).prop("disabled", true);
			var createQuoteUrl = $(this).data("createQuoteUrl");
			window.location = createQuoteUrl;
		});

		
		$('.expressCheckoutButton').click(function()
				{
					document.getElementById("expressCheckoutCheckbox").checked = true;
		});
		
		$(document).on("input",".confirmGuestEmail,.guestEmail",function(){
			  
			  var orginalEmail = $(".guestEmail").val();
			  var confirmationEmail = $(".confirmGuestEmail").val();
			  
			  if(orginalEmail === confirmationEmail){
			    $(".guestCheckoutBtn").removeAttr("disabled");
			  }else{
			     $(".guestCheckoutBtn").attr("disabled","disabled");
			  }
		});
		
		$('.js-continue-checkout-button').click(function ()
		{
			var checkoutUrl = $(this).data("checkoutUrl");
			
			cartEntriesError = ACC.pickupinstore.validatePickupinStoreCartEntires();
			if (!cartEntriesError)
			{
				var expressCheckoutObject = $('.express-checkout-checkbox');
				if(expressCheckoutObject.is(":checked"))
				{
					window.location = expressCheckoutObject.data("expressCheckoutUrl");
				}
				else
				{
					var flow = $('#selectAltCheckoutFlow').val();
					if ( flow == undefined || flow == '' || flow == 'select-checkout')
					{
						// No alternate flow specified, fallback to default behaviour
						window.location = checkoutUrl;
					}
					else
					{
						// Fix multistep-pci flow
						if ('multistep-pci' == flow)
						{
						flow = 'multistep';
						}
						var pci = $('#selectPciOption').val();

						// Build up the redirect URL
						var redirectUrl = checkoutUrl + '/select-flow?flow=' + flow + '&pci=' + pci;
						window.location = redirectUrl;
					}
				}
			}
			return false;
		});

	},

	bindAddressSearchResults: function() {
		var searchClose = "<span class='close search-close'>&times;</span>";
		$(document).on( "mouseup touchend", function(e){
			var container = $("#addressSearch, #addressValidationResults");
			// if the target of the click isn't the container nor a descendant of the container
			if (!container.is(e.target) && container.has(e.target).length === 0) 
			{
				$("#addressValidationResults").empty();
			}
		});
		$("#addressSearch").on("keyup", function() {
			$("#addressValidationResults").empty();
			if( !$(this).val() ) {
				$("#i18nAddressForm").find("input").prop( "readonly", false );
				$("#i18nAddressForm").find("input").val("");
				$(this).parent(".form-group").find(".close").remove();
		  	}
			else {	
				if($(this).val().length == 1) {
					$(this).parent(".form-group").append(searchClose);
					$("#addressForm .close").on("click", function() {
						$("#addressValidationResults").empty();
						$("#addressSearch").val("");
						$(this).remove();
						$("#i18nAddressForm").find("input").prop( "readonly", false );
						$("#i18nAddressForm").find("input").val("");
					});
				}
				$.ajax({ 
					url: ACC.config.encodedContextPath + "/addressSearch/results",
					data: { freeText: $(this).val().replace(/,/g , '')},
					cache: true,
					type: 'GET',
					dataType: 'json',
					success: function(data) {
						var addressul = "<ul>";
						if (data.length == 0) {
							var noResults = $("#noSearchResults").data("localizedText");
							addressul += noResults;
						}
						else {
							for (var i = 0; i < data.length; ++i) {
								addressul += "<li><a><span id='project'>" + data[i].project + 
								"</span>" + ",&nbsp;" + "<span id='neighbourhood'>" + data[i].neighbourhood + 
								"</span>" + ",&nbsp;" + "<span id='township'>" + data[i].township + 
								"</span>" + ",&nbsp;" + "<span id='district'>" + data[i].district + 
								"</span>" + ",&nbsp;" + "<span id='province'>" + data[i].province + 
								"</span></a></li>";
							}
						}						
						addressul += "</ul>";
						$("#addressValidationResults").append(addressul);
					},
					failure: function(error){
						console.error("error: " + error);
					}
				});
			}
			
		});

		$(document).on("click", "#addressValidationResults li a", function() {
			$("#addressValidationResults").empty();
			$("#i18nAddressForm").find("input").prop( "readonly", false );
			$("#i18nAddressForm").find("select").removeClass("readonly");
			var project = $(this).find("#project").text();
			var neighbourhood = $(this).find("#neighbourhood").text();
			var township = $(this).find("#township").text();
			var district = $(this).find("#district").text();
			var province = $(this).find("#province").text();
			$("#addressSearch").val(project + ", " + neighbourhood + ", " + township + ", " + district + ", " + province);
			$("input[name='building']").val(project).prop( "readonly", true );
			$("input[name='townCity']").val(township).prop( "readonly", true );
			$("input[name='neighbourhood']").val(neighbourhood).prop( "readonly", true );
			$("input[name='district']").val(district).prop( "readonly", true );
			$("input[name='province']").val(province).prop( "readonly", true );
			$("input[name='isServiceable']").val("true");
		});

		$('#manualAddress').change(function() {
			var populateField = $("input[name='building'], input[name='townCity'], input[name='neighbourhood'], input[name='district'], input[name='province']");
			if(this.checked) {
				$("#i18nAddressForm").find("input").prop( "readonly", false );
				$("#i18nAddressForm").find("select").removeClass("readonly");
				$("#addressSearch").val("");
				populateField.val("");
				$("#addressSearch").prop( "readonly", true );
				$("input[name='isServiceable']").val("false");
			}
			else {
				$("#addressSearch").prop( "readonly", false );
				$("#i18nAddressForm").find("input").prop( "readonly", true );
				$("#i18nAddressForm").find("select").addClass("readonly");
				$("input[name='isServiceable']").val("true");
			}
		});

	},

	bindPaymentMethodMessage: function() {
		function showPaymentMessage() {
			var selectedPayment = $('input[name=paymentMethod]:checked', '#silentOrderPostForm').val(); 
			if(selectedPayment == "COD") {
				$(".COD").show();
				$(".Online, .PayInStore").hide();
			}
			else if(selectedPayment == "Online") {
				$(".Online").show();
				$(".COD, .PayInStore").hide();
			}
			else if (selectedPayment == "PayInStore") {
				$(".PayInStore").show();
				$(".COD, .Online").hide();
			}
		}
		$('#silentOrderPostForm input').on('change', function() {
			showPaymentMessage();
		});
	},

	bindDeliveryMethodMessage: function() {
	    $('input[name=delivery_method]:checked').each(function () {
            var selectedDeliveryMethoddata = $(this).val();
            $(".global-alerts."+selectedDeliveryMethoddata).removeClass('hide');
        });
        function showDeliveryMessage() {
            var selectedDelivery = $('input[name=delivery_method]:checked', '#selectDeliveryMethodForm').val();
            if(selectedDelivery == "pickup") {
                $(".global-alerts.pickup").removeClass('hide');
                $(".global-alerts.home-delivery").addClass('hide');
            }
            else if(selectedDelivery == "home-delivery") {
                $(".global-alerts.home-delivery").removeClass('hide');
                $(".global-alerts.pickup").addClass('hide');
            }
        }
        $('#selectDeliveryMethodForm input').on('change', function() {
            showDeliveryMessage();
        });
    }

};

$(document).ready(function () {
	if ($("#customerProfileForm_error").length) {
		var additionalEmail_error = $("#additionalEmail_error").val();
		var mobilePhone_error = $('#mobilePhone_error').val();
		var mobilePhoneSecondary_error = $('#mobilePhoneSecondary_error').val();
		var firstName_error = $('#firstName_error').val();
		var lastName_error = $("#lastName_error").val();
		var documentNumber_error = $("#documentNumber_error").val();
		var cedula_invalid = $("#cedula_invalid").val();
		var cedula_empty = $("#cedula_empty").val();
		
		$("#customerProfileForm").on('keypress',"input[name='mobilePhone']", function(e){
            const pattern = /^[0-9]$/;
            return pattern.test(e.key )
    	});
		$("#customerProfileForm").on('keypress',"input[name='mobilePhoneSecondary']", function(e){
                    const pattern = /^[0-9]$/;
                    return pattern.test(e.key )
            	});
	}
	
	$.validator.addMethod("cedulaRegex", function(value, element, regexpr) {  
		var cedula = $("input[name='documentNumber']").val();
		Array.prototype.insert = function (index, item) {
			this.splice(index, 0, item);
		};
		var re=/^P$|^(?:PE|E|N|[23456789]|[23456789](?:A|P)?|1[0123]?|1[0123]?(?:A|P)?)$|^(?:PE|E|N|[23456789]|[23456789](?:AV|PI)?|1[0123]?|1[0123]?(?:AV|PI)?)-?$|^(?:PE|E|N|[23456789](?:AV|PI)?|1[0123]?(?:AV|PI)?)-(?:\d{1,4})-?$|^(PE|E|N|[23456789](?:AV|PI)?|1[0123]?(?:AV|PI)?)-(\d{1,4})-(\d{1,6})$/i
		var matched = cedula.match(re)
		var isComplete = false;
		if (matched !== null) {
			matched.splice(0,1); // remove the first match, it contains the input string.
			if (matched[0] !== undefined) { // if matched[0] is set => cedula complete
			isComplete = true;
			if (matched[0].match(/^PE|E|N$/)) {
				matched.insert(0,'0');
			}
			if (matched[0].match(/^(1[0123]?|[23456789])?$/)) {
				matched.insert(1,'');
			}
			if (matched[0].match(/^(1[0123]?|[23456789])(AV|PI)$/)) {
				var tmp = matched[0].match(/(\d+)(\w+)/);
				matched.splice(0,1);
				matched.insert(0,tmp[1]);
				matched.insert(1,tmp[2]);
			}
			} // matched[0]
		}
	  
		var result = {
			'isComplete': isComplete,
		};
		return result.isComplete;
	}, cedula_invalid);
	$("#customerProfileForm").validate({
		rules: {
			additionalEmail: "required email",
			mobilePhone: "required digits",
			mobilePhoneSecondary: "required digits",
			"firstName": {
				required: true,
			},
			"lastName": {
				required: true,
			},
			mobilePhone: {
				required: true,
				minlength:8,
			},
			mobilePhoneSecondary: {
				required: false,
				minlength:8,
			    },
			"documentNumber": {
				required: true,
				cedulaRegex: {
					param: /^P$|^(?:PE|E|N|[23456789]|[23456789](?:A|P)?|1[0123]?|1[0123]?(?:A|P)?)$|^(?:PE|E|N|[23456789]|[23456789](?:AV|PI)?|1[0123]?|1[0123]?(?:AV|PI)?)-?$|^(?:PE|E|N|[23456789](?:AV|PI)?|1[0123]?(?:AV|PI)?)-(?:\d{1,4})-?$|^(PE|E|N|[23456789](?:AV|PI)?|1[0123]?(?:AV|PI)?)-(\d{1,4})-(\d{1,6})$/i,
					depends: function (element) {
						var idType = $('.docType').val();
						if(idType === "CEDULA") {
							return true;
						}
					}
				}
			}
		},
		messages: {
			additionalEmail: {
				required:additionalEmail_error,
				email: additionalEmail_error,
			},
			mobilePhone: {
				required: mobilePhone_error,
				digits: mobilePhone_error,
				minlength: mobilePhone_error,
			},
			mobilePhoneSecondary: {
				digits: mobilePhoneSecondary_error,
				minlength: mobilePhoneSecondary_error,
			    },
			firstName: {
				required: firstName_error,
			},
			lastName: {
				required: lastName_error,
			},
			documentNumber: {
				required: function(element) {
					var err;
					var idType = $('.docType').val();
					if(idType === "CEDULA") {
						err = cedula_empty;
					}
					else {
						err = documentNumber_error;
					}
					return err;
				}
			},
		}
	});
	var selectedID;
	function cedulaText() {
        selectedID = $("select[name='documentType']").children("option:selected").val();
        $("input[name='documentNumber']").val("");
        if(selectedID == "CEDULA") {
            $(".validation-text").show();
            $("input[name='documentNumber']").parent(".form-group").find(".error").text(cedula_empty);
        }
        else {
            $(".validation-text").hide();
            $("input[name='documentNumber']").parent(".form-group").find(".error").text(documentNumber_error);
        }
    }
	$("select[name='documentType']").change(function(){
		cedulaText();
	});
	
	if ($("#address_validation_error").length) {
		var streetname_error = $("#streetname_error").val();
		var province_error = $("#province_error").val();
		var district_error = $('#district_error').val();
		var town_error = $('#town_error').val();
		var neighbourhood_error = $("#neighbourhood_error").val();
		var buildingname_error = $("#buildingname_error").val();
		var apartment_error = $("#apartment_error").val();
		var addr_maxlen_error = $("#addr_maxlen_error").val();
		var addr_strlen_error = $("#addr_strlen_error").val();
	}
	$("#addressForm").validate({
		rules: {

			"streetname": {
				required: true,
				maxlength: 20
			},
			"province": {
				required: true,
				maxlength: 40
			},
			"district": {
				required: true,
				maxlength: 40
			},
			"townCity": {
				required: true,
				maxlength: 40
			},
			"neighbourhood": {
				required: true,
				maxlength: 40
			},
			"building":{
				//required: true,
				maxlength: 40
			},
			"appartment": {
				required: true,
				maxlength: 40
			}
		},
		messages: {

			streetname: {
				required:streetname_error,
				maxlength: addr_strlen_error
			},
			province: {
				required:province_error,
				maxlength: addr_maxlen_error
			},
			district: {
				required:district_error,
				maxlength: addr_maxlen_error
			},
			townCity: {
				required:town_error,
				maxlength: addr_maxlen_error
			},
			neighbourhood: {
				required: neighbourhood_error,
				maxlength: addr_maxlen_error
			},
			building: {
				required: buildingname_error,
				maxlength: addr_maxlen_error
			},
			appartment:{
				required: apartment_error,
				maxlength: addr_maxlen_error
			}
		},
        invalidHandler: function() {
             var errorMsg = $("#adobeAddressFormError").val();
             handleGtmErrors(errorMsg);
         }
	});
	//GTM error	
	var errorGlobalAlert= 0;   
    $(".global-alerts .alert-danger").each(function (){
    	errorGlobalAlert++;
    });   
    if(errorGlobalAlert > 0 && $(".js-checkout-step.active").prop("id") == 'step3'){ //checkout-3
    	var errorMsg=""; 
    	$(".global-alerts .alert-danger").each(function (){
    		errorMsg = errorMsg + $(this)[0].innerText.replace( /[\r\n\t]+/gm, "" ).substring(1);    		
        });    	
    	handleGtmErrors(errorMsg);     	
  
    }
})

$(document).ready(function() {
	if($('.page-multiStepCheckoutSummaryPage').length > 0) {
        if (localStorage.getItem('line-1') !== null ) {
            $('[id="address.province"]').val(localStorage.getItem('line-1'));
            $('[id="address.district"]').val(localStorage.getItem('line-2'));
            $('[id="address.townCity"]').val(localStorage.getItem('line-3'));
            $('[id="address.neighbourhood"]').val(localStorage.getItem('line-4'));
            $('[id="address.building"]').val(localStorage.getItem('line-5'));
            $('[id="address.appartment"]').val(localStorage.getItem('line-6'));
        }
    }

	//maxlength characters for address validation
	$("#addressSubmit").on('click', function(event) {
		event.preventDefault();
		var l1 = $("input[name='streetname']").val().length;
		localStorage.setItem("line-1", $('[id="address.province"]').val());
        localStorage.setItem("line-2", $('[id="address.district"]').val());
        localStorage.setItem("line-3", $('[id="address.townCity"]').val());
        localStorage.setItem("line-4", $('[id="address.neighbourhood"]').val());
        localStorage.setItem("line-5", $('[id="address.building"]').val());
        localStorage.setItem("line-6", $('[id="address.appartment"]').val());
		var l2 = $("input[name='building']").val().length;
		var l3 = $("input[name='appartment']").val().length;
		var line =  l2+l3;
		var len = 40;
		if(line > len){
			$("label.maxLenError").remove();
			if($("input[name='appartment'] + maxLenError").length == 0) {
			    var addr_len_error = $("#addr_maxlen_error").val();
			    var label = '<label id="address.appartment-error" class="error maxLenError" for="address.appartment">'+addr_len_error+'</label>';
				$("input[name='appartment']").after(label);
			}
		    return false;
		}
		else{
		    return true;
		}
	});
	
	//Guest checkout fields validation error messages handle in cart page.
	if ($("#guestCheckout").length) {
        $("#guestCheckout").on('keypress',"input[name='mobilePhone']", function(e){
            const pattern = /^[0-9]$/;
            return pattern.test(e.key )
        });
		var guestEmail_error = $("#guestEmail_error").val();
        var guestPhone_error = $("#guestPhone_length_error").val();
        $.validator.addMethod("regx", function(value, element, regexpr) {
            if( regexpr.test(value)){
              return true;
            }
            else{
              return false;
            }
        }, guestEmail_error);
        $("#guestCheckout").validate({
            rules: {
                "guestEmail": {
                    required: true,
                    regx: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                },
                "mobilePhone": {
                    required: true,
					minlength: 8
                }
            },
             messages: {
                guestEmail: guestEmail_error,
                mobilePhone: guestPhone_error
            },
            invalidHandler: function() {
                if (typeof googleAnalyticsTagManager.loginFormGtmError === "function") {
                    googleAnalyticsTagManager.loginFormGtmError();
                }
            }
        });
    }
	
});
