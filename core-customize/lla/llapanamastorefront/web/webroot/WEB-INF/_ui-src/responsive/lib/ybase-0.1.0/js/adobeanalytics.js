adobeAnalytics = {

	pageLoadAdobeAnalyticsData:function(){
	    window.digitalData = window.digitalData || {};

        var referringURL = document.referrer;
        if(referringURL == ''){ referringURL = 'NA'; }

        var primaryCategory = $("#panamaAdobeAnalyticsPageInfo").attr('data-attr-pageName');
        if($('body').hasClass('page-plansAndDevices')){ primaryCategory = 'POSTPAID_HOME'; }
        if($('body').hasClass('page-productDetailsForDevices')){ primaryCategory = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-pageName'); }
        if(primaryCategory != '' && primaryCategory != undefined && (primaryCategory == 'POSTPAID_HOME' || primaryCategory == 'PREPAID_HOME') ){
            primaryCategory = "Mobile";
        }else if(primaryCategory != '' && primaryCategory != undefined && primaryCategory == 'BUNDLE_HOME'){
            primaryCategory = "Bundles";
        }

        var subCategory = $("#panamaAdobeAnalyticsPageInfo").attr('data-attr-pageName');
        if(primaryCategory != '' && primaryCategory != undefined && $("#panamaAdobeAnalyticsPageInfo").attr('data-attr-pageName') == 'POSTPAID_HOME'){
            subCategory = "Postpaid";
        }else if(primaryCategory != '' && primaryCategory != undefined && $("#panamaAdobeAnalyticsPageInfo").attr('data-attr-pageName') == 'PREPAID_HOME'){
            subCategory = "Prepaid";
        }else if(primaryCategory != '' && primaryCategory != undefined && primaryCategory == 'Bundles'){
            subCategory = "Bundles";
        }else{
            subCategory = "NA";
        }

        var breadCrumbs = $("#panamaAdobeAnalyticsPageInfo").attr('data-attr-breadcrumbsdata');
        if(breadCrumbs == undefined){
            var breadCrumbs = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-breadcrumbsdata');
        }
        if(breadCrumbs == '' || breadCrumbs == undefined){ breadCrumbs = 'N/A'; } else { breadCrumbs = breadCrumbs.split(","); }
        if(breadCrumbs == 'N/A'){breadCrumbs = $("#panamaAdobeAnalyticsPageInfo").attr('data-attr-pageName');}

        if($('body').hasClass('page-orderConfirmationPage')){
            breadCrumbs = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-pageName');
        }

        if($('body').hasClass('page-cartPage') || $('body').hasClass('page-multiStepCheckoutSummaryPage') || $('body').hasClass('page-orderConfirmationPage')){
            primaryCategory = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-pageName');
        }

        var profileID = $("#panamaAdobeAnalyticsPageInfo").attr('data-attr-userid');
        var email = $("#panamaAdobeAnalyticsPageInfo").attr('data-attr-email');
        if($('body').hasClass('page-productDetailsForDevices') || $('body').hasClass('page-cartPage') || $('body').hasClass('page-multiStepCheckoutSummaryPage') || $('body').hasClass('page-orderConfirmationPage')){
            profileID = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-userid');
            email = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-email');
        }
        var statusLogged = 'Logged';
        var mail = "true";
        if(profileID == '' || profileID == undefined){ profileID = 'N/A'; statusLogged = 'Not Logged'; mail = "flase"; }

        var encryptedEmail1 = 'NA';
        if(email != '' && email != undefined && email != 'anonymous'){ encryptedEmail1 = sha256(email); }

        var pageType = '';
        if($("#panamaAdobeAnalyticsPageInfo").attr('data-attr-pageName') == 'HOME'){ pageType = "Homepage"; }
        else if($('body').hasClass('page-productDetails') || $('body').hasClass('page-productDetailsForDevices')){ pageType = "Listed Page"; }
        else { pageType = "Subcategory"; }

        var productPage = "NA";
        if($('body').hasClass('page-productDetails') || $('body').hasClass('page-productDetailsForDevices')){
            productPage = $("#pdpAdobeAnalyticsData").attr('data-attr-name');
        }

        if($('body').hasClass('page-productDetails') && $("#pdpAdobeAnalyticsData").attr('data-attr-category') == '3Pbundles'){
            primaryCategory = "Bundles";
            subCategory = "Bundles";
        }

        var pageName = $("#panamaAdobeAnalyticsPageInfo").attr('data-attr-pageName');
        if($('body').hasClass('page-productDetailsForDevices')){
            primaryCategory = "Mobile";
            subCategory = "Postpaid";
            pageName = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-pageName');
        }

        if($('body').hasClass('page-productDetails') || $('body').hasClass('page-productDetailsForDevices') || $('body').hasClass('page-cartPage') || $('body').hasClass('page-multiStepCheckoutSummaryPage') || $('body').hasClass('page-orderConfirmationPage')){
            pageName = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-pageName');
        }

        var userType = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-customerType');
        if(userType == '' || userType == undefined){ userType = 'NA';}

        window.digitalData = window.digitalData || {};
        window.digitalData = {
            page: {
                pageInfo: {
                    hostName: location.hostname, //"https://cwpanama.com", //dynamic value
                    pageName: pageName, //"Bundle - Complete Package 300", //dynamic value
                    currentURL: window.location.href, //"https://cwpanama.com/home", //dynamic value
                    referringURL: referringURL, //"https://example.com/referrer ", //dynamic value
                    breadCrumbs: breadCrumbs, //["Online Store","Mobile Pospaid Plans"], //dynamic value
                },
                category: {
                    primaryCategory: primaryCategory, //"Movil", //dynamic value
                    subCategory: subCategory, //"Postpago", //dynamic value
                    productPage: productPage,//"Data Without Limits", //dynamic value
                    pageType: pageType, //"Listed Page", //dynamic value
                    channel: "ecommerce_panama", //static value
                },
                attributes: {
                    country: "Panama", //static value
                }
            },
            user: [{
                profile: [{
                    statusLogged: statusLogged, //"Logged", //dynamic value
                    userType: userType, //"Guest", //dynamic value
                    profileInfo: {
                        profileID: profileID, //"aq123123", //dynamic value
                        email: encryptedEmail1, //"aiadifaoidfasdfsadfs", //dynamic value
                        phone: "NA", //dynamic value
                    },
                    social: {
                        facebook: "false", //dynamic value
                        mail: mail
                    }
                }]
            }],
            version: "1.0",
        }
        if($('body').hasClass('page-productDetails') || $('body').hasClass('page-productDetailsForDevices')){
            onpageloadpdp();
        }
        if($('body').hasClass('page-prepaid') || $('body').hasClass('page-postpaid')){
            onpageloadplp();
        }
        if($('body').hasClass('page-triple-play')){
            onpageloadtripleplp();
        }
        if($('body').hasClass('page-cartPage')){
            onCartPageLoad();
        }

        if($("body").hasClass('page-multiStepCheckoutSummaryPage')){
            var step = $(".js-checkout-step.active").prop("id");
            var stepInfo = '';
            if(step == 'step1') {
                stepInfo = 1;
            }else if(step == 'step2') {
                stepInfo = 2;
            }else if(step == 'step3') {
                stepInfo = 3;
            }else if(step == 'step4') {
                stepInfo = 4;
            }else{
                stepInfo = '';
            }
            if(stepInfo != ''){
                _satellite.track("evCheckout",{
                    step: stepInfo
                });
                console.log("User is in Step : "+stepInfo);
            }
            var products = [];
            $(".productGtmData").each(function(){
                var productInfo = {}, category = {};
                productInfo.productName = $(this).attr('data-attr-name');
                productInfo.productID = $(this).attr('data-attr-id');
                productInfo.price = $(this).attr('data-attr-price');
                productInfo.brand = "NA";
                if(typeof $(this).attr('data-attr-devicebrand') !== 'undefined') {
                    productInfo.brand = $(this).attr('data-attr-devicebrand');
                }
                productInfo.quantity = $(this).attr('data-attr-quantity');
                var primaryCategory = $(this).attr('data-attr-category_2');
                var subcategory = $(this).attr('data-attr-category_2');

                if($(this).attr('data-attr-category_2') == 'Mobile Services') { primaryCategory = 'mobile'; }
                if($(this).attr('data-attr-category') == 'Add on') { primaryCategory = subcategory = 'bundles'; }
                if($(this).attr('data-attr-category') == 'postpaid' && $(this).attr('data-attr-category_2') == 'Mobile Services') { primaryCategory = 'postpaid'; }
                category.primaryCategory = primaryCategory;
                category.subcategory = subcategory;
                products.push({
                    'productInfo': productInfo,
                    'category': category
                });
            });
            window.digitalData.product = products;

            var errorCount = 0;
            $(".help-block span").each(function (){
                errorCount++;
            });
            if(errorCount > 0){
                console.log("Error pushed");
                var errorMsg = $("#gtmPersonalFormError").val();
                _satellite.track("evCartPageEvent",{
                    descriptionError: errorMsg
                });
                //console.log(errorMsg);
            }
        }

        if($("body").hasClass('page-orderConfirmationPage')){
            _satellite.track("evPurchase",{});

            onCartPageLoad();

            var products = [];
            $(".productGtmDataOrderConfirmationPage").each(function(){
                var productInfo = {}, category = {};
                productInfo.productName = $(this).attr('data-attr-name');
                productInfo.productID = $(this).attr('data-attr-id');
                productInfo.price = $(this).attr('data-attr-price');
                productInfo.brand = "NA";
                if(typeof $(this).attr('data-attr-devicebrand') !== 'undefined') {
                    productInfo.brand = $(this).attr('data-attr-devicebrand');
                }
                productInfo.quantity = $(this).attr('data-attr-quantity');
                var primaryCategory = $(this).attr('data-attr-category_2');
                var subcategory = $(this).attr('data-attr-category_2');

                if($(this).attr('data-attr-category_2') == 'Mobile Services') { primaryCategory = 'mobile'; }
                if($(this).attr('data-attr-category') == 'Add on') { primaryCategory = subcategory = 'bundles'; }
                if($(this).attr('data-attr-category') == 'postpaid' && $(this).attr('data-attr-category_2') == 'Mobile Services') { primaryCategory = 'postpaid'; }
                category.primaryCategory = primaryCategory;
                category.subcategory = subcategory;
                products.push({
                    'productInfo': productInfo,
                    'category': category
                });
            });
            window.digitalData.product = products;

            var transaction = [], profile = [], address = [];
            transaction.transactionID =  $("#orderNumber").attr('data-attr-order-id'); //dynamic value
            transaction.affiliation =  "Ecommerce Panamá"; //static value
            transaction.shippingMethod = $("#addressInfo").attr('data-attr-shippingMethod'); //dynamic value
            address.province = $("#addressInfo").attr('data-attr-province'); //dynamic value
            address.district = $("#addressInfo").attr('data-attr-district'); //dynamic value
            address.town = $("#addressInfo").attr('data-attr-town'); //dynamic value
            address.neighbourhood = $("#addressInfo").attr('data-attr-neighbourhood'); //dynamic value
            address.street = $("#addressInfo").attr('data-attr-street'); //dynamic value
            address.country = $("#addressInfo").attr('data-attr-country'); //dynamic value

            profile.address = address;
            transaction.profile = profile;
            window.digitalData.transaction = transaction;
        }
        //console.log(window.digitalData);
        _satellite.track("vpageview",{});

        var userType = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-customerType');
        if(userType == 'GUEST' || userType == 'guest'){
            document.cookie = "loginSuccessCount=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        }
	}
}

function onCartPageLoad(){
    var cart = {};
    var price = {};

    var subtotal = 0;
    if(typeof $("#cartPagePriceInfo").attr('data-attr-subtotal') !== 'undefined') {
        subtotal = $("#cartPagePriceInfo").attr('data-attr-subtotal').replace(/[^0-9\.]+/g, "").substring(1);
    }
    price.subtotal = subtotal; //dynamic value

    var savings = 0;
    if(typeof $("#cartPagePriceInfo").attr('data-attr-savings') !== 'undefined') {
        savings = $("#cartPagePriceInfo").attr('data-attr-savings').replace(/[^0-9\.]+/g, "").substring(1);
    }
    price.savings = savings;//dynamic value

    var currency = '';
    if(typeof $("#cartPagePriceInfo").attr('data-attr-currency') !== 'undefined') {
        currency = $("#cartPagePriceInfo").attr('data-attr-currency');
    }
    price.currency = currency; //dynamic value

    var taxRate = 0;
    if(typeof $("#cartPagePriceInfo").attr('data-attr-taxRate') !== 'undefined') {
        taxRate = $("#cartPagePriceInfo").attr('data-attr-taxRate').replace(/[^0-9\.]+/g, "").substring(1);
    }
    price.taxRate = taxRate; //dynamic value

    var cartTotal = 0;
    if(typeof $("#cartPagePriceInfo").attr('data-attr-cartTotal') !== 'undefined') {
        cartTotal = $("#cartPagePriceInfo").attr('data-attr-cartTotal').replace(/[^0-9\.]+/g, "").substring(1);
    }
    price.cartTotal = cartTotal; //dynamic value
    cart.price = price;
    window.digitalData.cart = cart;
}

function onpageloadtripleplp(){
    var products = [];
    $(".productListForGtmData").each(function(){
        var productInfo = {}, category = {};
        productInfo.productName = $(this).attr('data-attr-name');
        productInfo.productID = $(this).attr('data-attr-id');
        productInfo.price = $(this).attr('data-attr-price').replace(/[^0-9\.]+/g, "").substring(1);
        productInfo.brand = "NA";
        productInfo.quantity = 1;
        category.primaryCategory = window.digitalData.page.category.primaryCategory;
        category.subcategory = window.digitalData.page.category.subCategory;
        products.push({
            'productInfo': productInfo,
            'category': category
        });
    });
    window.digitalData.product = products;
}

function onpageloadplp(){
    var products = [];
    $(".productListForGtmData").each(function(){
        var productInfo = {}, category = {};
        productInfo.productName = $(this).attr('data-attr-name');
        productInfo.productID = $(this).attr('data-attr-id');
        productInfo.price = $(this).attr('data-attr-price').replace(/[^0-9\.]+/g, "").substring(1);
        productInfo.brand = "NA";
        productInfo.quantity = 1;
        var primarycat = '', subcat = '';
        primarycat = window.digitalData.page.category.primaryCategory;
        subcat = window.digitalData.page.category.subCategory;
        if($(this).attr('data-attr-category') == 'Devices' && $(this).attr('data-attr-category_2') == 'Mobile Services'){
                primarycat = 'mobile'; subcat = 'Mobile Services';
                productInfo.brand = $(this).attr('data-attr-devicebrand');
        }
        category.primaryCategory = primarycat;
        category.subcategory = subcat;
        products.push({
            'productInfo': productInfo,
            'category': category
        });
    });
    window.digitalData.product = products;
}

function onpageloadpdp(){
    var products = [];
    $("#pdpAdobeAnalyticsData").each(function(){
        var productInfo = {}, category = {};
        productInfo.productName = $(this).attr('data-attr-name');
        productInfo.price = $(this).attr('data-attr-price').replace(/[^0-9\.]+/g, "").substring(1);
        productInfo.brand = "NA";
        productInfo.quantity = 1;
        category.primaryCategory = $(this).attr('data-attr-primarycategoryName');
        category.subcategory = $(this).attr('data-attr-primarycategoryName');
        products.push({
            'productInfo': productInfo,
            'category': category
        });
    });
    if($('body').hasClass('page-productDetailsForDevices')){
        products[0].category.primaryCategory = "Mobile";
        products[0].category.subcategory = $("#pdpAdobeAnalyticsData").attr('data-attr-primarycategoryName');
        products[0].productInfo.brand = $("#pdpAdobeAnalyticsData").attr('data-attr-devicebrand');
        //console.log("Device PDP OnLoad Push");
    }
    /* Add on products */
    $(".addonproductdatapdp").each(function(){
        var productInfo = {}, category = {};
        productInfo.productName = $(this).parent().find("input[name='productName']").val();
        productInfo.productID = $(this).parent().find("input[name='productCodePostList']").val();
        productInfo.price = $(this).val();
        productInfo.brand = "NA";
        var input = $(this).parent().find(".js-qty-selector-input");
        var inputVal = parseInt(input.val());
        productInfo.quantity = inputVal;
        category.primaryCategory = 'bundles';
        category.subcategory = 'bundles';
        products.push({
            'productInfo': productInfo,
            'category': category
        });
    });
    /* Devices on PDP */
    $(".productListForGtmData").each(function(){
        var productInfo = {}, category = {};
        productInfo.productName = $(this).attr('data-attr-name');
        productInfo.productID = $(this).attr('data-attr-id');
        productInfo.price = $(this).attr('data-attr-price').replace(/[^0-9\.]+/g, "").substring(1);
        productInfo.brand = "NA";
        productInfo.quantity = 1;
        var primarycat = '', subcat = '';
        primarycat = window.digitalData.page.category.primaryCategory;
        subcat = window.digitalData.page.category.subCategory;
        if($(this).attr('data-attr-category') == 'Devices' && $(this).attr('data-attr-category_2') == 'Mobile Services'){
                primarycat = 'mobile'; subcat = 'Mobile Services';
                productInfo.brand = $(this).attr('data-attr-devicebrand');
        }
        category.primaryCategory = primarycat;
        category.subcategory = subcat;
        products.push({
            'productInfo': productInfo,
            'category': category
        });
    });

    //console.log("PDP OnLoad Push");
    _satellite.track("evProductDetail", { product: products });
    window.digitalData.product = products;
}

$(window).on('load', function(){
    let adobeCallInterval = setInterval(function(){
        // when _satellite is defined and true, stop the function
        if(window.hasOwnProperty('_satellite')){
            clearInterval(adobeCallInterval);
            adobeAnalytics.pageLoadAdobeAnalyticsData();
        }
    }, 1000);
});

$(document).ready(function(){

    $(document).on("click", ".addToCartFmc3pbundle, .fmc-onlyBundle-btn, .fmc-device-mobile, .device-add-to-cart #addToCartForm #addToCartButton", function(e) {
        addtocartpdpadobeanalytics();
    });

    $(document).on("click", ".product__listing .add_to_cart_form .add-cart-btn", function(e){
        //console.log("Postpaid PLP or Bundle PDP Device add to cart");
        var productCode = $(this).parent().find("input[name='productCodePost']").val();
        var addedProduct = [];
        $(".productListForGtmData").each(function(){
            if($(this).attr('data-attr-id') == productCode) {
               var productInfo = {}, category = {};
               productInfo.productName = $(this).attr('data-attr-name');
               productInfo.productID = $(this).attr('data-attr-id');
               productInfo.price = $(this).attr('data-attr-price').replace(/[^0-9\.]+/g, "").substring(1);
               productInfo.brand = $(this).attr('data-attr-devicebrand');
               productInfo.quantity = 1;
               category.primaryCategory = "mobile";
               category.subcategory = "Mobile Services";
               addedProduct.push({
                   'productInfo': productInfo,
                   'category': category
               });
               //console.log(addedProduct);
                _satellite.track("evAddToCart", { product: addedProduct });
           }
        });
    });

    $(document).on("click", ".devices-plp .product__listing .add_to_cart_form .add-cart-btn", function(e){
        //console.log("Postpaid PLP or Bundle PDP Device add to cart");
        var productCode = $(this).parent().find("input[name='productCodePost']").val();
        var addedProduct = [];
        var productInfo = {}, category = {};
        productInfo.productName = $('a.btn.add-cart-btn[data-device-code=' + productCode + ']').attr('data-device-name');
        productInfo.productID = productCode;
        productInfo.price = $('span.actual-price.'+ productCode).attr('data-price');
        productInfo.price = productInfo.price.replace(/[^0-9\.]+/g, "").substring(1, (productInfo.price.length));
        productInfo.brand = $('a.btn.add-cart-btn[data-device-code=' + productCode + ']').attr('data-device-brand');
        productInfo.quantity = 1;
        category.primaryCategory = "mobile";
        category.subcategory = "Mobile Services";
        addedProduct.push({
           'productInfo': productInfo,
           'category': category
        });
        //console.log(addedProduct);
        _satellite.track("evAddToCart", { product: addedProduct });
    });

    $(document).on("click", ".plp-onlyPlan-btn, .fmc-onlyBundle-btn", function(e) {
        _satellite.track("evChooseCellphone",{ isNewPhone: false });
    });

    $(document).on("click", ".btn-plan-mobile, .fmc-device-mobile", function(e) {
        _satellite.track("evChooseCellphone",{ isNewPhone: true });
    });

    $(document).on("click", ".prepaid-addcart-btn", function(e) {
        var products = [];
        var productInfo = {}, category = {};
        productInfo.productName = $(this).attr('data-arrt-name');
        productInfo.productID = $(this).attr('data-attr-item-code');
        productInfo.price = $(this).attr('data-attr-price').replace(/[^0-9\.]+/g, "").substring(1);
        productInfo.brand = "NA";
        productInfo.quantity = 1;
        category.primaryCategory = window.digitalData.page.category.primaryCategory;
        category.subcategory = window.digitalData.page.category.subCategory;
        products.push({
            'productInfo': productInfo,
            'category': category
        });
        //console.log("Prepaid PLP Add To Cart");
        //console.log(products);
        _satellite.track("evAddToCart", { product: products });
    });

    $(document).on("click", ".plp-onlyPlan-btn, .btn-plan-mobile", function(e) {
        var productCodePost = $("#addToCartFormPostPaidPlp input[name='productCodePost']").val();
        $(".addcart-btn").each(function() {
            if($(this).attr('data-attr-item-code') == productCodePost){
                var products = [];
                var productInfo = {}, category = {};
                productInfo.productName = $(this).attr('data-arrt-name');
                productInfo.productID = $(this).attr('data-attr-item-code');
                productInfo.price = $(this).attr('data-attr-price').replace(/[^0-9\.]+/g, "").substring(1);
                productInfo.brand = "NA";
                productInfo.quantity = 1;
                category.primaryCategory = window.digitalData !== undefined ? window.digitalData.page.category.primaryCategory : 'Mobile';
                category.subcategory = window.digitalData !== undefined ? window.digitalData.page.category.subCategory : 'Postpaid';
                products.push({
                    'productInfo': productInfo,
                    'category': category
                });
                //console.log("Postpaid PLP Add To Cart");
                //console.log(products);
                _satellite.track("evAddToCart", { product: products });
            }
        });
    });

    $('.qtyplus').on("click", function (e) {
        var productCode = $(this).parent().parent().find("input[name='productCode']").val();
        var quantity = parseInt($(this).parent().parent().find("input[name='initialQuantity']").val()) + 1;
        cartpageaddorremoveqty("add", productCode, quantity);
    });

    $('.qtyminus').on("click", function (e) {
        var productCode = $(this).parent().parent().find("input[name='productCode']").val();
        var quantity = parseInt($(this).parent().parent().find("input[name='initialQuantity']").val()) - 1;
        if(quantity < 0 ){ quantity = 0}
        cartpageaddorremoveqty("remove", productCode, quantity);
    });

    $('.js-cartItemDetailBtn').click(function(event) {
        var fetchData = $(this).parent().parent().parent().find('.item__quantity .quantity-box .gtmProductData');
        var removedProduct = [];
        var productInfo = {}, category = {};
        productInfo.productName = fetchData.attr('data-attr-name');
        productInfo.productID = fetchData.attr('data-attr-id');
        productInfo.price = fetchData.attr('data-attr-price').replace(/[^0-9\.]+/g, "");
        productInfo.brand = "NA";
        productInfo.quantity = fetchData.attr('data-attr-initialQuantity');
        var primarycat = 'bundles', subcat = 'bundles';
        if(fetchData.attr('data-attr-category_2') == 'Mobile Services') {
            subcat = 'Mobile Services';
           if(fetchData.attr('data-attr-category') == 'postpaid') {
                primarycat = 'postpaid';
            }
           if($(this).attr('data-attr-category') == 'prepaid') {
                primarycat = 'prepaid';
           }
        }else if(fetchData.attr('data-attr-category_2') == 'Devices' && fetchData.attr('data-attr-category_2') == 'Mobile Services'){
                primarycat = 'mobile'; subcat = 'Mobile Services';
        }
        if($(this).attr('data-attr-category') == 'Add on') { primarycat = subcat = 'bundles'; }
        category.primaryCategory = primarycat;
        category.subcategory = subcat;
        removedProduct.push({
           'productInfo': productInfo,
           'category': category
        });
        //console.log("Removed from cart");
        //console.log(removedProduct);
        _satellite.track("evRemoveFromCart", { product: removedProduct });
    });


    $(document).on("click", ".nav__links .right_edge .sub__navigation ul li a", function(e){
        //e.preventDefault();
        var text = '';
        var link = $(this).attr("href");
        if(link.indexOf("prepaid") != -1){ text = 'prepaid_plans';}
        else if(link.indexOf("postpaid") != -1){ text = 'postpaid_plans';}
        else if(link.indexOf("internet") != -1){ text = 'residential_internet_telephony';}
        else if(link.indexOf("FMC") != -1 && link.indexOf("400") != -1){ text = 'residential_complete_package_400';}
        else if(link.indexOf("FMC") != -1 && link.indexOf("600") != -1){ text = 'residential_complete_package_600';}
        //console.log(text);
        _satellite.track("evInternalPromotionMenuClick",{ clickText: text });
    });

    $(document).on("click", ".js-continue-shopping-button", function(){
        _satellite.track("evCartPageEvent",{
            buttonName: 'Continue Shopping '
        });
        console.log("Cart Event continue-shopping");
    });

    $('.js-continue-checkout-button, .guestCheckoutButton').click(function (){
        _satellite.track("evCartPageEvent",{
            buttonName: 'Checkout'
        });
        console.log("Cart Event continue-checkout");
    });

    $(".page-homepage .simple-banner .category-banner-section .banner-content a").click(function(e){
        _satellite.track("evInternalPromotionClick",{
            product: [{
                url: $(this).prop('href'), // Dynamic value
            }],
        });
    });
    /* Version 2 Adobe Changes */
    loginSuccessSetCookie();

    $(".nav__links li .accNavComponent nav div:last-child a").click(function(e){
        var encryptedEmailData = 'NA', encryptedUserPhoneData = 'NA';
        var email = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-email');
        if(email != '' && email != undefined && email != 'anonymous'){ encryptedEmailData = sha256(email); }
        var userPhone =  $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-mobilePhone');
        if(userPhone != '' && userPhone != undefined){ encryptedUserPhoneData = sha256(userPhone); }
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
            'event': 'logout',
            'userType': $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-customerType'),
            'profileID': $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-userid'),
            'email': encryptedEmailData,
            'phone': encryptedUserPhoneData,
            'accountid': encryptedUserPhoneData
        });
        document.cookie = "loginSuccessCount=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    });

    //Guest checkout
    $('.page-cartPage').on('click', '.guestCheckoutButton ', function(event) {
        _satellite.track("evCheckoutPopUpOptions",{
        	action: "GuestGO"
        });
    });

    $('#guestCheckout #guestEmail').on('keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            if($("#guestCheckout").valid()){
                _satellite.track("evCheckoutPopUpOptions",{
                    action: "GuestGO"
                });
            }
        }
    });

    $(document).on("click","#login-popup .btn-blue",function() {
        _satellite.track("evCheckoutPopUpOptions",{
            action: "Login"
        });
    });

    $(document).on("click",".login-popup-close",function() {
        _satellite.track("evCheckoutPopUpOptions",{
            action: "Cancel"
        });
    });

});

function cartpageaddorremoveqty(type, productCode, quantity){
    var addedProduct = [];
    $(".gtmProductData").each(function(){
      if($(this).attr('data-attr-id') == productCode) {
           var productInfo = {}, category = {};
           productInfo.productName = $(this).attr('data-attr-name');
           productInfo.productID = $(this).attr('data-attr-id');
           productInfo.price = $(this).attr('data-attr-price').replace(/[^0-9\.]+/g, "").substring(1);
           productInfo.brand = "NA";
           productInfo.quantity = quantity;
           var primarycat = 'bundles', subcat = 'bundles';
           if($(this).attr('data-attr-category_2') == 'Mobile Services') {
               if($(this).attr('data-attr-category') == 'postpaid') {
                    primarycat = 'postpaid'; subcat = 'Mobile Services';
                }
               if($(this).attr('data-attr-category') == 'prepaid') {
                    primarycat = 'prepaid'; subcat = 'Mobile Services';
               }
           }
           if($(this).attr('data-attr-category') == 'Add on') { primarycat = subcat = 'bundles'; }
           category.primaryCategory = primarycat;
           category.subcategory = subcat;
           addedProduct.push({
               'productInfo': productInfo,
               'category': category
           });
           //console.log(addedProduct);
           if(type == 'add') {
                //console.log("Increased product qty from cart");
                _satellite.track("evAddToCart", { product: addedProduct });
           }
           if(type == 'remove') {
                //console.log("Decreased product qty from cart");
               _satellite.track("evRemoveFromCart", { product: addedProduct });
          }
      }
    });
}

function addtocartpdpadobeanalytics(){
    var products = [];
    $("#pdpAdobeAnalyticsData").each(function(){
        var productInfo = {}, category = {};
        productInfo.productName = $(this).attr('data-attr-name');
        productInfo.price = $(this).attr('data-attr-price').replace(/[^0-9\.]+/g, "").substring(1);
        var brand;
        if($('.prod-brand').length > 0){
            brand = $('.prod-brand').html();
        } else{
            brand = 'NA';
        }
        productInfo.brand = brand;
        productInfo.quantity = 1;
        category.primaryCategory = $(this).attr('data-attr-primarycategoryName');
        category.subcategory = $(this).attr('data-attr-primarycategoryName');
        products.push({
            'productInfo': productInfo,
            'category': category
        });
    });
    if($('body').hasClass('page-productDetailsForDevices')){
        products[0].category.primaryCategory = "Mobile";
        products[0].category.subcategory = $("#pdpAdobeAnalyticsData").attr('data-attr-primarycategoryName');
        products[0].productInfo.brand = $("#pdpAdobeAnalyticsData").attr('data-attr-devicebrand');
        //console.log("DevicePDP Add To Cart");
    }

    $(".addonproductdatapdp").each(function(){
        var productInfo = {}, category = {};
        productInfo.productName = $(this).parent().find("input[name='productName']").val();
        productInfo.productID = $(this).parent().find("input[name='productCodePostList']").val();
        productInfo.price = $(this).val();
        productInfo.brand = "NA";
        var input = $(this).parent().find(".js-qty-selector-input");
        var inputVal = parseInt(input.val());
        productInfo.quantity = inputVal;
        category.primaryCategory = 'bundles';
        category.subcategory = 'bundles';
        products.push({
            'productInfo': productInfo,
            'category': category
        });
    });

    if($(".plan-type.selected").length > 0) {
        var id = $(".plan-type.selected").attr('id');
        var productInfo = {}, category = {};
        productInfo.productName = $('#' + id + ' .plan-name').html();
        productInfo.productID = id;
        productInfo.price = $('#' + id + ' .price label').html();
        productInfo.price = productInfo.price.replace(/[^0-9\.]+/g, "").substring(1, (productInfo.price.length));
        productInfo.brand = "NA";
        productInfo.quantity = 1;
        category.primaryCategory = 'Mobile';
        category.subcategory = 'Postpaid';
        products.push({
            'productInfo': productInfo,
            'category': category
        });
    }
    //console.log("PDP Add To Cart");
    //console.log(products);
    _satellite.track("evAddToCart", { product: products });
}


function sha256(ascii) {
	function rightRotate(value, amount) {
		return (value>>>amount) | (value<<(32 - amount));
	};

	var mathPow = Math.pow;
	var maxWord = mathPow(2, 32);
	var lengthProperty = 'length'
	var i, j; // Used as a counter across the whole file
	var result = ''

	var words = [];
	var asciiBitLength = ascii[lengthProperty]*8;

	//* caching results is optional - remove/add slash from front of this line to toggle
	// Initial hash value: first 32 bits of the fractional parts of the square roots of the first 8 primes
	// (we actually calculate the first 64, but extra values are just ignored)
	var hash = sha256.h = sha256.h || [];
	// Round constants: first 32 bits of the fractional parts of the cube roots of the first 64 primes
	var k = sha256.k = sha256.k || [];
	var primeCounter = k[lengthProperty];

	var isComposite = {};
	for (var candidate = 2; primeCounter < 64; candidate++) {
		if (!isComposite[candidate]) {
			for (i = 0; i < 313; i += candidate) {
				isComposite[i] = candidate;
			}
			hash[primeCounter] = (mathPow(candidate, .5)*maxWord)|0;
			k[primeCounter++] = (mathPow(candidate, 1/3)*maxWord)|0;
		}
	}

	ascii += '\x80' // Append Ƈ' bit (plus zero padding)
	while (ascii[lengthProperty]%64 - 56) ascii += '\x00' // More zero padding
	for (i = 0; i < ascii[lengthProperty]; i++) {
		j = ascii.charCodeAt(i);
		if (j>>8) return; // ASCII check: only accept characters in range 0-255
		words[i>>2] |= j << ((3 - i)%4)*8;
	}
	words[words[lengthProperty]] = ((asciiBitLength/maxWord)|0);
	words[words[lengthProperty]] = (asciiBitLength)

	// process each chunk
	for (j = 0; j < words[lengthProperty];) {
		var w = words.slice(j, j += 16); // The message is expanded into 64 words as part of the iteration
		var oldHash = hash;
		// This is now the undefinedworking hash", often labelled as variables a...g
		// (we have to truncate as well, otherwise extra entries at the end accumulate
		hash = hash.slice(0, 8);

		for (i = 0; i < 64; i++) {
			var i2 = i + j;
			// Expand the message into 64 words
			// Used below if
			var w15 = w[i - 15], w2 = w[i - 2];

			// Iterate
			var a = hash[0], e = hash[4];
			var temp1 = hash[7]
				+ (rightRotate(e, 6) ^ rightRotate(e, 11) ^ rightRotate(e, 25)) // S1
				+ ((e&hash[5])^((~e)&hash[6])) // ch
				+ k[i]
				// Expand the message schedule if needed
				+ (w[i] = (i < 16) ? w[i] : (
						w[i - 16]
						+ (rightRotate(w15, 7) ^ rightRotate(w15, 18) ^ (w15>>>3)) // s0
						+ w[i - 7]
						+ (rightRotate(w2, 17) ^ rightRotate(w2, 19) ^ (w2>>>10)) // s1
					)|0
				);
			// This is only used once, so *could* be moved below, but it only saves 4 bytes and makes things unreadble
			var temp2 = (rightRotate(a, 2) ^ rightRotate(a, 13) ^ rightRotate(a, 22)) // S0
				+ ((a&hash[1])^(a&hash[2])^(hash[1]&hash[2])); // maj

			hash = [(temp1 + temp2)|0].concat(hash); // We don't bother trimming off the extra ones, they're harmless as long as we're truncating when we do the slice()
			hash[4] = (hash[4] + temp1)|0;
		}

		for (i = 0; i < 8; i++) {
			hash[i] = (hash[i] + oldHash[i])|0;
		}
	}

	for (i = 0; i < 8; i++) {
		for (j = 3; j + 1; j--) {
			var b = (hash[i]>>(j*8))&255;
			result += ((b < 16) ? 0 : '') + b.toString(16);
		}
	}
	return result;
};
/* Version 2 Adobe Changes */
function loginSuccessSetCookie(){
    var email = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-email');
    if(email != '' && email != undefined && email != 'anonymous'){
        if(document.cookie.indexOf('loginSuccessCount=') == -1){
            var date = new Date();
            date.setTime(date.getTime() + (1*24*60*60*1000));
            document.cookie="loginSuccessCount=1;expires="+date.toUTCString()+";path=/";
            var encryptedEmailData = 'NA', encryptedUserPhoneData = 'NA';
            var userType = $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-customerType');
            if(email != '' && email != undefined && email != 'anonymous'){
                if(userType == 'GUEST' || userType == 'guest'){
                    var emaildata = email.split("|");
                    email = emaildata[1];
                }
                encryptedEmailData = sha256(email);
            }
            var userPhone =  $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-mobilePhone');
            if(userPhone != '' && userPhone != undefined){ encryptedUserPhoneData = sha256(userPhone); }
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                'event': 'login',
                'userType': userType,
                'profileID': $("#panamaAdobeAnalyticsPageInfoDevices").attr('data-attr-userid'),
                'email': encryptedEmailData,
                'phone': encryptedUserPhoneData,
                'accountid': encryptedUserPhoneData
            });
        }
    }
}
