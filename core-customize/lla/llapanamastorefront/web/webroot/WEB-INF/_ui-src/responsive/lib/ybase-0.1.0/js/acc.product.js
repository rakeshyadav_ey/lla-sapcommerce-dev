ACC.product = {

    _autoload: [
        "bindToAddToCartForm",
        "enableStorePickupButton",
        "enableVariantSelectors",
        "bindFacets",
        "bindTabLogic",
        "showaddtocartpopupPostpaid"
    ],


    bindTabLogic: function () {
        if($('.js-tab-head[data-content-id="devices"]').hasClass("active")) {
	    $(".postpaid-only").hide();
            $(".devices-only").show();	
            $(".selectedTab1").addClass('active');
        }
        if($('.js-tab-head[data-content-id="postpaid"]').hasClass("active")) {
	     $(".postpaid-only").show();
             $(".devices-only").hide();
             $(".selectedTab1").addClass('disable');
             $(".selectedTab2").addClass('active');
        }

        $(document).ready(function () {
            // show the actual device price if there is no price associated with the plan
            $(".page-productDetails").find(".product-price").each(function() {
                var _loopthis = $(this);
                var priceLength = _loopthis.find(".actual-price").length;
                if(priceLength == 0) {
                    _loopthis.find(".p-code").first().addClass("hide");
                    _loopthis.find(".no-offer-price").removeClass("hide").addClass("actual-price");
                }
            });
            $(".plan-type").each(function() {
                var _loopthis = $(this);
                var planLength = _loopthis.find(".with-offer").length;
                if(planLength == 0) {
                    _loopthis.find(".without-offer").addClass("with-offer").removeClass("without-offer");
                }
            });
            if($('.page-productDetails').length > 0){
                sessionStorage.setItem("page-wrapper", 'true');
            } else if ($('.page-internettvtelephone').length > 0) {
                if ((sessionStorage.getItem("page-wrapper") === 'true') && sessionStorage.getItem("residential-category") !== null) {
                    var type = sessionStorage.getItem("residential-category");
                    sessionStorage.removeItem('page-wrapper');
                    $('.js-tab-head[data-content-id='+ type + ']').trigger( "click" );
                } else {
                    sessionStorage.setItem("residential-category", $('.js-tab-head.active').attr('data-content-id'));
                }
            }
            if($(".pageLabel-residential").length > 0 ) {
            	localStorage.removeItem("step-1");
            	localStorage.removeItem("step-2");
            }
        });

        $(document).on("click", ".js-tab-head", function (e) {
            e.preventDefault();
            var _this = $(this);
            var tabId = _this.attr('data-content-id');
            $('.js-tab-head').each(function () {
                var _loopthis = $(this);
                _loopthis.removeClass('active');
            });
            _this.addClass('active');
            if($('.js-tab-head[data-content-id="devices"]').hasClass("active")) {
               $(".postpaid-only").hide();
               $(".devices-only").show();
            }
            if($('.js-tab-head[data-content-id="postpaid"]').hasClass("active")) {
                $(".postpaid-only").show();
                $(".devices-only").hide();
            }
            $('.tab-content-wrapper').each(function () {
                var _loopthis = $(this);
                _loopthis.addClass('display-none');
            });
            $('#' + tabId).removeClass('display-none');
            var plpCtaboxMaxHeight = 0;
            $(".page-internettvtelephone").find($('#' + tabId)).find(".plp-ctabox").each(function(){
                if ($(this).height() > plpCtaboxMaxHeight) { plpCtaboxMaxHeight = $(this).height(); }
            });
            $(".page-internettvtelephone").find($('#' + tabId)).find(".plp-ctabox").height(plpCtaboxMaxHeight);
            sessionStorage.setItem("residential-category", tabId);
        });
        // spliting price to use 2 different font sizes 
        $(".residential-plp .livemore-col").each(function(){
            var priceData = $(this).find(".newPrice").text();
            var dotCount = priceData.match(/\./g).length;
            console.log(dotCount);
            var floatPrice = priceData.split(".");
            if (dotCount == 1) {
                var htmlData = "<span>"+floatPrice[0]+".</span>"+floatPrice[1];
            }
            if (dotCount == 2) {
                var htmlData = "<span>"+floatPrice[0]+".</span>"+floatPrice[1]+ "<span>."+floatPrice[2]+"</span>";
            }
            $(this).find(".newPrice").text("").html(htmlData);
        });
    },


    bindFacets: function () {
        $(document).on("click", ".js-show-facets", function (e) {
            e.preventDefault();
            var selectRefinementsTitle = $(this).data("selectRefinementsTitle");
            var colorBoxTitleHtml = ACC.common.encodeHtml(selectRefinementsTitle);
            ACC.colorbox.open(colorBoxTitleHtml, {
                href: ".js-product-facet",
                inline: true,
                width: "480px",
                onComplete: function () {
                    $(document).on("click", ".js-product-facet .js-facet-name", function (e) {
                        e.preventDefault();
                        $(".js-product-facet  .js-facet").removeClass("active");
                        $(this).parents(".js-facet").addClass("active");
                        $.colorbox.resize()
                    })
                },
                onClosed: function () {
                    $(document).off("click", ".js-product-facet .js-facet-name");
                }
            });
        });
        enquire.register("screen and (min-width:" + screenSmMax + ")", function () {
            $("#cboxClose").click();
        });
    },


    enableAddToCartButton: function () {
        $('.js-enable-btn').each(function () {
            if (!($(this).hasClass('outOfStock') || $(this).hasClass('out-of-stock'))) {
                $(this).prop("disabled", false);
            }
            if ($(this).hasClass('disabled')) {
                $(this).prop("disabled", true);
            }
        });
    },

    enableVariantSelectors: function () {
        $('.variant-select').prop("disabled", false);
    },

    bindToAddToCartForm: function () {
        var addToCartForm = $('.add_to_cart_form');
        addToCartForm.ajaxForm({
        	beforeSubmit:ACC.product.showRequest,
        	//success: ACC.product.displayAddToCartPopup
        	success: function() {
                var url = ACC.config.encodedContextPath + "/cart";
                window.location.href = url;
            }
         });   
         var addToCartFormPopup = $('.add_to_cart_form_popup');
         addToCartFormPopup.ajaxForm({
        	beforeSubmit:ACC.product.showRequest,
        	success: ACC.product.displayAddToCartPopup
        	// success: function() {
            //     var url = ACC.config.encodedContextPath + "/cart";
            //     window.location.href = url;
            // }
         }); 
        setTimeout(function(){
        	$ajaxCallEvent  = true;
         }, 2000);
     },
     showRequest: function(arr, $form, options) {  
    	 if($ajaxCallEvent)
    		{
    		 $ajaxCallEvent = false;
    		 return true;
    		}   	
    	 return false;
 
    },

    bindToAddToCartStorePickUpForm: function () {
        var addToCartStorePickUpForm = $('#colorbox #add_to_cart_storepickup_form');
        addToCartStorePickUpForm.ajaxForm({success: ACC.product.displayAddToCartPopup});
    },

    enableStorePickupButton: function () {
        $('.js-pickup-in-store-button').prop("disabled", false);
    },

    displayAddToCartPopup: function (cartResult, statusText, xhr, formElement) {
        var productExist  = cartResult.cartData.planExist;
        var newProdCat  = $(".product-main-info .product-details .add_to_cart_form_popup input[name='productCodePost']").val();
        if(productExist == "false") {
            var url = ACC.config.encodedContextPath + "/cart";
            window.location.href = url;
        }
        else {
            $ajaxCallEvent=true;
            $('#addToCartLayer').remove();
            if (typeof ACC.minicart.updateMiniCartDisplay == 'function') {
                ACC.minicart.updateMiniCartDisplay();
            }
            var titleHeader = $('#addToCartTitle').html();
            var popupWidth = 0;
            if($(window).width() <= 1023) {
                popupWidth = "95%";
            }
            else {
                popupWidth = "792px";
            }
            ACC.colorbox.open(titleHeader, {
                html: cartResult.addToCartLayer,
                width: popupWidth,
                close:'<span class="cartItemRemoval-popup-close">&times;</span>',
                onComplete: function() {
                    $("#cboxLoadedContent").css({
                        "margin-top": "30px"
                    });
                    $("#cboxContent").css({
                        "border-radius": "10px"
                    });
                    $("#cboxClose").css({
                        "top": "0"
                    });
                    if($(".page-productDetails").length) {
                        if(newProdCat.indexOf('FMC') == 0) {
                            $(".cartItemRemoval-popup form").find("button").addClass("addToCartFmcPopup");
                        }
                        else {
                            $(".cartItemRemoval-popup form").find("button").removeClass("addToCartFmcPopup");
                        }
                    }
                    
                    ACC.common.refreshScreenReaderBuffer();
                },
            });
            
            $(".cartItemRemoval-popup-close").on("click", function() {
                $.colorbox.close();
            })
            var productCode = $('[name=productCodePost]', formElement).val();
            var quantityField = $('[name=qty]', formElement).val();

            var quantity = 1;
            if (quantityField != undefined) {
                quantity = quantityField;
            }

            var cartAnalyticsData = cartResult.cartAnalyticsData;

            var cartData = {
                "cartCode": cartAnalyticsData.cartCode,
                "productCode": productCode, "quantity": quantity,
                "productPrice": cartAnalyticsData.productPostPrice,
                "productName": cartAnalyticsData.productName
            };
            ACC.track.trackAddToCart(productCode, quantity, cartData);
        }
    },    
    		showaddtocartpopupPostpaid: function() {
    		    //postpaid page popup desgin for panama theme
                $(document).on("click touchend", ".addcart-btn ,.postpaid-plp", function(e) {
                    e.preventDefault();
                    var popupHeadLine = $(".postpaid-cta-popup").prev(".headline").html();                
                    var _this = $(this);
                    if(this.className.includes('addcart-btn buynow-btn')){           	
                        $("#postpaid-cta-popup .cta-header").html(_this.parents('.livemore-col').find(".cta-header").html()); //price data
                        $("#postpaid-cta-popup .cta-prod-data .char-spec .planName").text(_this.attr('data-arrt-name')); // Item name
                        $("#postpaid-cta-popup .cta-prod-data ").html(_this.parents('.livemore-col').find(".cta-prod-data").html()); // Item description
                        $("#postpaid-cta-popup .postpaid-popup-btn-group #addToCartFormPostPaidPlp .productCodePost").val(_this.attr('data-attr-item-code')); //Item code
                    }
                    else{                	
                        $("#postpaid-cta-popup .cta-header").html(_this.find(".cta-header").html()); //price data
                        $("#postpaid-cta-popup .cta-prod-data .char-spec .planName").text(_this.find(".addcart-btn").attr('data-arrt-name')); // Item name
                        $("#postpaid-cta-popup .cta-prod-data ").html(_this.find(".cta-prod-data").html()); // Item description
                        $("#postpaid-cta-popup .postpaid-popup-btn-group #addToCartFormPostPaidPlp .productCodePost").val(_this.find(".addcart-btn").attr('data-attr-item-code')); //Item code
                    }
                    
                    var popupWidth = 0;
                    if($(window).width() <= 1024) { 
                        popupWidth = "95%";
                    }
                    else {
                        popupWidth = "35%";
                    }
                    $.colorbox({
                        maxWidth:"100%",
                        maxHeight:"100%",
                        inline: true,
                        width:popupWidth,
                        scrolling:true,
                        href: "#postpaid-cta-popup",
                        close:'<span class="glyphicon glyphicon-remove"></span>',
                        title:popupHeadLine,
                        onComplete: function() {
                            $("#cboxLoadedContent").css({
                                "margin-top": "30px"
                            });
                            ACC.common.refreshScreenReaderBuffer();
                        },
                        onClosed: function() {
                            ACC.common.refreshScreenReaderBuffer();
                        }
                    });
                });
                //Adding only plan to cart
               $(document).on("click", ".plp-onlyPlan-btn", function(e) {
                    e.preventDefault();
                    var buttonType = 'no';   
                    var postpaidcode = $(this).parents(".postpaid-cta-popup").find(".code").text();             
                    postPaidPlanAddToCart(buttonType, postpaidcode);
                });
                //adding mobile device to post paid plan
                $(document).on("click", ".btn-plan-mobile", function(e) {
                    e.preventDefault();
                    var buttonType = 'yes';
                    var postpaidcode = $(this).parents(".postpaid-cta-popup").find(".code").text();
                    postPaidPlanAddToCart(buttonType, postpaidcode);
                });
    		}    
};

$(document).ready(function () {
	$ajaxCallEvent = true;
    ACC.product.enableAddToCartButton();
    //selecting Mobile_35 on page load
    $(".plan-type").find('h4[class="Mobile_35"]').trigger('click');
    // setting equal height for all cta boxes
    var plpCtaboxMaxHeight = 0;
    var activeTab = $(".tabs-container").find(".active.js-tab-head").attr('data-content-id');
    $(".page-internettvtelephone").find($('#' + activeTab)).find(".plp-ctabox").each(function(){
        if ($(this).height() > plpCtaboxMaxHeight) { plpCtaboxMaxHeight = $(this).height(); }
    });
    $(".page-internettvtelephone").find($('#' + activeTab)).find(".plp-ctabox").height(plpCtaboxMaxHeight);    
});
/*For Device PDP*/
$(document).on("click", ".plan-type", function(e) {	
	var bundleInCart = $(this).find('input[type="radio"]').attr("data-bundleInCart");
	if(bundleInCart == "true") {
        $("#addToCartButton").prop("disabled", "true").addClass('disabled');
    }
    else {
        $("#addToCartButton").removeClass('disabled').removeAttr("disabled");
    }
	$(this).find('input[type="radio"]').prop('checked', true);	 
	
	var selectedRadioValue=$('input[name="deviceprice_radio"]:checked').val();
	var idSelectedId="#" + selectedRadioValue;
	var idSelectedClass="." + selectedRadioValue;
	 
	 $(".plan-type").removeClass("selected");
	 $("h4").removeClass("selected");
	 
	 $(idSelectedId).addClass('selected');
	 $(idSelectedClass).addClass('selected');	
	 
	 $("#deviceToolTip").addClass('hide');
	 $("#deviceplan").val(selectedRadioValue);	 
});
/*For PLP clickable-cta box*/
$(document).on("click", ".plp-ctabox, .postpaid-devices-plp", function(e) {
	 e.preventDefault();     
     if($(this).find("form").length > 0) {
        $(this).find(".cta-btn-block").find("form").submit();
     }  
     else { 
        if ($(this).find("a").length > 0) {
            var ctaHref= $(this).find(".text-center").find("a").attr('href');
            location.href = ctaHref;
        }
    }
});
$(".plp-ctabox .more-benefits").click(function(e) {
    e.stopPropagation();
});

$(document).ready(function() {
    var isPlanExist = $("#isPlanExist").val();
    if(isPlanExist == "true") {
        $(".page-productDetails .product-main-info .product-details .add_to_cart_form_popup button").removeClass("addToCartFmcPopup");
    }
    else {
        $(".page-productDetails .product-main-info .product-details .add_to_cart_form_popup button").addClass("addToCartFmcPopup"); 
    }
});
