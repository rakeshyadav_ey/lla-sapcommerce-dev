ACC.cartitem = {

	_autoload: [
		"bindCartItem"
	],

	submitTriggered: false,

	bindCartItem: function ()
	{

		$('.js-execute-entry-action-button').on("click", function ()
		{
			var entryAction = $(this).data("entryAction");
			var entryActionUrl =  $(this).data("entryActionUrl");
			var entryProductCode =  $(this).data("entryProductCode");
			var entryInitialQuantity =  $(this).data("entryInitialQuantity");
			var actionEntryNumbers =  $(this).data("actionEntryNumbers");

			if(entryAction == 'REMOVE')
			{
				ACC.track.trackRemoveFromCart(entryProductCode, entryInitialQuantity);
			}

			var cartEntryActionForm = $("#cartEntryActionForm");
			var entryNumbers = actionEntryNumbers.toString().split(';');
			entryNumbers.forEach(function(entryNumber) {
				var entryNumbersInput = $("<input>").attr("type", "hidden").attr("name", "entryNumbers").val(entryNumber);
				cartEntryActionForm.append($(entryNumbersInput));
			});
			cartEntryActionForm.attr('action', entryActionUrl).submit();
		});

		$('.qtyplus').on("click", function (e) {
		    $(this).prop('disabled', true);
			ACC.cartitem.handleUpdateQuantityPlus(this, e);
		});

		$('.qtyminus').on("click", function (e) {
		    $(this).prop('disabled', true);
			ACC.cartitem.handleUpdateQuantityMinus(this, e);
		});

		$('.js-update-entry-quantity-input').on("blur", function (e)
		{
			ACC.cartitem.handleUpdateQuantity(this, e);

		}).on("keyup", function (e)
		{
			if($(this).val() > $(this).parent().attr('data-qty-max')){
                $(this).val($(this).parent().attr('data-qty-max'));
            }else if($(this).val() == 0 && $(this).val() != ''){
                $(this).val(0);
            }else{
                return ACC.cartitem.handleKeyEvent(this, e);
            }
		}
		).on("keydown", function (e)
		{
			return ACC.cartitem.handleKeyEvent(this, e);
		}
		);
	},

	handleKeyEvent: function (elementRef, event)
	{
		//console.log("key event (type|value): " + event.type + "|" + event.which);

		if (event.which == 13 && !ACC.cartitem.submitTriggered)
		{
			ACC.cartitem.submitTriggered = ACC.cartitem.handleUpdateQuantity(elementRef, event);
			return false;
		}
		else 
		{
			// Ignore all key events once submit was triggered
			if (ACC.cartitem.submitTriggered)
			{
				return false;
			}
		}

		return true;
	},

	handleUpdateQuantity: function (elementRef, event)
	{

		var form = $(elementRef).closest('form');

		var productCode = form.find('input[name=productCode]').val();
		var initialCartQuantity = form.find('input[name=initialQuantity]').val();
		var newCartQuantity = form.find('input[name=quantity]').val();

		if(initialCartQuantity != newCartQuantity)
		{
			ACC.track.trackUpdateCart(productCode, initialCartQuantity, newCartQuantity);
			form.submit();

			return true;
		}

		return false;
	},

	handleUpdateQuantityPlus: function (elementRef, event)
	{

		var form = $(elementRef).closest('form');
		
		var productCode = form.find('input[name=productCode]').val();
		var initialCartQuantity = parseInt(form.find('input[name=initialQuantity]').val());
		var currentVal = parseInt(form.find('input[name=quantity]').val());
		var newCartQuantity = parseInt(form.find('input[name=quantity]').val(currentVal+1));

		if(initialCartQuantity != newCartQuantity)
		{
			ACC.track.trackUpdateCart(productCode, initialCartQuantity, newCartQuantity);
			sessionStorage.scrollFLag = "true";
			form.submit();

			return true;
		}

		return false;
	},

	handleUpdateQuantityMinus: function (elementRef, event)
	{

		var form = $(elementRef).closest('form');
		
		var productCode = form.find('input[name=productCode]').val();
		var initialCartQuantity = parseInt(form.find('input[name=initialQuantity]').val());
		var currentVal = parseInt(form.find('input[name=quantity]').val());
		var newCartQuantity = parseInt(form.find('input[name=quantity]').val(currentVal-1));
		
		if(initialCartQuantity != newCartQuantity)
		{
			ACC.track.trackUpdateCart(productCode, initialCartQuantity, newCartQuantity);
			sessionStorage.scrollFLag = "true";
			form.submit();

			return true;
		}

		return false;
	}
};

$(document).ready(function() {
    $('.js-cartItemDetailBtn').click(function(event) {
        event.stopPropagation();
        var thisDetailGroup =  $(this).parent('.js-cartItemDetailGroup');
        $(thisDetailGroup).toggleClass('open'); //only in its parent
        if ( $(thisDetailGroup).hasClass('open') )  {
            //close all if not this parent
            $('.js-cartItemDetailGroup').not( thisDetailGroup ).removeClass('open');
            //change aria
            $('.js-cartItemDetailBtn').attr('aria-expanded', 'true');

        } else {
            $('.js-cartItemDetailBtn').attr('aria-expanded', 'false');
        }
        $(document).click( function(){
            $(thisDetailGroup).removeClass('open');
        }); // closes when clicking outside this div
    });

    //enable comment for this item only
    $('.js-entry-comment-button').click(function(event) {
        event.preventDefault();
        var linkID = $(this).attr('href');
        $( linkID ).toggleClass('in');
        $( thisDetailGroup ).removeClass('open');
    });

    //scroll back to old position after addon qty change

    if (sessionStorage.scrollFLag !== undefined && sessionStorage.scrollTop !== undefined) {
        $(window).scrollTop(sessionStorage.scrollTop);
        sessionStorage.removeItem('scrollFLag');
    }
    $(window).scroll(function() {
      var position = $(this).scrollTop();
      sessionStorage.scrollTop = $(this).scrollTop();
    });

    $(".addonqtyplus").click(function(){
        $(this).prop('disabled', true);
        $(this).parent().parent().find("input[name='qty']").val(parseInt($(this).parent().find("#quantity").val()) + 1);
        var form = $(this).closest('form');
        sessionStorage.scrollFLag = "true";
        form.submit();
    });

   $('#addonAddtoCart #quantity').on("blur", function(e) {
        $(this).parent().parent().find("input[name='qty']").val($(this).val());
        ACC.cartitem.handleUpdateQuantity(this, e);
   }).on("keyup", function(e) {
       if ($(this).val() > $(this).parent().attr('data-qty-max')) {
           $(this).val($(this).parent().attr('data-qty-max'));
       } else if ($(this).val() == 0 && $(this).val() != '') {
           $(this).val(0);
       } else {
           return ACC.cartitem.handleKeyEvent(this, e);
       }
   }).on("keydown", function(e) {
       return ACC.cartitem.handleKeyEvent(this, e);
   });

   if(window.innerWidth > 1023 && $(".page-cartPage .cart-item-list-details").length <= 2){
       $(".cartPageAlert").removeClass('hide');
   }else{
       $(".cartPageAlert").removeClass('hide');
   }

});