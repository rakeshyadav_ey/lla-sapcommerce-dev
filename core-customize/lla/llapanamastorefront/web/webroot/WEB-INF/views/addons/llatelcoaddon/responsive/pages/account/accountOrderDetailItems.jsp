<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="telco-order" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/order"%>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:if test="${cmsSite.uid ne 'panama'}">
<c:if test="${cmsSite.uid eq 'panama'}">
	<div class="order-info row">
		<div class="col-sm-6 col-xs-12">
			<div class="order-billing">
				<h3><spring:theme code="text.account.order.billing"/></h3>
				<div class="order-billing-details">
					<order:addressItem address="${orderData.deliveryAddress}"/>
				</div>
			</div>
		</div>
		<input type="hidden" id="addressInfo"
		    <c:if test="${orderData.deliveryMode.code ne null}"> data-attr-shippingMethod="${orderData.deliveryMode.code}" </c:if>
		    <c:if test="${orderData.deliveryAddress.province ne null}"> data-attr-province="${orderData.deliveryAddress.province}" </c:if>
		    <c:if test="${orderData.deliveryAddress.district ne null}"> data-attr-district="${orderData.deliveryAddress.district}" </c:if>
		    <c:if test="${orderData.deliveryAddress.town ne null}"> data-attr-town="${orderData.deliveryAddress.town}" </c:if>
		    <c:if test="${orderData.deliveryAddress.neighbourhood ne null}"> data-attr-neighbourhood="${orderData.deliveryAddress.neighbourhood}" </c:if>
		    <c:if test="${orderData.deliveryAddress.streetname ne null}"> data-attr-street="${orderData.deliveryAddress.streetname}" </c:if>
		    <c:if test="${orderData.deliveryAddress.country.name ne null}"> data-attr-country="${orderData.deliveryAddress.country.name}" </c:if>
		/>
		<div class="col-sm-6 col-xs-12">
			<div class="bill-summary">
				<telco-order:accountOrderDetailOrderTotals order="${orderData}"/>
			</div>
		</div>
	</div>
</c:if>

<c:if test="${cmsSite.uid eq 'panama'}">
	<div class="col-xs-12">
</c:if>
<c:if test="${cmsSite.uid eq 'panama'}">
	<div class="account-orderdetail account-consignment">
		<c:if test="${not empty orderData.unconsignedEntries}">
			<c:if test="${cmsSite.uid eq 'panama'}">
				<div class="ordered-prod-details ${cmsSite.uid eq 'panama' ? 'row':''}">
					<h3><spring:theme code="text.account.order.purchase.details"/></h3>
			</c:if>
			<telco-order:orderUnconsignedEntries order="${orderData}" />
			<c:if test="${cmsSite.uid eq 'panama'}">
				<h4><spring:theme code="text.account.order.free.shipping"/></h4>
				</div>
			</c:if>
		</c:if>
		<c:forEach items="${orderData.consignments}" var="consignment">
			<c:if test="${consignment.status.code eq 'WAITING' or consignment.status.code eq 'PICKPACK' or consignment.status.code eq 'READY'}">
				<telco-order:consignmentStatus consignmentCode="${ycommerce:encodeHTML(consignment.status.code)}" orderData="${orderData}" consignment="${consignment}"	inProgress="true" />
			</c:if>
		</c:forEach>
		<c:forEach items="${orderData.consignments}" var="consignment">
			<c:if test="${consignment.status.code ne 'WAITING' and consignment.status.code ne 'PICKPACK' and consignment.status.code ne 'READY'}">
				<telco-order:consignmentStatus consignmentCode="${ycommerce:encodeHTML(consignment.status.code)}" orderData="${orderData}" consignment="${consignment}" />
			</c:if>
		</c:forEach>
	</div>
</c:if>
<c:if test="${cmsSite.uid eq 'panama'}">
	</div>
</c:if>

<c:if test="${cmsSite.uid eq 'panama'}">
	<div class="col-xs-12">
		<div class="order-terms row">
			<div class="row">
				<div class="col-sm-6">
					<ul>
						<c:choose>
							<c:when test="${isPrepaidOrder}"> 
								<li><spring:theme code="text.account.order.prepaid.terms.listitem1"/></li>
		 						<li><spring:theme code="text.account.order.prepaid.terms.listitem2"/></li>
							</c:when>
							<c:otherwise>
								<li><spring:theme code="text.account.order.terms.listitem1"/></li>
								<li><spring:theme code="text.account.order.terms.listitem2"/></li>
							</c:otherwise>
						</c:choose>
					</ul>
				</div>
				<div class="col-sm-6">
					<ul>
						<c:choose>
							<c:when test="${isPrepaidOrder}"> 
								<li><spring:theme code="text.account.order.prepaid.terms.listitem3"/></li>
								<li><spring:theme code="text.account.order.prepaid.terms.listitem4"/></li>
								<li><spring:theme code="text.account.order.prepaid.terms.listitem5"/></li>
							</c:when>
							<c:otherwise>
								<li><spring:theme code="text.account.order.terms.listitem3"/></li>
								<li><spring:theme code="text.account.order.terms.listitem4"/></li>
								<li><spring:theme code="text.account.order.terms.listitem5"/></li>
							</c:otherwise>
						</c:choose>
					</ul>
				</div>
			</div>
		</div>
		
		<div class="order-purchase-types row"> 
			<h3><spring:theme code="text.account.order.purchase.headline"/></h3>
			<ul class="row">
				<li class="col-xs-12 col-sm-4">
					<span><spring:theme code="text.account.order.purchase.type1.headline"/></span>
					<spring:theme code="text.account.order.purchase.type1"/>
				</li>
				<li class="col-xs-12 col-sm-4">
					<span><spring:theme code="text.account.order.purchase.type2.headline"/></span>
					<spring:theme code="text.account.order.purchase.type2"/>
				</li>
				<li class="col-xs-12 col-sm-4">
					<span><spring:theme code="text.account.order.purchase.type3.headline"/></span>
					<spring:theme code="text.account.order.purchase.type3"/>
				</li>
			</ul>
		</div>
	</div>
</c:if>
</c:if>