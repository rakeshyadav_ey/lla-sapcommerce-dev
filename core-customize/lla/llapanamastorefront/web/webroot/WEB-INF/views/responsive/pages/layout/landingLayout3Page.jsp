<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<template:page pageTitle="${pageTitle}">
   <c:url value="/cart/add" var="addToCart"/>
   <div class="livemore-section row residential-plp">
      <div class="container">
         <div class="process-selection-bar">
            <ul class="selection-state">
               <li class="active col-xs-4"><p class="level"><span class="process-state">1</span><span class="process-info"><spring:theme code="selection.level1.text"/></span></p></li>
               <li class="col-xs-4">
                  <c:choose>
                        <c:when test="${fn:length(cartData.entries) > 0}">
                           <a href="${request.contextPath}/cart"><p class="level"><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></p>
                           </a>
                        </c:when>
                        <c:otherwise>
                           <p class="level"><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></p>
                        </c:otherwise>
                    </c:choose>
               </li>
               <li class="col-xs-4"><p class="level"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level3.text"/></span></p>
               </li>
            </ul>
         </div>
         <div class="headline">
            <spring:theme code="text.residential.bundle.headline" htmlEscape="false"/>
         </div>
         <div class="sub-headline">Selecciona el plan que se ajusta a tus necesidades.</div>
      </div>
      <div class="tabs-container">
         <div class="container">
            <ul>
               <c:forEach items="${categoriesList}" var="category">
                  <li class="${category eq activeCategoryName ? 'active':''} js-tab-head" data-content-id="${category}">
                     <c:if test="${category eq '2PInternetTv'}">
                        <span class="wifi-icon"></span>
                        <span class="tv-icon"></span>
                        <div class="bundle-name">
                           <spring:theme code="text.residential.bundle.category.${category}" htmlEscape="false"/>
                        </div>
                     </c:if>
                     <c:if test="${category eq '2PInternetTelephone'}">
                        <span class="wifi-icon"></span>
                        <div class="bundle-name">
                           <spring:theme code="text.residential.bundle.category.${category}" htmlEscape="false"/>
                        </div>
                     </c:if>
                     <c:if test="${category eq '3Pbundles'}">
                        <span class="wifi-icon"></span>
                        <span class="tv-icon"></span>
                        <span class="landphone-icon"></span>
                        <div class="bundle-name">
                           <spring:theme code="text.residential.bundle.category.${category}" htmlEscape="false"/>
                        </div>
                     </c:if>
                     <c:if test="${category eq '4Pbundles'}">
                        <span class="wifi-icon"></span>
                        <span class="tv-icon"></span>
                        <span class="landphone-icon"></span>
                        <span class="mobile-icon"></span>
                        <div class="bundle-name">
                           <spring:theme code="text.residential.bundle.category.${category}" htmlEscape="false"/>
                        </div>
                     </c:if>
                  </li>
               </c:forEach>
            </ul>
         </div>
      </div>
      <div class="cta-wrapper">
         <c:forEach items="${allProductDatas.entrySet()}" var="entry">
            <c:forEach items="${categoriesList}" var="category">
               <c:if test="${entry.key eq category}">
                  <div class="tab-content-wrapper container ${category eq activeCategoryName ? '':'display-none'}" id="${entry.key}">
                     <div class="cta-container ${productData.promoFlag eq 'true' ? 'promo-more':''}">
                        <c:forEach items="${entry.value}" var="productData" varStatus="productIndex">
                           <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                              <div class="livemore-col plp-ctabox ${productData.promoFlag eq 'true' ? 'PromotionflagBorder':''}">
                              <c:if test="${productData.promoFlag eq 'true'}">
                                     <div class="Promotionflag">
                                        ${productData.promotionMessage}
                                     </div>
                                 </c:if>
                                 <div class="cta-prod-data">
                                    <div class="char-spec spec-opts cta-content-spec">
                                       <c:if test="${productData.promoFlag eq 'true'}">
                                          <div class="cta-promo">
                                             <p>
                                                <spring:theme code="panama.plp.promoOnline"/>
                                             </p>
                                          </div>
                                       </c:if>
                                        <div class="name">
                                       	 <p class="Prodname"> ${productData.name}</p>
                                       	 <p class="Prodprimary"> ${productData.subTitle1}</p>
                                       	 <p class="Prodsecondary"> ${productData.subTitle2}</p>
                                       </div>
                                       <table>
                                          <tbody>
                                             <c:forEach var="specCharValue" items="${productData.productSpecCharValueUses}">
                                                <c:forEach items="${specCharValue.productSpecCharacteristicValues}" var="spcchar">
                                                   <c:if test="${spcchar.productSpecCharacteristic ne 'online_promo'}">
                                                      <tr>
                                                         <td>
                                                            <div class="${spcchar.productSpecCharacteristic} char-spec-col">
                                                               <div class="spec-description">
                                                                  <div class="spec-heading">
                                                                     <span>${spcchar.value}
                                                                     </span>
                                                                     <span>${spcchar.unitOfMeasurment}</span>
                                                                     <c:if test="${spcchar.gratisFlag eq 'true'}">
                                                                         ${spcchar.gratisImageURL}
                                                                       </c:if>
                                                                     <span class="spec-text">${spcchar.ctaSpecDescription}</span>
                                                                     <c:if test="${spcchar.channelFlag eq 'true'}">
                                                                        ${spcchar.channelImageURL}
                                                                     </c:if>

                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </td>
                                                      </tr>
                                                   </c:if>
                                                </c:forEach>
                                             </c:forEach>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <div class="cta-header">
                                    <div class="cta-onlinepromo char-spec" style="padding-top: 0;">
                                       <table>
                                          <tbody>
                                             <c:forEach var="specCharValue" items="${productData.productSpecCharValueUses}">
                                                <c:forEach items="${specCharValue.productSpecCharacteristicValues}" var="spcchar">
                                                   <c:if test="${spcchar.productSpecCharacteristic eq 'online_promo'}">
                                                      <tr>
                                                         <td>
                                                            <div class="${spcchar.productSpecCharacteristic} char-spec-col">
                                                               <div class="spec-description">
                                                                  <div class="spec-heading">
                                                                     <span>${spcchar.value}</span>
                                                                     <span>${spcchar.unitOfMeasurment}</span>
                                                                     <span class="spec-text">${spcchar.ctaSpecDescription}</span>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </td>
                                                      </tr>
                                                   </c:if>
                                                </c:forEach>
                                             </c:forEach>
                                          </tbody>
                                       </table>
                                    </div>
                                    <div class="cta-header-text">
                                       <div class="bundle">
                                          <c:forEach var="offeringPrices" items="${productData.productOfferingPrices}" varStatus="productIndex">
                                             <c:if test="${null ne offeringPrices.recurringChargeEntries}">
                                                <c:set var="recurringCharges" value="${offeringPrices.recurringChargeEntries[0]}"/>
                                             </c:if>
                                          </c:forEach>
                                          <c:choose>
                                             <c:when test="${recurringCharges.discountedPrice ne null}">
                                                <p class="oldPrice">
                                                   <spring:theme code="panama.plp.antes"/>
                                                   &nbsp;
                                                   <span class="slashedPrice">
                                                      <format:price priceData="${productData.monthlyPrice}"/>
                                                   </span>
                                                </p>
                                                <c:if test="${recurringCharges.discountedPrice.discountPeriod ne null}">
                                                    <p class="plp-description">${recurringCharges.discountedPrice.discountPeriod}</p>
                                                 </c:if>
                                                <p class="newPrice">${recurringCharges.discountedPrice.formattedValue}</p>
                                                <p class="description">
                                                       <span>
                                                          <spring:theme code="text.ctaMonthlyplan"/>
                                                       </span>
                                                </p>
                                             </c:when>
                                             <c:otherwise>
                                                <fmt:setLocale value="en_US" scope="session"/>
                                                <p class="newPrice">
                                                   <span>
                                                      ${currentCurrency.symbol}
                                                      <fmt:formatNumber value="${productData.monthlyPrice.value}" minFractionDigits="2" maxFractionDigits="2" pattern="########.##" />
                                                   </span>
                                                </p>
                                                <p class="description">
                                                   <span>
                                                      <spring:theme code="text.ctaMonthlyplan"/>
                                                   </span>
                                                </p>
                                             </c:otherwise>
                                          </c:choose>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="cta-btn-block text-center">
                                    <c:choose>
                                       <c:when test="${category eq '4Pbundles'}">
                                          <a class="buynow-btn" href="${contextPath}/${currentLanguage.isocode}${productData.url}">
                                             <spring:theme code="panama.plp.information.url"/>
                                          </a>
                                       </c:when>
                                       <c:otherwise>
                                          <form id="addToCartButton"  class="add_to_cart_form_popup" action="${addToCart}" method="post">
                                             <input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
                                             <input type="hidden" name="productCodePost" id="productCodePost" value="${productData.code}">
                                             <input type="hidden" name="processType" value="DEVICE_ONLY">
                                             <button type="submit" class="buynow-btn" id="buynow_${index.count}"
                                                data-attr-item-code="${productData.code}"
                                                data-attr-id="${productData.code}"
                                                data-attr-price="${productData.monthlyPrice.value}"
                                                data-arrt-name="${productData.name}"
                                                data-arrt-desc="${productData.description}"
                                                data-attr-category="${activeCategoryName}" data-attr-category_2="${category}">
                                                <spring:theme code="panama.plp.information.url"/>
                                             </button>
                                          </form>
                                       </c:otherwise>
                                    </c:choose>
                                 </div>                                 
                              </div>
                              <c:if test="${category eq '2PInternetTelephone' || category eq '2PInternetTv' || category eq '3Pbundles'}">
                                    <c:if test="${category eq '2PInternetTelephone'}">
                                        <cms:pageSlot position="1P_plans_Slot" var="feature">
                                            <cms:component component="${feature}" element="div" class="no-space yComponentWrapper col-xs-12 ${productData.promoFlag eq 'true' ? 'promo-more':''}"/>
                                        </cms:pageSlot>
                                    </c:if>
                                    <c:if test="${category eq '2PInternetTv'}">
                                        <cms:pageSlot position="2P_plans_Slot" var="feature">
                                            <cms:component component="${feature}" element="div" class="no-space yComponentWrapper col-xs-12 ${productData.promoFlag eq 'true' ? 'promo-more':''}"/>
                                        </cms:pageSlot>
                                    </c:if>
                                    <c:if test="${category eq '3Pbundles'}">
                                        <cms:pageSlot position="3P_plans_Slot" var="feature">
                                            <cms:component component="${feature}" element="div" class="no-space yComponentWrapper col-xs-12 ${productData.promoFlag eq 'true' ? 'promo-more':''}"/>
                                        </cms:pageSlot>
                                    </c:if>
                                 </c:if>
                           </div>
                        </c:forEach>
                     </div>
                  </div>
               </c:if>
            </c:forEach>
         </c:forEach>
      </div>
   </div>
</template:page>