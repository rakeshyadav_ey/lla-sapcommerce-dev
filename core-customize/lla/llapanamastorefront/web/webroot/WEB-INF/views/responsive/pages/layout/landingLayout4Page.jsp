<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="plan-devices" tagdir="/WEB-INF/tags/responsive/landinglayout4"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">
    <div class="livemore-section row residential-plp">
        <div class="container">
            <div class="process-selection-bar">
                <ul class="selection-state">
                    <c:choose>
                        <c:when test="${activeCategoryName eq 'postpaid'}">
                        <c:set var="category" value="${devices}"/>
                        <li class="col-xs-3 selectedTab1"><a href="${request.contextPath}/residential/plans-Devices-plp?selectedCategory=${category}"><p class="level"><span class="process-state">1</span><span class="process-info"><spring:theme code="selection.level1.text.device"/></span></p></a></li>
                        </c:when>
                    <c:otherwise>
                    <li class="col-xs-3 selectedTab1"><p class="level"><span class="process-state">1</span><span class="process-info"><spring:theme code="selection.level1.text.device"/></span></p></li>
                    </c:otherwise>
                    </c:choose>
                    <li class="col-xs-3 selectedTab2">
                        <c:choose>
                         <c:when test="${fn:length(cartData.entries) > 0}">
                            <c:choose>
                                <c:when test="${not empty lastProductCode}">
                                    <a href="${contextPath}/${currentLanguage.isocode}/p/${lastProductCode}"> <p class="level "><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text.device"/></span></p>
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <a href="${request.contextPath}/residential/plans-Devices-plp?selectedCategory=postpaid"> <p class="level "><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text.device"/></span></p>
                                    </a>
                                </c:otherwise>
                            </c:choose>
                         </c:when>
                         <c:otherwise>
                            <p class="level "><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text.device"/></span></p>
                         </c:otherwise>
                        </c:choose>
                    </li>
                    <li class="level col-xs-3">
                        <c:choose>
                            <c:when test="${fn:length(cartData.entries) > 0}">
                               <a href="${request.contextPath}/cart"><p class="level"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></p>
                               </a>
                            </c:when>
                            <c:otherwise>
                               <p class="level"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></p>
                            </c:otherwise>
                        </c:choose>
                    </li>
                    <li class="col-xs-3"><p class="level"><span class="process-state">4</span><span class="process-info"><spring:theme code="selection.level3.text"/></span></p></li>
                </ul>
            </div>

            <div class="postpaid-only">
                <div class="headline"><spring:theme code="text.fmc.bundle.headline.postpaid" htmlEscape="false"/></div>
                  <p class="sub-headline"><spring:theme code="text.fmc.bundle.subheadline.postpaid" htmlEscape="false"/></p> 
                   <cms:pageSlot position="Section1-BannerSlotName" var="feature" >
					   <cms:component component="${feature}" element="div"	class="yComponentWrapper col-xs-12 col-sm-12 postpaidBanner" />
				   </cms:pageSlot>
             </div>
            <div class="devices-only">
                <div class="headline"><spring:theme code="text.fmc.bundle.headline" htmlEscape="false"/></div>
                <cms:pageSlot position="Section1-BannerSlotName" var="feature" >
					<cms:component component="${feature}" element="div"	class="yComponentWrapper col-xs-12 col-sm-12 postpaidBanner" />
				</cms:pageSlot>
				 <cms:pageSlot position="Section2-BannerSlotName" var="feature">
					<cms:component component="${feature}" element="div"	class="yComponentWrapper col-xs-12 col-sm-12 deviceBanner" />
				</cms:pageSlot>
            </div>
        </div>
        <div class="tabs-container hide">
            <div class="container">
                <ul>
                    <c:forEach items="${categoriesList}" var="category">
                        <li class="${category eq activeCategoryName ? 'active':''} js-tab-head" data-content-id="${category}">
                            <c:if test="${category eq 'postpaid'}">
                                <span class="simcard-icon"></span>
                                <div class="bundle-name"><spring:theme code="text.residential.bundle.category.${category}" htmlEscape="false"/></div>
                            </c:if>
                            <c:if test="${category eq 'devices'}">
                                <span class="mobile-icon"></span>
                                <span class="simcard-icon"></span>
                                <div class="bundle-name"><spring:theme code="text.residential.bundle.category.${category}" htmlEscape="false"/></div>
                            </c:if>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </div>
        
        <plan-devices:postpaidDevicesTabContent />
        
    </div>

</template:page>

<c:if test="${cmsSite.uid eq 'panama'}">

<!-- popup design for postpaid plan selection -->
<div class="hide">
	<div id="postpaid-cta-popup" class="postpaid-cta-popup">
		<div class='headline'>
			<p class='headline-text'><spring:theme code="text.chooseplan" text="You choose a"/></p> 
		</div>
        	<div class="cta-prod-data"></div>
		<div class="cta-header"></div>
		<p class="plp-decision-text"><spring:theme code="plp.cta.plan.decision.text" text="Do you want a new Smartphone?"/></p>
		<div class="postpaid-popup-btn-group row">
            <div class="col-xs-12 text-center">
				<button class="btn-custom plp-pop-btn btn-plan-mobile" id="js-deviceFromPostpaid">
	                <spring:theme code="plp.cta.phone.text" text="iyes, I WANT A NEW smartphone!"/>
	            </button>
			</div>
			<div class="col-xs-12">
				<div class="text-center">
					<form id="addToCartFormPostPaidPlp" class="add_to_cart_form" method="post">
						<input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
						<input type="hidden" name="productCodePost" class="productCodePost" value="">
						<input type="hidden" name="processType" value="DEVICE_ONLY">
						<button type="submit" class="buynow-btn plp-onlyPlan-btn plp-pop-btn btn-white" id="buynow_${index.count}" >
							<spring:theme code="plp.cta.plan.text" text="NO, I just want the PLAN"/>
						</button>
					</form>
				</div>
			</div>			
		</div>
	</div>
</div>
</c:if>



