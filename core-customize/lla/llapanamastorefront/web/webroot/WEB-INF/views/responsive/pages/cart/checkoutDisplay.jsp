<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<spring:htmlEscape defaultHtmlEscape="true" />
<c:url value="/login/checkout/doguestcheckout" var="checkoutUrl" scope="session"/>
<!-- <div class="row">
    <div class="col-xs-12 col-sm-10 col-md-7 col-lg-6 pull-right cart-actions--print">
        <div class="express-checkout">
            <div class="headline"><spring:theme code="text.expresscheckout.header"/></div>
            <strong><spring:theme code="text.expresscheckout.title"/></strong>
            <ul>
                <li><spring:theme code="text.expresscheckout.line1"/></li>
                <li><spring:theme code="text.expresscheckout.line2"/></li>
                <li><spring:theme code="text.expresscheckout.line3"/></li>
            </ul>
            <sec:authorize access="isFullyAuthenticated()">
                <c:if test="${expressCheckoutAllowed}">
                    <div class="checkbox">
                        <label>
                            <c:url value="/checkout/multi/express" var="expressCheckoutUrl" scope="session"/>
                            <input type="checkbox" class="express-checkout-checkbox" data-express-checkout-url="${fn:escapeXml(expressCheckoutUrl)}">
                            <spring:theme text="I would like to Express checkout" code="cart.expresscheckout.checkbox"/>
                        </label>
                     </div>
                </c:if>
           </sec:authorize>
        </div>
    </div>
</div> -->

<div class="cart__actions">
    <div class="row">

       <form id="guestCheckout" action="${guestCheckoutUrl}" method="post">
        <c:set var="mobilePlanExists" value="false"/>
        <c:set var="deviceExists" value="false"/>
       <c:forEach var="entry" items="${cartData.entries}">      
                    <c:if test="${entry.product.categories[0].code eq 'postpaid' || entry.product.categories[0].code eq '4Pbundles' || entry.product.categories[0].code eq 'prepaid'}">
                        <c:set var="mobilePlanExists" value="true"/>
                    </c:if>
                  <c:if test="${deviceExists ne true}">
					<c:forEach var="category" items="${entry.product.categories}">
						<c:if test="${category.code eq 'devices'}">
							<c:set var="deviceExists" value="true" />
						</c:if>
					</c:forEach>
				</c:if>
                </c:forEach>
            <c:if test="${mobilePlanExists eq true || deviceExists eq true}">
              <div class="buying-options">                              
                <c:if test="${mobilePlanExists eq true}">
                    <div class="buying-options-list">
                        <p><spring:theme code="text.postpaid.type.question"/></p>
                        <c:forEach var="planType" items="${planTypes}" varStatus="index">
                            <label class="radio-inline custom-label">${planType.name}
                                <input id="planType${index.count}" name="planType" type="radio" value="${planType.code}" <c:if test="${index.count eq 1}">checked</c:if> />
                                <span class="checkmark"></span>
                            </label>
                        </c:forEach>
                        <div class="buying-options-note1">
                        	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><span class="infoIcon"></span></div>
                        	<div class="note-info-content col-sm-11 col-sm-11 col-sm-11 col-xs-11">
                        		<span class="note-info"><spring:theme code="cart.buying.note1.info"/></span>
                    		</div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${deviceExists eq true}">
                    <div class="buying-options-list">
                        <p><spring:theme code="text.device.payment.option.question"/></p>
                        <c:forEach var="devicePaymentOption" items="${devicePaymentOptions}" varStatus="index">
                            <label class="radio-inline custom-label">${devicePaymentOption.name}
                                <input id="devicePaymentOption${index.count}" name="devicePaymentOption" type="radio" value="${devicePaymentOption.code}" <c:if test="${index.count eq 1}">checked</c:if>>
                                <span class="checkmark"></span>
                            </label>    
                        </c:forEach>
                        <div class="buying-options-note2">
                        	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><span class="infoIcon"></span></div>
                        	<div class="note-info-content col-sm-11 col-sm-11 col-sm-11 col-xs-11">
                        		<span class="note-info"><spring:theme code="cart.buying.note2.info"/></span>
                    		</div>
                        </div>
                    </div>
                </c:if>
              </div>
              </c:if>
            
         <div class="col-sm-5 col-md-5 pull-right checkout-button">
           <p class="headline"><spring:theme code="add.information.to.continue" text="Ingresa los siguientes datos para continuar" />
            <div class="form-group clearfix">
                <label class="control-label" for="guestCheckoutEmail"><spring:theme code="customer.additionalEmail"/></label>
                <input type="email" class="form-control" id="guestEmail" value="${user.additionalEmail}" name="guestEmail"/>
             </div>  
             <div class="form-group clearfix"> 
                <label class="control-label" for="guestMobile"><spring:theme code="customer.panama.mobilePhone"/></label>
                 <input type="mobilePhone" class="form-control" id="mobilePhone" value="${user.mobilePhone}" name="mobilePhone" maxlength="8"/>
            </div>
            <div class="form-group clearfix text-center"> 
                 <button type="button" class="guestCheckoutButton btn btn-primary"><spring:theme code="checkout.checkout"/></button>
                <input type="hidden" id="guestEmail_error" value="<spring:theme code="guest.checkout.invalid"/>">
                <input type="hidden" id="guestPhone_length_error" value="<spring:theme code="guest.checkout.length.invalid.phonenumber"/>">
            </div>
            </div>
        </form>
        <input type="hidden" id="gtmCheckoutFormError" value="<spring:theme code="gtm.cartInfo.form.errors.msg"/>" >

        <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
            <c:if test="${not empty siteQuoteEnabled and siteQuoteEnabled eq 'true'}">
                <div class="col-sm-4 col-md-3 col-md-offset-3 pull-right">
                    <button class="btn btn-default btn-block btn--continue-shopping js-continue-shopping-button"    data-continue-shopping-url="${fn:escapeXml(createQuoteUrl)}">
                        <spring:theme code="quote.create"/>
                    </button>
                </div>
            </c:if>
        </sec:authorize>

        <div class="col-sm-7 col-md-7 pull-right">
            <button class="btn btn-default btn-block btn--continue-shopping js-continue-shopping-button" data-continue-shopping-url="${fn:escapeXml(continueShoppingUrl)}">
                <spring:theme code="cart.page.continue"/>
            </button>
        </div>
    </div>
</div>


<!-- <c:if test="${showCheckoutStrategies && not empty cartData.entries}" >
    <div class="cart__actions">
        <div class="row">
            <div class="col-xs-12 col-sm-5 col-md-3 col-lg-2 pull-right">
                <input type="hidden" name="flow" id="flow"/>
                <input type="hidden" name="pci" id="pci"/>
                <select id="selectAltCheckoutFlow" class="doFlowSelectedChange form-control">
                    <option value="select-checkout"><spring:theme code="checkout.checkout.flow.select"/></option>
                    <option value="multistep"><spring:theme code="checkout.checkout.multi"/></option>
                    <option value="multistep-pci"><spring:theme code="checkout.checkout.multi.pci"/></option>
                </select>
                <select id="selectPciOption" class="display-none">
                    <option value=""><spring:theme code="checkout.checkout.multi.pci.select"/></option>
                    <c:if test="${!isOmsEnabled}">
                        <option value="default"><spring:theme code="checkout.checkout.multi.pci-ws"/></option>
                        <option value="hop"><spring:theme code="checkout.checkout.multi.pci-hop"/></option>
                    </c:if>
                    <option value="sop"><spring:theme code="checkout.checkout.multi.pci-sop" text="PCI-SOP" /></option>
                </select>
            </div>
        </div>
    </div>
</c:if> -->
