<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<template:page pageTitle="${pageTitle}">

<div class="container">
	<cart:cartValidation/>
	<cart:cartPickupValidation/>

    <c:forEach items="${cartData.entries}" var="entry">
        <c:if test="${entry.product.categories[0].code ne 'devices'}">
            <c:set var="category" value="${entry.product.categories[0].code}"/>
            <c:set var="product" value="${entry.product}"/>
        </c:if>
    </c:forEach>

	<!-- <div class="cart-top-bar"> 
        <div class="text-right">
            <spring:theme var="textHelpHtml" code="text.help" />
            <a href="" class="help js-cart-help" data-help="${fn:escapeXml(textHelpHtml)}">${textHelpHtml}
                <span class="glyphicon glyphicon-info-sign"></span>
            </a>
            <div class="help-popup-content-holder js-help-popup-content">
                <div class="help-popup-content">
                    <strong>${fn:escapeXml(cartData.code)}</strong>
                    <spring:theme var="cartHelpContentVar" code="basket.page.cartHelpContent" htmlEscape="false" />
                    <c:set var="cartHelpContentVarSanitized" value="${ycommerce:sanitizeHTML(cartHelpContentVar)}" />
                    <div>${cartHelpContentVarSanitized}</div>
                </div>
            </div>
		</div>
	</div> -->

   
        <c:if test="${not empty cartData.rootGroups}">
            <c:if test="${cmsSite.uid eq 'panama'}">
                <div class="process-selection-bar">
                    <ul class="selection-state">
                        <c:choose>
                            <c:when test="${(deviceProduct eq 'true' && category eq 'postpaid') || (deviceProduct eq 'true' && category eq '4Pbundles') || (deviceProduct eq 'true' && category eq 'addon')}">
                                <li class="col-xs-3 ${(deviceProduct eq 'true' && lastCategoryCode eq 'devices') || (deviceProduct eq 'true' && category eq 'addon') ? 'previous':'disable'}"><p class="level">
                                    <c:choose>
                                    <c:when test="${lastCategoryCode eq 'devices' && deviceProduct eq 'true'}">
                                        <a href="${request.contextPath}/residential/plans-Devices-plp?selectedCategory=${lastCategoryCode}">
                                    </c:when>
                                    <c:when test="${onlyDevice eq 'devices'}">
                                        <a href="${request.contextPath}/residential/plans-Devices-plp?selectedCategory=devices">
                                    </c:when>
                                    <c:when test="${empty onlyDevice}">
                                        <a href="${request.contextPath}/residential/plans-Devices-plp?selectedCategory=devices">
                                    </c:when>
                                    </c:choose>
                                    <span class="process-state">1</span><span class="process-info"><spring:theme code="selection.level1.text.device"/></span>
                                    <c:if test="${empty onlyDevice || (lastCategoryCode eq 'devices' && category ne '4Pbundles') || (onlyDevice eq 'devices')}">
                                        </a>
                                    </c:if></p>
                                </li>    
                                <li class="col-xs-3 previous"><p class="level">
                                <c:choose>
                                    <c:when test="${deviceProduct eq 'true' && lastCategoryCode eq 'devices' || category eq 'addon' && deviceProduct eq 'true'}">
                                        <a href="${contextPath}/${currentLanguage.isocode}/p/${lastProductCode}" class="product__list--thumb prod-img"><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text.device"/></span></a>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="${request.contextPath}/residential/plans-Devices-plp?selectedCategory=${category}"><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text.device"/></span></a>
                                    </c:otherwise>
                                </c:choose></p>
                                </li>
                                <li class="col-xs-3 active"><p class="level"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></p></li>
                                <li class="col-xs-3"> <p class="level"><span class="process-state">4</span><span class="process-info"><spring:theme code="selection.level3.text"/></span></p></li>
                            </c:when>                            
                            <c:otherwise>
                                <li class="previous col-xs-4"><p class="level">
                                <c:choose>
                                     <c:when test="${category eq 'addon' && lastCategoryCode eq 'devices'}">
                                        <a href="${request.contextPath}/residential?selectedCategory=${fmcCategoryCode}">
                                    </c:when>
                                    <c:when test="${category eq 'addon' && lastCategoryCode ne 'devices'}">
                                      <a href="${request.contextPath}/residential?selectedCategory=${lastCategoryCode}">   
                                    </c:when>
                                    <c:otherwise>
                                        <a href="${request.contextPath}/residential?selectedCategory=${category}">
                                    </c:otherwise>
                                </c:choose>
                                <span class="process-state">1</span><span class="process-info"><spring:theme code="selection.level1.text"/></span></a></p></li>
                                <li class="active col-xs-4"><p class="level"><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></p></li>
                                <li class="col-xs-4"><p class="level"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level3.text"/></span></p></li>
                            </c:otherwise>
                        </c:choose>
                    </ul>
                </div>
            </c:if>
            <c:if test="${cmsSite.uid ne 'panama'}">
            <div class="global-alerts">
                <div class="alert alert-info alert-dismissable getAccAlert">
                    <spring:theme code="text.amount.paid.message" text="Monthly payment" htmlEscape="false" />
                </div>
            </div>
            </c:if>
            <div class="cart-header col-xs-12"> 
                <h1><spring:theme code="text.cart" text="My Shopping Cart" /></h1>
                <p><spring:theme code="text.cart.message.panama" text="Products added to you cart" /></p>
            </div>
        </c:if>
        <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-6">
        <cms:pageSlot position="TopContent" var="feature">
            <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
        </cms:pageSlot>
        </div>
	   <c:if test="${not empty cartData.rootGroups}">
           <cms:pageSlot position="CenterLeftContentSlot" var="feature">
                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
           </cms:pageSlot>
        </c:if>
		<div class="col-xs-12 col-sm-12 col-md-6">
		 <c:if test="${not empty cartData.rootGroups}">
            <div class="cart-right">
            <cms:pageSlot position="CenterRightContentSlot" var="feature">
                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
            </cms:pageSlot>
            <cms:pageSlot position="BottomContentSlot" var="feature">
                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
            </cms:pageSlot>            
            </div>
            <!--<div class="global-alerts">
                <div class="alert alert-info alert-dismissable getAccAlert">
                    <spring:theme code="text.amount.paid.before.service" />
                </div>
            </div>-->
		</c:if>
        </div>	
        </div>	
		<c:if test="${empty cartData.rootGroups}">
            <cms:pageSlot position="EmptyCartMiddleContent" var="feature">
                <cms:component component="${feature}" element="div" class="yComponentWrapper content__empty"/>
            </cms:pageSlot>
		</c:if>

    <div class="row">
        <div class="col-xs-12">
            <c:if test="${not empty cartData.rootGroups}">
            <div class="global-alerts cartPageAlert hide">
                <div class="alert alert-warning alert-dismissable getAccAlert">
                    <spring:theme code="text.for.cash.on.delivery" htmlEscape="false"/>
                </div>
            </div>
            </c:if>
        </div>
   </div>
</div>
</template:page>
