<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="telco-order" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/order"%>
<%@ taglib prefix="multi-checkout-telco" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<spring:url value="/login/register/termsandconditions" var="getTermsAndConditionsUrl" htmlEscape="false"/>
<c:if test="${cmsSite.uid eq 'panama'}">
    <c:forEach items="${orderData.entries}" var="entry">
        <c:if test="${entry.product.categories[0].code ne 'devices'}">
            <c:set var="category" value="${entry.product.categories[0].code}"/>
        </c:if>
    </c:forEach>
</c:if>
<div>
    <div class="container order-confirmation-page-section">
        <c:if test="${cmsSite.uid eq 'panama'}">
             <div class="process-selection-bar">
                <ul class="selection-state clearfix">
                    <c:choose>
                        <c:when test="${(deviceProduct eq 'true' && category eq 'postpaid') || (deviceProduct eq 'true' && category eq '4Pbundles') || (deviceProduct eq 'true' && category eq 'addon')}">
                            <li class="col-xs-3 ${(deviceProduct eq 'true' && lastCategoryCode eq 'devices') || (deviceProduct eq 'true' && category eq 'addon') ? 'previous':'disable'}"><p class="level"><span class="process-state">1</span><span class="process-info"><spring:theme code="selection.level1.text.device"/></span></p></li>    
                            <li class="col-xs-3 previous"><p class="level">
                            <span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text.device"/></span></p></li>
                            <li class="col-xs-3 previous"><p class="level"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></p></li>
                            <li class="col-xs-3 active"> <p class="level"><span class="process-state">4</span><span class="process-info"><spring:theme code="selection.level3.text"/></span></p></li>
                        </c:when>                            
                        <c:otherwise>
                            <li class="previous col-xs-4"><p class="level"><span class="process-state">1</span><span class="process-info"><spring:theme code="selection.level1.text"/></span></p></li>
                            <li class="col-xs-4 previous"><p class="level"><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></p></li>
                            <li class="col-xs-4 active"><p class="level"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level3.text"/></span></p></li>
                        </c:otherwise>
                    </c:choose>                     
                </ul>
            </div>
        </c:if>
       
        <div class="order-confirmation row">        
            <!-- <div>
                <p class="order-confirmation-title">
                    <spring:theme code="orderconfirmation.details.of.purchase" />
                </p>
            </div> -->

            <div class="col-sm-12 col-md-6 col-lg-6 left-content">
                <div class="purchase-request">
                    <div class="order-desc-content">
                        <p class="order-desc-title">
                            <spring:theme code="text.account.order.orderNumberLabel"/> : 
                            <span class="orderNumber">
                                ${fn:escapeXml(orderData.code)}
                            </span>
                        </p>
                        <p class="order-desc-sub-title">
                        <spring:theme code="orderconfirmation.successful.purchase.text" />
                        </p>
                    </div>
                </div>

                <div class="thankyou-text">
                    <p><spring:theme code="text.account.order.user.identity.text1"/> </p>
                     <!--<p class="thankyou-text-response">
                            <c:choose>
                                <c:when test="${isPrepaidOrder}">
                                    <spring:theme code="text.account.order.user.identity.text2"/>&nbsp;
                                    <a href="mailto:dlecommerce@cwpanama.com"><spring:theme code="text.account.order.user.identity.text2.email"/></a>
                                    <spring:theme code="text.account.order.user.identity.text2.or"/>&nbsp;
                                    <a href="tel:+507 323-7657"><spring:theme code="prepaid.text.account.order.user.identity.text2.whatsapp"/></a>,
                                    &nbsp;<spring:theme code="prepaid.text.account.order.user.identity.text3"/>
                                </c:when>
                            <c:otherwise>
                               <spring:theme code="text.account.order.user.identity.text3"/>
                             </c:otherwise>
                          </c:choose>
                     </p>-->
                     
                     <p class="thankyou-text-response">
                     	<c:choose>
                            <c:when test="${isPrepaidOrder || isPostpaidOrder}">
                    			<spring:theme code="text.account.order.user.identity.text2"/>
                    		</c:when>
                    		<c:otherwise>
                    			<spring:theme code="text.account.order.user.identity.text3"/>
                    		</c:otherwise>
                     	</c:choose>
                    </p>
                </div>

                <div class="sub-section-content">
                    <p class="sub-section-title">
                        <spring:theme code="text.account.order.billing"/>
                    </p>
                    <div class="sub-section-body invoiceData">
                        <order:addressItem address="${orderData.deliveryAddress}"/>
                    </div>
                    <input type="hidden" id="addressInfo"
                    <c:if test="${orderData.deliveryMode.code ne null}"> data-attr-shippingMethod="${orderData.deliveryMode.code}" </c:if>
                    <c:if test="${orderData.deliveryAddress.province ne null}"> data-attr-province="${orderData.deliveryAddress.province}" </c:if>
                    <c:if test="${orderData.deliveryAddress.district ne null}"> data-attr-district="${orderData.deliveryAddress.district}" </c:if>
                    <c:if test="${orderData.deliveryAddress.town ne null}"> data-attr-town="${orderData.deliveryAddress.town}" </c:if>
                    <c:if test="${orderData.deliveryAddress.neighbourhood ne null}"> data-attr-neighbourhood="${orderData.deliveryAddress.neighbourhood}" </c:if>
                    <c:if test="${orderData.deliveryAddress.building ne null}"> data-attr-street="${orderData.deliveryAddress.building}" </c:if>
                    <c:if test="${orderData.deliveryAddress.country.name ne null}"> data-attr-country="${orderData.deliveryAddress.country.name}" </c:if>                   
                />
                 <input type="hidden" id="cartPagePriceInfo"	
    <c:if test="${orderData.subTotal ne null}"> data-attr-subtotal="${orderData.subTotal.value}" </c:if>	
    <c:if test="${orderData.totalDiscounts ne null}"> data-attr-savings="${orderData.totalDiscounts.value}" </c:if>	
    <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>	
    <c:if test="${orderData.totalTax ne null}"> data-attr-taxRate='${orderData.totalTax.value}' </c:if>	
    <c:if test="${orderData.totalPriceWithTax ne null}"> data-attr-cartTotal="${orderData.totalPriceWithTax.value}" </c:if>	
/>
                </div>
                
                <!-- <div class="checkout-order-summary ">
                    <multi-checkout-telco:cableticaOrderItems orderData="${orderData}" />
                </div> -->
            </div> 


            <div class="col-sm-12 col-md-6 col-lg-6">
                 <div class="checkout-order-summary">              
                    <telco-order:cableticaOrderUnconsignedEntries order="${orderData}" />
                 </div>

                 <div class="sub-section-content general-condition">
                    <p class="sub-section-title">
                        <spring:theme code="orderconfirmation.general.conditions" />
                    </p>
                    <div class="sub-section-body">
                        <c:if test="${cmsSite.uid eq 'panama'}">
                            <c:choose>
                                <c:when test="${isPrepaidOrder}"> 
                                    <p>&#8226; <spring:theme code="text.account.order.prepaid.terms.listitem1"/></p>
                                    <p>&#8226; <spring:theme code="text.account.order.prepaid.terms.listitem2"/></p>
                                </c:when>
                                <c:otherwise>
                                    <p>&#8226; <spring:theme code="text.account.order.terms.listitem1"/></p>
                                    <p>&#8226; <spring:theme code="text.account.order.terms.listitem2"/></p>
                                </c:otherwise>
                            </c:choose>

                            <c:choose>
                                <c:when test="${isPrepaidOrder}"> 
                                    <p>&#8226; <spring:theme code="text.account.order.prepaid.terms.listitem3"/></p>
                                    <p>&#8226; <spring:theme code="text.account.order.prepaid.terms.listitem4"/></p>
                                    <p>&#8226; <spring:theme code="text.account.order.prepaid.terms.listitem5"/></p>
                                </c:when>
                                <c:otherwise>
                                    <p>&#8226; <spring:theme code="text.account.order.terms.listitem3"/></p>
                                    <p>&#8226; <spring:theme code="text.account.order.terms.listitem4"/></p>
                                    <p><spring:theme code="text.account.order.terms.listitem5"/></p>
                                </c:otherwise>
                            </c:choose>                                        
                        </c:if>  
                    </div>
                </div>
            </div>
        </div>        
    </div>  
</div>

<input type="hidden" id="orderNumber" data-attr-order-id="${fn:escapeXml(orderData.code)}" >
