<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="tab-details">
	<ycommerce:testId code="productDetails_content_label">
		<div class="tab-content-details">
	    	<p class="tab-data-desc"><spring:theme code="pdp.bundle.tvChannels.headtext1"/> </p>
	    </div>
	</ycommerce:testId>
</div>