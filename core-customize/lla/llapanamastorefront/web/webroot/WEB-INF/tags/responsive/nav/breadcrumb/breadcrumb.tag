<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="breadcrumbs" required="true" type="java.util.List"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:url value="/" var="homeUrl" />
<spring:url value="/plp/postpaid" var="postpaidPLPUrl"/>

<ol class="breadcrumb">
	<li>
		<spring:theme code="breadcrumb.home.panama" />
	</li>

	<c:forEach items="${breadcrumbs}" var="breadcrumb" varStatus="status">
		<spring:url htmlEscape="false" value="${breadcrumb.url}" var="breadcrumbUrl" />
		<c:choose>
			<c:when test="${status.last}">
				<li class="active">${fn:escapeXml(breadcrumb.name)}</li>
			</c:when>
			<c:when test="${breadcrumb.url eq '#'}">
				<li>
					<a href="#">${fn:escapeXml(breadcrumb.name)}</a>
				</li>
			</c:when>
			<c:otherwise>
		
			    <c:choose>
		
			    <c:when test="${breadcrumb.categoryCode eq 'devices' || breadcrumb.categoryCode eq '4Pbundles'}">
		        <c:if test="${breadcrumb.categoryCode eq 'devices'}">
		        <li>
			         ${fn:escapeXml(breadcrumb.name)}
			    </li>
		        </c:if>
		         <c:if test="${breadcrumb.categoryCode eq '4Pbundles'}">
				<li>
					${fn:escapeXml(breadcrumb.name)}
				</li>
				</c:if>
				</c:when>
				<c:otherwise>
				<li>
				
					<a href="${fn:escapeXml(breadcrumbUrl)}">${fn:escapeXml(breadcrumb.name)}</a>
				</li>
				</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>
	</c:forEach>
</ol>
