<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="address" required="true" type="de.hybris.platform.commercefacades.user.data.AddressData" %>
<%@ attribute name="storeAddress" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not storeAddress}">
	<p>${fn:escapeXml(address.firstName)}&nbsp;${fn:escapeXml(address.lastName)}</p>	
    <p>${fn:escapeXml(address.email)}</p>   
    <p>${fn:escapeXml(address.phone)}</p>
</c:if>