<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="tab-details">
	<ycommerce:testId code="productDetails_content_label">
		<div class="tab-content-details">
		   ${product.summary}
		</div>
	</ycommerce:testId>
</div>