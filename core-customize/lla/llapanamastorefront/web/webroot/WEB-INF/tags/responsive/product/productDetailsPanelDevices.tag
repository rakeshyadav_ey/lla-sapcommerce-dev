<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>



<c:set var="primarycategoryName" value="${fn:escapeXml(productCategories[productCategories.size()-2].name)}"/>
<input type="hidden" id="pdpAdobeAnalyticsData"
    <c:if test="${primarycategoryName ne null}"> data-attr-primarycategoryName="${primarycategoryName}" </c:if>
    <c:if test="${product.name ne null}"> data-attr-name="${fn:escapeXml(product.name)}" </c:if>
    <c:if test="${product.code ne null}"> data-attr-id="${product.code}" </c:if>
    <c:if test="${product.price.formattedValue ne null}"> data-attr-price="${product.price.formattedValue}" </c:if>
    <c:if test="${product.categories[0].name ne null}"> data-attr-category="${product.categories[0].code}" data-attr-brand="${product.categories[0].name}" </c:if>
    <c:if test="${product.price.currencyIso ne null}"> data-attr-currency="${product.price.currencyIso}" </c:if>   
/>

<div class="container">
<div class="product-details page-title">
	<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
		<div class="name">${fn:escapeXml(product.name)}
		<!--<span class="sku">ID</span>
		<span class="code">${fn:escapeXml(product.code)}</span>-->
		</div>
	</ycommerce:testId>
	<!--<product:productReviewSummary product="${product}" showLinks="true"/>-->
</div>
<div class="row">
	<div class="col-xs-10 col-xs-push-1 col-sm-6 col-sm-push-0 col-lg-6">
		<div class="col-xs-12 col-sm-6">
			<product:productImagePanel galleryImages="${galleryImages}" />
		</div>
		<div class="col-xs-12 col-sm-6">
			<!-- product specifications -->
		</div>
	</div>
	<div class="clearfix hidden-sm hidden-md hidden-lg"></div>
	<div class="col-sm-6 col-lg-6">
		<div class="product-main-info">
				<div class="row">
				<div class="col-xs-12">
					<div class="product-details">
						<product:productPromotionSection product="${product}"/>
						<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">

							<product:productPricePanel product="${product}" />
						</ycommerce:testId>
					</div>
					<cms:pageSlot position="VariantSelector" var="component" element="div" class="page-details-variants-select">
						<cms:component component="${component}" element="div" class="yComponentWrapper page-details-variants-select-component"/>
					</cms:pageSlot>
					<cms:pageSlot position="AddToCart" var="component" element="div" class="page-details-variants-select">
						<cms:component component="${component}" element="div" class="yComponentWrapper page-details-add-to-cart-component"/>
					</cms:pageSlot>
				</div>

				
			</div>
		</div>

	</div>
</div>
</div>