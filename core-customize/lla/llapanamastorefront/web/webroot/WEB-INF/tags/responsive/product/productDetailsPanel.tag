<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="primarycategoryName" value="${fn:escapeXml(productCategories[productCategories.size()-2].name)}"/>
<input type="hidden" id="pdpAdobeAnalyticsData"
    <c:if test="${primarycategoryName ne null}"> data-attr-primarycategoryName="${primarycategoryName}" </c:if>
    <c:if test="${product.name ne null}"> data-attr-name="${fn:escapeXml(product.name)}" </c:if>
    <c:if test="${product.code ne null}"> data-attr-id="${product.code}" </c:if>
    <c:if test="${product.price.formattedValue ne null}"> data-attr-price="${product.price.formattedValue}" </c:if>
    <c:if test="${product.categories[0].name ne null}"> data-attr-category="${product.categories[0].code}" </c:if>
    <c:if test="${product.price.currencyIso ne null}"> data-attr-currency="${product.price.currencyIso}" </c:if>
    <c:if test="${product.categories[0].name ne null}"> data-attr-brand="+ Movil" </c:if>
/>
<c:set var = "categoryCode" value="${product.categories[0].code}" />
<div class="pdp-custom container">
	<%--<div class="product-details page-title">
		<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
			<div class="name">${fn:escapeXml(product.name)}</div>
		</ycommerce:testId>
	</div>--%>
	<div class="process-selection-bar">
		<ul class="selection-state">
			<li class="active col-xs-4"><p class="level"><span class="process-state">1</span><span class="process-info"><spring:theme code="selection.level1.text"/></span></p></li>
			<li class="col-xs-4"><p class="level"><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></p></li>
			<li class="col-xs-4"><p class="level"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level3.text"/></span></p></li>
		</ul>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-lg-6">
			<product:productImagePanel galleryImages="${galleryImages}" />
		</div>
		<div class="clearfix hidden-sm hidden-md hidden-lg"></div>
		<div class="col-xs-12 col-sm-6 col-lg-6">
			<div class="product-main-info">
				<div class="row">
					<div class="col-lg-12">
						<div class="product-details">
							<div class="name">${fn:escapeXml(product.name)}</div>
							<div class="description">${product.summary}</div>
<%-- 							<div class="description">${ycommerce:sanitizeHTML(product.summary)}</div> --%>
							<product:productPromotionSection product="${product}"/>
							<c:if test="${cmsSite.uid ne 'panama'}">
							 <ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
								<product:productPricePanel product="${product}" />
							</ycommerce:testId> 
							</c:if>
							<c:if test="${cmsSite.uid eq 'panama'}">
                             <ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
                                <product:productPricePanel product="${product}" hidePrice="true" />
                            </ycommerce:testId>
                            </c:if>

							<cms:pageSlot position="AddToCart" var="component" element="div" class="page-details-variants-select">
								<cms:component component="${component}" element="div" class="yComponentWrapper page-details-add-to-cart-component"/>
							</cms:pageSlot>
						</div>
					</div>

					<div class="col-sm-12 col-md-9 col-lg-5">
						<cms:pageSlot position="VariantSelector" var="component" element="div" class="page-details-variants-select">
							<cms:component component="${component}" element="div" class="yComponentWrapper page-details-variants-select-component"/>
						</cms:pageSlot>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<c:if test="${cmsSite.uid eq 'panama' && product.categories[0].code == '4Pbundles'}">
<!-- popup design for FMC Bundles plan selection -->
<div class="hide">
	<div id="fmc-bundles-popup" class="fmc-bundles-popup">
		<div class='headline'>
			<p class='headline-text'><spring:theme code="pdp.fmc.popup.heading"/></p>
        </div>
        <div class="prodInfoData">
            <%--<div class="col-xs-12 col-sm-6 col-lg-6 prodImage">
                <div class="image-gallery js-gallery">
                    <img class="lazyOwl" src="">
                </div>
            </div>--%>
            <div class="col-xs-12 prodDetails">
                <div class="product-info">
                    <div class="product-details">
                        <p class="prodNamePopup"></p>  
                        <%--<p class="prodPricePopup"></p>--%>                    
                        <div class="description"></div>
                    </div>
                </div>
            </div>
         </div>
	 <div class="cta-header">
            <div class="cta-header-text">
                <div class="headertext">
                    <div class="prodPricePopup"></div>
                </div>
                <div class="desc"><spring:theme code="text.ctaMonthlyplan"/></div>
            </div>
        </div>
        <div class="col-xs-12">
            <p class="fmc-decision-text"><spring:theme code="pdp.fmc.decision.text"/></p>
            <div class="fmc-popup-btn-group row">
                <div class="col-xs-12 btnDiv text-center">
                    <button class="btn-custom fmc-pop-btn fmc-device-mobile">
                        <spring:theme code="pdp.fmc.popupYesButton.text" text="iyes, I WANT A NEW smartphone!"/>
                    </button>
                </div>
                <div class="col-xs-12 btnDiv">
                    <div class="text-center">
                        <form id="addToCartFormPopup" class="addToCartFormPopup">
                            <input type="hidden" name="isPlanExist" id="isPlanExistFmc">
                            <input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
                            <input type="hidden" name="productCodePost" class="productCodePost" value="">
                            <input type="hidden" name="processType" value="DEVICE_ONLY">
                            <input type="hidden" id="productCodePostListFirst" name="productCodePostList" value="">
                            <input type="hidden" id="qtyFirst" name="qtyList" value="">
                            <input type="hidden" id="productCodePostListSecond" name="productCodePostList" value="">
                            <input type="hidden" id="qtySecond" name="qtyList" value="">
                            <button type="submit" class="buynow-btn fmc-onlyBundle-btn fmc-pop-btn btn-white" id="buynow_${index.count}" >
                                <spring:theme code="pdp.fmc.popupNoButton.text"/>
                            </button>
                        </form>
                    </div>
                </div>                
            </div>
		</div>
	</div>
</div>
</c:if>
