<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<c:if test="${cartCalculationMessage != null}">
<div class="global-alerts">
    <div class="alert alert-info alert-dismissable getAccAlert">
        <spring:theme code="text.amount.paid.message" text="My Cart" />
    </div>
</div>
</c:if>