<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="deliveryMethod" required="true" type="de.hybris.platform.commercefacades.order.data.DeliveryModeData" %>
<%@ attribute name="isSelected" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<li>
	<input  id="${fn:escapeXml(deliveryMethod.code)}" name="delivery_method" checked="${isSelected ? 'checked': ''}" type="radio" value="${fn:escapeXml(deliveryMethod.code)}">
	<label for="${fn:escapeXml(deliveryMethod.code)}">${fn:escapeXml(deliveryMethod.name)}</label>
</li>