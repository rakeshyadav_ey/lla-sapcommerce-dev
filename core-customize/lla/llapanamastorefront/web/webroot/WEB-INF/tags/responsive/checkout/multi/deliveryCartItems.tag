<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="showDeliveryAddress" required="false" type="java.lang.Boolean"%>
<%@ attribute name="showPotentialPromotions" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="order-entry" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/entry"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<c:set var="hasDeliveryItems" value="${cartData.deliveryItemsQuantity > 0}" />
<c:set var="deliveryAddress" value="${cartData.deliveryAddress}" />
<c:set var="installationAddress" value="${cartData.installationAddress}" />
<c:if test="${not hasDeliveryItems}">
	<spring:theme code="checkout.pickup.no.delivery.required" text="No delivery required for this order" />
</c:if>

<ul class="checkout-order-summary-list">
	<c:if test="${hasDeliveryItems}">
		<c:if test="${cmsSite.uid eq 'jamaica'}">
		<li class="checkout-order-summary-heading">	
			<c:choose>
				<c:when test="${siteUid ne 'jamaica' and showDeliveryAddress and not empty deliveryAddress}">
				<div class="title">
						<spring:theme code="checkout.pickup.items.to.be.shipped" text="Installation Required At:" />
					</div>
						<div class="address">
						${ycommerce:encodeHTML(deliveryAddress.title)} ${ycommerce:encodeHTML(deliveryAddress.firstName)}&nbsp;
						${ycommerce:encodeHTML(deliveryAddress.lastName)}
						<br>
					                            ${ycommerce:encodeHTML(deliveryAddress.appartment)}&nbsp;
                        ${ycommerce:encodeHTML(deliveryAddress.building)}&nbsp;
                         ${ycommerce:encodeHTML(deliveryAddress.streetname)}&nbsp;
                       
                        <br>
                        ${ycommerce:encodeHTML(deliveryAddress.neighbourhood)}&nbsp;
                         ${ycommerce:encodeHTML(deliveryAddress.town)}&nbsp;
                          ${ycommerce:encodeHTML(deliveryAddress.district)}&nbsp;
                          ${ycommerce:encodeHTML(deliveryAddress.province)}&nbsp;
                       
                        <br>
                        ${ycommerce:encodeHTML(deliveryAddress.country.name)}&nbsp;
                        ${ycommerce:encodeHTML(deliveryAddress.postalCode)}&nbsp;
                       ${ycommerce:encodeHTML(deliveryAddress.phone)}
                      
					</div>
				</c:when>
				<c:when test="${siteUid eq 'jamaica' and hideInstallationAddress eq false and  not empty installationAddress}">
                    <div class="title">
                        <spring:theme code="checkout.pickup.items.to.be.shipped" text="Installation Required At:" />
                    </div>
                    <div class="address">
                        ${ycommerce:encodeHTML(installationAddress.title)} ${ycommerce:encodeHTML(installationAddress.firstName)}&nbsp;
                        ${ycommerce:encodeHTML(installationAddress.lastName)}
                        <br>
                        ${ycommerce:encodeHTML(installationAddress.line1)},&nbsp;
                        <c:if test="${not empty installationAddress.line2}">
                        ${ycommerce:encodeHTML(installationAddress.line2)},&nbsp;
                        </c:if>
                        <c:if test="${not empty installationAddress.area}">
                         ${ycommerce:encodeHTML(installationAddress.area)},&nbsp;
                        </c:if>
                        ${ycommerce:encodeHTML(installationAddress.town)},&nbsp;
                        <c:if test="${not empty installationAddress.region.name}">
                        ${ycommerce:encodeHTML(installationAddress.region.name)},&nbsp;
                        </c:if>
                        ${ycommerce:encodeHTML(installationAddress.postalCode)},&nbsp; ${ycommerce:encodeHTML(installationAddress.country.name)}
                        <br />
                        ${ycommerce:encodeHTML(installationAddress.phone)}
                    </div>
                </c:when>
				<c:otherwise>
					<spring:theme code="checkout.pickup.items.to.be.delivered" text="Items in the cart" />
				</c:otherwise>
			</c:choose>
		</li>
		</c:if>
	</c:if>
	<c:if test="${cmsSite.uid eq 'jamaica'}">
	<table id="order-totals" class="checkout-list-head">
		<thead>
	        <tr>
	            <th><spring:theme code="basket.items" text="Items" /></th>
	            <th><spring:theme code="basket.items.paycheckout" text="Pay on checkout" /></th>
	        </tr>
		</thead>
	</table>
	</c:if>
	<c:forEach items="${cartData.entries}" var="entry" varStatus="loop">
		<spring:url value="${entry.product.url}" var="productUrl" htmlEscape="false" />
		<li class="checkout-order-summary-list-items">
			<c:if test="${cmsSite.uid eq 'panama'}">
			<div class="col-xs-5">
			</c:if>
			<div class="thumb">
				<a href="${productUrl}">
					<product:productPrimaryImage product="${entry.product}" format="thumbnail" />
				</a>
			</div>
			<c:if test="${cmsSite.uid eq 'panama'}">
			</div>
			</c:if>

			<c:if test="${cmsSite.uid eq 'panama'}">
			<div class="col-xs-7">
			</c:if>
			<div class="details">
				<div class="name" style="display:flow-root;">
					<a href="${productUrl}">${ycommerce:encodeHTML(entry.product.name)}</a>
				</div>
				<order-entry:variantDetails product="${entry.product}"/>
				<!--<div class="qty">
					<span>
						<spring:theme code="basket.page.qty" text="Qty" />:
					</span>
					${entry.quantity}
				</div>-->
				<div>
					<c:forEach items="${entry.product.baseOptions}" var="option">
						<c:if test="${not empty option.selected and option.selected.url eq entry.product.url}">
							<c:forEach items="${option.selected.variantOptionQualifiers}" var="selectedOption">
								<div>${ycommerce:encodeHTML(selectedOption.name)}:${ycommerce:encodeHTML(selectedOption.value)}</div>
							</c:forEach>
						</c:if>
					</c:forEach>

					<div class="promotion">
						<c:if
							test="${ycommerce:doesPotentialPromotionExistForOrderEntryOrOrderEntryGroup(cartData, entry) && showPotentialPromotions}">
							<c:set var="promotions" value="${cartData.potentialProductPromotions}" />
						</c:if>
						<c:if test="${ycommerce:doesAppliedPromotionExistForOrderEntryOrOrderEntryGroup(cartData, entry)}">
							<c:set var="promotions" value="${cartData.appliedProductPromotions}" />
						</c:if>
                        <c:forEach items="${promotions}" var="promotion">
                            <c:if test="${not empty promotion.consumedEntries}">
                                <c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
                                    <c:if test="${ycommerce:isConsumedByEntry(consumedEntry,entry)}">
                                        <span class="promotion">${ycommerce:sanitizeHTML(promotion.description)}</span>
                                    </c:if>
                                </c:forEach>
                            </c:if>
                        </c:forEach>
					</div>
					<common:configurationInfos entry="${entry}" />
				</div>
				<spring:url value="/checkout/multi/getReadOnlyProductVariantMatrix" var="targetUrl" htmlEscape="false" />
				<grid:gridWrapper entry="${entry}" index="${loop.index}" styleClass="display-none" targetUrl="${targetUrl}" />
			</div>

			<div class="price">
				
					<c:forEach items="${entry.orderEntryPrices}" var="orderEntryPrice">
						<c:if test="${orderEntryPrice.basePrice.value != null}">
							
								<!--<div>${ycommerce:encodeHTML(orderEntryPrice.billingTime.name)}:</div>-->
								<div class="actualPrice">
									<order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryPrice.totalPrice}"
										orderEntryBasePrice="${orderEntryPrice.basePrice}" />
								</div>
								<input type="hidden" class="productGtmData"
                                        data-attr-name="${entry.product.name}"
                                        data-attr-id="${entry.product.code}"
                                        data-attr-price="${orderEntryPrice.totalPrice.value}"
                                        data-attr-brand="${entry.product.categories[0].name}"
                                        data-attr-category="${entry.product.categories[0].name}"
                                        data-attr-category_2="${entry.product.categories[0].parentCategoryName}"
                                        data-attr-quantity="${entry.quantity}"
                                >
								<c:if test="${entry.fixedLineProduct eq 'true'}">
									<div class="fixedline-product">
										<spring:theme code="text.installation.waiveoff" /><br>
										<spring:theme code="text.security.deposit" />:&nbsp;<order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryPrice.totalPrice}"
										orderEntryBasePrice="${orderEntryPrice.basePrice}" />
									</div>
								</c:if>
						</c:if>
					</c:forEach>
				
			</div>
			<c:if test="${cmsSite.uid eq 'panama'}">
			</div>
			</c:if>
		</li>
	</c:forEach>
</ul>
<input type="hidden" class="productGtmDataPaymentMethod" data-attr-cardtype="${cartData.paymentInfo.cardTypeData.name}" >