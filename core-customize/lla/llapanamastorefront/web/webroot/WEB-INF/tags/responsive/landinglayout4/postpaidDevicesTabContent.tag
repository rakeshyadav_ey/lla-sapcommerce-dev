<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<div class="cta-wrapper">            
	<c:forEach items="${allProductDatas.entrySet()}" var="entry">
		<c:forEach items="${categoriesList}" var="category">
			<c:if test="${entry.key eq category && category ne 'devices'}">
				<div class="tab-content-wrapper container ${category eq activeCategoryName ? '':'display-none'}" id="${entry.key}">
					<div class="cta-container"> 
						<c:forEach items="${entry.value}" var="productData" varStatus="productIndex">
							<c:if test="${productData.isVisibleOnPLP eq true}">
							<div class="col-xs-12 col-sm-5 col-md-4">
								<div class="livemore-col ${category eq 'postpaid' ? 'postpaid-devices-plp':'postpaid-plp'}">
									<div class="cta-prod-data">
										<div class="char-spec spec-opts cta-content-spec">
											<c:if test="${productData.promoFlag eq 'true'}"> 
												<span class="prod-card-bubble">
													<spring:theme code="panama.plp.promoOnline"/>
												</span>	
											</c:if>                
											<div class="name">${productData.name}</div> 
											<div class="name hide code">${productData.code}</div>                                                   
											<table>
												<tbody>
													<c:forEach var="specCharValue" items="${productData.productSpecCharValueUses}">
														<c:forEach items="${specCharValue.productSpecCharacteristicValues}" var="spcchar">
															<tr>
																<td>
																	<div class="${spcchar.productSpecCharacteristic} char-spec-col">
																		<div class="spec-description">
																			<div class="spec-heading">
																				<span>${spcchar.value}</span>
																				<span>${spcchar.unitOfMeasurment}</span>
																				<span class="spec-text">${spcchar.ctaSpecDescription}</span>
																			</div>
																		</div>
																	</div>
																</td>
															</tr>
														</c:forEach>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
									<div class="cta-header">
										<div class="cta-header-text">
											<div class="headertext newPrice">
												<fmt:setLocale value="en_US" scope="session"/>
												${currentCurrency.symbol}<fmt:formatNumber value="${productData.monthlyPrice.value}" minFractionDigits="2" maxFractionDigits="2" pattern="########.##" />
												</div>
											<div class="description"><span>
												<spring:theme code="text.ctaMonthlyplan"/>
											</span></div>
										</div>
									</div>
						<div class="cta-btn-block text-center">
							<c:if test="${category ne 'postpaid'}">
										<button type="submit" class="addcart-btn buynow-btn" id="buynow_${productIndex.count}" 
										data-attr-item-code="${productData.code}" 
										data-attr-id="${productData.code}" 
										data-attr-price="${productData.monthlyPrice.value}"
										data-arrt-name="${productData.name}"
										data-arrt-desc="${productData.description}"
										data-attr-category="${activeCategoryName}" data-attr-category_2="${category}">
										<spring:theme code="panama.plp.information.url"/>
										</button>
							</c:if>
							<c:if test="${category eq 'postpaid'}"> 
								  <form id="addToCartFormPostPaidPlp" class="add_to_cart_form" action="${contextPath}/${cmsSite.uid}/${currentLanguage.isocode}/cart/add" method="post">
						                <input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
						                <input type="hidden" name="productCodePost" id="productCodePost" value="${productData.code}">
						                <input type="hidden" name="processType" value="DEVICE_ONLY">
						               <button type="submit" class="buynow-btn" id="buynow_${index.count}" 
										data-attr-item-code="${productData.code}" 
										data-attr-id="${productData.code}" 
										data-attr-price="${productData.monthlyPrice.value}"
										data-arrt-name="${productData.name}"
										data-arrt-desc="${productData.description}"
										data-attr-category="${activeCategoryName}" data-attr-category_2="${category}">
										<spring:theme code="panama.plp.information.url"/>
										</button>
					              </form> 
							</c:if>		
						</div>
								</div>
								<div class="postpaid-terms">
									<spring:theme code="text.postpaid.terms" htmlEscape="false" />
								</div>
							</div>
							</c:if>
						</c:forEach>
					</div>
				</div>
			</c:if>
			<c:if test="${entry.key eq category && category eq 'devices'}">
				<div class="tab-content-wrapper container ${category eq activeCategoryName ? '':'display-none'}" id="${entry.key}">
					<div class="devices-plp">
						<div class="product__listing product__list prod_grid_view">
							<c:forEach items="${entry.value}" var="productData" varStatus="productIndex">
								<li class="grid_view product__list--items col-lg-4 col-md-4 col-sm-4">
									<div class="full-grid devices-full-box">
										<div class="prod-wrapper">
											<c:if test="${productData.promoFlag eq 'true'}"> 
												<span class="prod-card-bubble">
													<spring:theme code="text.online.price"/>
												</span>
											</c:if>	
											<div class="product-info">
												<a href="${contextPath}/${currentLanguage.isocode}${productData.url}" class="product__list--thumb prod-img">
													<product:productPrimaryImage product="${productData}" format="cartIcon"/>
												</a>
												<div class="prod-content">
													<span class="p-brand">${productData.manufacturer}</span>
													<p class="product__list--name prod-name">${productData.name}</p>
													<div class="prod-data">
														<div class="product__listing--price">
															<div class="product-price prod-price">
															<c:forEach items="${productData.devicePriceList}" var="priceList">
															<c:if test="${priceList.planType eq 'NOOFFER'}">
																<span class="p-code"><spring:theme code="text.device.fullPrice"/>&nbsp;&nbsp;<s>${priceList.formattedValue}</s></span>
																<span class="no-offer-price hide" data-price="${priceList.formattedValue}">${priceList.formattedValue}</span>
															</c:if>	
															</c:forEach>
															<div class="planFirst hide">
															<c:forEach items="${productData.devicePriceList}" var="priceList">
															<c:if test="${priceList.planType eq 'POSTPAID'}">
																<span class="actual-price ${priceList.planCode} hide" data-price="${priceList.formattedValue}">${priceList.formattedValue}</span>
															</c:if>	
															</c:forEach>
															</div>
															<div class="deviceFirst">
															<c:set var="minPrice" value="0"/>
															<c:forEach items="${productData.devicePriceList}" var="priceList">
																<c:if test="${minPrice == 0}">
																	<c:set var="minPrice" value="${priceList.value}"/>
																	<c:set var="minPriceList" value="${priceList}"/>
																</c:if>
																<c:if test="${priceList.value < minPrice}">
																	<c:set var="minPrice" value="${priceList.value}"/>
																	<c:set var="minPriceList" value="${priceList}"/>
																</c:if>
															</c:forEach>
															<span class="actual-price ${priceList.planCode}" data-price="${priceList.formattedValue}">${minPriceList.formattedValue}</span>
															</div>
															<span class="p-code"><spring:theme code="text.device.PriceWithPlan"/></span>
															</div>
															
														</div>
													</div>
												</div>
											</div>
											<div class="prod-btn-info">
												<a class="btn add-cart-btn" href="${contextPath}/${currentLanguage.isocode}${productData.url}" data-device-code="${productData.code}" data-device-name="${productData.name}" data-device-brand="${productData.manufacturer}">
														<spring:theme code="panama.plp.information.url"/>
													</a>   
													<%--
													<form id="addToCartForm" class="add_to_cart_form" action="${contextPath}/${cmsSite.uid}/${currentLanguage.isocode}/cart/add/mobileplans" method="post">
														<input type="hidden" id="qty" name="qty" value="1">
														<input type="hidden" id="productCodePost" name="productCodePost" value="${productData.code}">
														<input type="hidden" name="processType" value="DEVICE_ONLY">
														<input type="hidden" id="planType" name="planType" value="postpaid">
														<input type="hidden" id="plan" name="plan" value="">
														<button type="submit" class="btn add-cart-btn" id="buynow_${index.count}">
															<spring:theme code="text.device.addToCart"/>
														</button>
													</form>--%>                                               
												</div>
										</div>                                                  
									</div>
								</li>
							</c:forEach>
						</div>
					</div>
				</div>
			</c:if>
		</c:forEach>
	</c:forEach>
</div>

