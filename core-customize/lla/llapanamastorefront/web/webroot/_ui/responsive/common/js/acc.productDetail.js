ACC.productDetail = {

    _autoload: [
        "initPageEvents",
        "bindVariantOptions"
    ],

    checkQtySelector: function (self, mode) {
    	var $qtySelector = $(document).find(self).parents(".js-qty-selector");
        var input = $qtySelector.find(".js-qty-selector-input");
        var inputVal = parseInt(input.val());
        var max = input.data("max");
        var minusBtn = $qtySelector.find(".js-qty-selector-minus");
        var plusBtn = $qtySelector.find(".js-qty-selector-plus");

        $qtySelector.find(".btn").removeAttr("disabled");

        if (mode == "minus") {
            if (inputVal != 1) {
                ACC.productDetail.updateQtyValue(self, inputVal - 1)
                if (inputVal - 1 == 1) {
                    minusBtn.attr("disabled", "disabled")
                }

            } else {
                minusBtn.attr("disabled", "disabled")
            }
        } else if (mode == "reset") {
            ACC.productDetail.updateQtyValue(self, 1)

        } else if (mode == "plus") {
        	if(max == "FORCE_IN_STOCK") {
        		ACC.productDetail.updateQtyValue(self, inputVal + 1)
        	} else if (inputVal <= max) {
                ACC.productDetail.updateQtyValue(self, inputVal + 1)
                if (inputVal + 1 == max) {
                    plusBtn.attr("disabled", "disabled")
                }
            } else {
                plusBtn.attr("disabled", "disabled")
            }
        } else if (mode == "input") {
            if (inputVal == 1) {
                minusBtn.attr("disabled", "disabled")
            } else if(max == "FORCE_IN_STOCK" && inputVal > 0) {
            	ACC.productDetail.updateQtyValue(self, inputVal)
            } else if (inputVal == max) {
                plusBtn.attr("disabled", "disabled")
            } else if (inputVal < 1) {
                ACC.productDetail.updateQtyValue(self, 1)
                minusBtn.attr("disabled", "disabled")
            } else if (inputVal > max) {
                ACC.productDetail.updateQtyValue(self, max)
                plusBtn.attr("disabled", "disabled")
            }
        } else if (mode == "focusout") {
        	if (isNaN(inputVal)){
                ACC.productDetail.updateQtyValue(self, 1);
                minusBtn.attr("disabled", "disabled");
        	} else if(inputVal >= max) {
                plusBtn.attr("disabled", "disabled");
            }
        }

    },
    updateQtyValue: function (self, value) {
        var input = $(document).find(self).parents(".js-qty-selector").find(".js-qty-selector-input");
        var addtocartQty = $(document).find(self).parents(".addtocart-component").find("#addToCartForm").find(".js-qty-selector-input");
        var configureQty = $(document).find(self).parents(".addtocart-component").find("#configureForm").find(".js-qty-selector-input");
        input.val(value);
        addtocartQty.val(value);
        configureQty.val(value);
    },

    /* SetTopBox Qty Related Starts */
    checkSetTopBoxQtySelector: function (self, mode) {
        var $qtySelector = $(document).find(self).parents(".js-qty-selector_SETTOP_BOX");
        var input = $qtySelector.find(".js-qty-selector-input");
        var inputVal = parseInt(input.val());
        var max = input.data("max");
        var minussettopboxBtn = $qtySelector.find(".SETTOP_BOX-qty-minus");
        var plussettopboxBtn = $qtySelector.find(".SETTOP_BOX-qty-plus");

        $qtySelector.find(".btn").removeAttr("disabled");

        if (mode == "minus") {
            if (inputVal != 0) {
                ACC.productDetail.updateSetTopBoxQtyValue(self, inputVal - 1)
                if (inputVal - 1 == 0) {
                    minussettopboxBtn.attr("disabled", "disabled");
                }

            } else {
                minussettopboxBtn.attr("disabled", "disabled");
            }
        } else if (mode == "reset") {
            ACC.productDetail.updateSetTopBoxQtyValue(self, 0)

        } else if (mode == "plus") {
            if(max == "FORCE_IN_STOCK") {
                ACC.productDetail.updateSetTopBoxQtyValue(self, inputVal + 1)
            } else if (inputVal <= max) {
                ACC.productDetail.updateSetTopBoxQtyValue(self, inputVal + 1)
                if (inputVal + 1 == max) {
                    plussettopboxBtn.attr("disabled", "disabled");
                }
            } else {
                plussettopboxBtn.attr("disabled", "disabled");
            }
        } else if (mode == "input") {
            if (inputVal == 0) {
                minussettopboxBtn.attr("disabled", "disabled");
            } else if(max == "FORCE_IN_STOCK" && inputVal > 0) {
                ACC.productDetail.updateSetTopBoxQtyValue(self, inputVal)
            } else if (inputVal == max) {
                plussettopboxBtn.attr("disabled", "disabled");
            } else if (inputVal < 0) {
                ACC.productDetail.updateSetTopBoxQtyValue(self, 0)
                minussettopboxBtn.attr("disabled", "disabled");
            } else if (inputVal > max) {
                ACC.productDetail.updateSetTopBoxQtyValue(self, max)
                plussettopboxBtn.attr("disabled", "disabled");
            }
        } else if (mode == "focusout") {
            if (isNaN(inputVal)){
                ACC.productDetail.updateSetTopBoxQtyValue(self, 0);
                minussettopboxBtn.attr("disabled", "disabled");
            } else if(inputVal >= max) {
                plussettopboxBtn.attr("disabled", "disabled");
            }
            if (inputVal == 0) {
                minussettopboxBtn.attr("disabled", "disabled");
            } else if(max == "FORCE_IN_STOCK" && inputVal > 0) {
                ACC.productDetail.updateSetTopBoxQtyValue(self, inputVal)
            } else if (inputVal == max) {
                plussettopboxBtn.attr("disabled", "disabled");
            } else if (inputVal <= 0) {
                ACC.productDetail.updateSetTopBoxQtyValue(self, 0)
                minussettopboxBtn.attr("disabled", "disabled");
            } else if (inputVal > max) {
                ACC.productDetail.updateSetTopBoxQtyValue(self, max)
                plussettopboxBtn.attr("disabled", "disabled");
            }
        }

    },
    updateSetTopBoxQtyValue: function (self, value) {
        var input = $(document).find(self).parents(".js-qty-selector_SETTOP_BOX").find(".js-qty-selector-input");
        var addtocartQty = $(document).find(self).parents(".addtocart-component").find("#addToCartForm .js-qty-selector_SETTOP_BOX").find(".js-qty-selector-input");
        var configureQty = $(document).find(self).parents(".addtocart-component").find("#configureForm .js-qty-selector_SETTOP_BOX").find(".js-qty-selector-input");
        input.val(value);
        addtocartQty.val(value);
        configureQty.val(value);
    },
    /* SetTopBox Qty Related Ends */

    /* WifiExtBox Qty Related Starts */
    checkWifiExtBoxQtySelector: function (self, mode) {
        var $qtySelector = $(document).find(self).parents(".js-qty-selector_WIFI_EXT");
        var input = $qtySelector.find(".js-qty-selector-input");
        var inputVal = parseInt(input.val());
        var max = input.data("max");
        var minuswifiextboxBtn = $qtySelector.find(".WIFI_EXT-qty-minus");
        var pluswifiextboxBtn = $qtySelector.find(".WIFI_EXT-qty-plus");

        $qtySelector.find(".btn").removeAttr("disabled");

        if (mode == "minus") {
            if (inputVal != 0) {
                ACC.productDetail.updateWifiExtBoxQtyValue(self, inputVal - 1)
                if (inputVal - 1 == 0) {
                    minuswifiextboxBtn.attr("disabled", "disabled");
                }

            } else {
                minuswifiextboxBtn.attr("disabled", "disabled");
            }
        } else if (mode == "reset") {
            ACC.productDetail.updateWifiExtBoxQtyValue(self, 0)

        } else if (mode == "plus") {
            if(max == "FORCE_IN_STOCK") {
                ACC.productDetail.updateWifiExtBoxQtyValue(self, inputVal + 1)
            } else if (inputVal <= max) {
                ACC.productDetail.updateWifiExtBoxQtyValue(self, inputVal + 1)
                if (inputVal + 1 == max) {
                    pluswifiextboxBtn.attr("disabled", "disabled");
                }
            } else {
                pluswifiextboxBtn.attr("disabled", "disabled");
            }
        } else if (mode == "input") {
            if (inputVal == 0) {
                minuswifiextboxBtn.attr("disabled", "disabled");
            } else if(max == "FORCE_IN_STOCK" && inputVal > 0) {
                ACC.productDetail.updateWifiExtBoxQtyValue(self, inputVal)
            } else if (inputVal == max) {
                pluswifiextboxBtn.attr("disabled", "disabled");
            } else if (inputVal <= 0) {
                ACC.productDetail.updateWifiExtBoxQtyValue(self, 0)
                minuswifiextboxBtn.attr("disabled", "disabled");
            } else if (inputVal > max) {
                ACC.productDetail.updateWifiExtBoxQtyValue(self, max)
                pluswifiextboxBtn.attr("disabled", "disabled");
            }
        } else if (mode == "focusout") {
            if (isNaN(inputVal)){
                ACC.productDetail.updateWifiExtBoxQtyValue(self, 0);
                minuswifiextboxBtn.attr("disabled", "disabled");
            } else if(inputVal >= max) {
                pluswifiextboxBtn.attr("disabled", "disabled");
            }
            if (inputVal == 0) {
                minuswifiextboxBtn.attr("disabled", "disabled");
            } else if(max == "FORCE_IN_STOCK" && inputVal > 0) {
                ACC.productDetail.updateWifiExtBoxQtyValue(self, inputVal)
            } else if (inputVal == max) {
                pluswifiextboxBtn.attr("disabled", "disabled");
            } else if (inputVal <= 0) {
                ACC.productDetail.updateWifiExtBoxQtyValue(self, 0)
                minuswifiextboxBtn.attr("disabled", "disabled");
            } else if (inputVal > max) {
                ACC.productDetail.updateWifiExtBoxQtyValue(self, max)
                pluswifiextboxBtn.attr("disabled", "disabled");
            }
        }
    },
    updateWifiExtBoxQtyValue: function (self, value) {
        var input = $(document).find(self).parents(".js-qty-selector_WIFI_EXT").find(".js-qty-selector-input");
        var addtocartQty = $(document).find(self).parents(".addtocart-component").find("#addToCartForm .js-qty-selector_WIFI_EXT").find(".js-qty-selector-input");
        var configureQty = $(document).find(self).parents(".addtocart-component").find("#configureForm .js-qty-selector_WIFI_EXT").find(".js-qty-selector-input");
        input.val(value);
        addtocartQty.val(value);
        configureQty.val(value);
    },
    /* WifiExtBox Qty Related Ends */

    initPageEvents: function () {
        $(document).on("click", '.js-qty-selector .js-qty-selector-minus', function () {
            ACC.productDetail.checkQtySelector(this, "minus");
        })

        $(document).on("click", '.js-qty-selector .js-qty-selector-plus', function () {
            ACC.productDetail.checkQtySelector(this, "plus");
        })

        $(document).on("keydown", '.js-qty-selector .js-qty-selector-input', function (e) {

            if (($(this).val() != " " && ((e.which >= 48 && e.which <= 57 ) || (e.which >= 96 && e.which <= 105 ))  ) || e.which == 8 || e.which == 46 || e.which == 37 || e.which == 39 || e.which == 9) {
            }
            else if (e.which == 38) {
                ACC.productDetail.checkQtySelector(this, "plus");
            }
            else if (e.which == 40) {
                ACC.productDetail.checkQtySelector(this, "minus");
            }
            else {
                e.preventDefault();
            }
        })

        $(document).on("keyup", '.js-qty-selector .js-qty-selector-input', function (e) {
            ACC.productDetail.checkQtySelector(this, "input");
            ACC.productDetail.updateQtyValue(this, $(this).val());

        })

        $(document).on("focusout", '.js-qty-selector .js-qty-selector-input', function (e) {
            ACC.productDetail.checkQtySelector(this, "focusout");
            ACC.productDetail.updateQtyValue(this, $(this).val());
        })

        /* SetTopBox Qty Related Starts */
        $(document).on("click", '.js-qty-selector_SETTOP_BOX .SETTOP_BOX-qty-minus', function () {
            ACC.productDetail.checkSetTopBoxQtySelector(this, "minus");
        })

        $(document).on("click", '.js-qty-selector_SETTOP_BOX .SETTOP_BOX-qty-plus', function () {
            ACC.productDetail.checkSetTopBoxQtySelector(this, "plus");
        })

        $(document).on("keydown", '.js-qty-selector_SETTOP_BOX .js-qty-selector-input', function (e) {

            if (($(this).val() != " " && ((e.which >= 48 && e.which <= 57 ) || (e.which >= 96 && e.which <= 105 ))  ) || e.which == 8 || e.which == 46 || e.which == 37 || e.which == 39 || e.which == 9) {
            }
            else if (e.which == 38) {
                ACC.productDetail.checkSetTopBoxQtySelector(this, "plus");
            }
            else if (e.which == 40) {
                ACC.productDetail.checkSetTopBoxQtySelector(this, "minus");
            }
            else {
                e.preventDefault();
            }
        })

        $(document).on("keyup", '.js-qty-selector_SETTOP_BOX .js-qty-selector-input', function (e) {
            ACC.productDetail.checkSetTopBoxQtySelector(this, "input");
            ACC.productDetail.updateSetTopBoxQtyValue(this, $(this).val());

        })

        $(document).on("focusout", '.js-qty-selector_SETTOP_BOX .js-qty-selector-input', function (e) {
            ACC.productDetail.checkSetTopBoxQtySelector(this, "focusout");
            ACC.productDetail.updateSetTopBoxQtyValue(this, $(this).val());
        })
        /* SetTopBox Qty Related Ends */

         /* WifiExtBox Qty Related Starts */
        $(document).on("click", '.js-qty-selector_WIFI_EXT .WIFI_EXT-qty-minus', function () {
            ACC.productDetail.checkWifiExtBoxQtySelector(this, "minus");
        })

        $(document).on("click", '.js-qty-selector_WIFI_EXT .WIFI_EXT-qty-plus', function () {
            ACC.productDetail.checkWifiExtBoxQtySelector(this, "plus");
        })

        $(document).on("keydown", '.js-qty-selector_WIFI_EXT .js-qty-selector-input', function (e) {

            if (($(this).val() != " " && ((e.which >= 48 && e.which <= 57 ) || (e.which >= 96 && e.which <= 105 ))  ) || e.which == 8 || e.which == 46 || e.which == 37 || e.which == 39 || e.which == 9) {
            }
            else if (e.which == 38) {
                ACC.productDetail.checkWifiExtBoxQtySelector(this, "plus");
            }
            else if (e.which == 40) {
                ACC.productDetail.checkWifiExtBoxQtySelector(this, "minus");
            }
            else {
                e.preventDefault();
            }
        })

        $(document).on("keyup", '.js-qty-selector_WIFI_EXT .js-qty-selector-input', function (e) {
            ACC.productDetail.checkWifiExtBoxQtySelector(this, "input");
            ACC.productDetail.updateWifiExtBoxQtyValue(this, $(this).val());

        })

        $(document).on("focusout", '.js-qty-selector_WIFI_EXT .js-qty-selector-input', function (e) {
            ACC.productDetail.checkWifiExtBoxQtySelector(this, "focusout");
            ACC.productDetail.updateWifiExtBoxQtyValue(this, $(this).val());
        })
        /* WifiExtBox Qty Related Ends */

        $("#Size").change(function () {
            changeOnVariantOptionSelection($("#Size option:selected"));
        });

        $("#variant").change(function () {
            changeOnVariantOptionSelection($("#variant option:selected"));
        });

        $(".selectPriority").change(function () {
            window.location.href = $(this[this.selectedIndex]).val();
        });

        function changeOnVariantOptionSelection(optionSelected) {
            window.location.href = optionSelected.attr('value');
        }
    },

    bindVariantOptions: function () {
        ACC.productDetail.bindCurrentStyle();
        ACC.productDetail.bindCurrentSize();
        ACC.productDetail.bindCurrentType();
    },

    bindCurrentStyle: function () {
        var currentStyle = $("#currentStyleValue").data("styleValue");
        var styleSpan = $(".styleName");
        if (currentStyle != null) {
            styleSpan.text(": " + currentStyle);
        }
    },

    bindCurrentSize: function () {
        var currentSize = $("#currentSizeValue").data("sizeValue");
        var sizeSpan = $(".sizeName");
        if (currentSize != null) {
            sizeSpan.text(": " + currentSize);
        }
    },

    bindCurrentType: function () {
        var currentSize = $("#currentTypeValue").data("typeValue");
        var sizeSpan = $(".typeName");
        if (currentSize != null) {
            sizeSpan.text(": " + currentSize);
        }
    }
};

ACC.addToCartFMCPopUp = {
    showfmcaddtocartpopup: function() {
        /* FMC Addtocart Popup Realted */
        $(document).on("click", ".addToCartFmcPopup", function(e) {
            e.preventDefault();
            var popupHeadLine = $(".fmc-bundles-popup").prev(".headline").html();
            $("#fmc-bundles-popup .prodImage img").attr("src", $(".pdp-custom .image-gallery .owl-item img").attr('src')); //Product Image
            $("#fmc-bundles-popup .prodPricePopup").text($(".fmc-price").text()); //price data
            $("#fmc-bundles-popup .prodNamePopup").text($(".productNameInfo .prodName").text()); //Product Name
            $("#fmc-bundles-popup .description").html($(".product-details .description").html()); //Product Description
            $("#fmc-bundles-popup #addToCartFormPopup .productCodePost").val($(this).attr('data-attr-item-code')); //Item code
            var codes = [], qtyDetails = [];
            $(".qtyItemInfo").each(function(){
                codes.push($(this).find('[name ="productCodePostList"]').val());
                qtyDetails.push($(this).find('[name ="qtyList"]').val());
            });
            /* Item Code hidden fields */
            $("#fmc-bundles-popup #addToCartFormPopup #productCodePostListFirst").val(codes[0]);
            $("#fmc-bundles-popup #addToCartFormPopup #productCodePostListSecond").val(codes[1]);
            /* Quantity hidden fields */
            $("#fmc-bundles-popup #addToCartFormPopup #qtyFirst").val(qtyDetails[0]);
            $("#fmc-bundles-popup #addToCartFormPopup #qtySecond").val(qtyDetails[1]);
            var popupWidth = 0;
            if($(window).width() <= 1024) {
                popupWidth = "85%";
            }
            else {
               popupWidth = "45%";
            }
            $.colorbox({
                maxWidth:"100%",
                maxHeight:"100%",
                inline: true,
                width:popupWidth,
                scrolling:true,
                href: "#fmc-bundles-popup",
                close:'<span class="glyphicon glyphicon-remove"></span>',
                title:popupHeadLine,
                onComplete: function() {
                    $("#cboxLoadedContent").css({
                        "margin-top": "30px"
                    });
                    ACC.common.refreshScreenReaderBuffer();
                },
                onClosed: function() {
                    ACC.common.refreshScreenReaderBuffer();
                }
            });
        });

        $(document).on("click", ".fmc-onlyBundle-btn", function(e) {
            e.preventDefault();
            var buttonType = 'no';
            fmcAddToCart(buttonType);
        });

        $(document).on("click", ".fmc-device-mobile", function(e) {
            e.preventDefault();
            var buttonType = 'yes';
            fmcAddToCart(buttonType);
        });

    }
}

ACC.addToCartFMC3pbundle = {
    addtocart3pbundle: function() {
        $(document).on("click", ".addToCartFmc3pbundle", function(e) {
            setTimeout(function(){
                $(".addToCartFmc3pbundle").addClass('disabled').prop("disabled", true);
                $('.btnqtyItemInfo .tooltiptext').addClass('show').removeClass('hide');
            }, 1000);
        });
    }
}

function fmcAddToCart(buttonType){
    var formData = $("#addToCartFormPopup").serialize();
    $.ajax({
        url: ACC.config.encodedContextPath + "/cart/add",
        data: formData,
        async: false,
        type: 'POST',
        dataType: 'json',
        success: function(data) {
            ACC.minicart.updateMiniCartDisplay();
            if(buttonType == 'yes') {
                $.colorbox.close();
                $('.livemore-section .product__listing .add-cart-btn').removeAttr('disabled');
                $('.livemore-section .product__listing .tooltiptext').addClass('hide');
                $(".addToCartFmcPopup").addClass('disabled').prop("disabled", true);
                $('.btnqtyItemInfo .tooltiptext').addClass('show').removeClass('hide');
                $('html,body').animate({
				    scrollTop: $(".plp-livemore-section").offset().top
				}, 'slow');
            }else{
                var url = ACC.config.encodedContextPath + "/cart";
                window.location.href = url;
            }
        },
        failure: function(error){
            console.error("error: " + error);
        }
    });
}

$(document).ready(function (){
    ACC.addToCartFMCPopUp.showfmcaddtocartpopup();
    ACC.addToCartFMC3pbundle.addtocart3pbundle();
});