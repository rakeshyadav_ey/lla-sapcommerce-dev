ACC.product = {

    _autoload: [
        "bindToAddToCartForm",
        "enableStorePickupButton",
        "enableVariantSelectors",
        "bindFacets",
        "bindTabLogic"
    ],


    bindTabLogic: function () {
        if($('.js-tab-head[data-content-id="devices"]').hasClass("active")) {
            $(".postpaid-only").hide();
            $(".devices-only").show();
        }
         if($('.js-tab-head[data-content-id="postpaid"]').hasClass("active")) {
             $(".postpaid-only").show();
             $(".devices-only").hide();
        }

        $(document).ready(function () {
            // show the actual device price if there is no price associated with the plan
            $(".page-productDetails").find(".product-price").each(function() {
                var _loopthis = $(this);
                var priceLength = _loopthis.find(".actual-price").length;
                if(priceLength == 0) {
                    _loopthis.find(".p-code").first().addClass("hide");
                    _loopthis.find(".no-offer-price").removeClass("hide").addClass("actual-price");
                }
            });
            $(".plan-type").each(function() {
                var _loopthis = $(this);
                var planLength = _loopthis.find(".with-offer").length;
                if(planLength == 0) {
                    _loopthis.find(".without-offer").addClass("with-offer").removeClass("without-offer");
                }
            });
            if($('.page-productDetails').length > 0){
                sessionStorage.setItem("page-wrapper", 'true');
            } else if ($('.page-internettvtelephone').length > 0) {
                if ((sessionStorage.getItem("page-wrapper") === 'true') && sessionStorage.getItem("residential-category") !== null) {
                    var type = sessionStorage.getItem("residential-category");
                    sessionStorage.removeItem('page-wrapper');
                    $('.js-tab-head[data-content-id='+ type + ']').trigger( "click" );
                } else {
                    sessionStorage.setItem("residential-category", $('.js-tab-head.active').attr('data-content-id'));
                }
            }
            if($(".pageLabel-residential").length > 0 ) {
            	localStorage.removeItem("step-1");
            	localStorage.removeItem("step-2");
            }
        });

        $(document).on("click", ".js-tab-head", function (e) {
            e.preventDefault();
            var _this = $(this);
            var tabId = _this.attr('data-content-id');
            $('.js-tab-head').each(function () {
                var _loopthis = $(this);
                _loopthis.removeClass('active');
            });
            _this.addClass('active');
            if($('.js-tab-head[data-content-id="devices"]').hasClass("active")) {
               $(".postpaid-only").hide();
               $(".devices-only").show();
            }
            if($('.js-tab-head[data-content-id="postpaid"]').hasClass("active")) {
                $(".postpaid-only").show();
                $(".devices-only").hide();
            }
            $('.tab-content-wrapper').each(function () {
                var _loopthis = $(this);
                _loopthis.addClass('display-none');
            });
            $('#' + tabId).removeClass('display-none');
            sessionStorage.setItem("residential-category", tabId);
        });
        // spliting price to use 2 different font sizes
        $(".residential-plp .livemore-col").each(function(){
            var priceData = $(this).find(".cta-header .headertext").text();
            var dotCount = priceData.match(/\./g).length;
            console.log(dotCount);
            if (dotCount == 2) {
                var floatPrice = priceData.split(".");
                var htmlData = floatPrice[0]+'.'+floatPrice[1]+ "<span>."+floatPrice[2]+"</span>";
                $(this).find(".cta-header .headertext").text("").html(htmlData);
            }
        });
    },


    bindFacets: function () {
        $(document).on("click", ".js-show-facets", function (e) {
            e.preventDefault();
            var selectRefinementsTitle = $(this).data("selectRefinementsTitle");
            var colorBoxTitleHtml = ACC.common.encodeHtml(selectRefinementsTitle);
            ACC.colorbox.open(colorBoxTitleHtml, {
                href: ".js-product-facet",
                inline: true,
                width: "480px",
                onComplete: function () {
                    $(document).on("click", ".js-product-facet .js-facet-name", function (e) {
                        e.preventDefault();
                        $(".js-product-facet  .js-facet").removeClass("active");
                        $(this).parents(".js-facet").addClass("active");
                        $.colorbox.resize()
                    })
                },
                onClosed: function () {
                    $(document).off("click", ".js-product-facet .js-facet-name");
                }
            });
        });
        enquire.register("screen and (min-width:" + screenSmMax + ")", function () {
            $("#cboxClose").click();
        });
    },


    enableAddToCartButton: function () {
        $('.js-enable-btn').each(function () {
            if (!($(this).hasClass('outOfStock') || $(this).hasClass('out-of-stock'))) {
                $(this).prop("disabled", false);
            }
            if ($(this).hasClass('disabled')) {
                $(this).prop("disabled", true);
            }
        });
    },

    enableVariantSelectors: function () {
        $('.variant-select').prop("disabled", false);
    },

    bindToAddToCartForm: function () {
        var addToCartForm = $('.add_to_cart_form');
        addToCartForm.ajaxForm({
        	beforeSubmit:ACC.product.showRequest,
        	//success: ACC.product.displayAddToCartPopup
        	success: function() {
                var url = ACC.config.encodedContextPath + "/cart";
                window.location.href = url;
            }
         });
        setTimeout(function(){
        	$ajaxCallEvent  = true;
         }, 2000);
     },
     showRequest: function(arr, $form, options) {
    	 if($ajaxCallEvent)
    		{
    		 $ajaxCallEvent = false;
    		 return true;
    		}
    	 return false;

    },

    bindToAddToCartStorePickUpForm: function () {
        var addToCartStorePickUpForm = $('#colorbox #add_to_cart_storepickup_form');
        addToCartStorePickUpForm.ajaxForm({success: ACC.product.displayAddToCartPopup});
    },

    enableStorePickupButton: function () {
        $('.js-pickup-in-store-button').prop("disabled", false);
    },

    displayAddToCartPopup: function (cartResult, statusText, xhr, formElement) {
    	$ajaxCallEvent=true;
        $('#addToCartLayer').remove();
        if (typeof ACC.minicart.updateMiniCartDisplay == 'function') {
            ACC.minicart.updateMiniCartDisplay();
        }
        var titleHeader = $('#addToCartTitle').html();

        ACC.colorbox.open(titleHeader, {
            html: cartResult.addToCartLayer,
            width: "460px"
        });

        var productCode = $('[name=productCodePost]', formElement).val();
        var quantityField = $('[name=qty]', formElement).val();

        var quantity = 1;
        if (quantityField != undefined) {
            quantity = quantityField;
        }

        var cartAnalyticsData = cartResult.cartAnalyticsData;

        var cartData = {
            "cartCode": cartAnalyticsData.cartCode,
            "productCode": productCode, "quantity": quantity,
            "productPrice": cartAnalyticsData.productPostPrice,
            "productName": cartAnalyticsData.productName
        };
        ACC.track.trackAddToCart(productCode, quantity, cartData);
    }
};

$(document).ready(function () {
	$ajaxCallEvent = true;
    ACC.product.enableAddToCartButton();
    //selecting Mobile_35 on page load
    $(".plan-type").find('h4[class="Mobile_35"]').trigger('click');
});
/*For Device PDP*/
$(document).on("click", ".plan-type", function(e) {
	var bundleInCart = $(this).find('input[type="radio"]').attr("data-bundleInCart");
	if(bundleInCart == "true") {
        $("#addToCartButton").prop("disabled", "true").addClass('disabled');
    }
    else {
        $("#addToCartButton").removeClass('disabled').removeAttr("disabled");
    }
	$(this).find('input[type="radio"]').prop('checked', true);

	var selectedRadioValue=$('input[name="deviceprice_radio"]:checked').val();
	var idSelectedId="#" + selectedRadioValue;
	var idSelectedClass="." + selectedRadioValue;

	 $(".plan-type").removeClass("selected");
	 $("h4").removeClass("selected");

	 $(idSelectedId).addClass('selected');
	 $(idSelectedClass).addClass('selected');

	 $("#deviceToolTip").addClass('hide');
	 $("#deviceplan").val(selectedRadioValue);
});
/*For PLP clickable-cta box*/
$(document).on("click", ".plp-ctabox", function(e) {
	 e.preventDefault();
     var ctaHref= $(this).find(".text-center").find("a").attr('href');
	 location.href = ctaHref;
});