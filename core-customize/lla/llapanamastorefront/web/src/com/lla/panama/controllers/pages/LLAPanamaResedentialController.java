/**
 *
 */
package com.lla.panama.controllers.pages;

import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.lla.core.util.LLACommonUtil;
import com.lla.facades.product.LLATmaProductFacade;
import com.lla.panama.controllers.ControllerConstants;


@Controller
@RequestMapping(value = "/residential")
public class LLAPanamaResedentialController extends AbstractPageController
{
	private static final Logger LOG = Logger.getLogger(LLAPanamaResedentialController.class);
	private static final String DEFAULT_CATEGORY = "4Pbundles";;
	private static final String PANAMA_RESEDENTIAL_PLP = "residential";
	private static final String INTERNET_TV_TELEPHONIA = "3Pbundles";
	private static final String INTERNET_TV = "2PInternetTv";
	private static final String INTERNET_TELEPHONIA = "2PInternetTelephone";
	private static final String INTERNET_TV_TELEPHONIA_MOBILE = "4Pbundles";

	private static final String DEVICES = "devices";
	private static final Collection<String> ALL_CATEGORIES = Arrays.asList(INTERNET_TV, INTERNET_TELEPHONIA,
			INTERNET_TV_TELEPHONIA, INTERNET_TV_TELEPHONIA_MOBILE);

	@Resource(name = "llaTmaProductFacade")
	private LLATmaProductFacade llaTmaProductFacade;

	@Resource(name = "llaCommonUtil")
	private LLACommonUtil llaCommonUtil;

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;
	
	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Autowired
	private SessionService sessionService;

	@RequestMapping(method = RequestMethod.GET)
	public String getAllProducts(@RequestParam(value = "selectedCategory", required = false, defaultValue = DEFAULT_CATEGORY)
	final String selectedCategory, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		List<ProductModel> productList = new ArrayList<>();
		if (StringUtils.isNotEmpty(selectedCategory))
		{
			productList = llaTmaProductFacade.getProductForCategory(selectedCategory);
		}
		if (CollectionUtils.isNotEmpty(productList))
		{
			model.addAttribute("productList", productList);
		}
		final String categories =configurationService.getConfiguration().getString("categories.display.tab");
		final List<String> categoryList = Stream.of(categories.split(",", -1))
				.collect(Collectors.toList());
		model.addAttribute("categoriesList", categoryList);
		if (null != categoryList)
		{
			sessionService.setAttribute("fmcProduct", Boolean.TRUE);
		}
		model.addAttribute("activeCategoryName", selectedCategory);
		model.addAttribute("pageType", PageType.PLP.name());
		model.addAttribute("cartData", cartFacade.getSessionCart());
		model.addAttribute("allProductDatas", ALL_CATEGORIES.stream().collect(Collectors.toMap(r -> r,
				productData -> llaCommonUtil.sortProductOnSequenceId(llaTmaProductFacade.getProductDataForCategory(productData)))));
		final ContentPageModel contentPage = getContentPageForLabelOrId(PANAMA_RESEDENTIAL_PLP);
		setCMSPageData(model, contentPage, selectedCategory);
		super.guidedStep(model);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/plans-Devices-plp", method = RequestMethod.GET)
	public String getAllDevicesAndPlans(
			@RequestParam(value = "selectedCategory", required = false, defaultValue = ControllerConstants.DEFAULT_PLAN_CATEGORY)
			final String selectedCategory, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		model.addAttribute("categoriesList", ControllerConstants.PLANS_DEVICES_CATEGORIES);
		model.addAttribute("activeCategoryName", selectedCategory);
		if(selectedCategory.equalsIgnoreCase(ControllerConstants.POSTPAID)) {
			model.addAttribute("devices", ControllerConstants.DEVICES);
		}
		model.addAttribute("allProductDatas", Collections.singletonList(selectedCategory).stream().collect(Collectors.toMap(r -> r,
				productData -> llaCommonUtil.sortProductOnSequenceId(llaTmaProductFacade.getProductDataForCategory(productData)))));
		final ContentPageModel contentPage = getContentPageForLabelOrId(ControllerConstants.PANAMA_PLANS_DEVICES_PLP);
		model.addAttribute("cartData", cartFacade.getSessionCart());
		setCMSPageData(model, contentPage, selectedCategory);
		super.guidedStep(model);
		return getViewForPage(model);
	}

	private void setCMSPageData(final Model model, final ContentPageModel contentPage, final String selectedCategory)
	{
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		updatePageTitle(model, contentPage);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				resourceBreadcrumbBuilder.getBreadcrumbs("panama.create.breadcrumb" + selectedCategory));
	}

	protected void updatePageTitle(final Model model, final AbstractPageModel cmsPage)
	{
		storeContentPageTitleInModel(model, getPageTitleResolver().resolveHomePageTitle(cmsPage.getTitle()));
	}
}
