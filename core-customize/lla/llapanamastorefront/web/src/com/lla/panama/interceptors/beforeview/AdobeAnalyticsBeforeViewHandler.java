package com.lla.panama.interceptors.beforeview;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AdobeAnalyticsBeforeViewHandler implements BeforeViewHandler {
    private static final String PANAMA_PRODUCTION_SITE= "panamastorefront.productionsite";
    private static final String PANAMA_STAGGING_SITE= "panamastorefront.staggingsite";
    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Override
    public void beforeView(HttpServletRequest request, HttpServletResponse response, ModelAndView modelAndView) throws Exception {
        modelAndView.addObject("isPanamaProductionSite", configurationService.getConfiguration().getString(PANAMA_PRODUCTION_SITE));
        modelAndView.addObject("isPanamaStaggingSite", configurationService.getConfiguration().getString(PANAMA_STAGGING_SITE));
    }
}
