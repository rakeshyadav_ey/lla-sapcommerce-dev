package com.lla.panama.controllers.misc;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lla.facades.address.LLAAddressFacade;
import com.lla.facades.addressmapping.data.LLAPanamaAddressMappingData;


@Controller
@RequestMapping(value = "/addressSearch")
public class AddressFieldsPopulationController
{

	private static final Logger LOGGER = Logger.getLogger(AddressFieldsPopulationController.class);

	@Resource(name = "llaAddressFacade")
	private LLAAddressFacade llaAddressFacade;

	/*
	 * method returns the list of Panama address combination for a given search term
	 */

	@RequestMapping(value = "/results", method = RequestMethod.GET)
	@ResponseBody
	public List<LLAPanamaAddressMappingData> getResultsForGivenTextSearch(@RequestParam final String freeText)
	{
		List<LLAPanamaAddressMappingData> llaPanamaAddressMappingDataList = new ArrayList<>();
		if (!StringUtils.isBlank(freeText))
		{
			llaPanamaAddressMappingDataList = llaAddressFacade.getAddressMappingForGivenText(freeText);
		}

		return llaPanamaAddressMappingDataList;
	}

}
