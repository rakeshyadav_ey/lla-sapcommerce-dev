<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<c:if test="${cmsSite.uid eq 'jamaica'}">
	<h3>${headlineText}</h3>
</c:if>
<c:if test="${cmsSite.uid eq 'panama'}">
	<div class="headline">${headlineText}</div>
</c:if>

<div class="experience-block">
	<img class="img-responsive" src="${imageUrl}" alt="${headlineText}">
	<c:if test="${cmsSite.uid eq 'panama'}">
	  <div class="descContainer">		
	</c:if>
		<div class="desc">
			<p>${subHeadlineText}</p>
			<c:if test="${buttonVisible}">
				<a class="<c:if test="${cmsSite.uid eq 'panama'}">btn btn-device</c:if> btn-yellow" href="${actionUrl}">${buttonCTAText}</a>
			</c:if>
		</div>
	<c:if test="${cmsSite.uid eq 'panama'}">
	   </div>
	</c:if>	
</div>
