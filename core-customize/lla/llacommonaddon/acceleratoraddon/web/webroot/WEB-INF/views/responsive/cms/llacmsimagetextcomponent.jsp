<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
 
<c:if test="${cmsSite.uid eq 'jamaica'}">
	<div class="container">
		<div class="llacmsimagetextcomponent offers-banner">
			<div class="offers-banner_bgimage">
				<c:forEach items="${backgroundBannerImageList}" var="media">
					<img class="img-responsive ${media.format}" src="${media.url}">
				</c:forEach>
				<div class="offers-banner_info">
					<c:forEach items="${foregroundPromoImageList}" var="media">
						<img class="img-responsive" src="${media.url}">
					</c:forEach>
					<p class="offers-banner_text">${backgroundBannerText}</p>
				</div>
			</div>
		</div>
	</div>
</c:if>
