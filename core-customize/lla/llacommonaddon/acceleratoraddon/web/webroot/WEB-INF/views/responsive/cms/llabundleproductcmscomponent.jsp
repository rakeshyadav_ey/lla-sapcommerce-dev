<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="plp" tagdir="/WEB-INF/tags/addons/llacommonaddon/responsive/plp"%>
<div class="livemore-section ${(cmsSite.uid eq 'panama' && categoryName ne 'DevicePlan') ? 'plp-livemore-section':'device-livemore-section'}">
    <c:if test="${cmsSite.uid eq 'jamaica'}">
        <div class="container">
    </c:if>
	  	<c:choose>
           <c:when test="${categoryName eq 'Internet'}">
               <plp:subHeading />
               <plp:internet />
           </c:when>
           <c:when test="${categoryName eq 'HomePhone'}">
               <plp:subHeading />
               <plp:homephone />
           </c:when>
           <c:when test="${categoryName eq 'International'}">
           	   <plp:subHeading />
               <plp:internationalphone />
           </c:when>
           <c:when test="${categoryName eq 'Prepaid'}">
                <plp:subHeading />
                <plp:prepaid />
           </c:when>
           <c:when test="${categoryName eq 'Postpaid'}">
                <plp:subHeading />
                <plp:postpaid />
           </c:when>
           <c:when test="${categoryName eq 'DevicePlan' || categoryName eq 'DeviceFMC'}">
                <plp:subHeading />
                <plp:devices/>
           </c:when>
           <c:when test="${categoryName eq 'Roaming'}">
                <plp:subHeading />
                <plp:roaming />
           </c:when>
            <c:when test="${categoryName eq '3Pbundles'}">
             <plp:subHeading />
             <plp:bundles3P />
            </c:when>
            <c:when test="${categoryName eq '2Pbundles'}">
             <plp:subHeading />
             <plp:bundles2P />
            </c:when>
            <c:when test="${categoryName eq 'TV'}">
               <plp:subHeading />
               <plp:tv />
           </c:when>
           <c:when test="${categoryName eq 'TvAddon'}">
            <plp:subHeading />
            <plp:tvAddon />
           </c:when>
           <c:when test="${categoryName eq 'additional_combos'}">
              <plp:tvAddonAdditional />
           </c:when>
           <c:otherwise>
               <plp:bundledProducts />
           </c:otherwise>
       </c:choose>
    <c:if test="${cmsSite.uid eq 'jamaica'}">
	    </div>
    </c:if>

</div>

<c:forEach var="item" items="${productList}" varStatus="productIndex">
    <c:set var="count" value="${count + 1}" scope="page"/>
    <c:if test="${count == 1}">
        <input type="hidden" id="pageCategoryName" <c:if test="${item.supercategories[0].supercategories[0].name ne null}"> data-attr-name="${item.supercategories[0].supercategories[0].name}" </c:if> >
    </c:if>
    <input class="productListForGtmData" type="hidden"
        <c:if test="${item.name ne null}"> data-attr-name="${item.name}" </c:if>
        <c:if test="${item.code ne null}"> data-attr-id="${item.code}" </c:if>
        <c:if test="${itemRecurringPriceList[productIndex.count-1] ne null}"> data-attr-price="${itemRecurringPriceList[productIndex.count-1]}" </c:if>
        <c:if test="${item.supercategories[0].name ne null}"> data-attr-brand="${item.supercategories[0].name}" data-attr-category="${item.supercategories[0].name}" </c:if>
        <c:if test="${item.supercategories[0].supercategories[0].name ne null}"> data-attr-category_2="${item.supercategories[0].supercategories[0].name}" </c:if>
        <c:if test="${item.europe1Prices[0].currency.isocode ne null}"> data-attr-productCurrencyType="${item.europe1Prices[0].currency.isocode}" </c:if>
        <c:if test="${cmsSite.uid eq 'panama'}">
            <c:if test="${categoryName eq 'DevicePlan' && devicePostpaidPrice ne null}">data-attr-price="${devicePostpaidPrice[productIndex.count-1]}"</c:if>
            <c:if test="${categoryName eq 'DeviceFMC' && deviceFMCPrice ne null}">data-attr-price="${deviceFMCPrice[productIndex.count-1]}"</c:if>
            <c:if test="${itemPayNowPriceList[productIndex.count-1] ne null}"> data-attr-actualprice="${itemPayNowPriceList[productIndex.count-1]}" </c:if>
            <c:if test="${item.supercategories[0].name == 'prepaid' && itemPayNowPriceList[productIndex.count-1] ne null}"> data-attr-price="${itemPayNowPriceList[productIndex.count-1]}" </c:if>
            <c:if test="${not empty item.manufacturerName}"> data-attr-devicebrand="${item.manufacturerName}" </c:if>
            <c:if test="${item.supercategories[0].name eq null}"> data-attr-brand="" data-attr-category="" </c:if>
            <c:if test="${item.supercategories[0].supercategories[0].name eq null}"> data-attr-category_2="" </c:if>        
        </c:if>
    >
</c:forEach>

<c:if test="${cmsSite.uid eq 'jamaica'}">
	</div>
</c:if>
<c:if test="${cmsSite.uid eq 'panama'}">
    <c:choose>
      <c:when test="${not empty breadcrumbs}">
        <c:set var="breadcrumbinfo"><spring:theme code="breadcrumb.home.panama" />,</c:set>
        <c:forEach items="${breadcrumbs}" var="breadcrumb" varStatus="status">
            <c:set var="breadcrumbinfo">${breadcrumbinfo}${breadcrumb.name}<c:if test="${not status.last}">, </c:if></c:set>
        </c:forEach>
      </c:when>
      <c:otherwise>
        <c:set var="breadcrumbinfo" value="N/A"/>
      </c:otherwise>
    </c:choose>

    <input type="hidden" id="panamaAdobeAnalyticsPageInfo"
        <c:if test="${pageType ne null}"> data-attr-pageName="${pageType}" </c:if>
        <c:if test="${breadcrumbinfo ne null}"> data-attr-breadcrumbsdata="${breadcrumbinfo}" </c:if>
        <c:if test="${user.customerId ne null}"> data-attr-userid="${user.customerId}" </c:if>
        <c:if test="${user.emailAddress ne null}"> data-attr-email="${user.emailAddress}" </c:if>
    />
</c:if>
