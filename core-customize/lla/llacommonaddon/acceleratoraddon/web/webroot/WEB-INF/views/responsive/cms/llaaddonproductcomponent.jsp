<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="addon-section">	
	<div class="container">
		<h3 class="headline col-xs-12">${headlineText}Need More Data!</h3>
		<div class="row">
			<c:forEach var="item" items="${productList}" varStatus="productIndex">
				<div class="col-xs-12 col-sm-6 addon-col">					
					<h5>${item.name}</h5>
					<div class="addon-desc col-xs-12">	
						<div>
							<!-- this data should come dynamically-->
							<spring:theme code="text.addon.description"/>
						</div>					
						<div class="price">
							<c:if test="${itemRecurringPriceList ne null}">
								<b>${itemRecurringPriceList[productIndex.count-1]}</b>&nbsp;<span><spring:theme code="cta.price.gct.GCTPerMoText" text="+GCT/mo"/></span>
							</c:if>
						</div>										
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</div>