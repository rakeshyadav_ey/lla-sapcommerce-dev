<div class="category-head-section">
    <h3 class="subheadline">${headlineText}</h3>
    <span class="sub-desc">${subHeading}</span>
</div>
