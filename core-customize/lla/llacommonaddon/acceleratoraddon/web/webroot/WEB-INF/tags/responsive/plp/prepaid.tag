<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="container plpContainer">
    <div class="row">
        <div class="cta-container">
            <c:forEach var="item" items="${productList}" varStatus="productIndex">
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                    <div class="livemore-col prepaidcta">
                        <div class="cta-header">
                            <div class="cta-header-text">
                                <div class="headertext">
                                    <c:if test="${itemPayNowPriceList ne null}"> ${itemPayNowPriceList[productIndex.count-1]}</c:if>
                                </div>
                                <div class="description"><span>x&nbsp;${item.planValidityInDays}&nbsp;<spring:theme code="text.ctaDaysplan" text="Days"/></span></div>
                            </div>
                        </div>
                    <div class="cta-prod-data">
                        <div class="char-spec spec-opts cta-content-spec">
                            <div class="prepaidplanheading">
                                <p class="planName">${item.name}</p>
                            </div>
                            <table>
                                <tbody>
                                    <c:forEach var="specCharValue" items="${item.productSpecCharacteristicValues}" varStatus="index">
                                        <c:if test="${index.count-1 <4}">
                                         <tr>
                                            <td>
                                                <div class="${specCharValue.productSpecCharacteristic.id} char-spec-col">
                                                    <div class="spec-description">
                                                        <div class="spec-heading">
                                                          <span class="spec-text">   ${specCharValue.description}</span>
                                                         </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        </c:if>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="cta-btn-block">
                        <button type="submit" class="prepaid-addcart-btn buynow-btn" id="buynow_${index.count}"
                            <c:if test="${item.code ne null}"> data-attr-item-code="${item.code}" data-attr-id="${item.code}" </c:if>
                            <c:if test="${itemPayNowPriceList[productIndex.count-1] ne null}"> data-attr-price="${itemPayNowPriceList[productIndex.count-1]}" </c:if>
                            <c:if test="${item.name ne null}"> data-arrt-name="${item.name}" data-attr-name="${item.name}" </c:if>
                            <c:if test="${item.description ne null}"> data-arrt-desc="${item.description}" </c:if>
                            <c:if test="${item.productDisclaimer ne null}"> data-arrt-termsNcond="${item.productDisclaimer}" </c:if>
                            <c:if test="${item.supercategories[0].name ne null}"> data-attr-brand="${item.supercategories[0].name}" data-attr-category="${item.supercategories[0].name}" </c:if>
                            <c:if test="${item.supercategories[0].supercategories[0].name ne null}"> data-attr-category_2="${item.supercategories[0].supercategories[0].name}" </c:if>
                        >
                            <spring:theme code="text.addToCart" text="Add to cart"/>
                        </button>
                        <div class="prepaidTextInfo">
                            <p class="cityAvailabelInfo"><spring:theme code="text.planAvailableCity"/></p>
                        </div>
                    </div>
                </div>
                <div class="prepaid-terms">
                    <spring:theme code="text.prepaid.terms" htmlEscape="false"/>
                </div>
                </div>
            </c:forEach>
        </div>
    </div>
</div>


<!-- popup design for prepaid plan selection -->
<div class="hide">
	<div id="prepaid-cta-popup" class="prepaid-cta-popup">
	    <form id="addToCartFormPrePaidPlp" class="add_to_cart_form">
            <input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
            <input type="hidden" name="productCodePost" class="productCodePost" value="">
            <input type="hidden" name="processType" value="DEVICE_ONLY">
            <button type="submit" class="buynow-btn" id="buynow_${index.count}" style="display:none">
                <spring:theme code="text.addToCart" text="Add to cart"/>
            </button>
        </form>
		<div class="livemore-section">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 prepaidPlanPopupCtabox">
			    <div class="livemore-col">

				</div>
			</div>
		</div>
		<div class="col-lg-12 prepaidplanInfoTc">
            <p class="tc">  </p>
        </div>
	</div>
</div>