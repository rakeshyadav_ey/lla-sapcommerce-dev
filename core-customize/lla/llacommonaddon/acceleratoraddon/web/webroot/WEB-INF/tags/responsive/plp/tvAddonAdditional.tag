<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="container">
       <table class="table tv-addon-table">
           <thead>
               <tr>
                   <th colspan="4">Additional Combos</th>
               </tr>
           </thead>
           <tbody>
       		<c:forEach var="item" items="${productList}" varStatus="productIndex">
	     		<tr>
	              <td>
	              	<span class="channel-name">${item.name}</span>
	              </td>
	              <td class="channel-list">
	              	<span>
	 	               <c:forEach var="specCharValue" items="${item.productSpecCharacteristicValues}" varStatus="index">
	 	                        <c:if test="${index.count ==1}">
	                                ${specCharValue.description}
	                           </c:if>
	 	                        <c:if test="${index.count >1}">
	 	                             ,${specCharValue.description}
	 	                       </c:if>
	                  </c:forEach>
	                </span>
	               </td>
	               <td>
	 	                <div class="price">
	 	                   <c:if test="${itemRecurringPriceList ne null}">
	 	                        ${itemRecurringPriceList[productIndex.count-1]} <sub><spring:theme code="cta.price.gct.GCTPerMoText" text="+GCT/mo"/></sub>
	 	                    </c:if>
	 	                </div>
		           </td>
		           <td>
		                <div class="text-center">
		                    <form id="${headlineText}-${item.name}-addToCartForm" class="add_to_cart_form" action="${contextPath}/cart/add" method="post">
		                        <input type="hidden" id="qty" name="qty" value="1">
		                        <input type="hidden" id="productCodePost" name="productCodePost" value="${item.code}">
		                        <input type="hidden" name="processType" value="DEVICE_ONLY">
		                        <button type="submit" class="btn btn-custom btn-getnow" id="buynow_${index.count}">
		                            <spring:theme code="text.getyours" text="GET YOURS"/>
		                        </button>
		                    </form>
		                </div>
		         </td>
			</tr>
        </c:forEach>
       </tbody>
    </table>
</div>
