<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:if test="${cmsSite.uid eq 'jamaica'}">
	<div class="ctabox-container row">
		<c:forEach var="item" items="${productList}" varStatus="productIndex">
			<div class="col-xs-12 col-sm-6 col-md-3 cta-card" data-productcode="${item.code}" data-productname="${item.name}" data-noofchannels="${item.channelList.size()}">
				<div class="ctabox">
					<div class="bundleName">${item.name}</div>
					<div class="specification">
						<c:forEach var="specCharValue" items="${item.productSpecCharacteristicValues}" varStatus="index">
							<p class=" internet-spec ${specCharValue.productSpecCharacteristic.id}">
								${specCharValue.description}
							</p>
						</c:forEach>
					</div>
					<div class="ctabox-btn">
						<c:if test="${item.promoFlag eq true}">
							<div class="discount">
								<spring:theme code="text.exclusive.online.promo" />
							</div>
						</c:if>
						<c:forEach var="productDataListItem" items="${productDataList}">
							<c:if test="${item.code eq productDataListItem.code}">
								<c:forEach var="offeringPrices" items="${productDataListItem.productOfferingPrices}">
									<c:if test="${null ne offeringPrices.recurringChargeEntries}">
										<c:set var="recurringCharges" value="${offeringPrices.recurringChargeEntries[0]}" />
									</c:if>
								</c:forEach>
								<c:choose>
									<c:when test="${not empty recurringCharges.discountedPrice}">
										<div class="price">
											<p class="newPrice"><small>Now&nbsp;&nbsp;</small>${recurringCharges.discountedPrice.formattedValue}<span class="tax">&nbsp;+&nbsp;<spring:theme code="configurator.cgt" />&#42;</span></p>
											<p class="oldPrice">
												<span class="linethrough">Regular price ${currentCurrency.symbol}<fmt:formatNumber type="number" maxFractionDigits="0" value="${recurringCharges.price.value}" /> + <spring:theme code="configurator.cgt" /></span>
											</p>
											<c:if test="${cmsSite.uid eq 'jamaica'}">
											<input type="hidden" class="pageProductsPlpPrice"
											<c:if test="${recurringCharges.discountedPrice.formattedValue ne null}"> data-attr-price="${recurringCharges.discountedPrice.formattedValue}" </c:if>/>
											</c:if>
										</div>
									</c:when>
									<c:otherwise>
										<div class="price">
											<p class="newPrice">
												${recurringCharges.price.formattedValue} 
												<span class="tax">&nbsp;+&nbsp;<spring:theme code="configurator.cgt" />&#42;</span>
											</p>
											<c:if test="${cmsSite.uid eq 'jamaica'}">
											<input type="hidden" class="pageProductsPlpPrice"
											<c:if test="${recurringCharges.price.formattedValue ne null}"> data-attr-price="${recurringCharges.price.formattedValue}" </c:if>/>
											</c:if>
										</div>
									</c:otherwise>
								</c:choose>
							</c:if>
						</c:forEach>
						<c:url var="addToCart" value="/addtocart"></c:url>
						<form:form id="plpBundleAddToCartForm" class="plp_bundle_add_to_cart_form" action="${addToCart}" method="post">
						   <c:if test="${cmsSite.uid eq 'jamaica'}">
						   <input type="hidden" class="pageProductsPlp"
						   <c:if test="${item.name ne null}"> data-attr-name="${item.name}" </c:if>
                           <c:if test="${item.code ne null}"> data-attr-id="${item.code}"  </c:if>
                           <c:if test="${item.supercategories[0].name ne null}"> data-attr-brand="${item.supercategories[0].name}" data-attr-category="${item.supercategories[0].name}" </c:if>
                           <c:if test="${item.supercategories[0].supercategories[0].name ne null}"> data-attr-category_2="${item.supercategories[0].supercategories[0].name}" </c:if> 
                           />
                           </c:if> 
							<input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
							<input type="hidden" name="productCodePost" value="${item.code}">
							<input type="hidden" name="processType" value="DEVICE_ONLY">
							<c:if test="${cmsSite.uid eq 'jamaica'}">
								<button type="submit" class="btn btn-primary btn-block" id="buynow_${index.count}">
									<spring:theme code="text.buyIt.now.button" text="Buy now!" />
								</button>
							</c:if>
						</form:form>
					</div>
					
				</div>
				<div class="more-benefits">
					<a id="more" class="read-link" href="javascript:void(0)"><spring:theme code="text.more.benefits"/>&nbsp;&nbsp;</a>
					<a id="less" class="read-link hide" href="javascript:void(0)"><spring:theme code="text.less.info.benefitts"/>&nbsp;&nbsp;</a>
					<div class="benefits-list hide">
						<c:forEach var="productFeatureListValue" items="${item.productFeatureList}" varStatus="index">
							<p>${productFeatureListValue.featureDescription}</p>
						</c:forEach>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>


	<div class="hidden">
		<div class="headline">
			<div class="channel-popup-title">
				<div class="bundleName"></div>
				<div class="no-of-channels"><b></b> <spring:theme code="text.bundle.channels"/></div>
			</div>
		</div>
		<div id="channels-popup" class="channels-popup">
			<div class="channels-list"></div>
			<br>
			<button class="btn btn-primary btn-block js-channels-btn"><spring:theme code="text.buyIt.now.button"/></button>
		</div>
	</div>
</c:if>

<c:if test="${cmsSite.uid ne 'jamaica'}">
<c:if test="${cmsSite.uid eq 'panama' || cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'cabletica'}">
    <div class="container plpContainer">
</c:if>
<h3 class="headline">${headlineText}</h3>
<div class="row">
	<div class="cta-container">
			<c:forEach var="item" items="${productList}" varStatus="productIndex">
				<c:choose>
					<c:when test="${productList.size()<=3}">
						<div class="col-xs-12 col-sm-4">
					</c:when>
					<c:otherwise>
						<div class="col-xs-12 col-sm-3">
					</c:otherwise>
				</c:choose>				
				<c:choose>
					<c:when test="${cmsSite.uid eq 'puertorico'}">
						<div class="livemore-col preConf-plans">
					</c:when>
					<c:otherwise>
						<div class="livemore-col">
					</c:otherwise>
				</c:choose>
				<c:if test="${cmsSite.uid eq 'panama'}">
				<div class="cta-header">
						<div class="cta-header-text">
							<div class="headertext">
								<c:if test="${itemRecurringPriceList ne null}"> ${itemRecurringPriceList[productIndex.count-1]} </c:if>
							</div>
							<div class="description"><span><spring:theme code="text.ctaMonthlyplan" text="Monthly"/></span></div>
						</div>
					</div>
				</c:if>
					<div class="cta-prod-data">
					  <c:if test="${cmsSite.uid eq 'jamaica'}">
						<h3>${item.code}</h3>
						<div class="img-block"> <img src="${item.thumbnail.url}" class="img-responsive ${productList.size()<=3 ? 'cta-img-large' : 'cta-img-small'}" />
							<h4>${item.name}</h4> </div>
					  </c:if>
					  <c:if test="${cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'cabletica'}">
                            <div class="img-block"> <img src="${item.thumbnail.url}" class="img-responsive ${productList.size()<=3 ? 'cta-img-large' : 'cta-img-small'}" />
								<div class="img-overlay"></div>
							</div>
                            <div class="cta-header">
                                <div class="cta-header-text">
                                    <div class="headertext">
                                        <p class="price"><c:if test="${itemRecurringPriceList ne null}"> ${itemRecurringPriceList[productIndex.count-1]} </c:if></p>
                                    </div>
                                    <div class="description"><span><spring:theme code="text.puertorico.ctaMonthlyplan" text="per month"/>*</span></div>
                                </div>
                            </div>
					<c:choose>
						<c:when test="${pageType eq 'CREATE_COMBINATION'}">
							<h4 class="prodTitlePurtorico">${item.description}</h4>
						</c:when>
						<c:otherwise>
							<h4 class="prodTitlePurtorico">${item.name}</h4>
						</c:otherwise>
					</c:choose>
				</c:if>
						<div class="char-spec spec-opts">
							<table>
								<tbody>
									 <c:if test="${cmsSite.uid eq 'jamaica'}"><tr></c:if>
										<c:forEach var="specCharValue" items="${item.productSpecCharacteristicValues}" varStatus="index">
											<c:if test="${index.count-1 <4}">
												 <c:if test="${cmsSite.uid eq 'panama' || cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'cabletica'}"><tr></c:if>
												<td>
													<div class="${specCharValue.productSpecCharacteristic.id} char-spec-col">
													<c:if test="${cmsSite.uid eq 'jamaica'}">${specCharValue.ctaSpecDescription} </c:if>
													<c:if test="${cmsSite.uid eq 'panama'}">
													  <div class="spec-description">
															<div class="spec-heading"><span>${specCharValue.value}</span> <span>${specCharValue.unitOfMeasure.namePlural}</span>
                                                               <span class="spec-text">${specCharValue.ctaSpecDescription}</span>
                                                            </div>
														</div>
													</c:if>
													<c:if test="${cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'cabletica'}">
                                                        <p class="itemDesc">${specCharValue.ctaSpecDescription} </p>
                                                    </c:if>
													</div>
												</td>
												 <c:if test="${cmsSite.uid eq 'panama' || cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'cabletica'}"></tr></c:if>
											</c:if>
										</c:forEach>
									 <c:if test="${cmsSite.uid eq 'jamaica'}"></tr></c:if>
								</tbody>
							</table>
						</div>
					</div>
					<div class="cta-btn-block">
						<c:if test="${cmsSite.uid eq 'puertorico'}">
							<div class="price">
								<c:if test="${itemRecurringPriceList ne null}"> 
									${itemRecurringPriceList[productIndex.count-1]} 
									<span><spring:theme code="text.puertorico.recommendedplans.permonth" text="/mes*"/></span>
								</c:if>
							</div>
						</c:if>
<%-- 						<c:if test="${cmsSite.uid eq 'jamaica'}"> --%>
<!-- 						  <div class="price"> -->
<%-- 							<c:if test="${itemRecurringPriceList ne null}"> ${itemRecurringPriceList[productIndex.count-1]} <span><spring:theme code="cta.price.gct.GCTPerMoText" text="+GCT/mo"/></span> </c:if> --%>
<!-- 						  </div> -->
<%-- 						 </c:if> --%>
						 <c:if test ="${cmsSite.uid eq 'panama' || cmsSite.uid eq 'jamaica'}">
					     <c:url var="addtoCartUrl" value="/cart/add"></c:url>
					     <div class="text-center">
							<form id="addToCartForm" class="add_to_cart_form" action="${addtoCartUrl}" method="post">
								<input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
								<input type="hidden" name="productCodePost" value="${item.code}">
								<input type="hidden" name="processType" value="DEVICE_ONLY">
								<c:if test="${cmsSite.uid eq 'jamaica'}">
								  <button type="submit" class="buynow-btn" id="buynow_${index.count}">
									<spring:theme code="text.buynow" text="Buy now!" />
									</button>
								</c:if>
								<c:if test="${cmsSite.uid eq 'panama'}">
									 <div class="buynow-btn home-btn"><a class="btn-yellow" href="panama/${currentLanguage.isocode}/p/${item.code}" >
									    <spring:theme code="text.information" text="Information"/>
									</a></div>
								</c:if>
							</form>
						</div>
						</c:if>
						<c:if test="${cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'cabletica'}">
						 <spring:url value="/cart/addBpo" var="addBpoToCartUrl" htmlEscape="false"/>
						 <spring:theme code="basket.add.to.basket" text="Add To Basket" var="addToBasketText"/>
						  <form:form action="${addBpoToCartUrl}" method="post">
                          		<button class="btn btn-block btn btn-block btn-primary btn-dark-style js-enable-btn"
						<c:if test="${cmsSite.uid eq 'puertorico'}">
                                                    <c:if test="${item.name ne null}"> data-attr-name="${item.name}" </c:if>
                                        	    <c:if test="${item.code ne null}"> data-attr-id="${item.code}" </c:if>
                                        	    <c:if test="${itemRecurringPriceList ne null}"> data-attr-price="${itemRecurringPriceList[productIndex.count-1]}" </c:if>
                                                    <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
                                                </c:if>
                          				  title="${addToBasketText}"><spring:theme code="text.iwantit" text="I want it!" />
                          		</button>
                          		<input type="hidden" name="processType" value="DEVICE_ONLY">
                          		<input type="hidden" name="rootBpoCode" value="${ycommerce:encodeHTML(item.code)}"/>
                          		<c:if test="${not empty subscriptionTermId}">
                          			<input type="hidden" name="subscriptionTermId" value="${ycommerce:encodeHTML(subscriptionTermId)}"/>
                          		</c:if>
                          		<c:if test="${not empty subscriberIdentity}">
                          			<input type="hidden" name="subscriberIdentity" value="${ycommerce:encodeHTML(subscriberIdentity)}"/>
                          		</c:if>
                          		<c:if test="${not empty subscriberBillingId}">
                          			<input type="hidden" name="subscriberBillingId" value="${ycommerce:encodeHTML(subscriberBillingId)}"/>
                          		</c:if>
                          		<c:forEach var="spoItem" items="${item.children}">
                          		   <input type="hidden" name="simpleProductOfferings" value="${ycommerce:encodeHTML(spoItem.code)}"/>
                                </c:forEach>
                          	</form:form>
						</c:if>
						<div id="addToCartTitle" class="display-none">
							<div class="add-to-cart-header">
								<div class="headline"> <span class="headline-text"><spring:theme code="basket.added.to.basket"/></span> </div>
							</div>

						</div>
						    <c:if test="${cmsSite.uid eq 'jamaica'}">
							   <a href="p/${item.code}" class="seeMoreBundle">
							     <spring:theme code="text.seemore" text="seemore" />
						       </a>
						    </c:if>
					</div>
				</div>
				</div>
			</c:forEach>
    </div>
	<c:if test="${cmsSite.uid eq 'puertorico'}">
		<p class="regulartory-info col-xs-12"><spring:theme code="basket.page.cart.terms.text" text="regulatoryInfo" /></p>
	</c:if>
</div>
<c:if test="${cmsSite.uid eq 'panama' || cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'cabletica'}">
</div>
</c:if>
</c:if>
