<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var = "region" value = "${fn:replace(regionCode, ' ', '')}" />
<div class="row">
<div class="cta-container">
    <c:forEach var="item" items="${productList}" varStatus="productIndex">
        <c:set var = "isMega" value = "${fn:startsWith(item.name, 'Mega')}" />
        <c:choose>
	        <c:when test="${productList.size()<=3}">
	            <div class="col-xs-12 col-sm-4">
	        </c:when>
	        <c:otherwise>
	            <div class="col-xs-12 col-sm-3">
	        </c:otherwise>
	    </c:choose>
            <div class="livemore-col"
            <c:if test="${not fn:containsIgnoreCase(item.regions, region)}">
                style="display:none"
            </c:if>
            >
            <div class="cta-prod-data">
                <h3 class='<c:if test="${isMega}">internet-mega </c:if> <c:if test="${item.popularFlag}">promotion-product</c:if>'>${item.name}</h3>
                <div class="img-block">
                    <img src="${item.thumbnail.url}" class="img-responsive ${productList.size()<=3 ? 'cta-img-large' : 'cta-img-small'}"/>
                </div>
                <div class="char-spec">
                    <table>
                        <tbody>
                          <tr>
                            <td>
	                            <div class="char-spec-col
	                            <c:if test="${isMega}">internet-mega-speed</c:if>
	                            <c:if test="${not isMega}">internet-speed</c:if>
	                            ">
	                                <c:forEach var="specCharValue" items="${item.productSpecCharacteristicValues}" varStatus="index">
							            <div>
								            <c:if test="${specCharValue.productSpecCharacteristic.id eq 'download_speed'}">
							                   <span class="glyphicon glyphicon-arrow-down download-icon"></span>                                                                                       
							                   <span class="speed-value">${specCharValue.value}&nbsp;${specCharValue.unitOfMeasure.namePlural}</span>
								            </c:if>
							            </div>
							 		</c:forEach>
							 		<c:forEach var="specCharValue" items="${item.productSpecCharacteristicValues}" varStatus="index">
							            <div>
								            <c:if test="${specCharValue.productSpecCharacteristic.id eq 'upload_speed'}">
							                    <span class="glyphicon glyphicon-arrow-up upload-icon"></span>
							                    <span class="speed-value">${specCharValue.value}&nbsp;${specCharValue.unitOfMeasure.namePlural}</span>
								             </c:if>
							             </div>
							   		</c:forEach>
	                            </div>
                            </td>
                            <td>
                                <div class='char-spec-col ${fn:replace(item.description, " ", "_")}'>
                                    <div><span class="ideal-text">ideal for</span></div>
                                    <div><span class="speed-value">${fn:substringAfter(item.description, "for ")}</span></div>
                                </div>
                            </td>
                          </tr>
                        </tbody>
                    </table>
                </div>
                </div>
                <div class="cta-btn-block">
                <div class="price">
                   <c:if test="${itemRecurringPriceList ne null}">
                        ${itemRecurringPriceList[productIndex.count-1]} <span><spring:theme code="cta.price.gct.GCTPerMoText" text="+GCT/mo"/></span>
                    </c:if>
                </div>
                <div class="text-center">
                    <form id="${headlineText}-${item.name}-addToCartForm" class="add_to_cart_form" action="${contextPath}/cart/add" method="post">
                        <input type="hidden" id="qty" name="qty" value="1">
                        <input type="hidden" id="productCodePost" name="productCodePost" value="${item.code}">
                        <input type="hidden" name="processType" value="DEVICE_ONLY">
                        <button type="submit" class="buynow-btn" id="buynow_${index.count}">
                            <spring:theme code="text.buynow"/>
                        </button>
                    </form>
                </div>
                <div id="addToCartTitle" class="display-none">
                    <div class="add-to-cart-header">
                        <div class="headline">
                            <span class="headline-text"><spring:theme code="basket.added.to.basket"/></span>
                        </div>
                    </div>
                </div>
                <c:if test="${itemsPDPLinkList ne null}">
                <a href="${contextPath}/${itemsPDPLinkList[productIndex.count-1]}" class="seeMoreBundle">see more</a>
                </c:if>
            </div>
        </div>
        </div>
    </c:forEach>
    </div>
</div>