<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="row">
	<div class="cta-container">
	<c:forEach var="item" items="${productList}" varStatus="productIndex">
		<c:choose>
	        <c:when test="${productList.size()<=3}">
	            <div class="col-xs-12 col-sm-4">
	        </c:when>
	        <c:otherwise>
	            <div class="col-xs-12 col-sm-3">
	        </c:otherwise>
	    </c:choose>
			<div class="livemore-col">
			<div class="cta-prod-data">
				<h3 class='<c:if test="${item.popularFlag}">promotion-product</c:if>'>${item.name}</h3>
				
				<div class="img-block">
					<img src="${item.thumbnail.url}" class="img-responsive ${productList.size()<=3 ? 'cta-img-large' : 'cta-img-small'}"/>
				</div>
				<div class="char-spec spec-opts">
					<table>
						<tbody>
							<tr>
								<c:forEach var="specCharValue" items="${item.productSpecCharacteristicValues}" varStatus="index">
									<c:if test="${index.count-1 <4}">
										<td>
											<div class="${specCharValue.productSpecCharacteristic.id} char-spec-col">
												${specCharValue.description}
											</div>
										</td>
									</c:if>
								</c:forEach>
							</tr>
						</tbody>
					</table>
				</div>
				</div>
				<div class="cta-btn-block">
				<div class="price">
					<c:if test="${itemRecurringPriceList ne null}">
						${itemRecurringPriceList[productIndex.count-1]} <span><spring:theme code="cta.price.gct.GCTPerMoText" text="+GCT/mo"/></span>
					</c:if>
				</div>
				<div class="text-center">
					<form id="addToCartForm" class="add_to_cart_form" action="${contextPath}/cart/add" method="post">
						<input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
						<input type="hidden" name="productCodePost" value="${item.code}">
						<input type="hidden" name="processType" value="DEVICE_ONLY">
						<button type="submit" class="buynow-btn" id="buynow_${index.count}" >
							<spring:theme code="text.buynow" text="Buy now!"/>
						</button>
					</form>
				</div>
				<div id="addToCartTitle" class="display-none">
					<div class="add-to-cart-header">
						<div class="headline">
							<span class="headline-text"><spring:theme code="basket.added.to.basket"/></span>
						</div>
					</div>
				</div>
				<a href="p/${item.code}" class="seeMoreBundle"> <spring:theme code="text.seemore" text="seemore"/></a>
			</div>
		</div>
		</div>
	</c:forEach>
	</div>
</div>