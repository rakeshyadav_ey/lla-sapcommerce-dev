<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<div class="row">
<div class="cta-container">
	<c:forEach var="item" items="${productList}" varStatus="productIndex">
		<c:choose>
	        <c:when test="${productList.size()<=3}">
	            <div class="col-xs-12 col-sm-4">
	        </c:when>
	        <c:otherwise>
	            <div class="col-xs-12 col-sm-3">
	        </c:otherwise>
	    </c:choose>
			<div class="livemore-col">
			<div class="cta-prod-data">
				<h3 class='<c:if test="${item.popularFlag}">promotion-product</c:if>'>${item.name}</h3>
				<div class="img-block">

				        <c:set var="count" value="0" scope="page" />
                        <c:forEach var="channelList" items="${item.channelList}"
                            varStatus="index">
                            <c:if test="${channelList.channelImage != null && count < 6}">
                                <c:set var="count" value="${count + 1}" scope="page"/>
                                <td>
                                    <div class="tv-img col-xs-4">
                                        <img src="${channelList.channelImage.url}"
                                            class="img-responsive ${productList.size()<=3 ? 'cta-img-large' : 'cta-img-small'}" />
                                    </div>
                                </td>
                            </c:if>
                        </c:forEach>
                </div>
				<div class="clearfix"></div>
				<c:if test="${itemsPDPLinkList ne null}">
					<div class="more-channels">
						<a href="${contextPath}/${itemsPDPLinkList[productIndex.count-1]}/?activeTab=channels"
							class="seeMoreBundle">See all Channels</a>
					</div>
				</c:if>
				<div class="char-spec spec-opts">
					<table>
						<tbody>
							<tr>

								<c:forEach var="specCharValue" items="${item.productSpecCharacteristicValues}">
									<c:if test="${specCharValue.productSpecCharacteristic.id == 'sd_channels'}">
										<td>
                                                <div
                                                    class="${specCharValue.productSpecCharacteristic.id} char-spec-col">
                                                    ${specCharValue.ctaSpecDescription}
                                                </div>
										</td>
									</c:if>
									</c:forEach>
									<c:forEach var="specCharValue" items="${item.productSpecCharacteristicValues}">
									<c:if test="${specCharValue.productSpecCharacteristic.id == 'audio'}">
                                            <td>
                                                <div
                                                    class="${specCharValue.productSpecCharacteristic.id} char-spec-col">
                                                    ${specCharValue.ctaSpecDescription}
                                                </div>
                                            </td>
                                     </c:if>
                                     </c:forEach>
                                     <c:forEach var="specCharValue" items="${item.productSpecCharacteristicValues}">
                                     <c:if test="${specCharValue.productSpecCharacteristic.id == 'cloud_recording'}">
                                             <td>
                                                 <div
                                                     class="${specCharValue.productSpecCharacteristic.id} char-spec-col">
                                                     ${specCharValue.ctaSpecDescription}
                                                 </div>
                                             </td>
                                      </c:if>
                                      </c:forEach>
                                      <c:forEach var="specCharValue" items="${item.productSpecCharacteristicValues}">
                                     <c:if test="${specCharValue.productSpecCharacteristic.id == 'hd_channels'}">
                                           <td>
                                               <div
                                                   class="${specCharValue.productSpecCharacteristic.id} char-spec-col">
                                                   ${specCharValue.ctaSpecDescription}
                                               </div>
                                           </td>
                                    </c:if>
								</c:forEach>
							</tr>
						</tbody>
					</table>
				</div>
				</div>
				<div class="cta-btn-block">
				<div class="price">
					<c:if test="${itemRecurringPriceList ne null}">
                                ${itemRecurringPriceList[productIndex.count-1]} <span><spring:theme code="cta.price.gct.GCTPerMoText" text="+GCT/mo"/></span>
					</c:if>
				</div>
				<div class="text-center">
					<form id="addToCartForm" class="add_to_cart_form"
						action="${contextPath}/cart/add" method="post">
						<input type="hidden" maxlength="3" size="1" id="qty" name="qty"
							class="qty js-qty-selector-input" value="1"> <input
							type="hidden" name="productCodePost" value="${item.code}">
						<input type="hidden" name="processType" value="DEVICE_ONLY">
						<button type="submit" class="buynow-btn" id="buynow_${index.count}">
							<spring:theme code="text.buynow" text="Buy now!" />
						</button>
					</form>
				</div>
				<div id="addToCartTitle" class="display-none">
					<div class="add-to-cart-header">
						<div class="headline">
							<span class="headline-text"><spring:theme code="basket.added.to.basket" /></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</c:forEach>
	</div>
</div>
<div class="tv-addon--component">
	<div class="headline">
		<span class="headline-text"><spring:theme code="tv.included.text" /></span>
	</div>
	<div class="row">
		<div class="yCmsComponent yComponentWrapper  col-xs-12 col-sm-6">
			<div class="tv-addon-experience">
				<h3 class="subhead">
					<spring:theme code="tv.flow.on.demand" />
				</h3>
				<div class="tv-experience-block">
					<div class="col-xs-3">
						<img class="img-responsive"
							src="${contextPath}/_ui/responsive/theme-jamaica/images/FlowOnDemand.svg"
							alt="Flow On Demand">
					</div>
					<div class="col-xs-9">
						<p class="desc"><spring:theme code="tv.flow.on.demand.text" /></p>
						<div class="learnmore-btn">
							<a class="btn btn-custom btn-learn-more"
								href="https://discoverflow.co/jamaica/tv/video-on-demand"
								type="button">Learn more</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="yCmsComponent yComponentWrapper  col-xs-12 col-sm-6">
			<div class="tv-addon-experience">
				<h3 class="subhead">
	                  	<spring:theme code="tv.flow.to.go" />
	                  </h3>
				<div class="tv-experience-block">
					<div class="col-xs-3">
						<img class="img-responsive"
							src="${contextPath}/_ui/responsive/theme-jamaica/images/FlowToGo.svg">
					</div>
					<div class="col-xs-9">
						<p class="desc"><spring:theme code="tv.flow.to.go.text" /></p>
						<div class="learnmore-btn">
							<a class="btn btn-custom btn-learn-more"
								href="https://discoverflow.co/jamaica/tv/flow-togo"
								type="button">Learn more</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

