<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<c:url value="/cart/add" var="addToCartUrl"/>

<div class="row">
<div class="container">
	<div class="product__listing product__list prod_grid_view">
	<c:forEach var="item" items="${devicesProductDataList}" varStatus="productIndex">
		<li class="grid_view product__list--items col-lg-4 col-md-4 col-sm-4 devicesFmcPdp ${cmsSite.uid eq 'panama' && product.bundlePresentInCart ? 'disabled ':''}">
			<div class="full-grid">
				<div class="prod-wrapper">
					<c:if test="${item.promoFlag eq 'true'}"> 
						<span class="prod-card-bubble">
							<spring:theme code="text.online.price"/>
						</span>
					</c:if>	
					<div class="product-info">
						<a class="product__list--thumb prod-img">
                        	<product:productPrimaryImage product="${item}" format="cartIcon"/>
                        </a>
						<div class="prod-content">
							<span class="p-brand">${item.manufacturer}</span>
							<p class="product__list--name prod-name">${item.name}</p>
							<div class="prod-data">
								<div class="product__listing--price">
									<div class="product-price prod-price">
										<span class="p-code"><spring:theme code="text.device.fullPrice"/>&nbsp;&nbsp;<s>${deviceNoOfferPrice[productIndex.count-1]}</s></span>
										<span class="no-offer-price hide">${deviceNoOfferPrice[productIndex.count-1]}</span>
                                        <c:forEach items="${item.devicePriceList}" var="priceList">
                                            <c:if test="${priceList.planCode eq product.code}">
                                                <span class="actual-price">${priceList.formattedValue}</span>
                                            </c:if>

                                            </c:forEach>

                                        <span class="p-code"><spring:theme code="text.device.PriceWithPlan"/></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="prod-btn-info">
					    <form id="addToCartForm" class="add_to_cart_form device_tooltip" action="${addToCartUrl}" method="post">
					        <input type="hidden" id="qty" name="qty" value="1">
                            <input type="hidden" id="productCodePost" name="productCodePost" value="${item.code}">
                            <input type="hidden" name="processType" value="DEVICE_ONLY">
                            <button type="submit" class="btn add-cart-btn ${cmsSite.uid eq 'panama' && product.bundlePresentInCart ? 'disabled ':''}" id="buynow_${productIndex.count}">
                                <spring:theme code="text.device.addToCart"/>
                            </button>
                        
					      
                            <c:if test="${categoryName eq 'DevicePlan'}">
                                <span class="tooltiptext"><spring:theme code="plp.device.tooltip.text" text="select a plan to buy this mobile"/></span>
                            </c:if>
                            <c:if test="${categoryName eq 'DeviceFMC'}">
                                <span class="tooltiptext"><spring:theme code="fmc.device.tooltip.text" text="Select a bundles first to add this Smartphone"/></span>
                            </c:if>
                        </form>
                    </div>
                    <c:if test="${categoryName eq 'DevicePlan'}">
	                    <a class="features-link" href="p/${item.code}">
	                        <spring:theme code="text.information"/>
	                    </a>
					</c:if>
					<div id="addToCartTitle" class="display-none">
							<div class="add-to-cart-header">
								<div class="headline"> <span class="headline-text"><spring:theme code="basket.added.to.basket"/></span> </div>
							</div>
						</div>
					</div>
			</div>
		</li>
		</c:forEach>
	</div>
</div>
</div>
