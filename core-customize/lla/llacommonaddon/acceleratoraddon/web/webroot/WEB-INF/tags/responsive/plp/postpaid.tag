<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:if test="${cmsSite.uid eq 'panama'}">
    <div class="container plpContainer">
</c:if>
<div class="row">
<div class="cta-container">
	<c:forEach var="item" items="${productList}" varStatus="productIndex">
		<c:if test="${cmsSite.uid eq 'jamaica'}">
			<c:choose>
		        <c:when test="${productList.size()<=3}">
		            <div class="col-xs-12 col-sm-4">
		        </c:when>
		        <c:otherwise>
		            <div class="col-xs-12 col-sm-3">
		        </c:otherwise>
		    </c:choose>
	    </c:if>
	    <c:if test="${cmsSite.uid eq 'panama'}">
	    	<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
	    </c:if>
			<div class="livemore-col">
			<c:if test="${cmsSite.uid eq 'panama'}">
                <div class="cta-header">
                    <div class="cta-header-text">
                        <div class="headertext">
                            <c:if test="${itemRecurringPriceList ne null}"> ${itemRecurringPriceList[productIndex.count-1]}</c:if>
                        </div>
                        <div class="description"><span><spring:theme code="text.ctaMonthlyplan" text="Monthly"/></span></div>
                    </div>
                </div>
            </c:if>
			<div class="cta-prod-data">
				<c:if test="${cmsSite.uid eq 'jamaica'}">
                    <h3 class='<c:if test="${item.popularFlag}">promotion-product</c:if>'>${item.name}</h3>
                    <div class="img-block">
                        <img src="${item.thumbnail.url}" class="img-responsive ${productList.size()<=3 ? 'cta-img-large' : 'cta-img-small'}"/>
                    </div>
                 </c:if>
				<div class="char-spec spec-opts ${productList.size()>=3?'cta-content-spec': ''}">
					<table>
						<tbody>
							<tr>
								<c:if test="${cmsSite.uid eq 'jamaica'}">
									<c:forEach var="specCharValue" items="${item.productSpecCharacteristicValues}" varStatus="index">
										<c:if test="${index.count-1 <4}">
											<td>
												<div class="${specCharValue.productSpecCharacteristic.id} char-spec-col">
													${specCharValue.ctaSpecDescription}
												</div>
											</td>
										</c:if>
									</c:forEach>
								</c:if>
								<c:if test="${cmsSite.uid eq 'panama'}">
									<td>
										<p class="planName">${item.name}</p>
										<p class="planInfo">${item.description}</p>
									</td>
								</c:if>
							</tr>
						</tbody>
					</table>
				</div>
				</div>
				<div class="cta-btn-block">
				<c:if test="${cmsSite.uid eq 'jamaica'}">
                    <div class="price">
                        <c:if test="${itemRecurringPriceList ne null}"> ${itemRecurringPriceList[productIndex.count-1]} <span><spring:theme code="cta.price.gct.GCTPerMoText" text="+GCT/mo"/></span> </c:if>
                    </div>
					<div class="text-center">
					<form id="addToCartForm" class="add_to_cart_form" action="${contextPath}/${currentLanguage.isocode}/cart/add" method="post">
							<input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
							<input type="hidden" name="productCodePost" value="${item.code}">
							<input type="hidden" name="processType" value="DEVICE_ONLY">
							<button type="submit" class="buynow-btn" id="buynow_${index.count}" >
								<c:if test="${cmsSite.uid eq 'jamaica'}"><spring:theme code="text.buynow" text="Buy now!"/></c:if>
							</button>
						</form>
					</div>
					<div id="addToCartTitle" class="display-none">
						<div class="add-to-cart-header">
							<div class="headline">
								<span class="headline-text"><spring:theme code="basket.added.to.basket"/></span>
							</div>
						</div>
					</div>
                   <a href="p/${item.code}" class="seeMoreBundle"><spring:theme code="text.seemore" text="seemore" /></a>
                </c:if>
				<c:if test="${cmsSite.uid eq 'panama'}">
					<button type="submit" class="addcart-btn buynow-btn" id="buynow_${index.count}" 
						data-attr-item-code="${item.code}" 
						data-attr-price="${itemRecurringPriceList[productIndex.count-1]}"
						data-arrt-name="${item.name}"
						data-arrt-desc="${item.description}"
					>
						
						<spring:theme code="text.addToCart" text="Add to cart"/>
					</button>
				</c:if>
			</div>
		</div>
		<c:if test="${cmsSite.uid eq 'panama'}">
            <div class="postpaid-terms">
                <spring:theme code="text.postpaid.terms" htmlEscape="false"/>
            </div>
        </c:if>
		</div>  


	</c:forEach>
</div>
</div>
<c:if test="${cmsSite.uid eq 'panama'}">
</div>
<!-- popup design for postpaid plan selection -->
<div class="hide">
	<div id="postpaid-cta-popup" class="postpaid-cta-popup">
		<div class='headline'>
			<p class='headline-text'><spring:theme code="text.chooseplan" text="You choose a"/>&nbsp;<span class="planhead"></span></p> 
		</div>
		<div class="livemore-section">
			<div class="col-xs-12">
			    <div class="livemore-col">
					<div class="cta-header">
	                    <div class="cta-header-text">
	                        <div class="headertext">
                        	</div>
	                        <div class="description"><span><spring:theme code="text.ctaMonthlyplan" text="Monthly"/></span></div>
	                    </div>
		            </div>
		            <div class="cta-prod-data">
						<div class="char-spec spec-opts ">
							<table>
								<tbody>
									<tr>
										<td>
											<p class="planName"></p>
											<p class="planInfo"></p>
											</td>
										</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<p class="plp-decision-text"><spring:theme code="plp.cta.plan.decision.text" text="Do you want a new Smartphone?"/></p>
		<div class="postpaid-popup-btn-group row">
			<div class="col-xs-12 col-sm-6">
				<div class="text-center">
					<form id="addToCartFormPostPaidPlp" class="add_to_cart_form" method="post">
						<input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
						<input type="hidden" name="productCodePost" class="productCodePost" value="">
						<input type="hidden" name="processType" value="DEVICE_ONLY">
						<button type="submit" class="buynow-btn plp-onlyPlan-btn plp-pop-btn btn-white" id="buynow_${index.count}" >
							<spring:theme code="plp.cta.plan.text" text="NO, I just want the PLAN"/>
						</button>
					</form>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<button class="btn-custom plp-pop-btn btn-plan-mobile">
	                <spring:theme code="plp.cta.phone.text" text="iyes, I WANT A NEW smartphone!"/>
	            </button>
			</div>
		</div>
	</div>
</div>
</c:if>
