<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<c:if test="${cmsSite.uid eq 'panama'}">
    <div class="container plpContainer">
</c:if>
<div class="row">
	<div class="cta-container">
	<c:forEach var="item" items="${productList}" varStatus="productIndex">
		<c:if test="${cmsSite.uid eq 'jamaica'}">
			<c:choose>
		        <c:when test="${productList.size()<=3}">
		            <div class="col-xs-12 col-sm-4">
		        </c:when>
		        <c:otherwise>
		            <div class="col-xs-12 col-sm-3">
		        </c:otherwise>
		    </c:choose>
	    </c:if>
	    <c:if test="${cmsSite.uid eq 'panama'}">
	    	<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
	    </c:if>
			<div class="livemore-col">
			<c:if test="${cmsSite.uid eq 'panama'}">
                <div class="cta-header">
                    <div class="cta-header-text">
                        <div class="headertext">
                            <c:if test="${itemRecurringPriceList ne null}"> ${itemRecurringPriceList[productIndex.count-1]} </c:if>
                        </div>
                        <div class="description"><span><spring:theme code="text.ctaMonthlyplan" text="Monthly"/></span></div>
                    </div>
                </div>
            </c:if>
			<div class="cta-prod-data">
			    <c:if test="${cmsSite.uid eq 'jamaica'}">
                    <h3 class='<c:if test="${item.popularFlag}">promotion-product</c:if>'>${item.name}</h3>
                    <div class="img-block">
                        <img src="${item.thumbnail.url}" class="img-responsive ${productList.size()<=3 ? 'cta-img-large' : 'cta-img-small'}"/>
                    </div>
                 </c:if>
				<div class="char-spec spec-opts ${productList.size()>=3?'cta-content-spec': ''}">
					<table>
						<tbody>
							<c:if test="${cmsSite.uid eq 'jamaica'}"><tr></c:if>
                                <c:forEach var="specCharValue" items="${item.productSpecCharacteristicValues}" varStatus="index">
                                    <c:if test="${index.count-1 <4}">
                                         <c:if test="${cmsSite.uid eq 'panama'}"><tr></c:if>
                                        <td>
                                            <div class="${specCharValue.productSpecCharacteristic.id} char-spec-col">
                                            <c:if test="${cmsSite.uid eq 'jamaica'}">${specCharValue.ctaSpecDescription} </c:if>
                                            <c:if test="${cmsSite.uid eq 'panama'}">
                                              <div class="spec-description">
                                                    <div class="spec-heading"><span>${specCharValue.value}</span> <span>${specCharValue.unitOfMeasure.namePlural}</span>
                                                       <span class="spec-text">${specCharValue.ctaSpecDescription}</span>
                                                    </div>
                                                </div>
                                            </c:if>
                                            </div>
                                        </td>
                                         <c:if test="${cmsSite.uid eq 'panama'}"></tr></c:if>
                                    </c:if>
                                </c:forEach>
                             <c:if test="${cmsSite.uid eq 'jamaica'}"></tr></c:if>
						</tbody>
					</table>
				</div>
				</div>
				<div class="cta-btn-block">
				<c:if test="${cmsSite.uid eq 'jamaica'}">
                    <div class="price">
                        <c:if test="${itemRecurringPriceList ne null}"> ${itemRecurringPriceList[productIndex.count-1]} <span><spring:theme code="cta.price.gct.GCTPerMoText" text="+GCT/mo"/></span> </c:if>
                    </div>
                 </c:if>
				<div class="text-center">
					<form id="addToCartForm" class="add_to_cart_form" action="${contextPath}/${currentLanguage.isocode}/cart/add" method="post">
						<input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
						<input type="hidden" name="productCodePost" value="${item.code}">
						<input type="hidden" name="processType" value="DEVICE_ONLY">
						<c:if test="${cmsSite.uid eq 'jamaica'}">
						  <button type="submit" class="buynow-btn" id="buynow_${index.count}" >
							<spring:theme code="text.buynow" text="Buy now!"/>						
						  </button>
						</c:if>
						  <c:if test="${cmsSite.uid eq 'panama'}">
						   <div class="buynow-btn home-btn">
						      <a class="btn-yellow" href="p/${item.code}">
							    <spring:theme code="text.information" text="Information"/>
							   </a>
							</div>
						  </c:if>
						
					</form>
				</div>
				<div id="addToCartTitle" class="display-none">
					<div class="add-to-cart-header">
						<div class="headline">
							<span class="headline-text"><spring:theme code="basket.added.to.basket"/></span>
						</div>
					</div>
				</div>
				<c:if test="${cmsSite.uid eq 'jamaica'}">
                   <a href="p/${item.code}" class="seeMoreBundle"><spring:theme code="text.seemore" text="seemore" /></a>
                </c:if>
			</div>
		</div>
		</div>
	</c:forEach>
	</div>
</div>
<c:if test="${cmsSite.uid eq 'panama'}">
</div>
</c:if>