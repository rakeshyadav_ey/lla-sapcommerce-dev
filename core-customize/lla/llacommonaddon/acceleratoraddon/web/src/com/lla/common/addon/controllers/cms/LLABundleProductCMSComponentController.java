/**
 *
 */
package com.lla.common.addon.controllers.cms;

import com.lla.common.addon.controllers.LlacommonaddonControllerConstants;
import com.lla.common.addon.model.LLABundleProductCMSComponentModel;
import com.lla.core.enums.DevicePriceType;
import com.lla.core.util.LLACommonUtil;
import com.lla.facades.product.LLATmaProductFacade;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.addonsupport.controllers.cms.AbstractCMSAddOnComponentController;
import de.hybris.platform.b2ctelcoservices.model.TmaBundledProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cmsfacades.util.models.CatalogVersionModelMother;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.subscriptionservices.model.RecurringChargeEntryModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionPricePlanModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static com.lla.core.constants.LlaCoreConstants.SESSION_REGION;


/**
 * @author GZ132VA
 *
 */
@Controller("LLABundleProductCMSComponentController")
@RequestMapping(value = LlacommonaddonControllerConstants.Actions.Cms.LLABundleProductCMSComponent)
public class LLABundleProductCMSComponentController extends AbstractCMSAddOnComponentController<LLABundleProductCMSComponentModel>
{
	public static final String PAGE_TYPE = "pageType";
	private static final String COMPONENT_PRODUCT_DEVICE_PLAN_PLP = "product_device_plan_plp";
	private static final String COMPONENT_PRODUCT_DEVICE_FMC_PDP = "product_device_fmc_pdp";
	 private static final String DEVICES="devices";
	 
	 
	@Resource(name = "priceDataFactory")
	PriceDataFactory priceDataFactory;

	@Autowired
	CommerceCommonI18NService commerceCommonI18NService;

	@Autowired
	SessionService sessionService;

	@Autowired
	private UrlResolver<ProductModel> productModelUrlResolver;

	@Autowired
	private LLATmaProductFacade llaTmaProductFacade;

	@Autowired
	private CatalogVersionService catalogVersionService;

	@Resource(name = "llaCommonUtil")
	private LLACommonUtil llaCommonUtil;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final LLABundleProductCMSComponentModel component)
	{
		model.addAttribute("headlineText", component.getHeadlineText());
		model.addAttribute("subHeading", component.getSubHeading());
		model.addAttribute("categoryName", component.getName());
		final List<TmaProductOfferingModel> productModelList=component.getProducts();
		if (CollectionUtils.isNotEmpty(productModelList))
		{
			model.addAttribute("productList", productModelList);
			model.addAttribute("itemRecurringPriceList", fetchRecurringPriceForBundle(productModelList));
			model.addAttribute("itemPayNowPriceList",fetchPayNowPrice(productModelList));
			model.addAttribute("itemsPDPLinkList", fetchPDPForProducts(productModelList));
		}
		if(llaCommonUtil.isSiteJamaica())
		{
			final List<ProductData> dataList= llaTmaProductFacade.getProductDataListForModels(productModelList);
			model.addAttribute("productDataList",dataList);
			if(StringUtils.isNotEmpty(sessionService.getAttribute(SESSION_REGION))) {
				model.addAttribute("regionCode", sessionService.getAttribute(SESSION_REGION));
			}
		}
		if(llaCommonUtil.isSitePanama())
		{
		//	model.addAttribute("devicesProductDataList", llaCommonUtil.sortProductOnSequenceId(llaTmaProductFacade.getProductDataForCategory(DEVICES)));
			if(null != component.getName())
		        setPageType(model, component);
			if(StringUtils.isNotEmpty(component.getUid()) && (component.getUid().equalsIgnoreCase(COMPONENT_PRODUCT_DEVICE_PLAN_PLP) || component.getUid().equalsIgnoreCase(COMPONENT_PRODUCT_DEVICE_FMC_PDP)))
			{
				model.addAttribute("headlineText", component.getHeadlineText());
				model.addAttribute("subHeading", component.getSubHeading());
				model.addAttribute("categoryName", component.getName());
				final List<TmaProductOfferingModel> devicesProductModelList=component.getProducts();
                model.addAttribute("devicesProductDataList", llaTmaProductFacade.getProductDataListForModels(devicesProductModelList));
				if (CollectionUtils.isNotEmpty(devicesProductModelList)) {
					model.addAttribute("devicesProductList", devicesProductModelList);
					model.addAttribute("deviceNoOfferPrice",fetchPriceForDevice(devicesProductModelList,DevicePriceType.NOOFFER));
					model.addAttribute("devicePostpaidPrice",fetchPriceForDevice(devicesProductModelList,DevicePriceType.POSTPAID));
					model.addAttribute("deviceFMCPrice",fetchPriceForDevice(devicesProductModelList,DevicePriceType.FMC));
				}
			}
		}
	}
	
	protected void setPageType(Model model, LLABundleProductCMSComponentModel component) {
		if(component.getName().equalsIgnoreCase("Postpaid"))
			model.addAttribute(PAGE_TYPE, PageType.POSTPAID_HOME.name());
		else if(component.getName().equalsIgnoreCase("Prepaid"))
			model.addAttribute(PAGE_TYPE, PageType.PREPAID_HOME.name());
		else
			model.addAttribute(PAGE_TYPE, PageType.BUNDLE_HOME.name());
	}
	
	/**
	 *
	 * @param productModelList
	 * @return
	 */
	private List<String> fetchPayNowPrice(List<TmaProductOfferingModel> productModelList) {
		final List<String> itemPayNowPriceList = new ArrayList();
		for (final TmaProductOfferingModel item : productModelList)
		{

			final Collection<PriceRowModel> priceRowModelCollection = item.getEurope1Prices();
			for (final PriceRowModel priceRow : priceRowModelCollection)
			{
      				final Double price = priceRow.getPrice();
					final PriceData priceData = priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(price.doubleValue()),
							commerceCommonI18NService.getCurrentCurrency().getIsocode());
					itemPayNowPriceList.add(StringUtils.chomp(priceData.getFormattedValue(),".00"));
					break;
			}
		}
		return itemPayNowPriceList;
	}


	/**
	 * @param productModelList
	 * @return
	 */
	private List<String> fetchRecurringPriceForBundle(final List<TmaProductOfferingModel> productModelList)
	{
		final List<String> itemRecurringPriceList = new ArrayList();
		for (final TmaProductOfferingModel item : productModelList)
		{

			final Collection<PriceRowModel> priceRowModelCollection = item.getEurope1Prices();
			for (final PriceRowModel priceRow : priceRowModelCollection)
			{
				if (priceRow instanceof SubscriptionPricePlanModel) {
					final SubscriptionPricePlanModel pricePlan = (SubscriptionPricePlanModel) priceRow;
					final Collection<RecurringChargeEntryModel> recurringChargeEntries = pricePlan.getRecurringChargeEntries();
					final RecurringChargeEntryModel model = recurringChargeEntries.iterator().next();
					final Double price = model.getPrice();
					final PriceData priceData = priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(price.doubleValue()),
							commerceCommonI18NService.getCurrentCurrency().getIsocode());
					if (llaCommonUtil.isSitePuertorico()) {
						itemRecurringPriceList.add(StringUtils.join(new String[]{commerceCommonI18NService.getCurrentCurrency().getSymbol(), String.valueOf(priceData.getValue())}));
					} else{
						itemRecurringPriceList.add(StringUtils.chomp(priceData.getFormattedValue(), ".00"));
					}
					break;
				}
			}
		}
		return itemRecurringPriceList;
	}

	/**
	 * Adds the PLP Url for each product
	 */
	private List<String> fetchPDPForProducts(final List<TmaProductOfferingModel> productModelList)
	{
		final List<String> plpUrlForProducts = new ArrayList();
		for(TmaProductOfferingModel product: productModelList)
		{
			plpUrlForProducts.add(productModelUrlResolver.resolve(product));
		}
		return plpUrlForProducts;
	}

	private List<String> fetchPriceForDevice(List<TmaProductOfferingModel> productModelList, DevicePriceType devicePriceType) {
		final List<String> priceList = new ArrayList();
		for (final TmaProductOfferingModel item : productModelList)
		{

			final Collection<PriceRowModel> priceRowModelCollection = item.getEurope1Prices();
			for (final PriceRowModel priceRow : priceRowModelCollection)
			{
				if(null != priceRow.getPriceType())
				{
					if(priceRow.getPriceType().equals(devicePriceType))
					{
						final Double price = priceRow.getPrice();
						final PriceData priceData = priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(price.doubleValue()),
								commerceCommonI18NService.getCurrentCurrency().getIsocode());
						priceList.add(priceData.getFormattedValue());
					}
				}
			}
		}
		return priceList;
	}
}
