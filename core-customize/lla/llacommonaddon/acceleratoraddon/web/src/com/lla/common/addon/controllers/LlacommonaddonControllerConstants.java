/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.common.addon.controllers;

import de.hybris.platform.acceleratorcms.model.components.LLACMSImageTextComponentModel;

import com.lla.common.addon.model.LLAAddonProductComponentModel;
import com.lla.common.addon.model.LLABundleProductCMSComponentModel;
import com.lla.common.addon.model.LLACMSImageButtonTextComponentModel;
import com.lla.common.addon.model.LLASimpleResponsiveBannerComponentModel;


/**
 */
public interface LlacommonaddonControllerConstants
{
	String ADDON_PREFIX = "addon:/llacommonaddon/";

	interface Actions
	{
		interface Cms // NOSONAR
		{
			String _Prefix = "/view/"; // NOSONAR
			String _Suffix = "Controller"; // NOSONAR

			String LLACMSImageButtonTextComponent = _Prefix + LLACMSImageButtonTextComponentModel._TYPECODE + _Suffix; // NOSONAR
			String LLABundleProductCMSComponent = _Prefix + LLABundleProductCMSComponentModel._TYPECODE + _Suffix; // NOSONAR
			String LLAAddonProductComponent = _Prefix + LLAAddonProductComponentModel._TYPECODE + _Suffix; // NOSONAR
			String LLASimpleResponsiveBannerComponent = _Prefix + LLASimpleResponsiveBannerComponentModel._TYPECODE + _Suffix; //NOSONAR
			String LLACMSImageTextComponent = _Prefix + LLACMSImageTextComponentModel._TYPECODE + _Suffix; // NOSONAR

		}
	}
}
