/**
 *
 */
package com.lla.common.addon.controllers.cms;

import de.hybris.platform.addonsupport.controllers.cms.AbstractCMSAddOnComponentController;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lla.common.addon.controllers.LlacommonaddonControllerConstants;
import com.lla.common.addon.model.LLACMSImageButtonTextComponentModel;


/**
 * @author GZ132VA
 *
 */
@Controller("LLACMSImageButtonTextComponentController")
@RequestMapping(value = LlacommonaddonControllerConstants.Actions.Cms.LLACMSImageButtonTextComponent)
public class LLACMSImageButtonTextComponentController
		extends AbstractCMSAddOnComponentController<LLACMSImageButtonTextComponentModel>
{
	private static final String LLA_CMS_IMAGE_BUTTON_TEXT_COMPONENT = "addon:/llacommonaddon/cms/llacmsimagebuttontextcomponent";
	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final LLACMSImageButtonTextComponentModel component)
	{
		model.addAttribute("headlineText", component.getImageHeadlineText());
		model.addAttribute("subHeadlineText", component.getImageSubHeadlineText());
		model.addAttribute("imageUrl", null!=component.getMedia()?component.getMedia().getURL(): StringUtils.EMPTY);
		model.addAttribute("buttonCTAText", component.getButtonCTAText());
		model.addAttribute("buttonVisible", component.getButtonCTA());
		model.addAttribute("actionUrl", component.getActionUrl());
	}

}
