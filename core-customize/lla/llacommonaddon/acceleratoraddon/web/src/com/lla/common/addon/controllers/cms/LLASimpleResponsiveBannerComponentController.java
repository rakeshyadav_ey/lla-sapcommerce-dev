package com.lla.common.addon.controllers.cms;

import com.lla.common.addon.controllers.LlacommonaddonControllerConstants;
import com.lla.common.addon.model.LLASimpleResponsiveBannerComponentModel;
import de.hybris.platform.acceleratorfacades.device.ResponsiveMediaFacade;
import de.hybris.platform.addonsupport.controllers.cms.AbstractCMSAddOnComponentController;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller("LLASimpleResponsiveBannerComponentController")
@RequestMapping(value = LlacommonaddonControllerConstants.Actions.Cms.LLASimpleResponsiveBannerComponent)
public class LLASimpleResponsiveBannerComponentController extends
        AbstractCMSAddOnComponentController<LLASimpleResponsiveBannerComponentModel>
{
    @Resource(name = "responsiveMediaFacade")
    private ResponsiveMediaFacade responsiveMediaFacade;

    @Resource(name = "commerceCommonI18NService")
    private CommerceCommonI18NService commerceCommonI18NService;

    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
                             final LLASimpleResponsiveBannerComponentModel component)
    {
        final List<ImageData> mediaDataList = responsiveMediaFacade.getImagesFromMediaContainer(component
                .getMedia(commerceCommonI18NService.getCurrentLocale()));
        model.addAttribute("medias", mediaDataList);
        model.addAttribute("urlLink", component.getUrlLink());
        model.addAttribute("bannerText", component.getBannerText());
        model.addAttribute("bannerContent", component.getBannerContent());
    }
}