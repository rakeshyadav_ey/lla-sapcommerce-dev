/**
 *
 */
package com.lla.common.addon.controllers.cms;

import de.hybris.platform.addonsupport.controllers.cms.AbstractCMSAddOnComponentController;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.subscriptionservices.model.RecurringChargeEntryModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionPricePlanModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lla.common.addon.controllers.LlacommonaddonControllerConstants;
import com.lla.common.addon.model.LLAAddonProductComponentModel;


/**
 * @author GZ132VA
 *
 */
@Controller("LLAAddonProductComponentController")
@RequestMapping(value = LlacommonaddonControllerConstants.Actions.Cms.LLAAddonProductComponent)
public class LLAAddonProductComponentController extends AbstractCMSAddOnComponentController<LLAAddonProductComponentModel>
{
	@Resource(name = "priceDataFactory")
	PriceDataFactory priceDataFactory;

	@Autowired
	CommerceCommonI18NService commerceCommonI18NService;


	/**
	 * @param productModelList
	 * @return
	 */
	private List<String> fetchRecurringPriceForBundle(final List<TmaProductOfferingModel> productModelList)
	{
		final List<String> itemRecurringPriceList = new ArrayList();
		for (final TmaProductOfferingModel item : productModelList)
		{

			final Collection<PriceRowModel> priceRowModelCollection = item.getEurope1Prices();
			for (final PriceRowModel priceRow : priceRowModelCollection)
			{
				if (priceRow instanceof SubscriptionPricePlanModel)
				{
					final SubscriptionPricePlanModel pricePlan = (SubscriptionPricePlanModel) priceRow;
					final Collection<RecurringChargeEntryModel> recurringChargeEntries = pricePlan.getRecurringChargeEntries();
					final RecurringChargeEntryModel model = recurringChargeEntries.iterator().next();
					final Double price = model.getPrice();
					final PriceData priceData = priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(price.doubleValue()),
							commerceCommonI18NService.getCurrentCurrency().getIsocode());
					itemRecurringPriceList.add(StringUtils.chomp(priceData.getFormattedValue(),".00"));
					break;
				}
			}
		}
		return itemRecurringPriceList;
	}



	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final LLAAddonProductComponentModel component)
	{
		model.addAttribute("headlineText", component.getHeadlineText());
		final List<TmaProductOfferingModel> productModelList = component.getProducts();
		if (CollectionUtils.isNotEmpty(productModelList))
		{
			model.addAttribute("productList", productModelList);
			model.addAttribute("itemRecurringPriceList", fetchRecurringPriceForBundle(productModelList));

		}
	}
}
