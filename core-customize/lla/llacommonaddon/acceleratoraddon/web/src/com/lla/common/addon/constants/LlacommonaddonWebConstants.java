/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.common.addon.constants;

/**
 * Global class for all Llacommonaddon web constants. You can add global constants for your extension into this class.
 */
public final class LlacommonaddonWebConstants // NOSONAR
{
	private LlacommonaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
