/**
 *
 */
package com.lla.common.addon.controllers.cms;


import de.hybris.platform.acceleratorcms.model.components.LLACMSImageTextComponentModel;
import de.hybris.platform.acceleratorfacades.device.ResponsiveMediaFacade;
import de.hybris.platform.addonsupport.controllers.cms.AbstractCMSAddOnComponentController;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lla.common.addon.controllers.LlacommonaddonControllerConstants;


/**
 * @author GZ132VA
 *
 */
@Controller("LLACMSImageTextComponentController")
@RequestMapping(value = LlacommonaddonControllerConstants.Actions.Cms.LLACMSImageTextComponent)
public class LLACMSImageTextComponentController extends AbstractCMSAddOnComponentController<LLACMSImageTextComponentModel>
{
	private static final String LLA_CMS_IMAGE_TEXT_COMPONENT = "addon:/llacommonaddon/cms/llacmsimagetextcomponent";
	@Resource(name = "responsiveMediaFacade")
	private ResponsiveMediaFacade responsiveMediaFacade;

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final LLACMSImageTextComponentModel component)
	{
		final List<ImageData> mediaBackgroundDataList = responsiveMediaFacade
				.getImagesFromMediaContainer(component.getBackgroundBannerImage(commerceCommonI18NService.getCurrentLocale()));
		final List<ImageData> mediaForegroundDataList = responsiveMediaFacade
				.getImagesFromMediaContainer(component.getForegroundPromoImage(commerceCommonI18NService.getCurrentLocale()));
		model.addAttribute("backgroundBannerText", component.getBackgroundBannerText());
		model.addAttribute("backgroundBannerImageList", mediaBackgroundDataList);
		model.addAttribute("foregroundPromoImageList", mediaForegroundDataList);
	}

}
