package com.lla.common.addon.dao.impl;

import com.lla.common.addon.dao.LLATmaBillingAccountDao;
import de.hybris.platform.b2ctelcoservices.model.TmaBillingAccountModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.notificationservices.enums.SiteMessageType;
import de.hybris.platform.notificationservices.model.SiteMessageForCustomerModel;
import de.hybris.platform.notificationservices.model.SiteMessageModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Collections;
import java.util.List;

public class LLATmaBillingAccountDaoImpl extends DefaultGenericDao<TmaBillingAccountModel> implements LLATmaBillingAccountDao {

    private static final String BILLINGACCOUNT_CUSTOMER_QUERY = "select {bill:pk} from {" + TmaBillingAccountModel._TYPECODE
            + " as bill left join " + CustomerModel._TYPECODE + " as cus on {bill:customer} = {cus:pk}}"
            + " where {cus:uid} = ?customer";

    public LLATmaBillingAccountDaoImpl()
    {
        super(TmaBillingAccountModel._TYPECODE);
    }

    @Override
    public List<TmaBillingAccountModel> findCustomerBillingAccounts(final CustomerModel customer) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(BILLINGACCOUNT_CUSTOMER_QUERY);
        query.addQueryParameter("customer",customer.getUid());
        final List<TmaBillingAccountModel> billingAccounts = getFlexibleSearchService().<TmaBillingAccountModel>search(query).getResult();
        if (null == billingAccounts) {
            return Collections.emptyList();
        }
        return billingAccounts;
    }
}


