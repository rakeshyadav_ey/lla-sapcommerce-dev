package com.lla.common.addon.workflow;

import com.lla.mulesoft.integration.service.LLAMulesoftCustomerService;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.OrderService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

public class OrderApprovalStartWorkflowTemplateJob extends AbstractOrderWorkflowTemplateJob{
    private static final Logger LOG = Logger.getLogger(OrderApprovedWorkflowTemplateJob.class);
    @Autowired
    LLAMulesoftCustomerService llaMulesoftCustomerService;

    private Converter<OrderModel, OrderData> orderConverter;
    @Autowired
    private OrderService orderService;
    @Override
    public WorkflowDecisionModel perform(WorkflowActionModel workflowActionModel) {
        LOG.info("*********************************");
        LOG.info("Inside the Order Approval Workflow");
        LOG.info("*********************************");
        LOG.info("Processed the orderEntries for SourceEPC, proceeding ahead");
        return defaultDecision(workflowActionModel);
    }


    public Converter<OrderModel, OrderData> getOrderConverter() {
        return orderConverter;
    }

    @Required
    public void setOrderConverter(Converter<OrderModel, OrderData> orderConverter) {
        this.orderConverter = orderConverter;
    }
}