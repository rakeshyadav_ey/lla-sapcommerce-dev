package com.lla.common.addon.openidp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class UserAssociationFlag {
    @JsonProperty("mint.enabledByUser")
    @JsonSerialize(using = IgnoreSerializer.class)
    private String enabledByUser;
    @JsonProperty("mint.permission.administration")
    @JsonSerialize(using = IgnoreSerializer.class)
    private String administration;
    @JsonProperty("mint.permission.delete")
    @JsonSerialize(using = IgnoreSerializer.class)
    private String delete;
    @JsonProperty("mint.permission.execute")
    @JsonSerialize(using = IgnoreSerializer.class)
    private String execute;
    @JsonProperty("mint.permission.read")
    @JsonSerialize(using = IgnoreSerializer.class)
    private String read;
    @JsonProperty("mint.permission.write")
    @JsonSerialize(using = IgnoreSerializer.class)
    private String write;

    public String getEnabledByUser() {
        return enabledByUser;
    }

    public void setEnabledByUser(String enabledByUser) {
        this.enabledByUser = enabledByUser;
    }

    public String getAdministration() {
        return administration;
    }

    public void setAdministration(String administration) {
        this.administration = administration;
    }

    public String getDelete() {
        return delete;
    }

    public void setDelete(String delete) {
        this.delete = delete;
    }

    public String getExecute() {
        return execute;
    }

    public void setExecute(String execute) {
        this.execute = execute;
    }

    public String getRead() {
        return read;
    }

    public void setRead(String read) {
        this.read = read;
    }

    public String getWrite() {
        return write;
    }

    public void setWrite(String write) {
        this.write = write;
    }
}
