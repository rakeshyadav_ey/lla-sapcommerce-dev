/**
 *
 */
package com.lla.common.addon.exception;

/**
 * @author GZ132VA
 *
 */
public class LLAInvalidTokenException extends RuntimeException
{
	/**
	 * Default constructor
	 */
	public LLAInvalidTokenException()
	{
		super();
	}

	/**
	 * @param message
	 */
	public LLAInvalidTokenException(final String message)
	{
		super(message);
	}

	public LLAInvalidTokenException(final Exception e)
	{
		super(e);
	}

}
