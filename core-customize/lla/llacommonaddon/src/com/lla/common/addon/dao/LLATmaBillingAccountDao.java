package com.lla.common.addon.dao;

import de.hybris.platform.b2ctelcoservices.model.TmaBillingAccountModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;


public interface LLATmaBillingAccountDao {

    List<TmaBillingAccountModel> findCustomerBillingAccounts(final CustomerModel customerUID);
}
