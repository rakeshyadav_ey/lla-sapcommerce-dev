/**
 *
 */
package com.lla.common.addon.openidp;

import com.lla.mulesoft.integration.exception.LLAApiException;
import org.apache.commons.lang.StringUtils;

/**
 * @author GZ132VA
 *
 */
public class OpenIdpUserDetailResponse
{
	private String id;
	private String displayName;
	private String status;
	private String emailAddress;
	private String username;
	private Long createdDate;
	private Long updatedDate;

	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * @param id
	 *           the id to set
	 */
	public void setId(final String id)
	{
		this.id = id;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName()
	{
		return displayName;
	}

	/**
	 * @param displayName
	 *           the displayName to set
	 */
	public void setDisplayName(final String displayName)
	{
		this.displayName = displayName;
	}

	/**
	 * @return the status
	 */
	public String getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *           the status to set
	 */
	public void setStatus(final String status)
	{
		this.status = status;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() throws  LLAApiException
	{
		if(StringUtils.isNotEmpty(emailAddress))
			return emailAddress.toLowerCase() ;
		else
			throw new LLAApiException("No email address received in ULM Response");
	}

	/**
	 * @param emailAddress
	 *           the emailAddress to set
	 */
	public void setEmailAddress(final String emailAddress)
	{
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the username
	 */
	public String getUsername()
	{
		return username.toLowerCase();
	}

	/**
	 * @param username
	 *           the username to set
	 */
	public void setUsername(final String username)
	{
		this.username = username;
	}

	/**
	 * @return the createdDate
	 */
	public Long getCreatedDate()
	{
		return createdDate;
	}

	/**
	 * @param createdDate
	 *           the createdDate to set
	 */
	public void setCreatedDate(final Long createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * @return the updatedDate
	 */
	public Long getUpdatedDate()
	{
		return updatedDate;
	}

	/**
	 * @param updatedDate
	 *           the updatedDate to set
	 */
	public void setUpdatedDate(final Long updatedDate)
	{
		this.updatedDate = updatedDate;
	}
}
