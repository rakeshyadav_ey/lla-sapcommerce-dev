package com.lla.common.addon.exception;

public class LLAInvalidWorkflowException extends RuntimeException {
    public LLAInvalidWorkflowException(){super();}
    public LLAInvalidWorkflowException(String msg){super(msg);}
    public LLAInvalidWorkflowException(Exception ex){super(ex);}
}
