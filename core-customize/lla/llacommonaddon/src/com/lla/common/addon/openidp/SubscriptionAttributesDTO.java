package com.lla.common.addon.openidp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class SubscriptionAttributesDTO {
    @JsonProperty("name")
    @JsonSerialize(using = IgnoreSerializer.class)
    private String name;
    @JsonProperty("value")
    @JsonSerialize(using = IgnoreSerializer.class)
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


}
