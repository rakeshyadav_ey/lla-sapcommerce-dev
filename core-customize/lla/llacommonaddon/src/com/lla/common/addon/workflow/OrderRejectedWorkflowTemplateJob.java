package com.lla.common.addon.workflow;


import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;
import org.apache.log4j.Logger;
import org.llapayment.client.LLAPaymentClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

public class OrderRejectedWorkflowTemplateJob extends AbstractOrderWorkflowTemplateJob {
    private LLAPaymentClient llaPaymentClient;
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

    public static final Logger LOG = Logger.getLogger(OrderRejectedWorkflowTemplateJob.class);
    private LLAMulesoftNotificationService llaMulesoftNotificationService;
    @Override
    public WorkflowDecisionModel perform(WorkflowActionModel workflowActionModel) {
        if(getConfigurationService().getConfiguration().getBoolean(WORKFLOW_ENABLED_PROPERTY)) {
            LOG.debug("Inside " + OrderRejectedWorkflowTemplateJob.class);
            OrderModel order = getOrderAttachment(workflowActionModel);
            if (null != order) {
                order.setStatus(OrderStatus.CSA_REJECTED);
                getModelService().save(order);
            }
            LOG.debug("Updated OrderStatus to CSA_REJECTED");
            if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())){
                LOG.info("Initiating the order refund");
                try {
                    llaPaymentClient.sendRefundRequest(order);
                } catch (LLAApiException e) {
                    LOG.error("Exception in making refund request for Authorized amount");
                }
                LOG.info(String.format("Order %s refund completed",order.getCode()));
            }
            try {
                String response=llaMulesoftNotificationService.generateOrderCancelNotification(order);
                LOG.info(String.format("Order %s is cancelled successfully", order.getCode()));
            }catch(LLAApiException exception){
               LOG.error("Exception in making API Call");
            }

        }else{
            LOG.info(String.format("Ignoring order confirmation as fulfilment is disabled in property %s",WORKFLOW_ENABLED_PROPERTY));
        }
        return defaultDecision(workflowActionModel);
    }

    public LLAPaymentClient getLlaPaymentClient() {
        return llaPaymentClient;
    }

    @Required
    public void setLlaPaymentClient(LLAPaymentClient llaPaymentClient) {
        this.llaPaymentClient = llaPaymentClient;
    }

    public LLAMulesoftNotificationService getLlaMulesoftNotificationService() {
        return llaMulesoftNotificationService;
    }

    @Required
    public void setLlaMulesoftNotificationService(LLAMulesoftNotificationService llaMulesoftNotificationService) {
        this.llaMulesoftNotificationService = llaMulesoftNotificationService;
    }


}
