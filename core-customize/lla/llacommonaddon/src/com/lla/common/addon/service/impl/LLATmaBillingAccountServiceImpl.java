package com.lla.common.addon.service.impl;

import com.lla.common.addon.dao.impl.LLATmaBillingAccountDaoImpl;
import com.lla.common.addon.service.LLATmaBillingAccountService;
import de.hybris.platform.b2ctelcoservices.model.TmaBillingAccountModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class LLATmaBillingAccountServiceImpl implements LLATmaBillingAccountService{

    @Autowired
    LLATmaBillingAccountDaoImpl llaTmaBillingAccountDao;

    @Autowired
    ModelService modelService;

    @Override
    public void deleteBillingAccount(final CustomerModel customer){
        final List<TmaBillingAccountModel> billingAccountModel = llaTmaBillingAccountDao.findCustomerBillingAccounts(customer);
        modelService.removeAll(billingAccountModel);
    }
}
