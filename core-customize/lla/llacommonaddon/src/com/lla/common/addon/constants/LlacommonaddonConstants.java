/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.common.addon.constants;

/**
 * Global class for all Llacommonaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class LlacommonaddonConstants extends GeneratedLlacommonaddonConstants
{
	public static final String EXTENSIONNAME = "llacommonaddon";
	public static final String TOKEN_OPENID_URL = "token.openid.url";

	public static final String BEARER = "Bearer ";

	public static final String CONTENT_TYPE = "content-type";

	public static final String APPLICATION_JSON = "application/json";

	public static final String FLOW_USER_INFO_URL = "flow.user.info.url";

	public static final String OPENID_URL = "openid.url";
	public static final String BASIC = "Basic ";
	public static final String AUTHORIZATION = "Authorization";
	public static final String UTF_8 = "UTF-8";
	public static final String CLIENT_ID = "flow.client.id";
	public static final String CLIENT_SECRET = "flow.client.secret";
	public static final String TEMP_CREDENTIALS = "testlla";
	public static final int SUCCESS_RESPONSE = 200;
	public static final String BILLING_ACCOUNT = "BillingAccount";
	public static final String COM_UXPSYSTEMS_MINT_ACCOUNT_ACCOUNT_NUMBER = "com.uxpsystems.mint.account.AccountNumber";
	public static final String SOURCE_BSS = "co.flowid.SourceBSS";
	public static final String API_EXCEPTION_MSG = "Exception in making API call";
	public static final String AUTH_URL = "authUrl";
	private LlacommonaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
	public final static class Workflows
	{
		public final static String ORDER_CONFIRMATION_WORKFLOW = "OrderConfirmation";
	}
}
