package com.lla.common.addon.openidp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class UserAssociationDTO {

    @JsonProperty("flags")
    @JsonSerialize(using = IgnoreSerializer.class)
    @JsonIgnoreProperties(ignoreUnknown = true)
    private UserAssociationFlag flags;
    public UserAssociationFlag getFlags() {
        return flags;
    }

    public void setFlags(UserAssociationFlag flags) {
        this.flags = flags;
    }



}
