package com.lla.common.addon.openidp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

public class UserSubscriptionDTO {
    @JsonProperty("association")
    @JsonSerialize(using = IgnoreSerializer.class)
    private UserAssociationDTO association;
    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("displayName")
    private String displayName;
    @JsonProperty("status")
    private String status;
    @JsonProperty("attributes")
    @JsonSerialize(using = IgnoreSerializer.class)
    private List<SubscriptionAttributesDTO> attributes;
    @JsonProperty("features")
    @JsonSerialize(using = IgnoreSerializer.class)
    private List<UserSubscriptionFeatureDTO> features;

    public String getId() {
        return id;
    }

    public UserAssociationDTO getAssociation() {
        return association;
    }

    public void setAssociation(UserAssociationDTO association) {
        this.association = association;
    }

    public void setAttributes(List<SubscriptionAttributesDTO> attributes) {
        this.attributes = attributes;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
