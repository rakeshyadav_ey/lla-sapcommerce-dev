/**
 *
 */
package com.lla.common.addon.idp.strategy;

import com.lla.mulesoft.integration.exception.LLAApiException;

import com.lla.common.addon.exception.LLAInvalidUserException;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @author GZ132VA
 *
 */
public interface LLALoginStrategy
{
	String fetchLoginAuthenticationToken(final String authCode, final HttpServletRequest request,
	final HttpServletResponse response) throws UnknownIdentifierException, IllegalArgumentException, DuplicateUidException, LLAInvalidUserException,LLAApiException;
	String authUrlConstructor(final HttpServletRequest request, String callType);
}
