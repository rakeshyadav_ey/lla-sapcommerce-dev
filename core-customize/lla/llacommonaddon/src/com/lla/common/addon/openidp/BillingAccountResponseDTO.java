package com.lla.common.addon.openidp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

public class BillingAccountResponseDTO {

    @JsonProperty("association")
    @JsonSerialize(using = IgnoreSerializer.class)
    @JsonIgnoreProperties(ignoreUnknown = true)
    private UserAssociationDTO association;
    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("displayName")
    private String displayName;
    @JsonProperty("status")
    private String status;
    @JsonProperty("subscriptions")
    @JsonSerialize(using = IgnoreSerializer.class)
    private List<UserSubscriptionDTO> subscriptions;
    @JsonProperty("attributes")
    @JsonSerialize(using = IgnoreSerializer.class)
    private List<SubscriptionAttributesDTO> attributes;


    public UserAssociationDTO getAssociation() {
        return association;
    }

    public void setAssociation(UserAssociationDTO association) {
        this.association = association;
    }

    public List<UserSubscriptionDTO> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscription(List<UserSubscriptionDTO> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public List<SubscriptionAttributesDTO> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<SubscriptionAttributesDTO> attributes) {
        this.attributes = attributes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
