package com.lla.common.addon.workflow;

import com.lla.mulesoft.integration.service.LLAMulesoftCustomerService;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.OrderService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

public class OrderApprovedWorkflowTemplateJob extends AbstractOrderWorkflowTemplateJob{
    public static final Logger LOG = Logger.getLogger(OrderApprovedWorkflowTemplateJob.class);
    LLAMulesoftNotificationService llaMulesoftNotificationService;
    private Converter<OrderModel, OrderData> orderConverter;
    @Autowired
    LLAMulesoftCustomerService llaMulesoftCustomerService;
    @Autowired
    private OrderService orderService;
    @Override
    public WorkflowDecisionModel perform(WorkflowActionModel workflowActionModel) {
        if(getConfigurationService().getConfiguration().getBoolean(WORKFLOW_ENABLED_PROPERTY)) {
            LOG.debug("Inside " + OrderApprovedWorkflowTemplateJob.class);
            OrderModel order = getOrderAttachment(workflowActionModel);
            if (null != order) {
                order.setStatus(OrderStatus.CSA_APPROVED);
                order.setCalculated(Boolean.TRUE);
                getModelService().save(order);
                getModelService().refresh(order);
                LOG.info(String.format("Launching Order Process for Order :::  %s", order.getCode()));
                orderService.submitOrder(order);
            }
        }else{
            LOG.info(String.format("Ignoring order confirmation as fulfilment is disabled in property %s",WORKFLOW_ENABLED_PROPERTY));
        }
        return defaultDecision(workflowActionModel);
    }

    public LLAMulesoftNotificationService getLlaMulesoftNotificationService() {
        return llaMulesoftNotificationService;
    }

    @Required
    public void setLlaMulesoftNotificationService(LLAMulesoftNotificationService llaMulesoftNotificationService) {
        this.llaMulesoftNotificationService = llaMulesoftNotificationService;
    }

    public Converter<OrderModel, OrderData> getOrderConverter() {
        return orderConverter;
    }

    @Required
    public void setOrderConverter(Converter<OrderModel, OrderData> orderConverter) {
        this.orderConverter = orderConverter;
    }
}
