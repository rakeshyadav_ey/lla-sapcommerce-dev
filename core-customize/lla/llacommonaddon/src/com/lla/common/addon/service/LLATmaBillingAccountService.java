package com.lla.common.addon.service;

import de.hybris.platform.core.model.user.CustomerModel;

public interface LLATmaBillingAccountService {

    void deleteBillingAccount(final CustomerModel customerUID);
}
