package com.lla.common.addon.workflow;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.workflow.WorkflowAttachmentService;
import de.hybris.platform.workflow.jobs.AutomatedWorkflowTemplateJob;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;

public abstract  class AbstractOrderWorkflowTemplateJob implements AutomatedWorkflowTemplateJob {

    private WorkflowAttachmentService workflowAttachmentService;

    private ModelService modelService;

    private ConfigurationService configurationService;

    protected static final String WORKFLOW_ENABLED_PROPERTY="llacommonaddon.orderconfirmation.workflowEnabled";

    public OrderModel getOrderAttachment(final WorkflowActionModel workflowActionModel)
    {
        if(CollectionUtils.isNotEmpty(workflowActionModel.getWorkflow().getAttachments())){
            return (OrderModel) workflowActionModel.getWorkflow().getAttachments().get(0).getItem();
        }
        return null;
    }

    protected WorkflowDecisionModel defaultDecision(final WorkflowActionModel workflowActionModel)
    {
        for (final WorkflowDecisionModel decision : workflowActionModel.getDecisions())
        {
            return decision;
        }
        return null;
    }

    protected <T extends ItemModel> T getModelOfType(final WorkflowActionModel workflowActionModel, final Class<T> clazz)
    {
        final List<ItemModel> models = workflowAttachmentService.getAttachmentsForAction(workflowActionModel, clazz.getName());

        if (CollectionUtils.isNotEmpty(models))
        {
            return (T) models.iterator().next();
        }

        return null;

    }

    public WorkflowAttachmentService getWorkflowAttachmentService() {
        return workflowAttachmentService;
    }

    public void setWorkflowAttachmentService(WorkflowAttachmentService workflowAttachmentService) {
        this.workflowAttachmentService = workflowAttachmentService;
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }
}
