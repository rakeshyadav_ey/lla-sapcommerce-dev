package com.lla.common.addon.facade.impl;

import com.lla.common.addon.constants.LlacommonaddonConstants;
import com.lla.common.addon.facade.OrderConfirmationFacade;
import com.lla.core.util.LLACommonUtil;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.OrderModel;

import de.hybris.platform.core.model.user.*;

import de.hybris.platform.ordersplitting.model.NotificationEmailProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;

import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.workflow.WorkflowProcessingService;
import de.hybris.platform.workflow.WorkflowService;
import de.hybris.platform.workflow.WorkflowTemplateService;
import de.hybris.platform.workflow.model.WorkflowModel;
import de.hybris.platform.workflow.model.WorkflowTemplateModel;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.LanguageModel;
import java.util.*;

public class DefaultOrderConfirmationFacade implements OrderConfirmationFacade{

    private static final Logger LOG = Logger.getLogger(DefaultOrderConfirmationFacade.class);

    private WorkflowService workflowService;

    private WorkflowTemplateService workflowTemplateService;

    private WorkflowProcessingService workflowProcessingService;

    private ModelService modelService;

    private FlexibleSearchService flexibleSearchService;

    private ConfigurationService configurationService;
    @Autowired
    private UserService userService;

    @Autowired
    private BusinessProcessService businessProcessService;

    @Autowired
    private LLACommonUtil llaCommonUtil;
    
    @Autowired
    private CommerceCommonI18NService commerceCommonI18NService;

    @Override
    public void launchWorkflow(OrderData orderData)
    {
        final WorkflowTemplateModel workflowTemplate = getWorkflowTemplateService()
                .getWorkflowTemplateForCode(LlacommonaddonConstants.Workflows.ORDER_CONFIRMATION_WORKFLOW);
        LOG.debug("Inside Order confirmation facade, launching the workflow");
        final OrderModel order = new OrderModel();
        order.setCode(orderData.getCode());
        final OrderModel orderModel = getFlexibleSearchService().getModelByExample(order);
        UserModel workflowOwner=null;
        if(llaCommonUtil.isSitePanama()){
            workflowOwner= userService.getUserForUID(
                    getConfigurationService().getConfiguration().getString("llacommonaddon.panama.workflowowner.uid", "CustomerSupportAgent_PA"));

        }else if(llaCommonUtil.isSiteJamaica()){
           workflowOwner = userService.getUserForUID(
                getConfigurationService().getConfiguration().getString("llacommonaddon.jamaica.workflowowner.uid", "CustomerSupportAgent"));
        } else if(llaCommonUtil.isSitePuertorico()){
            workflowOwner = userService.getUserForUID(
                    getConfigurationService().getConfiguration().getString("llacommonaddon.puertorico.workflowowner.uid", "CustomerSupportAgent_PR"));

        } else if(llaCommonUtil.isSiteCabletica()){
            workflowOwner = userService.getUserForUID(
                    getConfigurationService().getConfiguration().getString("llacommonaddon.cabletica.workflowowner.uid", "CustomerSupportAgent_CR"));

        }
        else if (llaCommonUtil.isSiteVTR())
     		{
     			workflowOwner = userService.getUserForUID(getConfigurationService().getConfiguration()
     					.getString("llacommonaddon.vtr.workflowowner.uid", "CustomerSupportAgent_CL"));
   
     		}
        final WorkflowModel orderConfirmationWorkflow = getWorkflowService().createWorkflow(workflowTemplate, orderModel, workflowOwner);
        String workflowName = orderConfirmationWorkflow.getName()+": "+orderData.getCode();
        for(WorkflowModel existingWorkflow: getWorkflowService().getWorkflowsForTemplateAndUser(workflowTemplate, workflowOwner)){
            if(null!=existingWorkflow.getName() && existingWorkflow.getName().equalsIgnoreCase(workflowName)){
                LOG.warn(String.format("Not triggering workflow %s as it is already created", workflowName));
                return;
            }
        }

        //orderConfirmationWorkflow.setName(workflowName);
        Collection<LanguageModel> supportedLanguages= commerceCommonI18NService.getAllLanguages();
        for(LanguageModel lang:supportedLanguages){
            orderConfirmationWorkflow.setName(workflowName,new Locale(lang.getIsocode()));
        }
        getModelService().save(orderConfirmationWorkflow);
        LOG.debug("Starting workflow");
        getWorkflowProcessingService().startWorkflow(orderConfirmationWorkflow);

        final NotificationEmailProcessModel notificationEmailProcessModel = (NotificationEmailProcessModel) businessProcessService.createProcess(
                "NotificationEmailProcess-" + orderModel.getCode() + "-" + System.currentTimeMillis(),
                "notification-email-process");
        notificationEmailProcessModel.setOrder(orderModel);
        modelService.save(notificationEmailProcessModel);
        modelService.refresh(notificationEmailProcessModel);
        businessProcessService.startProcess(notificationEmailProcessModel);
        LOG.debug("Workflow started");
    }


    public WorkflowService getWorkflowService() {
        return workflowService;
    }

    @Required
    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    public WorkflowProcessingService getWorkflowProcessingService() {
        return workflowProcessingService;
    }

    @Required
    public void setWorkflowProcessingService(WorkflowProcessingService workflowProcessingService) {
        this.workflowProcessingService = workflowProcessingService;
    }

    public ModelService getModelService() {
        return modelService;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public WorkflowTemplateService getWorkflowTemplateService() {
        return workflowTemplateService;
    }

    @Required
    public void setWorkflowTemplateService(WorkflowTemplateService workflowTemplateService) {
        this.workflowTemplateService = workflowTemplateService;
    }

    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    @Required
    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

     public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    @Required
    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

}
