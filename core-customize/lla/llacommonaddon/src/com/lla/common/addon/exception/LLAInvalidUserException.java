package com.lla.common.addon.exception;

public class LLAInvalidUserException extends Exception
{
    /**
     * Default constructor
     */
    public LLAInvalidUserException()
    {
        super();
    }

    /**
     * @param message
     */
    public LLAInvalidUserException(final String message)
    {
        super(message);
    }

    public LLAInvalidUserException(final Exception e)
    {
        super(e);
    }

}