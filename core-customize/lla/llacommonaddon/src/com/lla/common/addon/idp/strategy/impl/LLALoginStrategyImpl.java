/**
 *
 */
package com.lla.common.addon.idp.strategy.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.lla.common.addon.constants.LlacommonaddonConstants;
import com.lla.common.addon.exception.LLAInvalidTokenException;
import com.lla.common.addon.exception.LLAInvalidUserException;
import com.lla.common.addon.idp.strategy.LLALoginStrategy;
import com.lla.common.addon.openidp.BillingAccountResponseDTO;
import com.lla.common.addon.openidp.OpenIdpAuthTokenResponse;
import com.lla.common.addon.openidp.OpenIdpUserDetailResponse;
import com.lla.common.addon.openidp.SubscriptionAttributesDTO;
import com.lla.common.addon.service.impl.LLATmaBillingAccountServiceImpl;
import com.lla.core.constants.LlaCoreConstants;
import com.lla.core.enums.SourceEPC;
import com.lla.mulesoft.integration.exception.LLAApiException;

import com.lla.core.util.LLACommonUtil;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;

import de.hybris.platform.b2ctelcoservices.model.TmaBillingAccountModel;
import de.hybris.platform.b2ctelcoservices.services.TmaBillingAccountService;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.customergroups.exceptions.InvalidCustomerGroupException;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.exceptions.UnauthorizedUserException;
import org.springframework.util.Base64Utils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author GZ132VA
 *
 */
public class LLALoginStrategyImpl implements LLALoginStrategy
{
	private static final Logger LOG = Logger.getLogger(LLALoginStrategyImpl.class);
	@Resource(name = "customerFacade")
	private CustomerFacade llaCustomerFacade;
	@Autowired
	ModelService modelService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;
	@Autowired
	private TmaBillingAccountService tmaBillingAccountService;

	@Autowired
	private LLATmaBillingAccountServiceImpl llaTmaBillingAccountService;
	@Autowired
	private LLACommonUtil llaCommonUtil;

	@Autowired
	private SessionService sessionService;

	@Autowired
	private CommerceCommonI18NService commerceCommonI18NService;
	@Autowired
	private SiteConfigService siteConfigService;
	@Override
	public String fetchLoginAuthenticationToken(final String authCode, final HttpServletRequest request,
			final HttpServletResponse response) throws UnknownIdentifierException, IllegalArgumentException, DuplicateUidException,LLAApiException,LLAInvalidUserException

	{
		String encoded;
		try
		{
			encoded = Base64Utils
					.encodeToString(new StringBuilder(configurationService.getConfiguration().getString(LlacommonaddonConstants.CLIENT_ID)).append(":")
							.append(configurationService.getConfiguration().getString(LlacommonaddonConstants.CLIENT_SECRET)).toString().getBytes(LlacommonaddonConstants.UTF_8));
			final CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			final String url = tokenUrlConstructor(authCode);
			final HttpPost httpPost = new HttpPost(url);
			httpPost.addHeader(LlacommonaddonConstants.AUTHORIZATION, LlacommonaddonConstants.BASIC + encoded);
			httpPost.addHeader(LlacommonaddonConstants.CONTENT_TYPE, LlacommonaddonConstants.APPLICATION_JSON);
			final CloseableHttpResponse result = httpClient.execute(httpPost);
			final String responseJson = EntityUtils.toString(result.getEntity(), LlacommonaddonConstants.UTF_8);
			final Gson gson = new Gson();
			final OpenIdpAuthTokenResponse authTokenResponse = gson.fromJson(responseJson, OpenIdpAuthTokenResponse.class);
			final String registeredUserName=  fetchUserDetails(authTokenResponse, request, response);
			return registeredUserName;
		}
	    catch (final UnsupportedEncodingException unsupportedEncodingException)
		{
			LOG.error(LlacommonaddonConstants.API_EXCEPTION_MSG,unsupportedEncodingException);
		}
		catch (final ClientProtocolException clientProtocolException)
		{
			LOG.error(LlacommonaddonConstants.API_EXCEPTION_MSG,clientProtocolException);
		}
		catch (final IOException ioException)
		{
			LOG.error(LlacommonaddonConstants.API_EXCEPTION_MSG,ioException);
		}
		return StringUtils.EMPTY;

	}

	/**
	 * @param authTokenResponse
	 * @throws DuplicateUidException
	 * @throws IllegalArgumentException
	 * @throws UnknownIdentifierException
	 */
	private String fetchUserDetails(final OpenIdpAuthTokenResponse authTokenResponse, final HttpServletRequest request,
			final HttpServletResponse response)throws UnknownIdentifierException, IllegalArgumentException, DuplicateUidException,LLAInvalidUserException,LLAApiException

	{
		final String userDetailsEndPoint = configurationService.getConfiguration().getString(LlacommonaddonConstants.FLOW_USER_INFO_URL);
		final CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		final HttpGet httpGet = new HttpGet(userDetailsEndPoint);
		httpGet.addHeader(LlacommonaddonConstants.AUTHORIZATION, LlacommonaddonConstants.BEARER + authTokenResponse.getAccess_token());
		httpGet.addHeader(LlacommonaddonConstants.CONTENT_TYPE, LlacommonaddonConstants.APPLICATION_JSON);
		CloseableHttpResponse result;
		try
		{
			result = httpClient.execute(httpGet);
			if (null != result.getStatusLine() && result.getStatusLine().getStatusCode() == LlacommonaddonConstants.SUCCESS_RESPONSE)
			{
				final String responseJson = EntityUtils.toString(result.getEntity(), LlacommonaddonConstants.UTF_8);
				final Gson gson = new Gson();
				final OpenIdpUserDetailResponse userDetailResponse = gson.fromJson(responseJson, OpenIdpUserDetailResponse.class);
				LOG.info(String.format("User Details : %s", responseJson));
				if(!validateUserGroupsMapping(userDetailResponse.getEmailAddress())){
					final String registeredCustomer=createCustomerIfNotExists(userDetailResponse);
					retrieveCustomerBillingAccounts(userDetailResponse.getEmailAddress(),userDetailsEndPoint,authTokenResponse.getAccess_token());
					return registeredCustomer;
				}else{
					LOG.error("User belongs to other group or country and not allowed to proceed further");
					throw new LLAInvalidUserException(userDetailResponse.getEmailAddress());
				}
			}else {
				LOG.error(String.format("Unable to get the User Details from Flow System due to status code::: %s ",
						result.getStatusLine().getStatusCode()));
				throw new LLAInvalidTokenException("Token is already exhausted");
			}

		}
		catch (final ClientProtocolException exception)
		{
		  LOG.error(LlacommonaddonConstants.API_EXCEPTION_MSG,exception);
		}
		catch (final IOException ioException)
		{
			LOG.error(LlacommonaddonConstants.API_EXCEPTION_MSG,ioException);
		}
		return StringUtils.EMPTY;
	}

	private boolean validateUserGroupsMapping(String uid) {
       if(userService.isUserExisting(uid) && checkUserBelongsToAnyCountryOrNot(uid)){
		   final String customerOrigin = siteConfigService.getProperty("user.group.code");
		   final UserGroupModel userGroupModel = userService.getUserGroupForUID(customerOrigin);
		   CustomerModel customerModel=(CustomerModel) userService.getUserForUID(uid);
		   return userService.getAllUserGroupsForUser(customerModel).stream().filter(str -> str.getUid().equalsIgnoreCase(cmsSiteService.getCurrentSite().getUid())).findFirst().isPresent()?false:true;

	   }else{
       	 return false;
	   }
	}

	private boolean checkUserBelongsToAnyCountryOrNot(final String uid) {
		CustomerModel customerModel=(CustomerModel) userService.getUserForUID(uid);
		final String configuredCustomerGrps = configurationService.getConfiguration()
				.getString("user.group.allowed", "");
		final List<String> listOfAllowedCustomerGrps = Stream.of(configuredCustomerGrps.split(",", -1))
				.collect(Collectors.toList());
		boolean flag=false;
		for(String group:listOfAllowedCustomerGrps){
			final UserGroupModel userGroupModel = userService.getUserGroupForUID(group);
			 if( userService.isMemberOfGroup(customerModel, userGroupModel)){
			 	return true;
			 }
		}
		return flag;

	}

	/**
	 *  Service Call to Retrieve Customer Billing Account from Flow Response
	 * @param customerLoggingIn
	 * @param userDetailsEndPoint
	 * @param access_token
	 */
	private void retrieveCustomerBillingAccounts(final String customerLoggingIn,final String userDetailsEndPoint, final String access_token) {
		final String userAccountDetailsEndPoint = new StringBuffer( configurationService.getConfiguration().getString(LlacommonaddonConstants.FLOW_USER_INFO_URL)).append("/accounts").toString();
		final CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		final HttpGet httpGet = new HttpGet(userAccountDetailsEndPoint);
		httpGet.addHeader(LlacommonaddonConstants.AUTHORIZATION, LlacommonaddonConstants.BEARER + access_token);
        httpGet.addHeader(LlacommonaddonConstants.CONTENT_TYPE, LlacommonaddonConstants.APPLICATION_JSON);
		CloseableHttpResponse result;
		try
		{
			result = httpClient.execute(httpGet);
			if (null != result.getStatusLine() && result.getStatusLine().getStatusCode() == LlacommonaddonConstants.SUCCESS_RESPONSE)
			{
				ObjectMapper mapper = new ObjectMapper();
				mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
				final String responseJson = EntityUtils.toString(result.getEntity(), LlacommonaddonConstants.UTF_8);
				LOG.info(String.format("User accounts : %s", responseJson));
				if(StringUtils.isNotEmpty(responseJson)) {
					List<BillingAccountResponseDTO> billingAccountResponsesList = Arrays.asList(mapper.readValue(responseJson, BillingAccountResponseDTO[].class));
					createCustomerBillingAccount(customerLoggingIn,billingAccountResponsesList);
					LOG.info(String.format("Billing accounts %s to be mapped for customer : %s", billingAccountResponsesList.size(), customerLoggingIn));
				}else{
					LOG.error(String.format("Received invalid response from Service :: %s",responseJson));
				}
			}else {
				LOG.error(String.format("Unable to get the User Account from Flow System due to status code::: %s ",
						result.getStatusLine().getStatusCode()));
			}

		}
		catch (final ClientProtocolException clientProtocolException)
		{
			// XXX Auto-generated catch block
			LOG.error(LlacommonaddonConstants.API_EXCEPTION_MSG,clientProtocolException);
		}
		catch (final IOException ioException)
		{
			LOG.error(LlacommonaddonConstants.API_EXCEPTION_MSG,ioException);
		}

	}

	/**
	 * Create Customer Billing Account Using Flow Response
	 * @param billingAccountResponsesList
	 */
	private void createCustomerBillingAccount(final String emailAddress,List<BillingAccountResponseDTO> billingAccountResponsesList) {

		final CustomerModel customerModel=(CustomerModel) userService.getUserForUID(emailAddress);
		final Set<TmaBillingAccountModel> billingAccountModelList=new HashSet<>();
		llaTmaBillingAccountService.deleteBillingAccount(customerModel);
		LOG.info(String.format("Deleted billing acccounts for customer %s",customerModel.getContactEmail()));
		if(CollectionUtils.isNotEmpty(billingAccountResponsesList)){
			for(BillingAccountResponseDTO billingRespone:billingAccountResponsesList){
				if(billingRespone.getType().contains(LlacommonaddonConstants.BILLING_ACCOUNT)){
					List<SubscriptionAttributesDTO> attributesDTOS=billingRespone.getAttributes();
					final Map<String,String> attributeMap=createAttributeMap(attributesDTOS);
					if(MapUtils.isNotEmpty(attributeMap)){
						setInfoForCustomerBillingAccount(attributeMap,billingAccountResponsesList,customerModel,billingAccountModelList);
								}
							}
						}
					}
		/*if(CollectionUtils.isNotEmpty(billingAccountModelList)){
			sessionService.setAttribute("billingAccountList",billingAccountModelList);
		}*/
	}
	
private void setInfoForCustomerBillingAccount(Map<String,String> attributeMap,List<BillingAccountResponseDTO> billingAccountResponsesList, CustomerModel customerModel,Set<TmaBillingAccountModel> billingAccountModelList) {
		
		final String billingAccountNumber=attributeMap.get(LlacommonaddonConstants.COM_UXPSYSTEMS_MINT_ACCOUNT_ACCOUNT_NUMBER);
		final String billingSystem=attributeMap.get(LlacommonaddonConstants.SOURCE_BSS);
		 try{
			 if(null!=tmaBillingAccountService.findBillingAccount(billingSystem,billingAccountNumber)){
				LOG.info(String.format("Billing Account Number  :: %s already exist",billingAccountNumber));
			 }
			}catch(ModelNotFoundException exception){
		 		TmaBillingAccountModel billingAccount = tmaBillingAccountService.createBillingAccount(billingSystem,billingAccountNumber);
		 		billingAccount.setCustomer(customerModel);
			 	modelService.save(billingAccount);
			 	billingAccountModelList.add(billingAccount);
		   }
		if(SourceEPC.LIBERATE.getCode().equalsIgnoreCase(billingSystem)){
			List<String> liberateAccounts = new ArrayList<>(customerModel.getLiberateAccounts());
			liberateAccounts.add(billingAccountNumber);
			customerModel.setLiberateAccounts(liberateAccounts);
		}
		if(SourceEPC.CERILLION.getCode().equalsIgnoreCase(billingSystem)){
			List<String> cerellionAccounts = new ArrayList<>(customerModel.getCerillionAccounts());
			cerellionAccounts.add(billingAccountNumber);
			customerModel.setCerillionAccounts(cerellionAccounts);
		}

		modelService.save(customerModel);
		modelService.refresh(customerModel);
	}

	/**
	 * Create Attribute Map from Response
	 * @param attributesDTOSList
	 * @return
	 */
	private Map<String, String> createAttributeMap(List<SubscriptionAttributesDTO> attributesDTOSList) {
		final Map<String,String> attrMap=new HashMap<>();
		for(SubscriptionAttributesDTO subscriptionAttributesDTO:attributesDTOSList){
			attrMap.put(subscriptionAttributesDTO.getName(),subscriptionAttributesDTO.getValue());
		}
		return attrMap;
	}

	/**
	 * @param userDetailResponse
	 * @throws DuplicateUidException
	 * @throws IllegalArgumentException
	 * @throws UnknownIdentifierException
	 */
	private String createCustomerIfNotExists(final OpenIdpUserDetailResponse userDetailResponse) throws UnknownIdentifierException, IllegalArgumentException, DuplicateUidException, LLAApiException
	{
		LOG.info(String.format("Trying to Login/Register Customer having Flow ID ::: %s and name :: %s",userDetailResponse.getId(),userDetailResponse.getDisplayName()));
		final String uid = userDetailResponse.getEmailAddress();
		String[] names=userDetailResponse.getDisplayName().split("\\s",2);
		if (!userService.isUserExisting(uid))
		{
			registerCustomer(userDetailResponse, names);
		}else{
			CustomerModel customerModel=(CustomerModel) userService.getUserForUID(uid);
			if(null!=customerModel){
				updateCustomerName(userDetailResponse, names, customerModel);
			}
		}
		setCustomerUserGroup(uid);
		return uid;

	}

	private void setCustomerUserGroup(final String uid) {
		CustomerModel customerModel=(CustomerModel) userService.getUserForUID(uid);
		final String customerGroup = siteConfigService.getProperty("user.group.code");
		if(customerGroup.equalsIgnoreCase(cmsSiteService.getCurrentSite().getUid())){
				final Set<PrincipalGroupModel> userGroup = new HashSet<PrincipalGroupModel>(customerModel.getGroups());
				userGroup.add(userService.getUserGroupForUID(customerGroup));
				customerModel.setGroups(userGroup);
				LOG.info(String.format("UserGroup %s adding to customer: %s", customerGroup , customerModel.getUid()));
		}
	}

	/**
	 * Register Customer
	 * @param userDetailResponse
	 * @param names
	 * @throws DuplicateUidException
	 */
	private void registerCustomer(OpenIdpUserDetailResponse userDetailResponse, String[] names) throws DuplicateUidException, LLAApiException {
		final RegisterData registerData = new RegisterData();
		registerData.setLogin(userDetailResponse.getEmailAddress());
		registerData.setFlowId(userDetailResponse.getId());
		if(names.length==2) {
			registerData.setLastName(names[1]);
		}
		registerData.setFirstName(names[0]);

	    registerData.setPassword(LlacommonaddonConstants.TEMP_CREDENTIALS);
		llaCustomerFacade.register(registerData);
	}

	/**
	 * Update Customer Name
	 * @param userDetailResponse
	 * @param names
	 * @param customerModel
	 */
	private void updateCustomerName(OpenIdpUserDetailResponse userDetailResponse, String[] names, CustomerModel customerModel) {
		customerModel.setName(userDetailResponse.getDisplayName());
		if(names.length==2){
			customerModel.setLastName(names[1]);
		}
		customerModel.setFirstName(names[0]);
		customerModel.setFlowId(userDetailResponse.getId());
		modelService.save(customerModel);
		modelService.refresh(customerModel);
	}

	/**
	 *
	 * @param request
	 * @param method
	 * @return
	 */
	public String authUrlConstructor(final HttpServletRequest request, final String method)
	{
	  if(StringUtils.isNotEmpty((String)sessionService.getAttribute(LlacommonaddonConstants.AUTH_URL)) && ((String) sessionService.getAttribute(LlacommonaddonConstants.AUTH_URL)).contains("/"+commerceCommonI18NService.getCurrentLanguage().getIsocode()+"/")){
			return (String)sessionService.getAttribute(LlacommonaddonConstants.AUTH_URL);
		}
		else {
	    final StringBuilder url = new StringBuilder(openIdpUrlConstructor(method));
		final Configuration config = configurationService.getConfiguration();
		url.append("?").append("response_type=code&scope=openid&client_id=").append(config.getString(LlacommonaddonConstants.CLIENT_ID))
				.append("&redirect_uri=");
		final String siteUid=cmsSiteService.getCurrentSite().getUid();
		String authorizationUrl=prepareRedirectUri(siteUid, url, config).toString();
		sessionService.setAttribute(LlacommonaddonConstants.AUTH_URL,authorizationUrl);
		return authorizationUrl;
	   }
	}

	/**
	 * @param siteUid
	 * @param url
	 * @param config
	 */
	private StringBuilder prepareRedirectUri(final String siteUid, final StringBuilder url, final Configuration config)
	{
		final StringBuilder redirectUri = new StringBuilder();
		url.append(config.getString(redirectUri.append(siteUid).append(".").append("redirect.uri").toString())).append(commerceCommonI18NService.getCurrentLanguage().getIsocode()).append("/");
		return url;
	}

	private String tokenUrlConstructor(final String authCode)
	{
		final Configuration config = configurationService.getConfiguration();
		final StringBuilder url = new StringBuilder(config.getString(LlacommonaddonConstants.TOKEN_OPENID_URL));
		final String siteUid = cmsSiteService.getCurrentSite().getUid();
		url.append("?grant_type=authorization_code&code=").append(authCode).append("&redirect_uri=");
		return prepareRedirectUri(siteUid, url, config).toString();
	}

	/**
	 * Construct Open Idp Url
	 * @param method
	 * @return
	 */
	private String openIdpUrlConstructor(final String method)
	{

		final StringBuilder urlBuilder = new StringBuilder(method);
		urlBuilder.append(".").append(LlacommonaddonConstants.OPENID_URL);
		return configurationService.getConfiguration().getString(urlBuilder.toString());
	}


}
