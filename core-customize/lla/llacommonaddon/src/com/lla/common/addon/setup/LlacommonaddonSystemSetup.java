package com.lla.common.addon.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;

import java.util.Collections;
import java.util.List;

@SystemSetup(
        extension = "llacommonaddon"
)
public class LlacommonaddonSystemSetup extends AbstractSystemSetup {

    @SystemSetup(type = SystemSetup.Type.ESSENTIAL, process = SystemSetup.Process.ALL)
    public void createEssentialData(final SystemSetupContext context)
    {
        importImpexFile(context, "/llacommonaddon/import/common/user-groups.impex");
        importImpexFile(context, "/llacommonaddon/import/essentiadataWorkFlow.impex");
             importImpexFile(context, "/llacommonaddon/import/essentialdataWorkFlow_es.impex");
    }

    @SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.ALL)
    public void createProjectData(final SystemSetupContext context)
    {
        importImpexFile(context, "/llacommonaddon/import/common/common-addon-extra.impex");
    }

    @Override
    public List<SystemSetupParameter> getInitializationOptions() {
        return Collections.EMPTY_LIST;
    }
}
