package com.lla.common.addon.facade;

import de.hybris.platform.commercefacades.order.data.OrderData;

public interface OrderConfirmationFacade{

    void launchWorkflow(OrderData orderData);

}