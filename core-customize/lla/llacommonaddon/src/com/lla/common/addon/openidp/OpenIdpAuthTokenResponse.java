/**
 *
 */
package com.lla.common.addon.openidp;

/**
 * @author GZ132VA
 *
 */
public class OpenIdpAuthTokenResponse
{
	private String access_token;
	private String scope;
	private String token_type;
	private long expires_in;
	private String id_token;

	/**
	 * @return the access_token
	 */
	public String getAccess_token()
	{
		return access_token;
	}

	/**
	 * @param access_token
	 *           the access_token to set
	 */
	public void setAccess_token(final String access_token)
	{
		this.access_token = access_token;
	}

	/**
	 * @return the scope
	 */
	public String getScope()
	{
		return scope;
	}

	/**
	 * @param scope
	 *           the scope to set
	 */
	public void setScope(final String scope)
	{
		this.scope = scope;
	}

	/**
	 * @return the token_type
	 */
	public String getToken_type()
	{
		return token_type;
	}

	/**
	 * @param token_type
	 *           the token_type to set
	 */
	public void setToken_type(final String token_type)
	{
		this.token_type = token_type;
	}

	/**
	 * @return the expires_in
	 */
	public long getExpires_in()
	{
		return expires_in;
	}

	/**
	 * @param expires_in
	 *           the expires_in to set
	 */
	public void setExpires_in(final long expires_in)
	{
		this.expires_in = expires_in;
	}

	/**
	 * @return the id_token
	 */
	public String getId_token()
	{
		return id_token;
	}

	/**
	 * @param id_token
	 *           the id_token to set
	 */
	public void setId_token(final String id_token)
	{
		this.id_token = id_token;
	}
}
