///*
// * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
// */
//package org.llapayment.setup;
//
//import static org.llapayment.constants.LlapaymentConstants.PLATFORM_LOGO_CODE;
//
//import de.hybris.platform.core.initialization.SystemSetup;
//
//import java.io.InputStream;
//
//import org.llapayment.constants.LlapaymentConstants;
//import org.llapayment.service.LlapaymentService;
//
//
//@SystemSetup(extension = LlapaymentConstants.EXTENSIONNAME)
//public class LlapaymentSystemSetup
//{
//	private final LlapaymentService llapaymentService;
//
//	public LlapaymentSystemSetup(final LlapaymentService llapaymentService)
//	{
//		this.llapaymentService = llapaymentService;
//	}
//
//	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
//	public void createEssentialData()
//	{
//		llapaymentService.createLogo(PLATFORM_LOGO_CODE);
//	}
//
//	private InputStream getImageStream()
//	{
//		return LlapaymentSystemSetup.class.getResourceAsStream("/llapayment/sap-hybris-platform.png");
//	}
//}
