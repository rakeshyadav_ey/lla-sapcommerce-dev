package org.llapayment.populator;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.CustomerInfoResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.CustomerInfoData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

public class LLACustomerInfoPopulator extends CustomerInfoResultPopulator {

    @Override
    public void populate(final Map<String, String> source, final CreateSubscriptionResult target) throws ConversionException
    {
        super.populate(source,target);
        final String area=source.get("area");
        if (StringUtils.isNotBlank(area)){
            CustomerInfoData data=target.getCustomerInfoData();
            data.setArea(area);
            target.setCustomerInfoData(data);
        }
    }
}
