package org.llapayment.client;


import com.firstatlanticcommerce.schemas.gateway.Authorize3DSResponse;
import com.firstatlanticcommerce.schemas.gateway.AuthorizeResponse;
import com.firstatlanticcommerce.schemas.gateway.ObjectFactory;
import com.firstatlanticcommerce.schemas.gateway.TransactionModificationResponse;
import com.firstatlanticcommerce.schemas.gateway.*;
import com.firstatlanticcommerce.schemas.gateway.data.*;
import com.lla.core.util.LLAEncryptionUtil;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import com.llapayment.data.LLAPaymentAuthData;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorservices.payment.utils.impl.DefaultAcceleratorDigestUtils;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapMessage;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LLAPaymentClient {

    private static final Logger LOG = Logger.getLogger(LLAPaymentClient.class);
    private static final String AUTH_ENDPOINT_URL = "payment.integrations.auth.service.endpoint";
    private static final String AUTH_REQUEST_SOAP_ACTION = "payment.integrations.auth.action.request";
    private static final String CAPTURE_REQUEST_SOAP_ACTION="payment.integrations.capture.action.request";
    private static final String TOKEN_REQUEST_SOAP_ACTION="payment.integrations.token.action.request";
    private static final String FAC_CURRENCY_ID = "fac.currency.id";
    private static final String FAC_MERCHANT_ID="fac.merchant.id";
    private static final String FAC_TRANSACTION_TOKEN_ID = "fac.transaction.token.id";
    private static final String FAC_MERCHANT_PASSWORD = "fac.merchant.password";
    private static final String TOKEN_CHAR = "payment.token.char";
    public static final String DECIMAL = ".";
    public static final String PAYMENT_LOG_ENABLE = "payment.log.enable";
    @Autowired
    ConfigurationService configurationService;

    @Autowired
    private WebServiceTemplate llaWebServiceTemplate;
    @Autowired
    private LLAEncryptionUtil llaEncryptionUtil;
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;
    @Autowired
    private SessionService sessionService;
    @Autowired
    private CartService cartService;

    @Autowired
    private BaseStoreService baseStoreService;

    @Autowired
    private PersistentKeyGenerator merchantTokenCodeGenerator;

    @Autowired
    private CommerceCommonI18NService commerceCommonI18NService;

    @Autowired
    private BaseSiteService baseSiteService;

    @Autowired
    private CustomerAccountService customerAccountService;

    @Autowired
    private DefaultAcceleratorDigestUtils llaAcceleratorDigestUtils;

    @Autowired
    private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

    @Autowired
    private SiteConfigService siteConfigService;

    public AuthorizeResponse createTokenRequest(LLAPaymentAuthData paymentDetails, CartModel cartModel) {
        ObjectFactory objectFactory = new ObjectFactory();
        com.firstatlanticcommerce.schemas.gateway.data.ObjectFactory objectFactoryData = new com.firstatlanticcommerce.schemas.gateway.data.ObjectFactory();
        Authorize authorize = objectFactory.createAuthorize();
        AuthorizeRequest authorizeRequest = objectFactoryData.createAuthorizeRequest();
        BillingDetails billingDetails = objectFactoryData.createBillingDetails();
        populateBillingDetails(paymentDetails,billingDetails);
        authorizeRequest.setBillingDetails(objectFactoryData.createBillingDetails(billingDetails));
        CardDetails cardDetails = objectFactoryData.createCardDetails();
        populateCardDetails(paymentDetails,cardDetails);
        authorizeRequest.setCardDetails(cardDetails);
        TransactionDetails transactionDetails = objectFactoryData.createTransactionDetails();
        populateTransactionDetails(cartModel,transactionDetails);
        authorizeRequest.setTransactionDetails(transactionDetails);
        ShippingDetails shippingDetails = objectFactoryData.createShippingDetails();
        populateShippingDetails(shippingDetails,cartModel);
        authorizeRequest.setShippingDetails(objectFactoryData.createShippingDetails(shippingDetails));
        JAXBElement<AuthorizeRequest> authRequest = objectFactory.createAuthorizeRequest(authorizeRequest);
        authorize.setRequest(authRequest);
        if (siteConfigService.getBoolean(PAYMENT_LOG_ENABLE, false))
        {
            LOG.info("Token Request "+logXmlMessage(authRequest));
        }
        com.firstatlanticcommerce.schemas.gateway.AuthorizeResponse authorizeResponse = objectFactory.createAuthorizeResponse();
        try
        {
            authorizeResponse = (com.firstatlanticcommerce.schemas.gateway.AuthorizeResponse)sendAndReceive(
                    configurationService.getConfiguration().getString(AUTH_ENDPOINT_URL), authorize,configurationService.getConfiguration().getString(TOKEN_REQUEST_SOAP_ACTION));
            //this.logMessage(authorizeResponse,false,Level.INFO);
            CreditCardTransactionResults transactionResult = null;
            if(null != authorizeResponse.getAuthorizeResult() && null != authorizeResponse.getAuthorizeResult().getValue() && null != authorizeResponse.getAuthorizeResult().getValue().getCreditCardTransactionResults() && null != authorizeResponse.getAuthorizeResult().getValue().getCreditCardTransactionResults().getValue())
            {
                transactionResult = authorizeResponse.getAuthorizeResult().getValue().getCreditCardTransactionResults().getValue();
            }
            if(null != transactionResult)
            {
                LOG.debug("Tokenization response code: "+transactionResult.getReasonCode().getValue());
                LOG.debug("Tokenization reason code: "+transactionResult.getOriginalResponseCode().getValue());
                LOG.debug("Tokenization reason code description: "+transactionResult.getReasonCodeDescription().getValue());
            }
            if (null != authorizeResponse.getAuthorizeResult() && siteConfigService.getBoolean(PAYMENT_LOG_ENABLE, false))
            {
                LOG.info("Token response "+logXmlMessage(authorizeResponse.getAuthorizeResult()));
            }
        }
        catch (final Exception e)
        {
            LOG.debug("Exception in Auth WebService Call:", e);
        }
        return authorizeResponse;
    }

    public Authorize3DSResponse createAuthRequest(CartData cart) {
        ObjectFactory objectFactory = new ObjectFactory();
        CartModel cartModel = cartService.getSessionCart();
        com.firstatlanticcommerce.schemas.gateway.data.ObjectFactory objectFactoryData = new com.firstatlanticcommerce.schemas.gateway.data.ObjectFactory();
        Authorize authorize = objectFactory.createAuthorize();
        Authorize3DSRequest authorizeRequest = objectFactoryData.createAuthorize3DSRequest();
        BillingDetails billingDetails = objectFactoryData.createBillingDetails();
        authorizeRequest.setBillingDetails(objectFactoryData.createBillingDetails(billingDetails));
        CardDetails cardDetails = objectFactoryData.createCardDetails();
        populateCardDetailsForAuth(cartModel,cardDetails);
        authorizeRequest.setCardDetails(objectFactoryData.createAuthorize3DSRequestCardDetails(cardDetails));
        TransactionDetails transactionDetails = objectFactoryData.createTransactionDetails();
        populateTransactionDetailsForAuth(cart,transactionDetails);
        authorizeRequest.setTransactionDetails(objectFactoryData.createAuthorize3DSRequestTransactionDetails(transactionDetails));
        ShippingDetails shippingDetails = objectFactoryData.createShippingDetails();
        populateShippingDetails(shippingDetails,cartModel);
        authorizeRequest.setShippingDetails(objectFactoryData.createShippingDetails(shippingDetails));
        String merchantResponseURL = getFullResponseUrl("/checkout/multi/summary/threedsresponse",true);
        authorizeRequest.setMerchantResponseURL(objectFactoryData.createAuthorize3DSRequestMerchantResponseURL(merchantResponseURL));
        JAXBElement<Authorize3DSRequest> authRequest = objectFactory.createAuthorize3DSRequest(authorizeRequest);
        Authorize3DS authorize3DS = objectFactory.createAuthorize3DS();
        authorize3DS.setRequest(authRequest);
        if (siteConfigService.getBoolean(PAYMENT_LOG_ENABLE, false))
        {
            LOG.info("authRequest "+logXmlMessage(authRequest));
        }
        com.firstatlanticcommerce.schemas.gateway.Authorize3DSResponse authorizeResponse = objectFactory.createAuthorize3DSResponse();
        try
        {
            authorizeResponse = (com.firstatlanticcommerce.schemas.gateway.Authorize3DSResponse)sendAndReceive(
                    configurationService.getConfiguration().getString(AUTH_ENDPOINT_URL), authorize3DS,configurationService.getConfiguration().getString(AUTH_REQUEST_SOAP_ACTION));
            //this.logMessage(authorizeResponse,false,Level.INFO);
            com.firstatlanticcommerce.schemas.gateway.data.Authorize3DSResponse authorizationResult = null;
            if(null != authorizeResponse.getAuthorize3DSResult() && null != authorizeResponse.getAuthorize3DSResult().getValue() && null != authorizeResponse.getAuthorize3DSResult().getValue())
            {
                authorizationResult = authorizeResponse.getAuthorize3DSResult().getValue();
            }
            if(null != authorizationResult) {
                LOG.debug("Authorization response code: " + authorizationResult.getResponseCode().getValue());
                LOG.debug("Authorization response code description: " + authorizationResult.getResponseCodeDescription().getValue());
            }
            if (null != authorizeResponse.getAuthorize3DSResult() && siteConfigService.getBoolean(PAYMENT_LOG_ENABLE, false))
            {
                LOG.info("Authorization response "+logXmlMessage(authorizeResponse.getAuthorize3DSResult()));
            }
        }
        catch (final Exception e)
        {
            LOG.debug("Exception in Auth WebService Call:", e);
        }
        return authorizeResponse;
    }

    public TransactionModificationResponse sendCaptureRequest(final OrderData order) throws LLAApiException {
        final BaseStoreModel baseStore = null != baseStoreService.getCurrentBaseStore() ? baseStoreService.getCurrentBaseStore() : baseStoreService.getBaseStoreForUid(order.getStore());
        OrderModel orderModel = customerAccountService.getOrderForCode(order.getCode(),baseStore);
        double modificationAmt = order.getTotalPriceWithTax().getValue().doubleValue();
        return this.modifyAmountRequest(orderModel, 1, modificationAmt);
    }

    public TransactionModificationResponse sendRefundRequest(final OrderModel orderModel) throws LLAApiException {
        Double modificationAmount = orderModel.getTotalPrice()+orderModel.getTotalTax();
        return this.modifyAmountRequest(orderModel, 2, modificationAmount);
    }

    private TransactionModificationResponse modifyAmountRequest(final OrderModel orderModel, int modificationType, double modificationAmount) throws LLAApiException {
        ObjectFactory objectFactory = new ObjectFactory();
        com.firstatlanticcommerce.schemas.gateway.data.ObjectFactory objectFactoryData = new com.firstatlanticcommerce.schemas.gateway.data.ObjectFactory();
        TransactionModification transactionModification = objectFactory.createTransactionModification();
        TransactionModificationRequest transactionModificationRequest=objectFactoryData.createTransactionModificationRequest();
        transactionModificationRequest.setAcquirerId(objectFactoryData.createTransactionModificationRequestAcquirerId(configurationService.getConfiguration().getString("payment.acquirer.id")));
        transactionModificationRequest.setOrderNumber(objectFactoryData.createTransactionModificationRequestOrderNumber(orderModel.getCartNumber()));
        transactionModificationRequest.setCurrencyExponent(2);
        transactionModificationRequest.setMerchantId(objectFactoryData.createTransactionModificationRequestMerchantId(configurationService.getConfiguration().getString(new StringBuffer(FAC_MERCHANT_ID).append(DECIMAL).append(orderModel.getSite().getUid()).toString())));
        transactionModificationRequest.setModificationType(modificationType);
        transactionModificationRequest.setAmount(objectFactoryData.createTransactionModificationRequestAmount(StringUtils.leftPad(String.valueOf(Math.round( orderModel.getPaymentTransactions().iterator().next().getPlannedAmount().doubleValue() * 100)),12,'0')));
        transactionModificationRequest.setPassword(objectFactoryData.createTransactionModificationRequestPassword(configurationService.getConfiguration().getString(new StringBuffer(FAC_MERCHANT_PASSWORD).append(DECIMAL).append(orderModel.getSite().getUid()).toString())));
        JAXBElement<TransactionModificationRequest> captureRequest = objectFactory.createTransactionModificationRequest(transactionModificationRequest);
        transactionModification.setRequest(captureRequest);
        if (configurationService.getConfiguration().getBoolean(new StringBuffer(PAYMENT_LOG_ENABLE).append(DECIMAL).append(orderModel.getSite().getUid()).toString(), false))
        {
            LOG.info("Capture request "+logXmlMessage(captureRequest));
        }
        TransactionModificationResponse captureResponse = objectFactory.createTransactionModificationResponse();
        try
        {
            captureResponse = (com.firstatlanticcommerce.schemas.gateway.TransactionModificationResponse)sendAndReceive(
                    configurationService.getConfiguration().getString(AUTH_ENDPOINT_URL), transactionModification,configurationService.getConfiguration().getString(CAPTURE_REQUEST_SOAP_ACTION));

            if (null != captureResponse && null != captureResponse.getTransactionModificationResult() && configurationService.getConfiguration().getBoolean(new StringBuffer(PAYMENT_LOG_ENABLE).append(DECIMAL).append(orderModel.getSite().getUid()).toString(), false))
            {
                LOG.info("Capture response "+logXmlMessage(captureResponse.getTransactionModificationResult()));
            }

        }
        catch (final Exception e)
        {
            LOG.debug("Exception in Auth WebService Call:", e);
            throw new LLAApiException(String.format("Exception in Capture WebService Call:",e.getMessage()));
        }
        return captureResponse;
    }

    private String logXmlMessage(JAXBElement element) {
        try {
            JAXBContext jc = JAXBContext.newInstance(element.getValue().getClass());
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            marshaller.marshal(element, baos);
            return baos.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private void populateCardDetailsForAuth(CartModel cartModel, CardDetails cardDetails) {
        com.firstatlanticcommerce.schemas.gateway.data.ObjectFactory obj = new com.firstatlanticcommerce.schemas.gateway.data.ObjectFactory();
        cardDetails.setCardCVV2(obj.createCardDetailsCardCVV2(llaEncryptionUtil.doDecryption(sessionService.getAttribute("encryptedCVV").toString())));
        cardDetails.setCardNumber(obj.createCardDetailsCardNumber(llaEncryptionUtil.doDecryption(sessionService.getAttribute("encryptedCard").toString())));
        cardDetails.setCardExpiryDate(obj.createCardDetailsCardExpiryDate((org.apache.commons.lang3.StringUtils.leftPad(((CreditCardPaymentInfoModel) cartModel.getPaymentInfo()).getValidToMonth(),2,"0")+((CreditCardPaymentInfoModel) cartModel.getPaymentInfo()).getValidToYear().substring(2))));
    }

    private void populateTransactionDetailsForAuth(CartData cart, TransactionDetails transactionDetails) {
        com.firstatlanticcommerce.schemas.gateway.data.ObjectFactory Object = new com.firstatlanticcommerce.schemas.gateway.data.ObjectFactory();
        transactionDetails.setAcquirerId(configurationService.getConfiguration().getString("payment.acquirer.id"));
        transactionDetails.setOrderNumber(cart.getCode());
        if(llaMulesoftIntegrationUtil.isJamaicaOrder(baseSiteService.getBaseSiteForUID(cart.getSite()))) {
            transactionDetails.setAmount(StringUtils.leftPad(String.valueOf(Math.round(cart.getTotalPriceWithTax().getValue().doubleValue() * 100)),12,'0'));
        }else{
            transactionDetails.setAmount(StringUtils.leftPad(String.valueOf(Math.round(cart.getTotalPrice().getValue().doubleValue() * 100)), 12, '0'));
        }
        transactionDetails.setMerchantId(siteConfigService.getProperty(FAC_MERCHANT_ID));
        transactionDetails.setCurrency(siteConfigService.getProperty(FAC_CURRENCY_ID));
        transactionDetails.setCurrencyExponent(2);
        transactionDetails.setSignature(signature(transactionDetails.getOrderNumber(),transactionDetails.getAmount()));
        transactionDetails.setSignatureMethod("SHA1");
        transactionDetails.setTransactionCode(Integer.parseInt(configurationService.getConfiguration().getString("fac.transaction.auth.id")));
        transactionDetails.setCustomerReference(Object.createTransactionDetailsCustomerReference(cart.getUser().getUid()));
    }

    private void populateShippingDetails(ShippingDetails shippingDetails, CartModel cartModel) {
        com.firstatlanticcommerce.schemas.gateway.data.ObjectFactory obj = new com.firstatlanticcommerce.schemas.gateway.data.ObjectFactory();
        if(null != cartModel.getDeliveryAddress()){
            shippingDetails.setShipToAddress(obj.createShippingDetailsShipToAddress(cartModel.getDeliveryAddress().getLine1()));
            shippingDetails.setShipToAddress2(obj.createShippingDetailsShipToAddress2(cartModel.getDeliveryAddress().getLine2()));
            shippingDetails.setShipToCountry(obj.createShippingDetailsShipToCountry(siteConfigService.getProperty("fac.currency.id")));
            shippingDetails.setShipToFirstName(obj.createShippingDetailsShipToFirstName(cartModel.getDeliveryAddress().getFirstname()));
            shippingDetails.setShipToLastName(obj.createShippingDetailsShipToLastName(cartModel.getDeliveryAddress().getLastname()));
            shippingDetails.setShipToMobile(obj.createShippingDetailsShipToMobile(cartModel.getDeliveryAddress().getPhone1()));
            shippingDetails.setShipToZipPostCode(obj.createShippingDetailsShipToZipPostCode(cartModel.getDeliveryAddress().getPostalcode()));
        }
    }

    private void populateCardDetails(LLAPaymentAuthData paymentDetailsForm, CardDetails cardDetails) {
        com.firstatlanticcommerce.schemas.gateway.data.ObjectFactory obj = new com.firstatlanticcommerce.schemas.gateway.data.ObjectFactory();
        cardDetails.setCardCVV2(obj.createCardDetailsCardCVV2(paymentDetailsForm.getCard_cvNumber()));
        cardDetails.setCardNumber(obj.createCardDetailsCardNumber(paymentDetailsForm.getCard_accountNumber()));
        StringBuffer cardExpiryDate=new StringBuffer();
        if(StringUtils.isNotEmpty(paymentDetailsForm.getCard_expirationYear())){
            cardExpiryDate.append(paymentDetailsForm.getCard_expirationMonth()).append(paymentDetailsForm.getCard_expirationYear().substring(2));
            cardDetails.setCardExpiryDate(obj.createCardDetailsCardExpiryDate(cardExpiryDate.toString()));
        }
    }

    private void populateBillingDetails(LLAPaymentAuthData paymentDetailsForm, BillingDetails billingDetails) {
        com.firstatlanticcommerce.schemas.gateway.data.ObjectFactory obj = new com.firstatlanticcommerce.schemas.gateway.data.ObjectFactory();
        billingDetails.setBillToAddress(obj.createBillingDetailsBillToAddress(paymentDetailsForm.getBillTo_street1()));
        billingDetails.setBillToAddress2(obj.createBillingDetailsBillToAddress2(paymentDetailsForm.getBillTo_street2()));
        billingDetails.setBillToCity(obj.createBillingDetailsBillToCity(paymentDetailsForm.getBillTo_city()));
        billingDetails.setBillToCountry(obj.createBillingDetailsBillToCountry(siteConfigService.getProperty(FAC_CURRENCY_ID)));
        //billingDetails.setBillToEmail(obj.createBillingDetailsBillToEmail(paymentDetailsForm.getBillTo_email()));
        billingDetails.setBillToFirstName(obj.createBillingDetailsBillToFirstName(paymentDetailsForm.getBillTo_firstName()));
        billingDetails.setBillToLastName(obj.createBillingDetailsBillToLastName(paymentDetailsForm.getBillTo_lastName()));
        billingDetails.setBillToState(obj.createBillingDetailsBillToState(paymentDetailsForm.getBillTo_state()));
        billingDetails.setBillToMobile(obj.createBillingDetailsBillToMobile(paymentDetailsForm.getBillTo_phoneNumber()));
        billingDetails.setBillToZipPostCode(obj.createBillingDetailsBillToZipPostCode(paymentDetailsForm.getBillTo_postalCode()));
    }


    protected Object sendAndReceive(final String svcUrl,Object request,String soapaction)
            throws WebServiceClientException, LLAApiException {
        Object obj = null;
        try {
            llaWebServiceTemplate.setDefaultUri(URLDecoder.decode(svcUrl,"UTF-8"));
            LOG.info("Webservice call to:" + llaWebServiceTemplate.getDefaultUri());
            //llaWebServiceTemplate.setFaultMessageResolver(new SimpleFaultMessageResolver());
            //this.setServiceTimeout();
            if(StringUtils.isNotBlank(soapaction))
            {
                obj = llaWebServiceTemplate.marshalSendAndReceive(request, new WebServiceMessageCallback() {
                    public void doWithMessage(WebServiceMessage message) {
                        ((SoapMessage)message).setSoapAction(soapaction);
                    }
                });
            }
            else{
                obj = llaWebServiceTemplate.marshalSendAndReceive(request);
            }
            //this.logMessage(request,true,Level.INFO);
        }
        catch (final Exception e)
        {
            LOG.error("ERROR sending and receiving SOAP message!", e);
            throw new LLAApiException(String.format("Exception in SOAP WebService Call:",e.getMessage()));
        }
        return obj;
    }

    protected String getFullResponseUrl(final String responseUrl, final boolean isSecure)
    {
        final BaseSiteModel currentBaseSite = baseSiteService.getCurrentBaseSite();

        final String fullResponseUrl = siteBaseUrlResolutionService.getWebsiteUrlForSite(currentBaseSite, isSecure,
                responseUrl);

        return fullResponseUrl == null ? "" : fullResponseUrl;
    }

    private void  populateTransactionDetails(CartModel cartData,TransactionDetails transactionDetails)
    {
        com.firstatlanticcommerce.schemas.gateway.data.ObjectFactory Object = new com.firstatlanticcommerce.schemas.gateway.data.ObjectFactory();
        transactionDetails.setAcquirerId(configurationService.getConfiguration().getString("payment.acquirer.id"));
        String tokenChar = siteConfigService.getProperty(TOKEN_CHAR);
        transactionDetails.setOrderNumber(tokenChar+merchantTokenCodeGenerator.generate().toString());

        if(llaMulesoftIntegrationUtil.isJamaicaOrder(baseSiteService.getBaseSiteForUID(cartData.getSite().getUid()))) {
            transactionDetails.setAmount("000000000000");
        }else{
            transactionDetails.setAmount(StringUtils.leftPad(String.valueOf(Math.round(configurationService.getConfiguration().getInt("token.currency.amount",1) * 100)), 12, '0'));
        }
       // transactionDetails.setAmount("000000000000");
        transactionDetails.setMerchantId(siteConfigService.getProperty(FAC_MERCHANT_ID));
        transactionDetails.setCurrency(siteConfigService.getProperty("fac.usa.currency.id"));
        transactionDetails.setCurrencyExponent(2);
        transactionDetails.setSignature(tokenSignature(transactionDetails.getOrderNumber(),transactionDetails.getAmount()));
        transactionDetails.setSignatureMethod("SHA1");
        transactionDetails.setTransactionCode(Integer.parseInt(siteConfigService.getProperty(FAC_TRANSACTION_TOKEN_ID)));
        transactionDetails.setCustomerReference(Object.createTransactionDetailsCustomerReference(cartData.getUser().getUid()));
    }

    String signature(String orderNumber,String amount)
    {
        StringBuffer stringtohash = new StringBuffer();
        stringtohash.append(siteConfigService.getProperty(FAC_MERCHANT_PASSWORD)).append(siteConfigService.getProperty(FAC_MERCHANT_ID)).append(configurationService.getConfiguration().getString("payment.acquirer.id")).append(orderNumber).append(amount).append(siteConfigService.getProperty(FAC_CURRENCY_ID));
        final byte[] result = getSha().digest(stringtohash.toString().getBytes());
        final Base64 encoder = new Base64();
        return encoder.encodeAsString(result);
    }

    String tokenSignature(String orderNumber,String amount)
    {
        StringBuffer stringtohash = new StringBuffer();
        stringtohash.append(siteConfigService.getProperty(FAC_MERCHANT_PASSWORD)).append(siteConfigService.getProperty(FAC_MERCHANT_ID)).append(configurationService.getConfiguration().getString("payment.acquirer.id")).append(orderNumber).append(amount).append(siteConfigService.getProperty("fac.usa.currency.id"));
        final byte[] result = getSha().digest(stringtohash.toString().getBytes());
        final Base64 encoder = new Base64();
        return encoder.encodeAsString(result);
    }

    protected MessageDigest getSha()
    {
        MessageDigest sha = null;
        try {
            sha = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return sha;
    }

    protected void logMessage(final Object xmlObject, final boolean isRequest, Level logLevel) throws JAXBException {
        ByteArrayOutputStream bos = null;
        LOG.info(xmlObject);
        if (xmlObject != null && LOG.isDebugEnabled()) {
            bos = new ByteArrayOutputStream();
            JAXBContext context = null;
            if (xmlObject instanceof JAXBElement) {
                context = JAXBContext.newInstance(((JAXBElement) xmlObject).getValue().getClass());
            } else {
                context = JAXBContext.newInstance(xmlObject.getClass());
            }

            final Marshaller marshaller = context.createMarshaller();
            marshaller.marshal(xmlObject, bos);

            if (logLevel == null) {
                logLevel = Level.DEBUG;
            }
            LOG.log(logLevel, isRequest ? "REQUEST:" : "RESPONSE:");
            LOG.log(logLevel, bos);
        }
    }
}
