/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.llapayment.constants;

/**
 * Global class for all Llapayment constants. You can add global constants for your extension into this class.
 */
public final class LlapaymentConstants extends GeneratedLlapaymentConstants
{
	public static final String EXTENSIONNAME = "llapayment";

	private LlapaymentConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "llapaymentPlatformLogo";
}
