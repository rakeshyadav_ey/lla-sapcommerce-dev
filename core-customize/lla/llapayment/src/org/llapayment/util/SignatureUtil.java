package org.llapayment.util;

import org.apache.log4j.Logger;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;


public class SignatureUtil
{

	private static final Logger LOG = Logger.getLogger(SignatureUtil.class);

	private static final String HMAC_SHA256 = "HmacSHA256";

	public static String sign(final Map<String, Object> params, final String secretKey)
	{
		return sign(buildDataToSign(params), secretKey);
	}

	private static String sign(final String data, final String secretKey)
	{
		final SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA256);

		String result = null;
		byte[] rawHmac;
		Mac mac;
		try
		{
			mac = Mac.getInstance(HMAC_SHA256);
			mac.init(secretKeySpec);
			rawHmac = mac.doFinal(data.getBytes("UTF-8"));
			result = DatatypeConverter.printBase64Binary(rawHmac).replace("\n", "");

		}
		catch (final NoSuchAlgorithmException e1)
		{
			LOG.error(e1);
		}
		catch (final InvalidKeyException e)
		{
			LOG.error(e);
		}
		catch (IllegalStateException | UnsupportedEncodingException e)
		{
			LOG.error(e);
		}
		return result;
	}

	private static String buildDataToSign(final Map<String, Object> params)
	{
		final String[] signedFieldNames = String.valueOf(params.get("signed_field_names")).split(",");
		final ArrayList<String> dataToSign = new ArrayList<String>();
		for (final String signedFieldName : signedFieldNames)
		{
			dataToSign.add(signedFieldName + "=" + String.valueOf(params.get(signedFieldName)));
		}
		return commaSeparate(dataToSign);
	}

	private static String commaSeparate(final ArrayList<String> dataToSign)
	{
		final StringBuilder csv = new StringBuilder();
		for (final Iterator<String> it = dataToSign.iterator(); it.hasNext();)
		{
			csv.append(it.next());
			if (it.hasNext())
			{
				csv.append(",");
			}
		}
		return csv.toString();
	}
}
