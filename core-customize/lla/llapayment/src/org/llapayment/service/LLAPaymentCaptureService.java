package org.llapayment.service;
import com.lla.mulesoft.integration.exception.LLAApiException;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.request.CaptureRequest;
import de.hybris.platform.payment.commands.result.CaptureResult;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.util.Currency;
import java.util.Date;
import java.util.Iterator;


public interface LLAPaymentCaptureService{

    boolean capturePayment(final OrderModel orderModel) throws LLAApiException;
}
