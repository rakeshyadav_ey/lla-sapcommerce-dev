package org.llapayment.service.impl;

import com.lla.mulesoft.integration.exception.LLAApiException;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.payment.commands.request.CaptureRequest;
import de.hybris.platform.payment.commands.result.CaptureResult;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.log4j.Logger;
import org.llapayment.service.LLAPaymentCaptureService;
import de.hybris.platform.core.model.order.OrderModel;
import org.llapayment.service.LlapaymentService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Currency;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class LLAPaymentCaptureServiceImpl implements LLAPaymentCaptureService {

    private static final Logger LOG = Logger.getLogger(LLAPaymentCaptureServiceImpl.class);
   @Autowired
    private LlapaymentService paymentService;
    @Autowired
    private ModelService modelService;
    public LlapaymentService getPaymentService() {
        return paymentService;
    }

    @Override
    public boolean capturePayment(final OrderModel order) throws LLAApiException {

        for (final PaymentTransactionModel txn : order.getPaymentTransactions())
        {
            if (txn.getInfo() instanceof CreditCardPaymentInfoModel)
            {
                final PaymentTransactionEntryModel txnEntry = getPaymentService().capture(txn);

                if (null != txnEntry && TransactionStatus.ACCEPTED.name().equals(txnEntry.getTransactionStatus()))
                {
                    if (LOG.isDebugEnabled())
                    {
                        LOG.debug("The payment transaction has been captured. Order: " + order.getCode() + ". Txn: " + txn.getCode());
                    }
                    setOrderStatus(order, OrderStatus.PAYMENT_CAPTURED);
                    return Boolean.TRUE;
                }
                else
                {
                    LOG.error("The payment transaction capture has failed. Order: " + order.getCode() + ". Txn: " + txn.getCode());
                    setOrderStatus(order, OrderStatus.PAYMENT_NOT_CAPTURED);
                    return Boolean.FALSE;
                }
            }
        }

      return true;
    }

    private void setOrderStatus(OrderModel order, OrderStatus orderStatus) {
        order.setStatus(orderStatus);
        modelService.save(order);
    }


}
