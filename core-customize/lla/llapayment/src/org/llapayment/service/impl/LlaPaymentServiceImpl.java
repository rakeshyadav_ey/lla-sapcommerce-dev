/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.llapayment.service.impl;

import com.firstatlanticcommerce.schemas.gateway.TransactionModificationResponse;
import com.lla.llapayment.LLAPaymentForm;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.acceleratorservices.model.payment.CCPaySubValidationModel;
import de.hybris.platform.acceleratorservices.payment.data.*;
import de.hybris.platform.acceleratorservices.payment.enums.DecisionsEnum;
import de.hybris.platform.acceleratorservices.payment.impl.DefaultAcceleratorPaymentService;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CashOnDeliveryPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.OnlinePaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PayInStorePaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.BillingInfo;
import de.hybris.platform.payment.dto.CardInfo;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.methods.CardPaymentService;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang.StringUtils;
import org.llapayment.client.LLAPaymentClient;
import org.llapayment.service.LlapaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.*;
import java.util.Currency;
import java.util.Date;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class LlaPaymentServiceImpl extends DefaultAcceleratorPaymentService implements LlapaymentService
{
	private static final Logger LOG = LoggerFactory.getLogger(LlaPaymentServiceImpl.class);

	private static final String SUBSCRIPTION_INFO_DATA_CANNOT_BE_NULL_MSG = "SubscriptionInfoData cannot be null";
	private static final String SIGNATURE_DATA_CANNOT_BE_NULL_MSG = "SignatureData cannot be null";
	private static final String PAYMENT_INFO_DATA_CANNOT_BE_NULL_MSG = "PaymentInfoData cannot be null";
	private static final String ORDER_INFO_DATA_CANNOT_BE_NULL_MSG = "OrderInfoData cannot be null";
	private static final String CUSTOMER_INFO_DATA_CANNOT_BE_NULL_MSG = "CustomerInfoData cannot be null";
	private static final String DECISION_CANNOT_BE_NULL_MSG = "Decision cannot be null";
	private static final String CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG = "CreateSubscriptionResult cannot be null";

	@Autowired
	private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

	@Autowired
	private LLAPaymentClient llaPaymentClient;

	@Autowired
	private CardPaymentService cardPaymentService;

	@Autowired
	private CartFacade cartFacade;

	@Autowired
	private OrderFacade orderFacade;

	@Autowired
	private CartService cartService;

	@Autowired
	private ModelService modelService;

	private Converter<OrderModel, OrderData> orderConverter;

	@Override
	public PaymentSubscriptionResultItem completeSopCreatePaymentSubscription(final CustomerModel customerModel, final boolean saveInAccount, final Map<String, String> parameters,final String cvvResult)
	{
		final PaymentSubscriptionResultItem paymentSubscriptionResult = new PaymentSubscriptionResultItem();
		final Map<String, PaymentErrorField> errors = new HashMap<String, PaymentErrorField>();
		paymentSubscriptionResult.setErrors(errors);

		final CreateSubscriptionResult response = getPaymentResponseInterpretation().interpretResponse(parameters,
				getClientReferenceLookupStrategy().lookupClientReferenceId(), errors);

		validateParameterNotNull(response, CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG);
		validateParameterNotNull(response.getDecision(), DECISION_CANNOT_BE_NULL_MSG);

		if (!getCreateSubscriptionResultValidationStrategy().validateCreateSubscriptionResult(errors, response).isEmpty())
		{
			return paymentSubscriptionResult;
		}

		paymentSubscriptionResult.setSuccess(DecisionsEnum.ACCEPT.name().equalsIgnoreCase(response.getDecision()) || (DecisionsEnum.REJECT.name().equalsIgnoreCase(response.getDecision()) && null != response.getReasonCode() && response.getReasonCode().equals(85) && cvvResult.equals("M")));
		paymentSubscriptionResult.setDecision(String.valueOf(response.getDecision()));
		paymentSubscriptionResult.setResultCode(String.valueOf(response.getReasonCode()));

		if (DecisionsEnum.ACCEPT.name().equalsIgnoreCase(response.getDecision()) || (DecisionsEnum.REJECT.name().equalsIgnoreCase(response.getDecision()) && null != response.getReasonCode() && response.getReasonCode().equals(85) && cvvResult.equals("M")))
		{
			// in case of ACCEPT we should have all these fields filled out
			Assert.notNull(response.getCustomerInfoData(), CUSTOMER_INFO_DATA_CANNOT_BE_NULL_MSG);
			Assert.notNull(response.getOrderInfoData(), ORDER_INFO_DATA_CANNOT_BE_NULL_MSG);
			Assert.notNull(response.getPaymentInfoData(), PAYMENT_INFO_DATA_CANNOT_BE_NULL_MSG);
			Assert.notNull(response.getSignatureData(), SIGNATURE_DATA_CANNOT_BE_NULL_MSG);
			Assert.notNull(response.getSubscriptionInfoData(), SUBSCRIPTION_INFO_DATA_CANNOT_BE_NULL_MSG);
			getPaymentTransactionStrategy().savePaymentTransactionEntry(customerModel, response.getRequestId(),
					response.getOrderInfoData());
			final CreditCardPaymentInfoModel cardPaymentInfoModel = getCreditCardPaymentInfoCreateStrategy().saveSubscription(
					customerModel, response.getCustomerInfoData(), response.getSubscriptionInfoData(),
					response.getPaymentInfoData(), saveInAccount);
			paymentSubscriptionResult.setStoredCard(cardPaymentInfoModel);

			// Check if the subscription has already been validated
			final CCPaySubValidationModel subscriptionValidation = getCreditCardPaymentSubscriptionDao()
					.findSubscriptionValidationBySubscription(cardPaymentInfoModel.getSubscriptionId());
			if (subscriptionValidation != null)
			{
				cardPaymentInfoModel.setSubscriptionValidated(true);
				getModelService().save(cardPaymentInfoModel);
				getModelService().remove(subscriptionValidation);
			}
		}
		else
		{
			LOG.error("Cannot create subscription. Decision: " + response.getDecision() + " - Reason Code: "
					+ response.getReasonCode());
		}
		return paymentSubscriptionResult;
	}

	@Override
	public void setPaymentMode(String paymentMethod) {
		final CartModel cartModel = cartService.getSessionCart();
		final String code=cartModel.getUser().getUid() + "_" + UUID.randomUUID();
		switch(paymentMethod){
			case "COD":
				CashOnDeliveryPaymentInfoModel cashOnDeliveryPaymentInfoModel=modelService.create(CashOnDeliveryPaymentInfoModel.class);
				cashOnDeliveryPaymentInfoModel.setUser(cartModel.getUser());
				cashOnDeliveryPaymentInfoModel.setCode(code);
				modelService.save(cashOnDeliveryPaymentInfoModel);
				cartModel.setPaymentInfo(cashOnDeliveryPaymentInfoModel);
				break;
			case "PayInStore":
				PayInStorePaymentInfoModel payInStorePaymentInfoModel=modelService.create(PayInStorePaymentInfoModel.class);
				payInStorePaymentInfoModel.setUser(cartModel.getUser());
				payInStorePaymentInfoModel.setCode(code);
				modelService.save(payInStorePaymentInfoModel);
				cartModel.setPaymentInfo(payInStorePaymentInfoModel);
				break;
			case "Online":
				OnlinePaymentInfoModel onlinePaymentInfoModel=modelService.create(OnlinePaymentInfoModel.class);
				onlinePaymentInfoModel.setUser(cartModel.getUser());
				onlinePaymentInfoModel.setCode(code);
				modelService.save(onlinePaymentInfoModel);
				cartModel.setPaymentInfo(onlinePaymentInfoModel);
				break;
		}

		modelService.save(cartModel);
		modelService.refresh(cartModel);
	}

	@Override
	public PaymentTransactionEntryModel capture(PaymentTransactionModel transaction) throws LLAApiException {
		PaymentTransactionEntryModel auth = null;

		for (PaymentTransactionEntryModel pte : transaction.getEntries()) {
			if (pte.getType().equals(PaymentTransactionType.AUTHORIZATION)) {
				auth = pte;
				break;
			}
		}

		if (auth == null) {
			throw new AdapterException("Could not capture without authorization");
		} else {
			PaymentTransactionType transactionType = PaymentTransactionType.CAPTURE;
			String newEntryCode = this.getNewPaymentTransactionEntryCode(transaction, transactionType);
			OrderData order;
			if(null != transaction.getOrder() && llaMulesoftIntegrationUtil.isCableticaOrder(transaction.getOrder().getSite())){
				order=getOrderConverter().convert((OrderModel) transaction.getOrder());
			}else {
				order = orderFacade.getOrderDetailsForCode(transaction.getOrder().getCode());
			}
			TransactionModificationResponse result = llaPaymentClient.sendCaptureRequest(order);
			if(null != result && null != result.getTransactionModificationResult() && null != result.getTransactionModificationResult().getValue()
					&& null != result.getTransactionModificationResult().getValue().getResponseCode()
					&& result.getTransactionModificationResult().getValue().getResponseCode().getValue().equals("3")){
				LOG.error(String.format("Error in payment capture due to Reason Code : %s ", result.getTransactionModificationResult().getValue().getReasonCode() ));
				LOG.error(String.format("Error in payment capture due to  Response Code : %s ", result.getTransactionModificationResult().getValue().getResponseCode() ));
				LOG.error(String.format("Error in payment capture due to  Response Description : %s ", result.getTransactionModificationResult().getValue().getReasonCodeDescription() ));
				return null;
			}
			PaymentTransactionEntryModel entry = (PaymentTransactionEntryModel)this.getModelService().create(PaymentTransactionEntryModel.class);
			entry.setAmount(auth.getAmount());
			if (transaction.getOrder().getCurrency() != null) {
				entry.setCurrency(this.getCommonI18NService().getCurrency(transaction.getOrder().getCurrency().getIsocode()));
			}

			entry.setType(transactionType);
			entry.setRequestId(auth.getRequestId());
			entry.setSubscriptionID(auth.getSubscriptionID());
			entry.setRequestToken(auth.getRequestToken());
			entry.setTime(auth.getTime() == null ? new Date() :auth.getTime());
			entry.setPaymentTransaction(transaction);
			entry.setTransactionStatus(null!=auth.getTransactionStatus()?auth.getTransactionStatus().toString(): StringUtils.EMPTY);
			entry.setTransactionStatusDetails(null!=auth.getTransactionStatusDetails()?auth.getTransactionStatusDetails().toString(): StringUtils.EMPTY);
			entry.setCode(newEntryCode);
			this.getModelService().save(entry);
			return entry;
		}
	}

	private String getNewPaymentTransactionEntryCode(PaymentTransactionModel transaction, PaymentTransactionType paymentTransactionType) {
		return transaction.getEntries() == null ? transaction.getCode() + "-" + paymentTransactionType.getCode() + "-1" : transaction.getCode() + "-" + paymentTransactionType.getCode() + "-" + (transaction.getEntries().size() + 1);
	}

	public PaymentTransactionEntryModel authorize(String merchantTransactionCode, BigDecimal amount, Currency currency, AddressModel deliveryAddress, String subscriptionID, String cv2, String paymentProvider,final LLAPaymentForm formData) {
		BillingInfo shippingInfo = this.createBillingInfo(deliveryAddress);
		PaymentTransactionModel transaction = (PaymentTransactionModel)this.getModelService().create(PaymentTransactionModel.class);
		transaction.setCode(merchantTransactionCode);
		transaction.setPlannedAmount(amount);
		return this.authorizeInternal(transaction, amount, currency, shippingInfo, (CardInfo)null, subscriptionID, cv2, paymentProvider,formData);
	}

	protected PaymentTransactionEntryModel authorizeInternal(PaymentTransactionModel transaction, BigDecimal amount, Currency currency, BillingInfo shippingInfo, CardInfo card, String subscriptionID, String cv2, String paymentProvider,final LLAPaymentForm formData) {
		PaymentTransactionType paymentTransactionType = PaymentTransactionType.AUTHORIZATION;
		CartData cartData = cartFacade.getSessionCart();
		if(formData.getResponseCode().equals("1")){
			String newEntryCode = this.getNewPaymentTransactionEntryCode(transaction, paymentTransactionType);
			transaction.setRequestId(formData.getAuthCode());
			transaction.setRequestToken(formData.getReferenceNo());
			transaction.setPaymentProvider(paymentProvider);
			transaction.setCAVVValue(formData.getCAVVValue());
			transaction.setTransactionStain(formData.getTransactionStain());
			this.getModelService().save(transaction);
			PaymentTransactionEntryModel entry = (PaymentTransactionEntryModel)this.getModelService().create(PaymentTransactionEntryModel.class);
			entry.setAmount(transaction.getPlannedAmount());
			entry.setCurrency(this.getCommonI18NService().getCurrentCurrency());
			entry.setType(paymentTransactionType);
			entry.setTime(new Date());
			entry.setPaymentTransaction(transaction);
			entry.setRequestId(formData.getAuthCode());
			entry.setRequestToken(formData.getReferenceNo());
			switch (formData.getResponseCode())
			{
				case "1" : entry.setTransactionStatus("ACCEPTED");
					break;
				case "2" : entry.setTransactionStatus("REJECTED");
					break;
				case "3" : entry.setTransactionStatus("ERROR");
					break;
			}
			entry.setTransactionStatusDetails(formData.getReasonCodeDesc());
			entry.setCode(newEntryCode);
			if (subscriptionID != null) {
				entry.setSubscriptionID(subscriptionID);
			}
			this.getModelService().save(entry);
			this.getModelService().refresh(transaction);
			return entry;}
		else
			return null;
	}


	protected BillingInfo createBillingInfo(AddressModel address) {
		if (address == null) {
			return null;
		} else {
			BillingInfo billingInfo = new BillingInfo();
			billingInfo.setCity(address.getTown());
			if (address.getCountry() != null) {
				billingInfo.setCountry(address.getCountry().getIsocode());
			}

			billingInfo.setEmail(address.getEmail());
			billingInfo.setFirstName(address.getFirstname());
			billingInfo.setLastName(address.getLastname());
			billingInfo.setPhoneNumber(address.getPhone1());
			billingInfo.setPostalCode(address.getPostalcode());
			if (address.getRegion() != null) {
				billingInfo.setState(address.getRegion().getName());
			}

			billingInfo.setStreet1(address.getStreetname());
			billingInfo.setStreet2(address.getStreetnumber());
			return billingInfo;
		}
	}


	public Converter<OrderModel, OrderData> getOrderConverter() {
		return orderConverter;
	}

	public void setOrderConverter(Converter<OrderModel, OrderData> orderConverter) {
		this.orderConverter = orderConverter;
	}

}
