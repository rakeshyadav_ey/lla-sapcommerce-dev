package org.llapayment.service.impl;

import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import org.llapayment.strategies.impl.LLACommercePaymentAuthorizationStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import com.lla.llapayment.LLAPaymentForm;

import java.util.Map;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class LlaCheckoutServiceImpl extends DefaultCommerceCheckoutService {

    @Autowired
    private LLACommercePaymentAuthorizationStrategy llaCommercePaymentAuthorizationStrategy;

    public PaymentTransactionEntryModel authorizePayment(final CommerceCheckoutParameter parameter, final LLAPaymentForm formData)
    {
        final CartModel cartModel = parameter.getCart();
        validateParameterNotNull(cartModel, "Cart model cannot be null");
        validateParameterNotNull(cartModel.getPaymentInfo(), "Payment information on cart cannot be null");

        // if the authorization amount is not passed in figure it out from the cart.
        if (parameter.getAuthorizationAmount() == null)
        {
            parameter.setAuthorizationAmount(calculateAuthAmount(cartModel));
        }
        return llaCommercePaymentAuthorizationStrategy.authorizePaymentAmount(parameter,formData);
    }


}
