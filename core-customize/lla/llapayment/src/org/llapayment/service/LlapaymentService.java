/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.llapayment.service;

import com.lla.mulesoft.integration.exception.LLAApiException;
import de.hybris.platform.acceleratorservices.payment.data.PaymentSubscriptionResultItem;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.util.Map;

public interface LlapaymentService
{
    PaymentTransactionEntryModel capture(PaymentTransactionModel transaction) throws LLAApiException;

    public PaymentSubscriptionResultItem completeSopCreatePaymentSubscription(final CustomerModel customerModel, final boolean saveInAccount, final Map<String, String> parameters, final String cvvResult);

    public void setPaymentMode(final String paymentMethod);
}
