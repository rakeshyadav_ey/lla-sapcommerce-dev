package org.llapayment.strategies;

import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.acceleratorservices.payment.strategies.CreditCardPaymentInfoCreateStrategy;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

public interface LLACCPaymentInfoStrategy extends CreditCardPaymentInfoCreateStrategy {

    CreditCardPaymentInfoModel llaCreateCreditCardPaymentInfo(final PaymentInfoData paymentInfo, final AddressModel billingAddress, final CustomerModel customerModel,
                                                                     final boolean saveInAccount);
}
