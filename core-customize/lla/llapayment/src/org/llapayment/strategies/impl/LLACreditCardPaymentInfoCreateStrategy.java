package org.llapayment.strategies.impl;

import de.hybris.platform.acceleratorservices.payment.cybersource.enums.CardTypeEnum;
import de.hybris.platform.acceleratorservices.payment.cybersource.strategies.impl.DefaultCreditCardPaymentInfoCreateStrategy;
import de.hybris.platform.acceleratorservices.payment.data.CustomerInfoData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.acceleratorservices.payment.data.SubscriptionInfoData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import org.apache.commons.lang.StringUtils;
import org.llapayment.strategies.LLACCPaymentInfoStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class LLACreditCardPaymentInfoCreateStrategy extends DefaultCreditCardPaymentInfoCreateStrategy implements  LLACCPaymentInfoStrategy{

    @Override
    public CreditCardPaymentInfoModel saveSubscription(final CustomerModel customerModel, final CustomerInfoData customerInfoData,
                                                       final SubscriptionInfoData subscriptionInfo, final PaymentInfoData paymentInfoData, final boolean saveInAccount)
    {
        CreditCardPaymentInfoModel cardPaymentInfoModel=super.saveSubscription(customerModel,customerInfoData,subscriptionInfo,paymentInfoData,saveInAccount);
        final AddressModel billingAddress=cardPaymentInfoModel.getBillingAddress();
        billingAddress.setArea(customerInfoData.getArea());
        cardPaymentInfoModel.setBillingAddress(billingAddress);
        getModelService().saveAll(billingAddress,cardPaymentInfoModel);
        return cardPaymentInfoModel;
  }

    @Override
    public CreditCardPaymentInfoModel llaCreateCreditCardPaymentInfo(final PaymentInfoData paymentInfo, final AddressModel billingAddress, final CustomerModel customerModel,
                                                                  final boolean saveInAccount)
    {
    //    validateParameterNotNull(subscriptionInfo, "subscriptionInfo cannot be null");
        validateParameterNotNull(paymentInfo, "paymentInfo cannot be null");
        validateParameterNotNull(billingAddress, "billingAddress cannot be null");
        validateParameterNotNull(customerModel, "customerModel cannot be null");

        final CreditCardPaymentInfoModel cardPaymentInfoModel = getModelService().create(CreditCardPaymentInfoModel.class);
        cardPaymentInfoModel.setBillingAddress(billingAddress);
        cardPaymentInfoModel.setCode(customerModel.getUid() + "_" + UUID.randomUUID());
        cardPaymentInfoModel.setUser(customerModel);
        //  cardPaymentInfoModel.setSubscriptionId(subscriptionInfo.getSubscriptionID());

        cardPaymentInfoModel.setNumber(paymentInfo.getCardAccountNumber());

        CardTypeEnum cardTypeEnum = CardTypeEnum.get(paymentInfo.getCardCardType());
        if (cardTypeEnum != null)
        {
            cardPaymentInfoModel.setType(CreditCardType.valueOf(cardTypeEnum.name().toUpperCase()));
        }
        else
        {
            cardPaymentInfoModel.setType(CreditCardType.valueOf(paymentInfo.getCardCardType().toUpperCase()));
        }

        cardPaymentInfoModel.setCcOwner(getCCOwner(paymentInfo, billingAddress));
        cardPaymentInfoModel.setValidFromMonth(paymentInfo.getCardStartMonth());
        cardPaymentInfoModel.setValidFromYear(paymentInfo.getCardStartYear());
        if (paymentInfo.getCardExpirationMonth() != null && paymentInfo.getCardExpirationMonth().intValue() > 0)
        {
            cardPaymentInfoModel.setValidToMonth(String.valueOf(paymentInfo.getCardExpirationMonth()));
        }
        if (paymentInfo.getCardExpirationYear() != null && paymentInfo.getCardExpirationYear().intValue() > 0)
        {
            cardPaymentInfoModel.setValidToYear(String.valueOf(paymentInfo.getCardExpirationYear()));
        }

       // cardPaymentInfoModel.setSubscriptionId(subscriptionInfo.getSubscriptionID());
        cardPaymentInfoModel.setSaved(saveInAccount);
        if (StringUtils.isNotBlank(paymentInfo.getCardIssueNumber()))
        {
            cardPaymentInfoModel.setIssueNumber(Integer.valueOf(paymentInfo.getCardIssueNumber()));
        }

        return cardPaymentInfoModel;
    }


}
