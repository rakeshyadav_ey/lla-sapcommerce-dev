package org.llapayment.strategies.impl;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.llapayment.data.LLACreateSubscriptionRequest;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;

public class LLACreateSubscriptionRequestStrategy {


    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Autowired
    private CartService cartService;

    @Autowired
    private Converter<CreditCardPaymentInfoModel, PaymentInfoData> paymentInfoDataConverter;

    @Autowired
    private SiteConfigService siteConfigService;



    private static final String PAYMENT_METHOD = "payment-method";
    private static final String PAYMENT_METHOD2 = "payment_method";
    private static final String CART_DATA_ATTR = "cartData";
    private static final String CREATE_PAYMENT_TOKEN = "create_payment_token";
    private static final String CARD_TYPE = "card_type";
    private static final String SIGNATURE2 = "signature";
    private static final String DEVICE_FINGERPRINT_ID = "device_fingerprint_id";
    private static final String BILL_TO_EMAIL = "bill_to_email";
    private static final String ISO_CODE = "MX";
    private static final String REQ_DEVICE_FINGERPRINT_ID = "req_device_fingerprint_id";
    private static final String ACCEPT = "Accept";
    public static final String REDIRECT_PREFIX = "redirect:";
    public static final String VOUCHER_FORM = "voucherForm";

    private static final String SIGNED_FIELD_NAMES = "signed_field_names";
    private static final String FINANCING_SIGNED_FIELD_NAMES_VALUES = "recurring_frequency,recurring_amount,recurring_number_of_installments";

    private static final String SIGNED_FIELD_NAMES_VALUE = "access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,"
            + "signed_date_time,locale,transaction_type,reference_number,currency,bill_to_email,override_custom_receipt_page";

    private static final String ACCESS_KEY2 = "access_key";
    private static final String CARD = "card";
    private static final String UNSIGNED_FIELD_NAMES = "unsigned_field_names";
    final static String UNSIGNED_FIELD_NAMES_VALUE = "card_type,card_number,card_expiry_date,payment_method,bill_to_forename,"
            + "bill_to_surname,bill_to_address_line1,bill_to_address_line2,bill_to_address_city,bill_to_address_state,bill_to_address_country,"
            + "bill_to_address_postal_code,merchant_defined_data10,merchant_defined_data11";
    private static final String MERCHANT_DEFINED_DATA_10 = "merchant_defined_data10";
    private static final String MERCHANT_DEFINED_DATA_11 = "merchant_defined_data11";

    public LLACreateSubscriptionRequest createSubscriptionRequest(final String siteName, final String requestUrl,
                                                                  final String responseUrl, final String merchantCallbackUrl, final CustomerModel customerModel,
                                                                  final CreditCardPaymentInfoModel cardInfo, final AddressModel paymentAddress)
    {
        final CartModel cartModel = cartService.getSessionCart();
        if (cartModel == null)
        {
            return null;
        }
        final LLACreateSubscriptionRequest request = new LLACreateSubscriptionRequest();
        //Common Data
//        final String signature = SignatureUtil.sign(params, getSecretKey());
        request.setCardNumber(paymentInfoDataConverter.convert(cardInfo).getCardAccountNumber());
        request.setCustomerReference("");
        request.setExpiryDate((paymentInfoDataConverter.convert(cardInfo).getCardExpirationMonth()+paymentInfoDataConverter.convert(cardInfo).getCardExpirationYear().toString()));
        request.setMerchantNumber(cartModel.getCode());
//        request.setSignature(signature);
        return request;
    }

    /*private Map<String, Object> fillHiddenFields(final CartModel cartModel)
    {
        final Map<String, Object> params = new HashMap<String, Object>();
        String signed_fields = SIGNED_FIELD_NAMES_VALUE;
        signed_fields += "," + DEVICE_FINGERPRINT_ID;
        signed_fields += "," + FINANCING_SIGNED_FIELD_NAMES_VALUES;
        params.put(PROFILE_ID, getProfileId());
        params.put(SIGNED_DATE_TIME, CSUtil.getUTCDateTime());
        params.put(TRANSACTION_TYPE, CREATE_PAYMENT_TOKEN);
        params.put(LOCALE, getSorianaLocale());
        params.put(TRANSACTION_UUID, UUID.randomUUID().toString());
        params.put(REFERENCE_NUMBER, cartModel.getCode());
        params.put(SIGNED_FIELD_NAMES, signed_fields);
        params.put(DEVICE_FINGERPRINT_ID, cartModel.getCode());
        params.put(ACCESS_KEY2, getAccessKey());
        params.put(PAYMENT_METHOD2, CARD);
        params.put(UNSIGNED_FIELD_NAMES, UNSIGNED_FIELD_NAMES_VALUE);
        params.put(MERCHANT_DEFINED_DATA_10, "false");
        params.put(MERCHANT_DEFINED_DATA_11, "false");
        final String signature = SignatureUtil.sign(params, getSecretKey());
        params.put(SIGNATURE2, signature);

        return params;
    }

    *//**
     * @return secret key
     *//*
    private String getSecretKey()
    {
        return configurationService.getConfiguration().getString("lla.fac.checkout.profile.secret_key");
    }

    *//**
     * @return access key
     *//*
    private String getAccessKey()
    {
        return configurationService.getConfiguration().getString("lla.fac.checkout.profile.access_key");
    }*/

}
