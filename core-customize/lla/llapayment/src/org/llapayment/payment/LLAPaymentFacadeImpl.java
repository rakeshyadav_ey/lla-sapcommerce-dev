package org.llapayment.payment;

import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorfacades.payment.impl.DefaultPaymentFacade;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentSubscriptionResultItem;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.CustomerEmailResolutionService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import org.apache.commons.lang.StringUtils;
import org.llapayment.service.impl.LlaPaymentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.util.Map;

public class LLAPaymentFacadeImpl extends DefaultPaymentFacade implements LLAPaymentFacade{

        @Resource
        private CommonI18NService commonI18NService;

        @Resource
        private CartService cartService;

        @Resource
        private CustomerEmailResolutionService customerEmailResolutionService;

        @Autowired
        private LlaPaymentServiceImpl llapaymentService;

        @Autowired
        private CustomerAccountService customerAccountService;

    @Override
    public PaymentSubscriptionResultData completeSopCreateSubscription(final Map<String, String> parameters,
                                                                       final boolean saveInAccount, final boolean defaultPayment, final String cvvResult)
    {
        final CustomerModel customerModel = getCurrentUserForCheckout();
        final PaymentSubscriptionResultItem paymentSubscriptionResultItem = llapaymentService
                .completeSopCreatePaymentSubscription(customerModel, saveInAccount, parameters,cvvResult);

        if (paymentSubscriptionResultItem != null)
        {
            if (defaultPayment)
            {
                customerAccountService.setDefaultPaymentInfo(customerModel, paymentSubscriptionResultItem.getStoredCard());
            }
            return getPaymentSubscriptionResultDataConverter().convert(paymentSubscriptionResultItem);
        }

        return null;
    }
}
