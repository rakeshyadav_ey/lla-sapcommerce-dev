package org.llapayment.payment;

import de.hybris.platform.acceleratorfacades.payment.PaymentFacade;
import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;

import java.util.Map;

public interface LLAPaymentFacade extends PaymentFacade {

    PaymentSubscriptionResultData completeSopCreateSubscription(final Map<String, String> parameters,
                                                                final boolean saveInAccount, final boolean defaultPayment, final String cvvResult);

}
