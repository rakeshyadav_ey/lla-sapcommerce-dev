package org.llapayment.payment;

import de.hybris.platform.commercefacades.order.impl.DefaultCheckoutFacade;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import org.apache.commons.lang.StringUtils;
import org.llapayment.service.impl.LlaCheckoutServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import com.lla.llapayment.LLAPaymentForm;

import java.util.Map;

public class LLaPaymentCheckoutFacadeImpl extends DefaultCheckoutFacade {

    @Autowired
    private LlaCheckoutServiceImpl llaCheckoutService;

    @Autowired
    private CartService cartService;

    public boolean authorizePayment(final String securityCode,final LLAPaymentForm formData)
    {
        CartModel cartModel = cartService.getSessionCart();
        final CreditCardPaymentInfoModel creditCardPaymentInfoModel = cartModel == null ? null
                : (CreditCardPaymentInfoModel) cartModel.getPaymentInfo();
        if (this.getCurrentUserForCheckout().equals(cartModel.getUser()) && creditCardPaymentInfoModel != null
                && StringUtils.isNotBlank(creditCardPaymentInfoModel.getSubscriptionId()))
        {
            final CommerceCheckoutParameter parameter = createCommerceCheckoutParameter(cartModel, true);
            parameter.setSecurityCode(securityCode);
            parameter.setPaymentProvider(getPaymentProvider());
            final PaymentTransactionEntryModel paymentTransactionEntryModel = llaCheckoutService.authorizePayment(parameter,formData);

            return paymentTransactionEntryModel != null
                    && (TransactionStatus.ACCEPTED.name().equals(paymentTransactionEntryModel.getTransactionStatus())
                    || TransactionStatus.REVIEW.name().equals(paymentTransactionEntryModel.getTransactionStatus()));
        }
        return false;
    }

    protected CustomerModel getCurrentUserForCheckout()
    {
        return getCheckoutCustomerStrategy().getCurrentUserForCheckout();
    }
}
