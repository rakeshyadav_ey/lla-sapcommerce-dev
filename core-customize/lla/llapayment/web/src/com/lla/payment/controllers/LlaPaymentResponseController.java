/*
package com.lla.payment.controllers;

import com.llapayment.data.LLAPaymentData;
import de.hybris.platform.acceleratorservices.web.payment.forms.SopPaymentDetailsForm;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import org.llapayment.client.LLAPaymentClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;


@Controller
@RequestMapping("/sop-response")
public class LlaPaymentResponseController {

    @Autowired
    LLAPaymentClient llaPaymentClient;

    @Autowired
    CheckoutFacade checkoutFacade;

    @RequestMapping(value = "/process", method = RequestMethod.POST)
    public String doValidateAndPost(@Valid final SopPaymentDetailsForm form, final BindingResult bindingResult,
                                    final HttpServletRequest request, final Model model)
    {
        LLAPaymentData paymentData = new LLAPaymentData();
        final CartData cartData = checkoutFacade.getCheckoutCart();
        populatePaymentFormData(form,paymentData);
        llaPaymentClient.createAuthRequest(paymentData,cartData);
    }

    private void  populatePaymentFormData(SopPaymentDetailsForm form, LLAPaymentData paymentData) {
        paymentData.setBillTo_firstName(form.getBillTo_firstName());
        paymentData.setBillTo_email(form.getBillTo_email());
        paymentData.setBillTo_country(form.getBillTo_country());
        paymentData.setBillTo_lastName(form.getBillTo_lastName());
        paymentData.setBillTo_city(form.getBillTo_city());
        paymentData.setBillTo_postalCode(form.getBillTo_postalCode());
        paymentData.setBillTo_street1(form.getBillTo_street1());
        paymentData.setBillTo_street2(form.getBillTo_street2());
        paymentData.setCard_accountNumber(form.getCard_accountNumber());
        paymentData.setCard_cvNumber(form.getCard_cvNumber());
        paymentData.setShipTo_city(form.getShipTo_city());
        paymentData.setShipTo_country(form.getShipTo_country());
        paymentData.setShipTo_firstName(form.getShipTo_firstName());
        paymentData.setShipTo_phoneNumber(form.getShipTo_phoneNumber());
        paymentData.setShipTo_lastName(form.getShipTo_lastName());
        paymentData.setCard_expirationMonth(form.getCard_expirationMonth());
        paymentData.setCard_expirationYear(form.getCard_expirationYear());

    }
}
*/
