///*
// * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
// */
//package org.llapayment.controller;
//
//import static org.llapayment.constants.LlapaymentConstants.PLATFORM_LOGO_CODE;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.ModelMap;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//
//import org.llapayment.service.LlapaymentService;
//
//
//@Controller
//public class LlapaymentHelloController
//{
//	@Autowired
//	private LlapaymentService llapaymentService;
//
//	@RequestMapping(value = "/", method = RequestMethod.GET)
//	public String printWelcome(final ModelMap model)
//	{
//		model.addAttribute("logoUrl", llapaymentService.getHybrisLogoUrl(PLATFORM_LOGO_CODE));
//		return "welcome";
//	}
//}
