/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.channelref;

import com.lla.telcotmfwebservices.v2.dto.ChannelRef;
import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.europe1.enums.PriceRowChannel;
import de.hybris.platform.util.Config;
import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for referred type attribute between {@link PriceRowChannel} and {@link ChannelRef}
 *
 * @since 1903
 */
public class ChannelRefReferredTypeAttributeMapper extends TmaAttributeMapper<PriceRowChannel, ChannelRef>
{

	@Override
	public void populateTargetAttributeFromSource(PriceRowChannel source, ChannelRef target, MappingContext context)
	{
		target.setAtreferredType(Config.getParameter(LlatelcotmfwebservicesConstants.TMA_CHANNEL_DEFAULT_REFERRED));
	}
}
