/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.productofferingprice;

import com.lla.telcotmfwebservices.v2.dto.TimePeriod;
import com.lla.telcotmfwebservices.v2.dto.ProductOfferingPrice;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.subscriptionfacades.data.SubscriptionPricePlanData;
import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for validFor attribute between {@link SubscriptionPricePlanData} and
 * {@link ProductOfferingPrice}
 *
 * @since 1903
 */
public class PoPriceValidForAttributeMapper extends TmaAttributeMapper<SubscriptionPricePlanData, ProductOfferingPrice>
{
	@Override
	public void populateTargetAttributeFromSource(final SubscriptionPricePlanData source, final ProductOfferingPrice target,
			final MappingContext context)
	{
		final TimePeriod timePeriod = new TimePeriod();
		if (source.getStartTime() != null)
		{
			timePeriod.setStartDateTime(source.getStartTime());
		}

		if (source.getEndTime() != null)
		{
			timePeriod.setEndDateTime(source.getEndTime());
		}

		target.setValidFor(timePeriod);
	}
}
