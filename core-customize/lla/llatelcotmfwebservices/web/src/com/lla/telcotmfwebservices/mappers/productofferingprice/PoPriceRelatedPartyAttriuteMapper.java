/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */

package com.lla.telcotmfwebservices.mappers.productofferingprice;

import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.ProductOfferingPrice;
import com.lla.telcotmfwebservices.v2.dto.RelatedPartyRef;
import de.hybris.platform.subscriptionfacades.data.SubscriptionPricePlanData;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for related party attribute between {@link SubscriptionPricePlanData} and
 * {@link ProductOfferingPrice}
 *
 * @since 1911
 */
public class PoPriceRelatedPartyAttriuteMapper extends TmaAttributeMapper<SubscriptionPricePlanData, ProductOfferingPrice>
{
	/**
	 * Mapper facade
	 */
	private MapperFacade mapperFacade;

	public PoPriceRelatedPartyAttriuteMapper(MapperFacade mapperFacade)
	{
		this.mapperFacade = mapperFacade;
	}

	@Override
	public void populateTargetAttributeFromSource(final SubscriptionPricePlanData source, final ProductOfferingPrice target,
			final MappingContext context)
	{
		if (source.getUser() != null)
		{
			target.setRelatedParty(getMapperFacade().map(source.getUser(), RelatedPartyRef.class, context));
		}
	}

	public MapperFacade getMapperFacade()
	{
		return mapperFacade;
	}
}
