/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.productoffering;

import com.lla.telcotmfwebservices.v2.dto.Attachment;
import com.lla.telcotmfwebservices.v2.dto.ProductOffering;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.commercefacades.product.data.ProductData;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.List;


/**
 * This attribute Mapper class maps data for attachment attribute between {@link ProductData} and {@link ProductOffering}
 *
 * @since 1903
 */
public class PoAttachmentAttributeMapper extends TmaAttributeMapper<ProductData, ProductOffering>
{
	/**
	 * Mapper facade
	 */
	private MapperFacade mapperFacade;

	@Override
	public void populateTargetAttributeFromSource(final ProductData source, final ProductOffering target,
			final MappingContext context)
	{
		if (source.getImages() == null)
		{
			return;
		}

		final List<Attachment> attachments = new ArrayList<>();
		source.getImages().forEach(imageData ->
		{
			final Attachment attachmentWsDto = getMapperFacade().map(imageData, Attachment.class, context);
			attachments.add(attachmentWsDto);
		});

		target.setAttachment(attachments);
	}

	public MapperFacade getMapperFacade()
	{
		return mapperFacade;
	}

	@Required
	public void setMapperFacade(MapperFacade mapperFacade)
	{
		this.mapperFacade = mapperFacade;
	}
}
