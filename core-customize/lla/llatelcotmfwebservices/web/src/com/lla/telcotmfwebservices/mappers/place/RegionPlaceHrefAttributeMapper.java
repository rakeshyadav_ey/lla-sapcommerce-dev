/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company.  All rights reserved.
 */

package com.lla.telcotmfwebservices.mappers.place;

import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import com.lla.telcotmfwebservices.v2.dto.Place;
import de.hybris.platform.commercefacades.user.data.RegionData;

import org.apache.commons.lang.StringUtils;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for href attribute between {@link RegionData} and
 * {@link Place}
 *
 * @since 2003
 */
public class RegionPlaceHrefAttributeMapper extends TmaAttributeMapper<RegionData, Place>
{

	@Override
	public void populateTargetAttributeFromSource(final RegionData source, final Place target,
			final MappingContext context)
	{
		if (StringUtils.isNotBlank(source.getIsocode()))
		{
			target.setHref(LlatelcotmfwebservicesConstants.TMA_PLACE_REF_API_URL + source.getIsocode());
		}
	}
}
