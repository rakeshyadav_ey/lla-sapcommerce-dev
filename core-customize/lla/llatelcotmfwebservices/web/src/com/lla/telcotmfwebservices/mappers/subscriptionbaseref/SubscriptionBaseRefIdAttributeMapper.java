/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.subscriptionbaseref;

import de.hybris.platform.b2ctelcofacades.data.TmaSubscriptionBaseData;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.SubscriptionBaseRef;

import org.apache.commons.lang.StringUtils;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for id attribute between {@link TmaSubscriptionBaseData} and {@link SubscriptionBaseRef}
 *
 * @since 1907
 */
public class SubscriptionBaseRefIdAttributeMapper extends TmaAttributeMapper<TmaSubscriptionBaseData, SubscriptionBaseRef>
{
	@Override
	public void populateTargetAttributeFromSource(TmaSubscriptionBaseData source, SubscriptionBaseRef target,
			MappingContext context)
	{
		if ((StringUtils.isNotEmpty(source.getCode())))
		{
			target.setId(source.getCode());
		}
	}
}
