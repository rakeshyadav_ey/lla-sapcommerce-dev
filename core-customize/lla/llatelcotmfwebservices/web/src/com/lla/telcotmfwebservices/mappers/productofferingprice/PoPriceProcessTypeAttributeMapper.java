/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.productofferingprice;

import com.lla.telcotmfwebservices.v2.dto.ProcessType;
import com.lla.telcotmfwebservices.v2.dto.ProductOfferingPrice;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.subscriptionfacades.data.SubscriptionPricePlanData;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * This attribute Mapper class maps data for processType attribute between {@link SubscriptionPricePlanData} and
 * {@link ProductOfferingPrice}
 *
 * @since 1903
 */
public class PoPriceProcessTypeAttributeMapper extends TmaAttributeMapper<SubscriptionPricePlanData, ProductOfferingPrice>
{
	/**
	 * Mapper facade
	 */
	private MapperFacade mapperFacade;

	public PoPriceProcessTypeAttributeMapper(MapperFacade mapperFacade)
	{
		this.mapperFacade = mapperFacade;
	}

	@Override
	public void populateTargetAttributeFromSource(final SubscriptionPricePlanData source, final ProductOfferingPrice target,
			final MappingContext context)
	{
		if (CollectionUtils.isEmpty(source.getProcessTypes()))
		{
			return;
		}

		final List<ProcessType> processTypes = new ArrayList<>();
		source.getProcessTypes().forEach(processType ->
				processTypes.add(getMapperFacade().map(processType, ProcessType.class, context)));

		target.setProcessType(processTypes);
	}

	public MapperFacade getMapperFacade()
	{
		return mapperFacade;
	}

}
