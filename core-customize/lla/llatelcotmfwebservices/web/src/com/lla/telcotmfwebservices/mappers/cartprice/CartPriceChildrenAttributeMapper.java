/*
 *   Copyright (c) 2019 SAP SE or an SAP affiliate company.  All rights reserved.
 */

package com.lla.telcotmfwebservices.mappers.cartprice;


import de.hybris.platform.b2ctelcofacades.data.TmaAbstractOrderCompositePriceData;
import de.hybris.platform.b2ctelcofacades.data.TmaAbstractOrderPriceData;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.CartPrice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for cartPrice attribute between {@link TmaAbstractOrderCompositePriceData} and
 * {@link CartPrice}
 *
 * @since 1907
 */
public class CartPriceChildrenAttributeMapper
		extends TmaAttributeMapper<TmaAbstractOrderCompositePriceData, CartPrice>
{
	private MapperFacade mapperFacade;

	private Map<String, Class<CartPrice>> priceTypeDtoMap;

	@Override
	public void populateTargetAttributeFromSource(final TmaAbstractOrderCompositePriceData source,
			final CartPrice target,
			final MappingContext context)
	{
		if (source.getChildPrices() == null)
		{
			return;
		}

		final List<CartPrice> outputPrices = new ArrayList<>();
		for (TmaAbstractOrderPriceData tmaAbstractOrderPriceData : source.getChildPrices())
		{
			final CartPrice cartPrice = getMapperFacade()
					.map(tmaAbstractOrderPriceData, priceTypeDtoMap.get(tmaAbstractOrderPriceData.getClass().getSimpleName()),
							context);
			outputPrices.add(cartPrice);
		}
		target.setCartPrice(outputPrices);
	}

	public MapperFacade getMapperFacade()
	{
		return mapperFacade;
	}

	@Required
	public void setMapperFacade(MapperFacade mapperFacade)
	{
		this.mapperFacade = mapperFacade;
	}

	public Map<String, Class<CartPrice>> getPriceTypeDtoMap()
	{
		return priceTypeDtoMap;
	}

	@Required
	public void setPriceTypeDtoMap(Map<String, Class<CartPrice>> priceTypeDtoMap)
	{
		this.priceTypeDtoMap = priceTypeDtoMap;
	}
}
