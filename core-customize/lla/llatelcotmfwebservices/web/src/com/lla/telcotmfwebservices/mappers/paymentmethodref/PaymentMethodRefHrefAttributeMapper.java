/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.paymentmethodref;

import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import com.lla.telcotmfwebservices.v2.dto.PaymentMethodRef;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;

import org.apache.commons.lang.StringUtils;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for href attribute between {@link CCPaymentInfoData} and {@link PaymentMethodRef}
 *
 * @since 1911
 */
public class PaymentMethodRefHrefAttributeMapper extends TmaAttributeMapper<CCPaymentInfoData, PaymentMethodRef>
{
	@Override
	public void populateTargetAttributeFromSource(CCPaymentInfoData source, PaymentMethodRef target, MappingContext context)
	{
		if (StringUtils.isNotEmpty(source.getCode()))
		{
			target.setHref(LlatelcotmfwebservicesConstants.TMA_PAYMENT_METHOD_API_URL + source.getCode());
		}
	}
}