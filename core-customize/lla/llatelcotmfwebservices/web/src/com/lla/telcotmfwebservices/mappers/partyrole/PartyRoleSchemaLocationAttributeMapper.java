/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.partyrole;

import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.commercefacades.user.data.PrincipalData;
import com.lla.telcotmfwebservices.v2.dto.PartyRole;
import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for schema location attribute between {@link PrincipalData} and {@link PartyRole}
 *
 * @since 1907
 */
public class PartyRoleSchemaLocationAttributeMapper extends TmaAttributeMapper<PrincipalData, PartyRole>
{
	@Override
	public void populateTargetAttributeFromSource(PrincipalData source, PartyRole target, MappingContext context)
	{
		target.setAtschemaLocation(LlatelcotmfwebservicesConstants.TMA_API_SCHEMA);
	}
}
