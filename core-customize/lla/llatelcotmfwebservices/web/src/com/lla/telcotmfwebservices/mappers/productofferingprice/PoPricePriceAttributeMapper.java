/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.productofferingprice;

import com.lla.telcotmfwebservices.v2.dto.Money;
import com.lla.telcotmfwebservices.v2.dto.ProductOfferingPrice;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.subscriptionfacades.data.SubscriptionPricePlanData;
import ma.glasnost.orika.MappingContext;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;


/**
 * This attribute Mapper class maps data for price attribute between {@link SubscriptionPricePlanData} and
 * {@link ProductOfferingPrice}
 *
 * @since 1903
 */
public class PoPricePriceAttributeMapper extends TmaAttributeMapper<SubscriptionPricePlanData, ProductOfferingPrice>
{
	@Override
	public void populateTargetAttributeFromSource(final SubscriptionPricePlanData source, final ProductOfferingPrice target,
			final MappingContext context)
	{
		if (CollectionUtils.isNotEmpty(source.getOneTimeChargeEntries())
				|| CollectionUtils.isNotEmpty(source.getRecurringChargeEntries())
				|| CollectionUtils.isNotEmpty(source.getUsageCharges()))
		{
			return;
		}

		target.setIsBundle(false);

		if (StringUtils.isEmpty(source.getCurrencyIso()) && source.getValue() == null)
		{
			return;
		}

		final Money price = new Money();
		price.setValue(source.getValue().toString());
		price.setUnit(source.getCurrencyIso());
		target.setPrice(price);
	}
}
