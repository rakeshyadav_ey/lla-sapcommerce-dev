/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.category;

import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.Category;
import de.hybris.platform.commercefacades.catalog.data.CategoryHierarchyData;
import ma.glasnost.orika.MappingContext;
import org.apache.commons.lang.StringUtils;


/**
 * This attribute Mapper class maps data for href attribute between {@link CategoryHierarchyData} and {@link Category}
 *
 * @since 1903
 */
public class CategoryHrefAttributeMapper extends TmaAttributeMapper<CategoryHierarchyData, Category>
{

	@Override
	public void populateTargetAttributeFromSource(CategoryHierarchyData source, Category target, MappingContext context)
	{
		if (StringUtils.isNotEmpty(source.getId()))
		{
			target.setHref(LlatelcotmfwebservicesConstants.CATEGORY_API_URL + source.getId());
		}
	}
}
