/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company.  All rights reserved.
 */

package com.lla.telcotmfwebservices.mappers.productofferingtermref;

import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.ProductOfferingTermRef;
import de.hybris.platform.subscriptionfacades.data.SubscriptionTermData;
import de.hybris.platform.util.Config;

import org.apache.commons.lang.StringUtils;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for referred type attribute between {@link SubscriptionTermData} and {@link ProductOfferingTermRef}
 *
 * @since 1907
 */
public class TmaProductOfferingTermRefReferredTypeAttributeMapper extends TmaAttributeMapper<SubscriptionTermData,
	 ProductOfferingTermRef>
{
	 @Override
	 public void populateTargetAttributeFromSource(SubscriptionTermData source, ProductOfferingTermRef target,
		  MappingContext context)
	 {
		  if (StringUtils.isNotEmpty(source.getId()))
		  {
				target.setAtreferredType(Config.getParameter(LlatelcotmfwebservicesConstants.PRODUCT_OFFERING_TERM_DEFAULT_REFERRED));
		  }

	 }
}
