/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.billingaccountref;


import de.hybris.platform.b2ctelcofacades.data.TmaBillingAccountData;
import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.BillingAccountRef;

import org.apache.commons.lang.StringUtils;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for href attribute between {@link TmaBillingAccountData} and {@link BillingAccountRef}
 *
 * @since 1907
 */
public class BillingAccountRefHrefAttributeMapper extends TmaAttributeMapper<TmaBillingAccountData, BillingAccountRef>
{
	@Override
	public void populateTargetAttributeFromSource(TmaBillingAccountData source, BillingAccountRef target, MappingContext context)
	{
		if (StringUtils.isNotEmpty(source.getBillingAccountId()))
		{
			target.setHref(LlatelcotmfwebservicesConstants.BILLING_ACCOUNT_REF_API_URL + source.getBillingAccountId());
		}
	}
}
