/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.v2.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.hybris.platform.b2ctelcofacades.bundle.TmaCheckoutFacade;
import de.hybris.platform.b2ctelcofacades.user.TmaUserFacade;
import de.hybris.platform.b2ctelcoservices.order.exception.OrderProcessingException;
import com.lla.telcotmfwebservices.security.IsAuthorizedOrderCreatorOrAdmin;
import com.lla.telcotmfwebservices.v2.api.ProductOrderingApi;
import com.lla.telcotmfwebservices.v2.dto.ErrorRepresentation;
import com.lla.telcotmfwebservices.v2.dto.ProductOrder;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.validation.Valid;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


/**
 * Default implementation of {@link ProductOrderingApi}
 *
 * @since 1907
 */
@SuppressWarnings("unused")
@Controller
@Api(value = "productOrdering", description = "Product Ordering API", tags = { "Product Ordering" })
public class TmaProductOrderApiController extends TmaBaseController implements ProductOrderingApi
{
	@Resource(name = "userFacade")
	private TmaUserFacade userFacade;

	@Resource(name = "acceleratorCheckoutFacade")
	private TmaCheckoutFacade checkoutFacade;
	
	private final String[] DISALLOWED_FIELDS = new String[] {};

	/**
	 * An order resource is created and persisted. Creation of an order resource is performed in 2 ways: <br/> - creation of the
	 * order taking data from an existing shopping cart (provided in the request as a reference to the shopping cart resource)
	 * <br/> - creation of the order with full data provided in the request <br/>
	 *
	 * @param productOrder
	 * 		input storing data (reference to a cart resource or full order data) for creating the order resource
	 * @return resource order created in the format of a {@link ProductOrder}
	 */
	@ApiOperation(value = "Creates a 'ProductOrder' resource", nickname = "productOrderCreate", notes = "This operation creates a product order entity and triggers the place order process", response = ProductOrder.class, tags = {
			"Product Ordering", })
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Success", response = ProductOrder.class),
			@ApiResponse(code = 400, message = "Bad Request  List of supported error codes: - 20: Invalid URL parameter value - 21: Missing body - 22: Invalid body - 23: Missing body field - 24: Invalid body field - 25: Missing header - 26: Invalid header value - 27: Missing query-string parameter - 28: Invalid query-string parameter value", response = ErrorRepresentation.class),
			@ApiResponse(code = 401, message = "Unauthorized  List of supported error codes: - 40: Missing credentials - 41: Invalid credentials - 42: Expired credentials", response = ErrorRepresentation.class),
			@ApiResponse(code = 403, message = "Forbidden  List of supported error codes: - 50: Access denied - 51: Forbidden requester - 52: Forbidden user - 53: Too many requests", response = ErrorRepresentation.class),
			@ApiResponse(code = 404, message = "Not Found  List of supported error codes: - 60: Resource not found", response = ErrorRepresentation.class),
			@ApiResponse(code = 422, message = "Unprocessable entity  Functional error", response = ErrorRepresentation.class),
			@ApiResponse(code = 500, message = "Internal Server Error  List of supported error codes: - 1: Internal error", response = ErrorRepresentation.class),
			@ApiResponse(code = 503, message = "Service Unavailable  List of supported error codes: - 5: The service is temporarily unavailable", response = ErrorRepresentation.class) })
	@RequestMapping(value = "productOrdering/productOrder",
			produces = { "application/json;charset=utf-8" },
			consumes = { "application/json;charset=utf-8" },
			method = RequestMethod.POST)
	@IsAuthorizedOrderCreatorOrAdmin
	@SuppressWarnings("unchecked")
	@Override
	public ResponseEntity<ProductOrder> productOrderCreate(
			@ApiParam(required = true) @Valid @RequestBody ProductOrder productOrder)

	{
		if (productOrder == null)
		{
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		OrderData createdOrderData;
		try
		{
			String userId = productOrder.getRelatedParty().get(0).getId();
			if (productOrder.getShoppingCart() != null)
			{
				createdOrderData = checkoutFacade.placeOrderFromCart(productOrder.getShoppingCart().getId(), userId, true);
				CartData cartData = new CartData();
				cartData.setCode(productOrder.getShoppingCart().getId());
				createdOrderData.setCart(cartData);
			}
			else
			{
				OrderData orderDataToBeCreated = getDataMapper().map(productOrder, OrderData.class);
				createdOrderData = checkoutFacade.placeOrderFromDto(orderDataToBeCreated, userId);
			}

			if (createdOrderData == null)
			{
				return getUnsuccessfulResponseWithErrorRepresentation(null, null, 1, "Order cannot be created",
						HttpStatus.UNPROCESSABLE_ENTITY);
			}

			final ProductOrder createdProductOrderWsDto = getDataMapper().map(createdOrderData, ProductOrder.class);

			return new ResponseEntity(getObjectMapper().writeValueAsString(createdProductOrderWsDto), HttpStatus.CREATED);
		}
		catch (final OrderProcessingException e)
		{
			return getUnsuccessfulResponseWithErrorRepresentation("Order cannot be created", e,
					e.getOrderProcessingErrorCode().getErrorCode(), e.getBusinessReason(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
		catch (final IllegalArgumentException e)
		{
			return getUnsuccessfulResponseWithErrorRepresentation(BAD_REQUEST, e, 22, e.getMessage(),
					HttpStatus.BAD_REQUEST);
		}
		catch (final JsonProcessingException e)
		{
			return getUnsuccessfulResponse("Error occurred while preparing the response", e, HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}
	
	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		binder.setDisallowedFields(DISALLOWED_FIELDS);
	}
}
