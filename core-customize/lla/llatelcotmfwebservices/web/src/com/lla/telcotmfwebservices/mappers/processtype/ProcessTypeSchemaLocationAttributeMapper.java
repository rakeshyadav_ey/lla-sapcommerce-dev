/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.processtype;

import de.hybris.platform.b2ctelcoservices.enums.TmaProcessType;
import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.ProcessType;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for schemaLocation attribute between {@link TmaProcessType} and
 * {@link ProcessType}
 *
 * @since 1907
 */
public class ProcessTypeSchemaLocationAttributeMapper extends TmaAttributeMapper<TmaProcessType, ProcessType>
{
	@Override
	public void populateTargetAttributeFromSource(TmaProcessType source, ProcessType target, MappingContext context)
	{
		target.setAtschemaLocation(LlatelcotmfwebservicesConstants.TMA_API_SCHEMA);
	}
}
