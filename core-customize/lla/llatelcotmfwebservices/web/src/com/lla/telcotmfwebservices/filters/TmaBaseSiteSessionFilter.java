/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.filters;


import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import com.lla.telcotmfwebservices.exception.TmaInvalidResourceException;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.Config;
import de.hybris.platform.webservicescommons.util.YSanitizer;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;


/**
 * Filter that creates a new session for the request and sets current baseSite. if request
 * url contains baseSite id, that value is used to set session baseSite
 * otherwise the default value is picked from project.properties.
 *
 * @since 1810
 */
public class TmaBaseSiteSessionFilter extends OncePerRequestFilter
{
	private static final Logger LOG = Logger.getLogger(TmaBaseSiteSessionFilter.class);
	private BaseSiteService baseSiteService;

	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws ServletException, IOException
	{
		final String baseSiteId = request.getParameter(LlatelcotmfwebservicesConstants.HTTP_REQUEST_PARAM_BASESITE);
		if (StringUtils.isNotBlank(baseSiteId))
		{
			final BaseSiteModel requestedBaseSite = getBaseSiteService().getBaseSiteForUID(baseSiteId);

			if (requestedBaseSite != null)
			{
				final BaseSiteModel currentBaseSite = getBaseSiteService().getCurrentBaseSite();

				if (!requestedBaseSite.equals(currentBaseSite))
				{
					getBaseSiteService().setCurrentBaseSite(requestedBaseSite, true);
				}
			}
			else
			{
				final TmaInvalidResourceException ex = new TmaInvalidResourceException(YSanitizer.sanitize(baseSiteId));
				LOG.debug(ex.getMessage());
				throw ex;
			}
		}
		else
		{
			getBaseSiteService().setCurrentBaseSite(Config.getParameter(LlatelcotmfwebservicesConstants.SESSION_DEFAULT_BASESITE),
					true);
		}

		filterChain.doFilter(request, response);
	}

	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}
}
