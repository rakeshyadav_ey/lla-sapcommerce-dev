/*
 *   Copyright (c) 2019 SAP SE or an SAP affiliate company.  All rights reserved.
 */

package com.lla.telcotmfwebservices.mappers.orderprice;


import de.hybris.platform.b2ctelcofacades.data.TmaAbstractOrderChargePriceData;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.Money;
import com.lla.telcotmfwebservices.v2.dto.OrderPrice;
import com.lla.telcotmfwebservices.v2.dto.Price;
import org.springframework.util.ObjectUtils;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;
import org.springframework.beans.factory.annotation.Required;


/**
 * This attribute Mapper class maps data for duty free price attribute between {@link TmaAbstractOrderChargePriceData} and {@link
 * OrderPrice}
 *
 * @since 1907
 */
public class OrderPriceDutyFreeValueAttributeMapper extends TmaAttributeMapper<TmaAbstractOrderChargePriceData, OrderPrice>
{
	private MapperFacade mapperFacade;

	@Override
	public void populateTargetAttributeFromSource(final TmaAbstractOrderChargePriceData source, final OrderPrice target,
			final MappingContext context)
	{
		if (source.getDutyFreeAmount() == null)
		{
			return;
		}

		Money money = getMapperFacade().map(source.getDutyFreeAmount(), Money.class);
		if (ObjectUtils.isEmpty(target.getPrice()))
		{
			final Price price = new Price();
			target.setPrice(price);
		}

		target.getPrice().setDutyFreeAmount(money);
	}

	public MapperFacade getMapperFacade()
	{
		return mapperFacade;
	}

	@Required
	public void setMapperFacade(MapperFacade mapperFacade)
	{
		this.mapperFacade = mapperFacade;
	}
}


