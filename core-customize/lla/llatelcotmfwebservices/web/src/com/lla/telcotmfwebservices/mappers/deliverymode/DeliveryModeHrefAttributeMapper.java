/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.deliverymode;

import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.DeliveryMode;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;

import org.apache.commons.lang3.StringUtils;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for href attribute between {@link DeliveryModeData} and {@link DeliveryMode}
 *
 * @since 1907
 */
public class DeliveryModeHrefAttributeMapper extends TmaAttributeMapper<DeliveryModeData, DeliveryMode>
{
	@Override
	public void populateTargetAttributeFromSource(DeliveryModeData source, DeliveryMode target, MappingContext context)
	{
		if (StringUtils.isNotEmpty(source.getCode()))
		{
			target.setHref(LlatelcotmfwebservicesConstants.DELIVERY_MODE_API_URL + source.getCode());
		}
	}
}
