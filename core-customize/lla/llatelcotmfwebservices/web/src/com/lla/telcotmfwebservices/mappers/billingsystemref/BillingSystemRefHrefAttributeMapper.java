/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.billingsystemref;

import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.BillingSystemRef;

import org.apache.commons.lang.StringUtils;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for href attribute between {@link String} and {@link BillingSystemRef}
 *
 * @since 1907
 */
public class BillingSystemRefHrefAttributeMapper extends TmaAttributeMapper<String, BillingSystemRef>
{
	@Override
	public void populateTargetAttributeFromSource(String source, BillingSystemRef target, MappingContext context)
	{
		if (StringUtils.isNotEmpty(source))
		{
			target.setHref(LlatelcotmfwebservicesConstants.BILLING_SYSTEM_REF_API_URL + source);
		}
	}
}
