/*
 *   Copyright (c) 2019 SAP SE or an SAP affiliate company.  All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.shoppingcart;

import de.hybris.platform.b2ctelcofacades.data.TmaCartValidationData;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.CartItem;
import com.lla.telcotmfwebservices.v2.dto.ProductOfferingRef;
import com.lla.telcotmfwebservices.v2.dto.ShoppingCart;
import com.lla.telcotmfwebservices.v2.dto.ValidationItem;
import de.hybris.platform.commercefacades.order.EntryGroupData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class  maps data for entries attribute between {@link CartData} and {@link ShoppingCart}
 *
 * @since 1907
 */
public class ShoppingCartItemAttributeMapper extends TmaAttributeMapper<CartData, ShoppingCart>
{

	/**
	 * Underline separator
	 */
	private static final String SEPARATOR = "_";

	/**
	 * Mapper facade
	 */
	private MapperFacade mapperFacade;

	@Override
	public void populateTargetAttributeFromSource(CartData source, ShoppingCart target, MappingContext context)
	{
		if (CollectionUtils.isEmpty(source.getEntries()))
		{
			return;
		}

		final List<CartItem> cartItemsList = new ArrayList<>();

		final List<OrderEntryData> entriesWithoutBpo =
				source.getEntries().stream().filter((OrderEntryData entry) -> entry.getBpo() == null)
						.collect(Collectors.toList());
		final List<OrderEntryData> entriesWithBpo = source.getEntries().stream()
				.filter((OrderEntryData entry) -> entry.getBpo() != null)
				.collect(Collectors.toList());

		Map<Collection<Integer>, List<OrderEntryData>> groupedByBpo = entriesWithBpo.stream()
				.collect(Collectors.groupingBy(OrderEntryData::getEntryGroupNumbers));

		Map<String, List<OrderEntryData>> groupByBpoAndEntryNumber = new HashMap<>();
		for (Map.Entry<Collection<Integer>, List<OrderEntryData>> entry : groupedByBpo.entrySet())
		{
			groupByBpoAndEntryNumber.putAll(groupedItems(entry.getValue()));
		}

		groupByBpoAndEntryNumber.forEach((bpo, entries) -> {
			final CartItem bpoItem = new CartItem();
			setValidationErrors(source, entries.get(0), bpoItem, context);
			if (isTheSameBpoForEntries(entries) || entries.size() == 1)
			{
				if (entries.get(0).getBpo() != null)
				{
					final ProductOfferingRef productOfferingRef = getMapperFacade()
							.map(entries.get(0).getBpo(), ProductOfferingRef.class, context);
					bpoItem.setProductOffering(productOfferingRef);
				}

				String id = source.getCode() + (entries.get(0).getBpo() != null ?
						SEPARATOR + entries.get(0).getEntryGroupNumbers().iterator().next() : "");

				bpoItem.setId(id);
				final List<CartItem> items = new ArrayList<>();
				entries.forEach((OrderEntryData orderEntryData) ->
				{
					final CartItem cartItem = getMapperFacade().map(orderEntryData, CartItem.class, context);
					items.add(cartItem);
				});

				bpoItem.setCartItem(items);
				cartItemsList.add(bpoItem);
			}
		});

		entriesWithoutBpo.forEach((OrderEntryData entry) -> {
			final CartItem cartItem = getMapperFacade().map(entry, CartItem.class, context);
			cartItemsList.add(cartItem);
		});
		target.setCartItem(cartItemsList);
	}

	private Map<String, List<OrderEntryData>> groupedItems(List<OrderEntryData> allCartItems)
	{
		return allCartItems.stream()
				.collect(Collectors.groupingBy((OrderEntryData entryData) -> entryData.getEntryGroupNumbers().toString()));
	}

	private boolean isTheSameBpoForEntries(List<OrderEntryData> entriesList)
	{
		if (CollectionUtils.isEmpty(entriesList) || entriesList.get(0).getBpo() == null)
		{
			return false;
		}
		String bpoId = entriesList.get(0).getBpo().getCode();
		for (OrderEntryData entry : entriesList)
		{
			if (!bpoId.equals(entry.getBpo().getCode()))
			{
				return false;
			}
		}
		return true;
	}

	private void setValidationErrors(CartData source, OrderEntryData entry, CartItem bpoItem,
			MappingContext context)
	{
		List<String> errors = new ArrayList<>();
		List<TmaCartValidationData> validationDataList = new ArrayList<>();
		Iterator iterator = entry.getEntryGroupNumbers().iterator();
		while (iterator.hasNext())
		{
			Integer groupNumber = (Integer) iterator.next();
			for (EntryGroupData entryGroupData : source.getRootGroups())
			{
				final Set<TmaCartValidationData> validationMessages = entryGroupData.getValidationMessages();
				if (entryGroupData.getGroupNumber().equals(groupNumber) && CollectionUtils
						.isNotEmpty(entryGroupData.getValidationMessages()))
				{
					validationDataList.addAll(validationMessages);
				}
			}
		}
		bpoItem.setValidationError(errors);

		final List<ValidationItem> validationDataSet = validationDataList.stream()
				.map(invalidMessage -> getMapperFacade().map(invalidMessage, ValidationItem.class, context))
				.collect(Collectors.toList());
		bpoItem.setValidation(validationDataSet);
	}

	public MapperFacade getMapperFacade()
	{
		return mapperFacade;
	}

	@Required
	public void setMapperFacade(MapperFacade mapperFacade)
	{
		this.mapperFacade = mapperFacade;
	}
}
