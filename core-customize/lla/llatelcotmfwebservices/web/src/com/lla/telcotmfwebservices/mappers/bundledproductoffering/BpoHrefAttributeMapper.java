/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.bundledproductoffering;

import com.lla.telcotmfwebservices.v2.dto.BundledProductOffering;
import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import ma.glasnost.orika.MappingContext;
import org.apache.commons.lang.StringUtils;


/**
 * This attribute Mapper class maps data for href attribute between {@link ProductData} and {@link BundledProductOffering}
 *
 * @since 1903
 */
public class BpoHrefAttributeMapper extends TmaAttributeMapper<ProductData, BundledProductOffering>
{

	@Override
	public void populateTargetAttributeFromSource(ProductData source, BundledProductOffering target, MappingContext context)
	{
		if (StringUtils.isNotEmpty(source.getCode()))
		{
			target.setHref(LlatelcotmfwebservicesConstants.PRODUCT_OFFERING_API_URL + source.getCode());
		}
	}
}
