/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */

package com.lla.telcotmfwebservices.mappers.review;

import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.Review;
import com.lla.telcotmfwebservices.v2.dto.ReviewStatus;
import de.hybris.platform.commercefacades.product.data.ReviewData;

import ma.glasnost.orika.MappingContext;


public class ReviewStatusAttributeMapper extends TmaAttributeMapper<ReviewData, Review>
{
	@Override
	public void populateTargetAttributeFromSource(ReviewData source, Review target, MappingContext context)
	{
		if (source.getLifecycleStatus() != null)
		{
			ReviewStatus reviewStatus = ReviewStatus.valueOf(source.getLifecycleStatus().toUpperCase());
			target.setStatus(reviewStatus);
		}
	}
}
