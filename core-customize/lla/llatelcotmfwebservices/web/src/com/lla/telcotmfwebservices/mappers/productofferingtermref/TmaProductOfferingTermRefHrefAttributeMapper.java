/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company.  All rights reserved.
 */

package com.lla.telcotmfwebservices.mappers.productofferingtermref;

import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.ProductOfferingTermRef;
import de.hybris.platform.subscriptionfacades.data.SubscriptionTermData;

import org.apache.commons.lang.StringUtils;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for href attribute between {@link SubscriptionTermData} and {@link ProductOfferingTermRef}
 *
 * @since 1907
 */
public class TmaProductOfferingTermRefHrefAttributeMapper extends TmaAttributeMapper<SubscriptionTermData,
	 ProductOfferingTermRef>
{
	 @Override
	 public void populateTargetAttributeFromSource(SubscriptionTermData source, ProductOfferingTermRef target,
		  MappingContext context)
	 {
		  if (StringUtils.isNotEmpty(source.getId()))
		  {
				target.setHref(LlatelcotmfwebservicesConstants.PRODUCT_OFFERING_TERM_API_URL + source.getId());
		  }

	 }
}
