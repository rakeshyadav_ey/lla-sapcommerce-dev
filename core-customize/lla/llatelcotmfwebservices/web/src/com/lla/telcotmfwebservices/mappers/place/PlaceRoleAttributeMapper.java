/*
 *   Copyright (c) 2019 SAP SE or an SAP affiliate company.  All rights reserved.
 */

package com.lla.telcotmfwebservices.mappers.place;

import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.b2ctelcoservices.enums.TmaPlaceRoleType;
import com.lla.telcotmfwebservices.v2.dto.Place;
import de.hybris.platform.commercefacades.user.data.AddressData;


import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for role attribute between {@link AddressData} and {@link Place}
 *
 * @since 1911
 */
public class PlaceRoleAttributeMapper extends TmaAttributeMapper<AddressData, Place>
{
	@Override
	public void populateTargetAttributeFromSource(final AddressData source, final Place target,
			final MappingContext context)
	{
		TmaPlaceRoleType roleType = (TmaPlaceRoleType) context.getProperty("placeRole");
		target.setRole(roleType.name());
	}
}
