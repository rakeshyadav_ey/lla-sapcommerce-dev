/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.channelref;

import com.lla.telcotmfwebservices.v2.dto.ChannelRef;
import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.europe1.enums.PriceRowChannel;
import ma.glasnost.orika.MappingContext;
import org.apache.commons.lang.StringUtils;


/**
 * This attribute Mapper class maps data for href attribute between {@link PriceRowChannel} and {@link ChannelRef}
 *
 * @since 1903
 */
public class ChannelRefHrefAttributeMapper extends TmaAttributeMapper<PriceRowChannel, ChannelRef>
{

	@Override
	public void populateTargetAttributeFromSource(PriceRowChannel source, ChannelRef target, MappingContext context)
	{
		if (StringUtils.isNotEmpty(source.getCode()))
		{
			target.setHref(LlatelcotmfwebservicesConstants.CHANNEL_API_URL + source.getCode());
		}
	}
}
