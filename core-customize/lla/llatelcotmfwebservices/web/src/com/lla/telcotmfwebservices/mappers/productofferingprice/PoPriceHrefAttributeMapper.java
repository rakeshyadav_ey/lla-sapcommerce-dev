/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.productofferingprice;

import com.lla.telcotmfwebservices.v2.dto.ProductOfferingPrice;
import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.subscriptionfacades.data.SubscriptionPricePlanData;
import ma.glasnost.orika.MappingContext;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * This attribute Mapper class maps data for href attribute between {@link SubscriptionPricePlanData} and
 * {@link ProductOfferingPrice}
 *
 * @since 1903
 */
public class PoPriceHrefAttributeMapper extends TmaAttributeMapper<SubscriptionPricePlanData, ProductOfferingPrice>
{
	/**
	 * Po price id generator
	 */
	private PoPriceIdGenerator poPriceIdGenerator;

	@Override
	public void populateTargetAttributeFromSource(final SubscriptionPricePlanData source, final ProductOfferingPrice target,
			final MappingContext context)
	{
		String poPriceId = getPoPriceIdGenerator().generateIdForPrice(source);

		if (StringUtils.isNotEmpty(poPriceId))
		{
			target.setHref(LlatelcotmfwebservicesConstants.PRODUCT_OFFERING_PRICE_API_URL + poPriceId);
		}
	}

	@SuppressWarnings("WeakerAccess")
	protected PoPriceIdGenerator getPoPriceIdGenerator()
	{
		return poPriceIdGenerator;
	}

	@Required
	public void setPoPriceIdGenerator(
			PoPriceIdGenerator poPriceIdGenerator)
	{
		this.poPriceIdGenerator = poPriceIdGenerator;
	}
}
