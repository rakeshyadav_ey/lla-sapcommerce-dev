/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.productorder;

import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.b2ctelcofacades.order.converters.populator.TmaOrderEntryPopulator;
import com.lla.telcotmfwebservices.v2.dto.OrderItem;
import com.lla.telcotmfwebservices.v2.dto.ProductOfferingRef;
import com.lla.telcotmfwebservices.v2.dto.ProductOrder;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for attype attribute between {@link OrderData} and {@link ProductOrder}
 *
 * @since 1907
 */
public class ProductOrderItemAttributeMapper extends TmaAttributeMapper<OrderData, ProductOrder>
{
	/**
	 * Mapper facade
	 */
	private MapperFacade mapperFacade;

	@Override
	public void populateTargetAttributeFromSource(final OrderData source, final ProductOrder target,
			final MappingContext context)
	{
		if (CollectionUtils.isEmpty(source.getEntries()))
		{
			return;
		}
		final List<OrderItem> cartItemsList = new ArrayList<>();

		final List<OrderEntryData> entriesWithoutBpo =
				source.getEntries().stream().filter((OrderEntryData entry) -> entry.getBpo() == null)
						.collect(Collectors.toList());
		final List<OrderEntryData> entriesWithBpo = source.getEntries().stream()
				.filter((OrderEntryData entry) -> entry.getBpo() != null)
				.collect(Collectors.toList());

		Map<Collection<Integer>, List<OrderEntryData>> groupedByBpo = entriesWithBpo.stream()
				.collect(Collectors.groupingBy(OrderEntryData::getEntryGroupNumbers));

		Map<String, List<OrderEntryData>> groupByBpoAndEntryNumber = new HashMap<>();
		for (Map.Entry<Collection<Integer>, List<OrderEntryData>> entry : groupedByBpo.entrySet())
		{
			groupByBpoAndEntryNumber.putAll(groupedItems(entry.getValue()));
		}

		groupByBpoAndEntryNumber.forEach((bpo, entries) -> {
			final OrderItem bpoItem = new OrderItem();
			if (isTheSameBpoForEntries(entries) || entries.size() == 1)
			{
				if (entries.get(0).getBpo() != null)
				{
					final ProductOfferingRef productOfferingRef = getMapperFacade()
							.map(entries.get(0).getBpo(), ProductOfferingRef.class, context);
					bpoItem.setProductOffering(productOfferingRef);
				}

				String id = entries.get(0).getId() + (entries.get(0).getBpo() != null ?
						TmaOrderEntryPopulator.SEPARATOR + entries.get(0).getEntryGroupNumbers().iterator().next() : "");

				bpoItem.setId(id);
				final List<OrderItem> items = new ArrayList<>();
				entries.forEach((OrderEntryData orderEntryData) ->
				{
					final OrderItem cartItem = getMapperFacade().map(orderEntryData, OrderItem.class, context);
					items.add(cartItem);
				});

				bpoItem.setOrderItem(items);
				cartItemsList.add(bpoItem);
			}
		});

		entriesWithoutBpo.forEach((OrderEntryData entry) -> {
			final OrderItem cartItem = getMapperFacade().map(entry, OrderItem.class, context);
			cartItemsList.add(cartItem);
		});

		target.setOrderItem(cartItemsList);
	}

	private Map<String, List<OrderEntryData>> groupedItems(List<OrderEntryData> allCartItems)
	{
		return allCartItems.stream()
				.collect(Collectors.groupingBy((OrderEntryData entryData) -> entryData.getEntryGroupNumbers().toString()));
	}

	private boolean isTheSameBpoForEntries(List<OrderEntryData> entriesList)
	{
		if (CollectionUtils.isEmpty(entriesList) || entriesList.get(0).getBpo() == null)
		{
			return false;
		}
		String bpoId = entriesList.get(0).getBpo().getCode();
		for (OrderEntryData entry : entriesList)
		{
			if (!bpoId.equals(entry.getBpo().getCode()))
			{
				return false;
			}
		}
		return true;
	}

	public MapperFacade getMapperFacade()
	{
		return mapperFacade;
	}

	@Required
	public void setMapperFacade(MapperFacade mapperFacade)
	{
		this.mapperFacade = mapperFacade;
	}
}
