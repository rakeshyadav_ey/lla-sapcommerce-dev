/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.usagepricechargeentry;

import com.lla.telcotmfwebservices.v2.dto.UsagePriceChargeEntry;
import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.subscriptionfacades.data.OverageUsageChargeEntryData;
import de.hybris.platform.subscriptionfacades.data.TierUsageChargeEntryData;
import de.hybris.platform.subscriptionfacades.data.UsageChargeEntryData;
import ma.glasnost.orika.MappingContext;
import org.apache.commons.lang.StringUtils;


/**
 * This attribute Mapper class maps data for href attribute between {@link UsageChargeEntryData} and {@link UsagePriceChargeEntry}
 *
 * @since 1903
 */
public class UpceHrefAttributeMapper extends TmaAttributeMapper<UsageChargeEntryData, UsagePriceChargeEntry>
{
	@Override
	public void populateTargetAttributeFromSource(final UsageChargeEntryData source, final UsagePriceChargeEntry target,
			final MappingContext context)
	{
		String sourceId = null;
		if (source instanceof TierUsageChargeEntryData)
		{
			sourceId = ((TierUsageChargeEntryData) source).getId();
		}

		if (source instanceof OverageUsageChargeEntryData)
		{
			sourceId = ((OverageUsageChargeEntryData) source).getId();
		}

		if (StringUtils.isNotEmpty(sourceId))
		{
			target.setHref(LlatelcotmfwebservicesConstants.PRODUCT_OFFERING_PRICE_API_URL + sourceId);
		}
	}
}
