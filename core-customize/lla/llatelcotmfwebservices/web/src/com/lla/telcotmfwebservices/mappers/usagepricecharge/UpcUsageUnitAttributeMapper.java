/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.usagepricecharge;

import com.lla.telcotmfwebservices.v2.dto.UsageUnit;
import com.lla.telcotmfwebservices.v2.dto.UsagePriceCharge;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.subscriptionfacades.data.UsageChargeData;
import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for href attribute between {@link UsageChargeData} and {@link UsagePriceCharge}
 *
 * @since 1903
 */
public class UpcUsageUnitAttributeMapper extends TmaAttributeMapper<UsageChargeData, UsagePriceCharge>
{
	@Override
	public void populateTargetAttributeFromSource(final UsageChargeData source, final UsagePriceCharge target,
			final MappingContext context)
	{
		if (source.getUsageUnit() == null)
		{
			return;
		}

		final UsageUnit usageUnit = new UsageUnit();
		usageUnit.setId(source.getUsageUnit().getId());
		usageUnit.setName(source.getUsageUnit().getName());
		target.setUsageUnit(usageUnit);
	}
}
