/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.security;

import com.lla.telcotmfwebservices.exception.TmaInvalidRelatedPartyException;
import com.lla.telcotmfwebservices.v2.dto.RelatedPartyRef;
import com.lla.telcotmfwebservices.v2.dto.ShoppingCart;
import com.lla.telcotmfwebservices.v2.dto.ShoppingCartUnderscoreCreate;
import com.lla.telcotmfwebservices.v2.dto.ShoppingCartUnderscoreUpdate;

import com.lla.telcotmfwebservices.v2.dto.ProductOrder;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import java.util.List;


/**
 * Validates if the authenticated user is authorized to access a resource
 *
 * @since 1907
 */
public class UserValidator
{
	private static final String ANONYMOUS = "anonymous";

	public boolean isResourceOwner(Authentication authentication, String userId)
	{
		if (StringUtils.isBlank(userId))
		{
			throw new TmaInvalidRelatedPartyException();
		}

		if (authentication.getPrincipal() == null)
		{
			return Boolean.FALSE;
		}

		return authentication.getPrincipal().equals(userId);
	}

	/**
	 * From the list of related parties provided in the productOrder object the first item is used for checking if it matches with
	 * the principal for which authorization has been obtained.
	 *
	 * @param authentication
	 * 		authentication object
	 * @param productOrder
	 * 		the product order from where the related party being checked is obtained
	 * @return true in case the authentication is not client only and if the principal matches with the id of thew first related
	 * party provided
	 */
	public boolean isRelatedPartyAuthorizedToPlaceOrder(Authentication authentication, ProductOrder productOrder)
	{
		List<RelatedPartyRef> relatedParties = (productOrder == null) ? null : productOrder.getRelatedParty();
		if (CollectionUtils.isEmpty(relatedParties))
		{
			throw new TmaInvalidRelatedPartyException();
		}

		if (authentication.getPrincipal() == null)
		{
			return Boolean.FALSE;
		}

		return (!((OAuth2Authentication) authentication).isClientOnly()) &&
				(authentication.getPrincipal().equals(relatedParties.get(0).getId()));
	}

	/**
	 * Checks if the user provided is not Anonymous.
	 *
	 * @param userId
	 * 		The userId provided.
	 * @return False if user is anonymous, otherwise true.
	 */
	public boolean isNotAnonymous(String userId)
	{
		if (userId == null)
		{
			throw new TmaInvalidRelatedPartyException();
		}

		return !ANONYMOUS.equalsIgnoreCase(userId);
	}

	/**
	 * Checks if the user provided is Anonymous and the client is authenticated.
	 *
	 * @param authentication - the authenticated client
	 * @param userId
	 * 		The userId provided.
	 * @return true if user is anonymous, otherwise false.
	 */
	public boolean isAnonymous(Authentication authentication, String userId)
	{
		if (StringUtils.isBlank(userId))
		{
			throw new TmaInvalidRelatedPartyException();
		}

		if (authentication.getPrincipal() == null)
		{
			return Boolean.FALSE;
		}

		if (StringUtils.equals(userId.toLowerCase(), ANONYMOUS))
		{
			if (((OAuth2Authentication) authentication).isClientOnly())
			{
				return Boolean.TRUE;
			}
			else
			{
				return ANONYMOUS.equals(authentication.getPrincipal().toString());
			}
		}

		return Boolean.FALSE;
	}

	 /**
	  * From the list of related parties provided in the{@link ShoppingCartUnderscoreUpdate} object the first item is used for
	  * checking if it matches with the principal for which authorization has been obtained.
	  *
	  * @param authentication
	  * 		authentication object
	  * @param shoppingCart
	  * 		the shopping cart from where the list of related parties being checked is obtained
	  * @return true in case the authentication is not client only and if the principal matches with the id of the first related
	  * party provided
	  * @deprecated since 1911. Use {@link #isRelatedPartyAuthorizedShoppingCartUser(Authentication, ShoppingCart)}
	  */
	 @Deprecated(since = "1911", forRemoval= true)
	public boolean isRelatedPartyAuthorizedToUpdateShoppingCart(Authentication authentication,
		 ShoppingCartUnderscoreUpdate shoppingCart)
	{
		 List<RelatedPartyRef> relatedParties = (shoppingCart == null) ? null : shoppingCart.getRelatedParty();
		 return isRelatedPartyAuthorizedUserOrAdmin(authentication, relatedParties);
	}

	/**
	 * From the list of related parties provided in the{@link ShoppingCart} object the first item is used for
	 * checking if it matches with the principal for which authorization has been obtained.
	 *
	 * @param authentication
	 * 		authentication object
	 * @param shoppingCart
	 * 		the shopping cart from where the list of related parties being checked is obtained
	 * @return true in case the authentication is not client only and if the principal matches with the id of the first related
	 * party provided
	 */
	public boolean isRelatedPartyAuthorizedShoppingCartUser(Authentication authentication,
			ShoppingCart shoppingCart)
	{
		List<RelatedPartyRef> relatedParties = (shoppingCart == null) ? null : shoppingCart.getRelatedParty();
		return isRelatedPartyAuthorizedUserOrAdmin(authentication, relatedParties);
	}

	 /**
	  * From the list of related parties provided in the {@link ShoppingCartUnderscoreCreate} object the first item is used for
	  * checking if it matches with the principal for which authorization has been obtained.
	  *
	  * @param authentication
	  * 		authentication object
	  * @param shoppingCart
	  * 		the shopping cart from where the list of related parties being checked is obtained
	  * @return true in case the authentication is not client only and if the principal matches with the id of the first related
	  * party provided
	  * @deprecated since 1911. Use {@link #isRelatedPartyAuthorizedShoppingCartUser(Authentication, ShoppingCart)}
	  */
	 @Deprecated(since = "1911", forRemoval= true)
	 public boolean isRelatedPartyAuthorizedToCreateShoppingCart(Authentication authentication,
		  ShoppingCartUnderscoreCreate shoppingCart)
	 {
		  final List<RelatedPartyRef> relatedParties = (shoppingCart == null) ? null : shoppingCart.getRelatedParty();
		  return isRelatedPartyAuthorizedUserOrAdmin(authentication, relatedParties);
	 }

	/**
	 * Checks if the first user provided in the list of related parties of the shopping cart is Anonymous and the client is
	 * authenticated.
	 *
	 * @param authentication
	 * 		the authenticated client
	 * @param shoppingCart
	 * 		the shopping cart from where the list of related parties being checked is obtained
	 * @return true if user is anonymous, otherwise false.
	 */
	public boolean isAnonymous(Authentication authentication, ShoppingCart shoppingCart)
	{
		final List<RelatedPartyRef> relatedParties = (shoppingCart == null) ? null : shoppingCart.getRelatedParty();
		return isAnonymous(authentication, relatedParties);
	}

	 /**
	  * Checks if the first user provided in the list of related parties of the shopping cart is Anonymous and the client is
	  * authenticated.
	  *
	  * @param authentication
	  * 		the authenticated client
	  * @param shoppingCart
	  * 		the shopping cart from where the list of related parties being checked is obtained
	  * @return true if user is anonymous, otherwise false.
	  * @deprecated since 1911. Use {@link #isAnonymous(Authentication, ShoppingCart)}
	  */
	 @Deprecated(since = "1911", forRemoval= true)
	 public boolean isAnonymous(Authentication authentication, ShoppingCartUnderscoreUpdate shoppingCart)
	 {
		  final List<RelatedPartyRef> relatedParties = (shoppingCart == null) ? null : shoppingCart.getRelatedParty();
		  return isAnonymous(authentication, relatedParties);
	 }

	 /**
	  * Checks if the first user provided in the list of related parties of the shopping cart is Anonymous and the client is
	  * authenticated.
	  *
	  * @param authentication
	  * 		the authenticated client
	  * @param shoppingCart
	  * 		the shopping cart from where the list of related parties being checked is obtained
	  * @return true if user is anonymous, otherwise false.
	  * @deprecated since 1911. Use {@link #isAnonymous(Authentication, ShoppingCart)}
	  */
	 @Deprecated(since = "1911", forRemoval= true)
	 public boolean isAnonymous(Authentication authentication, ShoppingCartUnderscoreCreate shoppingCart)
	 {
		  final List<RelatedPartyRef> relatedParties = (shoppingCart == null) ? null : shoppingCart.getRelatedParty();
		  return isAnonymous(authentication, relatedParties);
	 }

	 /**
	  * Checks if the first user provided in the list of related parties is Anonymous and the client is authenticated.
	  *
	  * @param authentication
	  * 		the authenticated client
	  * @param relatedParties
	  * 		the list of related parties
	  * @return true if user is anonymous, otherwise false.
	  */
	 public boolean isAnonymous(Authentication authentication, List<RelatedPartyRef> relatedParties)
	 {
		  if (CollectionUtils.isEmpty(relatedParties))
		  {
				throw new TmaInvalidRelatedPartyException();
		  }

		  final RelatedPartyRef firstRelatedParty = relatedParties.get(0);

		  if (firstRelatedParty == null)
		  {
				throw new TmaInvalidRelatedPartyException();
		  }

		  return isAnonymous(authentication, firstRelatedParty.getId());
	 }

	 /**
	  * From the list of related parties provided the first item is used for checking if it matches with the principal for which
	  * authorization has been obtained.
	  *
	  * @param authentication
	  * 		authentication object
	  * @param relatedParties
	  * 		the list of related parties
	  * @return true in case the authentication is not client only and if the principal matches with the id of the first related
	  * party provided
	  */
	 public boolean isRelatedPartyAuthorizedUserOrAdmin(Authentication authentication, List<RelatedPartyRef> relatedParties)
	 {
		  if (CollectionUtils.isEmpty(relatedParties))
		  {
				throw new TmaInvalidRelatedPartyException();
		  }

		  final RelatedPartyRef firstRelatedParty = relatedParties.get(0);

		  if (firstRelatedParty == null)
		  {
				throw new TmaInvalidRelatedPartyException();
		  }

		  if (authentication.getPrincipal() == null)
		  {
				return Boolean.FALSE;
		  }

		  return (!((OAuth2Authentication) authentication).isClientOnly()) &&
				(authentication.getPrincipal().equals(firstRelatedParty.getId()));

	 }
}
