/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.subscriptionbase;

import de.hybris.platform.b2ctelcofacades.data.TmaDetailedSubscriptionBaseData;
import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.SubscriptionBase;

import org.apache.commons.lang.StringUtils;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for schema location attribute between {@link TmaDetailedSubscriptionBaseData} and
 * {@link SubscriptionBase}
 *
 * @since 1907
 */
public class SubscriptionBaseSchemaLocationAttributeMapper
		extends TmaAttributeMapper<TmaDetailedSubscriptionBaseData, SubscriptionBase>
{
	@Override
	public void populateTargetAttributeFromSource(TmaDetailedSubscriptionBaseData source, SubscriptionBase target,
			MappingContext context)
	{
		if (StringUtils.isNotEmpty(source.getCode()))
		{
			target.setAtschemaLocation(LlatelcotmfwebservicesConstants.TMA_API_SCHEMA);
		}
	}
}
