/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */

package com.lla.telcotmfwebservices.mappers.productoffering;

import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.ProductOffering;
import de.hybris.platform.commercefacades.product.data.ProductData;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for rating attribute between {@link ProductData} and {@link ProductOffering}
 *
 * @since 1911
 */
public class PoRatingAttributeMapper extends TmaAttributeMapper<ProductData, ProductOffering>
{
	@Override
	public void populateTargetAttributeFromSource(ProductData source, ProductOffering target, MappingContext context)
	{
		if (source.getAverageRating() != null)
		{
			target.setRating(source.getAverageRating().toString());
		}
	}
}
