/*
 *   Copyright (c) 2019 SAP SE or an SAP affiliate company.  All rights reserved.
 */

package com.lla.telcotmfwebservices.mappers.place;

import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import com.lla.telcotmfwebservices.v2.dto.Place;
import de.hybris.platform.commercefacades.user.data.AddressData;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for href attribute between {@link AddressData} and {@link Place}
 *
 * @since 1911
 */
public class PlaceHrefAttributeMapper extends TmaAttributeMapper<AddressData, Place>
{

	@Override
	public void populateTargetAttributeFromSource(final AddressData source, final Place target,
			final MappingContext context)
	{
		target.setHref(LlatelcotmfwebservicesConstants.ADDRESS_API_URL + source.getCode());
	}
}
