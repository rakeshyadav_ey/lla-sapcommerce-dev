/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company.  All rights reserved.
 */

package com.lla.telcotmfwebservices.mappers.recommendationitem;

import de.hybris.platform.b2ctelcofacades.data.TmaOfferData;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.ProductOfferingPrice;
import com.lla.telcotmfwebservices.v2.dto.RecommendationItem;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for price attribute between {@link TmaOfferData} and {@link RecommendationItem}
 *
 * @since 1907
 */
public class TmaRecommendationItemPriceAttributeMapper extends TmaAttributeMapper<TmaOfferData, RecommendationItem>
{
	 /**
	  * Mapper facade
	  */
	 private MapperFacade mapperFacade;

	 @Override
	 public void populateTargetAttributeFromSource(TmaOfferData source, RecommendationItem target, MappingContext context)
	 {
		if (source.getProduct() != null && CollectionUtils.isEmpty(source.getProduct().getProductOfferingPrices()))
		{
			 return;
		}

		final List<ProductOfferingPrice> productOfferingPriceList = new ArrayList<>();

		 productOfferingPriceList
				 .add(getMapperFacade().map(source.getProduct().getMainSpoPriceInBpo(), ProductOfferingPrice.class, context));

		target.setPrice(productOfferingPriceList);
	 }

	 public MapperFacade getMapperFacade()
	 {
		  return mapperFacade;
	 }

	 @Required
	 public void setMapperFacade(MapperFacade mapperFacade)
	 {
		  this.mapperFacade = mapperFacade;
	 }
}
