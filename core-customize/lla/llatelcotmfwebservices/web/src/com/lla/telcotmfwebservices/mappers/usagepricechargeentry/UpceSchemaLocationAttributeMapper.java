/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.usagepricechargeentry;

import com.lla.telcotmfwebservices.v2.dto.UsagePriceChargeEntry;
import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.subscriptionfacades.data.UsageChargeEntryData;
import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for schema location attribute between {@link UsageChargeEntryData} and
 * {@link UsagePriceChargeEntry}
 *
 * @since 1903
 */
public class UpceSchemaLocationAttributeMapper extends TmaAttributeMapper<UsageChargeEntryData, UsagePriceChargeEntry>
{
	@Override
	public void populateTargetAttributeFromSource(final UsageChargeEntryData source, final UsagePriceChargeEntry target,
			final MappingContext context)
	{
		target.setAtschemaLocation(LlatelcotmfwebservicesConstants.TMA_API_SCHEMA);
	}
}
