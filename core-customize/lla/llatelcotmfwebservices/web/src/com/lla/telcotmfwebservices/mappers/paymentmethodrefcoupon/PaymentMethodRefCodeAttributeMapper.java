/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */

package com.lla.telcotmfwebservices.mappers.paymentmethodrefcoupon;

import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.PaymentMethodRef;
import de.hybris.platform.commercefacades.coupon.data.CouponData;

import org.apache.commons.lang.StringUtils;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for code attribute between {@link CouponData} and {@link PaymentMethodRef}
 *
 * @since 2003
 */
public class PaymentMethodRefCodeAttributeMapper extends TmaAttributeMapper<CouponData, PaymentMethodRef>
{
	@Override
	public void populateTargetAttributeFromSource(CouponData source, PaymentMethodRef target, MappingContext context)
	{
		if (StringUtils.isNotEmpty(source.getCouponId()))
		{
			target.setCode(source.getCouponId());
		}
	}
}
