/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.productofferingref;

import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.ProductOfferingRef;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.util.Config;
import ma.glasnost.orika.MappingContext;
import org.apache.commons.lang.StringUtils;


/**
 * This attribute Mapper class maps data for atreferredType attribute between {@link ProductData} and {@link ProductOfferingRef}
 *
 * @since 1903
 */
public class PoRefReferredTypeAttributeMapper extends TmaAttributeMapper<ProductData, ProductOfferingRef>
{

	@Override
	public void populateTargetAttributeFromSource(final ProductData source, final ProductOfferingRef target,
			final MappingContext context)
	{
		if (StringUtils.isNotEmpty(source.getCode()))
		{
			target.setAtreferredType(Config.getParameter(LlatelcotmfwebservicesConstants.TMA_PRODUCT_OFFERING_DEFAULT_REFERRED));
		}
	}
}
