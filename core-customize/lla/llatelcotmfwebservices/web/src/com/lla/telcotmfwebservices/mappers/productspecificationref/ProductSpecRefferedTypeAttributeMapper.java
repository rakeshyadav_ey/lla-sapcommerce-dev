/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.productspecificationref;

import com.lla.telcotmfwebservices.v2.dto.ProductSpecificationRef;
import de.hybris.platform.b2ctelcofacades.data.TmaProductSpecificationData;
import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.util.Config;
import ma.glasnost.orika.MappingContext;
import org.apache.commons.lang.StringUtils;


/**
 * This attribute Mapper class maps data for href attribute between {@link TmaProductSpecificationData} and
 * {@link ProductSpecificationRef}
 *
 * @since 1903
 */
public class ProductSpecRefferedTypeAttributeMapper
		extends TmaAttributeMapper<TmaProductSpecificationData, ProductSpecificationRef>
{

	@Override
	public void populateTargetAttributeFromSource(TmaProductSpecificationData source, ProductSpecificationRef target,
			MappingContext context)
	{
		if (StringUtils.isNotEmpty(source.getId()))
		{
			target.setAtreferredType(Config.getParameter(LlatelcotmfwebservicesConstants.TMA_PRODUCTSPECIFICATION_DEFAULT_REFERRED));
		}
	}
}
