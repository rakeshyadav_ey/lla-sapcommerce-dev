/*
 *   Copyright (c) 2019 SAP SE or an SAP affiliate company.  All rights reserved.
 */

package com.lla.telcotmfwebservices.mappers.message;

import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.Message;

import org.apache.commons.lang3.StringUtils;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps type of message cart between {@link String} and {@link Message}
 *
 * @since 1907
 */
public class MessageTypeAttributeMapper extends TmaAttributeMapper<String, Message>
{
	@Override
	public void populateTargetAttributeFromSource(String source, Message target, MappingContext context)
	{
		if (StringUtils.isNotEmpty(source))
		{
			target.setType(Message.TypeEnum.ERROR);
		}
	}
}
