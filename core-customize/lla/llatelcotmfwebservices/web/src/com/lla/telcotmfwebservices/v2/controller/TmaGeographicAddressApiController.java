/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.v2.controller;

import de.hybris.platform.b2ctelcofacades.user.TmaUserFacade;
import com.lla.telcotmfwebservices.security.IsAuthorizedResourceOwnerOrAdmin;
import com.lla.telcotmfwebservices.v2.api.GeographicAddressApi;
import com.lla.telcotmfwebservices.v2.dto.Error;
import com.lla.telcotmfwebservices.v2.dto.ErrorRepresentation;
import com.lla.telcotmfwebservices.v2.dto.GeographicAddress;
import com.lla.telcotmfwebservices.validators.TmaGeoraphicAddressValidator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


/**
 * Default implementation of {@link GeographicAddressApi}
 *
 * @since 1907
 */
@SuppressWarnings("unused")
@Controller
@Api(value = "geographicAddress", description = "Geographic address management API", tags = { "Geographic Address Management" })
public class TmaGeographicAddressApiController extends TmaBaseController implements GeographicAddressApi
{
	@Resource(name = "userFacade")
	private TmaUserFacade userFacade;

	@Resource(name = "tmaGeoraphicAddressValidator")
	private TmaGeoraphicAddressValidator tmaGeoraphicAddressValidator;

	private final String[] DISALLOWED_FIELDS = new String[] {};

	@ApiOperation(value = "Retrieves a list of 'GeographicAddress'", nickname = "listGeographicAddresses", response = GeographicAddress.class,
			responseContainer = "List", tags = { "Geographic Address Management", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Success", response = GeographicAddress.class, responseContainer = "List"),
			@ApiResponse(code = 400, message = "Bad Request  List of supported error codes: - 20: Invalid URL parameter value - 21: Missing body - 22: Invalid body - 23: Missing body field - 24: Invalid body field - 25: Missing header - 26: Invalid header value - 27: Missing query-string parameter - 28: Invalid query-string parameter value", response = ErrorRepresentation.class),
			@ApiResponse(code = 404, message = "Not Found  List of supported error codes: - 60: Resource not found", response = ErrorRepresentation.class),
			@ApiResponse(code = 405, message = "Method Not Allowed  List of supported error codes: - 61: Method not allowed", response = ErrorRepresentation.class),
			@ApiResponse(code = 422, message = "Unprocessable entity  Functional error", response = ErrorRepresentation.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = ErrorRepresentation.class) })
	@RequestMapping(value = "/geographicAddress",
			produces = { "application/json;charset=utf-8" },
			method = RequestMethod.GET)
	@IsAuthorizedResourceOwnerOrAdmin
	@Override
	public ResponseEntity<List<GeographicAddress>> geographicAddressFind(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body") @Valid @RequestParam(value = "fields", required = false) String fields,
			@ApiParam(value = "Identifier of the BaseSite") @Valid @RequestParam(value = "baseSiteId", required = false) String baseSiteId,
			@ApiParam(value = "Id of a specific party role for which the addresses should be provided") @Valid @RequestParam(value = "relatedParty.id", required = false) String relatedPartyId,
			@ApiParam(value = "Requested index for start of resources to be provided in response requested by client") @Valid @RequestParam(value = "offset", required = false) String offset,
			@ApiParam(value = "Requested number of resources to be provided in response requested by client") @Valid @RequestParam(value = "limit", required = false) String limit)
	{
		try
		{
			if (StringUtils.isEmpty(relatedPartyId)) {
				return getUnsuccessfulResponseWithErrorRepresentation("Unprocessable entity  Functional error",null, 27, "Unprocessable entity  Functional error", HttpStatus.BAD_REQUEST);
			}

			final List<AddressData> addressDataList = userFacade.getAddressesForUser(relatedPartyId, true);
			if (CollectionUtils.isEmpty(addressDataList))
			{
				return new ResponseEntity<>(HttpStatus.OK);
			}

			final List<GeographicAddress> geographicAddressList = new ArrayList<>();
			for (AddressData addressData : addressDataList)
			{
				final GeographicAddress geographicAddress = getDataMapper().map(addressData, GeographicAddress.class, fields);
				geographicAddressList.add(geographicAddress);
			}

			return new ResponseEntity(getObjectMapper().writeValueAsString(geographicAddressList), HttpStatus.OK);
		}
		catch (final UnknownIdentifierException | JsonProcessingException e)
		{
			return getUnsuccessfulResponseWithErrorRepresentation("Missing related party",e, 62, "Missing query-string parameter", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@ApiOperation(value = "Creates a 'Geographic Address'", nickname = "createGeographicAddress", notes = "", response = GeographicAddress.class, tags = {
			"Geographic Address Management", })
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Created", response = GeographicAddress.class),
			@ApiResponse(code = 400, message = "Bad Request", response = Error.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
			@ApiResponse(code = 403, message = "Forbidden", response = Error.class),
			@ApiResponse(code = 404, message = "Not Found", response = Error.class),
			@ApiResponse(code = 405, message = "Method Not allowed", response = Error.class),
			@ApiResponse(code = 409, message = "Conflict", response = Error.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = Error.class) })
	@RequestMapping(value = "/geographicAddress",
			produces = { "application/json;charset=utf-8" },
			consumes = { "application/json;charset=utf-8" },
			method = RequestMethod.POST)
	@IsAuthorizedResourceOwnerOrAdmin
	@SuppressWarnings("unchecked")
	@Override
	public ResponseEntity<GeographicAddress> createGeographicAddress(
			@ApiParam(value = "The 'Geographic Address' Cart to be created", required = true) @Valid @RequestBody GeographicAddress geographicAddress,
			@NotNull @ApiParam(value = "The id of the related party.", required = true) @Valid @RequestParam(value = "relatedParty.id") String relatedPartyId)

	{
		if (geographicAddress == null)
		{
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		try
		{
			final String errorMessage = validate(geographicAddress, "geographicAddress", tmaGeoraphicAddressValidator);

			if (StringUtils.isNotEmpty(errorMessage))
			{
				return getUnsuccessfulResponseWithErrorRepresentation(BAD_REQUEST, null, 22, errorMessage, HttpStatus.BAD_REQUEST);
			}
			AddressData addressData = getDataMapper().map(geographicAddress, AddressData.class);
			addressData.setVisibleInAddressBook(true);

			AddressData address = userFacade.createAddressForUser(addressData, relatedPartyId);

			if (address == null)
			{
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}

			geographicAddress = getDataMapper().map(address, GeographicAddress.class);

			return new ResponseEntity(getObjectMapper().writeValueAsString(geographicAddress), HttpStatus.CREATED);
		}
		catch (final UnknownIdentifierException | JsonProcessingException e)
		{
			return getUnsuccessfulResponse("Error occurred due to bad request", e, HttpStatus.BAD_REQUEST);
		}
	}

	private boolean isRequestCorrect(GeographicAddress geographicAddress)
	{
		return StringUtils.isNotEmpty(geographicAddress.getCountry()) && StringUtils.isNotEmpty(geographicAddress.getPostcode());
	}

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		binder.setDisallowedFields(DISALLOWED_FIELDS);
	}
}
