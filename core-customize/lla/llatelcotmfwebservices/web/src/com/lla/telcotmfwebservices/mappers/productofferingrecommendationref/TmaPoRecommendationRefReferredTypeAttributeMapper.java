/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company.  All rights reserved.
 */

package com.lla.telcotmfwebservices.mappers.productofferingrecommendationref;

import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import com.lla.telcotmfwebservices.v2.dto.ProductOfferingRecommendationRef;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.util.Config;

import org.apache.commons.lang.StringUtils;

import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for referred type attribute between {@link ProductData} and
 * {@link ProductOfferingRecommendationRef}
 *
 * @since 1907
 */
public class TmaPoRecommendationRefReferredTypeAttributeMapper
	 extends TmaAttributeMapper<ProductData, ProductOfferingRecommendationRef>
{
	 @Override
	 public void populateTargetAttributeFromSource(ProductData source, ProductOfferingRecommendationRef target,
		  MappingContext context)
	 {
		  if (StringUtils.isNotEmpty(source.getCode()))
		  {
				target.setAtreferredType(
					 Config.getParameter(LlatelcotmfwebservicesConstants.PRODUCT_OFFERING_RECOMMENDATION_DEFAULT_REFERRED));
		  }
	 }
}
