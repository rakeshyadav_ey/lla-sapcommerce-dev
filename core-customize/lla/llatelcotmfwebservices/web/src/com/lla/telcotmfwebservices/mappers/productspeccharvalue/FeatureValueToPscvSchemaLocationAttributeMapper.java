/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcotmfwebservices.mappers.productspeccharvalue;

import com.lla.telcotmfwebservices.v2.dto.ProductSpecCharacteristicValue;
import com.lla.telcotmfwebservices.constants.LlatelcotmfwebservicesConstants;
import de.hybris.platform.b2ctelcofacades.mappers.TmaAttributeMapper;
import de.hybris.platform.commercefacades.product.data.FeatureValueData;
import ma.glasnost.orika.MappingContext;


/**
 * This attribute Mapper class maps data for schema location attribute between {@link FeatureValueData} and
 * {@link ProductSpecCharacteristicValue}
 *
 * @since 1903
 */
public class FeatureValueToPscvSchemaLocationAttributeMapper
		extends TmaAttributeMapper<FeatureValueData, ProductSpecCharacteristicValue>
{

	@Override
	public void populateTargetAttributeFromSource(FeatureValueData productSpecCharacteristicValueData,
			ProductSpecCharacteristicValue productSpecCharacteristicValue, MappingContext context)
	{
		productSpecCharacteristicValue.setAtschemaLocation(LlatelcotmfwebservicesConstants.TMA_API_SCHEMA);
	}
}
