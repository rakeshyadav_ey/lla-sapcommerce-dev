ACC.checkoutTelco = {
bindCheckO :function ()
{
  var cartEntriesError = false;

  // Alternative checkout flows options
  $('.doFlowSelectedChange').change(function ()
  {
    if ('multistep-pci' == $('#selectAltCheckoutFlow').val())
    {
      $('#selectPciOption').show();
    }
    else
    {
      $('#selectPciOption').hide();

    }
  });
  
   $(".address_province").change(function(){
     var previousNode = $(this).val();
     var nextNodeVal="Canton";

     $.ajax({
         type: "GET",
         data: {prevNode: previousNode, nextNode:nextNodeVal},
         url: ACC.config.encodedContextPath + "/checkout/multi/delivery-address/getAddressFields",
         contentType:'json',
         success: function(data)
         {
            $(".address_canton").html("");
            $(".address_district").html("");
            $(".address_neighbourhood").html("");
            list = '<option value="">'+$("#selectDefaultText").val()+'</option>';
            $(".address_canton").append(list);
            $(data).each(function(i,val)
            {
            list = '<option value="'+val.code+'">'+val.name+'</option>';
              $(".address_canton").append(list);
            });

            $(".address_canton").find("option:first-child").prop('disabled', true);
            $(".address_canton").val($(".address_canton").find('option').first().val());
            list = '<option value="">'+$("#selectDefaultText").val()+'</option>';
            $(".address_district").append(list);
            $(".address_district").find("option:first-child").prop('disabled', true);
            $(".address_neighbourhood").append(list);
            $(".address_neighbourhood").find("option:first-child").prop('disabled', true);
         }
     });
  });

  $(".address_canton").change(function(){
      var previousNode = $(this).val();
      var nextNodeVal="District";

      var add_canton_prev = $('.address_canton option:selected').val();       
        
      var prevNodeFinal;
      if(previousNode != null || previousNode != ''){
        prevNodeFinal =  previousNode;
      }
      else if(add_canton_prev != undefined || add_canton_prev != ''){
        prevNodeFinal =  add_canton_prev;
      }

       $.ajax({
           type: "GET",
           data: {prevNode: prevNodeFinal, nextNode:nextNodeVal},
           url: ACC.config.encodedContextPath + "/checkout/multi/delivery-address/getAddressFields",
           success: function(data)
           {
              $(".address_district").html("");
              $(".address_neighbourhood").html("");
              list = '<option value="">'+$("#selectDefaultText").val()+'</option>';
              $(".address_district").append(list);
              $(data).each(function(i,val)
              {
                list = '<option value="'+val.code+'">'+val.name+'</option>';
                $(".address_district").append(list);
              });

              $(".address_district").find("option:first-child").prop('disabled', true);
              $(".address_district").val($(".address_district").find('option').first().val());
              list = '<option value="">'+$("#selectDefaultText").val()+'</option>';
              $(".address_neighbourhood").append(list);
              $(".address_neighbourhood").find("option:first-child").prop('disabled', true);
           }
       });
  });

  $(".address_district").change(function(){
      var previousNode = $(this).val();
      var nextNodeVal="NeighbourHood";

      var add_district_prev = $('.address_district option:selected').val();
        
      var prevNodeFinal;
      if(previousNode != null || previousNode != ''){
        prevNodeFinal =  previousNode;
      }
      else if(add_district_prev != undefined || add_district_prev != ''){
        prevNodeFinal =  add_district_prev;
      }

      $.ajax({
          type: "GET",
           data: {prevNode: prevNodeFinal, nextNode:nextNodeVal},
          url: ACC.config.encodedContextPath + "/checkout/multi/delivery-address/getAddressFields",
          success: function(data)
          {
            $(".address_neighbourhood").html("");
            list = '<option value="">'+$("#selectDefaultText").val()+'</option>';
            $(".address_neighbourhood").append(list);
            $(data).each(function(i,val)
            {
              list = '<option value="'+val.code+'">'+val.name+'</option>';
              $(".address_neighbourhood").append(list);
            });

            $(".address_neighbourhood").find("option:first-child").prop('disabled', true);
            $(".address_neighbourhood").val($(".address_neighbourhood").find('option').first().val());
          }
      });
   });


  $('.js-continue-shopping-button').click(function ()
  {
    var checkoutUrl = $(this).data("continueShoppingUrl");
    window.location = checkoutUrl;
  });

  $('.js-create-quote-button').click(function ()
  {
    $(this).prop("disabled", true);
    var createQuoteUrl = $(this).data("createQuoteUrl");
    window.location = createQuoteUrl;
  });


  $('.expressCheckoutButton').click(function()
      {
        document.getElementById("expressCheckoutCheckbox").checked = true;
  });

  $(document).on("input",".confirmGuestEmail,.guestEmail",function(){

      var orginalEmail = $(".guestEmail").val();
      var confirmationEmail = $(".confirmGuestEmail").val();

      if(orginalEmail === confirmationEmail){
        $(".guestCheckoutBtn").removeAttr("disabled");
      }else{
         $(".guestCheckoutBtn").attr("disabled","disabled");
      }
  });

  $('.js-continue-checkout-button').click(function ()
  {
    var checkoutUrl = $(this).data("checkoutUrl");



    if($( "#invalid-bundle-wizard" ).hasClass( "invalid-bundle-wizard" )){
      ACC.cartTelco.showCartError();
      return false;
    }

    cartEntriesError = ACC.pickupinstore.validatePickupinStoreCartEntires();
    if (!cartEntriesError)
    {
      var expressCheckoutObject = $('.express-checkout-checkbox');
      if(expressCheckoutObject.is(":checked"))
      {
        window.location = expressCheckoutObject.data("expressCheckoutUrl");
      }
      else
      {
                 var flow = $('#selectAltCheckoutFlow').val();
                 var userNotInSession = "false";
                  $.ajax({
                    url: "cart/validateUserSession",
                    type: 'GET',
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    async: false,
                    success: function (result) {
                      userNotInSession = result;
                     }
                  });
                    if ( (flow == undefined || flow == '' || flow == 'select-checkout') && userNotInSession == "true" )
                    {
                        var popupHeadLine = $("#login-popup").prev(".headline").html();
                        $.colorbox({
                            inline:true,
                            href: "#login-popup",
                            scrolling:true,
                            maxWidth:"100%",
                            width:"600px",
                          //  href: $(this).attr("href"),
                            close:'<span class="close-popup"></span>',
                            title: popupHeadLine,
                            onComplete: function() {
                                $("#cboxTitle").css({
                                    "padding": "0"
                                });
                                $("#cboxLoadedContent").css({
                                    "margin-top": "50px"
                                });
                                ACC.common.refreshScreenReaderBuffer();
                            },
                            onClosed: function() {
                                ACC.common.refreshScreenReaderBuffer();
                            }
                        });
                        $(".login-popup-close").on("click", function() {
                            $.colorbox.close();
                        })
                    }
                    else
                    {
                      window.location = checkoutUrl;
                    }
      }
    }

    return false;
  });

}
};
ACC.checkout.bindCheckO = ACC.checkoutTelco.bindCheckO;

$(document).ready(function () {
    //disabling the dropdown cabletica checkout page - paso step2
    $(".checkout-shipping .address_province").prop("disabled", true);
    $(".checkout-shipping .address_canton").prop("disabled", true);
    $(".checkout-shipping .address_district").prop("disabled", true);
    $(".checkout-shipping .address_neighbourhood").prop("disabled", true);
    $(".checkout-shipping [name='landMark']").prop("disabled", true);
    
    if($(".address_province").prop("selectedIndex") <= 0){
      //console.log('provience is blank');
      $(".address_canton").val($(".address_canton").find('option').first().val());
      $(".address_district").val($(".address_district").find('option').first().val());
      $(".address_neighbourhood").val($(".address_neighbourhood").find('option').first().val());
    }
    else{
      //console.log('prov is not blank');
      var add_provience = $('.address_province option:selected').val(); 
      var add_canton = $('.address_canton option:selected').val();
      var add_district = $('.address_district option:selected').val();
      var add_neighbourhood = $('.address_neighbourhood option:selected').val();  
    }

    //onload load the dropdown with values and selected option
    function cantonDropdown(callback) { 
      $(".address_canton").html("");
      $.ajax({
        type: "GET",
        data: {prevNode: add_provience, nextNode:"Canton"},
        url: ACC.config.encodedContextPath + "/checkout/multi/delivery-address/getAddressFields",
        success: function(data)
        {
          list = '<option value="">'+$("#selectDefaultText").val()+'</option>';
          $(".address_canton").append(list);

          $(data).each(function(i,val)
          {
            list = '<option value="'+val.code+'">'+val.name+'</option>';
            $(".address_canton").append(list);
          });
          $(".address_canton").val(add_canton);
          $(".address_canton").find("option:first-child").prop('disabled', true);
          callback();
        }
      });
    }

    function districtDropdown(callback) { 
      $(".address_district").html("");
      $.ajax({
        type: "GET",
        data: {prevNode: add_canton, nextNode:"District"},
        url: ACC.config.encodedContextPath + "/checkout/multi/delivery-address/getAddressFields",
        success: function(data)
        {
          list = '<option value="">'+$("#selectDefaultText").val()+'</option>';
          $(".address_district").append(list);

          $(data).each(function(i,val)
          {
            list = '<option value="'+val.code+'">'+val.name+'</option>';
            $(".address_district").append(list);
          });
          $(".address_district").val(add_district);
          $(".address_district").find("option:first-child").prop('disabled', true);
          callback();
        }
      });
    }

    function neighbourhoodDropdown() { 
      $(".address_neighbourhood").html("");
      $.ajax({
        type: "GET",
        data: {prevNode: add_district, nextNode:"NeighbourHood"},
        url: ACC.config.encodedContextPath + "/checkout/multi/delivery-address/getAddressFields",
        success: function(data)
        {
          list = '<option value="">'+$("#selectDefaultText").val()+'</option>';
          $(".address_neighbourhood").append(list);

          $(data).each(function(i,val)
          {
            list = '<option value="'+val.code+'">'+val.name+'</option>';
            $(".address_neighbourhood").append(list);
          });
          $(".address_neighbourhood").val(add_neighbourhood);
          $(".address_neighbourhood").find("option:first-child").prop('disabled', true);
        }
      });
    }

    //on checkout page checking if the dropdowns are filled
    // if yes then loading the dropdowns with seleted option
    if( add_provience && add_canton && add_district && add_neighbourhood )  {   
      cantonDropdown(function () {
          districtDropdown(function (){
            neighbourhoodDropdown();
        });
      }); 
    }


    if($(".page-cartPage").length > 0) {
        $('#guestEmail').val('');
        $('#mobilePhone').val('');
        if (typeof ACC.cart.getCartSessionData === "function") {
            ACC.cart.getCartSessionData();
        }
        $('#documentNumber').val('');
		$('#guestName').val('');
    }

    if ($("#guestCheckout").length) {
        $("#guestCheckout").on('keypress',"input[name='mobilePhone']", function(e){
            const pattern = /^[0-9]$/;
            return pattern.test(e.key )
        });
        var guestEmail_error = $("#guestEmail_error").val();
        var guestPhone_error = $("#guestPhone_error").val();
        $.validator.addMethod("regx", function(value, element, regexpr) {
            if( regexpr.test(value)){
              $(".guestCheckoutButton ").removeAttr("disabled");
              return true;
            }
            else{
              $(".guestCheckoutButton ").attr('disabled', 'disabled');
              return false;
            }
        }, guestEmail_error);
        $("#guestCheckout").validate({
            rules: {
                "guestEmail": {
                    required: true,
                    regx: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                },
                "mobilePhone": {
                    required: true,
                }
            },
            messages: {
                guestEmail: guestEmail_error,
                mobilePhone: guestPhone_error
            },
      invalidHandler: function() {
                if (typeof googleAnalyticsTagManager.loginFormGtmError === "function") {
                    googleAnalyticsTagManager.loginFormGtmError();
                }
            }
        });
    }
    
    //Guest checkout for button GO press functionality    
    $('.page-cartPage').on('click', '.guestCheckoutButton ', function(event) {
        guestLoginValidate();
    });
    
    // option for user to select the postpaid type and device payment
    var planTypeValue;
    var devicePaymentOptionValue;
    $("#guestCheckout input[type='radio']").on("change", function() {
      if($("input[name='planType']").length > 0) {
        planTypeValue = $('input[name=planType]:checked', '#guestCheckout').val(); 
      }
      if($("input[name='devicePaymentOption']").length > 0) {
        devicePaymentOptionValue = $('input[name=devicePaymentOption]:checked', '#guestCheckout').val();
      }
    })
    if($("input[name='planType']").length > 0) {
      planTypeValue = $('input[name=planType]:checked', '#guestCheckout').val(); 
    }
    if($("input[name='devicePaymentOption']").length > 0) {
      devicePaymentOptionValue = $('input[name=devicePaymentOption]:checked', '#guestCheckout').val();
    }
    
    //Guest checkout for Input Enter press functionality    
    if(!($("#guestCheckout #mobilePhone").length)) {
        $('#guestCheckout #guestEmail').on('keypress change paste', function(e) {
            if($("#guestCheckout").valid()){
                $(".guestCheckoutButton ").removeAttr("disabled");
            }
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                guestLoginValidate();
            }
        });
    }

    function guestLoginValidate(){
        if($("#guestCheckout").valid()){
            var guestEmail = $("#guestEmail").val();
            var mobilePhone = $("#mobilePhone").val();
            if(mobilePhone==undefined){
             mobilePhone="";
            }
            if(planTypeValue==undefined){
              planTypeValue="";
            }
            if(devicePaymentOptionValue==undefined){
              devicePaymentOptionValue="";
            }
            $.ajax({
                url: ACC.config.encodedContextPath + '/login/checkout/guest',
                async: true,
                type: "POST",
                dataType: "html",
                data: {
                    guestEmail: guestEmail,
                    mobilePhone:mobilePhone,
                    planType: planTypeValue,
                    devicePaymentOption: devicePaymentOptionValue 
                },
                success:function(data) {
                       console.log('saved successfully');
                       window.location=ACC.config.encodedContextPath + '/checkout';
               },
               error:function(data,status,xhr) {
                   console.log('error occured');
               }
            });
        }
    }

});
