ACC.productshowall = {
		showproducts: function (){
			var activeTab = sessionStorage.getItem('activeTab');
			if (activeTab == 'show_grid' || activeTab === null ) {
				$('a.btn-grid.glyphicon-th-large,.prod_grid_view').parent().addClass("active");    
				$('a.btn-grid.glyphicon-th-list').parent().removeClass("active");
			}    
			else { 
				  $('a.btn-grid.glyphicon-th-list,.prod_list_view').parent().addClass("active");    
				  $('a.btn-grid.glyphicon-th-large').parent().removeClass("active");    
			}  
			$('a.btn-grid.glyphicon-th-large').click(function(){
				sessionStorage.setItem('activeTab', $(this).attr('name'));
				$('a.btn-grid.glyphicon-th-large,.prod_grid_view').parent().addClass("active");
				$('a.btn-grid.glyphicon-th-list,.prod_list_view').parent().removeClass("active");   
				ACC.productoffers.setgridheight();
			});
			$('a.btn-grid.glyphicon-th-list').click(function(){
				sessionStorage.setItem('activeTab', $(this).attr('name'));
				$('a.btn-grid.glyphicon-th-list,.prod_list_view').parent().addClass("active");
				$('a.btn-grid.glyphicon-th-large,.prod_grid_view').parent().removeClass("active");
			});
		}
}
ACC.addToCartPrepaidPopUp = {
		showaddtocartpopup: function() {
            $(document).on("click", ".prepaid-addcart-btn", function(e) {
                e.preventDefault();
                $("#prepaid-cta-popup #addToCartFormPrePaidPlp .productCodePost").val($(this).attr('data-attr-item-code')); //Item code
                $("#prepaid-cta-popup .prepaidplanInfoTc .tc").empty();
                $("#prepaid-cta-popup .prepaidplanInfoTc .tc").text($(this).attr('data-arrt-termsNcond')); //Terms and Condition data
                var htmlDataa = $(this).parent().parent().html();
                $("#prepaid-cta-popup .livemore-section .livemore-col").html(htmlDataa);
                $("#prepaid-cta-popup .livemore-section .livemore-col .cta-btn-block").remove();
                prePaidPlanAddToCart();
            });
		}
};

ACC.disableDeviceAddToCartButton = {
		disableButton: function() {
            $(document).on("click",".js-mini-cart-close-button", function(e){
                if($('body').hasClass('page-postpaid')){
                    $('.device-livemore-section .add-cart-btn').prop("disabled",true);
                    $('.device-livemore-section .tooltiptext').removeClass('hide');
                    $('.device-livemore-section .features-link').removeClass('disabled');
                }
                if($('body').hasClass('page-productDetails')){
                    $('.livemore-section .product__listing .add-cart-btn').prop("disabled", true);;
                    $('.livemore-section .product__listing .tooltiptext').removeClass('hide');
                }
            });
		}
};

function postPaidPlanAddToCart(buttonType, postpaidcode){
    var formData = $("#addToCartFormPostPaidPlp").serialize();
    if(buttonType == 'yes') {
        $.colorbox.close();
        // logic to go to devices tab when user cliks on yes option in the popup
        $('.js-tab-head[data-content-id="devices"]').trigger('click');
        $(window).scrollTop($('.tabs-container').offset().top);
        $(".devices-plp .prod-btn-info a").hide();
        $(".devices-plp .prod-btn-info form").show();
        $(".postpaid-only").show();
        $(".devices-only").hide();
        $('.postpaid-only').removeClass('active');
        $('.postpaid-only.second').addClass('active');
        $('.devices-plp #addToCartForm #plan').val(postpaidcode);
        // to show device price associated with the postpaid plan
	$(".planFirst").removeClass("hide");
        $(".deviceFirst").addClass("hide");
        $(".actual-price").addClass("hide");
        $(".actual-price." + postpaidcode).removeClass("hide");
        // to show device price if there is no plan associated
        $(".product-price").each(function() {
            var _loopthis = $(this);
            var priceLength = _loopthis.find(".actual-price").length;
            var priceLengthHide = _loopthis.find(".actual-price.hide").length;
            if(priceLength == priceLengthHide) {
                _loopthis.find(".p-code").first().addClass("hide");
                _loopthis.find(".no-offer-price").removeClass("hide").addClass("actual-price");
            }
        });
    }
    else {
        $.ajax({
            url: ACC.config.encodedContextPath + "/cart/add",
            data: formData,
            async: false,
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                ACC.minicart.updateMiniCartDisplay();
                var url = ACC.config.encodedContextPath + "/cart";
                window.location.href = url;
            },
            failure: function(error){
                console.error("error: " + error);
            }
        });
    }
}

function prePaidPlanAddToCart(){
    $.ajax({
        url: ACC.config.encodedContextPath + "/cart/add",
        data: $("#addToCartFormPrePaidPlp").serialize(),
        async: false,
        type: 'POST',
        dataType: 'json',
        success: function(data) {
            ACC.minicart.updateMiniCartDisplay();
            ACC.common.refreshScreenReaderBuffer();
            var url = ACC.config.encodedContextPath + "/cart";
            window.location.href = url;
        },
        error:function (xhr, ajaxOptions, thrownError){
            console.error("error: " + thrownError);
        }
    });
}

ACC.channelDataList={
    showChannelList: function(){
        $(".see-channels-list").on("click", function(e) {
            e.preventDefault();
            var _parentThis = $(this);
            if($(".page-cartPage").length) {
                var itemName = $(this).parents(".cart-item-list-details").find(".item__name").text();
                var itemCode = $(this).parents(".cart-item-list-details").find(".item__name").data("productcode");
                var noOfChannels = $(this).parents(".cart-item-list-details").find(".item__name").data("noofchannels");
            }
            if($(".page-multiStepCheckoutSummaryPage").length) {
                var itemName = $(this).parents(".checkout-order-summary-list-items").find(".name").data("productname");
                var itemCode = $(this).parents(".checkout-order-summary-list-items").find(".name").data("productcode");
                var noOfChannels = $(this).parents(".checkout-order-summary-list-items").find(".name").data("noofchannels");
            }
            if($(".page-bundle").length) {
                var itemName = $(this).parents(".cta-card").data("productname");
                var itemCode = $(this).parents(".cta-card").data("productcode");
                var noOfChannels = $(this).parents(".cta-card").data("noofchannels");
            }
            if($(".page-doItBundlePage").length) {
                var itemName = $(this).parents(".cta-card").data("productname");
                var itemCode = $(this).parents(".cta-card").data("productcode");
                var noOfChannels = $(this).parents(".cta-card").data("noofchannels");
            }
            var url  = ACC.config.encodedContextPath + '/configureProduct/getChannelData';
            $.ajax({
                type:"GET",
                url:url,
                data:{ itemCode:itemCode },
                success:function(data) {
                    var jsonObj=JSON.stringify(data);
                    var mapData=JSON.parse(jsonObj);
		    $(".channels-list").empty();
                    $("#channels-popup").prev(".headline").find(".bundleName").text(itemName);
                    $("#channels-popup").prev(".headline").find(".no-of-channels").find("b").text(noOfChannels);
                    $.each( mapData, function( key, value ) {
                        var data = '';
                        data = "<div class='channels_listbox'><h3 class='channels-title'>"+key+"</h3><ul class='channels__list' data-islist='true'>";
                        $.each( value, function( key1, value1 ) {
                           data = data + "<li> <span class='channel_no'>"+value1.channelNumber+"</span>  <span class='channel_name'>"+ value1.channelName+"</span></li>";
                        });
                        data = data + "</ul></div>";
                        $(".channels-list").append(data);
                    });
                    var popupHeadLine = $("#channels-popup").prev(".headline").html();
                    $.colorbox({
                        inline:true,
                        scrolling:true,
                        maxWidth:"100%",
                        width:"500px",
                        height: "100%",
                        href: $(".see-channels-list").attr("href"),
                        title: popupHeadLine,
                        close:'<span class="channels-popup-close">&times;</span>',
                        onComplete: function() {
                            $("#cboxContent").css({
                                "border-radius": "10px"
                            });
                            ACC.common.refreshScreenReaderBuffer();
                        },
                        onClosed: function() {
                            ACC.common.refreshScreenReaderBuffer();
                        }
                    });
                    $(".channels_listbox").on("click", function () {
                        var _this =$(this).find(".channels__list");	
                        if(_this.attr("data-isList") == "true"){
                            _this.siblings(".channels-title").toggleClass("open");
                            _this.slideToggle(); 
                        }	
                    })
                    .on("click", ".channels__list", function (event) {
                        event.stopPropagation();
                    });
                },
                error: function (request, status, error) {
                    alert(request.responseText);
                }
            });
            $(".js-channels-btn").on("click", function() {
                $.colorbox.close();
                _parentThis.parents(".cta-card").find(".ctabox-btn button").trigger("click");
            })
            
        });
    }
}

$(document).ready(function (){
	ACC.productshowall.showproducts();	
	ACC.addToCartPrepaidPopUp.showaddtocartpopup();
	ACC.disableDeviceAddToCartButton.disableButton();
	ACC.channelDataList.showChannelList();
});

$(document).ready(function() {
	$(".more-benefits #more").on('click', function() {
		$(this).addClass("hide");
		$(this).siblings("#less").removeClass("hide");
		$(this).parents(".more-benefits").find(".benefits-list").removeClass("hide");
	});
	$(".more-benefits #less").on('click', function() {
		$(this).addClass("hide");
		$(this).siblings("#more").removeClass("hide");
		$(this).parent(".more-benefits").find(".benefits-list").addClass("hide");
	});
	var plpCtaboxMaxHeight = 0;
        $(".page-bundle .ctabox").each(function(){
	    if ($(this).height() > plpCtaboxMaxHeight) { plpCtaboxMaxHeight = $(this).height(); }
        });
        $(".page-bundle .ctabox").height(plpCtaboxMaxHeight);
});
