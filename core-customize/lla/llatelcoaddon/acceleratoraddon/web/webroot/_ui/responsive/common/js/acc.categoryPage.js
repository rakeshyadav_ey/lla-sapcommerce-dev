//search page load more products Ajax functionality for devices
    var buynowbtn = $("#buynow_text").data('localizedText');
    var featurelnk = $("#feature_text").data('localizedText');
    var prePostaid = $("#pre_postpaid_text").data('localizedText');

$('#show-more').click(function(){
    var loadedPages = parseInt($("#loadedPages").val());
    var categoryName = $(".active")[0].innerHTML.toLowerCase();

    $.ajax({
        url: ACC.config.encodedContextPath+"/data/"+categoryName+"?page="+loadedPages,
        cache: false,
        type: 'GET',
        dataType: 'json',
        success: function(jsonData){
            addNewProducts(this, jsonData);
            loadedPages +=1;
            $("#loadedPages").val(loadedPages);
            if(jsonData.length<12){
            	$('#show-more').hide();
            }
        },
        failure: function(error){
            console.error("error: "+error);
        }
    });
});

const addNewProducts = function(btn, data){
    data.forEach(function(prdt){
        prdt.url = ACC.config.encodedContextPath+prdt.url;
        let productGrid = '<li class="grid_view product__list--items col-lg-4 col-md-4 col-sm-4">';
        productGrid += '<div class="full-grid" onclick="location.href=\''+prdt.url+'\'">';
        productGrid += '<div class="prod-wrapper"> <div class="product-info"> <div class="prod-img"> <a class="product__list--thumb" href="'+prdt.url;
        if(prdt.images !== null && Object.keys(prdt.images).length && prdt.images !== "undefined"){
        	productGrid += '" title="BST-33"> <img src="'+prdt.images[0].url+'" alt="'+prdt.name+'" title="'+prdt.name+'" />';
        }else{
        	productGrid += '" title="BST-33"> <img src="#" alt="'+prdt.name+'" title="'+prdt.name+'" />';
        }
        productGrid += '</a></div><div class="prod-content">';
        productGrid += '<strong class="product__list--name prod-name" href="'+prdt.url+'" title="'+prdt.name+'">'+prdt.name+'</strong>';
        productGrid += '<div class="prod-data"><div class="product__listing--price"><!--Starting from-->';
        if(prdt.productOfferingPrices !== null && Object.keys(prdt.productOfferingPrices).length && prdt.productOfferingPrices !== "undefined"){
        	if(typeof Object.keys(prdt.productOfferingPrices[0].oneTimeChargeEntries) !== null && Object.keys(prdt.productOfferingPrices[0].oneTimeChargeEntries).length && prdt.productOfferingPrices.oneTimeChargeEntries !== "undefined"){
        		productGrid += '<div class="product-price prod-price">'+prdt.productOfferingPrices[0].oneTimeChargeEntries[0].price.formattedValue+' &#43 GCT';
        	}else{
            	productGrid += '<div class="product-price prod-price">';
            }
        }
        productGrid += '</div></div>';
        productGrid += '<div class="prod-plans">Pre and Post Paid</div>'
        productGrid += '</div></div></div>';
        productGrid += '<div class="price-details"><a class="btn btn-primary add-cart-btn" href=';
        productGrid += prdt.url+'>'+buynowbtn+'</a></div><a class="features-link" href=';
        productGrid += prdt.url+'>'+featurelnk+'</a>';
        productGrid += '</div></div></li>';
        $(".prod_grid_view").append(productGrid);
    });
    return ;
};

//search page load more products Ajax functionality
$('#show-more-search').click(function(){
    var loadedPagesSearch = parseInt($("#loadedPagesSearch").val());
    var text = (window.location.search);

    $.ajax({
        url: ACC.config.encodedContextPath+"/search/searchData/"+text+"&page="+loadedPagesSearch,
        cache: false,
        type: 'GET',
        dataType: 'json',
        success: function(jsonData){
        	addNewProductsSearch(this, jsonData);
            loadedPagesSearch +=1;
            $("#loadedPagesSearch").val(loadedPagesSearch);
            if(jsonData.length<12){
            	$('#show-more-search').hide();
            }
        },
        failure: function(error){
            console.error("error: "+error);
        }
    });
});

const addNewProductsSearch = function(btn, data){
    data.forEach(function(prdt){
        prdt.url = ACC.config.encodedContextPath+prdt.url;
        let productGrid = '<li class="grid_view product__list--items col-lg-4 col-md-4 col-sm-4">';
        productGrid += '<div class="full-grid" onclick="location.href=\''+prdt.url+'\'">';
        productGrid += '<div class="prod-wrapper"> <div class="product-info"> <div class="prod-img"> <a class="product__list--thumb" href="'+prdt.url;
        if(prdt.images !== null && Object.keys(prdt.images).length && prdt.images !== "undefined"){
        	productGrid += '" title="BST-33"> <img src="'+prdt.images[0].url+'" alt="'+prdt.name+'" title="'+prdt.name+'" />';
        }else{
        	productGrid += '" title="BST-33"> <img src="#" alt="'+prdt.name+'" title="'+prdt.name+'" />';
        }
        productGrid += '</a></div><div class="prod-content">';
        productGrid += '<strong class="product__list--name prod-name" href="'+prdt.url+'" title="'+prdt.name+'">'+prdt.name+'</strong>';
        productGrid += '<div class="prod-data"><div class="product__listing--price"><!--Starting from-->';
        if(prdt.productOfferingPrices !== null && Object.keys(prdt.productOfferingPrices).length && prdt.productOfferingPrices !== "undefined"){
        	if(prdt.productOfferingPrices[0].oneTimeChargeEntries !== null && Object.keys(prdt.productOfferingPrices[0].oneTimeChargeEntries).length && prdt.productOfferingPrices.oneTimeChargeEntries !== "undefined"){
        		productGrid += '<div class="product-price prod-price">'+prdt.productOfferingPrices[0].oneTimeChargeEntries[0].price.formattedValue+' &#43 GCT';
        	}else{
            	productGrid += '<div class="product-price prod-price">';
            }
        }
        productGrid += '</div></div>';
        if(prdt.allCategories !== null && prdt.allCategories.includes("devices")){
        	productGrid += '<div class="prod-plans">'+prePostaid+'</div>'
        }
        productGrid += '</div></div></div>';
        productGrid += '<div class="price-details"><a class="btn btn-primary add-cart-btn" href=';
        productGrid += prdt.url+'>'+buynowbtn+'</a></div><a class="features-link" href=';
        productGrid += prdt.url+'>'+featurelnk+'</a>';
        productGrid += '</div></div></li>';
        $(".prod_grid_view").append(productGrid);
    });
    return ;
};
