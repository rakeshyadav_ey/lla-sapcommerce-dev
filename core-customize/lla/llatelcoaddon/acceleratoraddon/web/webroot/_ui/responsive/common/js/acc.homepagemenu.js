ACC.homepagemenu = {
		menutoggle: function (){
		      $('#signedInUserOptionsToggle').click(function (){
		    	  $(this).toggleClass('show');
		    	  $(".offcanvasGroup1").slideToggle(400);
		          if ($(this).hasClass('show')){
		              $(this).find('span').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
		          }
		          else {
		                $(this).find('span').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
		          }
		      });
		},
		regionDropdown: function(){
		    $("#region").change(function(){
                            item = $("#region option:selected").text();
                            $.ajax({url:ACC.config.encodedContextPath+"/setRegion/"+item, async: false, type:'GET'});
                            location.reload();
                        });
		},
		selectedRegion: function(){
            $.ajax({url:ACC.config.encodedContextPath+"/getRegion", type:'GET',success: function(region){$("#region").val(region)}});
		}
};
$(document).ready(function (){    
	ACC.homepagemenu.menutoggle();
	ACC.homepagemenu.regionDropdown();
	ACC.homepagemenu.selectedRegion();
});