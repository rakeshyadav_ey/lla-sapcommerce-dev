ACC.silentorderpost = {

	spinner: $("<img src='" + ACC.config.commonResourcePath + "/images/spinner.gif' />"),

	bindUseDeliveryAddress: function ()
	{
		$('#useDeliveryAddress').on('change', function ()
		{
			if ($('#useDeliveryAddress').is(":checked"))
			{
				var options = {'countryIsoCode': $('#useDeliveryAddressData').data('countryisocode'), 'useDeliveryAddress': true};
				ACC.silentorderpost.enableAddressForm();
				ACC.silentorderpost.displayCreditCardAddressForm(options, ACC.silentorderpost.useDeliveryAddressSelected);
				ACC.silentorderpost.disableAddressForm();
			}
			else
			{
				ACC.silentorderpost.clearAddressForm();
				ACC.silentorderpost.enableAddressForm();
				$('#billingAddressForm').empty();
			}
		});

		if ($('#useDeliveryAddress').is(":checked"))
		{
			ACC.silentorderpost.disableAddressForm();
		}
	},

	bindUseDeliveryDataAddress: function ()
	{
		$('.useDeliveryAddress').on('change', function ()
		{
			if ($('.useDeliveryAddress').is(":checked"))
			{
				var options = {'countryIsoCode': $('#useDeliveryAddressData').data('countryisocode'), 'useDeliveryAddress': true};
				ACC.silentorderpost.enableAddressForm();
				ACC.silentorderpost.displayCreditCardAddressForm(options, ACC.silentorderpost.useDeliveryAddressSelected);
				ACC.silentorderpost.disableAddressForm();
				setTimeout(function(){ 
					var mapSelctedValue = $("#address_province_value").val();
					$(".address_province option").each(function(){
					    if($(this).text() == mapSelctedValue){
					        $(this).attr('selected', true);
					    }
					});
				 }, 300);
			}
			else
			{
				ACC.silentorderpost.clearAddressForm();
				ACC.silentorderpost.enableAddressForm();
				//$('#billingAddressForm').empty();
				$(".address_province").change(function(){
					var previousNode = $(this).val();
             		var nextNodeVal="Canton";

					$.ajax({
			             type: "GET",
			             data: {prevNode: previousNode, nextNode:nextNodeVal},
			             url: ACC.config.encodedContextPath + "/checkout/multi/delivery-address/getAddressFields",
			             contentType:'json',
			             success: function(data)
			             {
			               $(".address_canton").html("");
			                $(".address_district").html("");
			                  list = '<option value="">'+$("#selectDefaultText").val()+'</option>';
			                  $(".address_canton").append(list);
			               $(data).each(function(i,val)
			                {
			                  list = '<option value="'+val.code+'">'+val.name+'</option>';
			                  $(".address_canton").append(list);
			                 });
			
			                 $(".address_canton").find("option:first-child").prop('disabled', true);
			                 list = '<option value="">'+$("#selectDefaultText").val()+'</option>';
			                 $(".address_district").append(list);
			                 $(".address_district").find("option:first-child").prop('disabled', true);
			             }
	             	});
				});
				
				$(".address_canton").change(function(){
                   var previousNode = $(this).val();
                   var nextNodeVal="District";

                   $.ajax({
                       type: "GET",
                        data: {prevNode: previousNode, nextNode:nextNodeVal},
                       url: ACC.config.encodedContextPath + "/checkout/multi/delivery-address/getAddressFields",
                       success: function(data)
                       {
                           $(".address_district").html("");
                           list = '<option value="">'+$("#selectDefaultText").val()+'</option>';
                            $(".address_district").append(list);
                            $(data).each(function(i,val)
                             {
                               list = '<option value="'+val.code+'">'+val.name+'</option>';
                               $(".address_district").append(list);
                              });
                             $(".address_district").find("option:first-child").prop('disabled', true);
                             list = '<option value="">'+$("#selectDefaultText").val()+'</option>';
                       }
                   });
            });

			}
		});

		if ($('.useDeliveryAddress').is(":checked"))
		{
			ACC.silentorderpost.disableAddressForm();
		}
	},

	bindSubmitSilentOrderPostForm: function ()
	{
		$('.submit_silentOrderPostForm').click(function ()
		{
			ACC.common.blockFormAndShowProcessingMessage($(this));
			$('.billingAddressForm').filter(":hidden").remove();
			//ACC.silentorderpost.enableAddressForm();
			$('#silentOrderPostForm').submit();
		});
	},

	bindCycleFocusEvent: function ()
	{
		$('#lastInTheForm').blur(function ()
		{
			$('#silentOrderPostForm [tabindex$="10"]').focus();
		})
	},

	isEmpty: function (obj)
	{
		if (typeof obj == 'undefined' || obj === null || obj === '') return true;
		return false;
	},

	disableAddressForm: function ()
	{
		$('input[id^="address\\."]').prop('disabled', true);
		$('select[id^="address\\."]').prop('disabled', true);
	},

	enableAddressForm: function ()
	{
		$('input[id^="address\\."]').prop('disabled', false);
		$('select[id^="address\\."]').prop('disabled', false);
	},

	clearAddressForm: function ()
	{
		$('input[id^="address\\."]').val("");
		$('select[id^="address\\."]').val("");
	},

	useDeliveryAddressSelected: function ()
	{
		if ($('#useDeliveryAddress').is(":checked"))
		{
			$('#address\\.country').val($('#useDeliveryAddressData').data('countryisocode'));
			ACC.silentorderpost.disableAddressForm();
		}
		else
		{
			ACC.silentorderpost.clearAddressForm();
			ACC.silentorderpost.enableAddressForm();
		}
	},

	displayStartDateIssueNum: function ()
	{
		var cardType = $('#silentOrderPostForm [tabindex="1"]').val();
		if (cardType == '024' || cardType == 'switch')
		{
			$('#startDate').removeAttr('hidden');
			$('#issueNum').removeAttr('hidden');
		}
		else
		{
			$('#startDate').attr('hidden', 'true');
			$('#issueNum').attr('hidden', 'true');
		}
	},

	bindCreditCardAddressForm: function ()
	{
		$('#silentOrderPostForm #billingCountrySelector :input').on("change", function ()
		{
			var countrySelection = $(this).val();
			var options = {
				'countryIsoCode': countrySelection,
				'useDeliveryAddress': false
			};
			ACC.silentorderpost.displayCreditCardAddressForm(options);
		})
	},

	displayCreditCardAddressForm: function (options, callback)
	{
		$.ajax({
			url: ACC.config.encodedContextPath + '/checkout/multi/sop/billingaddressform',
			async: true,
			data: options,
			dataType: "html",
			beforeSend: function ()
			{
				$('#billingAddressForm').html(ACC.silentorderpost.spinner);
			}
		}).done(function (data)
				{
					$("#billingAddressForm").html($(data).html());
					if (typeof callback == 'function')
					{
						callback.call();
					}
				});
	},

	bindEditCreditCardAddressForm: function ()
	{
		$('#sopPaymentDetailsForm #billingCountrySelector :input').on("change", function ()
		{
			var countrySelection = $(this).val();
			var options = {
				'countryIsoCode': countrySelection
			};
			ACC.silentorderpost.displayEditCreditCardAddressForm(options);
		})
	},

	displayEditCreditCardAddressForm: function (options, callback)
	{
		$.ajax({
			url: ACC.config.encodedContextPath + '/my-account/my-payment-details/edit/billingaddressform',
			async: true,
			data: options,
			dataType: "html",
			beforeSend: function ()
			{
				$('#billingAddressForm').html(ACC.silentorderpost.spinner);
			}
		}).done(function (data)
				{
					$("#billingAddressForm").html($(data).html());
					if (typeof callback == 'function')
					{
						callback.call();
					}
				});
	}
}

$(document).ready(function ()
{
	with (ACC.silentorderpost)
	{
		$('#silentOrderPostForm [tabindex="1"]').change(function ()
		{
			displayStartDateIssueNum();
		});
		bindUseDeliveryAddress();
		bindUseDeliveryDataAddress();
		bindSubmitSilentOrderPostForm();
		bindCreditCardAddressForm();
		bindEditCreditCardAddressForm();
	}
	
});

$(window).on('load', function(){
	var mapSelctedValue = $("#address_province_value").val();
	$(".address_province option").each(function(){
	    if($(this).text() == mapSelctedValue){
	        $(this).attr('selected', true);
	    }
	});
});
