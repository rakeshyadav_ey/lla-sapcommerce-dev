<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<div class="contactus">
   <h4>
      <b class="text-uppercase">
         <spring:theme code="contactus.Questions" />
      </b>
      <p class="contact-subtxt"><spring:theme code="contactus.contactus" /></p>
   </h4>
   <ul>
      <li class="call">
         <c:choose>
            <c:when test="${not empty cartCalculationMessage}">
               <spring:theme code="contactus.call" />
               <b>
                  <spring:theme code="contactus.call.number" />
               </b>
            </c:when>
            <c:otherwise>
               <spring:theme code="contactus.call" />
               <b>
                  <spring:theme code="prepaid.contactus.call.number" />
               </b>
            </c:otherwise>
         </c:choose>
      </li>
      <li class="whatsapp">
         <c:choose>
            <c:when test="${not empty cartCalculationMessage}">
               <spring:theme code="contactus.via" />
               <spring:theme code="contactus.whatsapp.number" htmlEscape="false" />
            </c:when>
            <c:otherwise>
               <spring:theme code="contactus.via" />
               &nbsp;
               <b>
                  <spring:theme code="prepaid.contactus.whatsapp.number" />
               </b>
            </c:otherwise>
         </c:choose>
      </li>
   </ul>
</div>