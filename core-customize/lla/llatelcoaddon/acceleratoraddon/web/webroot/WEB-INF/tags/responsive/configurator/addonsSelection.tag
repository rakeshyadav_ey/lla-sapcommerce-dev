<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:url value="/addtocart" var="addAddon"/>
<c:url value="/cart/removeAddon" var="removeAddon"/>

<div class="configurator-body">			
				<div class="next-button finish-button">
<%-- 					<c:if test="${not empty bundleMessage}">					 --%>
						<form:form action="${addAddon}" method="get" id="addonSubmission">
							<input type="hidden" id="productCodePost_cart" name="productCodePost" value=""/>
							<input type="hidden" value="ACQUISITION" id="processType" name="processType" />                          
						</form:form>
						<form:form action="${removeAddon}" method="get" id="addonRemoveSubmission">
							<input type="hidden" id="productCodePost_cart_remove" name="productCodePost" value=""/>							         
						</form:form>
<%-- 					</c:if>									 --%>
				</div>					

    <input type="hidden" name="selecteddAddons" id="selecteddAddons" value=""/>
    <input type="hidden" value="${productConfigDTO.twoPAddonHD}" id="js-twoPAddonHD">
    <input type="hidden" value="${productConfigDTO.twoPAddonDVR}" id="js-twoPAddonHD">
    <input type="hidden" value="${productConfigDTO.twoPHUB}" id="js-twoPHUB">

<%-- 	<c:if test="${not empty bundleMessage}"> --%>
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="row">			
						<div class="step-4-cntnr">                        
                                <div class="addon_listbox hubTv">
                                <h3 class="s4-title addonChannel-head"><spring:theme code="basket.page.cart.tvaddon.head" text="Additional Boxes"/><span class="arrow_accord"></span></span></h3>
                                <div class="cl-cntnr hub" data-isList="${addonProducts.size() > 0}">                                     
                                    <ul class="cl-cntnr__list">
                            
								<c:forEach var="product" items="${addonProducts}">
									<li class="cl-cntnr__item tvHubBox">
										<div class="radio hubtv--js" data-hubtv-addon="${product.code}">
											<div class="form-group addOnRadioInput">
											<input type="hidden" id="productCodePost" name="productCodePost" value="${product.code}"/>
											<input type="hidden" value="ACQUISITION" id="processType" name="processType" />                         
												<input type="radio" name="hubTvaddon" value=""
													id="${product.code}" data-attr-name="${product.name}"
													data-attr-code="${product.code}"
													data-save-url="${addAddon}"
													data-remove-url="${removeAddon}">
													 <label for="${product.code}"><span class="cl-cntnr__item-name">${product.name}</span></label>
											</div>
										</div>
										<c:set var="itemPrice" value="0"/>
										<c:forEach var="priceOnetime" items="${product.europe1Prices}">
											<c:forEach var="priceData" items="${priceOnetime.recurringChargeEntries}">
												<span class="cl-cntnr__btn"><button class="btn btn-yellow xs-btn cstm-btn">${currentCurrency.symbol} ${priceData.price}</button></span>
												 <c:set var="itemPrice" value='${priceData.price}' />
											</c:forEach>
										</c:forEach>
										  <input type="hidden" class="addAddonToPlanGTM"
                                                   <c:if test="${product.description ne null}"> data-attr-name="${product.description}" </c:if>
                                                   <c:if test="${product.code ne null}"> data-attr-id="${product.code}" </c:if>
                                                   <c:if test="${itemPrice ne null}"> data-attr-price="${itemPrice}" </c:if>
                                                   <c:if test="${product.supercategories[0].name ne null}"> data-attr-category="${product.supercategories[0].name}" </c:if>
                                                   <c:if test="${product.supercategories[0].supercategories[0].name ne null}"> data-attr-primarycategory="${product.supercategories[0].supercategories[0].name}" </c:if>
                                                   <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
                                               >
									</li>
								</c:forEach>
								
							</ul> 
                                </div>
                                </div>                        
							<div class="addon_listbox ultimateTv">
							<h3 class="s4-title"><spring:theme code="configurator.step4.title.can.choose" /><span class="arrow_accord"></span></h3>
							<div class="cl-cntnr ulti" data-isList="${channelList.size() > 0}">							
								<ul class="cl-cntnr__list" data-cl-selected="${productConfigDTO.channelList}">
									<c:forEach var="product" items="${channelList}">
										<li class="cl-cntnr__item ${product.code}" data-cl-code="${product.code}">
										    <span class="addOnCheckBoxInput checkbox">
										        <form:form action="" method="post">
                                                    <input type="hidden" id="productCodePost_ulti" name="productCodePost" value="${product.code}"/>
                                                   	<input type="hidden" value="ACQUISITION" id="processType" name="processType" />
													<input type="checkbox" class="addAddonToPlanData"
														value="${product.code}" id="${product.code}"
														data-save-url="${addAddon}"
														data-remove-url="${removeAddon}">
													<label for="${product.code}"></label>
                                                </form:form>
                                            </span>
											<span class="cl-cntnr__item-name">${product.name}<span class="cl-cntnr__spcl">${product.remarks}</span> </span> 
											<c:set var="itemPrice" value="0"/>
											<c:forEach var="priceOnetime" items="${product.europe1Prices}">
												<c:forEach var="priceData" items="${priceOnetime.recurringChargeEntries}">
												<fmt:setLocale value="en_US" scope="session" />
													<span class="cl-cntnr__btn"><button class="btn btn-yellow xs-btn cstm-btn">${currentCurrency.symbol} <fmt:formatNumber value="${priceData.price}" pattern="#,##0.00" /></button></span>
													<c:set var="itemPrice" value='${priceData.price}' />
												</c:forEach>
											</c:forEach>
											<input type="hidden" class="addAddonToPlanGTM"
                                               <c:if test="${product.name ne null}"> data-attr-name="${product.name}" </c:if>
                                               <c:if test="${product.code ne null}"> data-attr-id="${product.code}" </c:if>
                                               <c:if test="${itemPrice ne null}"> data-attr-price="${itemPrice}" </c:if>
                                               <c:if test="${product.supercategories[0].name ne null}"> data-attr-category="${product.supercategories[0].name}" </c:if>
                                               <c:if test="${product.supercategories[0].supercategories[0].name ne null}"> data-attr-primarycategory="${product.supercategories[0].supercategories[0].name}" </c:if>
                                               <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
                                            >										
										</li>
									</c:forEach>
								</ul>
							</div>
							</div>
							<!-- smartProtect Start -->						
						<div class="addon_listbox smartProtect">
						<h3 class="s4-title "><spring:theme code="configurator.step4.smart.protect" /><span class="arrow_accord"></span></h3>						
						<div class="cl-cntnr sp" data-isList="${smartProducts.size() > 0}">
						<p class="sp-msg">
							<spring:theme code="configurator.step4.smart.protect.message" />
						</p>
							<ul class="cl-cntnr__list">
								<c:forEach var="product" items="${smartProducts}">
									<li class="cl-cntnr__item">
										<div class="radio smartp--js" data-smart-protect="${product.code}">
											<div class="form-group">
												<input type="radio" name="smartProducts" value=""
													id="${product.name}" data-attr-name="${product.name}"
													data-attr-code="${product.code}"
													data-save-url="${addAddon}"
													data-remove-url="${removeAddon}">
											    <label for="${product.name}"><span class="cl-cntnr__item-name">${product.name}</span></label>
											</div>
										</div>
										<c:set var="itemPrice" value="0"/>
										<c:forEach var="priceOnetime" items="${product.europe1Prices}">
											<c:forEach var="priceData" items="${priceOnetime.recurringChargeEntries}">
												<span class="cl-cntnr__btn"><button class="btn btn-yellow xs-btn cstm-btn">${currentCurrency.symbol} ${priceData.price}</button></span>
												<c:set var="itemPrice" value='${priceData.price}' />
											</c:forEach>
										</c:forEach>
										<input type="hidden" class="addSmartAddonToPlanGTM"
                                           <c:if test="${product.name ne null}"> data-attr-name="${product.name}" </c:if>
                                           <c:if test="${product.code ne null}"> data-attr-id="${product.code}" </c:if>
                                           <c:if test="${itemPrice ne null}"> data-attr-price="${itemPrice}" </c:if>
                                           <c:if test="${product.supercategories[0].name ne null}"> data-attr-category="${product.supercategories[0].name}" </c:if>
                                           <c:if test="${product.supercategories[0].supercategories[0].name ne null}"> data-attr-primarycategory="${product.supercategories[0].supercategories[0].name}" </c:if>
                                           <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
                                        >
									</li>
								</c:forEach>
							</ul>
							<p class="more-sp">
							<spring:theme code="configurator.step4.smart.protect.know.more" />
						</p>
						</div>
						
						</div>
					</div>				
				<!-- smartProtect End -->															
			</div>
		</div>
<%-- 	</c:if> --%>
</div>

<c:forEach items="${llaTmaProductDatas}" var="productData" varStatus="count" >
   <input type="hidden" class="selectedProducts"
       <c:if test="${productData.name ne null}"> data-attr-name="${productData.name}" </c:if>
       <c:if test="${productData.code ne null}"> data-attr-id="${productData.code}" </c:if>
       <c:if test="${productData.categories[0].name ne null}"> data-attr-category="${productData.categories[0].name}" </c:if>
       <c:if test="${productData.categories[0].parentCategoryName ne null}"> data-attr-primarycategory="${productData.categories[0].parentCategoryName}" </c:if>
   />
</c:forEach>