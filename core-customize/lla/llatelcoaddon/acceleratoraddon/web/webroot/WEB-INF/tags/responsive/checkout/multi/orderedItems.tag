<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="showDeliveryAddress" required="false" type="java.lang.Boolean"%>
<%@ attribute name="showPotentialPromotions" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="order-entry" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/entry"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<c:set var="hasDeliveryItems" value="${cartData.deliveryItemsQuantity > 0}" />
<c:set var="deliveryAddress" value="${cartData.deliveryAddress}" />
<c:set var="installationAddinstallationAddressress" value="${cartData.installationAddress}" />
<spring:url value="/plp/postpaid" var="postpaidPLPUrl"/>
<spring:url value="/plp/prepaid" var="prepaidPLPUrl"/>


	<c:forEach items="${orderData.entries}" var="entry" varStatus="loop">
		<spring:url value="${entry.product.url}" var="productUrl" htmlEscape="false" />
		<li class="checkout-order-summary-list-items">
			
			<div class="details">
				<div class="name" style="display:flow-root;">

					<c:choose>
						<c:when test="${entry.product.categories[0].code eq 'addon' || entry.product.categories[0].code eq 'postpaid' || entry.product.categories[0].code eq 'prepaid'}">
							<c:if
								test="${entry.product.categories[0].code eq 'postpaid'}">
								<span class="today-deposit"><spring:theme code="text.cart.items.deposit"/> - </span>
								<a href="${postpaidPLPUrl}">${ycommerce:encodeHTML(entry.product.name)}</a>
							</c:if>
							<c:if
                                test="${entry.product.categories[0].code eq 'prepaid'}">
                                <a href="${prepaidPLPUrl}">${ycommerce:encodeHTML(entry.product.name)}</a>
                            </c:if>
							<c:if test="${entry.product.categories[0].code eq 'addon'}">
								${ycommerce:encodeHTML(entry.product.name)}
							</c:if>
						</c:when>						
						<c:otherwise>
							<span class="today-deposit"><spring:theme code="text.cart.items.deposit"/> - </span>
							<span class="monthly-bundle"><spring:theme code="basket.page.bundle.text"/>&nbsp;</span>
							<b><a href="${productUrl}">${ycommerce:encodeHTML(entry.product.name)}</a></b>
						</c:otherwise>
					</c:choose>

				</div>
				<order-entry:variantDetails product="${entry.product}"/>
				
				<div>
					<c:forEach items="${entry.product.baseOptions}" var="option">
						<c:if test="${not empty option.selected and option.selected.url eq entry.product.url}">
							<c:forEach items="${option.selected.variantOptionQualifiers}" var="selectedOption">
								<div>${ycommerce:encodeHTML(selectedOption.name)}:${ycommerce:encodeHTML(selectedOption.value)}</div>
							</c:forEach>
						</c:if>
					</c:forEach>

					<div class="promotion">
						<c:if
							test="${ycommerce:doesPotentialPromotionExistForOrderEntryOrOrderEntryGroup(cartData, entry) && showPotentialPromotions}">
							<c:set var="promotions" value="${cartData.potentialProductPromotions}" />
						</c:if>
						<c:if test="${ycommerce:doesAppliedPromotionExistForOrderEntryOrOrderEntryGroup(cartData, entry)}">
							<c:set var="promotions" value="${cartData.appliedProductPromotions}" />
						</c:if>
                        <c:forEach items="${promotions}" var="promotion">
                            <c:if test="${not empty promotion.consumedEntries}">
                                <c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
                                    <c:if test="${ycommerce:isConsumedByEntry(consumedEntry,entry)}">
                                        <span class="promotion">${ycommerce:sanitizeHTML(promotion.description)}</span>
                                    </c:if>
                                </c:forEach>
                            </c:if>
                        </c:forEach>
					</div>
					<common:configurationInfos entry="${entry}" />
				</div>
				<spring:url value="/checkout/multi/getReadOnlyProductVariantMatrix" var="targetUrl" htmlEscape="false" />
				<grid:gridWrapper entry="${entry}" index="${loop.index}" styleClass="display-none" targetUrl="${targetUrl}" />
			</div>

			<div class="price">
				
					<c:forEach items="${entry.orderEntryPrices}" var="orderEntryPrice">
					  <c:set var="orderEntryMonthlyPrice" value="${orderEntryPrice.monthlyPrice}" />
						<c:if test="${orderEntryPrice.basePrice.value != null}">
							
								<!--<div>${ycommerce:encodeHTML(orderEntryPrice.billingTime.name)}:</div>-->
								<div class="actualPrice">
									<order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryPrice.totalPrice}"
										orderEntryBasePrice="${orderEntryPrice.basePrice}" orderEntryMonthlyPrice="${orderEntryMonthlyPrice}"  monthlyTotalPrice="${cartData.monthlyTotalPrice}"/>
								</div>
								<input type="hidden" class="productGtmData"
                                    <c:if test="${entry.product.name ne null}"> data-attr-name="${entry.product.name}" </c:if>
                                    <c:if test="${entry.product.code ne null}"> data-attr-id="${entry.product.code}" </c:if>
                                    <c:if test="${orderEntryPrice.totalPrice.value ne null}"> data-attr-price="${orderEntryPrice.totalPrice.value}" </c:if>
                                    <c:if test="${entry.product.categories[0].name ne null}"> data-attr-brand="${entry.product.categories[0].name}" </c:if>
                                    <c:if test="${entry.product.categories[0].name ne null}"> data-attr-category="${entry.product.categories[0].name}" </c:if>
                                    <c:if test="${entry.product.categories[0].parentCategoryName ne null}"> data-attr-category_2="${entry.product.categories[0].parentCategoryName}" </c:if>
                                    <c:if test="${entry.quantity ne null}"> data-attr-quantity="${entry.quantity}" </c:if>
                                    <c:if test="${cmsSite.uid eq 'panama' && entry.product.categories[0].code == 'devices'}">
                                        <c:if test="${entry.product.manufacturer ne null}"> data-attr-devicebrand="${entry.product.manufacturer}" </c:if>
                                    </c:if>
                                >
								<c:if test="${entry.fixedLineProduct eq 'true'}">
									<div class="fixedline-product">
										<spring:theme code="text.installation.waiveoff" /><br>
										<spring:theme code="text.security.deposit" />:&nbsp;<order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryPrice.totalPrice}"
										orderEntryBasePrice="${orderEntryPrice.basePrice}" />
									</div>
								</c:if>
						</c:if>
					</c:forEach>
				
			</div>
			
		</li>
	</c:forEach>