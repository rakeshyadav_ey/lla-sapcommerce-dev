<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ attribute name="orderEntry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ attribute name="consignmentEntry" required="false" type="de.hybris.platform.commercefacades.order.data.ConsignmentEntryData" %>
<%@ attribute name="showStock" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="order-entry" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/entry"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="shouldShowStock" value="${(empty showStock) ? true : showStock}" />

<div class="item__list--item orderConfProductContent">
	<div class="oImage">
        <order-entry:productImage orderEntry="${orderEntry}"/>
    </div>
    <div class="oDetails">
       	<order-entry:productDetails orderEntry="${orderEntry}" order="${order}" showStock="${shouldShowStock}" />

        <%-- <order-entry:quantity orderEntry="${orderEntry}" consignmentEntryQuantity="${consignmentEntry.quantity}"/>
        <order-entry:delivery orderEntry="${orderEntry}"/> --%>
    	<order-entry:price orderEntry="${orderEntry}" monthlyTotalPrice="${order.monthlyTotalPrice}"/>
    </div>
	<input type="hidden" class="productGtmDataOrderConfirmationPage"
       <c:if test="${orderEntry.product.name ne null}"> data-attr-name="${orderEntry.product.name}" data-attr-price2="${orderEntry.product.monthlyPrice.value}"</c:if>
        <c:if test="${orderEntry.product.name ne null}"> data-attr-id="${orderEntry.product.code}" </c:if>
        <c:if test="${orderEntry.totalPrice.value ne null}"> data-attr-price="${orderEntry.totalPrice.value}" </c:if>     
        <c:if test="${orderEntry.quantity ne null}"> data-attr-quantity="${orderEntry.quantity}" </c:if>
         <c:if test="${cmsSite.uid eq 'panama'}">
            <c:if test="${orderEntry.product.manufacturer ne null}"> data-attr-devicebrand="${orderEntry.product.manufacturer}" </c:if>
             <c:if test="${orderData.planTypes.code ne null}"> data-attr-planType="${orderData.planTypes.code}" </c:if>
			<c:if test="${orderData.devicePaymentOption.code ne null}"> data-attr-devicePaymentOption="${orderData.devicePaymentOption.code}" </c:if>
        </c:if> 
         <c:choose>
           <c:when test="${fn:contains(orderEntry.product.categories[0].code, 'brand') || fn:startsWith(orderEntry.product.categories[0].code, 'brand')}">
             <c:if test="${orderEntry.product.categories[1].name ne null}"> data-attr-brand="${orderEntry.product.categories[1].name}" data-attr-category="${orderEntry.product.categories[1].name}" </c:if>
             <c:if test="${orderEntry.product.categories[1].parentCategoryName ne null}"> data-attr-category_2="${orderEntry.product.categories[1].parentCategoryName}" </c:if>						     
		   </c:when>
		   <c:otherwise>
			<c:if test="${orderEntry.product.categories[0].name ne null}"> data-attr-brand="${orderEntry.product.categories[0].name}" data-attr-category="${orderEntry.product.categories[0].name}" </c:if>
            <c:if test="${orderEntry.product.categories[0].parentCategoryName ne null}"> data-attr-category_2="${orderEntry.product.categories[0].parentCategoryName}" </c:if>						   
	        </c:otherwise>
	     </c:choose>         
    >
</div>
