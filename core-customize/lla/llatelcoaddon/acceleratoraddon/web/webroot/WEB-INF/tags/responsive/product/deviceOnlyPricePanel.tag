<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="guidedselling" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/guidedselling"%>

<spring:htmlEscape defaultHtmlEscape="true"/>
<div class="device-only-panel">
   <div class="device-only-title">
      <h3><spring:theme code="product.deviceOnly" text="Device Only"/></h3>
   </div>
   <div class="device-only-body">
        <span class="device-only-price">
            <format:fromPrice priceData="${product.price}"/>
        </span>
      <hr/>
      <spring:theme code="product.deviceOnly.body" text="Carrier Free"/>
   </div>
   
   <guidedselling:addSpoToCartForm product="${product}" processType="DEVICE_ONLY"/>
</div>
