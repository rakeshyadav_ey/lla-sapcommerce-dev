<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="orderData" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />
	<c:forEach items="${orderData.entries}" var="entry" varStatus="loop">
		<c:if test="${entry.product.categories[0].code ne 'channelsaddoncabletica'}">
			<c:set var="productHeadline" value="true"/>
		</c:if>
	</c:forEach>
	<c:if test="${productHeadline}">
		<div class="headline">
			<spring:theme code="text.order.summary.headline" />
		</div>
	</c:if>	
	<ul class="checkout-order-summary-list">
		<c:forEach items="${orderData.entries}" var="entry" varStatus="loop">
			<c:if test="${entry.product.categories[0].code ne 'channelsaddoncabletica'}">
				<li class="checkout-order-summary-list-items">
					<div class="col-xs-4 col-sm-3">
						<div class="thumb">
							<product:productPrimaryImage product="${entry.product}" format="thumbnail" />
						</div>
					</div>
					<div class="col-xs-12 col-sm-9">
						<div class="details">
							<div class="name">
								${ycommerce:encodeHTML(entry.product.name)}
							</div>
							<div class="product-specifications">
								<c:set var="product" value="${entry.product}"/>
								${product.summary}
							</div>
						</div>
					</div>
				</li>
			</c:if>
			<input type="hidden" class="orderedProducts"
                <c:if test="${entry.product.name ne null}"> data-attr-name="${entry.product.name}" </c:if>
                <c:if test="${entry.product.code ne null}"> data-attr-id="${entry.product.code}" </c:if>
                <c:if test="${entry.product.monthlyPrice.value ne null}"> data-attr-price="${entry.product.monthlyPrice.value}" </c:if>
                <c:if test="${entry.product.categories[0].code ne null}"> data-attr-category="${entry.product.categories[0].code}" </c:if>
                <c:if test="${entry.product.categories[0].parentCategoryName ne null}"> data-attr-primarycategory="${entry.product.categories[0].parentCategoryName}" </c:if>
                <c:if test="${entry.quantity ne null}"> data-attr-quantity="${entry.quantity}" </c:if>
            />
		</c:forEach>
	</ul>

	<input type="hidden" id="addressInfo"
        <c:if test="${orderData.code ne null}"> data-attr-order-id="${fn:escapeXml(orderData.code)}" </c:if>
        <c:if test="${orderData.deliveryAddress.province ne null}"> data-attr-province="${orderData.deliveryAddress.province}" </c:if>
        <c:if test="${orderData.deliveryAddress.district ne null}"> data-attr-district="${orderData.deliveryAddress.district}" </c:if>
        <c:if test="${orderData.deliveryAddress.building ne null}"> data-attr-town="${orderData.deliveryAddress.building}" </c:if>
        <c:if test="${orderData.deliveryAddress.neighbourhood ne null}"> data-attr-neighbourhood="${orderData.deliveryAddress.neighbourhood}" </c:if>
        <c:if test="${orderData.deliveryAddress.landMark ne null}"> data-attr-street="${orderData.deliveryAddress.landMark}" </c:if>
        <c:if test="${orderData.deliveryAddress.country.name ne null}"> data-attr-country="${orderData.deliveryAddress.country.name}" </c:if>
    />

	<c:forEach items="${orderData.entries}" var="entry" varStatus="loop">
		<c:if test="${entry.product.categories[0].code eq 'channelsaddoncabletica'}">
			<c:set var="productHeadline" value="true"/>
		</c:if>
	</c:forEach>

	<ul class="checkout-order-summary-list">
		<c:forEach items="${orderData.entries}" var="entry" varStatus="loop">
			<c:if test="${entry.product.categories[0].code eq 'channelsaddoncabletica'}">
				<c:if test= "${loop.index eq 1}">
	           		<div class="headline">
	           			<spring:theme code="text.order.summary.headline.addons" />
	           		</div>
           		</c:if>
				<li class="checkout-order-summary-list-items">
					<div class="col-xs-4 col-sm-3">
						<div class="thumb">
							<product:productPrimaryImage product="${entry.product}" format="thumbnail" />
						</div>
					</div>
					<div class="col-xs-12 col-sm-9">
						<div class="details">
							<div class="name">
								<c:if test="${entry.product.code eq 'Convertidor_Digital'}">
                           				 ${entry.quantity}&nbsp; ${ycommerce:encodeHTML(entry.product.name)}
                         			 </c:if>
                         			<c:if test="${entry.product.code ne 'Convertidor_Digital'}">
                        			  ${ycommerce:encodeHTML(entry.product.name)}
                         			</c:if>
							</div>
							<div class="product-specifications">
								<c:set var="product" value="${entry.product}"/>
								${product.summary}
							</div>
						</div>
					</div>
				</li>
			</c:if>
		</c:forEach>
	</ul>
