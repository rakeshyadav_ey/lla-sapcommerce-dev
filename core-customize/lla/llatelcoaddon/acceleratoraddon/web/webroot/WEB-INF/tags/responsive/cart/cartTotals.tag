<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="showTaxEstimate" required="false" type="java.lang.Boolean"%>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/cart" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<c:if test="${cmsSite.uid eq 'jamaica'}">
	<h3 class="totals-head">Pay on Checkout</h3>
</c:if>
<c:forEach items="${cartData.orderPrices}" var="orderPrice">
    <c:if test="${orderPrice.monthlyPrice ne '' or orderPrice.monthlyPrice ne null}">
        <c:set var="orderMonthlyTotal" value="${orderPrice.monthlyPrice}"/>
    </c:if>
</c:forEach>
<c:if test="${cmsSite.uid eq 'puertorico'}">
    <c:set scope="page"  var="creditSelectedFromUser"  value="creditCheckSelected"/>
    <c:if test="${hideCreditCheck ne null && hideCreditCheck eq 'true'}">
      <c:set scope="page"  var="creditSelectedFromUser"  value="creditCheckDeSelected"/>
    </c:if>
	<c:choose>
	<c:when test="${pageType == 'ORDERCONFIRMATION' || pageType == 'ORDERDETAILS'}">
		    <h3 class="summary-head ${creditSelectedFromUser == 'creditCheckDeSelected' ? 'hide':''}">
		       <spring:theme code="text.items.order.total.head" text="Order detail" />
		    </h3>
		    <c:if test="${creditSelectedFromUser eq 'creditCheckDeSelected'}">
		        <h3 class="summary-head ${creditSelectedFromUser}"><spring:theme code="text.items.cart.total.head" text="Order detail" /></h3>
		    </c:if>
	</c:when>
	<c:otherwise>
		<h3 class="totals-head ${creditSelectedFromUser}">
		  <spring:theme code="text.items.cart.total.head"
		   text="Now in your Shopping Cart" />
		</h3>
	</c:otherwise>
	</c:choose>
</c:if>
<c:if test="${cmsSite.uid eq 'puertorico'}">
	<div class="cart-totalwrapper ${creditSelectedFromUser}">
</c:if>
<table id="order-totals" class="cart-total-summary ${creditSelectedFromUser}">
	<thead>
        <tr>
			<c:if test="${cmsSite.uid eq 'jamaica'}">
                <th><spring:theme code="basket.items" text="Items" /></th>
            <c:forEach items="${cartData.orderPrices}" var="orderPrice" >
                <th scope="col">${ycommerce:encodeHTML(orderPrice.billingTime.name)}</th>
            </c:forEach>
			</c:if>
			<c:if test="${cmsSite.uid eq 'panama'}">
                <th colspan="2">
                    <spring:theme code="text.panama.items.in.cart" text="Currently in your cart" /><br/>
                    <p>
                        <span class="impo-icon"></span>
                        <spring:theme code="text.panama.items.in.cart.subheading"/>
                    </p>
                </th>
            </c:if>
        </tr>
	</thead>
	<tbody>

	<c:if test="${cmsSite.uid ne 'puertorico' && cmsSite.uid ne 'cabletica'}">

		<tr>
			<td class="cart-bundle-package"><spring:theme code="basket.page.totals.subtotal" text="Subtotal"/></td>

				<td><format:price priceData="${orderMonthlyTotal}" /></td>

		</tr>

		<tr>
			<td class="cart-bundle-package"><spring:theme code="basket.page.totals.savings" text="Savings"/></td>
			<c:forEach items="${cartData.orderPrices}" var="orderPrice" >
				<td><format:price priceData="${orderPrice.totalDiscounts}" /></td>
			</c:forEach>
		</tr>

		<c:if test="${cmsSite.uid eq 'panama'}">
			<c:choose>
				<c:when test="${freeShipping eq true && freeInstalltion eq true}">
					<tr>
						<td class="cart-bundle-package"><spring:theme code="basket.page.totals.freeShippingAndInstallation" text="free Shipping And Installation"/></td>
						<td>${currentCurrency.symbol}0.00</td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:if test="${freeShipping eq true}">
						<tr>
							<td class="cart-bundle-package"><spring:theme code="basket.page.totals.freeShipping" text="free Shipping"/></td>
							<td>${currentCurrency.symbol}0.00</td>
						</tr>
					</c:if>
					<c:if test="${freeInstalltion eq true}">
						<tr>
							<td class="cart-bundle-package"><spring:theme code="basket.page.totals.freeInstalltion" text="free Installtion"/></td>
							<td>${currentCurrency.symbol}0.00</td>
						</tr>
					</c:if>
				</c:otherwise>
			</c:choose>
			<c:if test="${freeSim eq true}">
				<tr>
					<td class="cart-bundle-package"><spring:theme code="basket.page.totals.freeSim" text="free Sim"/></td>
					<td>${currentCurrency.symbol}0.00</td>
				</tr>
			</c:if>
		</c:if>

<!-- 		<tr> -->
<%-- 			<td class="cart-bundle-package"><spring:theme code="basket.page.totals.delivery" text="Delivery"/></td> --%>
<%-- 			<c:forEach items="${cartData.orderPrices}" var="orderPrice"> --%>
<%-- 				<td><format:price priceData="${orderPrice.deliveryCost}" displayFreeForZero="TRUE" /></td> --%>
<%-- 			</c:forEach> --%>
<!-- 		</tr> -->

		<tr>
			<td class="cart-bundle-package"><spring:theme code="basket.page.totals.grossTax.noArgs" text="Tax"/></td>
			<c:forEach items="${cartData.orderPrices}" var="orderPrice">
				<td><format:price priceData="${orderPrice.totalTax}" displayFreeForZero="FALSE" /></td>
			</c:forEach>
		</tr>
        <c:if test="${cmsSite.uid ne 'panama'}">

            <tr class="cartTotal">
                <td class="cart-bundle-package"><spring:theme code="basket.page.total.checkout" text="Total" /></td>
                <td><format:price priceData="${cartData.totalPriceWithTax}" /></td>
            </tr>
        </c:if>

        <c:if test="${cmsSite.uid eq 'panama'}">
            <!--<tr class="seprated-line">
                <td colspan="2">
                    <hr/>
                </td>
            </tr>-->

            <tr class="cartTotalMonthly">

                <td class="cart-bundle-package"><b><spring:theme code="basket.page.total.monthly" text="Total monthly payment (ITBMS no include)" /></b></td>

                <td><b><format:price priceData="${orderMonthlyTotal}" /></b></td>

            </tr>
        </c:if>
		<cart:taxExtimate cartData="${cartData}" showTaxEstimate="${showTaxEstimate}" />
		</c:if>

        <c:if test="${(cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'cabletica') && null != cartData}">
			<tr>

				<td class="cart-bundle-package">${cartData.bundleProductPriceLabel}
				<c:if test="${(cartData.selectedProductName ne null)}">
                        <ol>
                           <c:forEach items="${cartData.selectedProductName}" var="entry">
                                <li><c:out value="${entry}"/></li>
                              </c:forEach>
                          </ol>
                  </c:if>
				</td>
				<td>${currentCurrency.symbol}${cartData.bundlePrice.value}</td>
			</tr>
			<c:if test = "${cartData.addonProductsTotalPrice.value>0}">
			<tr>
				<td class="cart-bundle-package"><spring:theme code="basket.page.cart.additonalHD.monthInfo" text="Additional HD box / month" /></td>
				<td>${currentCurrency.symbol}${cartData.addonProductsTotalPrice.value}</td>
			</tr>
			</c:if>
			<c:if test = "${cartData.channelsAddonTotalPrice.value>0}">
			<tr>
				<td class="cart-bundle-package"><spring:theme code="basket.page.cart.additionalChannel.monthInfo" text="Additional Channels  / month" /></td>
				<td>${currentCurrency.symbol}${cartData.channelsAddonTotalPrice.value}</td>
			</tr>
			</c:if>
			<c:if test="${cartData.smartProtectPrice ne null && cartData.smartProtectPrice.value>0}">
			<tr>
                <td class="cart-bundle-package"><spring:theme code="basket.page.cart.smartProtect.monthInfo" text="SmartProtect" /></td>
                <td>${currentCurrency.symbol}${cartData.smartProtectPrice.value}</td>
            </tr>
            </c:if>
			<tr class="cartTotal">
	            <td class="cart-bundle-package"><spring:theme code="basket.page.total.cart" text="TOTAL MONTHLY PAYMENT" /></td>
	             <td>${currentCurrency.symbol}${fn:escapeXml(cartData.totalPrice.value)}</td>
        	</tr>
		</c:if>
		<c:if test="${(cmsSite.uid eq 'cabletica') && null != orderData}">
            <tr>
                <td class="cart-bundle-package">${orderData.bundleProductPriceLabel}
                <c:if test="${(orderData.selectedProductName ne null)}">
                            <ol>
                               <c:forEach items="${orderData.selectedProductName}" var="entry">
                                    <li><c:out value="${entry}"/></li>
                                  </c:forEach>
                              </ol>
                      </c:if>
                </td>
                <td class="packageTotal">${currentCurrency.symbol}${orderData.bundlePrice.value}</td>
            </tr>
            <c:if test="${orderData.addonProductsTotalPrice.value>0}">
            <tr>
                <td class="cart-bundle-package"><spring:theme code="basket.page.cart.additonalHD.monthInfo" text="Additional HD box / month" /></td>
                <td class="packageTotal">${currentCurrency.symbol}${orderData.addonProductsTotalPrice.value}</td>
            </tr>
            </c:if>
            <c:if test="${orderData.channelsAddonTotalPrice.value>0}">
            <tr>
                <td class="cart-bundle-package"><spring:theme code="basket.page.cart.additionalChannel.monthInfo" text="Additional Channels  / month" /></td>
                <td class="packageTotal">${currentCurrency.symbol}${orderData.channelsAddonTotalPrice.value}</td>
            </tr>
            </c:if>
            <c:if test="${orderData.smartProtectPrice ne null && orderData.smartProtectPrice.value>0}">
            <tr>
                <td class="cart-bundle-package"><spring:theme code="basket.page.cart.smartProtect.monthInfo" text="SmartProtect" /></td>
                <td class="packageTotal">${currentCurrency.symbol}${orderData.smartProtectPrice.value}</td>
            </tr>
            </c:if>
            <tr class="cartTotal">
                <td class="cart-bundle-package"><spring:theme code="basket.page.total.cart" text="TOTAL MONTHLY PAYMENT" /></td>
               <td>${currentCurrency.symbol}${fn:escapeXml(orderData.totalPrice.value)}</td>
            </tr>
        </c:if>

         <c:if test="${(cmsSite.uid eq 'puertorico' && fn:endsWith(requestScope['javax.servlet.forward.request_uri'], '/summary/view'))|| (cmsSite.uid eq 'puertorico' && fn:endsWith(requestScope['javax.servlet.forward.request_uri'], '/summary/placeOrder'))}">
        		<div class="cart-sum-box ${creditSelectedFromUser}">
                         <div class="sumbox">
                         <p class="price">${currentCurrency.symbol}${fn:escapeXml(cartData.totalPrice.value)}*</p>
                         <p class="desc"><spring:theme code="text.items.cart.total.head" text="Monthly Payment" /></p>
                        </div>
                        <div class="sumbox-border"></div>
                        <div class="sumbox">
                               <p class="price">${currentCurrency.symbol}${fn:escapeXml(cartData.amountDueToday)}*</p>
                                <p class="desc"><spring:theme code="text.cartData.payNow" text="Pay Today" /></p>
                        </div>
                         <div class="sumbox-border"></div>
                        <div class="sumbox">
                               <p class="price">${currentCurrency.symbol}${fn:escapeXml(cartData.amountDueFirstBill)}*</p>
                                <p class="desc"><spring:theme code="text.cartData.firstBill" text="First bill to pay" /></p>
                        </div>
                  </div>

                   <div class="addon_paybox ${creditSelectedFromUser}">
                      <h3 class="totals-head"><spring:theme code="text.items.cart.total.head" text="Now in your Shopping Cart"/>
                              <span class="arrow_accord"></span>
                              <span class="arrow_accordup hide"></span>
                      </h3>
                          <div class="paybox">
                              <div class="cart-totals-payBox">
                                <div class="price-list">
                                  <div class="cart-bundle-package">${cartData.bundleProductPriceLabel}
                                    <c:if test="${(cartData.selectedProductName ne null)}">
                                        <ol>
                                           <c:forEach items="${cartData.selectedProductName}" var="entry">
                                                <li><c:out value="${entry}"/></li>
                                              </c:forEach>
                                          </ol>
                                   </c:if>
                                 </div>
                                 <p class="priceValue">${currentCurrency.symbol}${cartData.bundlePrice.value}</p>
                          </div>

                        <c:if test="${(orderData ne null)}">
                          <c:set var="data" value="${orderData}"/>
                       </c:if>

                        <c:if test="${(cartData ne null)}">
                           <c:set var="data" value="${cartData}"/>
                        </c:if>
                          <c:if test="${data.addonProductsTotalPrice.value>0}">
                             <div class="price-list">
                                <p class="cart-bundle-package"><spring:theme code="basket.page.cart.additonalHD.monthInfo" text="Additional HD box / month" /></p>
                                <p class="priceValue">${currentCurrency.symbol}${data.addonProductsTotalPrice.value}</p>
                            </div>
                            </c:if>
                            <c:if test="${data.channelsAddonTotalPrice.value>0}">
                              <div class="price-list">
                                <p class="cart-bundle-package"><spring:theme code="basket.page.cart.additionalChannel.monthInfo" text="Additional Channels  / month" /></p>
                                <p class="priceValue">${currentCurrency.symbol}${data.channelsAddonTotalPrice.value}</p>
                            </div>
                            </c:if>
                             <c:if test="${data.smartProtectPrice ne null && data.smartProtectPrice.value>0}">
                             <div class="price-list">
                                <p class="cart-bundle-package"><spring:theme code="basket.page.cart.smartProtect.monthInfo" text="SmartProtect" /></p>
                                <p class="priceValue">${currentCurrency.symbol}${data.smartProtectPrice.value}</p>
                            </div>
                            </c:if>
                            <div class="cartTotal">
                                <p class="cart-bundle-package"><spring:theme code="basket.page.total.cart" text="TOTAL MONTHLY PAYMENT" /></p>
                                <p>${currentCurrency.symbol}${fn:escapeXml(cartData.totalPrice.value)}</p>
                            </div>
                       </div>
                       </div>
                       </div>
                        <div class="addon_paybox ${creditSelectedFromUser}">
                          <h3 class="totals-head"><spring:theme code="text.cartData.payNow" text="Now in your Shopping Cart"/>
                                      <span class="arrow_accord"></span>
                                      <span class="arrow_accordup hide"></span>
                         </h3>
                      <div class="paybox">
                     <div class="cart-totals-payBox">
                     <c:if test="${cartData.creditScoreLevel.upfrontPayment ne null || (fn:contains(cartData.creditScoreLevel.upfrontPayment, 'payment'))}">
                            <div class="price-list">
                                <p class="cart-bundle-package"><spring:theme code="text.items.cart.total.head" text="Monthly Payment" /></p>
                                <p class="priceValue">${currentCurrency.symbol}${cartData.totalPrice.value}</p>
                            </div>
                            </c:if>
                           <c:if test="${cartData.creditScoreLevel.installationFee ne null}">
                            <div class="price-list">
                                <p class="cart-bundle-package"><spring:theme code="text.cartData.installation" text="Installation" /></p>
                                <p class="priceValue">${currentCurrency.symbol}${cartData.creditScoreLevel.installationFee}</p>
                            </div>
                           </c:if>
                           <c:if test="${cartData.creditScoreLevel.dvrDeposit ne null}">
                            <div class="price-list">
                                <p class="cart-bundle-package"><spring:theme code="text.cartData.dvrDeposit" text="DVR Deposit" /></p>
                                <p class="priceValue">${currentCurrency.symbol}${cartData.creditScoreLevel.dvrDeposit}</p>
                            </div>
                           </c:if>
                            <div class="cartTotal">
                                <p class="cart-bundle-package"><spring:theme code="text.cartData.extimatePayToday" text="ESTIMATED TO PAY TODAY" /></p>
                                <p>${currentCurrency.symbol}${fn:escapeXml(cartData.amountDueToday)}</p>
                            </div>
                       </div>
                       </div>
                       </div>
                    <div class="addon_paybox ${creditSelectedFromUser}">
                           <h3 class="totals-head"><spring:theme code="text.cartData.payOnFirstbill" text="Now in your Shopping Cart"/>
                               <span class="arrow_accord"></span>
                               <span class="arrow_accordup hide"></span>
                          </h3>
                    <div class="paybox">
                     <div class="cart-totals-payBox">
                             <c:if test="${cartData.creditScoreLevel.upfrontPayment eq null || !(fn:contains(cartData.creditScoreLevel.upfrontPayment, 'payment'))}">
                            <div class="price-list">
                                <p class="cart-bundle-package"><spring:theme code="text.items.cart.total.head" text="Monthly Payment" /></p>
                                <p class="priceValue">${currentCurrency.symbol}${cartData.totalPrice.value}</p>
                            </div>
                            </c:if>
                             <c:if test="${cartData.creditScoreLevel.onetimeCharges ne null}">
                            <div class="price-list">
                                <p class="cart-bundle-package"><spring:theme code="text.cartData.onetimeCharges" text="One Time Charges(Installation Fee)" /></p>
                                <p class="priceValue">${currentCurrency.symbol}${cartData.creditScoreLevel.onetimeCharges}</p>

                            </div>
                            </c:if>
                            <c:if test="${cartData.creditScoreLevel.paperBilling ne null}">
                            <div class="price-list">
                                <p class="cart-bundle-package"><spring:theme code="text.cartData.paperBilling" text="Paper Billing" /></p>
                                <p class="priceValue">${currentCurrency.symbol}${cartData.creditScoreLevel.paperBilling}</p>
                            </div>
                            </c:if>
                            <div class="cartTotal">
                                <p class="cart-bundle-package"><spring:theme code="text.cartData.estimatedFirstPayment" text="ESTIMATED PAYMENT ON YOUR FIRST BILL" /></p>
                                <p>${currentCurrency.symbol}${fn:escapeXml(cartData.amountDueFirstBill)}</p>
                            </div>
                          </div>
                        </div>
                      </div>
                </c:if>
        </tbody>
</table>

<c:if test="${cmsSite.uid eq 'puertorico'}">
	</div>
</c:if>
<%--Order Summary Page --%>
  <c:if test="${cmsSite.uid eq 'puertorico' && null != orderData}">

      <div class="order-sum-box ${creditSelectedFromUser}">

         <div class="sumbox">
         <p class="price">${currentCurrency.symbol}${fn:escapeXml(orderData.totalPrice.value)}*</p>
         <p class="desc"><spring:theme code="text.items.cart.total.head" text="Monthly Payment" /></p>
        </div>
        <div class="sumbox-border"></div>
        <div class="sumbox">
                <p class="price">${currentCurrency.symbol}${fn:escapeXml(orderData.amountDueToday)}*</p>
                <p class="desc"><spring:theme code="text.orderData.payNow" text="Pay Today" /></p>
        </div>
         <div class="sumbox-border"></div>
        <div class="sumbox">
                <p class="price">${currentCurrency.symbol}${fn:escapeXml(orderData.amountDueFirstBill)}*</p>
                <p class="desc"><spring:theme code="text.orderData.firstBill" text="First bill to pay" /></p>
        </div>
      </div>

	 <div class="order-paybox ${creditSelectedFromUser} monthly">
      <div class="payboxHeading"><h3 class="totals-heading"><spring:theme code="text.items.cart.total.head" text="Now in your Shopping Cart" /></h3><span class="arrow_accord up"></span></div>
	 <div class="order-totals-payBox">
            <div class="price-list">
               <div class="cart-bundle-package">${orderData.bundleProductPriceLabel}
                  <c:if test="${(orderData.selectedProductName ne null)}">
                    <ol>
                    <c:forEach items="${orderData.selectedProductName}" var="entry">
                    <li><c:out value="${entry}"/></li>
                    </c:forEach>
                    </ol>
                  </c:if>
                </div>
                <p class="priceValue">${currentCurrency.symbol}${orderData.bundlePrice.value}</p>
            </div>
           <c:if test="${orderData.addonProductsTotalPrice.value>0}">
             <div class="price-list">
                <p class="cart-bundle-package"><spring:theme code="basket.page.cart.additonalHD.monthInfo" text="Additional HD box / month" /></p>
                <p class="priceValue">${currentCurrency.symbol}${orderData.addonProductsTotalPrice.value}</p>
            </div>
            </c:if>
            <c:if test="${orderData.channelsAddonTotalPrice.value>0}">
              <div class="price-list">
                <p class="cart-bundle-package"><spring:theme code="basket.page.cart.additionalChannel.monthInfo" text="Additional Channels  / month" /></p>
                <p class="priceValue">${currentCurrency.symbol}${orderData.channelsAddonTotalPrice.value}</p>
            </div>
            </c:if>
             <c:if test="${orderData.smartProtectPrice ne null && orderData.smartProtectPrice.value>0}">
             <div class="price-list">
                <p class="cart-bundle-package"><spring:theme code="basket.page.cart.smartProtect.monthInfo" text="SmartProtect" /></p>
                <p class="priceValue">${currentCurrency.symbol}${orderData.smartProtectPrice.value}</p>
            </div>
            </c:if>
            <div class="cartTotal">
                <p class="cart-bundle-package"><spring:theme code="basket.page.total.cart" text="TOTAL MONTHLY PAYMENT" /></p>
                <p>${currentCurrency.symbol}${fn:escapeXml(orderData.totalPrice.value)}</p>
            </div>
       </div>
       </div>
	 <div class="order-paybox ${creditSelectedFromUser}">
	<div class="payboxHeading"><h3 class="totals-heading"><spring:theme code="text.orderData.payNow" text="Pay Today" /></h3><span class="arrow_accord up"></span></div>
	 <div class="order-totals-payBox">
	 <c:if test="${orderData.creditScoreLevel.upfrontPayment ne null && (fn:contains(orderData.creditScoreLevel.upfrontPayment, 'payment'))}">
            <div class="price-list">
                <p class="cart-bundle-package"><spring:theme code="text.items.cart.total.head" text="Monthly Payment" /></p>
                <p class="priceValue">${currentCurrency.symbol}${orderData.totalPrice.value}</p>
            </div>
            </c:if>
	       <c:if test="${orderData.creditScoreLevel.installationFee ne null}">
            <div class="price-list">
                <p class="cart-bundle-package"><spring:theme code="text.orderData.installation" text="Installation" /></p>
                <p class="priceValue">${currentCurrency.symbol}${orderData.creditScoreLevel.installationFee}</p>
            </div>
           </c:if>
           <c:if test="${orderData.creditScoreLevel.dvrDeposit ne null}">
            <div class="price-list">
                <p class="cart-bundle-package"><spring:theme code="text.orderData.dvrDeposit" text="DVR Deposit" /></p>
                <p class="priceValue">${currentCurrency.symbol}${orderData.creditScoreLevel.dvrDeposit}</p>
            </div>
           </c:if>
            <div class="cartTotal">
                <p class="cart-bundle-package"><spring:theme code="text.orderData.extimatePayToday" text="ESTIMATED TO PAY TODAY" /></p>
                <p>${currentCurrency.symbol}${fn:escapeXml(orderData.amountDueToday)}</p>
            </div>
       </div>
       </div>
     <div class="order-paybox ${creditSelectedFromUser}">
	<div class="payboxHeading"><h3 class="totals-heading"><spring:theme code="text.orderData.payOnFirstbill" text="To pay on your first bill" /></h3><span class="arrow_accord up"></span></div>
	 <div class="order-totals-payBox">
	         <c:if test="${orderData.creditScoreLevel.upfrontPayment eq null || !(fn:contains(orderData.creditScoreLevel.upfrontPayment, 'payment'))}">
            <div class="price-list">
                <p class="cart-bundle-package"><spring:theme code="text.items.cart.total.head" text="Monthly Payment" /></p>
                <p class="priceValue">${currentCurrency.symbol}${orderData.totalPrice.value}</p>
            </div>
            </c:if>
             <c:if test="${orderData.creditScoreLevel.onetimeCharges ne null}">
            <div class="price-list">
                <p class="cart-bundle-package"><spring:theme code="text.orderData.onetimeCharges" text="One Time Charges(Installation Fee)" /></p>
                <p class="priceValue">${currentCurrency.symbol}${orderData.creditScoreLevel.onetimeCharges}</p>

            </div>
            </c:if>
            <c:if test="${orderData.creditScoreLevel.paperBilling ne null}">
            <div class="price-list">
                <p class="cart-bundle-package"><spring:theme code="text.orderData.paperBilling" text="Paper Billing" /></p>
                <p class="priceValue">${currentCurrency.symbol}${orderData.creditScoreLevel.paperBilling}</p>

            </div>
            </c:if>
            <div class="cartTotal">
                <p class="cart-bundle-package"><spring:theme code="text.orderData.estimatedFirstPayment" text="ESTIMATED PAYMENT ON YOUR FIRST BILL" /></p>
                <p>${currentCurrency.symbol}${fn:escapeXml(orderData.amountDueFirstBill)}</p>
            </div>
       </div>
       </div>
   </c:if>

<c:if test="${cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'cabletica'}">
	<p class="terms"><spring:theme code="basket.page.cart.terms.text" text="Tax and regulatory fees not included."/></p>
	<c:if test="${null != cartData}">
        <input type="hidden" id="cartPagePriceInfo"
            <c:if test="${cartData.bundlePrice.value ne null}"> data-attr-internet="${cartData.bundlePrice.value}" </c:if>
            <c:if test="${cartData.addonProductsTotalPrice.value ne null}"> data-attr-additionalBox="${cartData.addonProductsTotalPrice.value}" </c:if>
            <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
            <c:if test="${cartData.channelsAddonTotalPrice.value ne null}"> data-attr-additionalChannel='${cartData.channelsAddonTotalPrice.value}' </c:if>
            <c:if test="${cartData.smartProtectPrice.value ne null}"> data-attr-smartProtect='${cartData.smartProtectPrice.value}' </c:if>
            <c:if test="${cartData.totalPrice.value ne null}"> data-attr-cartTotal="${fn:escapeXml(cartData.totalPrice.value)}" </c:if>
        />
            <%--GTM --%>
	  <input type="hidden" class="Gtm_creditScore"
	         <c:if test="${cartData.creditScoreLevel.minScoreRange ne null}"> data-attr-minScoreRange="${cartData.creditScoreLevel.minScoreRange}" </c:if>
	         <c:if test="${cartData.creditScoreLevel.maxScoreRange ne null}"> data-attr-maxScoreRange="${cartData.creditScoreLevel.maxScoreRange}" </c:if>
	         <c:if test="${cartData.creditScoreLevel.letterMapping ne null}"> data-attr-letterMapping="${cartData.creditScoreLevel.letterMapping}" </c:if>
	         <c:if test="${cartData.code ne null}"> data-attr-cartCode="${cartData.code}" </c:if>
	         <c:if test="${cartData.debtCheckFlag ne null}"> data-attr-debtCheckFlag="${cartData.debtCheckFlag}" </c:if>
                data-attr-acceptsCreditCheck=""
	         <c:if test="${cartData.isCustomerExisting ne null}"> data-attr-isCustomer="${cartData.isCustomerExisting}" </c:if>
             <c:if test="${cartData.serviceabilityCheck ne null}"> data-attr-serviceabilityCheck="${cartData.serviceabilityCheck}" </c:if>
             <c:if test="${cartData.creditCheckAccpetFlag ne null}"> data-attr-creditCheckAccpetFlag="${cartData.creditCheckAccpetFlag}" </c:if>
	         />
    </c:if>
    <c:if test="${null != orderData}">
        <input type="hidden" id="cartPagePriceInfo"
            <c:if test="${orderData.bundlePrice.value ne null}"> data-attr-internet="${orderData.bundlePrice.value}" </c:if>
            <c:if test="${orderData.addonProductsTotalPrice.value ne null}"> data-attr-additionalBox="${orderData.addonProductsTotalPrice.value}" </c:if>
            <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
            <c:if test="${orderData.channelsAddonTotalPrice.value ne null}"> data-attr-additionalChannel='${orderData.channelsAddonTotalPrice.value}' </c:if>
            <c:if test="${orderData.smartProtectPrice.value ne null}"> data-attr-smartProtect='${orderData.smartProtectPrice.value}' </c:if>
            <c:if test="${orderData.totalPrice.value ne null}"> data-attr-cartTotal="${fn:escapeXml(orderData.totalPrice.value)}" </c:if>
        />
          <input type="hidden" class="Gtm_creditScore"
	         <c:if test="${orderData.creditScoreLevel.minScoreRange ne null}"> data-attr-minScoreRange="${orderData.creditScoreLevel.minScoreRange}" </c:if>
	         <c:if test="${orderData.creditScoreLevel.maxScoreRange ne null}"> data-attr-maxScoreRange="${orderData.creditScoreLevel.maxScoreRange}" </c:if>
	         <c:if test="${orderData.creditScoreLevel.letterMapping ne null}"> data-attr-letterMapping="${orderData.creditScoreLevel.letterMapping}" </c:if>
	         <c:if test="${orderData.debtCheckFlag ne null}"> data-attr-debtCheckFlag="${orderData.debtCheckFlag}" </c:if>
                data-attr-acceptsCreditCheck=""
             <c:if test="${orderData.isCustomerExisting ne null}"> data-attr-isCustomer="${orderData.isCustomerExisting}" </c:if>
             <c:if test="${orderData.serviceabilityCheck ne null}"> data-attr-serviceabilityCheck="${orderData.serviceabilityCheck}" </c:if>
             <c:if test="${orderData.creditCheckAccpetFlag ne null}"> data-attr-creditCheckAccpetFlag="${orderData.creditCheckAccpetFlag}" </c:if>
            />
    </c:if>

    <c:forEach items="${cartData.rootGroups}" var="entryGroup" varStatus="count" >
       <input type="hidden" class="checkoutProducts"
           <c:if test="${entryGroup.orderEntries[0].product.name ne null}"> data-attr-name="${entryGroup.orderEntries[0].product.name}" </c:if>
           <c:if test="${entryGroup.orderEntries[0].product.code ne null}"> data-attr-id="${entryGroup.orderEntries[0].product.code}" </c:if>
           <c:if test="${entryGroup.orderEntries[0].orderEntryPrices[0].basePrice.value ne null}"> data-attr-price="${entryGroup.orderEntries[0].orderEntryPrices[0].basePrice.value}" </c:if>
           <c:if test="${entryGroup.orderEntries[0].quantity ne null}"> data-attr-quantity="${entryGroup.orderEntries[0].quantity}" </c:if>
           <c:if test="${entryGroup.orderEntries[0].product.categories[0].name ne null}"> data-attr-category="${entryGroup.orderEntries[0].product.categories[0].name}" </c:if>
           <c:if test="${entryGroup.orderEntries[0].product.categories[0].parentCategoryName ne null}"> data-attr-primarycategory="${entryGroup.orderEntries[0].product.categories[0].parentCategoryName}" </c:if>
       />
    </c:forEach>

    <c:forEach items="${orderData.entries}" var="entryGroup" varStatus="count" >
       <input type="hidden" class="checkoutProducts"
           <c:if test="${entryGroup.product.name ne null}"> data-attr-name="${entryGroup.product.name}" </c:if>
           <c:if test="${entryGroup.product.code ne null}"> data-attr-id="${entryGroup.product.code}" </c:if>
           <c:if test="${entryGroup.orderEntryPrices[0].basePrice.value ne null}"> data-attr-price="${entryGroup.orderEntryPrices[0].basePrice.value}" </c:if>
           <c:if test="${entryGroup.quantity ne null}"> data-attr-quantity="${entryGroup.quantity}" </c:if>
           <c:if test="${entryGroup.product.categories[0].name ne null}"> data-attr-category="${entryGroup.product.categories[0].name}" </c:if>
           <c:if test="${entryGroup.product.categories[0].parentCategoryName ne null}"> data-attr-primarycategory="${entryGroup.product.categories[0].parentCategoryName}" </c:if>
       />
    </c:forEach>

</c:if>

<c:if test="${cmsSite.uid eq 'panama'}">
    <input type="hidden" id="cartPagePriceInfo"
        <c:if test="${cartData.subTotal ne null}"> data-attr-subtotal="${cartData.subTotal.formattedValue}" </c:if>
        <c:if test="${cartData.totalDiscounts ne null}"> data-attr-savings="${cartData.totalDiscounts.formattedValue}" </c:if>
        <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
        <c:if test="${cartData.totalTax ne null}"> data-attr-taxRate='${cartData.totalTax.formattedValue}' </c:if>
        <c:if test="${cartData.totalPriceWithTax ne null}"> data-attr-cartTotal="${cartData.totalPriceWithTax.formattedValue}" </c:if>
    />
</c:if>
