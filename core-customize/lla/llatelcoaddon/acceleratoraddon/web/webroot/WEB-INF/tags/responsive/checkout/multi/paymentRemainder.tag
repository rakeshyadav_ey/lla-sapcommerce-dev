<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="showDeliveryAddress" required="false" type="java.lang.Boolean"%>
<%@ attribute name="showPotentialPromotions" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="order-entry" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/entry"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="monthlyTotalPrice" required="false"  type="de.hybris.platform.commercefacades.product.data.PriceData"%>


<spring:htmlEscape defaultHtmlEscape="true" />
<c:set var="hasDeliveryItems" value="${cartData.deliveryItemsQuantity > 0}" />
<c:set var="deliveryAddress" value="${cartData.deliveryAddress}" />
<c:set var="installationAddress" value="${cartData.installationAddress}" />
<spring:url value="/plp/postpaid" var="postpaidPLPUrl"/>
<spring:url value="/plp/prepaid" var="prepaidPLPUrl"/>


<div class="today-totals-head">
	<spring:theme code="text.cart.items.friendly.remainder.head" htmlEscape="false" />
</div>
<div class="payment-details">
	<div class="price">
		<div class="monthly"><spring:theme code="text.due.monthly" /></div>
		<div class="monthly-price">
			<format:price priceData="${cartData.monthlyTotalPrice}" />
		</div>
		<div class="gct"><spring:theme code="text.due.monthly.gtc" htmlEscape="false" /></div>
	</div>
	<div class="seperater">
	</div>
	<div class="price">
		<div class="monthly"><spring:theme code="text.due.today" /></div>
		<div class="monthly-price">
			<format:price priceData="${cartData.totalPriceWithTax}" />
		</div>
		<div class="gct"><spring:theme code="text.one.time.payment" /></div>
	</div>
</div>

	
	

