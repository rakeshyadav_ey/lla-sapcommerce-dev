<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="productData" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:url value="${productData.url}" var="productDataUrl" htmlEscape="false"/>

<c:set var="productName" value="${ycommerce:encodeHTML(productData.name)}"/>

<li class="grid_view product__list--items col-lg-4 col-md-4 col-sm-4">
	<div class="full-grid">
		<div class="prod-wrapper">
			<div class="product-info">
				<div class="prod-img">
					<a class="product__list--thumb" href="${productDataUrl}" title="${productName}">
						<product:productPrimaryImage product="${productData}" format="thumbnail"/>
					</a>
				</div>
				<div class="prod-content">
					<strong class="product__list--name prod-name" href="${productDataUrl}" title="${productName}">${productName}</strong>
					<!-- <div class="rating">
						<div class="rating-stars js-ratingCalc" data-rating='{"rating":"${productData.averageRating}","total":5}'>
							<div class="greyStars">
								<c:forEach begin="1" end="5">
									<span class="glyphicon glyphicon-star"></span>
								</c:forEach>
							</div>
							<div class="greenStars js-greenStars">
								<c:forEach begin="1" end="5">
									<span class="glyphicon glyphicon-star active"></span>
								</c:forEach>
							</div>
						</div>
					</div> -->
					<div class="prod-data">
						<div class="product__listing--price">
							<!--<spring:theme code="offer.price.startingFrom" text="Starting from"/>-->
							<div class="product-price prod-price">
								<c:choose>
									<c:when test="${cmsSite.uid eq 'jamaica'}">
										<format:price priceData="${productData.price}" displayFreeForZero="true"/> &#43 GCT
									</c:when>
									<c:otherwise>
										<span class="actual-price">
											<format:price priceData="${productData.price}" displayFreeForZero="true"/>
										</span>
									</c:otherwise>
								</c:choose>
							</div>
							<c:if test="${productData.stock.stockLevelStatus.code eq 'outOfStock' }">
								<spring:theme code="text.addToCart.outOfStock" text="Out of Stock"/>
							</c:if>
						</div>
						<%--<c:if test="${fn:contains(productData.allCategories, 'device')}">
							<div class="prod-plans">
								<spring:theme code="pre.post.plan" text="Pre and Post Paid"/>
							</div>
						</c:if>--%>
					</div>
				</div>
			</div>
			<!-- <table class="tma-variant-colors-category-grid" align="center">
				<c:choose>
					<c:when test="${not empty productData.colors}">
						<tr>
							<c:forEach items="${productData.colors}" var="color">
								<td>
									<span style="background-color: ${color};"></span>
								</td>
							</c:forEach>
						</tr>
					</c:when>
				</c:choose>
			</table>-->

			<div class="price-details prod-btn-info">
				<a class="btn btn-primary add-cart-btn btn-custom" href="${productDataUrl}">
					<c:if test="${cmsSite.uid eq 'jamaica'}">
						<spring:theme code="text.buynow" text="Buy Now"/>
					</c:if>
					<c:if test="${cmsSite.uid eq 'panama'}">
					<spring:theme code="text.addToCart" text="Add To Cart"/>
					</c:if>
				</a>
			</div>
			<a class="features-link" href="${productDataUrl}">
				<c:if test="${cmsSite.uid eq 'jamaica'}">
					<spring:theme code="prod.feature.text" text="Features"/>
				</c:if>
				<c:if test="${cmsSite.uid eq 'panama'}">
					<spring:theme code="text.information" text="Information"/>
				</c:if>
			</a>
		</div>
	</div>
</li>
