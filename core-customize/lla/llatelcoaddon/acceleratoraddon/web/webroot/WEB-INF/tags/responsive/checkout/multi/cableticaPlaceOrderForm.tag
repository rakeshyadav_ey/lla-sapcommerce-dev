<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ attribute name="placeOrderUrl" required="true"  type="java.lang.String"%>
<%@ attribute name="cartCalculationMessage" required="true"  type="java.lang.String"%>
<%@ attribute name="getTermsAndConditionsUrl" required="true"  type="java.lang.String"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/cart"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="multi-checkout-telco" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>
<div class="checkout-order-summary">
    <div class="cart-totals">
      <cart:cableticaCartTotals cartData="${cartData}" showTaxEstimate="${taxEstimationEnabled}" />
	</div>
</div>
<div class="place-order-form">
   <form:form action="${placeOrderUrl}" id="placeOrderForm1"
      modelAttribute="placeOrderForm">
      <div class="checkbox">
         <label class="custom-label">
            <spring:theme code="checkout.summary.placeOrder.readTermsAndConditions1.cabletica" />
            <form:checkbox id="Terms1" path="termsCheck1" />
            <span class="checkmark"></span>
         </label>
         <label class="custom-label">
         	<c:if test="${cmsSite.uid eq 'panama'}">
            	<a href="http://cw.pa/TCpostpaid-es" target="_blank" class="terms-link">
            </c:if>
            <spring:theme
               code="checkout.summary.placeOrder.readTermsAndConditions2.cabletica"
               arguments="${getTermsAndConditionsUrl}" text="click here"
               htmlEscape="false" />
            </a>
            <form:checkbox id="Terms2" path="termsCheck2" />
            <span class="checkmark"></span>
         </label>
      </div>
      <spring:theme code="checkout.summary.placeOrder" text="Place Order"
         var="placeOrderText" />
      <button type="submit"
         class="btn btn-block btn-primary btn-place-order"
         id="placeOrder" title="${placeOrderText}">
      ${placeOrderText}</button>
   </form:form>
   <input type="hidden" id="gtmCheckoutCheckBoxError" value="<spring:theme code="gtm.checkout.form.errors.msg"/>" >
</div>
<!--<multi-checkout-telco:cartFooterText/>-->

