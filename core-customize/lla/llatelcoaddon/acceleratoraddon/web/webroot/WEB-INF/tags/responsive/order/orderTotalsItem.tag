<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="structure" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/structure" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="order-total order-total-info">
    <div class="row">
		<c:forEach items="${order.orderPrices}" var="orderPrice">
        	<div class="col-xs-7 col-sm-7">
                <c:if test="${cmsSite.uid != 'panama'}">
             	    ${ycommerce:encodeHTML(orderPrice.billingTime.name)}:
                </c:if>

               <c:if test="${cmsSite.uid eq 'panama' && empty orderPrice.monthlyPrice}">
                 <spring:theme code="basket.page.totals.subtotal" text="Subtotal"/>
               </c:if>
        	</div>
			<structure:orderPriceWrapper>
               	<format:price priceData="${orderPrice.subTotal}"/>
         	</structure:orderPriceWrapper>
        </c:forEach>

        <c:if test="${order.totalDiscounts.value > 0}">
            <div class="col-xs-7 col-sm-7">
                <div class="subtotals__item--state-discount">
                    <spring:theme code="text.account.order.discount" text="Discount:"/>
                </div>
            </div>
            <div class="col-xs-5 col-sm-5">
                <div class="text-right subtotals__item--state-discount">
                    <format:price priceData="${order.totalDiscounts}" displayNegationForDiscount="true" />
                </div>
            </div>
        </c:if>

        <c:if test="${cmsSite.uid eq 'panama'}">
            <c:choose>
                <c:when test="${freeShipping eq true && freeInstalltion eq true}">
                    <div class="col-xs-7 col-sm-7">
                    <spring:theme code="basket.page.totals.freeShippingAndInstallation" text="free Shipping And Installation"/></div>
                    <div class="col-xs-5 col-sm-5"><div class="text-right">${currentCurrency.symbol}0.00</div></div>
                </c:when>
                <c:otherwise>
                    <c:if test="${freeShipping eq true}">
                        <div class="col-xs-7 col-sm-7"><spring:theme code="basket.page.totals.freeShipping" text="free Shipping"/></div>
                        <div class="col-xs-5 col-sm-5"><div class="text-right">${currentCurrency.symbol}0.00</div></div>
                    </c:if>
                    <c:if test="${freeInstalltion eq true}">
                        <div class="col-xs-7 col-sm-7"><spring:theme code="basket.page.totals.freeInstalltion" text="free Installtion"/></div>
                        <div class="col-xs-5 col-sm-5"><div class="text-right">${currentCurrency.symbol}0.00</div></div>
                    </c:if>
                </c:otherwise>
            </c:choose>
            <c:if test="${freeSim eq true}">
                <div class="col-xs-7 col-sm-7"><spring:theme code="basket.page.totals.freeSim" text="free Sim"/></div>
                <div class="col-xs-5 col-sm-5"><div class="text-right">${currentCurrency.symbol}0.00</div></div>
            </c:if>
		</c:if>

        <c:if test="${order.net}">
            <div class="col-xs-7 col-sm-7">
                <spring:theme code="text.account.order.netTax" text="Tax:"/>
            </div>
            <structure:orderPriceWrapper>
                    <format:price priceData="${order.totalTax}"/>
             </structure:orderPriceWrapper>
        </c:if>

        <div class="col-xs-7 col-sm-7">
            <div class="totals">
                <c:if test="${cmsSite.uid ne 'panama'}">
                <spring:theme code="basket.page.total" text="Total" />
                </c:if>
                <c:if test="${cmsSite.uid eq 'panama'}">
                <spring:theme code="basket.page.total.checkout" text="Total" />
                </c:if>
            </div>
        </div>

        <c:choose>
            <c:when test="${order.net}">
                <div class="col-xs-5 col-sm-5">
                    <div class="text-right totals">
                        <format:price priceData="${order.totalPriceWithTax}"/>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="col-xs-5 col-sm-5 text-right">
                    <div class="totals">
                        <format:price priceData="${order.totalPrice}"/>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
        <c:if test="${cmsSite.uid eq 'panama'}">
            <div class="col-xs-7 col-sm-9">
                <div class="monthlyTotals">
                    <spring:theme code="basket.page.total.monthly" text="Total monthly payment (ITBMS no include)" />
                </div>
            </div>
             <div class="col-xs-5 col-sm-3 text-right">
                  <c:forEach items="${order.orderPrices}" var="orderPrice">
                    <c:if test="${orderPrice.monthlyPrice ne '' or orderPrice.monthlyPrice ne null}">
                      <c:set var="orderMonthlyTotal" value="${orderPrice.monthlyPrice}"/>
                    </c:if>
                 </c:forEach>
                <div class="monthlyTotals">
                     <format:price priceData="${orderMonthlyTotal}" displayFreeForZero="FALSE"/>
                </div>
             </div>
          </c:if>
    </div>
</div>

<c:if test="${cmsSite.uid != 'panama'}">
<div class="account-orderdetail-orderTotalDiscount-section">
    <c:if test="${not order.net}">
        <div class="order-total__taxes">
            <spring:theme code="text.account.order.total.includesTax.payNowTax" argumentSeparator=";" arguments="${order.totalTax.formattedValue}"/>
        </div>
        <br/>
    </c:if>
</div>
</c:if>
<c:if test="${cmsSite.uid eq 'panama'}">
<input type="hidden" id="cartPagePriceInfo"
    <c:if test="${order.totalPrice ne null}"> data-attr-subtotal="${order.totalPrice.formattedValue}" </c:if>
    <c:if test="${order.totalDiscounts.value ne null}"> data-attr-savings="${order.totalDiscounts.value}" </c:if>
    <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
    <c:if test="${order.totalTax ne null}"> data-attr-taxRate='${order.totalTax.formattedValue}' </c:if>
    <c:if test="${order.totalPriceWithTax ne null}"> data-attr-cartTotal="${order.totalPriceWithTax.formattedValue}" </c:if>
/>
</c:if>