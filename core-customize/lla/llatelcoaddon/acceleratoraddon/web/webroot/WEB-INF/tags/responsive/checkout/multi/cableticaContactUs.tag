<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<div class="contactus">
   <h4><spring:theme code="contactus.Questions" />
   </h4>
   <div class="contactus-info">
      <ul>
         <li class="call">
         	<p>
         		<spring:theme code="contactus.contactus" />
         		<span><spring:theme code="contactus.call" htmlEscape="false" /></span>
         	</p>
         </li>
      </ul>
      </div>
   </div>
</div>