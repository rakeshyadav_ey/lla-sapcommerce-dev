<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="showDeliveryAddress" required="false" type="java.lang.Boolean"%>
<%@ attribute name="showPotentialPromotions" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="order-entry" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/entry"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="multi-checkout-telco" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<c:set var="hasDeliveryItems" value="${cartData.deliveryItemsQuantity > 0}" />
<c:set var="deliveryAddress" value="${cartData.deliveryAddress}" />
<c:set var="installationAddress" value="${cartData.installationAddress}" />
<spring:url value="/plp/postpaid" var="postpaidPLPUrl"/>
<spring:url value="/plp/prepaid" var="prepaidPLPUrl"/>
<%@ attribute name="monthlyTotalPrice" required="false"  type="de.hybris.platform.commercefacades.product.data.PriceData"%>


<div class="today-totals-head">
	<spring:theme code="text.cart.items.monthly.total.head" htmlEscape="false" />
</div>

<ul class="checkout-order-summary-list">
	<multi-checkout-telco:orderedItems cartData="${cartData}" showDeliveryAddress="${showDeliveryAddress}" />

	<div class="seperater"><hr></div>
	<li class="checkout-order-summary-list-items today-total">
		<div class="details"><spring:theme code="basket.page.total.checkout.monthly" text="Total" /></div>
		<div class="price"><format:price priceData="${orderData.monthlyTotalPrice}" /></div>
	</li>
	<li class="checkout-order-summary-list-items">
		<spring:theme code="text.checkout.monthly.disclaimer" />
	</li>
</ul>
<input type="hidden" class="productGtmDataPaymentMethod" data-attr-cardtype="${orderData.paymentInfo.cardTypeData.name}" >
