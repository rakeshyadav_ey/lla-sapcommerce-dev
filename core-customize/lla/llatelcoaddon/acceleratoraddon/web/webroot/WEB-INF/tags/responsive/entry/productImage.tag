<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="orderEntry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<spring:url value="${orderEntry.product.url}" var="productUrl"  htmlEscape="false"/>
<spring:url value="/plp/postpaid" var="postpaidPLPUrl"/>
<spring:url value="/plp/prepaid" var="prepaidPLPUrl"/>

<div class="item__image">

	<c:choose>
		<c:when test="${cmsSite.uid eq 'panama'}">
			<c:if test="${orderEntry.product.categories[0].code eq 'postpaid' || orderEntry.product.categories[0].code eq 'addon' || orderEntry.product.categories[0].code eq 'prepaid'}">
				<c:if test="${orderEntry.product.categories[0].code eq 'postpaid'}">
					<a href="${postpaidPLPUrl}">
						<product:productPrimaryImage product="${orderEntry.product}" format="cartIcon"/>
					</a>
				</c:if>
				<c:if test="${orderEntry.product.categories[0].code eq 'prepaid'}">
					<a href="${prepaidPLPUrl}">
						<product:productPrimaryImage product="${orderEntry.product}" format="cartIcon"/>
					</a>
				</c:if>

				<c:if test="${orderEntry.product.categories[0].code eq 'addon'}">
					<product:productPrimaryImage product="${orderEntry.product}" format="cartIcon"/>
				</c:if>
			</c:if>
			<c:if test="${orderEntry.product.categories[0].code ne 'postpaid' && orderEntry.product.categories[0].code ne 'addon' && orderEntry.product.categories[0].code ne 'prepaid'}">
				<a href="${productUrl}">
					<product:productPrimaryImage product="${orderEntry.product}" format="cartIcon"/>
				</a>
			</c:if>
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'cabletica' || cmsSite.uid eq 'jamaica'}">
					<product:productPrimaryImage product="${orderEntry.product}" format="thumbnail"/>
				</c:when>
				<c:otherwise>
					<a href="${productUrl}">
						<product:productPrimaryImage product="${orderEntry.product}" format="thumbnail"/>
					</a>
				</c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>
</div>

