<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ attribute name="orderEntryNumber" required="true" type="java.lang.Integer" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:forEach items="${order.entries}" var="entry">
   <c:if test="${entry.fmcRuleApplied and entry.entryNumber == orderEntryNumber }">
        <ul class="cart-promotions">
            <li class="cart-promotions-potential"><span>${ycommerce:sanitizeHTML(entry.fmcPromotionMessage)}</span></li> 
        </ul>
   </c:if>
</c:forEach>
