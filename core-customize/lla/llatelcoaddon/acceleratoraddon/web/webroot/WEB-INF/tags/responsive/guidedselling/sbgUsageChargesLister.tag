<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<c:forEach items="${product.price.usageCharges}" var="usageCharge">
	<div>
		<b>${ycommerce:encodeHTML(usageCharge.name)}</b>
		<br>
		<c:if test="${not empty usageCharge.usageChargeEntries}">
			<c:set var="isFirstChargeEntry" value="true" />
			<c:forEach items="${usageCharge.usageChargeEntries}" var="usageChargeEntry">
				<c:if test="${usageChargeEntry['class'].simpleName eq 'TierUsageChargeEntryData'}">
					<c:if test="${usageCharge['class'].simpleName eq 'PerUnitUsageChargeData'}">
						<c:if test="${isFirstChargeEntry}">
							<spring:theme code="product.list.viewplans.tierUsageChargeEntry"
								arguments="${usageChargeEntry.tierStart}^${usageChargeEntry.tierEnd}^^${usageChargeEntry.price.formattedValue}^${usageCharge.usageUnit.name}"
								argumentSeparator="^" />
							<br>
						</c:if>
						<c:if test="${not isFirstChargeEntry}">
							<c:if test="${usageCharge.usageChargeType.code eq 'each_respective_tier'}">
								<spring:theme code="product.list.viewplans.tierUsageChargeEntry"
									arguments="${usageChargeEntry.tierStart}^${usageChargeEntry.tierEnd}^^${usageChargeEntry.price.formattedValue}^${usageCharge.usageUnit.name}"
									argumentSeparator="^" />
								<br>
							</c:if>
							<c:if test="${usageCharge.usageChargeType.code eq 'highest_applicable_tier'}">
								<spring:theme code="product.list.viewplans.tierUsageChargeEntryHighestTier"
									arguments="${usageChargeEntry.tierStart}^${usageChargeEntry.tierEnd}^^${usageChargeEntry.price.formattedValue}^${usageCharge.usageUnit.name}"
									argumentSeparator="^" />
								<br>
							</c:if>
						</c:if>
					</c:if>
					<c:if test="${usageCharge['class'].simpleName eq 'VolumeUsageChargeData'}">
						<c:if test="${isFirstChargeEntry}">
							<spring:theme code="product.list.viewplans.volumeTierUsageChargeEntry"
								arguments="${usageChargeEntry.tierStart}^${usageChargeEntry.tierEnd}^^${usageChargeEntry.price.formattedValue}" argumentSeparator="^" />
							<br>
						</c:if>
						<c:if test="${not isFirstChargeEntry}">
							<spring:theme code="product.list.viewplans.volumeTierUsageChargeEntryNotFirstTier"
								arguments="${usageChargeEntry.tierStart}^${usageChargeEntry.tierEnd}^^${usageChargeEntry.price.formattedValue}" argumentSeparator="^" />
							<br>
						</c:if>
					</c:if>
				</c:if>

				<c:if test="${usageChargeEntry['class'].simpleName eq 'OverageUsageChargeEntryData' and fn:length(usageCharge.usageChargeEntries) gt 1}">
					<c:if test="${usageCharge['class'].simpleName eq 'PerUnitUsageChargeData' and usageCharge.usageChargeType.code eq 'each_respective_tier'}">
						<spring:theme code="product.list.viewplans.thereafterOverageUsageChargeEntry"
							arguments="${usageChargeEntry.price.formattedValue},${usageCharge.usageUnit.name}" />
					</c:if>
					<c:if test="${usageCharge['class'].simpleName eq 'PerUnitUsageChargeData' and usageCharge.usageChargeType.code eq 'highest_applicable_tier'}">
						<spring:theme code="product.list.viewplans.thereafterOverageUsageChargeEntryHighestTier"
							arguments="${usageChargeEntry.price.formattedValue},${usageCharge.usageUnit.name}" />
					</c:if>
					<c:if test="${usageCharge['class'].simpleName eq 'VolumeUsageChargeData'}">
						<spring:theme code="product.list.viewplans.thereafterOverageUsageChargeEntryVolume"
							arguments="${usageChargeEntry.price.formattedValue},${usageCharge.usageUnit.name}" />
					</c:if>
				</c:if>
				<c:if test="${usageChargeEntry['class'].simpleName eq 'OverageUsageChargeEntryData' and fn:length(usageCharge.usageChargeEntries) eq 1}">
					<spring:theme code="product.list.viewplans.overageUsageChargeEntry" arguments="${usageChargeEntry.price.formattedValue},${usageCharge.usageUnit.name}" />
				</c:if>
				<c:set var="isFirstChargeEntry" value="false" />
			</c:forEach>
		</c:if>
	</div>
	<br>
</c:forEach>