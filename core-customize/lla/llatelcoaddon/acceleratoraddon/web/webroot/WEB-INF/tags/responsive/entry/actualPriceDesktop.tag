<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="orderEntryTotalPrice" required="true" type="de.hybris.platform.commercefacades.product.data.PriceData"%>
<%@ attribute name="orderEntryBasePrice" required="true"  type="de.hybris.platform.commercefacades.product.data.PriceData"%>
<%@ attribute name="orderEntryMonthlyPrice" required="false"  type="de.hybris.platform.commercefacades.product.data.PriceData"%>
<%@ attribute name="monthlyTotalPrice" required="false"  type="de.hybris.platform.commercefacades.product.data.PriceData"%>
<%@ attribute name="orderEntryRegularPrice" required="false"  type="java.lang.String"%>
<%@ attribute name="orderEntryDiscountFlag" required="false"  type="java.lang.Boolean"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="totalPriceWithTax" required="false"  type="de.hybris.platform.commercefacades.product.data.PriceData"%>											
<c:choose>
	<c:when test="${not empty orderEntryTotalPrice}">
		<c:if test="${(orderEntryBasePrice.value - orderEntryTotalPrice.value) > 0}">
			<del>
				<format:price priceData="${orderEntryBasePrice}" displayFreeForZero="false" />
			</del>
		</c:if>
		<c:if test="${cmsSite.uid ne 'panama' && cmsSite.uid ne 'cabletica'  && cmsSite.uid ne 'jamaica'}">
    		<format:price priceData="${orderEntryTotalPrice}" displayFreeForZero="false" />
		</c:if>
		
		<c:if test="${cmsSite.uid eq 'panama'}">
		<div><spring:theme code="text.monthly.payment" /> <br> 
		    <b>
		<c:choose>
			<c:when test="${not empty orderEntryMonthlyPrice}">
				<format:price priceData="${orderEntryMonthlyPrice}" displayFreeForZero="false" />
			</c:when>
			<c:otherwise>
				${currentCurrency.symbol}0.00
			</c:otherwise>
		</c:choose>
		</b>
		</div>
		</c:if>

        <c:if test="${cmsSite.uid eq 'cabletica'}">
            <div>
                <b>
                    <c:choose>
                        <c:when test="${not empty totalPriceWithTax}">
                            ${currentCurrency.symbol}<fmt:formatNumber value="${totalPriceWithTax.value}" pattern="#,##0" />
                        </c:when>
                        <c:when test="${not empty orderEntryMonthlyPrice && empty totalPriceWithTax}">
                            ${currentCurrency.symbol}<fmt:formatNumber value="${orderEntryMonthlyPrice.value}" pattern="#,##0" />
                        </c:when>
                        <c:otherwise>
                            ${currentCurrency.symbol}0.00
                        </c:otherwise>
                    </c:choose>
                    &nbsp;<spring:theme code="text.cabletica.IVA"/>
                </b><br/>
                <span class="item__price--monthly"><spring:theme code="text.ctaMonthlyplan"/></span>
            </div>
        </c:if>
		
        <c:if test="${cmsSite.uid eq 'jamaica'}">
            <div class="price">
                
                <c:choose>
                    <c:when test="${orderEntryDiscountFlag}">
                        <div class="monthly">
                                                <spring:theme code="text.cart.discount.for"/>
                                                                                   &nbsp;${cartData.discountNumber}&nbsp;${cartData.discountPeriod.code}</div>
                        <div class="monthly-price">
                            <div class="newPrice">
                                <c:choose>
                                    <c:when test="${not empty orderEntryMonthlyPrice}">
                                        <small>Now&nbsp;&nbsp;</small><format:price priceData="${orderEntryMonthlyPrice}" displayFreeForZero="false" /><span class="tax">&nbsp;+&nbsp;GCT*</span>
                                    </c:when>
                                    <c:otherwise>
                                        ${currentCurrency.symbol}0.00
                                    </c:otherwise>
                                </c:choose>
                            </div>
                            <div class="oldPrice"><spring:theme code="text.regular.payment" /> ${orderEntryRegularPrice}&nbsp;<small>+ GCT</small> </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="monthly"><spring:theme code="text.cart.items.monthly.payment" /></div>
                        <div class="monthly-price">
                            <c:choose>
                                <c:when test="${not empty orderEntryMonthlyPrice}">
                                    <format:price priceData="${orderEntryMonthlyPrice}" displayFreeForZero="false" />
                                </c:when>
                                <c:otherwise>
                                    ${currentCurrency.symbol}0.00
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="gct"><spring:theme code="basket.page.cart.terms.text" /></div>
                    </c:otherwise>
                </c:choose>

            </div>
            <div class="price">
                <div class="monthly"><spring:theme code="text.cart.items.today.payment" /></div>
                <div class="today-price"><format:price priceData="${orderEntryTotalPrice}" displayFreeForZero="false" /></div>
                <div class="gct"><spring:theme code="text.one.time.payment" /></div>
            </div>
            <input type="hidden" id="cartPagePriceInfo"
                <c:if test="${cartData.subTotal ne null}"> data-attr-subtotal="${cartData.subTotal.formattedValue}" </c:if>
                <c:if test="${cartData.totalDiscounts ne null}"> data-attr-savings="${cartData.totalDiscounts.formattedValue}" </c:if>
                <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
                <c:if test="${cartData.totalTax ne null}"> data-attr-taxRate='${cartData.totalTax.formattedValue}' </c:if>
                <c:if test="${cartData.totalPriceWithTax ne null}"> data-attr-cartTotal="${cartData.totalPriceWithTax.formattedValue}" </c:if>
            />
		</c:if>

	</c:when>
	<c:otherwise>
		&mdash;
	</c:otherwise>
</c:choose>
