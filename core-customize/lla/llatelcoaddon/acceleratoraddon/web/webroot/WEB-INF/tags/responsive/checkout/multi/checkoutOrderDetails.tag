<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="showDeliveryAddress" required="true" type="java.lang.Boolean"%>
<%@ attribute name="showPaymentInfo" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="multi-checkout-telco" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/cart"%>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order"%>

<c:if test="${not empty cartData}">
	<!-- <div class="checkout-summary-headline hidden-xs">
		<spring:theme code="checkout.multi.order.summary" text="Order Summary" />
	</div>  -->
</c:if>
<%--<c:if test="${cmsSite.uid eq 'jamaica'}">
	<div class="checkout-order-summary monthly-details">
		<multi-checkout-telco:monthlyDeliveryCartItems cartData="${cartData}" showDeliveryAddress="${showDeliveryAddress}" />
	</div>
</c:if>--%>
<div class="checkout-order-summary">

	<c:if test="${cmsSite.uid ne 'puertorico'}">
	<multi-checkout-telco:deliveryCartItems cartData="${cartData}" showDeliveryAddress="${showDeliveryAddress}"  />
	</c:if>
	<c:forEach items="${cartData.pickupOrderGroups}" var="groupData">
		<multi-checkout:pickupCartItems cartData="${cartData}" groupData="${groupData}" showHead="true" />
	</c:forEach>
	<c:if test="${cmsSite.uid ne 'cabletica'}">
		<multi-checkout:paymentInfo cartData="${cartData}" paymentInfo="${cartData.paymentInfo}" showPaymentInfo="${showPaymentInfo}" />
	</c:if>


	<c:if test="${cmsSite.uid ne 'jamaica' && cmsSite.uid ne 'cabletica'}">
		<div class="cart-totals">
			<cart:cartTotals cartData="${cartData}" />
		</div>
	</c:if>
	<c:if test="${cmsSite.uid eq 'puertorico' and pageType ne 'ORDERSUMMARY'}" >
    <div class="contactus">
       <h4>
          <spring:theme code="contactus.Questions" htmlEscape="false"/>
       </h4>
        <p><spring:theme code="contactus.getincontact" htmlEscape="false"/></p>
       <ul>
          <li class="call">
             <spring:theme code="contactus.call" htmlEscape="false" />
           </li>
       </ul>
    </div>
    </c:if>
	<order:appliedVouchers order="${cartData}" />
	<cart:cartPromotions cartData="${cartData}" />
</div>
<%--<c:if test="${cmsSite.uid eq 'jamaica'}">
	<div class="checkout-order-summary payment-remainder">
		<multi-checkout-telco:paymentRemainder cartData="${cartData}" showDeliveryAddress="${showDeliveryAddress}" />
	</div>
</c:if>--%>