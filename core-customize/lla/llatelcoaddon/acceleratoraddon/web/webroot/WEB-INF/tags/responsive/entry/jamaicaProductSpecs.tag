<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="orderEntry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ attribute name="showStock" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="order-entry" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/entry" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set var="product" value="${orderEntry.product}"/>
<c:if test="${cmsSite.uid eq 'jamaica' }">
    <div class="more-benefits">
        <a id="more" class="read-link hide" href="javascript:void(0)"><spring:theme code="text.more.benefits"/>&nbsp;&nbsp;</a>
        <a id="less" class="read-link" href="javascript:void(0)"><spring:theme code="text.less.info.benefitts"/>&nbsp;&nbsp;</a>
        <ul class="benefits-list">
            <c:forEach var="specCharValue" items="${product.productSpecCharValueUses}" varStatus="index">
                <li>${specCharValue.description}</li>
            </c:forEach>
        </ul>
    </div>
</c:if>
