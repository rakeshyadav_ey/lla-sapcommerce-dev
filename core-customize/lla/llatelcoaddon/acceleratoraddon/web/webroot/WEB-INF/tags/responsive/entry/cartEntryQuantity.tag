<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="entry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<spring:url value="/cart/update" var="cartUpdateFormAction" htmlEscape="false" />

<c:if test="${not empty entry.product}">
	<div class="item__quantity ${cmsSite.uid eq 'cabletica' ? 'item__addon__check':''}">
		<form:form id="updateCartForm${entry.entryNumber}" action="${cartUpdateFormAction}" method="post"
			modelAttribute="updateQuantityForm${entry.entryNumber}"
			data-cart='{"cartCode" : "${ycommerce:encodeHTML(cartData.code)}",
		"productPostPrice":"${entry.basePrice.value}","productName":"${ycommerce:encodeHTML(entry.product.name)}"}'>
			<input type="hidden" name="entryNumber" value="${entry.entryNumber}" />
			<input type="hidden" name="productCode" value="${ycommerce:encodeHTML(entry.product.code)}" />
			<input type="hidden" name="initialQuantity" value="${entry.quantity}" />
			<c:choose>
				<c:when test="${not entry.updateable}">
					<input type="hidden" name="quantity" class="item__quantity form-control" value="${entry.quantity}"/>
					<input type="text" disabled="${not entry.updateable}" class="form-control js-update-entry-quantity-input"
						   value="${entry.quantity}"/>
				</c:when>
				<c:otherwise>
					<form:label cssClass="skip" path="quantity" for="quantity${entry.entryNumber}">
						<spring:theme code="basket.page.quantity" text="Quantity" />
					</form:label>
					<c:if test="${cmsSite.uid eq 'cabletica'}">
						<div class="ctabox-addToCart-section"><input type="checkbox" class="addOnCheckBox addOnCheckBoxdata" name="addOnCheckBox" value="" checked></div>
				    <c:set var="addonMaxQty" value="0"/>
					<c:forEach items="${cartData.rootGroups}" var="entryGroup">
					    <c:if test="${entryGroup.orderEntries[0].product.categories[0].code != 'channelsaddoncabletica'}">
                           <c:choose>
                               <c:when test="${(entryGroup.orderEntries[0].product.categories[0].code == '3PInternetTvTelephone' || entryGroup.orderEntries[0].product.categories[0].code == '2PInternetTv') && addonCableticaMaxOrderQuantityMap[entry.product.code] ne null}">
                                   <c:set var="addonMaxQty" value="${addonCableticaMaxOrderQuantityMap[entry.product.code]}"/>
                               </c:when>
                               <c:otherwise>
                                   <c:set var="addonMaxQty" value="${addontvCableticaMaxOrderQuantityMap[entry.product.code]}"/>
                               </c:otherwise>
                           </c:choose>
		                </c:if>
		       	    </c:forEach>
					</c:if>
					<div class="quantity-box" <c:if test="${cmsSite.uid eq 'panama' && addonMaxOrderQuantityMap[entry.product.code] ne null}"> data-qty-max="${addonMaxOrderQuantityMap[entry.product.code]}" </c:if>
					<c:if test="${cmsSite.uid eq 'cabletica' && addonMaxQty ne null}"> data-qty-max="${addonMaxQty}" </c:if>>

					    <input type="hidden" class="gtmProductData"
					        <c:if test="${entry.product.name ne null}"> data-attr-name="${entry.product.name}" </c:if>
                            <c:if test="${entry.product.code ne null}"> data-attr-id="${entry.product.code}" </c:if>
                            <c:if test="${entry.basePrice.value ne null}"> data-attr-price="${entry.basePrice.value}" </c:if>                           
                            <c:if test="${entry.quantity ne null}"> data-attr-initialQuantity="${entry.quantity}" </c:if>
                            <c:if test="${currentCurrency.isocode ne null}"> data-attr-currencyCode="${currentCurrency.isocode}" </c:if>
                            <c:if test="${entry.product.manufacturer ne null}"> data-attr-devicebrand="${entry.product.manufacturer}" </c:if>                                                     
						    <c:choose>
						    <c:when test="${fn:contains(entry.product.categories[0].code, 'brand') || fn:startsWith(entry.product.categories[0].code, 'brand')}">
						     <c:if test="${entry.product.categories[1].name ne null}"> data-attr-brand="${entry.product.categories[1].name}" data-attr-category="${entry.product.categories[1].name}" </c:if>
                             <c:if test="${entry.product.categories[1].parentCategoryName ne null}"> data-attr-category_2="${entry.product.categories[1].parentCategoryName}" </c:if>						    
						     </c:when>
						    <c:otherwise>
						     <c:if test="${entry.product.categories[0].name ne null}"> data-attr-brand="${entry.product.categories[0].name}" data-attr-category="${entry.product.categories[0].name}" </c:if>
                             <c:if test="${entry.product.categories[0].parentCategoryName ne null}"> data-attr-category_2="${entry.product.categories[0].parentCategoryName}" </c:if>
						    </c:otherwise>
						    </c:choose>
                        >
						<input type='button' value='-' class='qtyminus qtybtn' field='quantity'	/>
					<form:input readonly="${(cmsSite.uid eq 'panama') || (cmsSite.uid eq 'cabletica')}" disabled="${not entry.updateable}" type="text" size="1" id="quantity${entry.entryNumber}"
class="item__quantity form-control js-update-entry-quantity-input cart-qty" path="quantity" />
						<input type='button' value='&plus;' class='qtyplus qtybtn' field='quantity'
						    <c:if test="${cmsSite.uid eq 'cabletica' && addonMaxQty == entry.quantity}">disabled </c:if>
						    <c:if test="${cmsSite.uid eq 'panama' && (entry.product.categories[0].code == 'devices' || entry.product.categories[0].code == '4Pbundles' || entry.product.categories[0].code == '3Pbundles' || addonMaxOrderQuantityMap[entry.product.code] == entry.quantity)}">disabled </c:if>
						/>
					</div>
				</c:otherwise>
			</c:choose>
		</form:form>
	</div>
</c:if>


