<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="telco-order" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/order"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

    
<c:forEach items="${order.unconsignedEntries}" var="entry" varStatus="loop">
	<telco-order:cableticaOrderEntryDetails orderEntry="${entry}"  order="${orderData}" />
</c:forEach>
