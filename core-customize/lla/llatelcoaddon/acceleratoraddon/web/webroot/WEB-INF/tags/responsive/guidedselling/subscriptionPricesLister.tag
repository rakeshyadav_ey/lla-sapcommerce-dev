<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="subscriptionData" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="price" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/price" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:theme code="offer.price.startingFrom" text="Starting from" var="startingFromText"/>

<c:if test="${not empty subscriptionData.mainSpoPriceInBpo}">
	<spring:theme code="product.payNow" text="Pay Now" var="billingTimeText"/>
	<c:if test="${empty subscriptionData.mainSpoPriceInBpo.recurringChargeEntries and
				empty subscriptionData.mainSpoPriceInBpo.oneTimeChargeEntries}">
		${startingFromText}
		<br>
		<format:price priceData="${subscriptionData.mainSpoPriceInBpo}"/>
		<br>
		${billingTimeText}
		<br>
	</c:if>
	<price:recurringChargesLister recurringCharges="${subscriptionData.mainSpoPriceInBpo.recurringChargeEntries}"
											billingTime="${subscriptionData.subscriptionTerm.billingPlan.billingTime.name}"/>
	<price:oneTimeChargesLister oneTimeCharges="${subscriptionData.mainSpoPriceInBpo.oneTimeChargeEntries}"/>
</c:if>
<br>
<c:choose>
	<c:when test="${not empty subscriptionData.additionalSpoPriceInBpo}">
		<c:if test="${empty subscriptionData.additionalSpoPriceInBpo.recurringChargeEntries and
				empty subscriptionData.additionalSpoPriceInBpo.oneTimeChargeEntries}">
			${startingFromText}
			<br>
			<format:price priceData="${subscriptionData.additionalSpoPriceInBpo}"/>
			<br>
			${billingTimeText}
			<br>
		</c:if>
		<price:recurringChargesLister recurringCharges="${subscriptionData.additionalSpoPriceInBpo.recurringChargeEntries}"
												billingTime="${subscriptionData.subscriptionTerm.billingPlan.billingTime.name}"/>
		<price:oneTimeChargesLister oneTimeCharges="${subscriptionData.additionalSpoPriceInBpo.oneTimeChargeEntries}"/>
	</c:when>
	<c:otherwise>
		<price:recurringChargesLister recurringCharges="${subscriptionData.price.recurringChargeEntries}"
												billingTime="${subscriptionData.subscriptionTerm.billingPlan.billingTime.name}"/>
		<price:oneTimeChargesLister oneTimeCharges="${subscriptionData.price.oneTimeChargeEntries}"/>
	</c:otherwise>
</c:choose>
