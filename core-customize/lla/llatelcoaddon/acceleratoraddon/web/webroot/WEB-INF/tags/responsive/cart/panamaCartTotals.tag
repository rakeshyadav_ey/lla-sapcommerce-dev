<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="showTaxEstimate" required="false" type="java.lang.Boolean"%>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/cart" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<c:set var="payNowVisible" value="false" />
<c:forEach items="${cartData.rootGroups}" var="entryGroup" varStatus="count" >
    <c:forEach var="item" items="${entryGroup.orderEntries[0].product.categories}">
      <c:if test="${item.code eq 'devices' || item.code eq 'prepaid'}">
         <c:set var="payNowVisible" value="true" />
      </c:if>
    </c:forEach>
</c:forEach>
<c:if test="${payNowVisible}">
    <div class="clearfix rightsideProductlist">
        <table id="order-totals" >
            <thead>
                <tr>
                    <p class="mainInfo"><spring:theme code="text.panama.produtlist.maintext" text="Currently in your cart" /></p>
                    <p class="subInfo">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" class="bi bi-exclamation-circle" viewBox="0 0 16 16">
                          <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                          <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
                        </svg>
                        <spring:theme code="text.panama.produtlist.subtext" text="Currently in your cart" />
                    </p>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${cartData.rootGroups}" var="entryGroup" varStatus="count" >
                    <c:set var="isDeviceProduct" value="false" />
                        <c:forEach var="item" items="${entryGroup.orderEntries[0].product.categories}">
                          <c:if test="${item.code eq 'devices' }">
                             <c:set var="isDeviceProduct" value="true" />
                          </c:if>
                        </c:forEach>
                        <c:if test="${entryGroup.orderEntries[0].product.categories[0].code == 'prepaid' || isDeviceProduct }">
                        <tr>
                            <td class="cart-bundle-package">${entryGroup.orderEntries[0].product.name}</td>
                            <td><format:price priceData="${entryGroup.orderEntries[0].totalPrice}" /></td>
                        </tr>
                    </c:if>
                </c:forEach>
                <tr>
                    <td class="cart-bundle-package"><spring:theme code="basket.page.totals.grossTax.noArgs" text="Tax"/></td>
                    <c:forEach items="${cartData.orderPrices}" var="orderPrice">
                        <td><format:price priceData="${orderPrice.totalTax}" displayFreeForZero="FALSE" /></td>
                    </c:forEach>
                </tr>
                <tr>
                    <td class="cart-bundle-package"><spring:theme code="text.panama.subtotal.paynow" text="PayNowAmount"/></td>
                    <td><format:price priceData="${cartData.totalPriceWithTax}" /></td>
                </tr>
            </tbody>
        </table>
    </div>
</c:if>

<c:set var="monthlyVisible" value="false" />
<c:forEach items="${cartData.rootGroups}" var="entryGroup" varStatus="count" >
    <c:forEach var="item" items="${entryGroup.orderEntries[0].product.categories}">
      <c:if test="${item.code ne 'prepaid'}">
         <c:set var="monthlyVisible" value="true" />
      </c:if>
    </c:forEach>
</c:forEach>

<c:if test="${monthlyVisible}">
<div class="cart-total-summary">
    <table id="order-totals">
        <thead>
            <tr>
                <c:if test="${cmsSite.uid eq 'panama'}">
                    <th colspan="2">
                        <p class="mainHead"><spring:theme code="text.panama.items.in.cart" text="Currently in your cart" /></p>
                        <p class="subHead">
                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" class="bi bi-exclamation-circle" viewBox="0 0 16 16">
                                  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                  <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
                                </svg>
                            <spring:theme code="text.panama.items.in.cart.subheading"/>
                        </p>
                    </th>
                </c:if>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${cartData.rootGroups}" var="entryGroup" varStatus="count" >
                <c:set var="isDeviceProductData" value="false" />
                <c:forEach var="item" items="${entryGroup.orderEntries[0].product.categories}">
                   <c:if test="${item.code eq 'devices' }">
                      <c:set var="isDeviceProductData" value="true" />
                   </c:if>
                </c:forEach>
                <c:if test="${cmsSite.uid eq 'panama' && !isDeviceProductData}">
                    <tr>
                        <td class="cart-bundle-package">${entryGroup.orderEntries[0].product.name}</td>
                           <c:forEach items="${entryGroup.orderEntries[0].orderEntryPrices}" var="orderEntryPrice">
                            <c:set var="formattedValue"><fmt:formatNumber type="number" minFractionDigits="2" value="${orderEntryPrice.monthlyPrice.value}" /></c:set>
                          <td>${currentCurrency.symbol} ${fn:replace(formattedValue, ',', '.')}</td>                    
                        </c:forEach>
                   </tr>
                </c:if>
            </c:forEach>
            <c:forEach items="${cartData.entries}" var="entryGroup" >
                <c:if test="${freeSim eq true && entryGroup.product.categories[0].name eq 'postpaid'}">
                    <tr>
                        <td class="cart-bundle-package"><spring:theme code="basket.page.totals.freeSim" text="free Sim"/></td>
                        <td>${currentCurrency.symbol}0.00</td>
                    </tr>
                </c:if>
            </c:forEach>
            <%--<c:choose>
                <c:when test="${freeShipping eq true && freeInstalltion eq true}">
                    <tr>
                        <td class="cart-bundle-package"><spring:theme code="basket.page.totals.freeShippingAndInstallation" text="free Shipping And Installation"/></td>
                        <td>${currentCurrency.symbol}0.00</td>
                    </tr>
                </c:when>
                <c:otherwise>
                    <c:if test="${freeShipping eq true}">
                        <tr>
                            <td class="cart-bundle-package"><spring:theme code="basket.page.totals.freeShipping" text="free Shipping"/></td>
                            <td>${currentCurrency.symbol}0.00</td>
                        </tr>
                    </c:if>
                    <c:if test="${freeInstalltion eq true}">
                        <tr>
                            <td class="cart-bundle-package"><spring:theme code="basket.page.totals.freeInstalltion" text="free Installtion"/></td>
                            <td>${currentCurrency.symbol}0.00</td>
                        </tr>
                    </c:if>
                </c:otherwise>
            </c:choose> --%>
            <tr>
                <td class="cart-bundle-package"><spring:theme code="basket.page.totals.freeShipping" text="free Shipping"/></td>
                <td>${currentCurrency.symbol}0.00</td>
            </tr>
            <tr class="cartTotalMonthly">
                <c:forEach items="${cartData.orderPrices}" var="orderPrice">
                   <c:if test="${orderPrice.monthlyPrice ne '' or orderPrice.monthlyPrice ne null}">
                     <c:set var="orderMonthlyTotal" value="${orderPrice.monthlyPrice}"/>
                   </c:if>
                </c:forEach>
               <td class="cart-bundle-package"><spring:theme code="text.monthly.payment.with.taxInfo" text="Total monthly payment (ITBMS no include)" htmlEscape="false" /></td>

               <td><format:price priceData="${orderMonthlyTotal}" /></td>

            </tr>
            <cart:taxExtimate cartData="${cartData}" showTaxEstimate="${showTaxEstimate}" />
        </tbody>
    </table>
</div>
</c:if>

<input type="hidden" id="cartPagePriceInfo"
    <c:if test="${cartData.subTotal ne null}"> data-attr-subtotal="${cartData.subTotal.formattedValue}" </c:if>
    <c:if test="${cartData.totalDiscounts ne null}"> data-attr-savings="${cartData.totalDiscounts.formattedValue}" </c:if>
    <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
    <c:if test="${cartData.totalTax ne null}"> data-attr-taxRate='${cartData.totalTax.formattedValue}' </c:if>
    <c:if test="${cartData.totalPriceWithTax ne null}"> data-attr-cartTotal="${cartData.totalPriceWithTax.formattedValue}" </c:if>
/>
