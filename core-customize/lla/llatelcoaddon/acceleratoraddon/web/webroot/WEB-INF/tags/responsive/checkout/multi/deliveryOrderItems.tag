<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="showDeliveryAddress" required="false" type="java.lang.Boolean"%>
<%@ attribute name="showPotentialPromotions" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="order-entry" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/entry"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="multi-checkout-telco" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<c:set var="hasDeliveryItems" value="${cartData.deliveryItemsQuantity > 0}" />
<c:set var="deliveryAddress" value="${cartData.deliveryAddress}" />
<c:set var="installationAddress" value="${cartData.installationAddress}" />
<spring:url value="/plp/postpaid" var="postpaidPLPUrl"/>
<spring:url value="/plp/prepaid" var="prepaidPLPUrl"/>
<%@ attribute name="monthlyTotalPrice" required="false"  type="de.hybris.platform.commercefacades.product.data.PriceData"%>

<div class="checkout-order-summary">
	<div class="today-totals-head">
		<spring:theme code="text.cart.items.monthly.total.head" htmlEscape="false" />
	</div>
	<c:choose>
		<c:when test="${cmsSite.uid eq 'jamaica'}">
		     <c:choose>
		        <c:when test="${orderData.entries[0].discountFlag eq 'true'}">
                    <ul class="monthly-payment-info discounted-payment">
                      <li class="price-total paddingTopBottom0">
                            <div class="details textMagentaColor"><spring:theme code="basket.page.total.checkout.monthly" text="Total" /></div>
                            <div class="price textMagentaColor"><format:price priceData="${orderData.monthlyTotalPrice}" /></div>
                         </li>
                         <li class="info-text">
                             <p class="textMagentaColor"><spring:theme code="text.checkout.payment.first"/>&nbsp;${orderData.discountNumber}&nbsp;${orderData.discountPeriod.code}</p>
                             <p class="textMagentaColor"><spring:theme code="text.checkout.monthly.disclaimer" /></p>
                        </li>
                        <li class="price-total">
                            <div class="details"><spring:theme code="basket.page.regular.checkout.monthly" text="Total" />
                            	<p class="sm-label-text "><spring:theme code="text.checkout.monthly.disclaimer" /></p>
                            </div>
                            <div class="price"><format:price priceData="${orderData.regularMonthlyPrice}" /></div>
                        </li>

                    </ul>
             </c:when>
         <c:otherwise>
           <ul class="monthly-payment-info">
                <c:forEach items="${orderData.entries}" var="entry" varStatus="loop">
                   <li>
                        <div class="details">
                            <div class="name">
                                <c:choose>
                                    <c:when test="${entry.product.categories[0].code eq 'addon' || entry.product.categories[0].code eq 'postpaid' || entry.product.categories[0].code eq 'prepaid'}">
                                        ${ycommerce:encodeHTML(entry.product.name)}
                                    </c:when>
                                    <c:otherwise>
                                        <spring:theme code="basket.page.bundle.text" />&nbsp;${ycommerce:encodeHTML(entry.product.name)}
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        <div class="price">
                            <c:forEach items="${entry.orderEntryPrices}" var="orderEntryPrice">
                                <c:set var="orderEntryMonthlyPrice" value="${orderEntryPrice.monthlyPrice}" />
                                <c:if test="${orderEntryPrice.basePrice.value != null}">
                                    <div class="actualPrice">
                                        <order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryPrice.totalPrice}"
                                            orderEntryBasePrice="${orderEntryPrice.basePrice}" orderEntryMonthlyPrice="${orderEntryMonthlyPrice}"  monthlyTotalPrice="${cartData.monthlyTotalPrice}"/>
                                    </div>
                                    <input type="hidden" class="productGtmData"
                                        <c:if test="${entry.product.name ne null}"> data-attr-name="${entry.product.name}" </c:if>
                                        <c:if test="${entry.product.code ne null}"> data-attr-id="${entry.product.code}" </c:if>
                                        <c:if test="${orderEntryPrice.totalPrice.value ne null}"> data-attr-price="${orderEntryPrice.totalPrice.value}" </c:if>
                                        <c:if test="${entry.product.categories[0].name ne null}"> data-attr-brand="${entry.product.categories[0].name}" </c:if>
                                        <c:if test="${entry.product.categories[0].name ne null}"> data-attr-category="${entry.product.categories[0].name}" </c:if>
                                        <c:if test="${entry.product.categories[0].parentCategoryName ne null}"> data-attr-category_2="${entry.product.categories[0].parentCategoryName}" </c:if>
                                        <c:if test="${entry.quantity ne null}"> data-attr-quantity="${entry.quantity}" </c:if>
                                        <c:if test="${cmsSite.uid eq 'panama' && entry.product.categories[0].code == 'devices'}">
                                            <c:if test="${entry.product.manufacturer ne null}"> data-attr-devicebrand="${entry.product.manufacturer}" </c:if>
                                        </c:if>
                                    >
                                </c:if>
                            </c:forEach>
                        </div>
                    </li>
                    </c:forEach>
                    <li class="price-total">
                                <div class="details"><spring:theme code="basket.page.total.checkout.monthly" text="Total" /></div>
                                <div class="price"><format:price priceData="${orderData.monthlyTotalPrice}" /></div>
                            </li>
                            <li class="gtc-text">
                                <spring:theme code="text.checkout.monthly.disclaimer" />
                            </li>
                   </ul>
            </c:otherwise>
          </c:choose>
	 </c:when>
<c:otherwise>
	<ul class="monthly-payment-info">
		<c:forEach items="${orderData.entries}" var="entry" varStatus="loop">
			<li>
				<div class="details">
					<div class="name">
						<c:choose>
							<c:when test="${entry.product.categories[0].code eq 'addon' || entry.product.categories[0].code eq 'postpaid' || entry.product.categories[0].code eq 'prepaid'}">
								${ycommerce:encodeHTML(entry.product.name)}
							</c:when>
							<c:otherwise>
								<spring:theme code="basket.page.bundle.text" />&nbsp;${ycommerce:encodeHTML(entry.product.name)}
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="price">
					<c:forEach items="${entry.orderEntryPrices}" var="orderEntryPrice">
						<c:set var="orderEntryMonthlyPrice" value="${orderEntryPrice.monthlyPrice}" />
						<c:if test="${orderEntryPrice.basePrice.value != null}">
							<div class="actualPrice">
								<order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryPrice.totalPrice}"
									orderEntryBasePrice="${orderEntryPrice.basePrice}" orderEntryMonthlyPrice="${orderEntryMonthlyPrice}"  monthlyTotalPrice="${cartData.monthlyTotalPrice}"/>
							</div>
							<input type="hidden" class="productGtmData"
								<c:if test="${entry.product.name ne null}"> data-attr-name="${entry.product.name}" </c:if>
								<c:if test="${entry.product.code ne null}"> data-attr-id="${entry.product.code}" </c:if>
								<c:if test="${orderEntryPrice.totalPrice.value ne null}"> data-attr-price="${orderEntryPrice.totalPrice.value}" </c:if>
								<c:if test="${entry.product.categories[0].name ne null}"> data-attr-brand="${entry.product.categories[0].name}" </c:if>
								<c:if test="${entry.product.categories[0].name ne null}"> data-attr-category="${entry.product.categories[0].name}" </c:if>
								<c:if test="${entry.product.categories[0].parentCategoryName ne null}"> data-attr-category_2="${entry.product.categories[0].parentCategoryName}" </c:if>
								<c:if test="${entry.quantity ne null}"> data-attr-quantity="${entry.quantity}" </c:if>
								<c:if test="${cmsSite.uid eq 'panama' && entry.product.categories[0].code == 'devices'}">
									<c:if test="${entry.product.manufacturer ne null}"> data-attr-devicebrand="${entry.product.manufacturer}" </c:if>
								</c:if>
							>
						</c:if>
					</c:forEach>
				</div>
			</li>
		</c:forEach>
		<li class="price-total">
			<div class="details"><spring:theme code="basket.page.total.checkout.monthly" text="Total" /></div>
			<div class="price"><format:price priceData="${orderData.monthlyTotalPrice}" /></div>
		</li>
		<li>
			<spring:theme code="text.checkout.monthly.disclaimer" />
		</li>
		</ul>
</c:otherwise>
</c:choose>

	<input type="hidden" class="productGtmDataPaymentMethod" data-attr-cardtype="${cartData.paymentInfo.cardTypeData.name}" >
</div>

<div class="checkout-order-summary">
	<div class="today-totals-head">
		<spring:theme code="text.cart.items.today.total.head" htmlEscape="false" />
	</div>
	<c:choose>
	 <c:when test="${cmsSite.uid eq 'jamaica'}">
         <c:choose>
           <c:when test="${orderData.entries[0].discountFlag eq 'true'}">
                    <ul class="today-payment-info">
                    <c:forEach items="${orderData.entries}" var="entry" varStatus="loop">
                        <!-- <li>
                          <spring:theme code="text.cart.items.deposit" />-${ycommerce:encodeHTML(entry.product.name)}
                        </li> -->
                        <li>
								<div class="details">
									<div class="name">
									<c:choose>
										<c:when test="${entry.product.categories[0].code eq 'addon' || entry.product.categories[0].code eq 'postpaid' || entry.product.categories[0].code eq 'prepaid'}">
										<spring:theme code="text.cart.items.deposit" />&nbsp;-&nbsp;${ycommerce:encodeHTML(entry.product.name)}
										</c:when>
										<c:otherwise>
										<spring:theme code="text.cart.items.deposit" />&nbsp;-&nbsp;${ycommerce:encodeHTML(entry.product.name)}
										</c:otherwise>
									</c:choose>
									</div>
								</div>
								<c:if test="${orderData.entries[0].discountFlag eq 'false'}">
								<div class="price">
									<c:forEach items="${entry.orderEntryPrices}" var="orderEntryPrice">
									<c:set var="orderEntryMonthlyPrice" value="${orderEntryPrice.monthlyPrice}" />
										<c:if test="${orderEntryPrice.basePrice.value != null}">
										<div class="actualPrice">
										<order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryPrice.totalPrice}"
										orderEntryBasePrice="${orderEntryPrice.basePrice}" orderEntryMonthlyPrice="${orderEntryMonthlyPrice}" monthlyTotalPrice="${cartData.monthlyTotalPrice}"/>
										</div>
										</c:if>
									</c:forEach>
								</div>
								</c:if>
							</li>
							<c:if test="${orderData.entries[0].discountFlag eq 'true'}">
							<li class="promo-order">
								<div class="details">
									<div class="name"><spring:theme code="text.cart.items.deposit" /></div>
								</div>
								<div class="price">
									<c:forEach items="${entry.orderEntryPrices}" var="orderEntryPrice">
									<c:set var="orderEntryMonthlyPrice" value="${orderEntryPrice.monthlyPrice}" />
										<c:if test="${orderEntryPrice.basePrice.value != null}">
										<div class="actualPrice">
										<order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryPrice.totalPrice}"
										orderEntryBasePrice="${orderEntryPrice.basePrice}" orderEntryMonthlyPrice="${orderEntryMonthlyPrice}" monthlyTotalPrice="${cartData.monthlyTotalPrice}"/>
										</div>
										</c:if>
									</c:forEach>
								</div>
							</li>
							</c:if>
                    </c:forEach>
                    <li class="today-tax">
                        <div class="details"><spring:theme code="basket.page.totals.grossTaxes.noArgs" text="Tax"/></div>
                        <div class="price">${currentCurrency.symbol}0.00
                        </div>
                    </li>
                    <li class="price-total">
                        <div class="details"><spring:theme code="basket.page.total.checkout.today" text="Total" /></div>
                        <div class="price"><format:price priceData="${orderData.totalPriceWithTax}" /></div>
                        <input type="hidden" id="orderConfirmationPagePriceInfo"
                             <c:if test="${orderData.subTotal ne null}"> data-attr-subtotal="${orderData.subTotal.formattedValue}" </c:if>
                             <c:if test="${orderData.totalDiscounts ne null}"> data-attr-savings="${orderData.totalDiscounts.formattedValue}" </c:if>
                             <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
                             <c:if test="${orderData.totalTax ne null}"> data-attr-taxRate='${orderData.totalTax.formattedValue}' </c:if>
                             <c:if test="${orderData.totalPriceWithTax ne null}"> data-attr-cartTotal="${orderData.totalPriceWithTax.formattedValue}" </c:if>
                         />
                    </li>
                </ul>
     </c:when>
     <c:otherwise>
            <ul class="today-payment-info">
     			<c:forEach items="${orderData.entries}" var="entry" varStatus="loop">
     			<li>
     				<div class="details">
     					<div class="name">
     						<c:choose>
     							<c:when test="${entry.product.categories[0].code eq 'addon' || entry.product.categories[0].code eq 'postpaid' || entry.product.categories[0].code eq 'prepaid'}">
     								<spring:theme code="text.cart.items.deposit" />&nbsp;-&nbsp;${ycommerce:encodeHTML(entry.product.name)}
     							</c:when>
     							<c:otherwise>
     								 <spring:theme code="text.cart.items.deposit" />&nbsp;-&nbsp;${ycommerce:encodeHTML(entry.product.name)}
     							</c:otherwise>
     						</c:choose>
     					</div>
     				</div>
     				<div class="price">
     					<c:forEach items="${entry.orderEntryPrices}" var="orderEntryPrice">
     						<c:set var="orderEntryMonthlyPrice" value="${orderEntryPrice.monthlyPrice}" />
     						<c:if test="${orderEntryPrice.basePrice.value != null}">
     							<div class="actualPrice">
     								<order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryPrice.totalPrice}"
     									orderEntryBasePrice="${orderEntryPrice.basePrice}" orderEntryMonthlyPrice="${orderEntryMonthlyPrice}"  monthlyTotalPrice="${cartData.monthlyTotalPrice}"/>
     							</div>
     						</c:if>
     					</c:forEach>
     				</div>
     			</li>
     			</c:forEach>
     		<li class="today-tax">
     			<div class="details"><spring:theme code="basket.page.totals.grossTaxes.noArgs" text="Tax"/></div>
     			<div class="price">${currentCurrency.symbol}0.00
     			</div>
     		</li>
     		<li class="price-total">
     			<div class="details"><spring:theme code="basket.page.total.checkout.today" text="Total" /></div>
     			<div class="price"><format:price priceData="${orderData.totalPriceWithTax}" /></div>
     			<input type="hidden" id="orderConfirmationPagePriceInfo"
                     <c:if test="${orderData.subTotal ne null}"> data-attr-subtotal="${orderData.subTotal.formattedValue}" </c:if>
                     <c:if test="${orderData.totalDiscounts ne null}"> data-attr-savings="${orderData.totalDiscounts.formattedValue}" </c:if>
                     <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
                     <c:if test="${orderData.totalTax ne null}"> data-attr-taxRate='${orderData.totalTax.formattedValue}' </c:if>
                     <c:if test="${orderData.totalPriceWithTax ne null}"> data-attr-cartTotal="${orderData.totalPriceWithTax.formattedValue}" </c:if>
                 />
     		</li>
     	    </ul>
       </c:otherwise>
      </c:choose>
</c:when>
<c:otherwise>
     <ul class="today-payment-info">
		<c:forEach items="${orderData.entries}" var="entry" varStatus="loop">
			<li>
				<div class="details">
					<div class="name">
						<c:choose>
							<c:when test="${entry.product.categories[0].code eq 'addon' || entry.product.categories[0].code eq 'postpaid' || entry.product.categories[0].code eq 'prepaid'}">
								${ycommerce:encodeHTML(entry.product.name)}
							</c:when>
							<c:otherwise>
								<spring:theme code="basket.page.bundle.text" />&nbsp;${ycommerce:encodeHTML(entry.product.name)}
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="price">
					<c:forEach items="${entry.orderEntryPrices}" var="orderEntryPrice">
						<c:set var="orderEntryMonthlyPrice" value="${orderEntryPrice.monthlyPrice}" />
						<c:if test="${orderEntryPrice.basePrice.value != null}">
							<div class="actualPrice">
								<order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryPrice.totalPrice}"
									orderEntryBasePrice="${orderEntryPrice.basePrice}" orderEntryMonthlyPrice="${orderEntryMonthlyPrice}"  monthlyTotalPrice="${cartData.monthlyTotalPrice}"/>
							</div>
						</c:if>
					</c:forEach>
				</div>
			</li>
		</c:forEach>
		<li class="today-tax">
			<div class="details"><spring:theme code="basket.page.totals.grossTaxes.noArgs" text="Tax"/></div>
			<div class="price">${currentCurrency.symbol}0.00
			</div>
		</li>
		<li class="price-total">
			<div class="details"><spring:theme code="basket.page.total.checkout.today" text="Total" /></div>
			<div class="price"><format:price priceData="${orderData.totalPriceWithTax}" /></div>
			<input type="hidden" id="orderConfirmationPagePriceInfo"
                <c:if test="${orderData.subTotal ne null}"> data-attr-subtotal="${orderData.subTotal.formattedValue}" </c:if>
                <c:if test="${orderData.totalDiscounts ne null}"> data-attr-savings="${orderData.totalDiscounts.formattedValue}" </c:if>
                <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
                <c:if test="${orderData.totalTax ne null}"> data-attr-taxRate='${orderData.totalTax.formattedValue}' </c:if>
                <c:if test="${orderData.totalPriceWithTax ne null}"> data-attr-cartTotal="${orderData.totalPriceWithTax.formattedValue}" </c:if>
            />
		</li>
	</ul>
</c:otherwise>
</c:choose>

	<input type="hidden" class="productGtmDataPaymentMethod" data-attr-cardtype="${cartData.paymentInfo.cardTypeData.name}" >
</div>
