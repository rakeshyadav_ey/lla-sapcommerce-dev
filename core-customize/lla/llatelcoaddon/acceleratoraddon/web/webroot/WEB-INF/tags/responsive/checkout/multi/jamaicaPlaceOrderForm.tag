<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ attribute name="placeOrderUrl" required="true"  type="java.lang.String"%>
<%@ attribute name="cartCalculationMessage" required="true"  type="java.lang.String"%>
<%@ attribute name="getTermsAndConditionsUrl" required="true"  type="java.lang.String"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="multi-checkout-telco" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>

<div class="visible-xs">
<multi-checkout-telco:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="true" showPaymentInfo="true" />
</div>

<multi-checkout-telco:monthlyDeliveryCartItems cartData="${cartData}" showDeliveryAddress="${showDeliveryAddress}" />
					
<div class="place-order-form">
   <form:form action="${placeOrderUrl}" id="placeOrderForm1" modelAttribute="placeOrderForm">
      <div class="checkbox">
         <label class="custom-label"> 
            <spring:theme code="checkout.summary.placeOrder.readTermsAndConditions1" />
            <form:checkbox id="Terms1" path="termsCheck1" />
            <span class="checkmark"></span>
         </label> 
         <label class="bold custom-label"> 
            <spring:theme code="checkout.summary.placeOrder.readTermsAndConditions2"
               arguments="${getTermsAndConditionsUrl}" text="click here"
               htmlEscape="false" />
            <form:checkbox id="Terms2" path="termsCheck2" />
            <span class="checkmark"></span>
         </label>
      </div>
      <input type="hidden" id="termsCheck2_error" value="<spring:theme code="placeorderform.validation.error.message"/>">
      <spring:theme code="checkout.summary.placeOrder" text="Place Order" var="placeOrderText" />

      <button type="submit" class="btn btn-block btn-primary btn-place-order" id="placeOrder" title="${placeOrderText}">
         ${placeOrderText}
      </button>
   </form:form>
</div>
<%--<div class="contactus">
   <h4>
      <b>¿any Questions?</b><br> Give us a call, to assist you!</h4>
   <ul>
      <li class="call">
         Phone number: <a href="tel:1-800-804-2994">1-800-804-2994</a>
      </li>
   </ul>
</div>--%>