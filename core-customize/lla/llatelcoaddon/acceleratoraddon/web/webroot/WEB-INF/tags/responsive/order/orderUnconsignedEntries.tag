<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="telco-order" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/order"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<c:if test="${cmsSite.uid eq 'jamaica'}">
    <div class="well-quinary well-xs border-info">
<!--         <div class="well-headline orderPending"> -->
<%--             <spring:theme code="text.account.order.unconsignedEntry.status.pending" /> --%>
<!--         </div> -->

            <c:choose>
               <c:when test="${cmsSite.uid eq 'jamaica'}">
                <c:if test="${!hideInstallationAddress}">
                    <div class="well-headline">
                    	<div class="label-order">
                     <spring:theme code="text.account.paymentDetails.shippingAddress" text="Shipping Information"/>
                     </div>
                    </div>
                </c:if>
                </c:when>
                <c:otherwise>
                   <div class="well-headline">
                    <div class="label-order">
                    <spring:theme code="text.account.paymentDetails.shippingAddress" text="Shipping Information"/>
                    </div>
                   </div>
                </c:otherwise>
              </c:choose>


        <c:choose>
            <c:when test="${entry.deliveryPointOfService ne null}">
                <div class="well-content">
                    <div class="row">
                        <div class="col-sm-12 col-md-9">
                            <order:storeAddressItem deliveryPointOfService="${entry.deliveryPointOfService}" />
                        </div>
                    </div>
                </div>
            </c:when>
			<c:when test="${orderData.installationAddress ne null}">

			    <c:if test="${!hideInstallationAddress}">
                <div class="well-content  order-address-info">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="order-ship-to">
                                <div class="label-order">
                                    <spring:theme code="text.account.order.shipto" text="Shipping Address" />
                                </div>
                                <div class="value-order">
                                    <order:addressItem address="${orderData.installationAddress}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </c:if>
            </c:when>
            <c:otherwise>
                <c:if test="${cmsSite.uid ne 'jamaica' && 'panama'}">
                <div class="well-content  order-address-info">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="order-ship-to">
                                <div class="label-order">
                                    <spring:theme code="text.account.order.shipto" text="Shipping Address" />
                                </div>
                                <div class="value-order">
                                    <order:addressItem address="${orderData.deliveryAddress}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </c:if>
            </c:otherwise>
        </c:choose>
    </div>
</c:if>
   
<ul class="item__list">
	<li class="hidden-xs hidden-sm">
		<ul class="item__list--header">
			<c:choose>
				<c:when test="${cmsSite.uid eq 'jamaica'}">
					<li class="item__info"><spring:theme code="basket.page.itemStyle" text="Item (style number)" /></li>
				</c:when>
				<c:otherwise>
					<li class="item__info"><spring:theme code="basket.page.item" text="Item" /></li>
				</c:otherwise>
			</c:choose>
			<li class="item__image"></li>
			<li class="item__quantity"><spring:theme code="basket.page.qty" text="Qty" /></li>
<%-- 			<li class="item__delivery"><spring:theme code="basket.page.delivery" text="Delivery" /></li> --%>
			<c:if test="${cmsSite.uid ne 'panama'}">
            <c:forEach items="${orderData.orderPrices}" var="price">
				<li class="item__price">
					<c:choose>
					    <c:when test="${cmsSite.uid ne 'panama'}">
					    	${ycommerce:encodeHTML(price.billingTime.name)}
					    </c:when>    
					    <c:otherwise>
					    	<spring:theme code="basket.page.total" text="Total" />
					    </c:otherwise>
					</c:choose>
				</li>
			</c:forEach>
            </c:if>
            <c:if test="${cmsSite.uid eq 'panama'}">
                <li class="item__price">
                    <spring:theme code="basket.page.total" text="Total" />
				</li>
            </c:if>
		</ul>
	</li>
	<c:forEach items="${order.unconsignedEntries}" var="entry" varStatus="loop">
		<telco-order:orderEntryDetails orderEntry="${entry}"  order="${orderData}" />
	</c:forEach>
</ul>