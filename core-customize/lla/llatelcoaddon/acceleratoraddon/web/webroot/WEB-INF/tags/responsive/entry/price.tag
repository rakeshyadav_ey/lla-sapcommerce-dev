<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="orderEntry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="order-entry" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/entry"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="monthlyTotalPrice" required="false"  type="de.hybris.platform.commercefacades.product.data.PriceData"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ attribute name="totalPriceWithTax" required="false"  type="de.hybris.platform.commercefacades.product.data.PriceData"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<c:forEach items="${orderEntry.orderEntryPrices}" var="orderEntryPrice">
	<c:set var="orderEntryTotalPrice" value="${orderEntryPrice.totalPrice}" />
	<c:set var="orderEntryBasePrice" value="${orderEntryPrice.basePrice}" />
	<c:set var="orderEntryMonthlyPrice" value="${orderEntryPrice.monthlyPrice}" />

	<c:set var="orderEntryBillingName" value="${ycommerce:encodeHTML(orderEntryPrice.billingTime.name)}" />
	<c:set var="orderEntryRegularPrice" value="${orderEntry.actualPrice.formattedValue}" />
	<c:set var="orderEntryDiscountFlag" value="${orderEntry.discountFlag}" />
	<c:if test="${cmsSite.uid ne 'jamaica'}">
	<div class="item__price">
	</c:if>
		<c:choose>
            <c:when test="${cmsSite.uid eq 'panama'}">
                <c:set var="deviceProduct" value="false" />
                    <c:forEach var="item" items="${orderEntry.product.categories}">
                      <c:if test="${item.code eq 'devices'}">
                        <c:set var="deviceProduct" value="true" />
                  </c:if>
                </c:forEach>
                <c:choose>
                    <c:when test="${deviceProduct || orderEntry.product.categories[0].code == 'prepaid' }">
                        <spring:theme code="text.panama.subtotal.paynow" /> <br>
                        <b><format:price priceData="${orderEntryTotalPrice}" displayFreeForZero="false" /></b>
                    </c:when>
                    <c:otherwise>
                        <order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryTotalPrice}"
                                orderEntryBasePrice="${orderEntryBasePrice}" orderEntryMonthlyPrice="${orderEntryMonthlyPrice}" monthlyTotalPrice="${monthlyTotalPrice}"/>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:when test="${cmsSite.uid eq 'cabletica'}">
	            <div>
		            <span class="price-info"><spring:theme code="text.account.orderHistory.total"/>:</span><br/> 
		             <fmt:setLocale value="en_US" scope="session" />
		            <b>${currentCurrency.symbol}<fmt:formatNumber value="${totalPriceWithTax.value}" pattern="#,##0" />&nbsp;<spring:theme code="text.cabletica.IVA"/></b><br/>
		            <span class="item__price--monthly"><spring:theme code="text.ctaMonthlyplan"/></span>
	        	</div>
            </c:when>
            <c:otherwise>
                <order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryTotalPrice}"
                            orderEntryBasePrice="${orderEntryBasePrice}" orderEntryMonthlyPrice="${orderEntryMonthlyPrice}" monthlyTotalPrice="${monthlyTotalPrice}"
			    orderEntryRegularPrice="${orderEntryRegularPrice}" orderEntryDiscountFlag="${orderEntryDiscountFlag}" />
            </c:otherwise>
        </c:choose>

		<%-- <c:if test="${orderEntry.fixedLineProduct eq 'true'}">
			<c:if test="${cmsSite.uid eq 'jamaica'}">			
			<div class="fixedline-product">
				<spring:theme code="text.installation.waiveoff" /><br>
				<spring:theme code="text.security.deposit" />:&nbsp;<order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryTotalPrice}" orderEntryBasePrice="${orderEntryBasePrice}" />
			</div>
			</c:if>
		</c:if> --%>
	<c:if test="${cmsSite.uid ne 'jamaica'}">
	</div>
	</c:if>
	<%--<c:if test="${cmsSite.uid eq 'jamaica'}">
	<div class="item__price pull-right visible-sm col-xs-7">
		<order-entry:actualPriceMobile orderEntryTotalPrice="${orderEntryTotalPrice}" orderEntryBasePrice="${orderEntryBasePrice}"
			orderEntryBillingName="${orderEntryBillingName}" />
	</div> 
	<div class="item__price visible-xs">
		<order-entry:actualPriceMobile orderEntryTotalPrice="${orderEntryTotalPrice}" orderEntryBasePrice="${orderEntryBasePrice}"
			orderEntryBillingName="${orderEntryBillingName}" />
	</div>
	</c:if>--%>
</c:forEach>
