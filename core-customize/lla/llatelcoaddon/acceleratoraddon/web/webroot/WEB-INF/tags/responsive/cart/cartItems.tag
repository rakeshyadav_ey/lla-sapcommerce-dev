<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="store-pickup" tagdir="/WEB-INF/tags/responsive/storepickup"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/cart"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="order-entry" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/entry"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="configurator" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/configurator" %>
<c:set var="entryGroupNumber" value="1"/>
<spring:url value="/cart/delete" var="bpoDeleteAction" htmlEscape="false"/>
<spring:theme code="text.iconCartRemove" var="iconCartRemove" text="REMOVE"/>

<spring:htmlEscape defaultHtmlEscape="true" />

<ul class="item__list item__list__cart cart-item-list">
   <%--<li class="hidden-xs hidden-sm">
      <ul class="item__list--header">
         <li class="item__info">
            <spring:theme code="basket.page.item" text="Product Items" />
         </li>
         <li class="item__image"></li>
         <li class="item__quantity">
            <spring:theme code="basket.page.qty" text="Qty" />
         </li>
<!--         <li class="item__delivery"> -->
<!--            <spring:theme code="basket.page.delivery" text="Delivery" /> -->
<!--         </li> -->
         <c:forEach items="${cartData.orderPrices}" var="price" >
            <li class="item__price">${ycommerce:encodeHTML(price.billingTime.name)}</li>
         </c:forEach>
         <li class="item__remove"></li>
      </ul>
   </li> --%>

    <c:if test="${cmsSite.uid eq 'puertorico'}">
        <div>
         <c:if test="${not empty cartData.rootGroups}">
                       <div class="cart-header col-xs-12"> 
                       <h1><spring:theme code="text.cart" text="My Shopping Cart" /></h1>
                       <p><spring:theme code="text.cart.message.puertorico" text="Products added to you cart" /></p>
                       </div>
                     </c:if> 
            <div class="col-md-8 col-lg-8 col-sm-7 col-xs-7 left-align">
               <%--  <h3 class="item-head"><spring:theme code="basket.page.cart.item.head" text="At this time you add" /></h3> --%>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-5 col-xs-5">
                <c:if test="${not empty entryGroupNumber}">
                    <form:form id="deleteBpoForm${entryGroupNumber}" action="${bpoDeleteAction}" method="post"
                               modelAttribute="deleteBpoForm'${entryGroupNumber}" class="deleteForm">
                        <input type="hidden" name="groupNumber" value="${entryGroupNumber}"/>
                        <spring:theme code="text.iconCartRemove" var="iconCartRemove" text="REMOVE"/>
                        <a href="#" id="${entryGroupNumber}" class="submitRemoveBundle">
                            <%-- ${iconCartRemove} --%>                          
                            <span class="remove-text"><spring:theme code="basket.page.entry.action.REMOVE" text="Eliminar" /></span>
                        </a>
                    </form:form>
               </c:if>
            </div>
        </div>
    </c:if>
    <c:if test="${cmsSite.uid eq 'cabletica'}">
	    <div class="col-xs-2 pull-right text-center">
	    	<c:if test="${not empty entryGroupNumber}">
		       <form:form id="deleteBpoForm${entryGroupNumber}" action="${bpoDeleteAction}" method="post"
		                  modelAttribute="deleteBpoForm'${entryGroupNumber}" class="deleteForm">
		           <input type="hidden" name="groupNumber" value="${entryGroupNumber}"/>
		           <spring:theme code="text.iconCartRemove" var="iconCartRemove" text="REMOVE"/>
		           <span href="#" id="${entryGroupNumber}" class="submitRemoveBundle btnSpanData">
		               <%-- ${iconCartRemove} --%>
		               <span class="glyphicon glyphicon-trash remove"></span>
		           </span>
		       </form:form>
	        </c:if>
        </div>
    </c:if>
   <c:if test="${cmsSite.uid eq 'puertorico'}">
       <c:forEach items="${cartData.rootGroups}" var="entryGroup" varStatus="count" >
           <c:if test="${ entryGroup.orderEntries[0].product.categories[0].code eq 'internet'}">
               <cart:rootEntryGroup cartData="${cartData}" entryGroup="${entryGroup}" count="${count.index}" />
           </c:if>
       </c:forEach>
       <c:forEach items="${cartData.rootGroups}" var="entryGroup" varStatus="count" >
           <c:if test="${entryGroup.orderEntries[0].product.categories[0].code eq 'tv'}">
               <cart:rootEntryGroup cartData="${cartData}" entryGroup="${entryGroup}" count="${count.index}" />
           </c:if>
       </c:forEach>
       <c:forEach items="${cartData.rootGroups}" var="entryGroup" varStatus="count" >
           <c:if test="${entryGroup.orderEntries[0].product.categories[0].code eq 'phone'}">
               <cart:rootEntryGroup cartData="${cartData}" entryGroup="${entryGroup}" count="${count.index}" />
           </c:if>
       </c:forEach>
       <p class="myCartHeader">
       <span class="myCartMonthly"><spring:theme code="text.cart.monthly"/>*</span>
       <span class="myCartPrice">${currentCurrency.symbol}${cartData.bundlePrice.value}</span></p>
   </c:if>
   <c:if test="${cmsSite.uid eq 'cabletica'}">
      <c:forEach items="${cartData.rootGroups}" var="entryGroup" varStatus="count">
   		<c:choose>
   		<c:when test="${entryGroup.orderEntries[0].product.categories[0].code eq '3PInternetTvTelephone' || entryGroup.orderEntries[0].product.categories[0].code eq '2PInternetTv' || entryGroup.orderEntries[0].product.categories[0].code eq 'internetcabletica' || entryGroup.orderEntries[0].product.categories[0].code eq 'tvcabletica'}">
    		<cart:rootEntryGroup cartData="${cartData}" entryGroup="${entryGroup}" count="${count.index}" />
   		</c:when>
       <c:otherwise>
            <c:if test="${entryGroup.orderEntries[0].product.categories[0].code ne 'channelsaddoncabletica'}">
            <cart:rootEntryGroup cartData="${cartData}" entryGroup="${entryGroup}" count="${count.index}" />
        </c:if>
   	   </c:otherwise>
   		</c:choose>
	</c:forEach>
   </c:if>

   <c:if test="${cmsSite.uid ne 'puertorico' && cmsSite.uid ne 'cabletica'}">
       <c:forEach items="${cartData.rootGroups}" var="entryGroup" varStatus="count" >
           <cart:rootEntryGroup cartData="${cartData}" entryGroup="${entryGroup}" count="${count.index}" />
           <p></p>
       </c:forEach>
   </c:if>
   <!-- New Code -->
   <c:if test="${cmsSite.uid eq 'panama'}">
      <c:forEach items="${addonProductList}" var="product" varStatus="count" >
         <div class="clearfix cart-item-list-details addonlist">
             <div class="col-xs-4 col-sm-4">
                  <div class="item__image">
                       <product:productPrimaryImage product="${product}" format="thumbnail"/>
                  </div>
             </div>
             <div class="col-xs-7 col-sm-7">
                 <div class="item__info">
                     <span class="item__name">${ycommerce:encodeHTML(product.name)}</span>
                     <p class="addon-spec">${product.summary} &nbsp; <format:price priceData="${product.monthlyPrice}" /></p>
                 </div>
                 <div class="item__quantity">
                     <form:form id="addonAddtoCart" action="${contextPath}/addtocart" method="get">
                        <input type="hidden" name="productCodePost" value="${product.code}">
                        <input type="hidden" name="qty" value="1">
                        <input type="hidden" name="initialQuantity" value="1">                        
                        <label for="quantity" class="skip">Quantity</label>
                        <div class="quantity-box" data-qty-max="${addonMaxOrderQuantityMap[product.code]}">
                           <input type="button" value="-" class="addonqtyminus qtybtn" field="quantity" disabled="">
                           <input id="quantity" name="quantity" type="text" class="item__quantity form-control cart-qty" value="0" size="1" readonly>
                           <input type="button" value="+" class="addonqtyplus qtybtn" field="quantity">
                            <input type="hidden" id="addOnZeroAddon"  class="addOnZeroAddon"  
                            <c:if test="${product.name ne null}"> data-attr-name="${product.name}" </c:if>
                             <c:if test="${product.code ne null}"> data-attr-id="${product.code}" </c:if>
                            <c:if test="${product.monthlyPrice.value ne null}"> data-attr-price="${product.monthlyPrice.value}" </c:if>
                            <c:if test="${product.categories[0].name ne null}"> data-attr-brand="${product.categories[0].name}" data-attr-category="${product.categories[0].name}" </c:if>
                            <c:if test="${product.categories[0].parentCategoryName ne null}"> data-attr-category_2="${product.categories[0].parentCategoryName}" </c:if> >                                
                        </div>                                                         
                     </form:form>
                 </div>
                 <div class="item__price ">
                    <div><spring:theme code="text.monthly.payment" /> <br>
                       <b>${currentCurrency.symbol}0.00</b>
                    </div>
                 </div>
               </div>
          </div>
      <p></p>
      </c:forEach>
   </c:if>
    <c:if test="${cmsSite.uid eq 'cabletica' && not empty addonCableticaProductList}">
  	  <div class="cart-item">
	  <h3 class="item-head"><spring:theme code="basket.page.cart.channel.head"/></h3>
      <c:forEach items="${addonCableticaProductList}" var="addon" varStatus="count">
      <c:choose>
      <c:when test="${addon.code eq 'Telefonia_Plan'}">
			<div class="clearfix cart-item-list-details">
				<div class="col-xs-4 col-sm-3">
					<div class="item__image">
						<product:productPrimaryImage product="${addon}" format="thumbnail"/>
					</div>
                </div>
                <div class="col-xs-2 col-sm-1 deleteForm addondelForm hidden-lg hidden-md hidden-sm pull-right">
	               	<c:forEach items="${cartData.rootGroups}" var="entryGroup">
		                <c:if test="${entryGroup.orderEntries[0].product.code eq addon.code}">
		                	<order-entry:cartEntryQuantity entry="${entryGroup.orderEntries[0]}" />
	               			<order-entry:menu orderEntry="${entryGroup.orderEntries[0]}" />
	               		</c:if>
               		</c:forEach>
               	</div>
                <div class="col-xs-12 col-sm-8 hideqtybox">
                	<div class="item__info">
                		<span class="item__name">${addon.name}</span>
	                	<div class="product-specifications">
	                		${addon.description}
	                	</div>
                	</div>
	                <div class="item__price">
				        <div>
				            <span class="price-info"><spring:theme code="text.account.orderHistory.total"/>:</span><br>
				             <fmt:setLocale value="en_US" scope="session" />
				            <b>${currentCurrency.symbol}<fmt:formatNumber value="${addon.monthlyPrice.value}" pattern="#,##0" />&nbsp;<spring:theme code="text.cabletica.IVA"/></b><br>
				            <span class="item__price--monthly"><spring:theme code="text.ctaMonthlyplan"/></span>
				        </div>
	    			</div>
				    <div class="item__addCart">
				        <input type="hidden" class="addonTelephone" 
				            <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
				            <c:if test="${addon.name ne null}"> data-attr-name="${addon.name}" </c:if>
				            <c:if test="${addon.code ne null}"> data-attr-id="${addon.code}" </c:if>
				            <c:if test="${addon.monthlyPrice.value ne null}"> data-attr-price='<fmt:formatNumber value="${addon.monthlyPrice.value}" pattern="#,##0" />' </c:if>
				            <c:if test="${addon.categories[0].name ne null}"> data-attr-category="${addon.categories[0].name}" </c:if>
				            <c:if test="${addon.categories[0].parentCategoryName ne null}"> data-attr-primarycategory="${addon.categories[0].parentCategoryName}" </c:if>
				        />
			 			<form:form id="addonAddtoCart" action="${contextPath}/addtocart" method="get" class="ctabox-form ctabox-addtocart-form">
			                 <input type="hidden" name="productCodePost" id="productCodePost" value="${addon.code}"/>
			                 <input type="hidden" name="qty" value="1">
			                 <input type="hidden" name="initialQuantity" value="1">
			                 <input type="hidden" name="processType" value="DEVICE_ONLY">
			                 <div class="ctabox-addToCart-section">
			                	<button type="submit" class="btn cta-submit-btn ctabox-addToCart-btn">
			                       <spring:theme code="ctaBox.addToCart.button.text"/>
			                   </button>
			                 </div>
			             </form:form>
                 	</div>
                </div>
               	<div class="col-xs-1 col-sm-1 deleteForm addondelForm hidden-xs">
	               	<c:forEach items="${cartData.rootGroups}" var="entryGroup">
		                <c:if test="${entryGroup.orderEntries[0].product.code eq addon.code}">
		                	<order-entry:cartEntryQuantity entry="${entryGroup.orderEntries[0]}" />
	               			<order-entry:menu orderEntry="${entryGroup.orderEntries[0]}" />
	               		</c:if>
               		</c:forEach>
               	</div>
         	</div>
      	</c:when>
    	  <c:otherwise>
    	  </c:otherwise>
  	   </c:choose>
    </c:forEach>
    <br/>
    <div class="cl-cntnr">
		<ul class="cl-cntnr__list" data-cl-selected="">
		    <c:set var="displayAddonQty" value="0" />
		    <c:forEach items="${addonCableticaProductList}" var="addon" varStatus="count">
			    <c:if test="${addon.code ne 'Convertidor_Digital' && addon.code ne 'Telefonia_Plan'}">
					<li class="cl-cntnr__item">
					<input type="hidden" class="channelAddonData"
					    <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
                        <c:if test="${addon.name ne null}"> data-attr-name="${addon.name}" </c:if>
                        <c:if test="${addon.code ne null}"> data-attr-id="${addon.code}" </c:if>
                        <c:if test="${addon.monthlyPrice.value ne null}"> data-attr-price='<fmt:formatNumber value="${addon.monthlyPrice.value}" pattern="#,##0" />' </c:if>
                        <c:if test="${addon.categories[0].name ne null}"> data-attr-category="${addon.categories[0].name}" </c:if>
                        <c:if test="${addon.categories[0].parentCategoryName ne null}"> data-attr-primarycategory="${addon.categories[0].parentCategoryName}" </c:if>
                    />
			   		<span class="cl-cntnr__item-name"><span class="channel-name">${addon.name}</span><br/>${addon.description}</span>
			   	 	<span class="cl-cntnr__price">${currentCurrency.symbol}<fmt:formatNumber value="${addon.monthlyPrice.value}" pattern="#,##0" />&nbsp;<spring:theme code="text.cabletica.IVA"/><br/>
			   	 		<span class="price-monthly"><spring:theme code="text.ctaMonthlyplan"/></span>
			   	 	</span>
			   	 	<c:set var="displayVariable" value="show"/>
			   	 	<c:forEach items="${cartData.rootGroups}" var="entryGroup">
                        <c:if test="${entryGroup.orderEntries[0].product.code eq addon.code}">
                            <c:set var="displayVariable" value="hide"/>
                        </c:if>
                    </c:forEach>
			  	 	<div class="item__addon__check ${displayVariable}">
					<form:form id="addonAddtoCart" action="${contextPath}/addtocart" method="get" class="ctabox-form ctabox-addtocart-form">
			             <input type="hidden" name="productCodePost" id="productCodePost" value="${addon.code}"/>
			             <input type="hidden" name="qty" value="1">
			             <input type="hidden" name="initialQuantity" value="1">
			             <input type="hidden" name="processType" value="DEVICE_ONLY">
			             <div class="ctabox-addToCart-section">
						    <input type="checkbox" class="addOnCheckBox" name="addOnCheckBox" value="${addon.code}">
						</div>

			        </form:form>
			        </div>
		           <c:forEach items="${cartData.rootGroups}" var="entryGroup">
		           <c:if test="${entryGroup.orderEntries[0].product.code eq addon.code}">
		           <order-entry:cartEntryQuantity entry="${entryGroup.orderEntries[0]}" />
		           <order-entry:menu orderEntry="${entryGroup.orderEntries[0]}" />
		          </c:if>

		          <c:if test="${entryGroup.orderEntries[0].product.code eq 'Convertidor_Digital'}">
		                <c:set var="displayAddonQty" value="${entryGroup.orderEntries[0].quantity}" />
		          </c:if>
		           </c:forEach>
		   			</li>
			    </c:if>

			    <c:if test="${addon.code eq 'Convertidor_Digital'}">
				    <li class="cl-cntnr__item__digital convertAddon">
				        <input type="hidden" class="hdChannelAddonData"
				            <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
                            <c:if test="${addon.name ne null}"> data-attr-name="${addon.name}" data-attr-quantity=1</c:if>
                            <c:if test="${addon.code ne null}"> data-attr-id="${addon.code}" </c:if>
                            <c:if test="${addon.monthlyPrice.value ne null}"> data-attr-price='<fmt:formatNumber value="${addon.monthlyPrice.value}" pattern="#,##0" />' </c:if>
                            <c:if test="${addon.categories[0].name ne null}"> data-attr-category="${addon.categories[0].name}" </c:if>
                            <c:if test="${addon.categories[0].parentCategoryName ne null}"> data-attr-primarycategory="${addon.categories[0].parentCategoryName}" </c:if>
                        />
		       			<span class="cl-cntnr__item-name"><span class="channel-name">${addon.name}</span><br/>${addon.description}</span>
				   	 	<span class="cl-cntnr__price addon-price">${currentCurrency.symbol}<fmt:formatNumber value="${addon.monthlyPrice.value}" pattern="#,##0" />&nbsp;<spring:theme code="text.cabletica.IVA"/><br/>
				   	 		<span class="price-monthly"><spring:theme code="text.ctaMonthlyplan"/></span>
				   	 	</span>

				   	 	<c:if test="${displayAddonQty > 0}">
				   	 	<c:forEach items="${cartData.rootGroups}" var="entryGroup">
                           <c:if test="${entryGroup.orderEntries[0].product.code eq 'Convertidor_Digital'}">
                           <order-entry:cartEntryQuantity entry="${entryGroup.orderEntries[0]}" />
                           <order-entry:menu orderEntry="${entryGroup.orderEntries[0]}" />
                          </c:if>
                          </c:forEach>
                        </c:if>

                        <c:if test="${displayAddonQty == 0}">
		      			<div class="item__quantity">
		                    <form:form id="addonAddtoCart" action="${contextPath}/addtocart" method="get">
		                       <input type="hidden" name="productCodePost" value="${addon.code}">
		                       <input type="hidden" name="qty" value="${displayAddonQty}">
		                       <input type="hidden" name="initialQuantity" value="${displayAddonQty}">
		                       <label for="quantity" class="skip">Quantity</label>
		                       <div class="quantity-box" data-qty-max="${addonCableticaMaxOrderQuantityMap[addon.code]}">
		                          <input type="button" value="-" class="addonqtyminus qtybtn" field="quantity" <c:if test="${displayAddonQty == 0}">disabled=""</c:if>>
		                          <input id="quantity" name="quantity" type="text" class="item__quantity form-control cart-qty" value="${displayAddonQty}" size="1" readonly>
		                          <input type="button" value="+" class="addonqtyplus qtybtn" field="quantity">
		                       </div>
		                    </form:form>
		       			</div>
		       			</c:if>
		       		</li>
	      		</c:if>

      		</c:forEach>
		</ul>
	</div>
    </div>
   </c:if>
</ul>
<c:if test="${cmsSite.uid eq 'puertorico'}">
   <div><configurator:addonsSelection/>  </div>
    <c:forEach items="${cartData.rootGroups}" var="entryGroup" varStatus="count" >
       <input type="hidden" class="cartProducts"
           <c:if test="${entryGroup.orderEntries[0].product.name ne null}"> data-attr-name="${entryGroup.orderEntries[0].product.name}" </c:if>
           <c:if test="${entryGroup.orderEntries[0].product.code ne null}"> data-attr-id="${entryGroup.orderEntries[0].product.code}" </c:if>
           <c:if test="${entryGroup.orderEntries[0].orderEntryPrices[0].basePrice.value ne null}"> data-attr-price="${entryGroup.orderEntries[0].orderEntryPrices[0].basePrice.value}" </c:if>
           <c:if test="${entryGroup.orderEntries[0].quantity ne null}"> data-attr-quantity="${entryGroup.orderEntries[0].quantity}" </c:if>
           <c:if test="${entryGroup.orderEntries[0].product.categories[0].name ne null}"> data-attr-category="${entryGroup.orderEntries[0].product.categories[0].name}" </c:if>
           <c:if test="${entryGroup.orderEntries[0].product.categories[0].parentCategoryName ne null}"> data-attr-primarycategory="${entryGroup.orderEntries[0].product.categories[0].parentCategoryName}" </c:if>
       />
    </c:forEach>
</c:if>
<product:productOrderFormJQueryTemplates />
<store-pickup:pickupStorePopup />
