<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ attribute name="placeOrderUrl" required="true"  type="java.lang.String"%>
<%@ attribute name="cartCalculationMessage" required="true"  type="java.lang.String"%>
<%@ attribute name="getTermsAndConditionsUrl" required="true"  type="java.lang.String"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="place-order-form">
   <form:form action="${placeOrderUrl}" id="placeOrderForm1"
      modelAttribute="placeOrderForm">
      <div class="checkbox">
         <p class="priceInfoText"><spring:theme code="text.checkout.checkoutfinalstep.priceinfo"/></p>
         <%-- <label class="checkLabel">
            <form:checkbox id="Terms1" path="termsCheck1" />
            <span class="checkmark"></span>
            <spring:theme
               code="checkout.summary.placeOrder.readTermsAndConditions1.puertorico" />
         </label>  --%>
         <label class="checkLabel">
            <form:checkbox id="Terms2" path="termsCheck2" />
            <span class="checkmark"></span>
               <c:if test="${language == 'es'}">
               <a href="https://www.libertypr.com/es/legal" target="_blank" class="terms-link">
               </c:if>
               <c:if test="${language == 'en'}">
               <a href="https://www.libertypr.com/es/legal" target="_blank" class="terms-link">
               </c:if>
            <spring:theme
               code="checkout.summary.placeOrder.readTermsAndConditions2.puertorico"
               arguments="${getTermsAndConditionsUrl}" text="click here"
               htmlEscape="false" />
            </a>
         </label>
      </div>
      <spring:theme code="checkout.summary.placeOrder" text="Place Order"
         var="placeOrderText" />
      <button type="submit"
         class="btn btn-block btn-primary btn-place-order"
         id="placeOrder" title="${placeOrderText}">
      ${placeOrderText}</button>
   </form:form>
</div>
<div class="contactus">
   <h4>
      <spring:theme code="contactus.Questions" htmlEscape="false"/>
   </h4>
   <p><spring:theme code="contactus.getincontact" htmlEscape="false"/></p>
   <ul>
      <li class="call">
         <spring:theme code="contactus.call" htmlEscape="false" />
       </li>
   </ul>
</div>