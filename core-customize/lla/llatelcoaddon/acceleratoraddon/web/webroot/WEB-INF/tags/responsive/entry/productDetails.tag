<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="orderEntry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ attribute name="showStock" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="order-entry" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/entry" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set var="product" value="${orderEntry.product}"/>
<spring:url value="${product.url}" var="productUrl"/>
<spring:url value="/plp/postpaid" var="postpaidPLPUrl"/>
<spring:url value="/plp/prepaid" var="prepaidPLPUrl"/>
<div class="item__info">

    <c:choose>
     <c:when test="${cmsSite.uid eq 'panama' && (orderEntry.product.categories[0].code eq 'addon' || orderEntry.product.categories[0].code eq 'postpaid' || orderEntry.product.categories[0].code eq 'prepaid')}">
     <c:if test="${orderEntry.product.categories[0].code eq 'postpaid'}">
	     <a href="${postpaidPLPUrl}"><span class="item__name">${ycommerce:encodeHTML(product.name)}</span></a>
	     	${product.description}
     </c:if>
     <c:if test="${orderEntry.product.categories[0].code eq 'addon'}">
    <span class="item__name">${ycommerce:encodeHTML(product.name)}</span>
    <c:if test="${cmsSite.uid eq 'panama'}">
        <p class="addon-spec">${product.summary} &nbsp;<b><format:price priceData="${product.monthlyPrice}" /></b></p>
        <input type="hidden" class="gtmProductDataForAddon"	
		       <c:if test="${product.name ne null}"> data-attr-name="${product.name}" </c:if>	
		       <c:if test="${product.code ne null}"> data-attr-id="${product.code}" </c:if>	
		       <c:if test="${product.monthlyPrice ne null}"> data-attr-price="${product.monthlyPrice.formattedValue}" </c:if> />
    </c:if>
    </c:if>
    <c:if test="${orderEntry.product.categories[0].code eq 'prepaid'}">
    <a href="${prepaidPLPUrl}"><span class="item__name">${ycommerce:encodeHTML(product.name)}</span></a>
    </c:if>
    </c:when>
    <c:otherwise>
     <c:choose>
    <c:when test="${cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'cabletica'}">
    <span class="product ${fn:toLowerCase(fn:contains(product.code, 'tv') && fn:contains(product.code, 'ultimate') ? 'televisionUlti' : fn:contains(product.code, 'tv') && (fn:contains(product.code, 'upick') || fn:contains(product.code, 'Primera') )? 'television' :fn:contains(product.code, 'internet') ? 'internet' : 'phone' )}"></span>
    <c:choose>
    	 <c:when test="${cmsSite.uid eq 'cabletica'}">
    	     <span class="item__name"><b>${ycommerce:encodeHTML(product.name)}</b></span>
    	     <div class="product-specifications">
                  	${product.description}
                  </div>
    	 </c:when>
     <c:otherwise>
		<c:choose>
        <c:when test="${cmsSite.uid eq 'puertorico' && cartData.isTelephoneFree && product.code eq 'limitado_phone'}">
    	<span class="item__name"><spring:theme code="text.cart.free.telephone"/></span>        
        </c:when>
        <c:otherwise>
    	<span class="item__name">${ycommerce:encodeHTML(product.name)}</span>
    	</c:otherwise>
    	</c:choose>     
	 </c:otherwise>
    </c:choose>
    </c:when>
    <c:otherwise>
	    <c:choose>
	    	<c:when test="${cmsSite.uid eq 'jamaica'}">
	    		<span class="item__name" data-productcode="${ycommerce:encodeHTML(product.code)}" data-noofchannels="${product.channelList.size()}">${ycommerce:encodeHTML(product.name)}</span>
	    	</c:when>
	    	<c:otherwise>
	    		<a href="${productUrl}"><span class="item__name">${ycommerce:encodeHTML(product.name)}</span></a>
	    	</c:otherwise>
	    </c:choose>
     <c:set var="deviceProduct" value="false" />
     <c:forEach var="item" items="${orderEntry.product.categories}">
        <c:if test="${item.code eq 'devices' }">
           <c:set var="deviceProduct" value="true" />
        </c:if>
     </c:forEach>
     <c:if test="${cmsSite.uid eq 'panama' && !deviceProduct}">
         ${product.description}
     </c:if>
     
    </c:otherwise>
      </c:choose>
    </c:otherwise>
    </c:choose>
    <order-entry:variantDetails product="${product}"/>

    <%--<c:if test="${showStock}">
        <order-entry:stock orderEntry="${orderEntry}"/>
    </c:if> --%>
    <order-entry:promotions orderEntryNumber="${orderEntry.entryNumber}" order="${order}"/>
    <order-entry:fmcPromotionDisplay orderEntryNumber="${orderEntry.entryNumber}" order="${order}"/>
</div>
