<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="store-pickup" tagdir="/WEB-INF/tags/responsive/storepickup"%>
<%@ taglib prefix="telco-product" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:url value="/cart/add/mobileplans" var="addToCartUrl"/>

<spring:htmlEscape defaultHtmlEscape="true" />
<c:set value="${ycommerce:encodeHTML(product.code)}" var="productCode" />
<c:set var = "categoryCode" value="${product.categories[0].code}" />
<product:addToCartTitle />
<div class="container">
<c:if test="${cmsSite.uid eq 'panama'}">
	<div class="process-selection-bar">
		<ul class="selection-state">
	    	<li class="col-xs-3 selectedTab1 previous"><p class="level"><a href="${request.contextPath}/residential/plans-Devices-plp?selectedCategory=${categoryCode}"><span class="process-state">1</span><span class="process-info"><spring:theme code="selection.level1.text.device"/></span></a></p></li>
            <li class="col-xs-3 selectedTab2 active"><p class="level "><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text.device"/></span></p></li>
            <li class="level col-xs-3">
            	<c:choose>
                    <c:when test="${fn:length(cartData.entries) > 0}">
                       <a href="${request.contextPath}/cart"><p class="level"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></p>
                       </a>
                    </c:when>
                    <c:otherwise>
                       <p class="level"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></p>
                    </c:otherwise>
                </c:choose>
            </li>
            <li class="col-xs-3"><p class="level"><span class="process-state">4</span><span class="process-info"><spring:theme code="selection.level3.text"/></span></p></li>
		</ul>
	</div>
</c:if>
<c:if test="${cmsSite.uid ne 'panama'}">
<div class="product-details page-title">
	<c:if test="${cmsSite.uid eq 'jamaica'}">
	<div class="name">
		${ycommerce:encodeHTML(product.name)}
		<!--<span class="sku">
			<spring:theme code="product.device.id" text="ID" />
		</span>
		<span class="code">${productCode}</span>-->
	</div>
	</c:if>
	<!--<product:productReviewSummary product="${product}" showLinks="true" />-->
</div>
</c:if>
<div class="row">
	<div class="col-xs-12 col-sm-5 col-md-4">
		
			<div class="prod-details">
				<div class="prod-headline"><spring:theme code="text.device.pdp.selected.phone"/></div>
				<div class="prod-brand">${ycommerce:encodeHTML(product.manufacturer)}</div>
			  	<div class="prod-name">${ycommerce:encodeHTML(product.name)}</div>
			 </div>	
			<div class="free-delivery"><spring:theme code="text.device.free.delivery" /></div>	
<!-- 		<div class="col-xs-12 col-sm-6"> -->
			<product:productImagePanel galleryImages="${galleryImages}" />
<!-- 		</div> -->
		<div class="col-xs-12 col-sm-6">
			<div class="spec">
				<c:forEach items="${product.classifications}" var="classifications">
					<c:forEach items="${classifications.features}" var="features" varStatus="index">
						<c:if test="${features.code eq 'jamaicaClassification/1.0/mobilephoneclassification.mobileplatform'
                                          			|| features.code eq 'jamaicaClassification/1.0/mobilephoneclassification.camera'
                                          			|| features.code eq 'jamaicaClassification/1.0/mobilephoneclassification.front_camera'
                                          			|| features.code eq 'jamaicaClassification/1.0/mobilephoneclassification.displaysize'}">
							<c:forEach items="${features.featureValues}" var="featureValues">
								<div class="${features.name} spec-char">${featureValues.value}&nbsp; ${features.featureUnit.name}</div>
							</c:forEach>
						</c:if>						
					</c:forEach>
				</c:forEach>
			</div>
			<cms:pageSlot position="VariantSelector" var="component" element="div" class="page-details-variants-select">
				<cms:component component="${component}" element="div" class="yComponentWrapper page-details-variants-select-component" />
			</cms:pageSlot>
			<c:if test="${cmsSite.uid eq 'jamaica'}"><div class="delivery"><spring:theme code="text.home.delivery" /></div></c:if>
		</div>
		<div class="terms col-xs-12 text-center"><spring:theme code="text.device.pdp.terms" /></div>
	</div>
	<div class="clearfix hidden-sm hidden-md hidden-lg"></div>
	<div class="col-xs-12 col-sm-7 col-md-8">
		<div class="product-details row">
		    <c:if test="${cmsSite.uid eq 'panama'}">	
			 <c:forEach items="${product.devicePriceList}" var="item">
  <c:if test="${item.planType eq 'NOOFFER'}">
  <c:set var = "withoutOfferPrice" scope = "session" value = "${item.formattedValue}"/>
  </c:if>
  <c:if test="${item.planType eq 'POSTPAID'}">
  <c:set var = "withOfferPrice" scope = "session" value = "${item.formattedValue}"/>
  <c:if test="${cmsSite.uid eq 'panama'}"><c:set var = "planTypePostPaid" scope = "session" value = "${item.planType}"/></c:if>
  </c:if>	
   <c:if test="${item.planType eq 'FMC'}">
  <c:set var = "withOfferPriceFMC" scope = "session" value = "${item.formattedValue}"/>
  <c:if test="${cmsSite.uid eq 'panama'}"><c:set var = "planTypePostPaid" scope = "session" value = "${item.planType}"/></c:if>
  </c:if>	
</c:forEach>
			</c:if>
			<product:productPromotionSection product="${product}" />
			
			
			<c:if test="${product.purchasable and product.stock.stockLevel le 0}">
				<spring:theme code="product.variants.out.of.stock" text="Out of Stock" />
			</c:if>
			
			<c:if test="${product.stock.stockLevelStatus.code eq 'inStock' and product.stock.stockLevel le 0}">
				<spring:theme code="product.variants.available" text="available online" />
			</c:if>
			<cms:pageSlot position="ProcessTypeSelection" var="component" element="div">
				<cms:component component="${component}" element="div" class="yComponentWrapper" />
			</cms:pageSlot>
			<cms:pageSlot position="SubscriptionSelection" var="component" element="div">
				<cms:component component="${component}" element="div" class="yComponentWrapper" />
			</cms:pageSlot>
             <c:choose>	
            		<c:when test="${product.baseProduct ne null}">	
			<c:if test="${not empty product.devicePriceList}">	
				<c:choose>	
					<c:when test="${fn:contains(product.devicePriceList, 'NO_OFFER')}">	
						<input type="radio" name="deviceprice_radio" value="${item.planType}">	
						<format:fromPrice priceData="${item}" />	
					</c:when>	
					<c:otherwise>	
						<div class="plan clearfix">	
							<h3 class="col-xs-12">Choose your plan type</h3>		
							<c:forEach items="${product.devicePriceList}" var="item">	
								<c:choose>	
									<c:when test="${product.devicePriceList.size()<=2}">	
										<div class="col-xs-12 col-sm-6">	
									</c:when>	
									<c:when test="${product.devicePriceList.size()==3}">	
										<div class="col-xs-12 col-sm-4">	
									</c:when>										
								</c:choose>	
									<div class="plan-type">	
										<h4>	
											<spring:theme code="device.plan.type.${item.planType}" />	
										</h4>	
										<div class="price">	
											<label>	
												<input type="radio" name="deviceprice_radio" value="${item.planType}">${item.formattedValue}	
											</label>	
											<div class="card-icon"></div>	
										</div>											
									</div>	
								</div>																		
							</c:forEach>	
							<spring:theme code="text.choose.plan" var="planHeadline"/>	
							<span id="planHeadline_text" style="display:none;" data-localized-text="${planHeadline}"></span>	
							<div id="plans_details" class="col-xs-12 col-sm-12">	
							</div>	
						</div>	
					</c:otherwise>	
				</c:choose>	
			</c:if>	
			<div class="text-center col-xs-12">	
				<form id="addToCartFormWithoutOffer" class="add_to_cart_form"	
					action="${contextPath}/cart/add" method="post">	
					<input type="hidden" maxlength="3" size="1" id="qty" name="qty"	
						class="qty js-qty-selector-input" value="1">	
					<input type="hidden" name="productCodePost" value="${product.code}">	
				    <input type="hidden" name="processType" value="DEVICE_ONLY">	
					<input type="hidden" id="plan" name="plan">	
					<input type="hidden" id="planType" name="planType">	
					<button type="submit" class="buynow-btn btn-blue"	
						id="buynow_${index.count}">	
						<spring:theme code="text.buynow" text="Buy now!" />	
					</button>	
				</form>	
				<form id="addToCartFormWithOffer" class="add_to_cart_form"	
					action="${contextPath}/cart/add/mobileplans" method="post">	
					<input type="hidden" maxlength="3" size="1" id="qty" name="qty"	
						class="qty js-qty-selector-input" value="1">	
					<input type="hidden" name="productCodePost" value="${product.code}">	
				    <input type="hidden" name="processType" value="DEVICE_ONLY">	
					<input type="hidden" id="plan" name="plan">	
					<input type="hidden" id="planType" name="planType">	
					<button type="submit" class="buynow-btn btn-blue"	
						id="buynow_${index.count}">	
						<spring:theme code="text.buynow" text="Buy now!" />	
					</button>	
				</form>	
            </div>	
            </c:when>	
            		<c:otherwise>	
                        <c:if test="${cmsSite.uid eq 'jamaica'}"><h3 class="col-xs-12 variant-text text-center"><span class="headline-text"><spring:theme code="product.variant.add.to.cart" /></span></h3></c:if>	
                    </c:otherwise>	
            		</c:choose>
             
              <c:if test="${cmsSite.uid eq 'panama'}">
                <c:set var="primarycategoryName" value="${fn:escapeXml(productCategories[productCategories.size()-2].name)}"/>
                <input type="hidden" id="pdpAdobeAnalyticsData"
                    <c:if test="${primarycategoryName ne null}"> data-attr-primarycategoryName="${primarycategoryName}" </c:if>
                    <c:if test="${product.name ne null}"> data-attr-name="${fn:escapeXml(product.name)}" </c:if>
                    <c:if test="${product.code ne null}"> data-attr-id="${product.code}" </c:if>
                    <c:if test="${product.price.formattedValue ne null}"> data-attr-price="${product.price.formattedValue}" </c:if>
                    <c:if test="${product.categories[0].name ne null}"> data-attr-brand="${product.categories[0].name}" data-attr-category="${product.categories[0].code}" </c:if>
                    <c:if test="${product.price.currencyIso ne null}"> data-attr-currency="${product.price.currencyIso}" </c:if>
                    <c:if test="${product.manufacturer ne null}"> data-attr-devicebrand="${product.manufacturer}" </c:if>
                />
               <c:if test="${not empty postpaidProducts}">
				<c:choose>
					<c:when test="${fn:contains(product.devicePriceList, 'NO_OFFER')}">
						<input type="radio" name="deviceprice_radio" value="${item.planType}">
						<format:fromPrice priceData="${item}" />
					</c:when>
					<c:otherwise>
						<div class="plan clearfix">
							<h3 class="col-xs-12"><spring:theme code="text.device.selectPostPaidPlan"/></h3>	
							<c:forEach items="${postpaidProducts}" var="item">	
											
								<div class="col-xs-12 col-sm-6">
									<div class="plan-type" id="${item.key}">									
										<h4 class="${item.key}">
											<%--<spring:theme code="device.plan.type.${item.key}" /> --%>
										</h4>
										<div class="plan-details">
											<div class="price">
												<input type="radio" name="deviceprice_radio" value="${item.key}" class="device-plan-radio">
												<label>${item.value.price.formattedValue}</label>
												<div class="plan-desc"><spring:theme code="text.device.pdp.perMonth"/></div>
											</div>
											<p class="plan-name">${item.value.name}</p>
											<div class="price-section row">	
												<span class="plan-desc col-xs-12"><spring:theme code="text.device.pdp.devicePlanPrice"/></span>											  												  												 
												<div>
													<div class="offer-text col-xs-6">
														<spring:theme code="text.device.fullPrice"/><br>
														<span class="without-offer">${withoutOfferPrice}</span>
													</div>
													<c:forEach items="${product.devicePriceList}" var="price">
													<c:if test="${price.planCode eq item.key}">	
													<div class="offer-text col-xs-6">
														<spring:theme code="text.device.offerPrice"/><br>
														<span class="with-offer">${price.formattedValue}</span>
													</div>	
													</c:if>
													</c:forEach>
												</div>																							   												 											 
											</div>	
											<div class="prod-spec">
												<c:forEach var="specCharValue" items="${item.value.productSpecCharValueUses}">
													<c:forEach items="${specCharValue.productSpecCharacteristicValues}" var="spcchar">
														<div class="${spcchar.productSpecCharacteristic} char-spec-col">
															<span>${spcchar.value}</span>
															<span>${spcchar.unitOfMeasurment}</span>
															<span class="spec-text">${spcchar.ctaSpecDescription}</span>
														</div>
													</c:forEach>
												</c:forEach>
											</div>																												
										</div>										
									</div>
								</div>	
																									
							</c:forEach>
						</div>
					</c:otherwise>
				</c:choose>
			</c:if>
			<c:if test="${not empty fmcProducts}">
				<c:choose>
					<c:when test="${fn:contains(product.devicePriceList, 'NO_OFFER')}">
						<input type="radio" name="deviceprice_radio" value="${item.planType}">
						<format:fromPrice priceData="${item}" />
					</c:when>
					<c:otherwise>
					<div class="plan clearfix">
							<h3 class="col-xs-12"><spring:theme code="text.device.selectFMCPlan"/></h3>	
							<c:forEach items="${fmcProducts}" var="item">	
												
								<div class="col-xs-12 col-sm-6">
									<div class="plan-type" id="${item.key}">									
										<h4 class="${item.key}">
											<%--<spring:theme code="device.plan.type.${item.key}" /> --%>
										</h4>
										<div class="plan-details">
                                <c:forEach var="offeringPrices" items="${item.value.productOfferingPrices}" varStatus="productIndex">
                                    <c:if test="${null ne offeringPrices.recurringChargeEntries}">
                                        <c:set var="recurringCharges" value="${offeringPrices.recurringChargeEntries[0]}"/>
                                    </c:if>
                                </c:forEach>
                                <c:choose>
                                    	<c:when test="${item.value.promoFlag eq 'true'}"> 
                                    	<div class="price"> 
                                    	<input type="radio" name="deviceprice_radio" value="${item.key}" class="device-plan-radio" data-bundleInCart="${product.bundlePresentInCart}">
                                        <c:choose>
						<c:when test="${not empty recurringCharges.discountedPrice}">
							<label>${recurringCharges.discountedPrice.formattedValue}</label>
						</c:when>
						<c:otherwise>
							<label>${item.value.price.formattedValue}</label>
						</c:otherwise>
					</c:choose>
					<div class="plan-desc"><spring:theme code="text.device.pdp.perMonth"/></div>
                                        </div>
                                        </c:when>
                                        <c:otherwise>
												<div class="price">
													<input type="radio" name="deviceprice_radio" value="${item.key}" class="device-plan-radio" data-bundleInCart="${product.bundlePresentInCart}">
													<label>${item.value.price.formattedValue}</label>
													<div class="plan-desc"><spring:theme code="text.device.pdp.perMonth"/></div>
												</div>
									    </c:otherwise>
								 </c:choose>
												
												<p class="plan-name">${item.value.name}</p>
												<div class="price-section row">	
													<span class="plan-desc col-xs-12"><spring:theme code="text.device.pdp.devicePlanPrice"/></span>	
													<div>
														<div class="offer-text col-xs-6">
															<spring:theme code="text.device.fullPrice"/><br>
															<span class="without-offer">${withoutOfferPrice}</span>
														</div>
														<c:forEach items="${product.devicePriceList}" var="price">
														<c:if test="${price.planCode eq item.key}">	
														<div class="offer-text col-xs-6">
															<spring:theme code="text.device.offerPrice"/><br>
															<span class="with-offer">${price.formattedValue}</span>
														</div>	
														</c:if>
														</c:forEach>
													</div>										 											 
												</div>
												<div class="prod-spec fmc-prod-spec">
													<c:forEach var="specCharValue" items="${item.value.productSpecCharValueUses}">
														<c:forEach items="${specCharValue.productSpecCharacteristicValues}" var="spcchar">
															<div class="${spcchar.productSpecCharacteristic} char-spec-col">
																<span>${spcchar.value}</span>
																<span>${spcchar.unitOfMeasurment}</span>
																<span class="spec-text">${spcchar.ctaSpecDescription}</span>
															</div>
														</c:forEach>
													</c:forEach>	
												</div>																												
										</div>										
									</div>
								</div>	
																								
							</c:forEach>																																					
						</div>
					</c:otherwise>
				</c:choose>
			</c:if>
			<div class="text-center col-xs-12">	
			<div class="device-add-to-cart">
			  <form id="addToCartForm" class="add_to_cart_form device_tooltip" action="${addToCartUrl}" method="post">
              <input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
              <input type="hidden" name="productCodePost" value="${product.code}">
              <input type="hidden" name="processType" value="DEVICE_ONLY">
              <input type="hidden" id="deviceplan" name="plan" value="">
              <input type="hidden" id="planType" name="planType" value="${planTypePostPaid}">
                <button id="addToCartButton" type="submit" class="btn buynow-btn btn-blue disabled" disabled>
		    	<spring:theme code="basket.add.to.basket"/>
                </button>
             <span class="tooltiptext" id="deviceToolTip"><spring:theme code="text.device.selectPlanMandatory"/></span>
            </form>
          </div>                  											
            </div>           
              </c:if>
              
	   </div>
	</div>
</div>
</div>
