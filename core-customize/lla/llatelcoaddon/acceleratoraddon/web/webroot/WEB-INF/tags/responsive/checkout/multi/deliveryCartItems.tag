<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="showDeliveryAddress" required="false" type="java.lang.Boolean"%>
<%@ attribute name="showPotentialPromotions" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="order-entry" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/entry"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<c:set var="hasDeliveryItems" value="${cartData.deliveryItemsQuantity > 0}" />
<c:set var="deliveryAddress" value="${cartData.deliveryAddress}" />
<c:set var="installationAddress" value="${cartData.installationAddress}" />
<spring:url value="/plp/postpaid" var="postpaidPLPUrl"/>
<spring:url value="/plp/prepaid" var="prepaidPLPUrl"/>
<c:if test="${not hasDeliveryItems}">
	<spring:theme code="checkout.pickup.no.delivery.required" text="No delivery required for this order" />
</c:if>
<c:if test="${cmsSite.uid eq 'jamaica'}">
	<div class="checkout-headline">
		<spring:theme code="text.cart.items.total.head" htmlEscape="false" />
	</div>
</c:if>
<ul class="checkout-order-summary-list">
	<c:set var="addonHeader" value="0"/>
	<c:forEach items="${cartData.entries}" var="entry" varStatus="loop">
		<spring:url value="${entry.product.url}" var="productUrl" htmlEscape="false" />
		<c:if test="${cmsSite.uid eq 'cabletica'}">
			<c:if test="${entry.product.categories[0].code ne 'channelsaddoncabletica'}">
				<div class="headline"><spring:theme code="text.checkout.summary.headline"/></div>
			</c:if>
			<c:if test="${entry.product.categories[0].code eq 'channelsaddoncabletica' && addonHeader eq 0}">
				<c:set var="addonHeader" value="${addonHeader+1}"/>
				<div class="headline"><spring:theme code="text.order.summary.headline.addons"/></div>
			</c:if>
		</c:if>
		<li class="checkout-order-summary-list-items <c:if test="${entry.discountFlag eq true}">online-promo</c:if>">
			<c:if test="${cmsSite.uid eq 'jamaica'}">
			<c:if test="${entry.discountFlag eq true}">
                <div class="online-offer">
                    <spring:theme code="text.online.discount"/>
                </div>
            </c:if>
			</c:if>

			<c:if test="${cmsSite.uid eq 'jamaica'}">
				<div class="payment-details col-xs-12">
					
				<div class="details">
					<div class="name" style="display:flow-root;" data-productname="${ycommerce:encodeHTML(entry.product.name)}" data-productcode="${entry.product.code}" data-noofchannels="${entry.product.channelList.size()}">
								<c:choose>
									<c:when test="${entry.product.categories[0].code eq 'postpaid'}">
										<spring:theme code="text.cart.items.deposit"/> -
										<span>${ycommerce:encodeHTML(entry.product.name)}</span>
									</c:when>
									<c:otherwise>
										<span>${ycommerce:encodeHTML(entry.product.name)}</span>
									</c:otherwise>
								</c:choose>
					</div>
				</div>
				
				<div class="price">
					<c:forEach items="${entry.orderEntryPrices}" var="orderEntryPrice">
						<c:set var="orderEntryMonthlyPrice" value="${orderEntryPrice.monthlyPrice}" />
						<c:if test="${orderEntryPrice.basePrice.value != null}">
							<div class="actualPrice <c:if test="${cmsSite.uid eq 'jamaica'}">col-xs-12</c:if>">
								<order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryPrice.totalPrice}" orderEntryBasePrice="${orderEntryPrice.basePrice}" orderEntryMonthlyPrice="${orderEntryMonthlyPrice}"  monthlyTotalPrice="${cartData.monthlyTotalPrice}" orderEntryRegularPrice ="${entry.actualPrice.formattedValue}" orderEntryDiscountFlag="${entry.discountFlag}" />
							</div>

							<%-- <input type="hidden" class="productGtmData"
								<c:if test="${entry.product.name ne null}"> data-attr-name="${entry.product.name}" </c:if>
								<c:if test="${entry.product.code ne null}"> data-attr-id="${entry.product.code}" </c:if>
								<c:if test="${orderEntryPrice.totalPrice.value ne null}"> data-attr-price="${orderEntryPrice.totalPrice.value}" </c:if>			
								<c:if test="${entry.quantity ne null}"> data-attr-quantity="${entry.quantity}" </c:if>
						       	<c:if test="${cmsSite.uid eq 'panama'}">
									<c:if test="${entry.product.manufacturer ne null}"> data-attr-devicebrand="${entry.product.manufacturer}" </c:if>
									<c:if test="${cartData.planTypes.code ne null}"> data-attr-planType="${cartData.planTypes.code}" </c:if>
									<c:if test="${cartData.devicePaymentOption.code ne null}"> data-attr-devicePaymentOption="${cartData.devicePaymentOption.code}" </c:if>
								</c:if>											
						    <c:choose>
							<c:when test="${fn:contains(entry.product.categories[0].code, 'brand') || fn:startsWith(entry.product.categories[0].code, 'brand')}">
						      <c:if test="${entry.product.categories[1].name ne null}"> data-attr-brand="${entry.product.categories[1].name}" data-attr-category="${entry.product.categories[1].name}" </c:if>
                              <c:if test="${entry.product.categories[1].parentCategoryName ne null}"> data-attr-category_2="${entry.product.categories[1].parentCategoryName}" </c:if>
						     </c:when>
						    <c:otherwise>
						      <c:if test="${entry.product.categories[0].name ne null}"> data-attr-brand="${entry.product.categories[0].name}" data-attr-category="${entry.product.categories[0].name}" </c:if>
                              <c:if test="${entry.product.categories[0].parentCategoryName ne null}"> data-attr-category_2="${entry.product.categories[0].parentCategoryName}" </c:if>						  
						    </c:otherwise>
						    </c:choose>
							> --%>
							<c:if test="${entry.fixedLineProduct eq 'true'}">
								<div class="fixedline-product">
									<spring:theme code="text.installation.waiveoff" /><br>
									<spring:theme code="text.security.deposit" />:&nbsp;<order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryPrice.totalPrice}"
									orderEntryBasePrice="${orderEntryPrice.basePrice}" />
								</div>
							</c:if>
						</c:if>
					</c:forEach>
				</div>
			</c:if>
						
			<c:if test="${cmsSite.uid eq 'panama'}">
			<div class="col-xs-5 col-sm-4">
			</c:if>
			<c:if test="${cmsSite.uid eq 'cabletica'}">
			<div class="col-xs-4 col-sm-3">
			</c:if>
			<c:if test="${cmsSite.uid eq 'jamaica'}">
			<div class="col-xs-12">
			</c:if>

				<div class="thumb">
					<c:choose>
						<c:when test="${cmsSite.uid eq 'panama'}">
							<c:if test="${entry.product.categories[0].code eq 'postpaid' || entry.product.categories[0].code eq 'addon' || entry.product.categories[0].code eq 'prepaid'}">
								<c:if test="${entry.product.categories[0].code eq 'postpaid'}">
									<a href="${postpaidPLPUrl}"> <product:productPrimaryImage product="${entry.product}" format="cartIcon" /> </a>
								</c:if>
								<c:if test="${entry.product.categories[0].code eq 'prepaid'}">
									<a href="${prepaidPLPUrl}"> <product:productPrimaryImage product="${entry.product}" format="cartIcon" /> </a>
								</c:if>
								<c:if test="${entry.product.categories[0].code eq 'addon'}">
									<product:productPrimaryImage product="${entry.product}" format="cartIcon" />
								</c:if>
							</c:if>
							<c:if test="${entry.product.categories[0].code ne 'postpaid' && entry.product.categories[0].code ne 'addon' && entry.product.categories[0].code ne 'prepaid'}">
								<a href="${productUrl}">
									<product:productPrimaryImage product="${entry.product}" format="cartIcon"/>
								</a>
							</c:if>
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'cabletica' || cmsSite.uid eq 'jamaica'}">
									<product:productPrimaryImage product="${entry.product}" format="thumbnail" />
								</c:when>
								<c:otherwise>
									<a href="${productUrl}"><product:productPrimaryImage product="${entry.product}" format="thumbnail" /></a>
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>
				</div>


			<c:if test="${cmsSite.uid eq 'panama' || cmsSite.uid eq 'jamaica' || cmsSite.uid eq 'cabletica'}">
			</div>
			</c:if>

			<c:if test="${cmsSite.uid eq 'jamaica'}">
				<div class="benefits"><order-entry:jamaicaProductSpecs orderEntry="${entry}" order="${cartData}" /></div>
				<div class="hidden">
					<div class="headline">
						<div class="channel-popup-title">
							<div class="bundleName"></div>
							<div class="no-of-channels"><b></b> <spring:theme code="text.bundle.channels"/></div>
						</div>
					</div>
					<div id="channels-popup" class="channels-popup">
						<div class="channels-list"></div>
					</div>
				</div>
			</c:if>

			<c:if test="${cmsSite.uid eq 'panama'}">
			<div class="col-xs-7 col-sm-8">
			</c:if>
			<c:if test="${cmsSite.uid eq 'cabletica'}">
			<div class="col-xs-12 col-sm-9">
			</c:if>
			<%--<c:if test="${cmsSite.uid eq 'jamaica'}">
			<div class="payment-details col-xs-12">
				<div class="col-xs-6 col-sm-5">
			</c:if>--%>

				<c:if test="${cmsSite.uid ne 'jamaica'}">
				<div class="details">
					<div class="name" style="display:flow-root;">
						<c:choose>
							<c:when test="${cmsSite.uid eq 'panama' && (entry.product.categories[0].code eq 'addon' || entry.product.categories[0].code eq 'postpaid' || entry.product.categories[0].code eq 'prepaid')}">
								<c:if
									test="${entry.product.categories[0].code eq 'postpaid'}">
									<a href="${postpaidPLPUrl}">${ycommerce:encodeHTML(entry.product.name)}</a>
								</c:if>
								<c:if
									test="${entry.product.categories[0].code eq 'prepaid'}">
									<a href="${prepaidPLPUrl}">${ycommerce:encodeHTML(entry.product.name)}</a>
								</c:if>
								<c:if test="${entry.product.categories[0].code eq 'addon'}">
									${ycommerce:encodeHTML(entry.product.name)}
								</c:if>
							</c:when>
							<c:when test="${cmsSite.uid eq 'cabletica'}">
								<c:if test="${entry.product.code eq 'Convertidor_Digital'}">
									${entry.quantity}&nbsp; ${ycommerce:encodeHTML(entry.product.name)}
								</c:if>
								<c:if test="${entry.product.code ne 'Convertidor_Digital'}">
									${ycommerce:encodeHTML(entry.product.name)}
								</c:if>
							</c:when>
							<%--<c:when test="${cmsSite.uid eq 'jamaica'}">
								<c:choose>
									<c:when test="${entry.product.categories[0].code eq 'postpaid'}">
										<spring:theme code="text.cart.items.deposit"/> -
										<span>${ycommerce:encodeHTML(entry.product.name)}</span>
									</c:when>
									<c:otherwise>
										<span>${ycommerce:encodeHTML(entry.product.name)}</span>
									</c:otherwise>
								</c:choose>
							</c:when>--%>
							<c:otherwise>
								<a href="${productUrl}">${ycommerce:encodeHTML(entry.product.name)}</a>
							</c:otherwise>
						</c:choose>
					</div>
					<%--<c:if test="${cmsSite.uid eq 'jamaica'}">
						<div class="item__spec">
							<c:forEach var="specCharValue" items="${entry.product.productSpecCharValueUses}" varStatus="index">
								${specCharValue.description} <br>
							</c:forEach>
						</div>
					</c:if>--%>
					<order-entry:variantDetails product="${entry.product}"/>
					<div>
						<c:forEach items="${entry.product.baseOptions}" var="option">
							<c:if test="${not empty option.selected and option.selected.url eq entry.product.url}">
								<c:forEach items="${option.selected.variantOptionQualifiers}" var="selectedOption">
									<div>${ycommerce:encodeHTML(selectedOption.name)}:${ycommerce:encodeHTML(selectedOption.value)}</div>
								</c:forEach>
							</c:if>
						</c:forEach>

						<div class="promotion">
							<c:if
								test="${ycommerce:doesPotentialPromotionExistForOrderEntryOrOrderEntryGroup(cartData, entry) && showPotentialPromotions}">
								<c:set var="promotions" value="${cartData.potentialProductPromotions}" />
							</c:if>
							<c:if test="${ycommerce:doesAppliedPromotionExistForOrderEntryOrOrderEntryGroup(cartData, entry)}">
								<c:set var="promotions" value="${cartData.appliedProductPromotions}" />
							</c:if>
							<c:forEach items="${promotions}" var="promotion">
								<c:if test="${not empty promotion.consumedEntries}">
									<c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
										<c:if test="${ycommerce:isConsumedByEntry(consumedEntry,entry)}">
											<span class="promotion">${ycommerce:sanitizeHTML(promotion.description)}</span>
										</c:if>
									</c:forEach>
								</c:if>
							</c:forEach>
						</div>
						<common:configurationInfos entry="${entry}" />
					</div>
					<spring:url value="/checkout/multi/getReadOnlyProductVariantMatrix" var="targetUrl" htmlEscape="false" />
					<grid:gridWrapper entry="${entry}" index="${loop.index}" styleClass="display-none" targetUrl="${targetUrl}" />
				</div>
				</c:if>

			<%--<c:if test="${cmsSite.uid ne 'jamaica'}">
			</div>
			</c:if>--%>

				<c:if test="${cmsSite.uid ne 'jamaica'}">
				<div class="price">
				</c:if>

					<c:forEach items="${entry.orderEntryPrices}" var="orderEntryPrice">
						<c:set var="orderEntryMonthlyPrice" value="${orderEntryPrice.monthlyPrice}" />
						<c:if test="${orderEntryPrice.basePrice.value != null}">

							<!--<div>${ycommerce:encodeHTML(orderEntryPrice.billingTime.name)}:</div>-->
							<c:if test="${cmsSite.uid eq 'panama'}">
								<div class="product-specifications">
									<c:set var="product" value="${entry.product}"/>
									<c:forEach var="specValue" items="${product.productSpecCharValueUses}">
										${specValue.productSpecCharacteristicValues[0].value}&nbsp ${specValue.productSpecCharacteristicValues[0].unitOfMeasurment}
										</br>
									 </c:forEach>
								</div>
								<c:if test="${entry.product.code eq 'WIFI_EXT'}">
									<p class="additional-cost"><spring:theme code="text.checkout.wifi.additional.cost" /></p>
								</c:if>
								<c:if test="${entry.product.code eq 'SETTOP_BOX'}">
									<p class="additional-cost"><spring:theme code="text.checkout.setupbox.additional.cost" /></p>
								</c:if>
							</c:if>

							<c:if test="${cmsSite.uid eq 'cabletica'}">
								<div class="product-specifications">
								<c:set var="product" value="${entry.product}"/>
								${product.summary}
								</div>
							</c:if>

							<%--<c:if test="${cmsSite.uid eq 'jamaica'}">
							<div class="actualPrice <c:if test="${cmsSite.uid eq 'jamaica'}">col-xs-6 col-sm-7</c:if>">

								<c:choose>
									<c:when test="${cmsSite.uid eq 'panama'}">
										<c:set var="payNowVisible" value="false" />
										<c:forEach var="item" items="${entry.product.categories}">
											<c:if test="${item.code eq 'devices' || item.code eq 'prepaid'}">
												<c:set var="payNowVisible" value="true" />
											</c:if>
										</c:forEach>
										<c:choose>
											<c:when test="${payNowVisible}">
												<spring:theme code="text.panama.subtotal.paynow" /> <br>
												<b><format:price priceData="${orderEntryPrice.totalPrice}" displayFreeForZero="false" /></b>
											</c:when>
											<c:otherwise>
												<order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryPrice.totalPrice}"
																		orderEntryBasePrice="${orderEntryPrice.basePrice}" orderEntryMonthlyPrice="${orderEntryMonthlyPrice}"  monthlyTotalPrice="${cartData.monthlyTotalPrice}"/>
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<c:if test="${cmsSite.uid eq 'cabletica'}">
											<c:choose>
												<c:when test="${entry.product.categories[0].code eq 'channelsaddoncabletica'}">
													<c:choose>
														<c:when test="${entry.product.code eq 'Convertidor_Digital'}">
															<span class="price-info"><spring:theme code="text.account.orderHistory.total.perunit"/></span> <br/>
														</c:when>
														<c:otherwise>
															<span class="price-info"><spring:theme code="text.account.orderHistory.total.price"/></span> <br/>
														</c:otherwise>
													</c:choose>
												</c:when>
												<c:otherwise>
													<span class="price-info"><spring:theme code="text.account.orderHistory.total"/></span><br/>
												</c:otherwise>
											</c:choose>
										</c:if>
										<c:if test="${cmsSite.uid eq 'cabletica'}">
											 <c:if test="${entry.product.categories[0].code eq 'channelsaddoncabletica'}">
												<order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryPrice.totalPrice}" orderEntryBasePrice="${orderEntryPrice.basePrice}" orderEntryMonthlyPrice="${orderEntryPrice.totalPrice}"  monthlyTotalPrice="${cartData.monthlyTotalPrice}"/>
											 </c:if>
											 <c:if test="${entry.product.categories[0].code ne 'channelsaddoncabletica'}">
												<order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryPrice.totalPrice}" orderEntryBasePrice="${orderEntryPrice.basePrice}" orderEntryMonthlyPrice="${orderEntryPrice.totalPrice}"  monthlyTotalPrice="${cartData.monthlyTotalPrice}"  totalPriceWithTax="${cartData.totalPriceWithTax}"/>
											 </c:if>
										</c:if>
										<c:if test="${cmsSite.uid ne 'cabletica'}">
											<order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryPrice.totalPrice}" orderEntryBasePrice="${orderEntryPrice.basePrice}" orderEntryMonthlyPrice="${orderEntryMonthlyPrice}"  monthlyTotalPrice="${cartData.monthlyTotalPrice}"/>
										</c:if>
									</c:otherwise>
								</c:choose>

							</div>
							</c:if>--%>

							<input type="hidden" class="productGtmData"
								<c:if test="${entry.product.name ne null}"> data-attr-name="${entry.product.name}" </c:if>
								<c:if test="${entry.product.code ne null}"> data-attr-id="${entry.product.code}" </c:if>
								<c:if test="${orderEntryPrice.totalPrice.value ne null}"> data-attr-price="${orderEntryPrice.totalPrice.value}" </c:if>			
								<c:if test="${entry.quantity ne null}"> data-attr-quantity="${entry.quantity}" </c:if>
						       	<c:if test="${cmsSite.uid eq 'panama'}">
									<c:if test="${entry.product.manufacturer ne null}"> data-attr-devicebrand="${entry.product.manufacturer}" </c:if>
									<c:if test="${cartData.planTypes.code ne null}"> data-attr-planType="${cartData.planTypes.code}" </c:if>
									<c:if test="${cartData.devicePaymentOption.code ne null}"> data-attr-devicePaymentOption="${cartData.devicePaymentOption.code}" </c:if>
								</c:if>											
						    <c:choose>
							<c:when test="${fn:contains(entry.product.categories[0].code, 'brand') || fn:startsWith(entry.product.categories[0].code, 'brand')}">
						      <c:if test="${entry.product.categories[1].name ne null}"> data-attr-brand="${entry.product.categories[1].name}" data-attr-category="${entry.product.categories[1].name}" </c:if>
                              <c:if test="${entry.product.categories[1].parentCategoryName ne null}"> data-attr-category_2="${entry.product.categories[1].parentCategoryName}" </c:if>
						     </c:when>
						    <c:otherwise>
						      <c:if test="${entry.product.categories[0].name ne null}"> data-attr-brand="${entry.product.categories[0].name}" data-attr-category="${entry.product.categories[0].name}" </c:if>
                              <c:if test="${entry.product.categories[0].parentCategoryName ne null}"> data-attr-category_2="${entry.product.categories[0].parentCategoryName}" </c:if>						  
						    </c:otherwise>
						    </c:choose>
							>
							<c:if test="${entry.fixedLineProduct eq 'true'}">
								<div class="fixedline-product">
									<spring:theme code="text.installation.waiveoff" /><br>
									<spring:theme code="text.security.deposit" />:&nbsp;<order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryPrice.totalPrice}"
									orderEntryBasePrice="${orderEntryPrice.basePrice}" />
								</div>
							</c:if>
						</c:if>
					</c:forEach>

				</div>

			<c:if test="${cmsSite.uid eq 'panama' || cmsSite.uid eq 'cabletica'}">
			</div>
			</c:if>
		</li>
	</c:forEach>
	<c:if test="${cmsSite.uid eq 'cabletica'}">
		<li class="checkout-order-summary-list-items checkout-total-summary-info">
			<ul>
				<div class="col-xs-6 row">
					<span class="price-info"><spring:theme code="text.account.orderHistory.payement.total"/></span>
				</div>
				<div class="col-xs-6">
					<b>${currentCurrency.symbol}<fmt:formatNumber value="${cartData.totalPrice.value}" pattern="#,##0" />&nbsp;<spring:theme code="text.cabletica.IVA"/></b>
					<span class="price_month"><spring:theme code="text.ctaMonthlyplan"/></span>
				</div>
			</ul>
		</li>
	</c:if>
	<%--<c:if test="${cmsSite.uid eq 'jamaica'}">
		<li class="checkout-order-summary-list-items today-tax">
			<div class="details"><spring:theme code="basket.page.totals.grossTaxes.noArgs" text="Tax"/></div>
			<div class="price">
				<c:forEach items="${cartData.orderPrices}" var="orderPrice">
					<format:price priceData="${orderPrice.totalTax}" displayFreeForZero="FALSE" />
				</c:forEach>
			</div>
		</li>
		<li class="checkout-order-summary-list-items today-total">
			<div class="details"><spring:theme code="basket.page.total.checkout.today" text="Total" /></div>
			<div class="price"><format:price priceData="${cartData.totalPriceWithTax}" /></div>
		</li>
	</c:if>--%>
</ul>
<input type="hidden" class="productGtmDataPaymentMethod" data-attr-cardtype="${cartData.paymentInfo.cardTypeData.name}" >
