<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="entryGroup" required="true" type="de.hybris.platform.commercefacades.order.EntryGroupData" %>
<%@ attribute name="count" required="false" type="java.lang.Integer" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/cart" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:set var="entryGroupNumber" value="${ycommerce:encodeHTML(entryGroup.groupNumber)}"/>
<spring:url value="/cart/delete" var="bpoDeleteAction" htmlEscape="false"/>

<spring:url var="editUrl" value="/bpo/edit/configure/{/externalReferenceId}/{/entryGroupNumber}" htmlEscape="false">
    <spring:param name="entryGroupNumber" value="${ycommerce:encodeHTML(entryGroup.groupNumber)}"/>
    <spring:param name="externalReferenceId" value="${ycommerce:encodeHTML(entryGroup.externalReferenceId)}"/>
</spring:url>

    <c:choose>
        <c:when test="${entryGroup.groupType.code == 'B2CTELCO_BPO'}">
            <c:if test="${cmsSite.uid eq 'panama' || cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'cabletica'}">
            <table>
            </c:if>
            <c:if test="${cmsSite.uid ne 'puertorico' && cmsSite.uid ne 'cabletica'}">
            <tr class="entry-group-header">
                <th>
                    <div class="row">
                        <div class="col-md-10 col-lg-10 col-sm-9 col-xs-9 left-align item-category">${ycommerce:encodeHTML(entryGroup.label)}
                            <%--<div class="small-entry-group-header">
                                <spring:theme code="text.cart.processType.${entryGroup.processType}"/>
                            </div>--%>
                        </div>
                        <c:if test="${cmsSite.uid ne 'puertorico' && cmsSite.uid ne 'cabletica'}">
                            <div class="col-md-2 col-lg-2 col-sm-3 col-xs-3">
                                <c:if test="${not empty entryGroupNumber}">
                                    <form:form id="deleteBpoForm${entryGroupNumber}" action="${bpoDeleteAction}" method="post"
                                               modelAttribute="deleteBpoForm'${entryGroupNumber}">
                                        <input type="hidden" name="groupNumber" value="${entryGroupNumber}"/>
                                        <spring:theme code="text.iconCartRemove" var="iconCartRemove" text="REMOVE"/>
                                        <a href="#" title="Remove Offer" id="${entryGroupNumber}" class="submitRemoveBundle">
                                                ${iconCartRemove}
                                        </a>
                                    </form:form>
                                    <%--<a href="${editUrl}">
                                        <spring:theme code="cart.groups.edit" text="EDIT"/>
                                    </a>--%>
                                </c:if>
                            </div>
                        </c:if>
                    </div>
                </th>
            </tr>
            </c:if>
            <c:if test="${cmsSite.uid eq 'panama' || cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'cabletica' }">
            </table>
            </c:if>
            <cart:entryGroup cartData="${cartData}" entryGroup="${entryGroup}"/>
        </c:when>

        <c:when test="${not empty entryGroup.orderEntries}">
            <c:forEach items="${entryGroup.orderEntries}" var="entry">
                <cart:cartItem cartData="${cartData}" entry="${entry}" count="${count}"/>
            </c:forEach>
        </c:when>
    </c:choose>

