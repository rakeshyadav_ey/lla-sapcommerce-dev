<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:htmlEscape defaultHtmlEscape="true" />
<c:forEach items="${cartData.entries}" var="entry">
    <c:if test="${entry.product.categories[0].code ne 'channelsaddoncabletica'}">
        <c:set var="category" value="${entry.product.categories[0].code}"/>
    </c:if>
</c:forEach>

<c:if test="${cmsSite.uid eq 'panama'}">
    <c:forEach items="${cartData.entries}" var="entry">
        <c:if test="${entry.product.categories[0].code ne 'devices'}">
            <c:set var="category" value="${entry.product.categories[0].code}"/>
        </c:if>
    </c:forEach>

    <c:forEach items="${cartData.entries}" var="entry">
        <c:if test="${entry.product.categories[0].code ne category}">
            <c:set var="subCategory" value="${entry.product.categories[0].code}"/> 
        </c:if>
    </c:forEach>
</c:if>

<c:if test="${siteId eq 'cabletica'}">
    <input type="hidden" id="c2cTimerBOnCheckout" name="c2cTimerBOnCheckout" value="${c2cTimerBOnCheckout}"/>
    <input type="hidden" id="c2cTimerCOnCheckout" name="c2cTimerCOnCheckout" value="${c2cTimerCOnCheckout}"/>
    <div class="process-selection-bar">
        <ul class="selection-state clearfix">
            <li class="previous col-xs-4"><p class="level"><a href="${request.contextPath}/residential?selectedCategory=${category}"><span class="process-state">1</span><span class="process-info"><spring:theme code="selection.level1.text"/></span></a></p></li>
            <li class="previous col-xs-4"><p class="level"><a href="${request.contextPath}/cart"><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></a></p></li>
            <li class="active col-xs-4"><p class="level"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level3.text"/></span></p></li>
        </ul>
    </div>
    <div class="checkout-header"> 
        <h1><spring:theme code="text.checkout.multi.headline"/></h1>
        <p><spring:theme code="text.checkout.multi.fill.details"/></p>
    </div>
</c:if>
<c:if test="${siteId eq 'jamaica' and hideProgressBar eq 'false'}">
    <div class="process-selection-bar">
        <ul class="selection-state clearfix">
            <li class="previous col-xs-4"><p class="level"><span class="process-state">1</span><span class="process-info"><spring:theme code="selection.level1.text"/></span></p></li>
            <li class="previous col-xs-4"><p class="level"><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></p></li>
            <li class="active col-xs-4"><p class="level"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level3.text"/></span></p></li>
        </ul>
    </div>
</c:if>
<c:if test="${cmsSite.uid eq 'panama'}">
    <div class="process-selection-bar">
        <ul class="selection-state clearfix">
            <c:choose>
                <c:when test="${(deviceProduct eq 'true' && category eq 'postpaid') || (deviceProduct eq 'true' && category eq '4Pbundles') || (deviceProduct eq 'true' && category eq 'addon')}">
                    <li class="col-xs-3 ${(deviceProduct eq 'true' && lastCategoryCode eq 'devices') || (deviceProduct eq 'true' && category eq 'addon') ? 'previous':'disable'}"><p class="level">
                    <c:choose>
                        <c:when test="${lastCategoryCode eq 'devices' && deviceProduct eq 'true'}">
                            <a href="${request.contextPath}/residential/plans-Devices-plp?selectedCategory=${lastCategoryCode}">
                        </c:when>
                        <c:when test="${onlyDevice eq 'devices'}">
                            <a href="${request.contextPath}/residential/plans-Devices-plp?selectedCategory=devices">
                        </c:when>
                        <c:when test="${empty onlyDevice}">
                            <a href="${request.contextPath}/residential/plans-Devices-plp?selectedCategory=devices">
                        </c:when>
                    </c:choose>
                    <span class="process-state">1</span><span class="process-info"><spring:theme code="selection.level1.text.device"/></span>
                    <c:if test="${empty onlyDevice || (lastCategoryCode eq 'devices' && category ne '4Pbundles') || (onlyDevice eq 'devices')}"></a></c:if></p>
                </li>    
                <li class="col-xs-3 previous"><p class="level">
                <c:choose>
                    <c:when test="${deviceProduct eq 'true' && lastCategoryCode eq 'devices'}">
                        <a href="${contextPath}/${currentLanguage.isocode}/p/${lastProductCode}" class="product__list--thumb prod-img"><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text.device"/></span></a>
                    </c:when>
                    <c:otherwise>
                        <a href="${request.contextPath}/residential/plans-Devices-plp?selectedCategory=${category}"><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text.device"/></span></a>
                    </c:otherwise>
                </c:choose></p>
                </li>
                    <li class="col-xs-3 previous"><p class="level"><a href="${request.contextPath}/cart" class="product__list--thumb prod-img"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></a></p></li>
                    <li class="col-xs-3 active"> <p class="level"><span class="process-state">4</span><span class="process-info"><spring:theme code="selection.level3.text"/></span></p></li>
                </c:when>                            
                <c:otherwise>
                    <li class="previous col-xs-4"><p class="level">
                        <c:choose>
                            <c:when test="${category eq 'addon' && lastCategoryCode eq 'devices'}">
                                <a href="${request.contextPath}/residential?selectedCategory=${fmcCategoryCode}">
                            </c:when>
                             <c:when test="${category eq 'addon' && lastCategoryCode ne 'devices'}">
                                 <a href="${request.contextPath}/residential?selectedCategory=${lastCategoryCode}">   
                              </c:when>
                            <c:otherwise>
                                <a href="${request.contextPath}/residential?selectedCategory=${category}">
                            </c:otherwise>
                        </c:choose>
                        <span class="process-state">1</span><span class="process-info"><spring:theme code="selection.level1.text"/></span></a>
                        </p></li>
                    <li class="col-xs-4 previous"><p class="level"><a href="${request.contextPath}/cart" class="product__list--thumb prod-img"><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></a></p></li>
                    <li class="col-xs-4 active"><p class="level"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level3.text"/></span></p></li>
                </c:otherwise>
            </c:choose>
        </ul>
    </div>
</c:if>
