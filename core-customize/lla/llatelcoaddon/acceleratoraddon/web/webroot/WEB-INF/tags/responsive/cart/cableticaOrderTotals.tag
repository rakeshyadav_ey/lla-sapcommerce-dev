<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="orderData" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData"%>
<%@ attribute name="showTaxEstimate" required="false" type="java.lang.Boolean"%>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/cart" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<c:set var="subTotalPrice"> <fmt:formatNumber value="0" /></c:set>
<c:set var="tax911"> <fmt:formatNumber value="0" /></c:set>
<c:set var="tax13"><fmt:formatNumber value="0" /> </c:set>
    <%-- <div class="cart-total-summary today-payment">
        <div class="headline">
            <spring:theme code="text.cabletica.subtotal.paynow.headline" />
        </div>
        <div class="payment-disclaimer">
            <span class="glyphicon glyphicon-info-sign"></span>
            <div><spring:theme code="text.cabletica.subtotal.paynow.info" /></div>
        </div>
        <div class="order-totals">
            <table id="order-totals">            
                <tbody>
                    <c:forEach items="${orderData.entries}" var="entryGroup" varStatus="count">
                        <c:set var="isNotchannelsaddoncabletica" value="true" />
                        <c:forEach var="item" items="${entryGroup.product.categories}">
                            <c:if test="${item.code eq 'channelsaddoncabletica' }">
                                <c:set var="isNotchannelsaddoncabletica" value="false" />
                            </c:if>
                            </c:forEach>
                     	 </c:forEach>
                     	  <c:forEach items="${orderData.entries}" var="entryGroup" varStatus="count">
                     	  <c:set var="isNotchannelsaddoncabletica" value="true" />
                     	  <c:forEach var="item" items="${entryGroup.product.categories}">
                     	  <c:if test="${item.code eq 'channelsaddoncabletica' }">
                            <c:set var="isNotchannelsaddoncabletica" value="false" />
                           </c:if>
                            <c:forEach items="${entryGroup.orderEntryPrices}" var="orderEntryPrice">
                                <c:if test="${isNotchannelsaddoncabletica}">
                                    <tr>
                                       <td class="cart-bundle-package">${entryGroup.product.name}</td>
                                        <fmt:setLocale value="en_US" scope="session" />
                                          <td>${currentCurrency.symbol}<fmt:formatNumber value="${orderEntryPrice.monthlyPrice.value}" pattern="#,##0" /> </td>
                                          <c:set var="subTotalPrice"><fmt:formatNumber value="${orderEntryPrice.monthlyPrice.value}" pattern="#,##0" /> </c:set>pro
                                    </tr>
                                    <tr>
                                        <td class="cart-bundle-package"><spring:theme code="basket.page.totals.grossTax.13percent" text="Tax"/></td>
                                       <td>${currentCurrency.symbol}<fmt:formatNumber value="${orderEntryPrice.tax911Price.value}" pattern="#,##0" /> </td>
                                    </tr>
                                    <tr>
                                        <td class="cart-bundle-package"><spring:theme code="text.cabletica.911.tax" /></td>
                                       <td>${currentCurrency.symbol}<fmt:formatNumber value="${orderEntryPrice.tax13Price.value}" pattern="#,##0" /></td>
                                    </tr>
                                </c:if>
                            </c:forEach>
                        </c:forEach>
                    </c:forEach>
                    <tr>
                        <td class="cart-bundle-package"><spring:theme code="text.cabletica.subtotal.paynow" text="PayNowAmount"/></td>
                         <td>${currentCurrency.symbol}<fmt:formatNumber value="${orderData.totalPriceWithTax.value}" pattern="#,##0" /></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div> --%>

    <div class="cart-total-summary monthly-payment">
        <div class="headline">
            <spring:theme code="text.cabletica.subtotal.payMonthly.headline" htmlEscape="false"/>
        </div>
        <!-- <c:if test="${cmsSite.uid eq 'cabletica'}">
          <c:choose>
            <c:when test="${tokenMoneyCapturedMsgRequired eq 'true'}">
              <div class="payment-disclaimer">
                  <span class="glyphicon glyphicon-info-sign"></span>
                  <div><spring:theme code="text.cabletica.subtotal.payMonthly.info.authorized.msg" /></div>
              </div>
            </c:when>
            <c:otherwise>
              <div class="payment-disclaimer">
                  <span class="glyphicon glyphicon-info-sign"></span>
                  <div><spring:theme code="text.cabletica.subtotal.payMonthly.info1" /></div>
              </div>
            </c:otherwise>
          </c:choose>
        </c:if> -->
        <div class="payment-disclaimer">
            <span class="glyphicon glyphicon-info-sign"></span>
            <div><spring:theme code="text.cabletica.subtotal.payMonthly.info" /></div>
        </div>
        <div class="order-totals">
            <table id="order-totals">            
                <tbody>
                     <c:forEach items="${orderData.entries}" var="entryGroup" varStatus="count" >
                        <tr>
                         <c:if test="${entryGroup.product.code eq 'Convertidor_Digital'}">
                             <td class="cart-bundle-package">${entryGroup.quantity} ${entryGroup.product.name}</td>
                          </c:if>
                         <c:if test="${entryGroup.product.code ne 'Convertidor_Digital'}">
                         <c:choose>
                            <c:when test="${cmsSite.uid eq 'cabletica' and entryGroup.product.categories[0].code ne 'channelsaddoncabletica'}">
                            <td class="cart-bundle-package"><b>${entryGroup.product.name}</b></td>
                             </c:when>
                            <c:otherwise>
                              <td class="cart-bundle-package">${entryGroup.product.name}</td>
                             </c:otherwise>
                          </c:choose>
                         </c:if>
                         <c:forEach items="${entryGroup.orderEntryPrices}" var="orderEntryPrice">
                            <c:set var="formattedValue"><fmt:formatNumber type="number" value="${orderEntryPrice.monthlyPrice.value * entryGroup.quantity}" pattern="#,##0" /></c:set>
                            <td>${currentCurrency.symbol} ${fn:escapeXml(formattedValue)}</td>                         
                        </c:forEach>
                        </tr>
                    </c:forEach>                   
                    <c:forEach items="${orderData.entries}" var="entryGroup" varStatus="count">
                        <c:forEach items="${entryGroup.orderEntryPrices}" var="orderEntryPrice">
                            <c:if test="${entryGroup.product.categories[0].code ne 'channelsaddoncabletica'}">
                               <c:if test="${not empty orderEntryPrice.tax911Price}">
                   				 <c:set var="tax911"> <fmt:formatNumber value="${orderEntryPrice.tax911Price.value}" pattern="#,##0" /></c:set>
                   				 </c:if>
                    		  <c:if test="${not empty orderEntryPrice.tax13Price}">
                                 <c:set var="tax13"><fmt:formatNumber value="${orderEntryPrice.tax13Price.value}" pattern="#,##0" /> </c:set>
                              </c:if>    
                            </c:if>                        
                        </c:forEach>
                    </c:forEach>
                    <tr>
                        <td class="cart-bundle-package"><spring:theme code="basket.page.totals.grossTax.13percent" text="Tax"/></td>
                        <td>${currentCurrency.symbol}${tax13}</td>
                    </tr>
                    <tr>
                        <td class="cart-bundle-package"><spring:theme code="text.cabletica.911.tax" /></td>
                        <td>${currentCurrency.symbol}${tax911}</td>
                    </tr>            
                    <tr class="cartTotalMonthly">
                        <td class="cart-bundle-package"><spring:theme code="text.cabletica.subtotal.payMonthly" htmlEscape="false" /></td>
                          <td>${currentCurrency.symbol}<fmt:formatNumber value="${orderData.monthlyTotalPrice.value}" pattern="#,##0" /></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

<input type="hidden" id="cartPagePriceInfo"
    <c:if test="${subTotalPrice ne null}"> data-attr-subtotal="${subTotalPrice}" </c:if>
    <c:if test="${orderData.totalDiscounts ne null}"> data-attr-savings="${orderData.totalDiscounts.formattedValue}" </c:if>
    <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
    <c:if test="${tax911 ne null}"> data-attr-taxRate911='${tax911}' </c:if>
    <c:if test="${tax13 ne null}"> data-attr-taxRate13='${tax13}' </c:if>
    <c:if test="${orderData.totalPriceWithTax ne null}"> data-attr-cartTotal='<fmt:formatNumber value="${orderData.totalPriceWithTax.value}" pattern="#,##0" />' </c:if>
/>
