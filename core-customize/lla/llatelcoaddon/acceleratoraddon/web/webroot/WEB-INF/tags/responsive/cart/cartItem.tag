<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="entry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData"%>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="count" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="order-entry" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/entry"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:if test="${not empty entry}">
    <c:if test="${cmsSite.uid eq 'jamaica'}">
	    <div class="clearfix cart-item-list-details <c:if test="${entry.discountFlag eq true}">online-promo</c:if>">
			<c:if test="${entry.discountFlag eq true}">
				<div class="online-offer">
				<spring:theme code="text.online.discount" />
				</div>
			</c:if>
			<div class="col-xs-12">
				<order-entry:productDetails orderEntry="${entry}" order="${cartData}" showStock="false" />
				<div class="payment-details">
					<div class="actualPrice">
						<order-entry:price orderEntry="${entry}" monthlyTotalPrice="${cartData.monthlyTotalPrice}"/>
					</div>
				</div>
				<order-entry:productImage orderEntry="${entry}" />
				<order-entry:menu orderEntry="${entry}" />
				<order-entry:jamaicaProductSpecs orderEntry="${entry}" order="${cartData}" />
			</div>
			<order-entry:cartEntryQuantity entry="${entry}" />
	    </div>
	    <div class="hidden">
			<div class="headline">
				<div class="channel-popup-title">
					<div class="bundleName"></div>
					<div class="no-of-channels"><b></b> <spring:theme code="text.bundle.channels"/></div>
				</div>
			</div>
			<div id="channels-popup" class="channels-popup">
				<div class="channels-list"></div>
			</div>
		</div>
    </c:if>
	
    <c:if test="${cmsSite.uid eq 'panama' || cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'cabletica'}">
    	<c:if test="${cmsSite.uid eq 'cabletica'}">
			<div class="cart-item">
			<h3 class="item-head"><spring:theme code="basket.page.cart.item.head"/></h3>
		</c:if>
        <div class="clearfix cart-item-list-details">
	        <c:if test="${cmsSite.uid eq 'panama' || cmsSite.uid eq 'cabletica'}">
                <div class="${cmsSite.uid eq 'cabletica' ? 'col-xs-4 col-sm-3' : 'col-xs-4 col-sm-4'}">
                    <order-entry:productImage orderEntry="${entry}" />
                </div>
                <div class="${cmsSite.uid eq 'cabletica' ? 'col-xs-12 col-sm-8 ' : 'col-xs-7 col-sm-7 '} ${entry.product.categories[0].name != 'Add on' ? 'hideqtybox':'addon'}">
                    <order-entry:productDetails orderEntry="${entry}" order="${cartData}" showStock="false" />
                    <order-entry:cartEntryQuantity entry="${entry}" />
					<order-entry:price orderEntry="${entry}" totalPriceWithTax="${cartData.totalPriceWithTax}" />
                </div>
                <c:if test="${entry.product.categories[0].name != 'Add on'}">
                    <div class="col-xs-1 col-sm-1 deleteForm <c:if test="${cmsSite.uid eq 'cabletica'}">hide</c:if>">
                        <order-entry:menu orderEntry="${entry}" />
                    </div>
                </c:if>
            </c:if>
            <c:if test="${cmsSite.uid eq 'puertorico'}">
            	<div class="col-xs-12">
					<div class="row">
            	    <p class="productCatName">${entry.product.categories[0].name}</p>
	            	<div class="item-details">
<%-- 		                <order-entry:productImage orderEntry="${entry}" /> --%>
		                <order-entry:productDetails orderEntry="${entry}" order="${cartData}" showStock="false" />
		                <c:if test="${cmsSite.uid eq 'cabletica'}">
		                 <order-entry:price orderEntry="${entry}" />
		                 </c:if>
		            </div>
					</div>
	            </div>
            </c:if>
        </div>
        <c:if test="${cmsSite.uid eq 'cabletica'}"></div></c:if>
    </c:if>
</c:if>
