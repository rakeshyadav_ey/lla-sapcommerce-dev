<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="multi-checkout"
	tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="multi-checkout-telco"
	tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>
<%@ taglib prefix="telco-structure"
	tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/structure"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<spring:url value="/checkout/multi/summary/placeOrder"
	var="placeOrderUrl" htmlEscape="false" />
<spring:url value="/checkout/multi/termsAndConditions"
	var="getTermsAndConditionsUrl" htmlEscape="false" />
<c:set var="colSm6cssClass" value="col-sm-6" />

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">

<div class="container">
	<c:if test="${cmsSite.uid eq 'cabletica' || cmsSite.uid eq 'jamaica' || cmsSite.uid eq 'panama'}">
		<multi-checkout-telco:checkoutheader />
	</c:if>
	<c:if test="${cmsSite.uid eq 'panama'}">
		<div class="row">
			<div class="col-xs-12">
				<h3 class="mt-0 mb-0 top-title">
				<spring:theme code="checkout.top.heading.panama"/>
				</h3>
			</div>
		</div>
	</c:if>
	<div class="row">
		<div class="${colSm6cssClass}">
			<%-- 		<telco-structure:checkoutHeadline/> --%>
			<multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}"
				progressBarId="${progressBarId}">
			   <c:if test="${cmsSite.uid eq 'panama'}">
                  <multi-checkout-telco:panamaPlaceOrderForm placeOrderUrl="${placeOrderUrl}" cartCalculationMessage="${cartCalculationMessage}" getTermsAndConditionsUrl="${getTermsAndConditionsUrl}"/>
              </c:if>
			  <c:if test="${cmsSite.uid eq 'jamaica'}">
                  <multi-checkout-telco:jamaicaPlaceOrderForm placeOrderUrl="${placeOrderUrl}" cartCalculationMessage="${cartCalculationMessage}" getTermsAndConditionsUrl="${getTermsAndConditionsUrl}"/>
              </c:if>
               <c:if test="${cmsSite.uid eq 'cabletica'}">
                                <multi-checkout-telco:cableticaPlaceOrderForm placeOrderUrl="${placeOrderUrl}" cartCalculationMessage="${cartCalculationMessage}" getTermsAndConditionsUrl="${getTermsAndConditionsUrl}"/>
               </c:if>
			</multi-checkout:checkoutSteps>
		</div>

		<div class="hide-cntent ${colSm6cssClass} <c:if test="${cmsSite.uid eq 'jamaica'}"> hidden-xs</c:if> ">
			<multi-checkout-telco:checkoutOrderDetails cartData="${cartData}"
				showDeliveryAddress="true" showPaymentInfo="true" />
				<c:if test="${cmsSite.uid eq 'panama'}">
     			   	<div class="contact-cntnr"><multi-checkout-telco:panamaContactUs/></div>
				</c:if>
				<c:if test="${cmsSite.uid eq 'puertorico'}">
					<multi-checkout-telco:puertoricoPlaceOrderForm placeOrderUrl="${placeOrderUrl}" cartCalculationMessage="${cartCalculationMessage}" getTermsAndConditionsUrl="${getTermsAndConditionsUrl}"/>
				</c:if>
				<c:if test="${cmsSite.uid eq 'cabletica'}">
     			   	<multi-checkout-telco:cableticaContactUs/>
				</c:if>
		</div>

	</div>
	<c:if test="${cmsSite.uid eq 'panama'}">
		<div class="row">
			<div class="col-xs-12">
				<div class="cart-footer-cntnr">
					<multi-checkout-telco:cartFooterText/>
				</div>
			</div>
		</div>
	</c:if>

</div>

</template:page>
