<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="multi-checkout-telco" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true" />
    <c:url value="/callBack" var="callBackUrl"/>
<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
	<div class="container">
        <c:if test="${cmsSite.uid eq 'cabletica' || cmsSite.uid eq 'panama'}">
            <multi-checkout-telco:checkoutheader />
        </c:if>
<c:if test="${cmsSite.uid eq 'panama'}">
    <div class="row">
      <div class="col-xs-12">
        <h3 class="mt-0 mb-0 top-title">
          <spring:theme code="checkout.top.heading.panama"/>
        </h3>
      </div>
    </div>
</c:if>
<div class="row">
    <div class="col-sm-6">
	    <!-- <div class="checkout-headline">
            <span class="glyphicon glyphicon-lock"></span>
            <spring:theme code="checkout.multi.secure.checkout" />
        </div> -->
        <multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
            <jsp:body>
                <ycommerce:testId code="checkoutStepOne">
                <c:if test="${cmsSite.uid eq 'puertorico'}">
                    <div class="divOverlay addressValidationLoader">
                        <div class="gifOverlay"></div>
                        <p class="msg"><spring:theme code="checkout.multi.addressValidation.loaderMsg" /></p>
                    </div>
                </c:if>
                    <div class="checkout-shipping">
                           <%--  <multi-checkout:shipmentItems cartData="${cartData}" showDeliveryAddress="false" /> --%>
                                    <c:if test="${cmsSite.uid eq 'cabletica'}">
                                        <div class="comn-vali-msg">
                                            <spring:theme code="checkout.mandatory.message" htmlEscape="false" />
                                        </div>
                                    </c:if>
                                    <c:choose>
                                      <c:when test="${cmsSite.uid eq 'puertorico'}">
                                        <address:addressFormSelector supportedCountries="${countries}" regions="${regions}" cancelUrl="${currentStepUrl}" country="${country}" urbanResidenceTypes="${urbanResidenceTypes}" ruralResidenceTypes="${ruralResidenceTypes}"/>
                                      </c:when>
                                      <c:otherwise>
                                        <address:addressFormSelector supportedCountries="${countries}" regions="${regions}" cancelUrl="${currentStepUrl}"  country="${country}"/>
                                      </c:otherwise>
                                    </c:choose>

                                        <div id="addressbook">
                                            <spring:url var="selectDeliveryAddressUrl" value="{contextPath}/checkout/multi/delivery-address/select" htmlEscape="false">
                                                <spring:param name="contextPath" value="${request.contextPath}" />
                                            </spring:url>

                                            <c:forEach items="${deliveryAddresses}" var="deliveryAddress" varStatus="status">
                                                <div class="addressEntry">
                                                    <form action="${fn:escapeXml(selectDeliveryAddressUrl)}" method="GET">
                                                        <input type="hidden" name="selectedAddressCode" value="${fn:escapeXml(deliveryAddress.id)}" />
                                                       <ul>
                                                <li>
                                                     
                                                   <strong>
                                                       ${ycommerce:encodeHTML(deliveryAddress.firstName)}&nbsp;
                                                       ${ycommerce:encodeHTML(deliveryAddress.lastName)}
                                                   </strong>
                                                   <br>
                                                      <c:if test="${cmsSite.uid eq 'panama' || cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'jamacia' || cmsSite.uid eq 'cabletica'}">
                                                        <c:if test="${deliveryAddress.line1 ne null}"> ${fn:escapeXml(deliveryAddress.line1)}&nbsp;<br></c:if>
                                                        <c:if test="${deliveryAddress.line2 ne null}"> ${fn:escapeXml(deliveryAddress.line2)}&nbsp;<br></c:if>
                                                        <c:if test="${deliveryAddress.appartment ne null}"> ${ycommerce:encodeHTML(deliveryAddress.appartment)}&nbsp;</c:if>
                                                        <c:if test="${deliveryAddress.building ne null}"> ${ycommerce:encodeHTML(deliveryAddress.building)}&nbsp;</c:if>
                                                        <c:if test="${deliveryAddress.streetname ne null}"> ${ycommerce:encodeHTML(deliveryAddress.streetname)}&nbsp;<br></c:if>
                                                        <c:if test="${deliveryAddress.neighbourhood ne null}"> ${ycommerce:encodeHTML(deliveryAddress.neighbourhood)}&nbsp;</c:if>
                                                        <c:if test="${deliveryAddress.town ne null}"> ${ycommerce:encodeHTML(deliveryAddress.town)}&nbsp;</c:if></br>
                                                        <c:if test="${deliveryAddress.district ne null}"> ${ycommerce:encodeHTML(deliveryAddress.district)}&nbsp;</c:if>
                                                        <c:if test="${deliveryAddress.province ne null}"> ${ycommerce:encodeHTML(deliveryAddress.province)}&nbsp;<br></c:if>
                                                        <c:if test="${deliveryAddress.country.name ne null}"> ${ycommerce:encodeHTML(deliveryAddress.country.name)}&nbsp;</c:if>
                                                        <c:if test="${deliveryAddress.postalCode ne null}"> ${fn:escapeXml(deliveryAddress.postalCode)}<br><br></c:if>
                                                      </c:if>   
                                             </li>
                                            </ul>
                                                        <button type="submit" class="btn btn-primary btn-block">
                                                            <spring:theme code="checkout.multi.deliveryAddress.useThisAddress" />
                                                        </button>
                                                    </form>
                                                </div>
                                            </c:forEach>
                                        </div>

                                        <address:suggestedAddresses selectedAddressUrl="/checkout/multi/delivery-address/select" />
                            

                                <multi-checkout:pickupGroups cartData="${cartData}" />
                                
                                <c:if test="${cmsSite.uid eq 'puertorico'}">
                                	<div class="addressSubmitButton">
                                        <button id="addressSubmit" type="button" class="btn btn-primary btn-block checkout-next"><spring:theme code="checkout.multi.checkAddress.next"/></button>
                                    </div>
                                </c:if>
                    </div>
                    <c:if test="${cmsSite.uid eq 'puertorico'}">
                        <form class="addressList" style="display:none">
                            <p class="addressText"><spring:theme code="checkout.multi.puertorico.addressTextData"/></p>
                            <div class="listData">
                            </div>
                        </form>
                        <div class="divOverlay checkServiceabilityLoader">
                            <div class="gifOverlay"></div>
                            <p class="msg"><spring:theme code="checkout.multi.checkServiceability.loaderMsg" /></p>
                        </div>
                        <div class="selectedAddress" style="display:none">
                            <p class="selectedAddressText"><spring:theme code="checkout.multi.puertorico.selectedAddressText"/></p>
                            <div class="addressDetails">
                                <div class="data">
                                    <label id="selectedAddr"class="selectedAddressLabel"></label>
                                    <input type="radio" id="addressSelected" name="addressSelected" checked="checked"/>
                                    <span class="checkMarkDesign"></span>
                                </div>
                            </div>
                            <button id="selectedAddressSubmit" type="button" class="btn btn-primary btn-block checkout-next"><spring:theme code="checkout.multi.checkServiceability.continue"/></button>
                            <button id="selectedAddressEdit" type="button" class="btn btn-primary btn-block checkout-next"><spring:theme code="checkout.multi.checkServiceability.edit"/></button>
                          </button>
                        </div>
                        <div id="callBackUrl" style="display:none"><a id="noAddressFound" href="#"><spring:theme code="text.no.address.found"/></a></div>
                     </c:if>
                    <c:if test="${cmsSite.uid eq 'panama'}">
                        <div class="comn-vali-msg">
                            <spring:theme code="checkout.mandatory.message" htmlEscape="false" />
                        </div>
                    </c:if>
					<c:if test="${cmsSite.uid ne 'puertorico'}">
                    	<button id="addressSubmit" type="button"
                        	class="btn btn-primary btn-block checkout-next"><spring:theme code="checkout.multi.deliveryAddress.continue"/></button>
                    </c:if>
                </ycommerce:testId>
            </jsp:body>
        </multi-checkout:checkoutSteps>
    </div>
    <c:if test="${cmsSite.uid ne 'cabletica'}">
    <div class="col-sm-6 hidden-xs hide-cntent">
		<multi-checkout:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="false" showPaymentInfo="false" showTaxEstimate="false" showTax="true" />
    </div>
    </c:if>
    <c:if test="${cmsSite.uid eq 'cabletica'}">
        <div class="col-sm-6">
            <multi-checkout-telco:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="true" />
            <multi-checkout-telco:cableticaContactUs/>
        </div>
    </c:if>

</div>
</div>

</template:page>
