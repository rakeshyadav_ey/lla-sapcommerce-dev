<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/cart"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${cmsSite.uid ne 'jamaica'}">
	<div class="cart-totals">
	    <c:if test="${cmsSite.uid eq 'puertorico'}">
		    <cart:cartTotals cartData="${cartData}" showTaxEstimate="${taxEstimationEnabled}" />
		</c:if>
		<c:if test="${cmsSite.uid eq 'panama'}">
            <cart:panamaCartTotals cartData="${cartData}" showTaxEstimate="${taxEstimationEnabled}" />
        </c:if>
		<c:if test="${cmsSite.uid eq 'cabletica'}">
            <cart:cableticaCartTotals cartData="${cartData}" showTaxEstimate="${taxEstimationEnabled}" />
        </c:if>
	</div>
</c:if>



