<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="multi-checkout"
	tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="multi-checkout-telco"
	tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>
<%@ taglib prefix="telco-structure"
	tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/structure"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>

<spring:theme code="checkout.multi.paymentMethod.continue" text="Next"
	var="next" />

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}"
					progressBarId="${progressBarId}">
					<jsp:body>
                        <div class="divOverlay addressValidationLoader">
                            <div class="gifOverlay"></div>
                            <p class="msg"><spring:theme code="checkout.multi.creditAddressValidation.loaderMsg" /></p>
                        </div>
                        <div id="creditCheckStep" class="customer-additional-details">
                            <spring:url value="/checkout/multi/credit-check/update" var="creditCheckUrl" scope="session" htmlEscape="false" />
                            <div id="selectCreditCheck">
                                <p class="ssnTitle"><spring:message code="text.checkout.puertorico.ssnTitle" /></p>
								<c:url value="/checkout/multi/credit-check/updateCustomerSSN" var="UpdateCustomerSSNUrl" />
                                <form:form action="${UpdateCustomerSSNUrl}" method="post" id="customerSSNForm" modelAttribute="customerSSNForm">
                            		<c:if test="${cmsSite.uid eq 'puertorico'}">
                            			<formElement:formInputBox  idKey="customercheckSSN" labelKey="checkout.credit.ssn" path="ssn" inputCSS="form-control" mandatory="true" maxlength="11" />
                            			<p class="hidden" id="ssn-val"></p>
                            		</c:if>
                                    <input type="hidden" id="ssnErrorMsgEmpty" value="<spring:theme code="register.ssn.lcpr.empty"/>" >
                                    <input type="hidden" id="ssnErrorMsgInvalid" value="<spring:theme code="register.ssn.lcpr.invalid"/>" >
                            		<div class="alert alert-info alert-dismissable getAccAlert termsInfo">
                                        <spring:message code="text.checkout.multi.puertorico.creditCheckInfo" />
                                    </div>
				    <c:url value="/checkout/multi/credit-check/cancel-credit-check" var="CancelCreditCheckUrl" />
                                    <a href="${CancelCreditCheckUrl}" class="creditSkipLink"><spring:message code="text.checkout.puertorico.skipCreditCheck" /></a>

                            		<button type="submit" id="updateSSNSubmit" class="btn btn-block btn-primary creditCheckWithNewAddress checkout-next">
                                         <spring:message code="checkout.multi.puertorico.creditCheck.submitCreditSelect" />
                                	</button>
                            	</form:form>
                                <div class="alert alert-info alert-dismissable getAccAlert termsInfo creditCheckNo" style="display:none">
                                    <spring:message code="text.checkout.multi.puertorico.creditCheckNoStep4.message" />
                                    <div class="readmoreText" style="display:none">
                                        <p class="termsText"><spring:message code="text.checkout.puertorico.creditCheckNo.termsInfo" /></p>
                                        <p class="readLess"><spring:message code="text.checkout.puertorico.readless.text" /></p>
                                    </div>
                                </div>
                            </div>
                            <div id="selectCreditCheckAddr">
                                <p class="creditCheckText"> <spring:message code="checkout.multi.puertorico.creditCheckAddress" /> </p>
                                <%-- <p class="creditCheckSubText"> <spring:message code="checkout.multi.puertorico.creditChecksubAddress" /> </p> --%>
                                <div class="selectAddrressCheckbox">
                                    <div class="checkAddrr">
                                        <label class="checkLabel">
                                            <span><spring:message code="checkout.multi.puertorico.creditCheck.selctedAddrOptionYes" /></span>
                                            <input type="checkbox" class="creditCheckSelect" id="selectedAddr" name="selectedAddr" checked/>
                                            <span class="checkmark"></span>
                                        </label>
                                        <p class="creditCheckSameAddrText"><spring:message code="checkout.multi.puertorico.creditCheck.selctedAddrOptionYesText" /></p>
                                        <label class="checkLabel noOptionCheck">
                                            <span><spring:message code="checkout.multi.puertorico.creditCheck.selctedAddrOptionNo" /></span>
                                            <input type="checkbox" class="creditCheckSelect" id="creditCheckdAddr" name="creditCheckdAddr" />
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                                <%-- <div class="alert alert-info alert-dismissable getAccAlert savedAddress">
                                    <p><spring:message code="text.checkout.multi.puertorico.creditCheck.creditCheckAddr" /></p>
                                </div> --%>
                                <div class="alert alert-info alert-dismissable getAccAlert newAddress">
                                    <p><spring:message code="text.checkout.multi.puertorico.creditCheck.creditCheckNewAddr" /></p>
                                </div>

                                <button class="btn btn-block btn-primary creditCheckWithSavedAddress checkout-next">
                                    <spring:theme code="checkout.multi.checkCreditAddress.button" />
                                </button>

                            </div>

                           <div id="newCreditCheckAddr">
                           	  <div class="addNewCreditCheckAdrress">
                        <c:url value="/checkout/multi/credit-check/updateCreditCheckAddress" var="CreditCheckActionUrl" />
                        <form:form action="${CreditCheckActionUrl}" method="post" id="creditCheckForm" modelAttribute="addressForm">
                            <c:if test="${cmsSite.uid eq 'puertorico'}">
                                 <div class="controls">
                                     <div class="addressTypes">
                                         <div class="addressTypeRadio selected">
                                             <label class="addressRadioLabel" for="walkUp"><spring:theme code="checkout.multi.type.residence.walkUp" />
                                                 <form:radiobutton path="typeOfResidence" class="addressRadioButton" id="walkUp" value="WALKUP" checked="checked"/>
                                                 <span class="checkmark"></span>
                                             </label>
                                         </div>

                                         <div class="addressTypeRadio">
                                             <label class="addressRadioLabel" for="urban"><spring:theme code="checkout.multi.type.residence.urban" />
                                                 <form:radiobutton path="typeOfResidence" class="addressRadioButton" id="urban" value="URBAN"/>
                                                 <span class="checkmark"></span>
                                             </label>
                                         </div>

                                         <div class="addressTypeRadio">
                                             <label class="addressRadioLabel" for="rural"><spring:theme code="checkout.multi.type.residence.rural" />
                                                 <form:radiobutton path="typeOfResidence" class="addressRadioButton" id="rural" value="RURAL"/>
                                                 <span class="checkmark"></span>
                                             </label>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="addressTypesFields">
                                     <div id="walkUpFormElements">
                                        <address:walkupAddressFormElements />
                                     </div>
                                     <div id="urbanFormElements">
                                        <address:urbanAddressFormElements urbanResidenceTypes="${urbanResidenceTypes}" />
                                     </div>
                                     <div id="ruralFormElements">
                                        <address:ruralAddressFormElements ruralResidenceTypes="${ruralResidenceTypes}" />
                                     </div>
                                     <button type="submit" id="creditCheckSubmit" class="btn btn-block btn-primary creditCheckWithNewAddress checkout-next">
                                         <spring:theme code="checkout.multi.checkCreditAddress.button" />
                                     </button>
                                 </div>
                                <div id="addressFormErrors">
                                    <input type="hidden" id="errorMsgStreetname" value="<spring:theme code="addess.walkup.streetname.invalid"/>" >
                                    <input type="hidden" id="errorMsgApartment" value="<spring:theme code="addess.walkup.appartment.invalid"/>" >
                                    <input type="hidden" id="errorMsgTowncity" value="<spring:theme code="addess.walkup.townCity.invalid"/>" >
                                    <input type="hidden" id="errorMsgPostcode" value="<spring:theme code="addess.walkup.postcode.invalid"/>" >
                                    <input type="hidden" id="errorMsgPostcodeLength" value="<spring:theme code="addess.walkup.postcodelength.invalid"/>" >
                                    <input type="hidden" id="errorMsgBuilding" value="<spring:theme code="addess.urban.building.invalid"/>" >
                                    <input type="hidden" id="errorMsgKilometer" value="<spring:theme code="addess.rural.kilometer.invalid"/>" >
                                    <input type="hidden" id="errorMsgHighway" value="<spring:theme code="addess.rural.line1.invalidHighway"/>" >
                                    <input type="hidden" id="errorMsgStreet" value="<spring:theme code="addess.rural.line1.invalidStreet"/>" >
                                </div>
                            </c:if>
                        </form:form>
                    </div>
                   </div>
                  </div>
                 </jsp:body>
			  </multi-checkout:checkoutSteps>
			</div>
			<div class="col-sm-6 hidden-xs">
				<multi-checkout-telco:checkoutOrderDetails cartData="${cartData}"
					showDeliveryAddress="true" />
			</div>
		</div>
	</div>
</template:page>
