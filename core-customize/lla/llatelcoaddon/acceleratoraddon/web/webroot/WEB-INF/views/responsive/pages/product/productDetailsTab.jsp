<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${productCategories[0].code != '4Pbundles'}" var="booleanValue">
<div class="tabhead">
	<a href="">${ycommerce:encodeHTML(title)}</a> <span class="glyphicon"></span>
</div>
<div class="tabbody">
	<div class=" ${cmsSite.uid eq 'panama' ? 'container':'container-lg'} ">
		<div class="row">
			<div class="${cmsSite.uid eq 'jamaica'? 'col-sm-10 col-md-6 col-lg-6 col-sm-offset-1 col-md-offset-3 col-lg-offset-3':'col-sm-10'}">
				<div class="tab-container">
					<product:productDetailsTab product="${product}" />
				</div>
			</div>
		</div>
	</div>
</div>
</c:if>