<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${productCategories[0].code == '3Pbundles' || productCategories[0].code == '4Pbundles' || productCategories[0].code == '2PInternetTelephone' || productCategories[0].code == '2PInternetTv'}" var="booleanValue">

	<div class="tabhead">
		<a href="">${fn:escapeXml(title)}</a> <span class="glyphicon"></span>
	</div>
	<div class="tabbody">
		<div class=" ${cmsSite.uid eq 'panama' ? 'container':'container-lg'} ">
			<div class="row">
				<div class="col-sm-10">
					<div class="tab-container">
						<product:disclaimerTab product="${product}" />
					</div>
				</div>
			</div>
		</div>
	</div>

</c:if>