<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<spring:htmlEscape defaultHtmlEscape="true" />
<c:url value="${component.url}" var="addToCartUrl"/>
<spring:url value="${product.url}/configuratorPage/{/configuratorType}" var="configureProductUrl" htmlEscape="false">
    <spring:param name="configuratorType"  value="${configuratorType}"/>
</spring:url>

<product:addToCartTitle/>

<c:forEach items="${cartData.entries}" var="entry">
 <input type="hidden" class="existingCartProductPDP" data-attr-id="${entry.product.code}"
    data-attr-price="${entry.product.monthlyPrice.value}"
    data-attr-name="${entry.product.name}"
    data-attr-category="${entry.product.categories[0].name}" data-attr-category_2="${entry.product.categories[0].parentCategoryName}"
 />
</c:forEach>

<form:form method="post" id="configureForm" class="configure_form" action="${configureProductUrl}">
    <c:if test="${product.purchasable}">
        <input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
    </c:if>
    <input type="hidden" name="productCodePost" value="${fn:escapeXml(product.code)}"/>

    <c:if test="${empty showAddToCart ? true : showAddToCart}">
        <c:set var="buttonType">button</c:set>
        <c:if test="${product.purchasable and product.stock.stockLevelStatus.code ne 'outOfStock' }">
            <c:set var="buttonType">submit</c:set>
        </c:if>
        <c:choose>
            <c:when test="${fn:contains(buttonType, 'button')}">
                <c:if test="${product.configurable}">
                    <button id="configureProduct" type="button" class="btn btn-primary btn-block js-enable-btn outOfStock" disabled="disabled">
                        <spring:theme code="basket.configure.product"/>
                    </button>
                </c:if>
            </c:when>
            <c:otherwise>
                <c:if test="${product.configurable}">
                    <button id="configureProduct" type="${buttonType}" class="btn btn-primary btn-block js-enable-btn" disabled="disabled"
                            name="configure">
                        <spring:theme code="basket.configure.product"/>
                    </button>
                </c:if>
            </c:otherwise>
        </c:choose>
    </c:if>
</form:form>

<%-- <c:if test="${cmsSite.uid eq 'panama'&& product.categories[0].code == '4Pbundles'}">
	<div class="qtyDiv">
	<c:set var="qtyMinus" value="0"/>
	<p class="qtyHeading"><spring:theme code="pdp.bundle.additional.devices.heading"/></p>
</c:if> --%>

<form:form method="post" id="addToCartForm" class="${cmsSite.uid eq 'panama' && (product.categories[0].code == '4Pbundles') ? 'add_to_cart_form_popup' : 'add_to_cart_form'}" action="${addToCartUrl}">
    <c:if test="${cmsSite.uid eq 'panama'&& product.categories[0].code == '4Pbundles'}">
    <c:forEach items="${addonProducts}" var="addonProduct" varStatus="loop">
		<div class="qtyItemInfo">
			<%-- <p class="box-type"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>${fn:escapeXml(addonProduct.name)}</p> --%>
        <input type="hidden" name="productCodePostList" value="${fn:escapeXml(addonProduct.code)}"/>
        <input type="hidden" name="productName" value="${fn:escapeXml(addonProduct.name)}"/>
        <input type="hidden" class="addonproductdatapdp" name="productprice" value="${fn:escapeXml(addonProduct.price.value)}"/>
			<div class="qty-selector input-group js-qty-selector_${fn:escapeXml(addonProduct.code)} hidden">
				<span class="input-group-btn hidden">
					<c:if test="${qtyMinus <= 0}">
						<c:set var="disabled" value='disabled'/>
					</c:if>
					<button class="btn btn-default ${fn:escapeXml(addonProduct.code)}-qty-minus" type="button" ${disabled}>
						<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
					</button>
				</span>

				<input type="hidden" maxlength="1" class="form-control qty js-qty-selector-input" size="1" value="0"
						data-max="${addonMaxOrderQuantityMap[addonProduct.code]}"
						data-min="0" name="qtyList"  id="${fn:escapeXml(addonProduct.code)}_Qty" >

				<span class="input-group-btn">
					<button class="btn btn-default ${fn:escapeXml(addonProduct.code)}-qty-plus" type="button">
						<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
					</button>
				</span>
			</div>
		 </div>
    </c:forEach>
    </c:if>
    <c:if test="${product.purchasable}">
        <input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
    </c:if>
    <input type="hidden" name="productCodePost" value="${fn:escapeXml(product.code)}"/>
    <input type="hidden" name="processType" value="DEVICE_ONLY"/>

    <c:if test="${empty showAddToCart ? true : showAddToCart}">
        <c:set var="buttonType">button</c:set>
        <c:if test="${product.purchasable and product.stock.stockLevelStatus.code ne 'outOfStock' }">
            <c:set var="buttonType">submit</c:set>
        </c:if>
        <c:if test="${cmsSite.uid eq 'panama'}">
        	<div class="btnqtyItemInfo">
        </c:if>
        <c:choose>
            <c:when test="${fn:contains(buttonType, 'button')}">
                <button type="${buttonType}" class="btn btn-primary btn-block js-add-to-cart btn-icon glyphicon-shopping-cart outOfStock" disabled="disabled">
                    <spring:theme code="product.variants.out.of.stock"/>
                </button>
            </c:when>
            <c:otherwise>
                <ycommerce:testId code="addToCartButton">
                    <button id="addToCartButton" type="${buttonType}"
                    class="btn btn-primary btn-block js-add-to-cart js-enable-btn btn-icon glyphicon-shopping-cart ${cmsSite.uid eq 'panama' && (product.categories[0].code == '4Pbundles') ? 'addToCartFmcPopupGTM' : ''} ${language == 'es' ? ' esFmcBuyNowBtn': ''}"
                    disabled="disabled"
                     data-attr-item-code="${fn:escapeXml(product.code)}"
                    >
                        <spring:theme code="basket.add.to.basket"/>
                    </button>
                    <c:if test="${cmsSite.uid eq 'panama' && (product.categories[0].code == '4Pbundles' || product.categories[0].code == '3Pbundles' || product.categories[0].code == '2PInternetTv' || product.categories[0].code == '2PInternetTelephone')}">
                        <span class="tooltiptext ${product.bundlePresentInCart ? 'show':'hide'}"><spring:theme code="pdp.fmc.buttondisable.tooltip.text"/></span>
                    </c:if>
                </ycommerce:testId>
            </c:otherwise>
        </c:choose>
        <c:if test="${cmsSite.uid eq 'panama'}">
            </div>
        </c:if>
    </c:if>
</form:form>
<c:if test="${cmsSite.uid eq 'panama'}">
	</div>
</c:if>
