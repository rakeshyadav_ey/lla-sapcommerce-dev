<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<%--@elvariable id="pageTitle" type="java.lang.String"--%>

<template:page pageTitle="${pageTitle}">
	<cms:pageSlot position="Section1" var="comp" element="div" class="productDetailsPageSection1">
		<cms:component component="${comp}" element="div" class="productDetailsPageSection1-component"/>
	</cms:pageSlot>
	<c:choose>	
		<c:when test="${product.categories[0].code == 'devices'}">
			<c:if test="${cmsSite.uid ne 'puertorico' && cmsSite.uid ne 'cabletica'}">
				<product:productDetailsPanelDevices/>
			</c:if>
		</c:when>
		<c:otherwise>
			<product:productDetailsPanel/> 
		</c:otherwise>
	</c:choose>	

	<cms:pageSlot position="CrossSelling" var="comp" element="div" class="productDetailsPageSectionCrossSelling">
		<cms:component component="${comp}" element="div" class="productDetailsPageSectionCrossSelling-component"/>
	</cms:pageSlot>
	<cms:pageSlot position="Section2" var="comp" element="div" class="productDetailsPageSection2">
		<cms:component component="${comp}" element="div" class="productDetailsPageSection2-component"/>
	</cms:pageSlot>
	<cms:pageSlot position="Section3" var="comp" element="div" class="productDetailsPageSection3">
		<cms:component component="${comp}" element="div" class="productDetailsPageSection3-component"/>
	</cms:pageSlot>
	<cms:pageSlot position="UpSelling" var="comp" element="div" class="productDetailsPageSectionUpSelling">
		<cms:component component="${comp}" element="div" class="productDetailsPageSectionUpSelling-component"/>
	</cms:pageSlot>
	<product:productPageTabs/>
	<c:if test="${product.categories[0].code == '4Pbundles' && cmsSite.uid eq 'panama'}">
	    <cms:pageSlot position="Section4" var="comp" element="div" class="productDetailsPageSection4">
		    <cms:component component="${comp}" element="div" class="productDetailsPageSection4-component container"/>
	    </cms:pageSlot>
	</c:if>
</template:page>
