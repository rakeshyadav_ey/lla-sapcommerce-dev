<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<c:if test="${productCategories[0].code == 'tvpackages'}" var="booleanValue">
<div class="tabhead">
	<a href="">${fn:escapeXml(title)}</a> <span class="glyphicon"></span>
</div>
<div class="tabbody">
	<div class="container-lg">
		<div class="row">
			<div class="col-sm-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
				<div class="tab-container">
					<product:waysToWatchTab product="${product}" />
				</div>
			</div>
		</div>
	</div>
</div>

</c:if>