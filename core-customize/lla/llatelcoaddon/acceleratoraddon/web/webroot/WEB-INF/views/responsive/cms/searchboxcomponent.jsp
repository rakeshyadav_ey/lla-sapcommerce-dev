<%@ page trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--@elvariable id="component" type="de.hybris.platform.acceleratorcms.model.components.SearchBoxComponentModel"--%>

<spring:htmlEscape defaultHtmlEscape="true" />

<spring:url value="/search/" var="searchUrl" htmlEscape="false" />
<spring:url value="/search/autocomplete/{/componentuid}" var="autocompleteUrl" htmlEscape="false">
	<spring:param name="componentuid" value="${component.uid}" />
</spring:url>

<div class="ui-front">
	<form name="search_form_${ycommerce:encodeHTML(component.uid)}" method="get" action="${searchUrl}">
		<div class="input-group inner-addon left-addon">
			<spring:theme code="search.placeholder" var="searchPlaceholder" text="I'm looking for" />
			<c:if test="${cmsSite.uid eq 'jamaica'}">
				<span class="search-icon"></span> 
			</c:if>
			<input type="text" id="js-site-search-input" class="form-control js-site-search-input" name="text" value="" 
			 maxlength="100" placeholder="${searchPlaceholder}" data-options='{"autocompleteUrl" : "${autocompleteUrl}","minCharactersBeforeRequest" : "${component.minCharactersBeforeRequest}",
				"waitTimeBeforeRequest" : "${component.waitTimeBeforeRequest}","displayProductImages" : ${component.displayProductImages}}'>
			
			<c:if test="${cmsSite.uid eq 'panama'}">
				<div class="input-group-btn">
					<button class="btn btn-default" type="submit">
						<i class="glyphicon glyphicon-search"></i>
					</button>
					<button class="btn btn-default hidden-xs hidden-sm" type="button" id="close-search">
						<i class="glyphicon glyphicon-remove"></i>
					</button>
				</div>
			</c:if>
		</div>
	</form>
</div>