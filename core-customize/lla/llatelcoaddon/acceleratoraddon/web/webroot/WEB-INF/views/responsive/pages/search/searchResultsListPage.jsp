<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/nav"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="b2ctelcoProduct" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/product"%>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup"%>

<spring:theme code="text.buynow" var="btn_buynow_val"/>
<span id="buynow_text" style="display:none;" data-localized-text="${btn_buynow_val}"></span>

<spring:theme code="prod.feature.text" var="feature_val"/>
<span id="feature_text" style="display:none;" data-localized-text="${feature_val}"></span>

<spring:theme code="pre.post.plan" var="pre_postpaid_val"/>
<span id="pre_postpaid_text" style="display:none;" data-localized-text="${pre_postpaid_val}"></span>

<div class="product__list--wrapper">
	<div class="results">
	    <c:if test="${!isFacetSearch}">
		    <h1><spring:theme code="search.page.searchText" arguments="${searchPageData.freeTextSearch}"/></h1>
		</c:if>
	</div>
    <nav:pagination top="true" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}"
        searchPageData="${searchPageData}" searchUrl="${searchPageData.currentQuery.url}"
        numberPagesShown="${numberPagesShown}" />
	<div class="tab-content">
       <div class="tab-pane active" id="show_grid">
         <div class="product__listing product__list prod_grid_view">
		       <c:forEach items="${searchPageData.results}" var="productData" varStatus="status">
		           <b2ctelcoProduct:productListerGridItem productData="${productData}" />
		       </c:forEach>
         </div>
       </div>
       <div class="tab-pane" id="show_list">
		 <div class="product__listing product__list prod_list_view">
			   <c:forEach items="${searchPageData.results}" var="productData" varStatus="status">
			       <b2ctelcoProduct:productListerListItem productData="${productData}" />
			   </c:forEach>
	     </div>
       </div>
    </div>
    <c:if test="${showDynamicProducts eq 'True'}">
		<div class="pager">
		    <input type="hidden" id="loadedPagesSearch" value="1" />
		    <button id="show-more-search" name="show-more" class="btn show-more btn-white"><spring:theme code="product.load.more" text="loadMore" /></button>
		</div>
	</c:if>
    <%--<nav:pagination top="false" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}"
        searchPageData="${searchPageData}" searchUrl="${searchPageData.currentQuery.url}"
        numberPagesShown="${numberPagesShown}" />--%>
    <storepickup:pickupStorePopup />
</div>
