<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="multi-checkout-telco" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>
<%@ taglib prefix="telco-structure" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/structure"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<spring:htmlEscape defaultHtmlEscape="true" />
<spring:theme code="checkout.multi.paymentMethod.continue" text="Next" var="next" />
<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
<div class="container">
  <c:if test="${cmsSite.uid eq 'panama'}">
    <div class="row">
      <div class="col-xs-12">
        <h3 class="mt-0 mb-0 top-title">
          <spring:theme code="checkout.top.heading.panama"/>
        </h3>
      </div>
    </div>
  </c:if>
 	 <div class="row">
	    <div class="col-xs-12 col-sm-12 col-md-6">
			<multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
				<jsp:body>
                    <div class="customer-additional-details">
                        <h4><spring:theme code="additional.contact.details" text="Contact Details" /></h4>
                       <spring:url value="/checkout/multi/contact-details/update" var="customerProfileUrl" scope="session" htmlEscape="false"/>
                       <form:form id="customerProfileForm" class="customerProfileForm" name="customerProfileForm" modelAttribute="customerProfileForm" action="${customerProfileUrl}" method="POST">
                         <formElement:formInputBox idKey="customer.additionalEmail" labelKey="customer.additionalEmail" path="additionalEmail" inputCSS="form-control" tabindex="1" mandatory="true" />
                         <formElement:formInputBox idKey="customer.panama.mobilePhone"  labelKey="customer.panama.mobilePhone" path="mobilePhone" inputCSS="form-control" tabindex="2" mandatory="true" />
                          <c:if test="${cmsSite.uid eq 'panama'}">
                            <div class="comn-vali-msg">
                              <spring:theme code="checkout.mandatory.message" htmlEscape="false" />
                            </div>
                          </c:if>
                         <button type="submit" class="btn btn-block btn-primary checkout-next" title="${next}">${next}</button>
                       </form:form>
                    </div>
                </jsp:body>

            </multi-checkout:checkoutSteps>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
                    <multi-checkout-telco:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="true" />
        </div>
      </div>
          <input type="hidden" id="gtmPersonalFormError" value="<spring:theme code="gtm.personalinfo.form.errors.msg"/>" >

   </div>
</template:page>
