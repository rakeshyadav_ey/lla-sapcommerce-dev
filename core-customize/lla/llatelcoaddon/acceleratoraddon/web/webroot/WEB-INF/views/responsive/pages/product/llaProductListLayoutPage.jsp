<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="secureFooter" tagdir="/WEB-INF/tags/responsive/common/footer"%>
<%@ taglib prefix="multi-checkout-telco" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>

<template:page pageTitle="${pageTitle}">
	<c:if test="${cmsSite.uid eq 'jamaica'}">
		<div class="container">
			<div class="process-selection-bar">
				<ul class="selection-state clearfix">
					<li class="active col-xs-4"><p class="level"><span class="process-state">1</span><span class="process-info"><spring:theme code="selection.level1.text"/></span></p></li>
					<li class="col-xs-4"><p class="level"><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></p></li>
					<li class="col-xs-4"><p class="level"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level3.text"/></span></p></li>
				</ul>
			</div>
		</div>
	</c:if>
	<c:if test="${cmsSite.uid ne 'jamaica'}">
	<div class="row">
		<cms:pageSlot position="Section1" var="feature" element="div" class="product-list-section1-slot">
			<cms:component component="${feature}" element="div" class="yComponentWrapper product-list-section1-component"/>
		</cms:pageSlot>
	</div>
	</c:if>
	<c:if test="${cmsSite.uid eq 'jamaica'}">	
	<div class="row">
		<cms:pageSlot position="Section2" var="feature" element="div" class="product-list-section2-slot">
			<cms:component component="${feature}" element="div" class="yComponentWrapper product-list-section2-component"/>
		</cms:pageSlot>
	</div>
	</c:if>
	<div class="row">		
		<cms:pageSlot position="ProductListSlot" var="feature" element="div" class="product-list-slot">
			<cms:component component="${feature}" element="div" class="yComponentWrapper product-list-component"/>
		</cms:pageSlot>		
	</div>
	<div class="row">
		<cms:pageSlot position="AdditionalComponentsSlot" var="feature" element="div" class="additional-components-slot">
			<cms:component component="${feature}" element="div" class="yComponentWrapper container promo-banner-row additional-components-slot"/>
		</cms:pageSlot>
	</div>


	<c:if test="${cmsSite.uid eq 'jamaica'}">
		<div class="container configurator-banner">
			<cms:pageSlot position="Section4" var="feature" element="div" class="row">
				<cms:component component="${feature}" element="div" class="spl-offer col-xs-12 yComponentWrapper" />
			</cms:pageSlot>
		</div>

		<div class="policy-text">
			<spring:theme code="usage.policy.text" htmlEscape="false" />
		</div>
		<multi-checkout-telco:footerSecuresiteLogo />
	</c:if>
	
</template:page>
