<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%--@elvariable id="totalDisplay" type="java.lang.String"--%>
<%--@elvariable id="totalItems" type="java.lang.Integer"--%>
<%--@elvariable id="component" type="de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel"--%>
<%--@elvariable id="PlanExist" type="java.lang.Boolean"--%>
<%--@elvariable id="oldProductName" type="java.lang.String"--%>
<spring:htmlEscape defaultHtmlEscape="true"/>

<spring:theme code="popup.cart.empty" text="Your Cart is empty" var="emptyCartMessage"/>
<spring:theme code="basket.items" text="Items" var="itemsTitle"/>
<spring:theme code="text.cart" text="Cart" var="cartTitle"/>

<spring:url value="/cart/miniCart/{/totalDisplay}" var="refreshMiniCartUrl" htmlEscape="false">
   <spring:param name="totalDisplay" value="${ycommerce:encodeHTML(totalDisplay)}"/>
</spring:url>
<spring:url value="/cart/rollover/{/componentUid}" var="rolloverPopupUrl" htmlEscape="false">
   <spring:param name="componentUid" value="${ycommerce:encodeHTML(component.uid)}"/>
</spring:url>
<spring:url value="/cart" var="cartUrl"/>

<input type="hidden" id="isPlanExist" value="${PlanExist}">
<div class="nav-cart ${(totalItems gt 0)?"updateqty":""}">
   <a href="${cartUrl}" class="mini-cart-link" data-mini-cart-url="${rolloverPopupUrl}" data-mini-cart-refresh-url="${refreshMiniCartUrl}" data-mini-cart-name="${cartTitle}"  data-mini-cart-empty-name="${emptyCartMessage}" data-mini-cart-items-text="${itemsTitle}">
      <div class="mini-cart-icon">
         <c:choose>
            <c:when test="${cmsSite.uid eq 'panama' || cmsSite.uid eq 'cabletica'}">

               <span class="glyphicon glyphicon-shopping-cart visible-xs visible-sm"></span>
               <span class="hidden-xs hidden-sm">
                  <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12 12" fill="none">
                     <path fill-rule="evenodd" clip-rule="evenodd" d="M0 0.4C0 0.293913 0.0421427 0.192172 0.117157 0.117157C0.192172 0.0421427 0.293913 0 0.4 0H1.6C1.68923 2.46492e-05 1.77588 0.0298805 1.84619 0.0848198C1.91649 0.139759 1.96641 0.216627 1.988 0.3032L2.312 1.6H11.6C11.6587 1.60005 11.7167 1.61304 11.7699 1.63804C11.823 1.66304 11.87 1.69944 11.9075 1.74464C11.945 1.78985 11.9721 1.84276 11.9868 1.89961C12.0016 1.95647 12.0036 2.01587 11.9928 2.0736L10.7928 8.4736C10.7756 8.56526 10.727 8.64805 10.6553 8.70765C10.5835 8.76725 10.4933 8.79991 10.4 8.8H3.2C3.10675 8.79991 3.01645 8.76725 2.94473 8.70765C2.873 8.64805 2.82436 8.56526 2.8072 8.4736L1.608 2.0856L1.288 0.8H0.4C0.293913 0.8 0.192172 0.757857 0.117157 0.682843C0.0421427 0.607828 0 0.506087 0 0.4ZM2.4816 2.4L3.532 8H10.068L11.1184 2.4H2.4816ZM4 8.8C3.57565 8.8 3.16869 8.96857 2.86863 9.26863C2.56857 9.56869 2.4 9.97565 2.4 10.4C2.4 10.8243 2.56857 11.2313 2.86863 11.5314C3.16869 11.8314 3.57565 12 4 12C4.42435 12 4.83131 11.8314 5.13137 11.5314C5.43143 11.2313 5.6 10.8243 5.6 10.4C5.6 9.97565 5.43143 9.56869 5.13137 9.26863C4.83131 8.96857 4.42435 8.8 4 8.8ZM9.6 8.8C9.17565 8.8 8.76869 8.96857 8.46863 9.26863C8.16857 9.56869 8 9.97565 8 10.4C8 10.8243 8.16857 11.2313 8.46863 11.5314C8.76869 11.8314 9.17565 12 9.6 12C10.0243 12 10.4313 11.8314 10.7314 11.5314C11.0314 11.2313 11.2 10.8243 11.2 10.4C11.2 9.97565 11.0314 9.56869 10.7314 9.26863C10.4313 8.96857 10.0243 8.8 9.6 8.8ZM4 9.6C3.78783 9.6 3.58434 9.68429 3.43431 9.83432C3.28429 9.98434 3.2 10.1878 3.2 10.4C3.2 10.6122 3.28429 10.8157 3.43431 10.9657C3.58434 11.1157 3.78783 11.2 4 11.2C4.21217 11.2 4.41566 11.1157 4.56569 10.9657C4.71571 10.8157 4.8 10.6122 4.8 10.4C4.8 10.1878 4.71571 9.98434 4.56569 9.83432C4.41566 9.68429 4.21217 9.6 4 9.6ZM9.6 9.6C9.38783 9.6 9.18434 9.68429 9.03432 9.83432C8.88429 9.98434 8.8 10.1878 8.8 10.4C8.8 10.6122 8.88429 10.8157 9.03432 10.9657C9.18434 11.1157 9.38783 11.2 9.6 11.2C9.81217 11.2 10.0157 11.1157 10.1657 10.9657C10.3157 10.8157 10.4 10.6122 10.4 10.4C10.4 10.1878 10.3157 9.98434 10.1657 9.83432C10.0157 9.68429 9.81217 9.6 9.6 9.6Z" fill="white"/>
                  </svg>
               </span>
            </c:when>
            <c:when test="${cmsSite.uid eq 'puertorico'}">
               <span class="glyphicon glyphicon-shopping-cart visible-xs"></span>
               <span class="hidden-xs">
                  <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12 12" fill="none">
                     <path fill-rule="evenodd" clip-rule="evenodd" d="M0 0.4C0 0.293913 0.0421427 0.192172 0.117157 0.117157C0.192172 0.0421427 0.293913 0 0.4 0H1.6C1.68923 2.46492e-05 1.77588 0.0298805 1.84619 0.0848198C1.91649 0.139759 1.96641 0.216627 1.988 0.3032L2.312 1.6H11.6C11.6587 1.60005 11.7167 1.61304 11.7699 1.63804C11.823 1.66304 11.87 1.69944 11.9075 1.74464C11.945 1.78985 11.9721 1.84276 11.9868 1.89961C12.0016 1.95647 12.0036 2.01587 11.9928 2.0736L10.7928 8.4736C10.7756 8.56526 10.727 8.64805 10.6553 8.70765C10.5835 8.76725 10.4933 8.79991 10.4 8.8H3.2C3.10675 8.79991 3.01645 8.76725 2.94473 8.70765C2.873 8.64805 2.82436 8.56526 2.8072 8.4736L1.608 2.0856L1.288 0.8H0.4C0.293913 0.8 0.192172 0.757857 0.117157 0.682843C0.0421427 0.607828 0 0.506087 0 0.4ZM2.4816 2.4L3.532 8H10.068L11.1184 2.4H2.4816ZM4 8.8C3.57565 8.8 3.16869 8.96857 2.86863 9.26863C2.56857 9.56869 2.4 9.97565 2.4 10.4C2.4 10.8243 2.56857 11.2313 2.86863 11.5314C3.16869 11.8314 3.57565 12 4 12C4.42435 12 4.83131 11.8314 5.13137 11.5314C5.43143 11.2313 5.6 10.8243 5.6 10.4C5.6 9.97565 5.43143 9.56869 5.13137 9.26863C4.83131 8.96857 4.42435 8.8 4 8.8ZM9.6 8.8C9.17565 8.8 8.76869 8.96857 8.46863 9.26863C8.16857 9.56869 8 9.97565 8 10.4C8 10.8243 8.16857 11.2313 8.46863 11.5314C8.76869 11.8314 9.17565 12 9.6 12C10.0243 12 10.4313 11.8314 10.7314 11.5314C11.0314 11.2313 11.2 10.8243 11.2 10.4C11.2 9.97565 11.0314 9.56869 10.7314 9.26863C10.4313 8.96857 10.0243 8.8 9.6 8.8ZM4 9.6C3.78783 9.6 3.58434 9.68429 3.43431 9.83432C3.28429 9.98434 3.2 10.1878 3.2 10.4C3.2 10.6122 3.28429 10.8157 3.43431 10.9657C3.58434 11.1157 3.78783 11.2 4 11.2C4.21217 11.2 4.41566 11.1157 4.56569 10.9657C4.71571 10.8157 4.8 10.6122 4.8 10.4C4.8 10.1878 4.71571 9.98434 4.56569 9.83432C4.41566 9.68429 4.21217 9.6 4 9.6ZM9.6 9.6C9.38783 9.6 9.18434 9.68429 9.03432 9.83432C8.88429 9.98434 8.8 10.1878 8.8 10.4C8.8 10.6122 8.88429 10.8157 9.03432 10.9657C9.18434 11.1157 9.38783 11.2 9.6 11.2C9.81217 11.2 10.0157 11.1157 10.1657 10.9657C10.3157 10.8157 10.4 10.6122 10.4 10.4C10.4 10.1878 10.3157 9.98434 10.1657 9.83432C10.0157 9.68429 9.81217 9.6 9.6 9.6Z" fill="white"/>
                  </svg>
               </span>
            </c:when>
            <c:otherwise>
               <span class="glyphicon glyphicon-shopping-cart"></span>
            </c:otherwise>
         </c:choose>
      </div>
      <c:choose>
      	<c:when test="${cmsSite.uid ne 'puertorico' && cmsSite.uid ne 'jamaica'}">
		      <div class="mini-cart-count js-mini-cart-count">
		         <span class="nav-items-total">
		         	<span class="items-mobile hidden-xs hidden-sm"><spring:theme code="text.minicart.message1" text="My cart(" /></span>
		        	   <span class="updatedcartitem">${totalItems lt 100 ? totalItems : "99+"}</span>        	
		         	<span class="items-mobile hidden-xs hidden-sm"><spring:theme code="text.minicart.message2" text=")" /></span>
		         </span>         
		      </div>
		</c:when>
		<c:otherwise>
			<div class="mini-cart-count js-mini-cart-count">
		         <span class="nav-items-total">
		         	<span class="items-mobile hidden-xs"><spring:theme code="text.minicart.message1" text="My cart(" /></span>
		        	   <span class="updatedcartitem">${totalItems lt 100 ? totalItems : "99+"}</span>        	
		         	<span class="items-mobile hidden-xs"><spring:theme code="text.minicart.message2" text=")" /></span>
		         </span>         
		      </div>
		</c:otherwise>
      </c:choose>

   </a>
</div>
<div class="mini-cart-container js-mini-cart-container"></div>
