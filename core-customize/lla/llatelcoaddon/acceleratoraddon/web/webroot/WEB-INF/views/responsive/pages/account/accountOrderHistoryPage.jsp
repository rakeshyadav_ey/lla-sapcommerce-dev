<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<jsp:useBean id="billingname" class="java.util.LinkedHashMap" />
<jsp:useBean id="tdlist" class="java.util.LinkedHashMap" scope="page" />
<spring:url value="/my-account/order/" var="orderDetailsUrl" htmlEscape="false" />
<spring:url value="/my-account/orders?sort=${ycommerce:encodeUrl(searchPageData.pagination.sort)}" var="orderSearchUrl"
	htmlEscape="false" />
<c:if test="${cmsSite.uid ne 'puertorico' && cmsSite.uid ne 'cabletica'}">
	<div class="account-head">
		<spring:theme code="text.account.orderHistory" text="Checkout Total" />
	</div>
</c:if>
<c:if test="${empty searchPageData.results}">
	<div class="account-section-content content-empty">
		<spring:theme code="text.account.orderHistory.noOrders" text="No Orders Found" />
	</div>
</c:if>
<c:if test="${not empty searchPageData.results}">
	<div class="account-section-content	">
		<div class="account-orderhistory">
			<div class="account-orderhistory-pagination">
				<nav:pagination top="true" msgKey="text.account.orderHistory.page" showCurrentPageInfo="true" hideRefineButton="true"
					supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}"
					searchUrl="${searchUrl}" numberPagesShown="${numberPagesShown}" />
			</div>
			<div class="account-overview-table">
				<table class="orderhistory-list-table responsive-table">
					<tr class="account-orderhistory-table-head responsive-table-head hidden-xs">
						<th><spring:theme code="text.account.orderHistory.orderNumber" text="Order Number" /></th>
						<th><spring:theme code="text.account.orderHistory.orderStatus" text="Order Status" /></th>
						<th><spring:theme code="text.account.orderHistory.datePlaced" text="Date Placed" /></th>
						<c:choose>
						    <c:when test="${cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'cabletica'}">
						        <th><spring:theme code="text.account.orderHistory.monthly.payment.total"/></th>
						    </c:when>
						    <c:otherwise>
						        <th><spring:theme code="text.account.orderHistory.totalamount" text="Total Amount"/></th>
						    </c:otherwise>
						 </c:choose>
						<c:if test="${cmsSite.uid eq 'panama'}">
						 <th><spring:theme code="text.account.orderHistory.totalmonthlyamount" text="Total Monthly Amount"/></th>
						</c:if>
						<c:forEach items="${searchPageData.results}" var="result">
							<c:forEach items="${result.orderHistoryPrices}" var="price">
								<c:set target="${billingname}" property="${(price.billingTime.name)}" value="${(price.billingTime.name)}" />
							</c:forEach>
						</c:forEach>
						<c:forEach items="${billingname}" var="billingEvent">
							<c:set var="key" value="${billingEvent.key}" />
<%-- 							<th>${ycommerce:encodeHTML(key)}</th> --%>
							<c:set target="${tdlist}" property="${key}" value="<td>&mdash;</td>" />
						</c:forEach>
					</tr>
					<c:forEach items="${searchPageData.results}" var="order">
						<tr class="responsive-table-item">
							<td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.orderNumber"
									text="Order Number" /></td>
							<td class="responsive-table-cell"><a href="${orderDetailsUrl}${ycommerce:encodeHTML(order.code)}"
								class="responsive-table-link order-num"> ${ycommerce:encodeHTML(order.code)} </a></td>
							<td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.orderStatus"
									text="Order Status" /></td>
							<td class="status"><spring:theme code="text.account.order.status.display.${order.statusDisplay}" /></td>
							<td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.datePlaced" text="Date Placed" />
							<td class="responsive-table-cell responsive-table-cell-bold date">
							<c:if test ="${cmsSite.uid eq 'panama' || cmsSite.uid eq 'jamaica'}">
							        <fmt:formatDate value="${order.placed}" dateStyle="medium" timeStyle="short" type="both"/>
							</c:if>
							    <c:if test="${cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'cabletica'}">
									<fmt:formatDate value="${order.placed}" timeStyle="short" type="both" pattern="MMM/dd/yy, hh:mm aa"/>
								</c:if>
						     </td>
							</td>
                            <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.total"/></td>
                                    <td class="responsive-table-cell responsive-table-cell-bold">
                                    <c:if test="${cmsSite.uid eq 'puertorico' || cmsSite.uid eq 'cabletica'}">
                                     <c:forEach items="${order.orderHistoryPrices}" var="orderPrice">
                                      			 ${currencySymbol}${fn:escapeXml(orderPrice.totalPrice.value)}
                                     </c:forEach>
                                    </c:if>
                                    </td>
                                 <c:if test="${cmsSite.uid eq 'panama'}">
                                 <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.total"/></td>
                                    <td class="responsive-table-cell responsive-table-cell-bold">
                                         <c:forEach items="${order.orderHistoryPrices}" var="orderPrice">
                                            <c:if test="${orderPrice.monthlyPrice ne '' or orderPrice.monthlyPrice ne null}">
                                             <c:set var="orderMonthlyTotal" value="${orderPrice.monthlyPrice.formattedValue}"/>
                                            </c:if>
                                         </c:forEach>
                                         ${fn:escapeXml(orderMonthlyTotal)}
                                    </td>
                               </c:if>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
		<div class="account-orderhistory-pagination">
			<nav:pagination top="false" msgKey="text.account.orderHistory.page" showCurrentPageInfo="true" hideRefineButton="true"
				supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}"
				searchUrl="${orderSearchUrl}" numberPagesShown="${numberPagesShown}" />
		</div>
	</div>
</c:if>
