<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="multi-checkout-telco" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>
<%@ taglib prefix="telco-structure" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/structure"%>
<%@ taglib prefix="secureFooter" tagdir="/WEB-INF/tags/responsive/common/footer"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
<div class="container">
	<c:if test="${cmsSite.uid eq 'jamaica'}">
        	<multi-checkout-telco:checkoutheader />
    	</c:if>
	<div class="row">
		<div class="col-sm-6">
			<multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
				<jsp:body>
                    <c:if test="${cmsSite.uid eq 'jamaica'}">
                        <div class="comn-vali-msg"><spring:theme code="checkout.mandatory.message" htmlEscape="false" /></div>
                    </c:if>
                    <div class="checkout-shipping">
                      <%--   <multi-checkout-telco:installationItems cartData="${cartData}" showInstallationAddress="false" /> --%>
                         <input type="hidden" id="installation_country" value="${siteId}"/>
                         <input type="hidden" id="installation_country_isocode" value="${countries[0].isocode}" />
                        <div class="checkout-indent">
                          <%--   <div class="headline">
									  <spring:theme code="checkout.summary.installationAddress" text="Installation Address" />
								</div> --%>

                            <address:addressFormSelector supportedCountries="${countries}" regions="${regions}" cancelUrl="${currentStepUrl}"
									country="${country}" />

                             <div id="addressbook">
                                <c:forEach items="${installationAddresses}" var="installationAddress" varStatus="status">
                                    <div class="addressEntry">
                                        <form action="${request.contextPath}/checkout/multi/installation-address/select" method="GET">
                                            <input type="hidden" name="selectedAddressCode" value="${ycommerce:encodeHTML(installationAddress.id)}" />
                                            <ul>
                                                <li>
                                                    <strong>
                                                       
                                                        ${ycommerce:encodeHTML(installationAddress.firstName)}&nbsp;
                                                        ${ycommerce:encodeHTML(installationAddress.lastName)}
                                                   </strong>
                                                   <br />
                                                   ${ycommerce:encodeHTML(installationAddress.line1)}&nbsp;
                                                   ${ycommerce:encodeHTML(installationAddress.line2)}&nbsp;
                                                  
                                                   <br />
                                                   ${ycommerce:encodeHTML(installationAddress.town)}
                                                  
                                                   <br />
                                                   ${ycommerce:encodeHTML(installationAddress.country.name)}&nbsp;
                                                   ${ycommerce:encodeHTML(installationAddress.postalCode)}
                                                </li>
                                            </ul>
                                            <button type="submit" class="btn btn-primary btn-block btn-custom">
                                                <spring:theme code="checkout.multi.installationAddress.useThisAddress" text="Use This Installation Address" />
                                            </button>
                                        </form>
                                    </div>
                                </c:forEach>
                            </div>

                           <address:suggestedAddresses selectedAddressUrl="/checkout/multi/installation-address/select" />
                           <button id="addressSubmit" type="button" class="btn btn-primary btn-block checkout-next">
                                <spring:theme code="checkout.multi.deliveryAddress.continue" text="Next" />
                            </button>
                        </div>

                     <multi-checkout:pickupGroups cartData="${cartData}" />
                    </div>

            	</jsp:body>
			</multi-checkout:checkoutSteps>
		</div>

		<div class="col-sm-6 <c:if test="${cmsSite.uid ne 'jamaica'}">hidden-xs</c:if>">
			<multi-checkout-telco:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="false" />
		</div>

		<!-- <div class="col-sm-12 col-lg-12">
			<cms:pageSlot position="SideContent" var="feature" element="div" class="checkout-help">
				<cms:component component="${feature}" />
			</cms:pageSlot>
		</div> -->
    </div>

    <c:if test="${cmsSite.uid eq 'jamaica'}">
      <multi-checkout-telco:footerSecuresiteLogo />
    </c:if>
</div>
</template:page>
