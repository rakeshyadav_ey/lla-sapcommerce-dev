<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="multi-checkout-telco" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>
<%@ taglib prefix="telco-structure" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/structure"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<c:if test="${cmsSite.uid ne 'puertorico'}">
<spring:htmlEscape defaultHtmlEscape="true" />
</c:if>
<spring:theme code="checkout.multi.paymentMethod.continue" text="Next" var="next" />
<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
     <div class="container">
      <div class="row">
	    <div class="col-sm-6">
			<multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
				<jsp:body>

                        <div class="customer-additional-details">
                           <spring:url value="/checkout/multi/contract-detail/update" var="contractDetailUrl" scope="session" htmlEscape="false"/>
                           <form:form id="contractTypeForm" name="contractTypeForm" modelAttribute="contractTypeForm" action="${contractDetailUrl}" method="POST">
                              <formElement:formRadio idKey="contractType" labelKey="customer.contractType" path="contractType"
                                mandatory="true" skipBlank="false" skipBlankMessageKey="contractType.pleaseSelect" items="${availableContractTypes}" tabindex="1" />
                               <p class="contractMessage"><spring:message code="checkout.multi.puertorico.contractCheck.message" /></p>
                                <c:forEach var="contractType" items="${availableContractTypes}">
                                    <div class="alert alert-info alert-dismissable getAccAlert termsInfo ${contractType.code}" style="display:none">
                                        <c:if test="${contractType.code eq 'WITHCONTRACT'}">
                                       <spring:message code="text.checkout.multi.puertorico.contarctStep3withcontract.message" />
                                        </c:if>
                                        <c:if test="${contractType.code eq 'WITHOUTCONTRACT'}">
                                            <spring:message code="text.checkout.multi.puertorico.contarctStep3withoutcontract.message" />
                                        </c:if>
                                        <div class="readmoreText" style="display:none">
                                           <p class="termsText"><spring:message code="text.checkout.puertorico.${contractType.code}.termsInfo" /></p>
                                           <p class="readLess"><spring:message code="text.checkout.puertorico.readless.text" /></p>
                                        </div>
                                    </div>
                                </c:forEach>
                              <button type="submit" class="btn btn-block btn-primary submit_contractTypeForm checkout-next" title="${next}">${next}</button>
                           </form:form>
                           <%--<div class="global-alerts">
                              <div class="alert alert-info">
                                 <h5><spring:theme code="contract.detail.general.importantnote" text="Important Note"/></h5>
                                 <spring:theme code="contract.detail.general.message" htmlEscape="false"/>
                              </div>
                           </div>--%>
                        </div>

                   </jsp:body>

               </multi-checkout:checkoutSteps>

            </div>
            <div class="col-sm-6 hidden-xs">
            			<multi-checkout-telco:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="true" />
             </div>
          </div>
   </div>
</template:page>
