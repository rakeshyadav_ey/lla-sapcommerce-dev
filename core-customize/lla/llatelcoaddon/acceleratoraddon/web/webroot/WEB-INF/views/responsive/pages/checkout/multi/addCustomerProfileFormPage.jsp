<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="multi-checkout-telco" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>
<%@ taglib prefix="telco-structure" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/structure"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="multi-checkout-telco" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>
<%@ taglib prefix="secureFooter" tagdir="/WEB-INF/tags/responsive/common/footer"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<spring:theme code="checkout.multi.deliveryAddress.continue" text="Next" var="next" />
<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
   <div class="container">
      <c:if test="${cmsSite.uid eq 'cabletica' || cmsSite.uid eq 'panama' || cmsSite.uid eq 'jamaica'}">
         <multi-checkout-telco:checkoutheader />
      </c:if>
      <c:if test="${cmsSite.uid eq 'panama'}">
         <div class="row">
            <div class="col-xs-12">
               <h3 class="mt-0 mb-0 top-title">
                  <spring:theme code="checkout.top.heading.panama"/>
               </h3>
            </div>
         </div>
      </c:if>
	   <div class="row">
	      <div class="col-sm-6">
			   <multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
               <jsp:body>
                  <c:if test="${cmsSite.uid eq 'jamaica'}">
                     <div class="comn-vali-msg"><spring:theme code="checkout.mandatory.message" htmlEscape="false" /></div>
                  </c:if>
                  <div class="customer-additional-details">
                  	<c:if test="${cmsSite.uid eq 'puertorico'}">
		                  <div class="divOverlay ssnValidationLoader">
		                  <div class="gifOverlay"></div>
		                  	<p class="msg"><spring:theme code="checkout.multi.ssnValidation.loaderMsg" /></p>
		                  </div>
	              	</c:if>
<%--                 <c:if test="${cmsSite.uid ne 'puertorico' && cmsSite.uid ne 'panama' && cmsSite.uid ne 'jamaica'}"> 
                        <h4><spring:theme code="additional.profile.details" text="Share your additional personal details" /></h4>
                     </c:if> --%>
                     <spring:url value="/checkout/multi/profile-form/update" var="customerProfileUrl" scope="session" htmlEscape="false"/>
                     <form:form id="customerProfileForm" class="customerProfileForm" name="customerProfileForm" modelAttribute="customerProfileForm" action="${customerProfileUrl}" method="POST">
                        <c:choose>
                           <c:when test="${siteId eq 'jamaica'}">
                               <formElement:formInputBox idKey="jamaica.register.firstName" placeholder="First Name" labelKey="jamaica.register.firstName" path="firstName" inputCSS="form-control" tabindex="1" />
                                <formElement:formInputBox idKey="jamaica.register.lastName" placeholder="Last Name" labelKey="jamaica.register.lastName" path="lastName" inputCSS="form-control" tabindex="2" />
                                <formElement:formInputBox idKey="jamaica.register.trnNumber" placeholder="xxx-xxx-xxx" labelKey="jamaica.register.trnNumber" path="trnNumber" inputCSS="form-control" tabindex="3" mandatory="true" maxlength="11"/>
                                <formElement:formInputBox idKey="jamaica.register.accountNumber" placeholder="00000000" labelKey="jamaica.register.accountNumber" path="accountNumber" inputCSS="form-control" tabindex="4" mandatory="true" />
                                 <formElement:formInputBox  idKey="jamaica.customer.fixedPhone" placeholder="(000) 000 0000" labelKey="jamaica.customer.fixedPhone" path="fixedPhone" inputCSS="form-control" tabindex="5" mandatory="false"/>
                                <div id="customerProfileForm_error">
                                   <input type="hidden" id="mobilePhone_error" value="<spring:theme code="jamaica.customer.mobilePhone.invalid"/>" >
                                   <input type="hidden" id="firstName_error" value="<spring:theme code="jamaica.register.firstName.invalid"/>" >
                                   <input type="hidden" id="lastName_error" value="<spring:theme code="jamaica.register.lastName.invalid"/>" >
                                   <input type="hidden" id="trnNumber_error" value="<spring:theme code="jamaica.register.accountNumber.invalid"/>" >
                                </div>
                           </c:when>
                           <c:when test="${siteId eq 'puertorico'}">
                              <formElement:formInputBox idKey="register.firstName" labelKey="register.firstName" path="firstName" inputCSS="form-control" tabindex="1" mandatory="true" />
			      <formElement:formInputBox idKey="register.secondName" labelKey="register.secondName" path="secondName" inputCSS="form-control" tabindex="2" mandatory="false" />
                              <formElement:formInputBox idKey="register.lastName" labelKey="register.lastName" path="lastName" inputCSS="form-control" tabindex="2" mandatory="true" />
		 	      <formElement:formInputBox idKey="register.secondSurname" labelKey="register.secondSurname" path="secondSurname" inputCSS="form-control" tabindex="2" mandatory="false" />
                              <div id="dobDatePicker">
                                <c:if test="${language == 'en'}">
                                 <formElement:formInputBox placeholder="DD-MM-YYYY" idKey="dob" labelKey="customer.dob" path="dob" inputCSS="form-control"  mandatory="true"  tabindex="3" />
                              </c:if>
                              <c:if test="${language == 'es'}">
                                 <formElement:formInputBox placeholder="DD-MM-AAAA" idKey="dob" labelKey="customer.dob" path="dob" inputCSS="form-control"  mandatory="true"  tabindex="3" />
                              </c:if>
                              </div>
<!-- 			      			  <div class="radio-inline ssn-check from-group"> -->
<%--                                    <p><spring:theme code="text.checkout.ask.for.ssn" /></p> --%>
<!--                                    <label class="custom-label" for="hasSSN"> -->
<%--                                       <spring:theme code="text.checkout.has.ssn" /> --%>
<%--                                       <input type="radio" id="hasSSN" name="SSNcheck" <c:if test="${customerProfileForm.checkSSN == true}">checked</c:if> /> --%>
<!--                                       <span class="checkmark"></span> -->
<!--                                    </label> -->
<!--                                    <label class="custom-label" for="noSSN"> -->
<%--                                       <spring:theme code="text.checkout.no.ssn" /> --%>
<%--                                       <input type="radio" id="noSSN" name="SSNcheck" <c:if test="${customerProfileForm.checkSSN == false}">checked</c:if> /> --%>
<!--                                       <span class="checkmark"></span> -->
<!--                                    </label> -->
<!--                                 </div> -->
<!--                                 <div class="hide"> -->
<%--                                 	<formElement:formInputBox  idKey="customercheckSSN" labelKey="" path="checkSSN" inputCSS="hidden" mandatory="false" /> --%>
<!--                               	</div> -->
<!--                               <div class="ssn-fields from-group"> -->
<%--                               	<formElement:formInputBox  idKey="customer.ssn"  labelKey="customer.lcpr.ssn" path="ssn" inputCSS="form-control" tabindex="4"/> --%>
<!--                                  <p class="hidden" id="ssn-val"></p> -->
<!--                               </div> -->
			      				<formElement:formSelectBoxDefaultEnabled skipBlank="true" skipBlankMessageKey="form.select.none" idKey="customer.documentType"    labelKey="customer.lcpr.documentType"   path="documentType" mandatory="true"
                                                             items="${documentTypes}"  tabindex="5"  selectCSSClass="form-control"/>
                              	<formElement:formInputBox  idKey="customer.documentNumber" labelKey="customer.lcpr.documentNumber" path="documentNumber" inputCSS="form-control" tabindex="6" mandatory="true" />
			      				<formElement:formSelectBoxDefaultEnabled skipBlank="true" skipBlankMessageKey="form.select.none" idKey="customer.driverLicenceState"    labelKey="customer.lcpr.driverLicenceState"   path="driverLicenceState" mandatory="false"
                                                                                                                       items="${driverLicenceStates}"  tabindex="6"  selectCSSClass="form-control"/>
                              <!--<formElement:formInputBox  idKey="customer.fixedPhone" labelKey="customer.lcpr.telephone" path="fixedPhone" inputCSS="form-control" tabindex="6" mandatory="true" />
                              <formElement:formInputBox idKey="customer.additionalEmail" labelKey="customer.additionalEmail" path="additionalEmail" inputCSS="form-control" tabindex="7" mandatory="true" /> -->
                           </c:when>
                           <c:when test="${siteId eq 'panama'}">
                              <formElement:formInputBox idKey="customer.additionalEmail" labelKey="customer.additionalEmail" path="additionalEmail" inputCSS="form-control" tabindex="1" mandatory="true" readonly="true" />
                              <formElement:formInputBox idKey="customer.panama.mobilePhone" labelKey="customer.panama.mobilePhone" path="mobilePhone" inputCSS="form-control" tabindex="2" mandatory="true" maxlength="8"/>
                              <formElement:formInputBox idKey="customer.panama.mobilePhoneSecondary" labelKey="customer.panama.mobilePhoneSecondary" path="mobilePhoneSecondary" inputCSS="form-control" tabindex="2" mandatory="false" maxlength="8"/>
                              <formElement:formInputBox idKey="register.firstName" labelKey="register.firstName" path="firstName" inputCSS="form-control" tabindex="3" mandatory="true" />
                              <formElement:formInputBox idKey="register.lastName" labelKey="register.lastName" path="lastName" inputCSS="form-control" tabindex="4" mandatory="true" />
                              <formElement:formSelectBoxDefaultEnabled documentTypeDefaultSelected="${documentTypes[0].code}" documentDefaultSelected="true" skipBlank="true" skipBlankMessageKey="form.select.none" idKey="customer.panama.documentType"    labelKey="customer.panama.documentType"   path="documentType" mandatory="true"
                                                   items="${documentTypes}"  tabindex="5"  selectCSSClass="form-control docType"/>
                              <div class="validation">
                                 <formElement:formInputBox  idKey="customer.panama.documentNumber" labelKey="customer.panama.documentNumber" path="documentNumber" inputCSS="form-control" tabindex="6" mandatory="true" />
                                 <div class="validation-text">
                                    <span class="tooltiptext"><spring:theme code="checkout.validation.ID.number" htmlEscape="false" /></span>
                                 </div> 
                              </div>
                              <c:if test="${cmsSite.uid eq 'panama'}">
                                 <div class="comn-vali-msg">
                                    <spring:theme code="checkout.mandatory.message" htmlEscape="false" />
                                 </div>
                              </c:if>
							  <div id="customerProfileForm_error">
                                 <input type="hidden" id="additionalEmail_error" value="<spring:theme code="panama.customer.additionalEmail.invalid"/>" >
                                 <input type="hidden" id="mobilePhone_error" value="<spring:theme code="panama.customer.mobilePhone.invalid"/>" >
				 <input type="hidden" id="mobilePhoneSecondary_error" value="<spring:theme code="panama.customer.mobilePhoneSecondary.invalid"/>" >
                                 <input type="hidden" id="firstName_error" value="<spring:theme code="panama.register.firstName.invalid"/>" >
                                 <input type="hidden" id="lastName_error" value="<spring:theme code="panama.register.lastName.invalid"/>" >
                                 <input type="hidden" id="documentNumber_error" value="<spring:theme code="panama.customer.documentNumber.invalid"/>" >
                                 <input type="hidden" id="cedula_invalid" value="<spring:theme code="panama.customer.documentNumber.cedula.invalid"/>" >
                                 <input type="hidden" id="cedula_empty" value="<spring:theme code="panama.customer.documentNumber.cedula.empty"/>" >
                              </div>
                           </c:when>
                           <c:when test="${siteId eq 'cabletica'}">
                               <div class="customer-details">
                                  <div class="comn-vali-msg">
                                     <spring:theme code="checkout.mandatory.message" htmlEscape="false" />
                                  </div>
                                  <br>
                                  <formElement:formInputBox idKey="register.firstName" labelKey="register.firstName" path="firstName" inputCSS="form-control" tabindex="3" mandatory="true" />
                                  <formElement:formInputBox idKey="register.lastName" labelKey="register.lastName" path="lastName" inputCSS="form-control" tabindex="4" mandatory="true" />
                                  <%-- <formElement:formSelectBoxDefaultEnabled documentTypeDefaultSelected="${documentTypes[0].code}" documentDefaultSelected="true" skipBlank="true" skipBlankMessageKey="form.select.none" idKey="customer.panama.documentType"    labelKey="customer.panama.documentType"   path="documentType" mandatory="true"
                                             items="${documentTypes}"  tabindex="5"  selectCSSClass="form-control docType"/>
                                  <formElement:formInputBox  idKey="customer.cabletica.documentNumber" labelKey="customer.cabletica.documentNumber" path="documentNumber" inputCSS="form-control" tabindex="6" mandatory="true" /> --%>

                                  <div style="display: none" id="profile_validation_error">
                                     <input type="hidden" id="firstName_error" value="<spring:theme code="profile.firstName.invalid"/>" >
                                     <input type="hidden" id="lastName_error" value="<spring:theme code="profile.lastName.invalid"/>" >
                                     <%-- <input type="hidden" id="documentNumber_error" value="<spring:theme code="profile.documentType.invalid"/>" >
                                     <input type="hidden" id="documentNumber_maxlen_error" value="<spring:theme code="profile.documentType.maxlength.invalid"/>" >
                                     <input type="hidden" id="addr_maxlen_error" value="<spring:theme code="cabletica.name.invalid.length"/>" > --%>
                                  </div>
                               </div>
                           </c:when>
                        </c:choose>
                        <button type="submit" class="btn btn-block btn-primary checkout-next" title="${next}">${next}</button>
                     </form:form>
                  </div>

               </jsp:body>

            </multi-checkout:checkoutSteps>

         </div>

         <div class="col-sm-6 hide-cntent <c:if test="${cmsSite.uid ne 'cabletica'} && ${cmsSite.uid ne 'jamaica'} && ${cmsSite.uid ne 'puertorico'}">hidden-xs</c:if>">

         
            <multi-checkout-telco:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="true" />

		     <c:if test="${cmsSite.uid eq 'cabletica'}">
                <multi-checkout-telco:cableticaContactUs/>
         </c:if>
         </div>
         <input type="hidden" id="gtmPersonalFormError" value="<spring:theme code="gtm.personalinfo.form.errors.msg"/>" >

      </div>
      
      <c:if test="${cmsSite.uid eq 'jamaica'}">
      <multi-checkout-telco:footerSecuresiteLogo />
      </c:if>
      
   </div>
</template:page>
