<div class="international-plan-rates container">
    <table class="table table-striped table-intenational-rates">
        <thead>
            <tr>
                <th>Destination</th>
                <th >Rate per minute + GCT</th>
            </tr>
            <tr colspan="2">
                <td colspan="2" class="subhead">Local Rates</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>FLOW Home Phone (formerly Columbus</td>
                <td>$1.05</td>
            </tr>
            <tr>
                <td>Communication's Flow)</td>
                <td>$1.05</td>
            </tr>
            <tr>
                <td>FLOW Home Phone (formerly LIME)</td>
                <td>$1.05</td>
            </tr>
            <tr>
                <td>FLOW Home Phone (formerly LIME)</td>
                <td>$1.05</td>
            </tr>
            <tr>
                <td>FLOW Home Phone (NEW)</td>
                <td>$1.05</td>
            </tr>
            <tr>
                <td>FLOW Home Phone (NEW)</td>
                <td>$2.55</td>
            </tr>
            <tr>
                <td>Digicel Fixed Line</td>
                <td>$2.55</td>
            </tr>
            <tr>
                <td>Other Local Operators (OLO) FIxed Line</td>
                <td>$2.55</td>
            </tr>
            <tr>
                <td>Other Local Operators (OLO) FIxed Line</td>
                <td>$2.85</td>
            </tr>
            <tr>
                <td>FLOW (formerly LIME) Mobile</td>
                <td>$2.85</td>
            </tr>
            <tr>
                <td>Digicel Mobile</td>
                <td>$2.85</td>
            </tr>
            <tr>
                <td>Other Local Operators (OLO) Mobile</td>
                <td>$2.85</td>
            </tr>
            <tr>
                <td>Other Local Operators (OLO) Mobile</td>
                <td>$2.85</td>
            </tr>
            <tr colspan="2">
                <td colspan="2" class="subhead">International Rates</td>
            </tr>
            <tr>
                <td>US, Canada, UK (landline)</td>
                <td>$11.00</td>
            </tr>
            <tr>
                <td>Caribbean</td>
                <td>$25/$35</td>
            </tr>
            <tr>
                <td>Caribbean</td>
                <td>$25/$35</td>
            </tr>
        </tbody>
    </table>
    
    <div class="additional-info">
    	<p class="subhead">see rates for additional destinations click here.</p>
    	<ul>
    		<li>Prices do not include General Consumption Tax (GCT) or Special Telephone Call Tax (STCT).</li>
			<li>Calls to Digicel are charged per minute.</li>
			<li>Calls to LIME are charged at a 30 second minimum and per second after.</li>
			<li>Note: Prices are quoted per minute. Peak Times are Monday-Friday 6:00am - 5:59pm. Off peak times are 6:00 pm - 5:59 am Monday -Friday and Weekends. All prices quoted exclude GCT. Dialing other fixed line or mobile networks, the charge on that call is as per the Flow to Fixed or Mobile rate for postpaid customers.</li>
			<li>For a listing of international calling codes, please visit the <a href="" class="calling-link">Country Calling Codes</a>. FLOW accepts no liability for incorrect information posted on this site.</li>
    	</ul>
    </div>

</div>