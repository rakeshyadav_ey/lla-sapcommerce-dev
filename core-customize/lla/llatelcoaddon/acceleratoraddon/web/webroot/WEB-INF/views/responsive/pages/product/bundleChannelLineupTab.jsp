<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<c:if test="${productCategories[0].code == '3Pbundles' && product.code != 'doit' && product.code != 'doitnow'}" var="booleanValue">

	<div class="tabhead">
		<a href="">${fn:escapeXml(title)}</a> <span class="glyphicon"></span>
	</div>
	<div class="tabbody">
		<div class="container-lg">
			<div class="row">
				<div class="col-sm-8 col-lg-8 col-md-offset-2 col-lg-offset-3">
					<div class="tab-container">
						<product:bundleChannelLineupTab product="${product}" />
					</div>
				</div>
			</div>
		</div>
	</div>

</c:if>