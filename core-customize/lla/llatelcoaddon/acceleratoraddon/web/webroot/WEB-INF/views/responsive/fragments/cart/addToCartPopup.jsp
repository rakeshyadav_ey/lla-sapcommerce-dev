<%@ page trimDirectiveWhitespaces="true" contentType="application/json" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/cart" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<spring:url value="/cart" var="cartUrl" htmlEscape="false" />

{"cartData": {
"total": "${cartData.totalPrice.value}",
"planExist":"${cartData.isPlanExist}",
"oldProductName":"${oldProduct.name}",
"newProductCode":"${newProduct.code}",
"newProductName":"${newProduct.name}",
"products": [
<c:forEach items="${cartData.entries}" var="cartEntry" varStatus="status">
	{
		"sku":		"${ycommerce:encodeHTML(cartEntry.product.code)}",
		"name": 	"<c:out value='${ycommerce:encodeHTML(cartEntry.product.name)}' />",
		"qty": 		"${cartEntry.quantity}",
		"price": 	"${cartEntry.basePrice.value}",
		"code": 	"<c:out value='${ycommerce:encodeHTML(cartEntry.product.code)}' />",
		"categories": [
		<c:forEach items="${cartEntry.product.categories}" var="category" varStatus="categoryStatus">
			"<c:out value='${ycommerce:encodeHTML(category.name)}' />"<c:if test="${not categoryStatus.last}">,</c:if>
		</c:forEach>
		]
	}<c:if test="${not status.last}">,</c:if>
</c:forEach>
]
<c:set var="planExist" value="${cartData.isPlanExist}"></c:set>
<c:set var="existingProductName" value="${oldProduct.name}"></c:set>
<c:set var="newProductName" value="${newProduct.name}"></c:set>
<c:set var="newProductCode" value="${newProduct.code}"></c:set>
},

"cartAnalyticsData":{"cartCode" : "${cartCode}","productPostPrice":"${entry.basePrice.value}","productName":"<c:out value='${ycommerce:encodeHTML(product.name)}' />"}
,
"addToCartLayer":"<spring:escapeBody javaScriptEscape="true" htmlEscape="false">
	<spring:htmlEscape defaultHtmlEscape="true">
	<spring:theme code="text.addToCart" var="addToCartText"/>
		<div id="addToCartLayer" class="add-to-cart">
	<c:choose>
	<c:when test="${planExist eq 'false'}">
            <div class="cart_popup_error_msg">
                <spring:theme code="${errorMsg}" />
            </div>
            <c:choose>
                <c:when test="${modifiedCartData ne null}">
                    <c:forEach items="${modifiedCartData}" var="modification">
                        <c:set var="product" value="${modification.entry.product}" />
                        <c:set var="entry" value="${modification.entry}" />
                        <c:set var="quantity" value="${modification.quantityAdded}" />
                        <cart:popupCartItems entry="${entry}" product="${product}" quantity="${quantity}"/>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <cart:popupCartItems entry="${entry}" product="${product}" quantity="${quantity}"/>
                </c:otherwise>
            </c:choose>
                <a href="${cartUrl}" class="btn btn-primary btn-block add-to-cart-button">
	                <spring:theme code="checkout.checkout"  text="Checkout"/>
                </a>
                <a href="" class="btn btn-default btn-block js-mini-cart-close-button ${cmsSite.uid eq 'jamaica' ? 'hide':''}">
                	<spring:theme code="cart.page.continue" text="Continue Shopping"/>
                </a>
	</c:when>
	<c:otherwise>
	    <div id="cartItemRemoval-popup" class="cartItemRemoval-popup">
		<p class="cartItemRemoval-msg">
		    <spring:theme code="panama.product.to.replace"/>&nbsp;<span>${existingProductName}</span> <br>
		    <spring:theme code="panama.product.to.replace.with"/>&nbsp;<span>${newProductName}</span>
		</p>
		<div class="col-xs-12">
		    <div class="col-xs-12 col-sm-6">
			<button class="btn btn-default btn-block cartItemRemoval-popup-close"><spring:theme code="panama.replace.product.cart.cancel"/></button>
		    </div>
		    <div class="col-xs-12 col-sm-6">
				<c:url var="addtocartUrl" value="/addtocart"></c:url>
				<form action="${addtocartUrl}" method="get">
                    <input type="hidden" name="productCodePost" id="productCodePost" value="${newProductCode}">
                    <input type="hidden" name="isPlanExist" id="isPlanExist" value="${planExist}">
                    <button type="submit" class="btn btn-primary btn-block updateCartProduct"
                            data-attr-id="${newProduct.code}"
                            data-attr-price="${newProduct.monthlyPrice.value}"
                            data-attr-name="${newProduct.name}"
                            data-attr-category="${newProduct.categories[0].name}" data-attr-category_2="${newProduct.categories[0].parentCategoryName}">
                            <spring:theme code="panama.replace.product.cart.proceed"/>
                    </button>
                    <c:forEach items="${cartData.entries}" var="entry">
                        <input type="hidden" class="existingCartProduct"
                            <c:if test="${entry.product.code ne null}">  data-attr-id="${entry.product.code}" </c:if>
                            <c:if test="${entry.product.categories[0].code == 'devices' && entry.basePrice.value ne null}"> data-attr-price="${entry.basePrice.value}" </c:if>
                            <c:if test="${entry.product.categories[0].code != 'devices' && entry.product.monthlyPrice.value ne null}">data-attr-price="${entry.product.monthlyPrice.value}" </c:if>
                            <c:if test="${entry.product.name ne null}">  data-attr-name="${entry.product.name}" </c:if>
                            <c:if test="${entry.product.categories[0].name ne null}"> data-attr-category="${entry.product.categories[0].name}" </c:if>
                            <c:if test="${entry.product.categories[0].parentCategoryName ne null}"> data-attr-category_2="${entry.product.categories[0].parentCategoryName}" </c:if>
                        />
                    </c:forEach>
                </form>
		    </div>
		</div>
	    </div>
	</c:otherwise>
	</c:choose>
		</div>
	</spring:htmlEscape>
</spring:escapeBody>"
}
