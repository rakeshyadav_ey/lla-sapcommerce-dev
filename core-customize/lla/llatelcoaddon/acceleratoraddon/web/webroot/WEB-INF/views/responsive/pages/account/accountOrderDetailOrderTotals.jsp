<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="telco-order" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/order" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${cmsSite.uid eq 'jamaica'}">
<telco-order:accountOrderDetailOrderTotals order="${orderData}"/>
</c:if>