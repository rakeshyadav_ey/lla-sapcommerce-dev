<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/nav"%>
<%@ taglib prefix="telcoProduct" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/product"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>

<spring:theme code="text.buynow" var="btn_buynow_val"/>
<span id="buynow_text" style="display:none;" data-localized-text="${btn_buynow_val}"></span>

<spring:theme code="prod.feature.text" var="feature_val"/>
<span id="feature_text" style="display:none;" data-localized-text="${feature_val}"></span>

<nav:pagination top="true" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}"
	searchUrl="${searchPageData.currentQuery.url}" numberPagesShown="${numberPagesShown}" />
<div class="tab-content">
   <div class="tab-pane" id="show_grid">
      <div class="product__listing product__list prod_grid_view" >
		   <c:forEach items="${searchPageData.results}" var="productData" varStatus="status">
			   <telcoProduct:productListerGridItem productData="${productData}" />
		   </c:forEach>
      </div>
   </div>
   <div class="tab-pane" id="show_list">
      <div class="product__listing product__list prod_list_view" >
		   <c:forEach items="${searchPageData.results}" var="productData" varStatus="status">
			   <telcoProduct:productListerListItem productData="${productData}" />
		   </c:forEach>
      </div>
   </div>
</div>
<c:if test="${showDynamicProducts eq 'True'}">
<div class="pager">
    <input type="hidden" id="loadedPages" value="1" />
    <button id="show-more" name="show-more" class="btn show-more btn-white"><spring:theme code="product.load.more" text="loadMore" /></button>
</div>
</c:if>
<nav:pagination top="false" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}"
	searchUrl="${searchPageData.currentQuery.url}" numberPagesShown="${numberPagesShown}" />
