<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="telco-multi-checkout" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>
<%@ taglib prefix="telco-structure" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/structure"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="multi-checkout-telco" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>
<%@ taglib prefix="secureFooter" tagdir="/WEB-INF/tags/responsive/common/footer"%>
<%@ taglib prefix="order-entry" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/entry"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<spring:htmlEscape defaultHtmlEscape="true" />
<spring:url value="/checkout/multi/payment-method/setPaymentType" var="paymentmethodURL" htmlEscape="false" />
<spring:url value="/checkout/multi/payment-method/choose" var="saveCardDetails" htmlEscape="false" />
<spring:theme code="checkout.multi.paymentMethod.continue" text="Next" var="next" />
<spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.useThesePaymentDetails" text="Use these payment details" var="useCardDetails" />
<spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.useSavedCard" text="Use a saved card" var="savedCard" />

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">

<div class="container">
	<c:if test="${cmsSite.uid eq 'jamaica'}">
		<multi-checkout-telco:checkoutheader />
	</c:if>
	<c:if test="${cmsSite.uid eq 'cabletica'}">
		<div class="process-selection-bar">
		    <ul class="selection-state clearfix">
		        <li class="previous col-xs-4"><p class="level"><a href="${request.contextPath}/residential?selectedCategory=${category}"><span class="process-state">1</span><span class="process-info"><spring:theme code="selection.level1.text"/></span></a></p></li>
		        <li class="previous col-xs-4"><p class="level"><a href="${request.contextPath}/cart"><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></a></p></li>
		        <li class="active col-xs-4"><p class="level"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level3.text"/></span></p></li>
		    </ul>
		</div>
		<div class="checkout-header"> 
		    <h1><spring:theme code="text.checkout.multi.headline"/></h1>
		    <p><spring:theme code="text.checkout.multi.fill.details"/></p>
		</div>
	</c:if>
	<div class="row">
		<div class="col-sm-6">
<%-- 			<telco-structure:checkoutHeadline /> --%>
			<multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
				<jsp:body>
					<c:if test="${cmsSite.uid eq 'jamaica' || cmsSite.uid eq 'cabletica'}">
                        <c:if test="${cmsSite.uid eq 'jamaica'}">
                        	<div class="comn-vali-msg"><spring:theme code="checkout.mandatory.message" htmlEscape="false" /></div>
						</c:if>
						<c:if test="${cmsSite.uid eq 'cabletica'}">
						    <input type="hidden" id="c2cTimerBOnCheckout" name="c2cTimerBOnCheckout" value="${c2cTimerBOnCheckout}"/>
                            <input type="hidden" id="c2cTimerCOnCheckout" name="c2cTimerCOnCheckout" value="${c2cTimerCOnCheckout}"/>
							<div class="payment-msg">
								<p class="payment-text"><spring:theme code="checkout.payment.message" htmlEscape="false" text="Ingresa tus datos de pago"/></p>
                                <p class="payment-card-message"><spring:theme code="checkout.payment.acceptedCard.message"/></p>
								<p>
									<span class="price-info"><spring:theme code="text.account.orderHistory.payement.total"/>:</span><br>
						            <fmt:setLocale value="en_US" scope="session" />
						            <b>${currentCurrency.symbol}<fmt:formatNumber value="${cartData.totalPrice.value}" pattern="#,##0" />&nbsp;<spring:theme code="text.cabletica.IVA"/></b><br>
<%-- 						            <span class="price_month"><spring:theme code="text.ctaMonthlyplan"/></span> --%>
					            </p>
							</div>
						</c:if>
						<div id="silentOrderPostForm_error" class="hide">
							<input type="hidden" id="card_cardType_error" value="<spring:theme code="card.type.invalid"/>" >
							<input type="hidden" id="card_nameOnCard_error" value="<spring:theme code="card.name.invalid"/>" >
							<input type="hidden" id="card_accountNumber_error" value="<spring:theme code="card.number.invalid"/>" >
							<input type="hidden" id="card_accountNumber_valid_error" value="<spring:theme code="card.number.invalid.error"/>" >
							<input type="hidden" id="card_expirationMonth_error" value="<spring:theme code="card.expirationMonth.invalid"/>" >
							<input type="hidden" id="card_expirationYear_error" value="<spring:theme code="card.expirationYear.invalid"/>" >
						    <input type="hidden" id="card_expirationMonthYear_error" value="<spring:theme code="card.expirationMonthYear.invalid"/>" >
						    <input type="hidden" id="card_expirationMonthYear_valid_error" value="<spring:theme code="card.expirationMonthYear.invalid.error"/>" >
							<input type="hidden" id="card_cvNumber_error" value="<spring:theme code="card.cvNumber.invalid"/>" >
						</div>
					</c:if>
				<c:choose>
				 <c:when test="${empty allowedPaymentModes || allowedPaymentModes eq null}">
                  <c:if test="${not empty paymentFormUrl}">
                    <div class="checkout-paymentmethod">

                        <div class="checkout-indent">
                        		<form:form id="silentOrderPostForm" name="silentOrderPostForm" class="silentOrderPostForm" modelAttribute="sopPaymentDetailsForm" action="${paymentFormUrl}" method="POST">
									<input type="hidden" name="orderPage_receiptResponseURL" value="${silentOrderPageData.parameters['orderPage_receiptResponseURL']}" />
									<input type="hidden" name="orderPage_declineResponseURL" value="${silentOrderPageData.parameters['orderPage_declineResponseURL']}" />
									<input type="hidden" name="orderPage_cancelResponseURL" value="${silentOrderPageData.parameters['orderPage_cancelResponseURL']}" />
									<c:forEach items="${sopPaymentDetailsForm.signatureParams}" var="entry">
										<input type="hidden" id="${entry.key}" name="${entry.key}" value="${ycommerce:encodeHTML(entry.value)}" />
									</c:forEach>
									<c:forEach items="${sopPaymentDetailsForm.subscriptionSignatureParams}" var="entry">
										<input type="hidden" id="${entry.key}" name="${entry.key}" value="${ycommerce:encodeHTML(entry.value)}" />
									</c:forEach>
									<input type="hidden" value="${ycommerce:encodeHTML(silentOrderPageData.parameters['billTo_email'])}" name="billTo_email" id="billTo_email">

									<c:if test="${not empty paymentInfos}">
										<div class="form-group">
											<button type="submit" class="btn btn-block btn-default js-saved-payments btn-custom" title="${savedCard}">${savedCard}</button>
										</div>
									</c:if>

                                    <c:if test="${cmsSite.uid eq 'jamaica'}">
                                        <div class="form-group">
                                            <input type="hidden" id="card_cardType" name="card_cardType" value="" />
                                        </div>
                                     </c:if>
                                     <c:if test="${cmsSite.uid eq 'cabletica'}">
                                        <div class="form-group">
                                            <input type="hidden" id="card_cardType" name="card_cardType" value="" />
                                        </div>
                                     </c:if>

                                    <c:if test="${cmsSite.uid eq 'jamaica'}">
                                    <div class="checkout-order-summary today-details">
                                        <div class="today-totals-head">
                                            <spring:theme code="text.cart.items.today.total.head" htmlEscape="false" />
                                        </div>

                                        <ul class="today-payment-info">
                                            <c:forEach items="${cartData.entries}" var="entry" varStatus="loop">
                                                <li>
                                                    <div class="details">
                                                        <div class="name">
                                                            <c:choose>
                                                                <c:when test="${entry.product.categories[0].code eq 'addon' || entry.product.categories[0].code eq 'postpaid' || entry.product.categories[0].code eq 'prepaid'}">
                                                                    ${ycommerce:encodeHTML(entry.product.name)}
                                                                </c:when>                       
                                                                <c:otherwise>
                                                                    <spring:theme code="text.cart.items.deposit" />&nbsp; - &nbsp;${ycommerce:encodeHTML(entry.product.name)}
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </div>
                                                    </div>
                                                    <div class="price">
                                                        <c:forEach items="${entry.orderEntryPrices}" var="orderEntryPrice">
                                                            <c:set var="orderEntryMonthlyPrice" value="${orderEntryPrice.monthlyPrice}" />
                                                            <c:if test="${orderEntryPrice.basePrice.value != null}">
                                                                <div class="actualPrice">
                                                                    <order-entry:actualPriceDesktop orderEntryTotalPrice="${orderEntryPrice.totalPrice}"
                                                                        orderEntryBasePrice="${orderEntryPrice.basePrice}" orderEntryMonthlyPrice="${orderEntryMonthlyPrice}"  monthlyTotalPrice="${cartData.monthlyTotalPrice}"/>
                                                                </div>
                                                            </c:if>
                                                        </c:forEach>
                                                    </div>
                                                </li>
                                            </c:forEach>
                                            <li class="today-tax">
                                                <div class="details"><spring:theme code="basket.page.totals.grossTaxes.noArgs" text="Tax"/></div>
                                                <div class="price">
                                                    <c:forEach items="${cartData.orderPrices}" var="orderPrice">
                                                        <format:price priceData="${orderPrice.totalTax}" displayFreeForZero="FALSE" />
                                                    </c:forEach>
                                                </div>
                                            </li>
                                            <li class="price-total">
                                                <div class="details"><spring:theme code="basket.page.total.checkout.today" text="Total" /></div>
                                                <div class="price"><format:price priceData="${cartData.totalPriceWithTax}" /></div>
                                            </li>
                                        </ul>
                                        <input type="hidden" class="productGtmDataPaymentMethod" data-attr-cardtype="${cartData.paymentInfo.cardTypeData.name}" >
                                    </div>
                                    </c:if>

									<c:choose>
                                     	<c:when test="${cmsSite.uid eq 'jamaica'}">
											<telco-structure:formInputBoxWrapper idKey="card_nameOnCard" labelKey="payment.nameOnCard" path="card_nameOnCard" tabIndex="2" mandatory="false" placeholder="Name as it appears on card" />
										</c:when>
										<c:otherwise>
											<telco-structure:formInputBoxWrapper idKey="card_nameOnCard" labelKey="payment.nameOnCard" path="card_nameOnCard" tabIndex="2" mandatory="true"/>
										</c:otherwise>
									</c:choose>
									<telco-structure:formInputBoxWrapper idKey="card_accountNumber" labelKey="payment.cardNumber" path="card_accountNumber" mandatory="true" tabIndex="3"
										autocomplete="off" placeholder="0000 0000 0000 0000" />
									<div id="show-cc-label"></div>
                                    <c:choose>
                                      <c:when test="${cmsSite.uid eq 'jamaica'}">
                                        <fieldset id="startDate">
                                            <label for="" class="control-label">
                                                    <spring:theme code="payment.startDate" text="Start date (Maestro/Solo/Switch only)" />
                                                </label>
                                            <div class="row">
                                                <telco-structure:formSelectBoxWrapper idKey="StartMonth" labelKey="payment.month" path="card_startMonth" mandatory="true"
                                                    skipBlankMessageKey="payment.month" items="${months}" />

                                                <telco-structure:formSelectBoxWrapper idKey="StartYear" labelKey="payment.year" path="card_startYear" mandatory="true"
                                                    skipBlankMessageKey="payment.year" items="${startYears}" tabIndex="7" />
                                            </div>
                                        </fieldset>

                                        <fieldset id="cardDate">
                                            <!-- <label class="control-label">
                                                    <spring:theme code="payment.expiryDate" text="Expiry date*" />
                                            </label> -->
                                            <div class="row">
                                            <telco-structure:formSelectBoxWrapper idKey="ExpiryMonth" labelKey="payment.month" path="card_expirationMonth" mandatory="true"
                                                    skipBlankMessageKey="payment.month.placeholder" items="${months}" tabIndex="6" />

                                            <telco-structure:formSelectBoxWrapper idKey="ExpiryYear" labelKey="payment.year" path="card_expirationYear" mandatory="true"
                                                    skipBlankMessageKey="payment.year.placeholder" items="${expiryYears}" tabIndex="7" />

                                            </div>
                                        </fieldset>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <formElement:formInputBox idKey="card_cvNumber" labelKey="payment.cvn" path="card_cvNumber" inputCSS="form-control" mandatory="true" tabindex="8" placeholder="000" />
                                            </div>
                                        </div>
                                     </c:when>
                                     <c:otherwise>
                                           <div class="col-xs-6 row">
                                               <formElement:formInputBox idKey="card_expiryMonthYear" labelKey="payment.expiryDate" path="card_expiryMonthYear" inputCSS="form-control cardInfo" mandatory="true" tabindex="8" placeholder="MM/YYYY" />
                                                 <input type="hidden" id="card_expirationMonth" name="card_expirationMonth" value="" />
                                                 <input type="hidden" id="card_expirationYear" name="card_expirationYear" value="" />

                                           </div>

                                           <div class="col-xs-6">
                                            <formElement:formInputBox idKey="card_cvNumber" labelKey="payment.cvn" path="card_cvNumber" inputCSS="form-control cardInfo" mandatory="true" tabindex="8" placeholder="000" />
                                           </div>
                                     </c:otherwise>
                                    </c:choose>



									<div class="row">
										<div class="col-xs-6">
											<div id="issueNum">
												<formElement:formInputBox idKey="card_issueNumber" labelKey="payment.issueNumber" path="card_issueNumber" inputCSS="text" mandatory="false"
													tabindex="9" />
											</div>
										</div>
									</div>
									<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                                       <div class="checkbox">
                                         <formElement:formCheckbox idKey="saveCardForUser"
                                                                   labelKey="checkout.summary.paymentdetails.savePaymentDetails"
                                                                   path="saveInAccount" inputCSS="add-address-left-input"
                                                                   labelCSS="add-address-left-label" mandatory="true" />
                                       </div>
                                    </sec:authorize>
                                    <!--cabletica changes-->
                                    <c:if test="${cmsSite.uid eq 'cabletica'}">
                                    <p class="payment-addr-info"><spring:theme code="checkout.payment.address.info" htmlEscape="false" text="Direccion de facturacion"/></p>
                                    <c:if test="${cartData.deliveryItemsQuantity > 0}">
                                                                <div id="useDeliveryAddressData"
                                                                    data-title="${fn:escapeXml(deliveryAddress.title)}"
                                                                    data-province="${fn:escapeXml(deliveryAddress.province)}"
                                                                    data-canton="${fn:escapeXml(deliveryAddress.building)}"
                                                                    data-district="${fn:escapeXml(deliveryAddress.district)}"
                                                                    data-landMark="${fn:escapeXml(deliveryAddress.landMark)}"
                                                                    data-firstname="${fn:escapeXml(deliveryAddress.firstName)}"
                                                                    data-lastname="${fn:escapeXml(deliveryAddress.lastName)}"
                                                                    data-line1="${fn:escapeXml(deliveryAddress.line1)}"
                                                                    data-line2="${fn:escapeXml(deliveryAddress.line2)}"
                                                                    data-town="${fn:escapeXml(deliveryAddress.town)}"
                                                                    data-postalcode="${fn:escapeXml(deliveryAddress.postalCode)}"
                                                                    data-countryisocode="${fn:escapeXml(deliveryAddress.country.isocode)}"
                                                                    data-regionisocode="${fn:escapeXml(deliveryAddress.region.isocodeShort)}"
                                                                    data-address-id="${fn:escapeXml(deliveryAddress.id)}"
                                                                ></div>
                                                                <formElement:formCheckbox
                                                                    path="useDeliveryAddress"
                                                                    idKey="useDeliveryAddress"
                                                                    inputCSS="useDeliveryAddress"
                                                                    labelKey="checkout.multi.sop.useMyDeliveryAddress"
                                                                    tabindex="11"/>
                                                            </c:if>

                                                            <input type="hidden" value="${fn:escapeXml(silentOrderPageData.parameters['billTo_email'])}" class="text" name="billTo_email" id="billTo_email">
                                    <address:billAddressFormSelector supportedCountries="${billingCountries}" regions="${regions}" tabindex="12"/>
                                    </c:if>
									

    						 <c:if test="${cmsSite.uid eq 'jamaica'}">
                                <div class="guaranteed-text">
                                    <spring:theme code="guaranteed.safe.checkout.text" />
                                    <span>  
                                        <img src="${contextPath}/_ui/responsive/theme-jamaica/images/dssl_logo.svg" alt="">
                                        <img src="${contextPath}/_ui/responsive/theme-jamaica/images/atlantic_commerce_logo.svg" alt="">
                                    </span>
                                </div>

                                <div class="place-order-form">
                                  <div class="checkbox">
                                     <label class="custom-label"> 
                                        <spring:theme code="checkout.summary.placeOrder.readTermsAndConditions1" />
                                        <form:checkbox id="Terms1" path="termsCheck1" />
                                        <span class="checkmark"></span>
                                     </label> 
                                     <label class="bold custom-label"> 
                                        <spring:theme code="checkout.summary.placeOrder.readTermsAndConditions2"
                                           arguments="${getTermsAndConditionsUrl}" text="click here"
                                           htmlEscape="false" />
                                        <form:checkbox id="Terms2" path="termsCheck2" />
                                        <span class="checkmark"></span>
                                     </label>
                                  </div>
                                  <input type="hidden" id="termsCheck2_error" value="<spring:theme code="placeorderform.validation.error.message"/>">
                                  <spring:theme code="checkout.summary.placeOrder" text="Place Order" var="placeOrderText" />                             
                                </div>    							
    						 </c:if>
    					   </form:form>	                             
                        </div> 

                        <button type="submit" class="btn btn-block btn-primary submit_silentOrderPostForm checkout-next" title="${next}">${next}</button>
                    </div>
                </c:if>
				<c:if test="${not empty paymentInfos}">
					<div id="savedpayments">
						<div id="savedpaymentstitle">
							<div class="headline">
								<span class="headline-text">
										<spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.useSavedCard" text="Use a saved card" />
									</span>
							</div>
						</div>

						<div id="savedpaymentsbody">
							<c:forEach items="${paymentInfos}" var="paymentInfo" varStatus="status">
								<div class="saved-payment-entry">
									<form action="${saveCardDetails}" method="GET">
										<div class="col-xs-6">
											<input type="hidden" name="selectedPaymentMethodId" value="${ycommerce:encodeHTML(paymentInfo.id)}" />
													<strong>${ycommerce:encodeHTML(paymentInfo.billingAddress.firstName)}&nbsp; ${ycommerce:encodeHTML(paymentInfo.billingAddress.lastName)}</strong>
													<br />
													${ycommerce:encodeHTML(paymentInfo.cardTypeData.name)}<br />
													${ycommerce:encodeHTML(paymentInfo.cardNumber)}<br />
													<spring:theme code="checkout.multi.paymentMethod.paymentDetails.expires"
													arguments="${ycommerce:encodeHTML(paymentInfo.expiryMonth)},${ycommerce:encodeHTML(paymentInfo.expiryYear)}" />
													<br />
													${ycommerce:encodeHTML(paymentInfo.billingAddress.line1)}<br />
													${ycommerce:encodeHTML(paymentInfo.billingAddress.town)}&nbsp; ${ycommerce:encodeHTML(paymentInfo.billingAddress.region.isocodeShort)}<br />
													${ycommerce:encodeHTML(paymentInfo.billingAddress.postalCode)}&nbsp; ${ycommerce:encodeHTML(paymentInfo.billingAddress.country.isocode)}<br />
										</div>
										<div class="col-xs-6">
											<div class="cvvn_input">
										   		<input type="hidden" name="savedCardType" id="cardType" value="${ycommerce:encodeHTML(paymentInfo.cardTypeData.name)}"/>
													<spring:theme code="saved.cvv.input"/> <input type="text" name="saved_card_cvn" class="saved_card_cvn" /> <br/>
													<label id="checkout-card-cvv-error" class="error checkout-card-cvv-error" for="card.cvv"><spring:theme code="checkout.cvv.error.text" text="This is required field"/></label>
	                                    	</div>
                                    	</div>
										<button type="submit" class="btn btn-block btn-primary margin-top-20 saved-card-button" title="${useCardDetails}">${useCardDetails}</button>
									</form>
								</div>
							</c:forEach>
						</div>
					</div>
				</c:if>
			   </c:when>
			   <c:otherwise>
			      <form:form id="silentOrderPostForm" name="silentOrderPostForm" modelAttribute="paymentDetailsForm" action="${paymentmethodURL}" method="POST">
			        <formElement:formRadio idKey="paymentMethodId" labelKey="payment.paymentMethod" path="paymentMethod"
                                       mandatory="true" skipBlank="false" skipBlankMessageKey="payment.paymentMethod.pleaseSelect"
                                       items="${allowedPaymentModes}" tabindex="1" />
                         <c:forEach var="paymentMode" items="${allowedPaymentModes}">
                           <div class="${paymentMode} payment-message"><spring:theme code="payment.mode.textmsg.${paymentMode}"/></div>
                         </c:forEach>
                      <button type="submit" class="btn btn-block btn-primary submit_silentOrderPostForm checkout-next" title="${next}">${next}</button>
			      </form:form>

			    </c:otherwise>
			   </c:choose>

		   </jsp:body>
			</multi-checkout:checkoutSteps>
		</div>

		<div class="col-sm-6 <c:if test="${cmsSite.uid ne 'jamaica'}">hidden-xs</c:if>">
			<telco-multi-checkout:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="true" />
            <c:if test="${cmsSite.uid eq 'cabletica'}">
               <multi-checkout-telco:cableticaContactUs/>
            </c:if>
		</div>

		<!--<div class="col-sm-12 col-lg-12">
			<cms:pageSlot position="SideContent" var="feature" element="div" class="checkout-help">
				<cms:component component="${feature}" />
			</cms:pageSlot>
		</div> -->
	</div>

	<c:if test="${cmsSite.uid eq 'jamaica'}">
		<multi-checkout-telco:footerSecuresiteLogo />
	</c:if>

</div>

</template:page>
