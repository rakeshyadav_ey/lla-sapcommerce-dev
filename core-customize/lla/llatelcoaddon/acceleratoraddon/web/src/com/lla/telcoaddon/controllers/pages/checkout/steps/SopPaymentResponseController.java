/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcoaddon.controllers.pages.checkout.steps;

import com.firstatlanticcommerce.schemas.gateway.Authorize3DSResponse;
import com.firstatlanticcommerce.schemas.gateway.AuthorizeResponse;
import com.firstatlanticcommerce.schemas.gateway.data.CreditCardTransactionResults;
import com.lla.core.service.address.LLAAddressService;
import com.lla.core.util.LLACommonUtil;
import com.lla.core.util.LLAEncryptionUtil;
import com.lla.facades.address.ProvinceData;
import com.lla.facades.checkout.LLACheckoutFacade;
import com.lla.facades.product.data.ProductRegionData;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.telcoaddon.controllers.ControllerConstants;
import com.lla.telcoaddon.forms.SopPaymentDetailsForm;
import com.lla.telcoaddon.forms.validation.LLASopPaymentDetailsValidator;
import com.llapayment.data.LLAPaymentAuthData;
import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorservices.payment.data.PaymentErrorField;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.subscriptionfacades.data.SubscriptionPaymentData;
import de.hybris.platform.subscriptionfacades.exceptions.SubscriptionFacadeException;
import org.apache.commons.lang.StringUtils;
import org.llapayment.client.LLAPaymentClient;
import org.llapayment.payment.LLAPaymentFacade;
import org.llapayment.service.LLAPaymentCaptureService;
import org.llapayment.strategies.impl.LLACreditCardPaymentInfoCreateStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Silent Order Post (SOP) controller.
 */
@Controller
@RequestMapping(value = "/checkout/multi/sop")
public class SopPaymentResponseController extends PaymentMethodCheckoutStepController
{
	private static final Logger LOG = LoggerFactory.getLogger(SopPaymentResponseController.class);

	@Resource(name = "llaSopPaymentDetailsValidator")
	private LLASopPaymentDetailsValidator llaSopPaymentDetailsValidator;

	@Autowired
	private CartService cartService;
	@Autowired
	private LLAAddressService llaAddressService;

	@Autowired
	private LLAPaymentClient llaPaymentClient;

	@Autowired
	private ModelService modelService;

	@Autowired
	private LLAPaymentFacade llaPaymentFacade;

	@Autowired
	private CustomerAccountService customerAccountService;

	@Autowired
	private BaseStoreService baseStoreService;

	@Autowired
	private LLAPaymentCaptureService llaPaymentCaptureService;

	@Autowired
	private LLACheckoutFacade llaCheckoutFacade;

	@Autowired
	private LLACommonUtil llaCommonUtil;
	@Autowired
	private LLACreditCardPaymentInfoCreateStrategy llaCreditCardPaymentInfoCreateStrategy;
	@Autowired
	private LLAEncryptionUtil llaEncryptionUtil;

	@Resource(name = "siteConfigService")
	private SiteConfigService siteConfigService;

	@ModelAttribute("areas")
	public List<ProductRegionData> getAreaList()
	{
		return  llaCommonUtil.isSiteJamaica()?llaCommonUtil.getJamaicaRegions(): Collections.emptyList();
	}



	/**
	 * Handle SOP response.
	 *
	 * @param request
	 * @param sopPaymentDetailsForm
	 * @param bindingResult
	 * @param model
	 * @param redirectAttributes
	 * @return the view for the next step in the checkout process, determined by (
	 *         {@link #enterStep(Model, RedirectAttributes)}
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/response", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doHandleSopResponse(final HttpServletRequest request, @Valid final SopPaymentDetailsForm sopPaymentDetailsForm,
			final BindingResult bindingResult, final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{
		final CartModel cartModel = cartService.getSessionCart();
		if(null != sopPaymentDetailsForm.getCard_cardType() && sopPaymentDetailsForm.getCard_cardType().equalsIgnoreCase("false")){
			GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.addPaymentDetails.error");
			return enterStep(model, redirectAttributes);
		}
		if(null != sopPaymentDetailsForm && null != sopPaymentDetailsForm.getCard_accountNumber()){
			sopPaymentDetailsForm.setCard_accountNumber(sopPaymentDetailsForm.getCard_accountNumber().replaceAll(" ", ""));
		}
		final Map<String, String> resultMap = getRequestParameterMap(request);
		resultMap.put("card_issueNumber", sopPaymentDetailsForm.getCard_accountNumber());
		if(llaCommonUtil.isSiteJamaica()) {
			getLlaSopPaymentDetailsValidator().validate(sopPaymentDetailsForm, bindingResult);
			if (bindingResult.hasErrors()) {
				GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.addPaymentDetails.error");
				return enterStep(model, redirectAttributes);
			}
			updateSopPaymentForm(sopPaymentDetailsForm, null != cartModel.getInstallationAddress() ? cartModel.getInstallationAddress() : cartModel.getDeliveryAddress());
			updateResultMapWithAddressFields(resultMap, null != cartModel.getInstallationAddress() ? cartModel.getInstallationAddress() : cartModel.getDeliveryAddress(), null);
		}
		if (llaCommonUtil.isSiteCabletica()) {
			updateSopPaymentForm(sopPaymentDetailsForm, null != cartModel.getInstallationAddress() ? cartModel.getInstallationAddress() : cartModel.getDeliveryAddress());
			updateResultMapWithAddressFields(resultMap, cartModel.getDeliveryAddress(), null);
			getLlaSopPaymentDetailsValidator().validate(sopPaymentDetailsForm, bindingResult);
			if (bindingResult.hasErrors()) {
				GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.addPaymentDetails.error");
				return enterStep(model, redirectAttributes);
			}
			if (sopPaymentDetailsForm.isUseDeliveryAddress()) {
				updateSopPaymentForm(sopPaymentDetailsForm,  cartModel.getDeliveryAddress());
			}


		}
	/*	AuthorizeResponse tokenizeResponse = new AuthorizeResponse();
		final LLAPaymentAuthData tokenRequest = createTokenRequest(sopPaymentDetailsForm);
		cartModel.setCartDataCVV(sopPaymentDetailsForm.getCard_cvNumber());
		modelService.save(cartModel);
		try
		{
			tokenizeResponse= llaPaymentClient.createTokenRequest(tokenRequest,cartModel);
		}
		catch (final Exception e)
		{
			LOG.error("Exception while tokenization due to :", e);

		}
		resultMap.put("card_accountNumber",maskAccountNumber(sopPaymentDetailsForm.getCard_accountNumber()));
		populateResultMap(tokenizeResponse,resultMap);
		String cvvResult = StringUtils.EMPTY;
		if(null != tokenizeResponse.getAuthorizeResult() && null != tokenizeResponse.getAuthorizeResult().getValue() && null != tokenizeResponse.getAuthorizeResult().getValue().getCreditCardTransactionResults() && null != tokenizeResponse.getAuthorizeResult().getValue().getCreditCardTransactionResults().getValue())
		{
			CreditCardTransactionResults creditCardTransactionResults = tokenizeResponse.getAuthorizeResult().getValue().getCreditCardTransactionResults().getValue();
			if(null != creditCardTransactionResults.getCVV2Result() && null != creditCardTransactionResults.getCVV2Result().getValue())
			{
				cvvResult = creditCardTransactionResults.getCVV2Result().getValue();
			}
		}
		
		final PaymentSubscriptionResultData paymentSubscriptionResultData = llaPaymentFacade.completeSopCreateSubscription(resultMap, null!=sopPaymentDetailsForm.getSaveInAccount()?sopPaymentDetailsForm.getSaveInAccount():Boolean.FALSE,false,cvvResult);
		boolean rejectResultCode = Boolean.TRUE;
		if(null != paymentSubscriptionResultData.getResultCode() && paymentSubscriptionResultData.getResultCode().equals("85") && cvvResult.equals("M"))
		{
			rejectResultCode = Boolean.FALSE;
		}
		if (paymentSubscriptionResultData.isSuccess() && paymentSubscriptionResultData.getStoredCard() != null
				&& StringUtils.isNotBlank(paymentSubscriptionResultData.getStoredCard().getSubscriptionId()))
		{
			CCPaymentInfoData newPaymentSubscription = paymentSubscriptionResultData.getStoredCard();
			final SubscriptionPaymentData result = subscriptionFinalizeTransaction(newPaymentSubscription,tokenizeResponse);

			if (result == null)
			{
				//add error message and finish.
				GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.addPaymentDetails.subscriptionError");
				return enterStep(model, redirectAttributes);

			}

			newPaymentSubscription = updateMerchantPaymentMethod(newPaymentSubscription, result.getParameters());
			if (newPaymentSubscription == null)
			{
				//... then the previous call has set it to null!
				//Add error message and finish.
				GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.addPaymentDetails.subscriptionError");
				return enterStep(model, redirectAttributes);
			}


			if (getUserFacade().getCCPaymentInfos(true).size() <= 1)
			{
				getUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
			}
			getCheckoutFacade().setPaymentDetails(newPaymentSubscription.getId());
		}
		else if ((paymentSubscriptionResultData.getDecision() != null
				&& "error".equalsIgnoreCase(paymentSubscriptionResultData.getDecision()))
				|| (paymentSubscriptionResultData.getErrors() != null && !paymentSubscriptionResultData.getErrors().isEmpty()))
		{
			// Have SOP errors that we can display

			setupAddPaymentPage(model);

			// Build up the SOP form data and render page containing form
			try
			{
				setupSilentOrderPostPage(sopPaymentDetailsForm, model);
			}
			catch (final Exception e)
			{
				LOG.error("Failed to build beginCreateSubscription request", e);
				GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.addPaymentDetails.generalError");
				return enterStep(model, redirectAttributes);
			}

			if (paymentSubscriptionResultData.getErrors() != null && !paymentSubscriptionResultData.getErrors().isEmpty())
			{
				GlobalMessages.addErrorMessage(model, "checkout.error.paymentethod.formentry.invalid");
				// Add in specific errors for invalid fields
				for (final PaymentErrorField paymentErrorField : paymentSubscriptionResultData.getErrors().values())
				{
					if (paymentErrorField.isMissing())
					{
						bindingResult.rejectValue(paymentErrorField.getName(),
								"checkout.error.paymentethod.formentry.sop.missing." + paymentErrorField.getName(),
								"Please enter a value for this field");
					}
					if (paymentErrorField.isInvalid())
					{
						bindingResult.rejectValue(paymentErrorField.getName(),
								"checkout.error.paymentethod.formentry.sop.invalid." + paymentErrorField.getName(),
								"This value is invalid for this field");
					}
				}
			}
			else if (paymentSubscriptionResultData.getDecision() != null
					&& "error".equalsIgnoreCase(paymentSubscriptionResultData.getDecision()))
			{
				LOG.error("Failed to create subscription. Error occurred while contacting external payment services.");
				GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.addPaymentDetails.generalError");
			}

			return ControllerConstants.Views.Pages.MultiStepCheckout.SilentOrderPostPage;
		}
		else if("reject".equalsIgnoreCase(paymentSubscriptionResultData.getDecision()) && rejectResultCode)
		{
			LOG.error("Transaction rejected");
			GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.addPaymentDetails.reject");
			return enterStep(model, redirectAttributes);
		}
		else if(rejectResultCode)
		{
			// SOP ERROR!
			LOG.error("Failed to create subscription.  Please check the log files for more information");
			return REDIRECT_URL_ERROR + "/?decision=" + paymentSubscriptionResultData.getDecision() + "&reasonCode="
					+ paymentSubscriptionResultData.getResultCode();
		}
*/
		getSessionService().setAttribute("encryptedCard",llaEncryptionUtil.doEncryption(sopPaymentDetailsForm.getCard_accountNumber()));
		getSessionService().setAttribute("encryptedCVV",llaEncryptionUtil.doEncryption(sopPaymentDetailsForm.getCard_cvNumber()));
		AddressModel addressModel=llaCommonUtil.isSiteJamaica()?cartModel.getInstallationAddress():llaCommonUtil.isSiteCabletica() && sopPaymentDetailsForm.isUseDeliveryAddress()?cartModel.getDeliveryAddress():updateBillingAddress(sopPaymentDetailsForm);
		PaymentInfoData paymentInfoData=createPaymentInfoData(sopPaymentDetailsForm);
		CreditCardPaymentInfoModel ccPaymentInfo=llaCreditCardPaymentInfoCreateStrategy.llaCreateCreditCardPaymentInfo(paymentInfoData,addressModel,(CustomerModel)cartModel.getUser(),true);
		modelService.save(ccPaymentInfo);
		cartModel.setPaymentInfo(ccPaymentInfo);
		modelService.save(cartModel);
		if(llaCommonUtil.isSiteJamaica()) {
			if (!sopPaymentDetailsForm.isTermsCheck2())
			{
				GlobalMessages.addErrorMessage(model, "checkout.error.terms.not.accepted");
				return enterStep(model, redirectAttributes);
			}
			return placeOrder(model, redirectAttributes, getCartFacade().getSessionCart(), cartModel);
		}
		return getCheckoutStep().nextStep();
	}

	private String placeOrder(Model model, RedirectAttributes redirectAttributes, CartData cartData, CartModel cartModel) throws CMSItemNotFoundException {
		if(cartData.getEntries().size() > 1){
			cartModel.setIsFMCBundle(true);
			modelService.save(cartModel);
			modelService.refresh(cartModel);
		}
		if(cartModel.getTotalPrice() != 0 && siteConfigService.getBoolean("payment.enabled", false))
		{
			Authorize3DSResponse authorizeResponse = llaPaymentClient.createAuthRequest(cartData);
			if(null != authorizeResponse && null!= authorizeResponse.getAuthorize3DSResult() && null!= authorizeResponse.getAuthorize3DSResult().getValue()
					&& null!=authorizeResponse.getAuthorize3DSResult().getValue() && null!=authorizeResponse.getAuthorize3DSResult().getValue().getResponseCode()
					&& authorizeResponse.getAuthorize3DSResult().getValue().getResponseCode().getValue().equals("0"))
			{
				String htmlFormData = authorizeResponse.getAuthorize3DSResult().getValue().getHTMLFormData().getValue();
				model.addAttribute("htmlFormData",htmlFormData);
				return ControllerConstants.Views.Pages.MultiStepCheckout.ThreeDSPostPage;
			}else {
				LOG.error("Invalid response code received from FAC ");
				GlobalMessages.addErrorMessage(model, "checkout.error.authorization.failed");
				return enterStep(model, redirectAttributes);
			}
		}
		return enterStep(model, redirectAttributes);
	}

	private AddressModel updateBillingAddress(@Valid SopPaymentDetailsForm sopPaymentDetailsForm) {
		final AddressModel addressModel=modelService.create(AddressModel.class);
		addressModel.setFirstname(sopPaymentDetailsForm.getBillTo_firstName());
		addressModel.setLastname(sopPaymentDetailsForm.getBillTo_lastName());
		addressModel.setEmail(sopPaymentDetailsForm.getBillTo_email());
		addressModel.setLandMark(sopPaymentDetailsForm.getBillTo_landMark());
		addressModel.setBuilding(sopPaymentDetailsForm.getBillTo_building());
		addressModel.setDistrict(sopPaymentDetailsForm.getBillTo_district());
		addressModel.setNeighbourhood(sopPaymentDetailsForm.getBillTo_neighbourhood());
		addressModel.setPostalcode(sopPaymentDetailsForm.getBillTo_postalCode());
		addressModel.setProvince(sopPaymentDetailsForm.getBillTo_province());
		addressModel.setPhone1(sopPaymentDetailsForm.getBillTo_phoneNumber());
		addressModel.setBillingAddress(Boolean.TRUE);
		addressModel.setOwner(cartService.getSessionCart().getUser());

		modelService.save(addressModel);
		return addressModel;
	}

	private PaymentInfoData createPaymentInfoData(@Valid SopPaymentDetailsForm sopPaymentDetailsForm) {
		PaymentInfoData paymentInfoData=new PaymentInfoData();
		paymentInfoData.setCardAccountHolderName(sopPaymentDetailsForm.getCard_nameOnCard());
		paymentInfoData.setCardAccountNumber(maskAccountNumber(sopPaymentDetailsForm.getCard_accountNumber()));
		paymentInfoData.setCardExpirationMonth(Integer.valueOf(sopPaymentDetailsForm.getCard_expirationMonth()));
		paymentInfoData.setCardExpirationYear(Integer.valueOf(sopPaymentDetailsForm.getCard_expirationYear()));
		paymentInfoData.setCardCardType(sopPaymentDetailsForm.getCard_cardType());
		return paymentInfoData;
	}

	private void updateSopPaymentFormForPersonalDetails(SopPaymentDetailsForm sopPaymentDetailsForm, AddressModel address) {
		sopPaymentDetailsForm.setBillTo_city(address.getTown());
		sopPaymentDetailsForm.setBillTo_country(address.getCountry().getIsocode());
		sopPaymentDetailsForm.setBillTo_firstName(address.getFirstname());
		sopPaymentDetailsForm.setBillTo_lastName(address.getLastname());
		sopPaymentDetailsForm.setBillTo_phoneNumber(address.getPhone1());
		sopPaymentDetailsForm.setBillTo_postalCode(address.getPostalcode());
		sopPaymentDetailsForm.setBillTo_province(llaAddressService.getProvinceByUID(sopPaymentDetailsForm.getBillTo_province()));
		sopPaymentDetailsForm.setBillTo_district(llaAddressService.getDistrictByUID(sopPaymentDetailsForm.getBillTo_district()));
		sopPaymentDetailsForm.setBillTo_building(llaAddressService.getCantonByUID(sopPaymentDetailsForm.getBillTo_building()));
		sopPaymentDetailsForm.setBillTo_neighbourhood(llaAddressService.getNeighbourHoodByUID(sopPaymentDetailsForm.getBillTo_neighbourhood()));
	}

	/**
	 * Creating Token Request from Payment Form
	 * @param sopPaymentDetailsForm
	 * @return
	 */
	private LLAPaymentAuthData createTokenRequest(SopPaymentDetailsForm sopPaymentDetailsForm) {
		final LLAPaymentAuthData tokenRequest = new LLAPaymentAuthData();
		populateTokenRequest(sopPaymentDetailsForm, tokenRequest);
		return tokenRequest;
	}

	private void populateTokenRequest(SopPaymentDetailsForm sopPaymentDetailsForm, LLAPaymentAuthData tokenRequest) {
		tokenRequest.setAmount(sopPaymentDetailsForm.getAmount());
		tokenRequest.setBillTo_city(null != sopPaymentDetailsForm.getBillTo_city() ? sopPaymentDetailsForm.getBillTo_city() : sopPaymentDetailsForm.getBillTo_district());
		tokenRequest.setBillTo_country(sopPaymentDetailsForm.getBillTo_country());
		tokenRequest.setBillTo_customerID(sopPaymentDetailsForm.getBillTo_customerID());
		tokenRequest.setBillTo_email(sopPaymentDetailsForm.getBillTo_email());
		tokenRequest.setBillTo_firstName(sopPaymentDetailsForm.getBillTo_firstName());
		tokenRequest.setBillTo_lastName(sopPaymentDetailsForm.getBillTo_lastName());
		tokenRequest.setBillTo_phoneNumber(sopPaymentDetailsForm.getBillTo_phoneNumber());
		tokenRequest.setBillTo_postalCode(sopPaymentDetailsForm.getBillTo_postalCode());
		tokenRequest.setCard_cardType(sopPaymentDetailsForm.getCard_cardType());
		tokenRequest.setBillTo_street1(null != sopPaymentDetailsForm.getBillTo_street1() ? sopPaymentDetailsForm.getBillTo_street1() :sopPaymentDetailsForm.getBillTo_province()+ " "+sopPaymentDetailsForm.getBillTo_neighbourhood());
		tokenRequest.setBillTo_street2(null != sopPaymentDetailsForm.getBillTo_street2() ? sopPaymentDetailsForm.getBillTo_street2() : sopPaymentDetailsForm.getBillTo_building());
		tokenRequest.setCard_accountNumber(sopPaymentDetailsForm.getCard_accountNumber());
		tokenRequest.setCard_cvNumber(sopPaymentDetailsForm.getCard_cvNumber());
		tokenRequest.setCard_expirationMonth(sopPaymentDetailsForm.getCard_expirationMonth());
		tokenRequest.setCard_expirationYear(sopPaymentDetailsForm.getCard_expirationYear());
		tokenRequest.setShipTo_city(sopPaymentDetailsForm.getShipTo_city());
		tokenRequest.setShipTo_country(sopPaymentDetailsForm.getShipTo_country());
		tokenRequest.setShipTo_firstName(sopPaymentDetailsForm.getShipTo_firstName());
		tokenRequest.setShipTo_lastName(sopPaymentDetailsForm.getShipTo_lastName());
		tokenRequest.setShipTo_phoneNumber(sopPaymentDetailsForm.getShipTo_phoneNumber());
		tokenRequest.setShipTo_postalCode(sopPaymentDetailsForm.getShipTo_postalCode());
		tokenRequest.setShipTo_street1(sopPaymentDetailsForm.getShipTo_street1());
		tokenRequest.setShipTo_street2(sopPaymentDetailsForm.getShipTo_street2());
	}

	private void populateResultMap(AuthorizeResponse tokenizeResponse, Map<String, String> resultMap) {
		if(null != tokenizeResponse && null != tokenizeResponse.getAuthorizeResult() && null != tokenizeResponse.getAuthorizeResult().getValue())
		{
			com.firstatlanticcommerce.schemas.gateway.data.AuthorizeResponse response = tokenizeResponse.getAuthorizeResult().getValue();
			String decision = "decision";
			String responseCode = null;
			CreditCardTransactionResults creditCardResult;
			if(null != response.getCreditCardTransactionResults() && null != response.getCreditCardTransactionResults().getValue()) {
				creditCardResult = response.getCreditCardTransactionResults().getValue();
				if (null != creditCardResult.getResponseCode()
						&& null != creditCardResult.getResponseCode().getValue()) {
					responseCode = creditCardResult.getResponseCode().getValue();
				}
				if(null != creditCardResult.getOriginalResponseCode() && null != creditCardResult.getOriginalResponseCode().getValue())
				{
					resultMap.put("reasonCode", creditCardResult.getOriginalResponseCode().getValue());
				}
				if (null != responseCode) {
					switch (responseCode) {
						case "1":
							resultMap.put(decision, "ACCEPT");
							break;
						case "2":
							resultMap.put(decision, "REJECT");
							break;
						case "3":
							resultMap.put(decision, "ERROR");
							break;
						default:
							throw new IllegalStateException("Unexpected value: " + creditCardResult.getResponseCode().getValue());
					}
				}
				resultMap.put("requestID",creditCardResult.getTokenizedPAN().getValue());
				resultMap.put("paySubscriptionCreateReply_subscriptionID",creditCardResult.getTokenizedPAN().getValue());
				resultMap.put("orderPage_requestToken",creditCardResult.getTokenizedPAN().getValue());
			}
			resultMap.put("merchantID",response.getMerchantId().getValue());
			resultMap.put("paySubscriptionCreateReply_subscriptionIDPublicSignature",response.getSignature().getValue());
		}
	}

	/**
	 * Update the Merchant Payment Method ID on the current paymentInfo.
	 *
	 * @param map
	 *           the parameter map from the SOP response
	 * @param paymentInfo
	 *           the currently active paymentInfo data object.
	 * @return an updated paymentInfo object, or null if the update fails.
	 */
	protected CCPaymentInfoData updateMerchantPaymentMethod(final CCPaymentInfoData paymentInfo, final Map<String, String> map)
	{
		CCPaymentInfoData newPaymentSubscription = null; //default: null indicates error.
		try
		{
			newPaymentSubscription = getSubscriptionFacade().updateCreatedPaymentMethod(paymentInfo, map);
		}
		catch (final SubscriptionFacadeException e)
		{
			LOG.error("Failed to build createPaymentSubscription request", e);
		}
		return newPaymentSubscription;
	}

	/**
	 * Finalize the Subscription Transaction.
	 *
	 * @param newPaymentSubscription
	 * @return the new SubscriptionPaymentData, or null if this method fails.
	 */
	protected SubscriptionPaymentData subscriptionFinalizeTransaction(final CCPaymentInfoData newPaymentSubscription,AuthorizeResponse authorizeResponse)
	{
		SubscriptionPaymentData result = null;
		try
		{
			final String authorizationRequestId = getSessionService().getAttribute("authorizationRequestId");
			final String authorizationRequestToken = getSessionService().getAttribute("authorizationRequestToken");
			final Map<String, String> createPaymentDetailsMap = createPaymentDetailsMap(newPaymentSubscription,authorizeResponse);

			result = getSubscriptionFacade().finalizeTransaction(authorizationRequestId, authorizationRequestToken,
					createPaymentDetailsMap);
		}
		catch (final SubscriptionFacadeException e)
		{
			LOG.error("Failed to build finalizeTransaction request", e);
		}
		return result;
	}

	/**
	 * Populate data for, and display the Billing address form.
	 *
	 * @param countryIsoCode
	 * @param useDeliveryAddress
	 * @param model
	 * @return The billing address form view.
	 */
	@GetMapping(value = "/billingaddressform")
	public String getCountryAddressForm(@RequestParam("countryIsoCode") final String countryIsoCode,
			@RequestParam("useDeliveryAddress") final boolean useDeliveryAddress, final Model model)
	{
		model.addAttribute("supportedCountries", getCountries());
		model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(countryIsoCode));
		model.addAttribute("country", countryIsoCode);
		final SopPaymentDetailsForm sopPaymentDetailsForm = new SopPaymentDetailsForm();
		model.addAttribute("sopPaymentDetailsForm", sopPaymentDetailsForm);
		if (useDeliveryAddress)
		{
			AddressData deliveryAddress = null;
			if(llaCommonUtil.isSiteJamaica()){
				deliveryAddress=	llaCheckoutFacade.getCheckoutCart().getInstallationAddress();
			}else if(llaCommonUtil.isSiteCabletica()){
				deliveryAddress=	llaCheckoutFacade.getCheckoutCart().getDeliveryAddress();
			}

			if (null != deliveryAddress){
				if(deliveryAddress.getRegion() != null && !StringUtils.isEmpty(deliveryAddress.getRegion().getIsocode()))
			    {
				  sopPaymentDetailsForm.setBillTo_state(deliveryAddress.getRegion().getIsocodeShort());
			    }
				sopPaymentDetailsForm.setBillTo_firstName(deliveryAddress.getFirstName());
				sopPaymentDetailsForm.setBillTo_lastName(deliveryAddress.getLastName());
				sopPaymentDetailsForm.setBillTo_street1(deliveryAddress.getLine1());
				sopPaymentDetailsForm.setBillTo_street2(deliveryAddress.getLine2());
				sopPaymentDetailsForm.setArea(deliveryAddress.getArea());
				sopPaymentDetailsForm.setBillTo_city(deliveryAddress.getTown());
				sopPaymentDetailsForm.setBillTo_postalCode(deliveryAddress.getPostalCode());
				sopPaymentDetailsForm.setBillTo_country(deliveryAddress.getCountry().getIsocode());
				sopPaymentDetailsForm.setBillTo_titleCode(deliveryAddress.getTitleCode());
				sopPaymentDetailsForm.setBillTo_phoneNumber(deliveryAddress.getPhone());
				sopPaymentDetailsForm.setBillTo_province(deliveryAddress.getProvince());
				sopPaymentDetailsForm.setBillTo_building(deliveryAddress.getBuilding());
				sopPaymentDetailsForm.setBillTo_district(deliveryAddress.getDistrict());
				sopPaymentDetailsForm.setBillTo_neighbourhood(deliveryAddress.getNeighbourhood());
				sopPaymentDetailsForm.setBillTo_landMark(deliveryAddress.getLandMark());
				if(llaCommonUtil.isSiteCabletica()) {
					model.addAttribute("selectedProvinceList", getList(deliveryAddress.getProvince()));
					model.addAttribute("building", getList(deliveryAddress.getBuilding()));
					model.addAttribute("district", getList(deliveryAddress.getDistrict()));
					model.addAttribute("neighbourhood", getList(deliveryAddress.getNeighbourhood()));
				}
			}
		}
		return ControllerConstants.Views.Fragments.Checkout.BillingAddressForm;
	}

	List<ProvinceData> getList(String str){
		ProvinceData data = new ProvinceData();
		data.setCode(str);
		data.setName(str);
		return Collections.singletonList(data);
	}
	protected Map<String, String> createPaymentDetailsMap(final CCPaymentInfoData payInfo,AuthorizeResponse authorizeResponse)
	{
		final Map<String, String> map = new HashMap<>();

		// Mask the card number
		final String maskedCardNumber = payInfo.getCardNumber();
		map.put("cardNumber", maskedCardNumber);
		map.put("cardType", payInfo.getCardType());
		map.put("expiryMonth", payInfo.getExpiryMonth());
		map.put("expiryYear", payInfo.getExpiryYear());
		map.put("issueNumber", payInfo.getIssueNumber());
		map.put("nameOnCard", payInfo.getAccountHolderName());
		map.put("startMonth", payInfo.getStartMonth());
		map.put("startYear", payInfo.getStartYear());


		final AddressData billingAddress = payInfo.getBillingAddress();
		map.put("billingAddress_countryIso", billingAddress.getCountry().getIsocode());
		map.put("billingAddress_firstName", billingAddress.getFirstName());
		map.put("billingAddress_titleCode", billingAddress.getTitleCode());
		map.put("billingAddress_lastName", billingAddress.getLastName());
		map.put("billingAddress_line1", billingAddress.getLine1());
		map.put("billingAddress_line2", billingAddress.getLine2());
		map.put("billingAddress_postcode", billingAddress.getPostalCode());
		map.put("billingAddress_townCity", billingAddress.getTown());

		return map;
	}

	protected String maskAccountNumber(final String accountNumber)
	{
		String maskedCardNumber = "************";
		if (accountNumber.length() >= 4)
		{
			final String endPortion = accountNumber.trim().substring(accountNumber.length() - 4);
			maskedCardNumber = maskedCardNumber + endPortion;
		}
		return maskedCardNumber;
	}



	@GetMapping(value="/capture-payment")
	@ResponseBody
	public boolean paymentCapture(@RequestParam(value="order",required = true) final String order) throws LLAApiException {
		final OrderModel orderModel=customerAccountService.getOrderForCode(order,baseStoreService.getCurrentBaseStore());
		return llaPaymentCaptureService.capturePayment(orderModel);
	}
	
	private void updateSopPaymentForm(SopPaymentDetailsForm sopPaymentDetailsForm, AddressModel address) {

		sopPaymentDetailsForm.setBillTo_city(address.getTown());
		sopPaymentDetailsForm.setBillTo_country(address.getCountry().getIsocode());
		sopPaymentDetailsForm.setBillTo_firstName(address.getFirstname());
		sopPaymentDetailsForm.setBillTo_lastName(address.getLastname());
		sopPaymentDetailsForm.setBillTo_phoneNumber(address.getPhone1());
		sopPaymentDetailsForm.setBillTo_postalCode(address.getPostalcode());
		sopPaymentDetailsForm.setBillTo_street1(address.getLine1());
		sopPaymentDetailsForm.setBillTo_street2(address.getLine2());
		sopPaymentDetailsForm.setBillTo_province(address.getProvince());
		sopPaymentDetailsForm.setBillTo_district(address.getDistrict());
		sopPaymentDetailsForm.setBillTo_neighbourhood(address.getNeighbourhood());
		sopPaymentDetailsForm.setBillTo_landMark(address.getLandMark());
		sopPaymentDetailsForm.setBillTo_building(address.getBuilding());
	}

	private void updateResultMapWithAddressFields(Map<String, String> resultMap,AddressModel address, final SopPaymentDetailsForm sopPaymentDetailsForm) {

		if (null != address) {
			resultMap.put("billTo_firstName", address.getFirstname());
			resultMap.put("billTo_lastName", address.getLastname());
			resultMap.put("billTo_street1", address.getLine1());
			resultMap.put("billTo_street2", address.getLine2());
			resultMap.put("billTo_city", address.getTown());
			resultMap.put("billTo_postalCode", address.getPostalcode());
			resultMap.put("billTo_country", address.getCountry().getIsocode());
			resultMap.put("billTo_province", address.getProvince());
			resultMap.put("billTo_building", address.getBuilding());
			resultMap.put("billTo_district", address.getDistrict());
			resultMap.put("billTo_neighbourhood", address.getNeighbourhood());
			resultMap.put("billTo_landMark", address.getLandMark());

		}
		else if(null != sopPaymentDetailsForm){
			resultMap.put("billTo_firstName", sopPaymentDetailsForm.getBillTo_firstName());
			resultMap.put("billTo_lastName", sopPaymentDetailsForm.getBillTo_lastName());
			resultMap.put("billTo_street1", sopPaymentDetailsForm.getBillTo_street1());
			resultMap.put("billTo_street2", sopPaymentDetailsForm.getBillTo_street2());
			resultMap.put("billTo_city", sopPaymentDetailsForm.getBillTo_city());
			resultMap.put("billTo_postalCode", sopPaymentDetailsForm.getBillTo_postalCode());
			resultMap.put("billTo_country", sopPaymentDetailsForm.getBillTo_country());
			resultMap.put("billTo_province", sopPaymentDetailsForm.getBillTo_province());
			resultMap.put("billTo_building", sopPaymentDetailsForm.getBillTo_building());
			resultMap.put("billTo_district", sopPaymentDetailsForm.getBillTo_district());
			resultMap.put("billTo_neighbourhood", sopPaymentDetailsForm.getBillTo_neighbourhood());
			resultMap.put("billTo_landMark", sopPaymentDetailsForm.getBillTo_landMark());
		}
	}

	public LLASopPaymentDetailsValidator getLlaSopPaymentDetailsValidator() {
		return llaSopPaymentDetailsValidator;
	}

	public void setLlaSopPaymentDetailsValidator(LLASopPaymentDetailsValidator llaSopPaymentDetailsValidator) {
		this.llaSopPaymentDetailsValidator = llaSopPaymentDetailsValidator;
	}
}
