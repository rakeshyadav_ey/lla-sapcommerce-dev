/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcoaddon.breadcrumb;

import com.lla.facades.product.mapping.LLACategoryRedirectUrlMapping;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder;
import de.hybris.platform.b2ctelcoservices.model.TmaPoVariantModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Resource;
import java.util.*;

/**
 * Breadcrumb builder which handles base product for {@link TmaPoVariantModel}.
 *
 * @since 1810
 */
public class TmaProductOfferingBreadcrumbBuilder extends ProductBreadcrumbBuilder
{
	private static final String LAST_LINK_CLASS = "active";

	@Resource(name="categoryRedirectUrlMapping")
	private LLACategoryRedirectUrlMapping categoryRedirectUrlMapping;
	@Override
	protected ProductModel getBaseProduct(ProductModel product)
	{
		if (product instanceof TmaPoVariantModel)
		{
			return getBaseProduct(((TmaPoVariantModel) product).getTmaBasePo());
		}
		return product;
	}

	/**
	 * Returns a list of breadcrumbs for the given product.
	 *
	 * @param productCode
	 * @return breadcrumbs for the given product
	 */
	public List<Breadcrumb> getBreadcrumbs(final String productCode)
	{
		final ProductModel productModel = getProductService().getProductForCode(productCode);
		final List<Breadcrumb> breadcrumbs = new ArrayList<>();

		final Collection<CategoryModel> categoryModels = new ArrayList<>();
		final Breadcrumb last;

		final ProductModel baseProductModel = getProductAndCategoryHelper().getBaseProduct(productModel);
		last = getProductBreadcrumb(baseProductModel);
		categoryModels.addAll(baseProductModel.getSupercategories());
		last.setLinkClass(LAST_LINK_CLASS);

		breadcrumbs.add(last);

		while (!categoryModels.isEmpty())
		{
			CategoryModel toDisplay = null;
			toDisplay = processCategoryModels(categoryModels, toDisplay);
			categoryModels.clear();
			if (toDisplay != null)
			{
				breadcrumbs.add(getCategoryBreadcrumb(toDisplay));
				break;
			}
		}
		Collections.reverse(breadcrumbs);
		return breadcrumbs;
	}

	protected Breadcrumb getCategoryBreadcrumb(final CategoryModel category)
	{
		Map<String, String> map= getCategoryRedirectUrlMapping().getMapping();
		return new Breadcrumb(map.get(category.getCode()), category.getName(), null, category.getCode());
	}


	public LLACategoryRedirectUrlMapping getCategoryRedirectUrlMapping() {
		return categoryRedirectUrlMapping;
	}

	public void setCategoryRedirectUrlMapping(LLACategoryRedirectUrlMapping categoryRedirectUrlMapping) {
		this.categoryRedirectUrlMapping = categoryRedirectUrlMapping;
	}

}
