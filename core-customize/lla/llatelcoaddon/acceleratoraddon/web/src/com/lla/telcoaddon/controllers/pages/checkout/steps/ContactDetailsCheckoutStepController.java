package com.lla.telcoaddon.controllers.pages.checkout.steps;

import com.lla.core.enums.DocumentType;
import com.lla.core.util.LLACommonUtil;
import com.lla.facades.checkout.LLACheckoutFacade;
import com.lla.facades.customer.LLACustomerFacade;
import com.lla.facades.customer.data.DocumentTypeData;
import com.lla.facades.product.data.GenderData;
import com.lla.telcoaddon.controllers.ControllerConstants;
import com.lla.telcoaddon.controllers.util.CartHelper;
import com.lla.telcoaddon.forms.CustomerProfileForm;
import com.lla.telcoaddon.forms.validation.CustomerProfileFormValidator;
import com.lla.telcoaddon.service.LLAPersonalDetailService;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import org.springframework.validation.Errors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/checkout/multi/contact-details")
public class ContactDetailsCheckoutStepController extends AbstractCheckoutStepController{
    private static final Logger LOG = LoggerFactory.getLogger(ContactDetailsCheckoutStepController.class);
    protected static final String CONTACT_DETAILS = "contact-details";
    protected static final String ATTR_CART_DATA = "cartData";
    private final String[] DISALLOWED_FIELDS = new String[] {};

    @Resource(name = "userFacade")
    private UserFacade userFacade;

    @Resource(name = "customerProfileFormValidator")
    private CustomerProfileFormValidator customerProfileFormValidator;
    @Resource(name="userService")
    private UserService userService;
    @Resource(name = "customerFacade")
    private LLACustomerFacade llaCustomerFacade;
    @Resource(name = "checkoutFacade")
    private LLACheckoutFacade llaCheckoutFacade;
    @Autowired
    private LLACommonUtil llaCommonUtil;

    @Autowired
    private LLAPersonalDetailService llaPersonalDetailService;

    @Autowired
    private ModelService modelService;


    @Override
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    @RequireHardLogIn
    @PreValidateCheckoutStep(checkoutStep = CONTACT_DETAILS)
    public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
    {
        return getContactDetailsForm(model);
    }
    @RequestMapping(value = "/update", method = RequestMethod.GET)
    @RequireHardLogIn
    @PreValidateCheckoutStep(checkoutStep = CONTACT_DETAILS)
    public String enterStepPost(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
    {

        return getContactDetailsForm(model);
   }


    private String getContactDetailsForm(Model model) throws CMSItemNotFoundException {
        prepareDataForPage(model);
        CustomerModel customerModel=(CustomerModel)userService.getCurrentUser();
        model.addAttribute("mobilePhone",customerModel.getMobilePhone());
        final CartData cartData = llaCheckoutFacade.getCheckoutCart();
        cartData.setEntries(CartHelper.removeEmptyEntries(cartData.getEntries()));
        model.addAttribute(ATTR_CART_DATA, cartData);
        populateProfileForm(customerModel, model);
        storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.contactdetails.breadcrumb"));
        model.addAttribute("metaRobots", "noindex,nofollow");
        setCheckoutStepLinksForModel(model, getCheckoutStep());

        return ControllerConstants.Views.Pages.MultiStepCheckout.AddContactDetailsForm;
    }

    private void populateProfileForm(final CustomerModel customerModel,final Model model) {
        CustomerProfileForm customerProfileForm=new CustomerProfileForm();
        prePopulateContactDetails(customerModel,  customerProfileForm);
        model.addAttribute("customerProfileForm",customerProfileForm);
    }


    private void prePopulateContactDetails(CustomerModel customerModel, CustomerProfileForm form) {
        if(CustomerType.GUEST.equals(customerModel.getType())){
            form.setAdditionalEmail(StringUtils.substringAfter(customerModel.getUid(),"|"));
        }else{
            form.setAdditionalEmail(customerModel.getUid());
        }
        form.setMobilePhone(customerModel.getMobilePhone());
    }


    @RequestMapping(value = "/back", method = RequestMethod.GET)
    @RequireHardLogIn
    @Override
    public String back(final RedirectAttributes redirectAttributes)
    {
        return getCheckoutStep().previousStep();
    }

    @RequestMapping(value = "/next", method = RequestMethod.GET)
    @RequireHardLogIn
    @Override
    public String next(final RedirectAttributes redirectAttributes)
    {
        return getCheckoutStep().nextStep();
    }

    @InitBinder
    public void initBinder(final WebDataBinder binder)
    {
        binder.setDisallowedFields(DISALLOWED_FIELDS);
    }


    protected CheckoutStep getCheckoutStep()
    {
        return getCheckoutStep(CONTACT_DETAILS);
    }

    @RequestMapping(value="/update", method=RequestMethod.POST)
    public String addAdditionalCustomerProfileDetails(final CustomerProfileForm form,
                                                      BindingResult bindingResult, final Model model, final HttpServletRequest request,
                                                      final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException {

        CustomerModel customerModel=(CustomerModel)userService.getCurrentUser();
        customerProfileFormValidator.contactDetailsValidate(form,bindingResult);
        if (bindingResult.hasErrors())
        {
            prepareDataForPage(model);
            final CartData cartData = llaCheckoutFacade.getCheckoutCart();
            cartData.setEntries(CartHelper.removeEmptyEntries(cartData.getEntries()));
            model.addAttribute(ATTR_CART_DATA, cartData);
            GlobalMessages.addErrorMessage(model, "profile.error.formentry.invalid");
            storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
            setCheckoutStepLinksForModel(model, getCheckoutStep());
            return ControllerConstants.Views.Pages.MultiStepCheckout.AddContactDetailsForm;
        }
        processNewCustomerAdditionalDetails(customerModel,form);
        return getCheckoutStep().nextStep();
    }

    protected void processNewCustomerAdditionalDetails(final CustomerModel customerModel, final CustomerProfileForm form) {   	 
       llaPersonalDetailService.updateContactDetails(customerModel, form.getAdditionalEmail(),form.getMobilePhone(),null, null);        
    }
}
