package com.lla.telcoaddon.forms;

public class PlaceOrderForm
{
    private String securityCode;
    private boolean termsCheck1;
    private boolean termsCheck2;

    public String getSecurityCode()
    {
        return securityCode;
    }

    public void setSecurityCode(final String securityCode)
    {
        this.securityCode = securityCode;
    }

    public boolean isTermsCheck1() {
        return termsCheck1;
    }

    public void setTermsCheck1(boolean termsCheck1) {
        this.termsCheck1 = termsCheck1;
    }

    public boolean isTermsCheck2() {
        return termsCheck2;
    }

    public void setTermsCheck2(boolean termsCheck2) {
        this.termsCheck2 = termsCheck2;
    }
}


