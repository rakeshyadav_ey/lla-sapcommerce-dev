package com.lla.telcoaddon.breadcrumb;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.SearchBreadcrumbBuilder;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class TmaSearchBreadcrumbBuilder extends SearchBreadcrumbBuilder {

    private static final String LAST_LINK_CLASS = "active";
    @Override
    protected void createBreadcrumbCategoryHierarchyPath(final String categoryCode, final boolean emptyBreadcrumbs,
                                                         final List<Breadcrumb> breadcrumbs)
    {
        final List<Breadcrumb> categoryBreadcrumbs = new ArrayList<>();
        final CategoryModel lastCategoryModel = getCommerceCategoryService().getCategoryForCode(categoryCode);
        categoryBreadcrumbs.add(getCategoryBreadcrumb(lastCategoryModel, !emptyBreadcrumbs ? LAST_LINK_CLASS : ""));
        Collections.reverse(categoryBreadcrumbs);
        breadcrumbs.addAll(categoryBreadcrumbs);
    }
}
