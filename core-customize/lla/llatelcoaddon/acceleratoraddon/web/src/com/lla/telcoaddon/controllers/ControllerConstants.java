/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcoaddon.controllers;

/**
 * Controller constants.
 */
public interface ControllerConstants
{
	String ADDON_PREFIX = "addon:/llatelcoaddon/";

	/**
	 * Class with view name constants.
	 */
	interface Views
	{

		interface Pages
		{

			interface MultiStepCheckout
			{
				String AddEditDeliveryAddressPage = ADDON_PREFIX + "pages/checkout/multi/addEditDeliveryAddressPage";
				String AddEditInstallationAddressPage = ADDON_PREFIX + "pages/checkout/multi/addEditInstallationAddressPage";
				String AddAdditionalProfileDetailsForm=ADDON_PREFIX + "pages/checkout/multi/addCustomerProfileFormPage";
				String AddContactDetailsForm=ADDON_PREFIX + "pages/checkout/multi/addContactDetailsFormPage";
				String AddAccountNumberPage=ADDON_PREFIX + "pages/checkout/multi/addAccountNumberPage";
				String ContractDetailPage=ADDON_PREFIX + "pages/checkout/multi/contractDetailPage";
				String ChooseDeliveryMethodPage = ADDON_PREFIX + "pages/checkout/multi/chooseDeliveryMethodPage";
				String ChoosePickupLocationPage = ADDON_PREFIX + "pages/checkout/multi/choosePickupLocationPage";
				String CheckoutSummaryPage = ADDON_PREFIX + "pages/checkout/multi/checkoutSummaryPage";
				String HostedOrderPageErrorPage = ADDON_PREFIX + "pages/checkout/multi/hostedOrderPageErrorPage";
				String HostedOrderPostPage = ADDON_PREFIX + "pages/checkout/multi/hostedOrderPostPage";
				String SilentOrderPostPage = ADDON_PREFIX + "pages/checkout/multi/silentOrderPostPage";
				String ThreeDSPostPage = ADDON_PREFIX + "pages/checkout/multi/threedsPostPage";
				String CreditCheckPage=ADDON_PREFIX + "pages/checkout/multi/creditCheckPage";
			}
		}

		interface Fragments
		{

			interface Checkout
			{
				String TermsAndConditionsPopup = ADDON_PREFIX + "fragments/checkout/termsAndConditionsPopup";
				String BillingAddressForm = ADDON_PREFIX + "fragments/checkout/billingAddressForm";
			}

		}
	}
}
