/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcoaddon.controllers.pages;

import com.lla.common.addon.exception.LLAInvalidUserException;
import com.lla.core.util.LLACommonUtil;
import com.lla.facades.customer.LLACustomerFacade;
import com.lla.common.addon.constants.LlacommonaddonConstants;
import com.lla.facades.product.data.ProductRegionData;
import com.lla.mulesoft.integration.exception.LLAApiException;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.security.AutoLoginStrategy;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.exceptions.UnauthorizedUserException;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.lla.common.addon.exception.LLAInvalidTokenException;
import com.lla.common.addon.idp.strategy.LLALoginStrategy;

import java.util.Collections;
import java.util.List;

import static com.lla.core.constants.LlaCoreConstants.SESSION_REGION;


/**
 * Controller for home page
 */
@Controller
@RequestMapping("/")
public class LLAHomePageController extends AbstractPageController
{
    private static final Logger LOG = Logger.getLogger(LLAHomePageController.class);
    private static final String LOGOUT = "logout";
    private static final String ACCOUNT_CONFIRMATION_SIGNOUT_TITLE = "account.confirmation.signout.title";
    private static final String ACCOUNT_CONFIRMATION_CLOSE_TITLE = "account.confirmation.close.title";
    private static final String FLOWID_CO_DOMAIN = "lla.flowid.domain";
    private static final String ACCOUNT_CONFIRMATION_SIGNIN_ERROR = "account.confirmation.signin.error";
    private static final String ACCOUNT_CONFIRMATION_SIGNIN_NOT_ALLOWED = "account.confirmation.signin.notallowed";
    private static final String ACCOUNT_CONFIRMATION_NOEMAIL_SIGNIN_ERROR = "account.confirmation.noemail.signin.error";
    private static final String LLA_REFERER_URL = "llaRefererUrl";
    private static final String DISCOVER_FLOW_URL = "lla.jamaica.discoverflow.url";
    private static final String CABLETICA_URL = "lla.cabletica.url";
    private static final String CWPANAMA_URL = "lla.panama.url";
    @Resource(name = "autoLoginStrategy")
    private AutoLoginStrategy autoLoginStrategy;

    @Autowired
    private LLACommonUtil llaCommonUtil;

    @Resource(name = "llaLoginStrategy")
    private LLALoginStrategy llaLoginStrategy;

    @Autowired
    private LLACustomerFacade llaCustomerFacade;

    @Resource(name = "sessionService")
    private SessionService sessionService;

    @Autowired
    private CMSSiteService cmsSiteService;
    
    @Resource(name="userService")
    private UserService userService;
    private HttpSessionRequestCache httpSessionRequestCache;
    @ModelAttribute("authUrl")
    public String getAuthorizationUrl(final HttpServletRequest request)
    {
        return llaLoginStrategy.authUrlConstructor(request, "auth");
    }
    @ModelAttribute("areas")
    public List<ProductRegionData> getAreaList()
    {
        return  llaCommonUtil.isSiteJamaica()?llaCommonUtil.getJamaicaRegions(): Collections.emptyList();
    }

    @Resource(name = "httpSessionRequestCache")
    public void setHttpSessionRequestCache(final HttpSessionRequestCache accHttpSessionRequestCache)
    {
        this.httpSessionRequestCache = accHttpSessionRequestCache;
    }
    @RequestMapping(method = RequestMethod.GET)
    public String home(@RequestParam(value = WebConstants.CLOSE_ACCOUNT, defaultValue = "false") final boolean closeAcc,
                       @RequestParam(value = LOGOUT, defaultValue = "false")
                       final boolean logout, @RequestParam(value = "code", defaultValue = "")
                       final String code, final Model model,
                       final RedirectAttributes redirectModel, final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
    {
        if (StringUtils.isNotEmpty(code))
        {
            String registeredUserName=StringUtils.EMPTY;
            String refererRedirectUrl=StringUtils.EMPTY;
            try
            {
                registeredUserName = llaLoginStrategy.fetchLoginAuthenticationToken(code, request, response);
                refererRedirectUrl=redirectToReferer(registeredUserName,request, request.getHeader("Referer"));
               if (StringUtils.isNotEmpty(registeredUserName))
                {
                    LOG.info(String.format("Trying to auto logged in Customer :: %s ",registeredUserName));
                    autoLoginStrategy.login(registeredUserName, LlacommonaddonConstants.TEMP_CREDENTIALS, request, response);
                    getSessionService().removeAttribute(WebConstants.ANONYMOUS_CHECKOUT);
                    CustomerModel customerModel=(CustomerModel)userService.getUser(registeredUserName);
                    model.addAttribute("currentUser",customerModel.getName());
                }
                else{

                    GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, ACCOUNT_CONFIRMATION_SIGNIN_ERROR);
                    LOG.error(String.format("Received Invalid User email address :::%s from ULM Response",registeredUserName));
                }
                return REDIRECT_PREFIX + refererRedirectUrl;
            }
            catch(LLAInvalidUserException exception){
                LOG.error(String.format("Exception occurred :: User %s belong to other country or group, not allowed to login", exception));
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, ACCOUNT_CONFIRMATION_SIGNIN_NOT_ALLOWED);
                return REDIRECT_PREFIX + redirectToReferer(registeredUserName,request, request.getHeader("Referer"));
            }
            catch (UnknownIdentifierException | IllegalArgumentException | DuplicateUidException  | LLAInvalidTokenException exception)
            {
                LOG.error(String.format("Exception occurred :: %s", exception));
                return REDIRECT_PREFIX + ROOT;
            }
            catch(LLAApiException exception){
                LOG.error(String.format("Exception occurred :: %s", exception));
                LOG.error(String.format("No Email address received in user response from ULM/Flow"));
                final StringBuffer updateProfileUrl=new StringBuffer(getConfigurationService().getConfiguration().getString("flow.domain.url")).append(getConfigurationService().getConfiguration().getString("flow.updateprofile.action.endpoint"));
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, ACCOUNT_CONFIRMATION_NOEMAIL_SIGNIN_ERROR, new Object[]
                        { updateProfileUrl });
                return REDIRECT_PREFIX + redirectToReferer(registeredUserName,request, request.getHeader("Referer"));
            }

        }
        if (logout)
        {
            String message = ACCOUNT_CONFIRMATION_SIGNOUT_TITLE;
            if (closeAcc)
            {
                message = ACCOUNT_CONFIRMATION_CLOSE_TITLE;
            }
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, message);
            return REDIRECT_PREFIX + ROOT;
        }
        if(llaCommonUtil.isSiteJamaica()) {
        	  String redirectURL = getConfigurationService().getConfiguration().getString(DISCOVER_FLOW_URL);
   		  return REDIRECT_PREFIX + redirectURL;
        }else if(llaCommonUtil.isSiteCabletica()){
            String redirectURL = getConfigurationService().getConfiguration().getString(CABLETICA_URL);
            return REDIRECT_PREFIX + redirectURL;
        }else if(llaCommonUtil.isSitePanama()){
            String redirectURL = getConfigurationService().getConfiguration().getString(CWPANAMA_URL);
            return REDIRECT_PREFIX + redirectURL;
        }else if(llaCommonUtil.isSitePuertorico()){
           String redirectURL="/configureProduct";
           return REDIRECT_PREFIX + redirectURL;
        }    
        final ContentPageModel contentPage = getContentPageForLabelOrId(null);
        storeCmsPageInModel(model, contentPage);
        setUpMetaDataForContentPage(model, contentPage);
        updatePageTitle(model, contentPage);
        model.addAttribute("pageType", PageType.HOME.name());
        return getViewForPage(model);
    }

    /**
     *  Redirect to Referer
     * @param request
     * @param referer
     * @param  userName
     * @return
     */
    private String redirectToReferer(final String userName,HttpServletRequest request, String referer) {
        String returnUrl=StringUtils.EMPTY;
        Object refererUrl=request.getSession().getAttribute(LLA_REFERER_URL);
        if(StringUtils.isNotEmpty(userName)&& referer.contains(getConfigurationService().getConfiguration().getString(FLOWID_CO_DOMAIN))){
            returnUrl=null!= refererUrl? refererUrl.toString():ROOT;
        }else if (StringUtils.isEmpty(userName) && referer.contains(getConfigurationService().getConfiguration().getString(FLOWID_CO_DOMAIN)))
        {
            if(null!=refererUrl && refererUrl.toString().contains("/checkout")){
                returnUrl="/cart";
            }else{
                returnUrl = null != refererUrl ? refererUrl.toString() : ROOT;
            }
        }else if("cart".equalsIgnoreCase(referer.substring(referer.lastIndexOf("/")+1, referer.length())) && StringUtils.isNotEmpty(userName)){
            returnUrl = refererUrl.toString();
        }else{
            returnUrl= request.getHeader("Referer");
        }
        request.getSession().setAttribute(LLA_REFERER_URL,null);
        LOG.info(String.format("Redirecting to %s ",returnUrl));
        return returnUrl;
    }

    /** Sets the session region value into the homepage dropdown*/
    @RequestMapping(value = "/setRegion/{regionCode}", method = RequestMethod.GET)
    public void setSessionRegion(@PathVariable final String regionCode){
        if(llaCommonUtil.isSiteJamaica()){
        sessionService.setAttribute(SESSION_REGION, regionCode);
        }
    }
    /** Gets the session region value from the homepage dropdown*/
    @ResponseBody
    @RequestMapping(value = "/getRegion", method = RequestMethod.GET)
    public String getSessionRegion(){
        return llaCommonUtil.isSiteJamaica()?sessionService.getAttribute(SESSION_REGION):StringUtils.EMPTY;
    }

    protected void updatePageTitle(final Model model, final AbstractPageModel cmsPage)
    {
        storeContentPageTitleInModel(model, getPageTitleResolver().resolveHomePageTitle(cmsPage.getTitle()));
    }
}
