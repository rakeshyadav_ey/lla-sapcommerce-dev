/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcoaddon.controllers.pages;

import com.lla.common.addon.idp.strategy.LLALoginStrategy;
import com.lla.core.enums.*;
import com.lla.core.service.order.LLACommerceCheckoutService;
import com.lla.core.util.LLACommonUtil;
import com.lla.facades.customer.LLACustomerFacade;
import com.lla.facades.product.data.ProductRegionData;
import com.lla.mulesoft.integration.service.LLAMulesoftCustomerService;
import com.lla.mulesoft.integration.service.LLAMulesoftDebtCheckApiService;
import com.lla.telcoaddon.controllers.TelcoControllerConstants;
import com.lla.telcoaddon.service.LLAPersonalDetailService;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import com.lla.mulesoft.integration.response.LLAApiResponse;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Checkout Login Controller. Handles login and register for the checkout flow.
 */
@Controller
@RequestMapping(value = "/login/checkout")
public class TelcoCheckoutLoginController extends AbstractLoginPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(TelcoCheckoutLoginController.class);

	private final String[] DISALLOWED_FIELDS = new String[] {};
	protected static final String CALLBACK_URL="/callBack";
	@Resource(name = "llaLoginStrategy")
	private LLALoginStrategy llaLoginStrategy;
	@Resource(name = "acceleratorCheckoutFacade")
	private CheckoutFacade checkoutFacade;
	@Autowired
	private ConfigurationService configurationService;
	@Autowired
	private LLACommonUtil llaCommonUtil;
	@Autowired
	private LLAPersonalDetailService llaPersonalDetailService;
	@Autowired
	private UserService userService;

	@Autowired
	private LLAMulesoftDebtCheckApiService llaMulesoftDebtCheckApiService;

	@Autowired
	private LLAMulesoftCustomerService llaMulesoftCustomerService;
	
	@Autowired
	private LLAMulesoftNotificationService llamulesoftNotificationService;

	@Autowired
	private CartService cartService;
	@Autowired
	private ModelService modelService;
    @Autowired
	private BusinessProcessService businessProcessService;

	@Autowired
	private LLACommerceCheckoutService llaCommerceCheckoutService;

	@Autowired
	private LLACustomerFacade llaCustomerFacade;

	protected CheckoutFacade getCheckoutFacade()
	{
		return checkoutFacade;
	}

	protected String getCheckoutUrl()
	{
		// Default to the multi-step checkout
		return "/checkout/multi";
	}

	/**
	 * Checks if there are any items in the cart.
	 *
	 * @return returns true if items found in cart.
	 */
	protected boolean hasItemsInCart()
	{
		final CartData cartData = getCheckoutFacade().getCheckoutCart();

		return (cartData.getEntries() != null && !cartData.getEntries().isEmpty());
	}

	@Override
	protected String getView()
	{
		return TelcoControllerConstants.Views.Pages.Checkout.CHECKOUT_LOGIN_PAGE;
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		if (hasItemsInCart())
		{
			return getCheckoutUrl();
		}
		//Redirect to the main checkout controller to handle checkout.
		return "/checkout";
	}
	@ModelAttribute("authUrl")
	public String getAuthorizationUrl(final HttpServletRequest request)
	{
		request.getSession().setAttribute("llaRefererUrl",ROOT);
		return llaLoginStrategy.authUrlConstructor(request, "auth");
	}

	@ModelAttribute("areas")
	public List<ProductRegionData> getAreaList()
	{
		return  llaCommonUtil.isSiteJamaica()?llaCommonUtil.getJamaicaRegions(): Collections.emptyList();
	}


	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId("checkout-login");
	}

	@RequestMapping(method = RequestMethod.GET)
	public String doCheckoutLogin(@RequestParam(value = "error", defaultValue = "false") final boolean loginError,
			final HttpSession session, final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{

		if (configurationService.getConfiguration().getBoolean("lla.customer.login.idp.enabled", true)){
			return REDIRECT_PREFIX + ROOT;
		}else{
			return getDefaultLoginPage(loginError, session, model);
		}
	}

	/**
	 * Registration.
	 *
	 * @param form
	 * @param bindingResult
	 * @param model
	 * @param request
	 * @param response
	 * @param redirectModel
	 * @return
	 * @throws CMSItemNotFoundException
	 */

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String doCheckoutRegister(@Valid final RegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		return processRegisterUserRequest(null, form, bindingResult, model, request, response, redirectModel);
	}

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		binder.setDisallowedFields(DISALLOWED_FIELDS);
	}

	@RequestMapping(value = "/guest", method = RequestMethod.POST)
	@ResponseBody
	public String doAnonymousCheckout(@RequestParam("guestEmail") final String guestEmail, @RequestParam("mobilePhone") final String mobilePhone,
			@RequestParam(name="guestName",required=false) final String guestName,
			@RequestParam(name="planType",required=false) final String planType,@RequestParam(name="devicePaymentOption",required=false) final String devicePaymentOption,
			@RequestParam(name="documentType",required=false) final String documentType, @RequestParam(name="documentNumber",required=false) final String documentNumber,
			@RequestParam(name="existingCustomer",required = false) final Boolean existingCustomer,
			final HttpServletRequest request, final HttpServletResponse response)	{
		   try{
		   	LOG.info("guest registration start");
		   	CustomerModel sessionCustomer=(CustomerModel) cartService.getSessionCart().getUser();
		   	CartModel cart =cartService.getSessionCart();
			if(llaCommonUtil.isSitePuertorico())
			{     //existing customer
				if (BooleanUtils.isTrue(existingCustomer))
				{
					llaCustomerFacade.setExistingCustomerDetails(mobilePhone, guestEmail, guestName, existingCustomer);

					if (Objects.nonNull(cart))
					{
						LOG.info(String.format("The cart id %s for existing customer %s.", cartService.getSessionCart().getCode(), existingCustomer));
						LOG.info(String.format("Launching fallout process for cart %s and redirecting to callBack Page as %s at %s", cartService.getSessionCart().getCode(), ClickToCallReasonEnum.EXISTING_CUSTOMER, OriginSource.CART));
						cart.setFalloutReasonValue(FalloutReasonEnum.EXISTING_CUSTOMER);
						modelService.save(cart);
						modelService.refresh(cart);
						llaCommonUtil.launchFalloutProcess(cart, ClickToCallReasonEnum.EXISTING_CUSTOMER, OriginSource.CART, null, StringUtils.EMPTY, StringUtils.EMPTY);
						return CALLBACK_URL;
					}
				}else {
					getCustomerFacade().createGuestUserForAnonymousCheckout(guestEmail, guestName);
				}
			}else {

				getCustomerFacade().createGuestUserForAnonymousCheckout(guestEmail,
						getMessageSource().getMessage("text.guest.customer", null, getI18nService().getCurrentLocale()));
			}
			getGuidCookieStrategy().setCookie(request, response);
			getSessionService().setAttribute(WebConstants.ANONYMOUS_CHECKOUT_GUID, Boolean.TRUE);
		   	CartModel currentCart=cartService.getSessionCart();
   			if(StringUtils.isNotEmpty(mobilePhone)){
      			 CustomerModel currentCustomer=(CustomerModel)userService.getCurrentUser();
   				if (llaCommonUtil.isSiteCabletica()) {
					String builder = evaluateCustomer(guestEmail, mobilePhone, documentType, documentNumber, currentCart, currentCustomer);
					if (builder != null) return builder;
				}
				else {
   					llaPersonalDetailService.updateContactDetails(currentCustomer, guestEmail, mobilePhone, null, null);
   				}				
   			}
			if (llaCommonUtil.isSitePanama()) {
				populatePlanTypeDeviceOption(planType, devicePaymentOption, currentCart);
			}
		}
		catch (final DuplicateUidException e)
		{
			LOG.info("guest registration failed.");
			return "false";
		}
		return "true";
	}

	/**
	 * Evaluate Customer for CURRENT or NEW and DEBT
	 * @param guestEmail
	 * @param mobilePhone
	 * @param documentType
	 * @param documentNumber
	 * @param currentCart
	 * @param currentCustomer
	 * @return
	 */
	private String evaluateCustomer(String guestEmail, String mobilePhone, String documentType, String documentNumber, CartModel currentCart, CustomerModel currentCustomer) {
		final StringBuilder builder = new StringBuilder("false : ");
		llaPersonalDetailService.updateContactDetails(currentCustomer, guestEmail, mobilePhone, documentType, documentNumber);
		final CartModel cart = cartService.getSessionCart();
		if(configurationService.getConfiguration().getBoolean("mulesoft.currentcustomer.api.execute")) {
			LOG.info(String.format("Excecuting Current customer API Call for user ::: %s having Cart Id::: %s ",guestEmail,cart.getCode()));
			try {
				if (!llaMulesoftCustomerService.isNewCustomer(cart)) {

					llaCommonUtil.launchFalloutProcess( cart, ClickToCallReasonEnum.CURRENT_CUSTOMER, OriginSource.CART, null, StringUtils.EMPTY,StringUtils.EMPTY);
					return builder.append(ClickToCallReasonEnum.CURRENT_CUSTOMER.toString()).toString();
				  }
			} catch ( Exception exception) {
				LOG.error("Exception Caught "+exception);
				llaCommonUtil.launchFalloutProcess( cart, ClickToCallReasonEnum.CURRENT_CUSTOMER, OriginSource.CART, null,StringUtils.EMPTY,StringUtils.EMPTY);
				return builder.append(ClickToCallReasonEnum.CURRENT_CUSTOMER.toString()).toString();
			}
		}else{
			LOG.info(String.format("Skipping Current customer API Call for user ::: %s having Cart Id::: %s",guestEmail,cart.getCode()));
		}
		if(configurationService.getConfiguration().getBoolean("mulesoft.debtcheck.api.execute") && null!= currentCustomer.getCustomerStatus() && !currentCustomer.getCustomerStatus().equals(CustomerStatusEnum.NEW) && !currentCustomer.getCustomerStatus().equals(CustomerStatusEnum.PROSPECT)) {
			LOG.info(String.format("Excecuting Customer Debt Check API Call for user ::: %s having Cart Id::: %s ",guestEmail,cart.getCode()));
			try {
				LLAApiResponse llaDebtCheckApiResponse = llaMulesoftDebtCheckApiService.getDebtCheckForCustomer(cartService.getSessionCart(), StringUtils.EMPTY);
				if(Objects.nonNull(llaDebtCheckApiResponse) && Boolean.valueOf(llaDebtCheckApiResponse.isDebtBalanceFlag())){

					llaCommonUtil.launchFalloutProcess(cart, ClickToCallReasonEnum.DEBT, OriginSource.CART, null,StringUtils.EMPTY,StringUtils.EMPTY);
					return builder.append(ClickToCallReasonEnum.DEBT.toString()).toString();
				}
			} catch (Exception exception) {
				LOG.error("IO Exception Caught "+exception);
				llaCommonUtil.launchFalloutProcess( cart, ClickToCallReasonEnum.DEBT, OriginSource.CART, null,StringUtils.EMPTY,StringUtils.EMPTY);
				return builder.append(ClickToCallReasonEnum.DEBT.toString()).toString();
			}
		}else{
			LOG.info(String.format("Skipping Customer Debt Check API Call for user ::: %s having Cart Id::: %s",guestEmail,cart.getCode()));
		}
		return null;
	}
	
	/**	 
        * Populate cart for PlanType and DevicePaymentOption
        * @param planType
        * @param devicePaymentOption
        * @param currentCart 	
        * @return
        */
	private void populatePlanTypeDeviceOption(String planType, String devicePaymentOption, CartModel currentCart){

		if(planType != null && planType.equalsIgnoreCase(PlanTypes.NEW_NUMBER.getCode().toString())){
			currentCart.setPlanTypes(PlanTypes.NEW_NUMBER);
		}else if(planType != null && planType.equalsIgnoreCase(PlanTypes.PORTABILITY.getCode().toString())){
			currentCart.setPlanTypes(PlanTypes.PORTABILITY);
		}else {
			currentCart.setPlanTypes(null);
		}
		if(devicePaymentOption != null && devicePaymentOption.equalsIgnoreCase(DevicePaymentOption.CASH_PAYMENT.getCode().toString())){
			currentCart.setDevicePaymentOption(DevicePaymentOption.CASH_PAYMENT);
		}else if(devicePaymentOption != null && devicePaymentOption.equalsIgnoreCase(DevicePaymentOption.PAYMENT_IN_FEES.getCode().toString())){
			currentCart.setDevicePaymentOption(DevicePaymentOption.PAYMENT_IN_FEES);
		}else{
			currentCart.setDevicePaymentOption(null);
		}
		modelService.save(currentCart);
		modelService.refresh(currentCart);
	}

}