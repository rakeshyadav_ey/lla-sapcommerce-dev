/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcoaddon.controllers.pages.checkout.steps;

import com.lla.core.util.LLACommonUtil;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.lla.facades.checkout.LLACheckoutFacade;
import com.lla.facades.checkout.LLACheckoutFlowFacade;
import com.lla.telcoaddon.controllers.ControllerConstants;
import com.lla.telcoaddon.controllers.util.CartHelper;


/**
 * Delivery address page controller.
 */
@Controller
@RequestMapping(value = "/checkout/multi/installation-address")
public class InstallationAddressCheckoutStepController extends AbstractCheckoutStepController
{
	protected static final String INSTALLATION_ADDRESS = "installation-address";
	protected static final String ATTR_CART_DATA = "cartData";
	protected static final String ATTR_COUNTRY = "country";
	protected static final String ATTR_REGIONS = "regions";
	protected static final String ATTR_INSTALLAION_ADDRESSES = "installationAddresses";
	protected static final String ATTR_NO_ADDRESS = "noAddress";
	protected static final String ATTR_ADDRESS_FORM = "addressForm";
	protected static final String ATTR_SHOW_SAVE_TO_ADDRESS_BOOK = "showSaveToAddressBook";
	protected static final String SITE_ID="siteId";

	private final String[] DISALLOWED_FIELDS = new String[] {};

	@Resource(name="cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "checkoutFacade")
	private LLACheckoutFacade llaCheckoutFacade;

	@Resource(name = "checkoutFlowFacade")
	private LLACheckoutFlowFacade llaCheckoutFlowFacade;

	@Resource(name = "llaCommonUtil")
	private LLACommonUtil llaCommonUtil;

	@Autowired
	private CartService cartService;

	@Autowired
	private UserService userService;

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateCheckoutStep(checkoutStep = INSTALLATION_ADDRESS)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		llaCheckoutFacade.setInstallationAddressIfAvailable();
		if(llaCommonUtil.isSiteJamaica()){
			CartData cartData=getCartFacade().getSessionCart();
			if(cartData.getEntries().size() >= 2 && llaCommonUtil.cartContainsSameProduct(cartData)){
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, "cart.entries.product.quantity.error", null);
				return REDIRECT_PREFIX + "/cart";
			}
			if(cartData.getEntries().size() >= 2 && !llaCommonUtil.cartContainsEntiresOfDistinctCategory(cartData)){
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, "cart.entries.quantity.error", null);
				return REDIRECT_PREFIX + "/cart";
			}

		}
		final CartData cartData = llaCheckoutFacade.getCheckoutCart();
		cartData.setEntries(CartHelper.removeEmptyEntries(cartData.getEntries()));
		model.addAttribute(ATTR_CART_DATA, cartData);
		model.addAttribute(ATTR_COUNTRY, getSiteConfigService().getProperty("website.code"));
		model.addAttribute(ATTR_INSTALLAION_ADDRESSES, getInstallationAddresses(cartData.getInstallationAddress()));
		model.addAttribute(ATTR_NO_ADDRESS, Boolean.valueOf(llaCheckoutFlowFacade.hasNoInstallationAddress()));
		model.addAttribute(ATTR_ADDRESS_FORM, new AddressForm());
		model.addAttribute(ATTR_SHOW_SAVE_TO_ADDRESS_BOOK, Boolean.TRUE);

		prepareDataForPage(model);
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		/*model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.installationAddress.breadcrumb"));*/
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditInstallationAddressPage;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
					  final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		addressForm.setCountryIso(getSiteConfigService().getProperty("website.code"));
		getAddressValidator().validate(addressForm, bindingResult);

		final CartData cartData = llaCheckoutFacade.getCheckoutCart();
		model.addAttribute(ATTR_CART_DATA, cartData);
		model.addAttribute(ATTR_INSTALLAION_ADDRESSES, getInstallationAddresses(cartData.getInstallationAddress()));
		this.prepareDataForPage(model);
		storeAddresses(addressForm, model);
		
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
			storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
			setCheckoutStepLinksForModel(model, getCheckoutStep());
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditInstallationAddressPage;
		}
		final AddressData newAddress = constructNewAddress(addressForm);
		adjustAddressVisibility(addressForm, newAddress);
		getUserFacade().addAddress(newAddress);
		final AddressData previousSelectedAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
		// Set the new address as the selected checkout delivery address
		//getCheckoutFacade().setDeliveryAddress(newAddress);
		llaCheckoutFacade.setInstallationAddress(newAddress);
		if (previousSelectedAddress != null && !previousSelectedAddress.isVisibleInAddressBook())
		{ // temporary address should be removed
			getUserFacade().removeAddress(previousSelectedAddress);
		}

		return getCheckoutStep().nextStep();
	}

	protected void adjustAddressVisibility(final AddressForm addressForm, final AddressData newAddress)
	{
		if (addressForm.getSaveInAddressBook() != null)
		{
			newAddress.setVisibleInAddressBook(addressForm.getSaveInAddressBook().booleanValue());
			newAddress.setShippingAddress(addressForm.getSaveInAddressBook().booleanValue());
			if (addressForm.getSaveInAddressBook().booleanValue() && getUserFacade().isAddressBookEmpty())
			{
				newAddress.setDefaultAddress(true);
			}
		}
		else if (getCheckoutCustomerStrategy().isAnonymousCheckout())
		{
			newAddress.setDefaultAddress(true);
			newAddress.setVisibleInAddressBook(true);
		}
	}

	protected AddressData constructNewAddress(final AddressForm addressForm)
	{
		if(llaCommonUtil.isSiteJamaica()) {
			CustomerModel customerModel=(CustomerModel)userService.getCurrentUser();
			addressForm.setFirstName(customerModel.getFirstName());
			addressForm.setLastName(customerModel.getLastName());
		}
		final AddressData newAddress = new AddressData();
		newAddress.setTitleCode(addressForm.getTitleCode());
		newAddress.setFirstName(addressForm.getFirstName());
		newAddress.setLastName(addressForm.getLastName());
		newAddress.setLine1(addressForm.getLine1());
		newAddress.setLine2(addressForm.getLine2());
		newAddress.setArea(addressForm.getArea());
		newAddress.setTown(addressForm.getTownCity());
		newAddress.setPostalCode(addressForm.getPostcode());
		newAddress.setBillingAddress(false);
		newAddress.setShippingAddress(false);
		newAddress.setInstallationAddress(true);
		newAddress.setPhone(addressForm.getPhone());
		newAddress.setCellphone(addressForm.getPhone2());
		if (addressForm.getCountryIso() != null)
		{
			final CountryData countryData = getI18NFacade().getCountryForIsocode(addressForm.getCountryIso());
			newAddress.setCountry(countryData);
		}
		if (addressForm.getRegionIso() != null && !StringUtils.isEmpty(addressForm.getRegionIso()))
		{
			final RegionData regionData = getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso());
			newAddress.setRegion(regionData);
		}
			/*if(llaCommonUtil.isSiteJamaica()){

			CustomerProfileForm form = new CustomerProfileForm();
			 form.setLastName(addressForm.getLastName());
			 form.setDob(addressForm.getDob());
			 form.setTrnNumber(addressForm.getTrnNumber());
			 form.setFirstName(addressForm.getFirstName());
			 form.setFixedPhone(addressForm.getPhone2());
			 CustomerModel customerModel=(CustomerModel)userService.getCurrentUser();
			 newAddress.setEmail(customerModel.getType().equals(CustomerType.GUEST)?StringUtils.substringAfter(customerModel.getUid(),"|"):customerModel.getUid());
			 llaPersonalDetailService.updatePersonalDetails(customerModel,form);
		  }*/
		return newAddress;
	}

	protected void storeAddresses(final AddressForm addressForm, final Model model)
	{
		if (StringUtils.isNotBlank(addressForm.getCountryIso()))
		{
			model.addAttribute(ATTR_REGIONS, getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
			model.addAttribute(ATTR_COUNTRY, addressForm.getCountryIso());
		}

		model.addAttribute(ATTR_NO_ADDRESS, Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
		model.addAttribute(ATTR_SHOW_SAVE_TO_ADDRESS_BOOK, Boolean.TRUE);
	}

	protected List<? extends AddressData> getInstallationAddresses(final AddressData selectedAddressData) // NOSONAR
	{
		List<AddressData> installationAddresses = null;
		if (selectedAddressData != null)
		{
			installationAddresses = llaCheckoutFacade.getSupportedInstallationAddresses(true);

			if (installationAddresses == null || installationAddresses.isEmpty())
			{
				installationAddresses = Collections.singletonList(selectedAddressData);
			}
			else if (!isAddressOnList(installationAddresses, selectedAddressData))
			{
				installationAddresses.add(selectedAddressData);
			}
		}

		return installationAddresses == null ? Collections.<AddressData> emptyList() : installationAddresses;
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		binder.setDisallowedFields(DISALLOWED_FIELDS);
	}


	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(INSTALLATION_ADDRESS);
	}
	/**
	 * Select address from list.
	 *
	 * @param addressForm
	 * @param redirectModel
	 * @return
	 */

	@RequestMapping(value = "/select", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doSelectSuggestedAddress(final AddressForm addressForm, final RedirectAttributes redirectModel)
	{
		final Set<String> resolveCountryRegions = org.springframework.util.StringUtils
				.commaDelimitedListToSet(Config.getParameter("resolve.country.regions"));

		final AddressData selectedAddress = new AddressData();
		selectedAddress.setId(addressForm.getAddressId());
		selectedAddress.setTitleCode(addressForm.getTitleCode());
		selectedAddress.setFirstName(addressForm.getFirstName());
		selectedAddress.setLastName(addressForm.getLastName());
		selectedAddress.setLine1(addressForm.getLine1());
		selectedAddress.setLine2(addressForm.getLine2());
		selectedAddress.setArea(addressForm.getArea());
		selectedAddress.setTown(addressForm.getTownCity());
		selectedAddress.setPostalCode(addressForm.getPostcode());
		selectedAddress.setBillingAddress(false);
		selectedAddress.setShippingAddress(true);
		final CountryData countryData = getI18NFacade().getCountryForIsocode(addressForm.getCountryIso());
		selectedAddress.setCountry(countryData);

		if (resolveCountryRegions.contains(countryData.getIsocode()) && addressForm.getRegionIso() != null
				&& !StringUtils.isEmpty(addressForm.getRegionIso()))
		{
			final RegionData regionData = getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso());
			selectedAddress.setRegion(regionData);
		}

		if (addressForm.getSaveInAddressBook() != null)
		{
			selectedAddress.setVisibleInAddressBook(addressForm.getSaveInAddressBook().booleanValue());
		}

		if (Boolean.TRUE.equals(addressForm.getEditAddress()))
		{
			getUserFacade().editAddress(selectedAddress);
		}
		else
		{
			getUserFacade().addAddress(selectedAddress);
		}

		final AddressData previousSelectedAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
		// Set the new address as the selected checkout delivery address
		//getCheckoutFacade().setDeliveryAddress(selectedAddress);
		llaCheckoutFacade.setInstallationAddress(selectedAddress);
		if (previousSelectedAddress != null && !previousSelectedAddress.isVisibleInAddressBook())
		{ // temporary address should be removed
			getUserFacade().removeAddress(previousSelectedAddress);
		}

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "checkout.multi.address.added");

		return getCheckoutStep().nextStep();
	}


	/**
	 * This method gets called when the "Use this Address" button is clicked. It sets the selected delivery address on
	 * the checkout facade - if it has changed, and reloads the page highlighting the selected delivery address.
	 *
	 * @param selectedAddressCode
	 *           - the id of the delivery address.
	 *
	 * @return - a URL to the page to load.
	 */
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	@RequireHardLogIn
	public String doSelectInstallationAddress(@RequestParam("selectedAddressCode") final String selectedAddressCode,
										  final RedirectAttributes redirectAttributes)
	{
		final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
		if (getCheckoutStep().checkIfValidationErrors(validationResults))
		{
			return getCheckoutStep().onValidation(validationResults);
		}
		if (StringUtils.isNotBlank(selectedAddressCode))
		{
			cleanupTempAddress(selectedAddressCode);
		}
		return getCheckoutStep().nextStep();
	}

	protected void cleanupTempAddress(final String selectedAddressCode)
	{
		final AddressData selectedAddressData = llaCheckoutFacade.getInstallationAddressForCode(selectedAddressCode);
		if (selectedAddressData != null)
		{
			final AddressData cartCheckoutInstallationAddress = getCheckoutFacade().getCheckoutCart().getInstallationAddress();
			if (isAddressIdChanged(cartCheckoutInstallationAddress, selectedAddressData))
			{
				llaCheckoutFacade.setInstallationAddress(selectedAddressData);
				if (cartCheckoutInstallationAddress != null && !cartCheckoutInstallationAddress.isVisibleInAddressBook())
				{ // temporary address should be removed
					getUserFacade().removeAddress(cartCheckoutInstallationAddress);
				}
			}
		}
	}


}
