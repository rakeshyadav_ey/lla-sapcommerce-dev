package com.lla.telcoaddon.forms;

/**
 * @author LU836YG
 *
 */
public class CustomerSSNForm
{
	private String ssn;

	/**
	 * @return the ssn
	 */
	public String getSsn()
	{
		return ssn;
	}

	/**
	 * @param ssn
	 *           the ssn to set
	 */
	public void setSsn(final String ssn)
	{
		this.ssn = ssn;
	}



}
