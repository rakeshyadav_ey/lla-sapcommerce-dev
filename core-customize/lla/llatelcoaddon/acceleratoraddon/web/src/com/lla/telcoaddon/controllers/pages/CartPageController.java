/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcoaddon.controllers.pages;

import com.lla.common.addon.idp.strategy.LLALoginStrategy;
import com.lla.core.constants.LlaCoreConstants;
import com.lla.core.util.LLACommonUtil;
import com.lla.facades.cart.LLACartFacade;
import com.lla.facades.constants.LlaFacadesConstants;
import com.lla.facades.product.LLATmaProductFacade;
import com.lla.facades.product.data.ProductRegionData;
import com.lla.telcoaddon.controllers.TelcoControllerConstants;
import com.lla.telcoaddon.forms.DeleteBundleForm;
import de.hybris.platform.acceleratorfacades.flow.impl.SessionOverrideCheckoutFlowFacade;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.enums.CheckoutFlowEnum;
import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCartPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateQuantityForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.EntryGroupData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.jalo.JaloObjectNoLongerValidException;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.media.MediaService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.category.model.CategoryModel;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.*;
import com.lla.core.enums.DocumentType;
import com.lla.core.enums.DevicePaymentOption;
import com.lla.core.enums.PlanTypes;
import com.lla.facades.customer.data.DocumentTypeData;
import org.apache.commons.collections.ListUtils;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;
import com.lla.facades.productconfigurator.data.LLAPuertoRicoProductConfigDTO;
import com.google.common.collect.Lists;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.enumeration.EnumerationService;
import java.util.stream.Stream;
import de.hybris.platform.core.model.product.ProductModel;
/**
 * Controller of the cart page and related cart operations, like remove or update cart.
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/cart")
public class CartPageController extends AbstractCartPageController
{

	private static final Logger LOGGER = Logger.getLogger(CartPageController.class);
	private static final String INVALID_PRE_CONFIG_CALL = "invalidPreConfigCall";
	public static final String SHOW_CHECKOUT_FLOWS_CONFIG_KEY = "storefront.show.checkout.flows";
	public static final String CART_CMS_PAGE_LABEL = "cart";
	public static final String CART_CHECKOUT_ERROR = "cart.checkout.error";
	public static final String CART_COMPATIBILITY_ERROR_ROAMING = "cart.checkout.compatibility.error1";
	public static final String CART_COMPATIBILITY_ERROR_PREPAID = "cart.checkout.compatibility.prepaid";
	public static final String CART_COMPATIBILITY_ERROR_PRODUCT_COUNT = "cart.checkout.compatibility.prepaid.product.count";
	public static final String CART_COMPATIBILITY_ERROR_TV = "cart.checkout.compatibility.error2";
	public static final String PAGE_TYPE_ATTRIBUTE = "pageType";
	public static final String CART_BREADCRUMB_KEY = "breadcrumb.cart";
	public static final String REDIRECT_TO_CART_URL = REDIRECT_PREFIX + "/cart";
	public static final String TYPE_MISMATCH_ERROR_CODE = "typeMismatch";
	public static final String SUBSCRIPTION_PRODUCT_ERROR_REGEX = "Subscription product.+must have a new quantity of 0 or 1.+";
	public static final String SUBSCRIPTION_PRODUCT_BASKET_ERROR = "basket.error.subscriptionProduct.quantity.invalid";
	public static final String BASKET_REMOVE_MESSAGE = "basket.page.message.remove";
	public static final String BASKET_REMOVE_MASTERENTRY_MESSAGE = "basket.page.message.remove.masterEntry";
	public static final String BASKET_UPDATE_MESSAGE = "basket.page.message.update";
	public static final String PRODUCT_CONFIG_DTO = "productConfigDTO";
	public static final String ONEP_90 = "internet_090";
	public static final String PRODUCT_CODES = "product.code.add";
	private static final String PUERTO_RICO_CATALOG_ID = "puertoricoContentCatalog";
	public static final String CURRENT_STEP_ID = "currentStepID";
	public static final String BUNDLE_MESSAGE = "bundleMessage";
	private static final String SMARTPRODUCTCATEGORY = "smartproduct";
	public static final String ADDONS_TV_LIST_WITHOUT_INTERNET = "addons.tv.list.without.internet";
	public static final String ADDONS_TV_LIST_WITH_INTERNET = "addons.tv.list.with.internet";
   public static final String DEFAULT_VALUE_SPACE = "";
   public static final String SELECTED_PLAN_PRICE = "selected_plan_price";
   public static final String COMMA = ",";
	public static final String ONEP = "internet_500";
	public static final String TWOP = "tv_ultimate";
	public static final String IMAGE = "image";
	public static final String DVR_BOX = "DVR_BOX";
   public static final String HD_BOX = "HD_BOX";
   public static final String HUB_TV_BOX = "HUB_TV_BOX";
   private static final String REDIRECT_CREATE_COMBINATION_URL = REDIRECT_PREFIX +"/configureProduct";
   private static final String HIDEPROGRESSBAR="hideProgressBar";
   private static final String PROGRESSBARFLAG="progressBarFlag";

	private static final String INTERNET_TV = "2PInternetTv";
	private static final String INTERNET_TV_TELEPHONIA = "3Pbundles";
	private static final String INTERNET_TELEPHONIA = "2PInternetTelephone";
	private static final String INTERNET_TV_TELEPHONIA_MOBILE = "4Pbundles";
	private static final String POSTPAID = "postpaid";
	private static final String DEVICES = "devices";

	public static final String CART_REDIRECT_URL = "lla.cabletica.redirect.url";

	private final String[] DISALLOWED_FIELDS = new String[] {};
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;
	@Resource(name = "llaLoginStrategy")
	private LLALoginStrategy llaLoginStrategy;
	@Autowired
	private SessionService sessionService;

	@Autowired
	private LLACommonUtil llaCommonUtil;

	@Autowired
	private CartService cartService;
	@Autowired
	private ModelService modelService;

	@Autowired
	private UserService userService;

	@Autowired
	private EnumerationService enumerationService;

	@Autowired
	private LLACartFacade llaCartFacade;

	@Autowired
	ProductService productService;

	@Autowired
	private ConfigurationService configurationService;
	
	@Resource(name = "llaTmaProductFacade")
	private LLATmaProductFacade llaTmaProductFacade;
	
	@Resource(name= "catalogVersionService")
	private CatalogVersionService catalogVersionService;
	
	@Resource(name= "mediaService")
	private MediaService mediaService;
	
	@Autowired
	private CalculationService calculationService;
	
	/**
	 * Retrieves the configured value for the {@link CartPageController#SHOW_CHECKOUT_FLOWS_CONFIG_KEY}.
	 *
	 * @return configured value for the {@link CartPageController#SHOW_CHECKOUT_FLOWS_CONFIG_KEY} or false if none is
	 *         provided
	 */
	@ModelAttribute("showCheckoutStrategies")
	public boolean isCheckoutStrategyVisible()
	{
		return getSiteConfigService().getBoolean(SHOW_CHECKOUT_FLOWS_CONFIG_KEY, false);
	}
	
	@ModelAttribute("documentTypes")
	public List<DocumentTypeData> getDocumentType()
	{

		final List<DocumentTypeData> documentTypeDataList = llaCommonUtil.getCustomerDocumentTypes();
		if (llaCommonUtil.isSiteCabletica())
		{
			List<DocumentTypeData> refinedDocumentTypeDataList = documentTypeDataList.stream()
					.filter(documentType -> !documentType.getCode().toString().equalsIgnoreCase(DocumentType.DRIVER_LICENSE.getCode()))
					.collect(Collectors.toList());
			refinedDocumentTypeDataList = refinedDocumentTypeDataList.stream()
					.filter(documentType -> !documentType.getCode().toString().equalsIgnoreCase(DocumentType.CEDULA.getCode()))
					.collect(Collectors.toList());
			refinedDocumentTypeDataList = refinedDocumentTypeDataList.stream()
					.filter(documentType -> !documentType.getCode().toString().equalsIgnoreCase(DocumentType.OTHER.getCode()))
					.collect(Collectors.toList());
			refinedDocumentTypeDataList.sort(Comparator.comparing(DocumentTypeData::getCode));
			return refinedDocumentTypeDataList;
		}
		return ListUtils.EMPTY_LIST;
	}
	
	

	@ModelAttribute("authUrl")
	public String getAuthorizationUrl(final HttpServletRequest request)
	{
		request.getSession().setAttribute("llaRefererUrl","/checkout");
		return llaLoginStrategy.authUrlConstructor(request, "auth");
	}

	@ModelAttribute("areas")
	public List<ProductRegionData> getAreaList()
	{
		return  llaCommonUtil.isSiteJamaica()?llaCommonUtil.getJamaicaRegions(): Collections.emptyList();
	}

	/**
	 * Displays the cart page.
	 *
	 * @param model
	 *           page model to be populated with information
	 * @return name of the view to be displayed
	 * @throws CMSItemNotFoundException
	 *            if the CmsPage cannot be found, while populating the model with meta data or title information
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String showCart(final Model model,final HttpServletRequest request) throws CMSItemNotFoundException
	{
		LLAPuertoRicoProductConfigDTO productConfig = getLlaPuertoRicoProductConfigDTO();
		if (model.asMap().containsKey(INVALID_PRE_CONFIG_CALL))
		{
			GlobalMessages.addErrorMessage(model, model.asMap().get(INVALID_PRE_CONFIG_CALL).toString());
		}
		prepareDataForPage(model);
		if(!llaCommonUtil.isSitePuertorico() && !llaCommonUtil.isSiteCabletica()){
			populateFixedLineProduct(model);
		}
	   if(getConfigurationService().getConfiguration().getString("fmc.active").equalsIgnoreCase("true")){
			removeFMCPromotionCodeFromCartAndEntry();
			displayPromotionMessageAtEntry(model);
		}
	   
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs(CART_BREADCRUMB_KEY));
		model.addAttribute(PAGE_TYPE_ATTRIBUTE, PageType.CART.name());
		model.addAttribute("userInSession",((CustomerModel)sessionService.getAttribute("user")).getUid().equalsIgnoreCase("anonymous")?true:false);
		final CartData cartData = llaCartFacade.getSessionCartWithEntryOrdering(false);
		List<EntryGroupData> entryGroups = cartData.getRootGroups();
		if(llaCommonUtil.isSitePanama()) {
			model.addAttribute("addonFreeOrderQuantityMap", getStringLongMap(getSiteConfigService().getString("productpage.addonproduct.freeproduct.numbers", null)));
			model.addAttribute("addonMaxOrderQuantityMap", getStringLongMap(getSiteConfigService().getString("productpage.addonproduct.maxperorder.numbers", null)));
			model.addAttribute("freeSim", llaCommonUtil.hideDeliveryMethod(cartService.getSessionCart()) ? false : true);
			model.addAttribute("freeShipping", llaCommonUtil.hideDeliveryMethod(cartService.getSessionCart()) ? false : true);
			model.addAttribute("freeInstalltion", llaCommonUtil.isFreeInstallationAvailable(cartService.getSessionCart()) ? true : false);
			model.addAttribute("cartCalculationMessage", llaCommonUtil.getCartCalculationMessage(getCartFacade().getSessionCart()));
			model.addAttribute("addonProductList",llaTmaProductFacade.getAddonProductsForPanama());
			populatePlanTypeDevicePaymentOption(model);
			if (entryGroups != null && llaTmaProductFacade.checkIfAddonProductListRequired())
			{
				setEntryGroups(cartData,entryGroups,model);
			}
			super.guidedStep(model);
			super.fmcDeviceStep(model);
			model.addAttribute("deviceWithFMC",sessionService.getAttribute("deviceWithFMC"));
		}
		if(llaCommonUtil.isSiteJamaica()){
			model.addAttribute("isCustomerExisting", null != cartService.getSessionCart() ?
					cartService.getSessionCart().getIsCustomerExisting() != null ?
							cartService.getSessionCart().getIsCustomerExisting().booleanValue() :
							false :
					false);
			boolean progressBarFlag=llaCommonUtil.isCartContainsFMCProducts(cartService.getSessionCart());
			sessionService.setAttribute(PROGRESSBARFLAG,progressBarFlag);
			model.addAttribute(HIDEPROGRESSBAR,sessionService.getAttribute(PROGRESSBARFLAG));
		}
		if (llaCommonUtil.isSitePuertorico())
		{
			if(null != sessionService.getAttribute(PRODUCT_CONFIG_DTO) &&
					StringUtils.isEmpty(productConfig.getOneP()) ||
					StringUtils.isEmpty(productConfig.getTwoP()) ||
					StringUtils.isEmpty(productConfig.getThreeP())) {
				productConfig = populateProductConfigDTO(new LLAPuertoRicoProductConfigDTO());
			}

			if (null != productConfig && StringUtils.isNotEmpty(productConfig.getTwoP()))
			{
				final Collection<TmaProductOfferingModel> channelProductList = llaTmaProductFacade
						.getProductForCode(productConfig.getTwoP()).getChannelProductList();
				final List<TmaProductOfferingModel> finalProducts = productConfig.getTwoP().contains(TWOP)
						? Arrays.asList(StringUtils.split(getConfigurationService().getConfiguration().getString(PRODUCT_CODES), ","))
								.stream().map(llaTmaProductFacade::getProductForCode).collect(Collectors.toList()).stream()
								.filter(fp -> channelProductList.contains(fp)).collect(Collectors.toList())
						: Lists.newArrayList();
				model.addAttribute("channelList",
						StringUtils.isNotEmpty(productConfig.getOneP()) && productConfig.getOneP().contains(ONEP)
								&& (!CollectionUtils.sizeIsEmpty(finalProducts)) ? finalProducts : channelProductList);
				model.addAttribute("llaTmaProductDatas", getProductDatas(productConfig));
			}
			if (StringUtils.isNotEmpty(productConfig.getTwoP()))
			{

				model.addAttribute("addonProducts", getAddons(productConfig));
				model.addAttribute("addonProductList", getAllTVAddons(productConfig));
			}
			model.addAttribute("smartProducts", llaTmaProductFacade.getProductForCategory(SMARTPRODUCTCATEGORY));
			model.addAttribute(BUNDLE_MESSAGE, llaTmaProductFacade.getBundleMessage(productConfig, 4));
			model.addAttribute(PRODUCT_CONFIG_DTO, productConfig);
			model.addAttribute(IMAGE, getMediaForCode("prAddonPageBanner"));
			model.addAttribute(CURRENT_STEP_ID, "4");
			model.addAttribute(SELECTED_PLAN_PRICE, sessionService.getAttribute(SELECTED_PLAN_PRICE));

			//		model.addAttribute("tvaddonProducts", llaCartFacade.getTVAddonIfPresent());
			//		model.addAttribute("primaryProducts", llaCartFacade.getPrimaryProductsList());
			//		model.addAttribute("channelAddonProducts", llaCartFacade.getChannelAddonProducts());
			//		model.addAttribute("smartProduct", llaCartFacade.getSmartProductIfPresent());
		}
 	if(llaCommonUtil.isSiteCabletica()) {
 		model.addAttribute("c2cTimerOnCart",configurationService.getConfiguration().getString("c2c.timerA.cartpage"));
 		model.addAttribute("cartRedirectURL", getSiteConfigService().getString(CART_REDIRECT_URL,null));
 		model.addAttribute("addonCableticaProductList",llaTmaProductFacade.getAddonProductsForCabletica());
		model.addAttribute("addonCableticaMaxOrderQuantityMap", getStringLongMap(getSiteConfigService().getString("productpage.addoncableticaproduct.maxperorder.numbers",null)));
		model.addAttribute("addontvCableticaMaxOrderQuantityMap", getStringLongMap(getSiteConfigService().getString("productpage.addontvcableticaproduct.maxperorder.numbers",null)));
		if (entryGroups != null)
		{
			setEntryGroups(cartData,entryGroups,model);
		}	
 	 }
			return TelcoControllerConstants.Views.Pages.Cart.CART_PAGE;
	}
	
	private LLAPuertoRicoProductConfigDTO populateProductConfigDTO(LLAPuertoRicoProductConfigDTO productConfig)
	{
		if(null != cartService.getSessionCart()) {
			CartModel cart = cartService.getSessionCart();
			if(null != cart.getEntries() && CollectionUtils.isNotEmpty(cart.getEntries())) {
				for(AbstractOrderEntryModel entry : cart.getEntries()) {
					TmaProductOfferingModel product = (TmaProductOfferingModel)entry.getProduct();
					CategoryModel cat = product.getSupercategories().iterator().next();
					if(cat.getCode().equalsIgnoreCase("internet"))
					{
						productConfig.setOneP(product.getCode());
					}
					if (cat.getCode().equalsIgnoreCase("tv"))
					{
						productConfig.setTwoP(product.getCode());
					}
					if(cat.getCode().equalsIgnoreCase("phone"))
					{
						productConfig.setThreeP(product.getCode());
					}
					if (cat.getCode().equalsIgnoreCase("tvaddon") && entry.getProduct().getCode().equalsIgnoreCase(DVR_BOX))
					{
						productConfig.setTwoPAddonDVR(DVR_BOX);
					}
					if (cat.getCode().equalsIgnoreCase("tvaddon") && entry.getProduct().getCode().equalsIgnoreCase(HD_BOX))
					{
						productConfig.setTwoPAddonHD(HD_BOX);
					}
					if (cat.getCode().equalsIgnoreCase("tvaddon") && entry.getProduct().getCode().equalsIgnoreCase(HUB_TV_BOX))
					{
						productConfig.setTwoPHUB(HUB_TV_BOX);
					}
				}
			}
		}
		return productConfig;
	}
	
	private List<ProductData> getProductDatas(LLAPuertoRicoProductConfigDTO productConfig)
	{
		List<String> productCodes = new ArrayList<String>(Arrays.asList(productConfig.getOneP(),productConfig.getTwoP(),productConfig.getTwoPHUB(),productConfig.getTwoPAddonHD(),productConfig.getTwoPAddonDVR(),productConfig.getThreeP()));
		productCodes.addAll(null!=productConfig.getChannelList()?productConfig.getChannelList() : CollectionUtils.EMPTY_COLLECTION);
		List<TmaProductOfferingModel> productDatas = productCodes.stream().filter(StringUtils::isNotEmpty).map(llaTmaProductFacade::getProductForCode).collect(Collectors.toList());
		return llaTmaProductFacade.getProductData(productDatas);
	}
	
	private List<String> getAllTVAddons(LLAPuertoRicoProductConfigDTO productConfig)
	{
		List <String> allAddons = new ArrayList<>();
		if(StringUtils.isNotEmpty(productConfig.getTwoPAddonDVR()))
		{
			allAddons.add(DVR_BOX);
		}
		if(StringUtils.isNotEmpty(productConfig.getTwoPAddonHD()))
		{
			allAddons.add(HD_BOX);
		}
		if(StringUtils.isNotEmpty(productConfig.getTwoPHUB()))
		{
			allAddons.add(HUB_TV_BOX);
		}
		return allAddons;
	}
	
	public LLAPuertoRicoProductConfigDTO getLlaPuertoRicoProductConfigDTO() {
      LLAPuertoRicoProductConfigDTO productConfig = new LLAPuertoRicoProductConfigDTO();
      if (null != sessionService.getAttribute(PRODUCT_CONFIG_DTO)) {
          productConfig = sessionService.getAttribute(PRODUCT_CONFIG_DTO);
      } else {
          sessionService.setAttribute(PRODUCT_CONFIG_DTO, productConfig);
      }
		return productConfig;
	}
	
	public MediaModel getMediaForCode(final String code)
	{
		try
		{
			return mediaService.getMedia(catalogVersionService.getCatalogVersion(PUERTO_RICO_CATALOG_ID, "Online"), code);
		}
		catch (final UnknownIdentifierException ignore)
		{
			// Ignore this exception
		}
		return null;
	}
	
	public List<TmaProductOfferingModel> getAddons(final LLAPuertoRicoProductConfigDTO productConfig) {
      List<String> addons;
      if (null != productConfig && null != productConfig.getOneP()) {
          addons = Arrays.asList(StringUtils.split(getSiteConfigService().getString(ADDONS_TV_LIST_WITH_INTERNET, DEFAULT_VALUE_SPACE), COMMA));
      } else{
          addons = Arrays.asList(StringUtils.split(getSiteConfigService().getString(ADDONS_TV_LIST_WITHOUT_INTERNET, DEFAULT_VALUE_SPACE), COMMA));
      }
      final List<TmaProductOfferingModel> list = new ArrayList<>();
      for(final String code: addons) {
          list.add(llaTmaProductFacade.getProductForCode(code));
      }
      return list;
  }

	private void setEntryGroups(CartData cartData,List<EntryGroupData> entryGroups,Model model) {
	     Collections.reverse(entryGroups);
	     cartData.setRootGroups(entryGroups);
	     model.addAttribute("cartData", cartData);
   }

	private Map<String, Long> getStringLongMap(String addonQuantity) {
		return Arrays.stream(addonQuantity.split(","))
				.map(s -> s.split(":"))
				.collect(Collectors.toMap(s -> s[0], s -> Long.valueOf(s[1])));
	}

	/**
	 * Remove applied promotion on Cart Page Landing
	 */
	private void removeFMCPromotionCodeFromCartAndEntry() {
		final CartModel cartModel=cartService.getSessionCart();
		final CustomerModel customerModel=(CustomerModel)cartModel.getUser();
		cartModel.setAppliedFMCPromoCode(null);
		for(AbstractOrderEntryModel item:cartModel.getEntries()){
			if(StringUtils.isNotEmpty(item.getFmcPromoPlanCode())){
				item.setFmcPromoPlanCode(StringUtils.EMPTY);
				modelService.save(item);
				modelService.refresh(item);
				modelService.save(customerModel);
				modelService.refresh(customerModel);
			}
		}
		modelService.save(cartModel);
		modelService.refresh(cartModel);
	}

	/**
	 *  Display Promotion Message at Entry
	 * @param model
	 */
	private void displayPromotionMessageAtEntry(Model model) {
		final CartData cartData = getCartFacade().getSessionCartWithEntryOrdering(false);
		final CartModel cartModel=cartService.getSessionCart();
		final CustomerModel customerModel=(CustomerModel) cartModel.getUser();
		final String catCode=getConfigurationService().getConfiguration().getString(LlaCoreConstants.FMC_PROMOTION_ALLOWED_CATEGORY);
	  	if(llaCommonUtil.validateCartContainsValidFMCProducts(cartModel).booleanValue()){
	      for(OrderEntryData entry:cartData.getEntries()){
				if(llaCommonUtil.containsFirstLevelCategoryWithCode(productService.getProductForCode(entry.getProduct().getCode()),catCode)) {
					entry.setFmcRuleApplied(Boolean.TRUE);
					GlobalMessages.addInfoMessage(model, "fmc.promotion.maximum.redemption.allowed");
				}
		   }
	  	}

	}


	/**
	 * Populate Fixed Line Products
	 * @param model
	 */
	private void populateFixedLineProduct(final Model model)
	{
		final CartData cartData = getCartFacade().getSessionCartWithEntryOrdering(false);
		if (CollectionUtils.isNotEmpty(cartData.getEntries()))
		{
			for (final OrderEntryData entry : cartData.getEntries())
			{
				entry.setFixedLineProduct(llaCommonUtil.checkFixedLineProduct(entry.getProduct().getCode()));

				LOGGER.info("Entry number: "+entry.getEntryNumber()+" with product code "+entry.getProduct().getCode()+" : "+entry.isFixedLineProduct());
			}
		}
		model.addAttribute("cartData", cartData);
	}

	/**
	 * Handle the '/cart/checkout' request url. This method checks to see if the cart is valid before allowing the
	 * checkout to begin. Note that this method does not require the user to be authenticated and therefore allows us to
	 * validate that the cart is valid without first forcing the user to login. The cart will be checked again once the
	 * user has logged in.
	 *
	 * @param model
	 *           the spring Model object. not used here, but included in the signature to allow overriding in subclasses.
	 * @return The page to redirect to
	 */
	@RequestMapping(value = "/checkout", method = RequestMethod.GET)
	@RequireHardLogIn
	public String cartCheck(final Model model, final RedirectAttributes redirectModel)
	{
		SessionOverrideCheckoutFlowFacade.resetSessionOverrides();

		if (!getCartFacade().hasEntries())
		{
			LOGGER.debug("Missing or empty cart");

			// No session cart or empty session cart. Bounce back to the cart page.
			return REDIRECT_PREFIX + "/cart";
		}


		if (validateCart(redirectModel))
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, CART_CHECKOUT_ERROR, null);
			return REDIRECT_PREFIX + "/cart";
		}

		if(llaCommonUtil.isSitePanama() && llaCommonUtil.isPrepaidCart()){
			if(!llaCommonUtil.isContainsOnlyPrepaidProducts()) {
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, CART_COMPATIBILITY_ERROR_PREPAID, null);
				return REDIRECT_PREFIX + "/cart";
			}else if(llaCommonUtil.getCartAddedProductCount() > 5){
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, CART_COMPATIBILITY_ERROR_PRODUCT_COUNT, null);
				return REDIRECT_PREFIX + "/cart";
			}
		}

		if(!llaCommonUtil.isSitePuertorico() && llaCartFacade.checkCartForCompatibilityIssues()!=null)
		{
			String category = llaCartFacade.checkCartForCompatibilityIssues();
			if(category.equalsIgnoreCase(configurationService.getConfiguration().getString(LlaFacadesConstants.SUBSCRIPTION_CATEGORY_MASTER_1)))
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, CART_COMPATIBILITY_ERROR_ROAMING, null);
			}
			else if (category.equalsIgnoreCase(configurationService.getConfiguration().getString(LlaFacadesConstants.SUBSCRIPTION_CATEGORY_MASTER_2)))
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, CART_COMPATIBILITY_ERROR_TV, null);
			}
			return REDIRECT_PREFIX + "/cart";
		}

		if(llaCommonUtil.isSiteJamaica()){
			CartData cartData=getCartFacade().getSessionCart();
			if(cartData.getEntries().size() >= 2 && llaCommonUtil.cartContainsSameProduct(cartData)){
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "cart.entries.product.quantity.error", null);
				return REDIRECT_PREFIX + "/cart";
			}
			if(cartData.getEntries().size() >= 2 && !llaCommonUtil.cartContainsEntiresOfDistinctCategory(cartData)){
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "cart.entries.quantity.error", null);
				return REDIRECT_PREFIX + "/cart";
			}
		}
		// Redirect to the start of the checkout flow to begin the checkout process
		// We just redirect to the generic '/checkout' page which will actually select the checkout flow
		// to use. The customer is not necessarily logged in on this request, but will be forced to login
		// when they arrive on the '/checkout' page.
		return REDIRECT_PREFIX + "/checkout";
	}

	/**
	 * @param model
	 *           the Spring Model object. Included to allow subclass overriding.
	 * @param redirectModel
	 *           the RedirectAttributes object. Included to allow subclass overriding.
	 */
	// This controller method is used to allow the site to force the visitor through a specified checkout flow.
	// If you only have a static configured checkout flow then you can remove this method.
	@RequestMapping(value = "/checkout/select-flow", method = RequestMethod.GET)
	@RequireHardLogIn
	public String initCheck(final Model model, final RedirectAttributes redirectModel,
			@RequestParam(value = "flow", required = false) final CheckoutFlowEnum checkoutFlow,
			@RequestParam(value = "pci", required = false) final CheckoutPciOptionEnum checkoutPci)
	{
		SessionOverrideCheckoutFlowFacade.resetSessionOverrides();

		if (!getCartFacade().hasEntries())
		{
			LOGGER.debug("Missing or empty cart");

			// No session cart or empty session cart. Bounce back to the cart page.
			return REDIRECT_PREFIX + "/cart";
		}

		// Override the Checkout Flow setting in the session
		if (checkoutFlow != null && StringUtils.isNotBlank(checkoutFlow.getCode()))
		{
			SessionOverrideCheckoutFlowFacade.setSessionOverrideCheckoutFlow(checkoutFlow);
		}

		// Override the Checkout PCI setting in the session
		if (checkoutPci != null && StringUtils.isNotBlank(checkoutPci.getCode()))
		{
			SessionOverrideCheckoutFlowFacade.setSessionOverrideSubscriptionPciOption(checkoutPci);
		}

		// Redirect to the start of the checkout flow to begin the checkout process
		// We just redirect to the generic '/checkout' page which will actually select the checkout flow
		// to use. The customer is not necessarily logged in on this request, but will be forced to login
		// when they arrive on the '/checkout' page.
		return REDIRECT_PREFIX + "/checkout";
	}

	/**
	 * Deletes all cart entries corresponding to the bundle with the number given from the current cart.
	 *
	 * @param form
	 *           bundle delete form, indicating the bundle No to be deleted
	 * @param model
	 *           page model to be populated with information
	 * @param bindingResult
	 *           request binding result to retrieve validation errors from
	 * @param redirectModel
	 *           page model used for redirection to be populated with information
	 * @return the url to which the request will be redirected
	 * @throws CMSItemNotFoundException
	 *            if the CmsPage cannot be found, while populating the model with meta data or title information
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String deleteCartBundle(@Valid final DeleteBundleForm form, final Model model, final BindingResult bindingResult,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (bindingResult.hasErrors())
		{
			for (final ObjectError error : bindingResult.getAllErrors())
			{
				GlobalMessages.addErrorMessage(model, error.getDefaultMessage());
			}
		}

		String alertMessageCode;
		try
		{
			if(llaCommonUtil.isSitePuertorico() || llaCommonUtil.isSiteCabletica() ){
				getCartFacade().removeSessionCart();
				sessionService.removeAttribute(PRODUCT_CONFIG_DTO);
			}else {
				getCartFacade().removeEntryGroup(form.getGroupNumber());
			}
			alertMessageCode = "basket.page.bundle.message.remove";
		}
		catch (final CommerceCartModificationException e)
		{
			LOGGER.warn("Could not delete entry group with number " + form.getGroupNumber(), e);
			alertMessageCode = "basket.page.bundle.message.remove.error";
		}
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, alertMessageCode);
		prepareDataForPage(model);
		if(llaCommonUtil.isSitePuertorico()) {
			return  REDIRECT_CREATE_COMBINATION_URL;
		}
		return REDIRECT_TO_CART_URL;
	}
	
	@RequestMapping(value = "/deleteCart", method = RequestMethod.POST)
	public String deleteCartCabletica(@RequestParam(value = "selectedCategory", required = false)
	final String selectedCategory,@Valid final DeleteBundleForm form, final Model model, final BindingResult bindingResult,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (bindingResult.hasErrors())
		{
			for (final ObjectError error : bindingResult.getAllErrors())
			{
				GlobalMessages.addErrorMessage(model, error.getDefaultMessage());
			}
		}
		String alertMessageCode;
		try
		{
			if(llaCommonUtil.isSiteCabletica()){
				getCartFacade().removeSessionCart();
			}
			else {
				getCartFacade().removeEntryGroup(form.getGroupNumber());
			}
			alertMessageCode = "basket.page.bundle.message.remove";
		}
		catch (final CommerceCartModificationException e)
		{
			LOGGER.warn("Could not delete entry group with number " + form.getGroupNumber(), e);
			alertMessageCode = "basket.page.bundle.message.remove.error";
		}
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, alertMessageCode);
		prepareDataForPage(model);
		return REDIRECT_PREFIX + "/residential?selectedCategory="+selectedCategory;
	}

	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/deleteCartOnCrossButton", method = RequestMethod.POST)
	public @ResponseBody String deleteCart(@RequestParam(value = "selectedCategory", required = false)
										   final String selectedCategory)
	{
		try
		{
			if(llaCommonUtil.isSiteCabletica()){
				getCartFacade().removeSessionCart();
			}
		}
		catch (final Exception e)
		{
			return String.valueOf(HttpStatus.FAILED_DEPENDENCY);
		}
		return String.valueOf(HttpStatus.OK);
	}

	/**
	 * Update the quantity of the product for the entry with the number given.
	 *
	 * @param entryNumber
	 *           entry number of the cart entry to be updated
	 * @param model
	 *           page model to be populated with information
	 * @param form
	 *           update quantity form specifying the new quantity of the product from the entry with the number given
	 * @param bindingResult
	 *           request binding result to retrieve validation errors from
	 * @param request
	 *           http request to retrieve the url from
	 * @param redirectModel
	 *           redirect model to be populated with information
	 * @return the url to the result page
	 * @throws CMSItemNotFoundException
	 *            if the CmsPage cannot be found, while populating the model with meta data or title information
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateCartQuantities(@RequestParam("entryNumber") final long entryNumber, final Model model,
			@Valid final UpdateQuantityForm form, final BindingResult bindingResult, final HttpServletRequest request,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (bindingResult.hasErrors())
		{
			populatePageModelWithErrors(model, bindingResult);
		}
		else if (getCartFacade().hasEntries())
		{
			try
			{
				final long newQuantity = form.getQuantity().longValue();
				final CartModificationData cartModification = getCartFacade().updateCartEntry(entryNumber, newQuantity);
				String alertMessageCode;
				if (cartModification.getQuantity() == newQuantity)
				{
					// Success in either removal or updating the quantity
					if(cartModification.isDeleteMasterEntry())
					{
						alertMessageCode = cartModification.getQuantity() == 0 ? BASKET_REMOVE_MASTERENTRY_MESSAGE : BASKET_UPDATE_MESSAGE;
					}
					else
					{
						alertMessageCode = cartModification.getQuantity() == 0 ? BASKET_REMOVE_MESSAGE : BASKET_UPDATE_MESSAGE;
					}
					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, alertMessageCode);
				}
				else
				{
					handleUpdateQuantityFailure(newQuantity, request, redirectModel, cartModification);
				}
			}
			catch (final JaloObjectNoLongerValidException | CommerceCartModificationException ex)
			{
				LOGGER.warn("Could not update quantity of product with entry number: " + entryNumber + ".", ex);
			}
			catch (final IllegalArgumentException e)
			{
				LOGGER.info(e);
				if (e.getMessage().matches(SUBSCRIPTION_PRODUCT_ERROR_REGEX))
				{
					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
							SUBSCRIPTION_PRODUCT_BASKET_ERROR, new Object[]
							{ form.getQuantity() });
				}
			}
		}
		if(llaCommonUtil.isSitePanama()) {
			if(cartService.getSessionCart().getEntries().size()==0) {
				sessionService.removeAttribute("fmcProduct");
				sessionService.removeAttribute("deviceProduct");
			}
			validatePlanTypeDevicePaymentOption();
		}
		prepareDataForPage(model);

		return REDIRECT_TO_CART_URL;
	}

	/**
	 * Handles the case when the update quantity call failed by adding corresponding error messages on the redirectModel.
	 *
	 * @param newQuantity
	 *           the new quantity, as set by the customer
	 * @param request
	 *           http request to retrieve the url from
	 * @param redirectModel
	 *           redirect model to be populated with information
	 * @param cartModification
	 *           resulting cart modification after applying the quantity update
	 */
	protected void handleUpdateQuantityFailure(final long newQuantity, final HttpServletRequest request,
			final RedirectAttributes redirectModel, final CartModificationData cartModification)
	{
		final ProductData modifiedProduct = cartModification.getEntry().getProduct();
		final StringBuffer productUrl = request.getRequestURL().append(modifiedProduct.getUrl());
		final long updatedQuantity = cartModification.getQuantity();
		if (updatedQuantity > 0)
		{
			// Not enough stock available
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"basket.page.message.update.reducedNumberOfItemsAdded.lowStock", new Object[]
					{ modifiedProduct.getName(), updatedQuantity, newQuantity, productUrl });
			return;
		}
		// No more items available
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
				"basket.page.message.update.reducedNumberOfItemsAdded.noStock", new Object[]
				{ modifiedProduct.getName(), productUrl });
	}

	/**
	 * Populates the page model with the errors found on the binding result given.
	 *
	 * @param model
	 *           page model to be populated with errors
	 * @param bindingResult
	 *           binding result to retrieve the errors from
	 */
	protected void populatePageModelWithErrors(final Model model, final BindingResult bindingResult)
	{
		for (final ObjectError error : bindingResult.getAllErrors())
		{
			final String errorMessageCode = TYPE_MISMATCH_ERROR_CODE.equals(error.getCode()) ? "basket.error.quantity.invalid"
					: error.getDefaultMessage();
			GlobalMessages.addErrorMessage(model, errorMessageCode);
		}
	}

	@Override
	protected void createProductList(final Model model) throws CMSItemNotFoundException
	{
		final CartData cartData = getCartFacade().getSessionCartWithEntryOrdering(false);
		super.createProductEntryList(model, cartData);
		storeCmsPageInModel(model, getContentPageForLabelOrId(CART_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CART_CMS_PAGE_LABEL));

		model.addAttribute(WebConstants.BREADCRUMBS_KEY, getResourceBreadcrumbBuilder().getBreadcrumbs(CART_BREADCRUMB_KEY));
		model.addAttribute(PAGE_TYPE_ATTRIBUTE, PageType.CART.name());
	}

	protected ResourceBreadcrumbBuilder getResourceBreadcrumbBuilder()
	{
		return resourceBreadcrumbBuilder;
	}

	@Resource(name = "simpleBreadcrumbBuilder")
	public void setResourceBreadcrumbBuilder(final ResourceBreadcrumbBuilder resourceBreadcrumbBuilder)
	{
		this.resourceBreadcrumbBuilder = resourceBreadcrumbBuilder;
	}

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		binder.setDisallowedFields(DISALLOWED_FIELDS);
	}

	@ResponseBody
	@RequestMapping(value = "/validateUserSession", method = RequestMethod.GET, produces = "application/json")
	public String validateUserSession(final HttpServletRequest request, final HttpServletResponse response) {
		return ((CustomerModel)sessionService.getAttribute("user")).getUid().equalsIgnoreCase("anonymous")?"true":"false";
	}
	
	/**
	 * Using productCodePost to get entry from cart to remove from cart
	 *
	 * @param productCodePost
	 *           {@link de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel#CODE}
	 * @return the cart page
	 */
	
	@RequestMapping(value = "/removeAddon", method = RequestMethod.GET)
	public String removeAddonFromCart(@RequestParam("productCodePost")
	final String code, final Model model,final RedirectAttributes redirectModel)
	{
		final long newQuantity = 0L;
		final CartModel cartModel = cartService.getSessionCart();
		final List<AbstractOrderEntryModel> entries = new ArrayList<AbstractOrderEntryModel>();
		if(StringUtils.isNotEmpty(code) && code.equals(DVR_BOX)) {
			llaCartFacade.removeDVRBundleFromCart(cartModel.getBundleProductMapping());
		}
		if (CollectionUtils.isNotEmpty(cartModel.getEntries()) && StringUtils.isNotEmpty(code))
		{
			for (final AbstractOrderEntryModel entry : cartModel.getEntries())
			{
				if ((entry.getProduct().getCode().equals(code)))
				{
					try
					{
						final CartModificationData cartModification = getCartFacade().updateCartEntry(entry.getEntryNumber(), newQuantity);
						String alertMessageCode;
						if (cartModification.getQuantity() == newQuantity)
						{
							alertMessageCode = cartModification.getQuantity() == 0 ? "basket.page.message.remove"
									: "basket.page.message.update";
							GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, alertMessageCode);
						}
					}
					catch (final JaloObjectNoLongerValidException | CommerceCartModificationException ex)
					{
						LOGGER.error("Could not update quantity of product with entry number: " + entry.getEntryNumber() + ".", ex);
					}
				}
			}
		}
		return REDIRECT_PREFIX + "/cart";
	}

	/*
	 * Method returns boolean value based on categories of products present in cart for DevicePaymentOption.If Cart only contains products from categories such as
	 * for devices it returns true
	 */
	private boolean checkDevicePaymentOption(final CartModel cartModel)
	{
		boolean toRemoveDevicePaymentOption = false;
		final String categoriesToHaveDevicePaymentOption =configurationService.getConfiguration().getString("categories.to.check.devicepaymentoption.panama");
		final List<String> categoryList= Stream.of(categoriesToHaveDevicePaymentOption.split(",", -1))
				.collect(Collectors.toList());

		if (null != cartModel && !CollectionUtils.isEmpty(cartModel.getEntries()))
		{
			for (final AbstractOrderEntryModel entry : cartModel.getEntries())
			{
				for (final String categoryCode : categoryList)
				{
					if (llaCommonUtil.containsFirstLevelCategoryWithCode(entry.getProduct(), categoryCode)
							|| llaCommonUtil.containsSecondLevelCategoryWithCode(entry.getProduct(), categoryCode))
					{
						toRemoveDevicePaymentOption = true;
						return toRemoveDevicePaymentOption;
					}
				}				
			}
		}
		return toRemoveDevicePaymentOption;
	}

	/*
	 * Method returns boolean value based on categories of products present in cart for PlanType.If Cart only contains products from categories such as
	 * for postpaid,4Pbundles,prepaid it returns true
	 */
	private boolean checkPlanTypeOption(final CartModel cartModel)
	{
		boolean toRemovePlanTypeOption = false;
		final String categoriesToHavePlanTypeOption =configurationService.getConfiguration().getString("categories.to.check.plantype.panama");
		final List<String> categoryList= Stream.of(categoriesToHavePlanTypeOption.split(",", -1))
				.collect(Collectors.toList());

		if (null != cartModel && !CollectionUtils.isEmpty(cartModel.getEntries()))
		{
			for (final AbstractOrderEntryModel entry : cartModel.getEntries())
			{
				for (final String categoryCode : categoryList)
				{
					if (llaCommonUtil.containsFirstLevelCategoryWithCode(entry.getProduct(), categoryCode)
							|| llaCommonUtil.containsSecondLevelCategoryWithCode(entry.getProduct(), categoryCode))
					{
						toRemovePlanTypeOption = true;
						return toRemovePlanTypeOption;
					}
				}
			}
		}
		return toRemovePlanTypeOption;
	}

	/*
	 * Check for plantype and devicepaymentoption to reset the value for invalid category
	 */
	private void validatePlanTypeDevicePaymentOption(){

			CartModel cartValue = cartService.getSessionCart();
			if (!checkDevicePaymentOption(cartValue)) {
				cartValue.setDevicePaymentOption(null);
				modelService.save(cartValue);
				modelService.refresh(cartValue);
			}
			if (!checkPlanTypeOption(cartValue)) {
				cartValue.setPlanTypes(null);
				modelService.save(cartValue);
				modelService.refresh(cartValue);
			}
	}

	/*
	 * Populate  plantype and devicepaymentoption value for valid category
	 */
	private void populatePlanTypeDevicePaymentOption(Model model) {
		if (checkDevicePaymentOption(cartService.getSessionCart())) {
			model.addAttribute("devicePaymentOptions", llaCommonUtil.getDevicePaymentOptions());
		}
		if (checkPlanTypeOption(cartService.getSessionCart())) {
			model.addAttribute("planTypes", llaCommonUtil.getPlanTypes());
		}
	}
}
