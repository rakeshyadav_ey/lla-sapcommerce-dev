package com.lla.telcoaddon.controllers.pages.checkout.steps;

import com.lla.core.enums.ClickToCallReasonEnum;
import com.lla.core.enums.FalloutReasonEnum;
import com.lla.core.enums.OriginSource;
import com.lla.core.service.creditscore.LLACreditScoreService;
import com.lla.mulesoft.integration.response.LLAApiResponse;
import com.lla.core.util.LLACommonUtil;
import com.lla.facades.cart.LLACartFacade;
import com.lla.facades.checkout.LLACheckoutFacade;
import com.lla.facades.customer.LLACustomerFacade;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.telcoaddon.controllers.ControllerConstants;
import com.lla.telcoaddon.controllers.util.CartHelper;
import com.lla.telcoaddon.service.LLAPersonalDetailService;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import com.lla.mulesoft.integration.service.LLAMulesoftDebtCheckApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.lla.telcoaddon.forms.validation.CustomerSSNFormValidator;
import com.lla.telcoaddon.forms.CustomerSSNForm;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Objects;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.commercefacades.user.data.AddressData;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.util.Strings;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.core.model.user.AddressModel;
import com.lla.core.util.LLAEncryptionUtil;


@Controller
@RequestMapping(value = "checkout/multi/credit-check")
public class CreditCheckCheckoutStepController extends AbstractCheckoutStepController
{
	private static final Logger LOG = LoggerFactory.getLogger(CreditCheckCheckoutStepController.class);
	protected static final String CREDIT_CHECK = "credit-check";
	protected static final String ATTR_CART_DATA = "cartData";
	protected static final String CALL_BACK = "/callBack";
	protected static final String ERROR="/error";
	private static final String CREDIT_CHECK_RESPONSE = "creditCheckResponse";
	private final String[] DISALLOWED_FIELDS = new String[] {};
	protected static final String UPDATED_DELIVERY_ADDRESS = "deliveryAddresses";
	protected static final String ATTR_COUNTRY = "country";
	protected static final String ATTR_REGIONS = "regions";
	protected static final String ATTR_ADDRESS_FORM = "addressForm";
	private static final String SUCCESS = "success";
	private static final String RURAL_RESIDENCE_ENUM = "RuralResidenceEnum";
	private static final String URBAN_RESIDENCE_ENUM = "UrbanResidenceEnum";
	private static final String REDIRECT_TO_ERROR_URL = REDIRECT_PREFIX + "/error";
	private static final String REDIRECT_TO_CALLBACK_URL = REDIRECT_PREFIX + "/callBack";
	public static final String ENABLE_CREDIT_CHECK_API = "puertorico.credit.check.api.enable";
	public static final String NOT_FOUND="400";
	public static final String ERROR_MESSAGE = "register.ssn.lcpr.invalid";

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "userService")
	private UserService userService;
	@Resource(name = "customerFacade")
	private LLACustomerFacade llaCustomerFacade;
	@Resource(name = "checkoutFacade")
	private LLACheckoutFacade llaCheckoutFacade;
	@Autowired
	private LLACommonUtil llaCommonUtil;
	@Autowired
	private EnumerationService enumerationService;
	@Autowired
	private CartService cartService;
	@Autowired
	private LLAPersonalDetailService llaPersonalDetailService;
	@Autowired
	private LLACartFacade llaCartFacade;
	@Autowired
	private LLACreditScoreService llaCreditScoreService;
	@Autowired
	private ModelService modelService;
	@Resource(name = "sessionService")
   private SessionService sessionService;
	@Autowired
	protected ConfigurationService configurationService;
	@Resource(name = "customerSSNFormValidator")
   private CustomerSSNFormValidator customerSSNFormValidator;
	@Autowired
   private LLAMulesoftDebtCheckApiService llaMulesoftDebtCheckApiService;
	@Autowired
   private LLAEncryptionUtil llaEncryptionUtil;

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateCheckoutStep(checkoutStep = CREDIT_CHECK)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{

		CustomerModel customerModel = (CustomerModel) userService.getCurrentUser();
		final CartData cartData = llaCheckoutFacade.getCheckoutCart();
		//skip debt check and credit check in case of error or inserviceable address
		if(llaCommonUtil.isSitePuertorico()){
			CartModel cartModel=cartService.getSessionCart();
      			  //update credit score details when user came back.           
       			llaCommonUtil.updateCreditScoreDetails(cartModel);
			removeFalloutReason();
			if(null != cartModel.getFalloutReasonValue() || cartModel.getIsRequiredServiceActive()) {
			       return getCheckoutStep().nextStep();
			}
		}

		cartData.setEntries(CartHelper.removeEmptyEntries(cartData.getEntries()));
		model.addAttribute(ATTR_CART_DATA, cartData);
		model.addAttribute(ATTR_ADDRESS_FORM, new AddressForm());
		model.addAttribute("urbanResidenceTypes",llaCommonUtil.getPossibleResidenceTypes(URBAN_RESIDENCE_ENUM));
		model.addAttribute("ruralResidenceTypes",llaCommonUtil.getPossibleResidenceTypes(RURAL_RESIDENCE_ENUM));
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.creditCheck.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		model.addAttribute("customerSSNForm", new CustomerSSNForm());
		setCheckoutStepLinksForModel(model, getCheckoutStep());
		this.prepareDataForPage(model);
		return ControllerConstants.Views.Pages.MultiStepCheckout.CreditCheckPage;
	}

	//This method is to remove FalloutReason from cart if customer do back from summary page
	private void removeFalloutReason() {
		if(llaCommonUtil.isSitePuertorico()){
			FalloutReasonEnum falloutReasonValue= cartService.getSessionCart().getFalloutReasonValue();
			if(null != falloutReasonValue && (falloutReasonValue.equals(FalloutReasonEnum.CREDIT_CHECK_REFUSED) || falloutReasonValue.equals(FalloutReasonEnum.CREDIT_CHECK_UNSUCCESSFUL)
					|| falloutReasonValue.equals(FalloutReasonEnum.CUSTOMER_WITH_DEBT) || falloutReasonValue.equals(FalloutReasonEnum.CUSTOMER_WITHOUT_SSN))) {
				CartModel cart = cartService.getSessionCart();
				cart.setFalloutReasonValue(null);
				modelService.save(cart);
				modelService.refresh(cart);
			}
		}
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		binder.setDisallowedFields(DISALLOWED_FIELDS);
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(CREDIT_CHECK);
	}
	
	@RequestMapping(value = "/updateCustomerSSN", method = RequestMethod.POST)
	@ResponseBody
	public String debtCheck(final CustomerSSNForm form, BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		if (configurationService.getConfiguration().getBoolean("mulesoft.lcpr.debtcheck.api.execute")
				&& llaCommonUtil.isSitePuertorico())
		{
			customerSSNFormValidator.validate(form, bindingResult);
			if (bindingResult.hasErrors())
			{
				this.prepareDataForPage(model);
				storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
				setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
				model.addAttribute(WebConstants.BREADCRUMBS_KEY,
						getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.creditCheck.breadcrumb"));
				model.addAttribute("metaRobots", "noindex,nofollow");
				model.addAttribute("customerSSNForm", new CustomerSSNForm());
				setCheckoutStepLinksForModel(model, getCheckoutStep());
				return ERROR_MESSAGE;
			}
			llaPersonalDetailService.updateCustomerSSN(form.getSsn());
			String redirectToCallbackUrl = triggerDebtCheckCallForLCPR(form, model, request, redirectModel);
			return redirectToCallbackUrl;
		}
		else
		{
			CartModel currentCart=cartService.getSessionCart();
			LOG.info(String.format("Skipping Debt check and Credit Check API Call for puertorico Cart :%s for User :%s", currentCart.getCode(),currentCart.getUser().getUid()));
			return getCheckoutStep().nextStep();
		}

	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public String creditCheck(final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		if (configurationService.getConfiguration().getBoolean(ENABLE_CREDIT_CHECK_API)){
			LLAApiResponse llaCreditCheckApiResponse = null;
			CartModel currentCart=cartService.getSessionCart();
			final CustomerModel customerModel = (CustomerModel) currentCart.getUser();
			try
			{
				//llaCreditCheckApiResponse = llaPersonalDetailService.updateCreditCheckDetails(customerssn);
				llaCreditCheckApiResponse = llaPersonalDetailService.updateCreditCheckDetails();
				return getViewPostCreditCheckOnServicableAddress(model, llaCreditCheckApiResponse, currentCart, customerModel);
			}
			catch (final LLAApiException exception)
			{
				LOG.error("Exception in Credit Check Response for Cart : " + cartService.getSessionCart().getCode() + " User : "+ customerModel.getUid());
				currentCart.setFalloutReasonValue(FalloutReasonEnum.ERROR);
				modelService.save(currentCart);
				triggerCreditCheckFailureScenario(model, customerModel, exception);
				return REDIRECT_TO_CALLBACK_URL;
			}
			

		}
		else
		{
			LOG.info("Skipping Credit Check API Call for puertorico");
			return getCheckoutStep().nextStep();
		}
	}

	/**
	 * Return View Post Credit Check on Service Address
	 * @param model
	 * @param llaCreditCheckApiResponse
	 * @param currentCart
	 * @param customerModel
	 * @return
	 */
	private String getViewPostCreditCheckOnServicableAddress(Model model, LLAApiResponse llaCreditCheckApiResponse, CartModel currentCart, CustomerModel customerModel) {
		if (null != llaCreditCheckApiResponse && llaCreditCheckApiResponse.getCreditScore() > 0)
		{
			model.addAttribute(CREDIT_CHECK_RESPONSE, llaCreditCheckApiResponse);
			return getCheckoutStep().nextStep();
		}
		else if(null != llaCreditCheckApiResponse && StringUtils.isNotEmpty(llaCreditCheckApiResponse.getErrorCode()) && NOT_FOUND.equalsIgnoreCase(llaCreditCheckApiResponse.getErrorCode()))
		{

			currentCart.setFalloutReasonValue(FalloutReasonEnum.CREDIT_CHECK_UNSUCCESSFUL);
			modelService.save(currentCart);
			final String houseKey= (Objects.nonNull(currentCart.getSelectedAddress()) && StringUtils.isNotEmpty(currentCart.getSelectedAddress().getHouseNo()) ? currentCart.getSelectedAddress().getHouseNo():StringUtils.EMPTY);
			llaCommonUtil.launchFalloutProcess(currentCart, ClickToCallReasonEnum.CREDIT, OriginSource.CHECKOUT, null, houseKey, customerModel.getCustomerSSN());
			return REDIRECT_TO_CALLBACK_URL;

		}
		else if(null != llaCreditCheckApiResponse && StringUtils.isNotEmpty(llaCreditCheckApiResponse.getErrorCode()) && MALFORMED_JSON_CODE.equalsIgnoreCase(llaCreditCheckApiResponse.getErrorCode()))
		{

			currentCart.setFalloutReasonValue(FalloutReasonEnum.ERROR);
			modelService.save(currentCart);
			LOG.info(String.format("Error parsing JSON for credit check API for response %s , error is %s", llaCreditCheckApiResponse.getMessage(), llaCreditCheckApiResponse.getDescription()));
			return REDIRECT_TO_ERROR_URL;

		}
		 else
		{
			LOG.error("Error in Credit Check Response for Cart : " + cartService.getSessionCart().getCode() + " User : "+ customerModel.getUid());
			currentCart.setFalloutReasonValue(FalloutReasonEnum.ERROR);
			modelService.save(currentCart);
			return REDIRECT_TO_ERROR_URL;
		}
	}

	@RequestMapping(value="/updateCreditCheckAddress", method=RequestMethod.POST)
	@ResponseBody
	@RequireHardLogIn
   public String updateCreditCheckAddress(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
		
		
			addressForm.setCountryIso(getSiteConfigService().getProperty("website.code"));
			final CartData cartData = getCheckoutFacade().getCheckoutCart();
			model.addAttribute(ATTR_CART_DATA, cartData);
			model.addAttribute(UPDATED_DELIVERY_ADDRESS, getDeliveryAddresses(cartData.getDeliveryAddress()));
			this.prepareDataForPage(model);
			storeAddresses(addressForm, model);
			if (bindingResult.hasErrors())
			{
				GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
				return getCheckoutStep().currentStep();
			}
			CustomerModel customerModel=(CustomerModel)cartService.getSessionCart().getUser();
			CartModel cartModel = cartService.getSessionCart();
			final AddressData newAddress = constructNewAddress(addressForm,customerModel);
			AddressModel addressModel = llaCheckoutFacade.setCreditCheckAddress(newAddress,cartModel);
			llaPersonalDetailService.updateSelectedAddress("CREDIT_ADDRESS");
			if (configurationService.getConfiguration().getBoolean(ENABLE_CREDIT_CHECK_API)){
   			LLAApiResponse llaCreditCheckApiResponse = null;
   			try {
   				llaCreditCheckApiResponse = llaPersonalDetailService.updateCreditCheckDetails();
				return getViewForUpdatedCreditAddress(model, customerModel, cartModel, llaCreditCheckApiResponse);
			} catch (LLAApiException exception) {
   				LOG.error("Exception in Credit Check Response for Cart : " + cartModel.getCode() +	" User : " + customerModel.getUid());
   				triggerCreditCheckFailureScenario(model,customerModel,  exception);
   				return  REDIRECT_TO_CALLBACK_URL;
   			}

		}else	{
			LOG.info("Skipping Credit Check API Call for puertorico");
			return getCheckoutStep().nextStep();
		}
	}

	/**
	 *  Return Next View Post Credit Check on Updated Address
	 * @param model
	 * @param customerModel
	 * @param cartModel
	 * @param llaCreditCheckApiResponse
	 * @return
	 */
	private String getViewForUpdatedCreditAddress(Model model, CustomerModel customerModel, CartModel cartModel, LLAApiResponse llaCreditCheckApiResponse) {
		if(null!= llaCreditCheckApiResponse && llaCreditCheckApiResponse.getCreditScore() >0){
			model.addAttribute(CREDIT_CHECK_RESPONSE, llaCreditCheckApiResponse);
			return getCheckoutStep().nextStep();
		}else if(null!= llaCreditCheckApiResponse && StringUtils.isNotEmpty(llaCreditCheckApiResponse.getErrorCode()) && NOT_FOUND.equalsIgnoreCase(llaCreditCheckApiResponse.getErrorCode()))
		{
			LOG.error("Error in Credit Check Response for Cart : " + cartService.getSessionCart().getCode() +	" User : " + customerModel.getUid());
		 cartModel.setFalloutReasonValue(FalloutReasonEnum.CREDIT_CHECK_UNSUCCESSFUL);
		 modelService.save(cartModel);
		 String houseKey=StringUtils.EMPTY;
		 if(Objects.nonNull(cartModel.getSelectedAddress())) {
			 houseKey = cartModel.getSelectedAddress().getHouseNo();
		 }

		 llaCommonUtil.launchFalloutProcess(cartModel, ClickToCallReasonEnum.CREDIT, OriginSource.CHECKOUT, null, houseKey, customerModel.getCustomerSSN());
		 return REDIRECT_TO_CALLBACK_URL;
		}
	 else if(null != llaCreditCheckApiResponse && StringUtils.isNotEmpty(llaCreditCheckApiResponse.getErrorCode()) && MALFORMED_JSON_CODE.equalsIgnoreCase(llaCreditCheckApiResponse.getErrorCode()))
	 {

		 cartModel.setFalloutReasonValue(FalloutReasonEnum.ERROR);
		 modelService.save(cartModel);
		 LOG.info(String.format("Error parsing JSON for credit check API for response %s , error is %s", llaCreditCheckApiResponse.getMessage(), llaCreditCheckApiResponse.getDescription()));
		 return ERROR;

	 }
	 else
	 {
		 LOG.error("Error in Credit Check Response for Cart : " + cartService.getSessionCart().getCode() + " User : "+ customerModel.getUid());
		 cartModel.setFalloutReasonValue(FalloutReasonEnum.ERROR);
		 modelService.save(cartModel);
		 return ERROR;
	 }
	}

	@RequestMapping(value="/cancel-credit-check", method=RequestMethod.GET)
	public String cancelCreditCheck( final Model model,
										   final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		CartModel cart = cartService.getSessionCart();
		if(Objects.nonNull(cart))
		{
			cart.setFalloutReasonValue(FalloutReasonEnum.CREDIT_CHECK_REFUSED);
			modelService.save(cart);
			modelService.refresh(cart);
			String houseKey= (Objects.nonNull(cart.getSelectedAddress())&& StringUtils.isNotEmpty(cart.getSelectedAddress().getHouseNo()))?cart.getSelectedAddress().getHouseNo():Strings.EMPTY;
		    CustomerModel customerModel=(CustomerModel)cartService.getSessionCart().getUser();
			LOG.info(String.format("Launching fallout process for cancel credit check selected by customer %s for cart %s ",customerModel.getUid(),cart.getCode()));
			llaCommonUtil.launchFalloutProcess(cart, ClickToCallReasonEnum.CREDIT, OriginSource.CHECKOUT, null, houseKey, customerModel.getCustomerSSN());

		}
		return getCheckoutStep().nextStep();
	}
	/**
	 * Trigger Credit Check Failure Scenario and Launch Fallout Process
	 * @param model
	 * @param customerModel
	 * @param exception
	 * @return
	 */
	private void triggerCreditCheckFailureScenario(Model model,CustomerModel customerModel, LLAApiException exception) {
		if(LOG.isDebugEnabled() && null!=exception){
			exception.printStackTrace();
		}
		CartModel currentCart=cartService.getSessionCart();
		final String houseKey=currentCart.getSelectedAddress().getHouseNo();
		llaCommonUtil.launchFalloutProcess(currentCart, ClickToCallReasonEnum.CREDIT, OriginSource.CHECKOUT, null, houseKey, customerModel.getCustomerSSN());

	}

	protected AddressData constructNewAddress(final AddressForm addressForm, final CustomerModel customerModel)
	{
		final AddressData newAddress = new AddressData();
		if(llaCommonUtil.isSitePuertorico()){
 		newAddress.setTitleCode(addressForm.getTitleCode());
 		newAddress.setFirstName(addressForm.getFirstName());
 		newAddress.setLastName(addressForm.getLastName());
 		newAddress.setPhone(addressForm.getPhone());
 		newAddress.setLine1(addressForm.getLine1());
 		newAddress.setTown(addressForm.getTownCity());
 		newAddress.setPostalCode(addressForm.getPostcode());
 		newAddress.setBillingAddress(false);
 		newAddress.setShippingAddress(true);
 		newAddress.setStreetname(addressForm.getStreetname());
 		newAddress.setAppartment(addressForm.getAppartment());
 		newAddress.setBuilding(addressForm.getBuilding());
 		newAddress.setStreetNo(addressForm.getStreetNumber());
 		newAddress.setBlock(addressForm.getBlock());
 		newAddress.setTypeOfResidence(addressForm.getTypeOfResidence());
 		newAddress.setArea(addressForm.getArea());
 		newAddress.setPlotNo(addressForm.getPlotNo());
 		newAddress.setKilometer(addressForm.getKilometer());
 		newAddress.setNeighbourhood(addressForm.getNeighbourhood());
 		newAddress.setDistrict(addressForm.getDistrict());
 		newAddress.setProvince(addressForm.getProvince());
 		newAddress.setServiceable(addressForm.getIsServiceable());
		}

		if (addressForm.getCountryIso() != null)
		{
			final CountryData countryData = getI18NFacade().getCountryForIsocode(addressForm.getCountryIso());
			newAddress.setCountry(countryData);
		}
		if (addressForm.getRegionIso() != null && !StringUtils.isEmpty(addressForm.getRegionIso()))
		{
			final RegionData regionData = getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso());
			newAddress.setRegion(regionData);
		}

		return newAddress;
	}

	protected void storeAddresses(final AddressForm addressForm, final Model model)
	{
		if (StringUtils.isNotBlank(addressForm.getCountryIso()))
		{
			model.addAttribute(ATTR_REGIONS, getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
			model.addAttribute(ATTR_COUNTRY, addressForm.getCountryIso());
		}
	}
	
	 protected void updatePageTitle(final Model model, final AbstractPageModel cmsPage)
    {
        storeContentPageTitleInModel(model, getPageTitleResolver().resolveHomePageTitle(cmsPage.getTitle()));
    }
	 
	 /**
     * Trigger Debt Check Call for Customer
     * @param form
     * @param model
     * @param request
     * @param redirectModel
     * @return
     */
	 private String triggerDebtCheckCallForLCPR(CustomerSSNForm form, Model model, HttpServletRequest request,
			 RedirectAttributes redirectModel)
	 {
		 CartModel cart = cartService.getSessionCart();
		 final CustomerModel currentCustomer = (CustomerModel) cart.getUser();
		 String houseKey = StringUtils.EMPTY;
		 try
		 {
			 final String ssnEncryptedValue = llaEncryptionUtil.doEncryption(form.getSsn().replaceAll("-", ""));
			 sessionService.setAttribute(SSN_ENCRYPTED_VALUE, ssnEncryptedValue);
			 LLAApiResponse llaDebtCheckApiResponse = llaMulesoftDebtCheckApiService.getDebtCheckForCustomer(cart,
					 ssnEncryptedValue);

			 //error page and call back page
			 if (Objects.nonNull(llaDebtCheckApiResponse))
			 {
				 return getViewPostDebtCheck(cart, currentCustomer, houseKey, llaDebtCheckApiResponse);
			 } else {     
   			 LOG.error(String.format("Null Debt Check Response for Customer :%s for Cart :%s", cart.getUser().getUid(), cart.getCode()));
             cart.setFalloutReasonValue(FalloutReasonEnum.ERROR);
             modelService.save(cart);
             return REDIRECT_TO_ERROR_URL;
			 }
		 }
		 catch (Exception exception)
		 {
			 LOG.error(String.format("Exception occured while making Debt Check API Call for LCPR::: %s", exception));
			 if (LOG.isDebugEnabled())
			 {
				 exception.printStackTrace();
			 }
			 return REDIRECT_TO_CALLBACK_URL;
		 }
	 }

	/**
	 * Get View Post Debt Check for LCPR
	 * @param cart
	 * @param currentCustomer
	 * @param houseKey
	 * @param llaDebtCheckApiResponse
	 * @return
	 */
	private String getViewPostDebtCheck(CartModel cart, CustomerModel currentCustomer, String houseKey, LLAApiResponse llaDebtCheckApiResponse) {
		if (Boolean.valueOf(llaDebtCheckApiResponse.isDebtBalanceFlag()))
		{
			LOG.error(String.format("Customer has debt balance for Cart :%s for User :%s", cart.getCode(), cart.getUser().getUid()));
			cart.setDebtCheckFlag(Boolean.FALSE);
			modelService.save(cart);
			modelService.refresh(cart);
			if (Objects.nonNull(cart.getSelectedAddress()) && StringUtils.isNotEmpty(cart.getSelectedAddress().getHouseNo()))
			{
				houseKey = cart.getSelectedAddress().getHouseNo();
			}
			cart.setFalloutReasonValue(FalloutReasonEnum.CUSTOMER_WITH_DEBT);
			modelService.save(cart);
			modelService.refresh(cart);
			llaCommonUtil.launchFalloutProcess(cart, ClickToCallReasonEnum.DEBT, OriginSource.CHECKOUT, null, houseKey,
					currentCustomer.getCustomerSSN());
			return REDIRECT_TO_CALLBACK_URL;

		}
		else if (StringUtils.isNotEmpty(llaDebtCheckApiResponse.getErrorCode())
				&& !llaDebtCheckApiResponse.getErrorCode().equals("404"))
		{
			LOG.error(String.format("Error in Debt Check Response for Cart :%s for User :%s", cart.getCode(), cart.getUser().getUid()));
			cart.setFalloutReasonValue(FalloutReasonEnum.ERROR);
			modelService.save(cart);
			modelService.refresh(cart);
			return REDIRECT_TO_ERROR_URL;
		}
		else if (StringUtils.isNotEmpty(llaDebtCheckApiResponse.getErrorCode())
				&& MALFORMED_JSON_CODE.equalsIgnoreCase(llaDebtCheckApiResponse.getErrorCode()))
		{
			cart.setFalloutReasonValue(FalloutReasonEnum.ERROR);
			modelService.save(cart);
			modelService.refresh(cart);
			LOG.info(String.format("Error parsing JSON for servicability API for response %s , error is %s",
					llaDebtCheckApiResponse.getMessage(), llaDebtCheckApiResponse.getDescription()));
			return REDIRECT_TO_ERROR_URL;
		}
		else
		{
			LOG.info(String.format("Zero Debt Found for Customer :%s for Cart :%s", cart.getUser().getUid(), cart.getCode()));
			LOG.info("Moving to Next Step");
			cart.setDebtCheckFlag(Boolean.TRUE);
			modelService.save(cart);
			modelService.refresh(cart);
			return SUCCESS;
		}
	}


}
