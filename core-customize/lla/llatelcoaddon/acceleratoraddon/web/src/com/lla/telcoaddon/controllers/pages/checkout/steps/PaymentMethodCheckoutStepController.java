/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcoaddon.controllers.pages.checkout.steps;


import com.lla.core.util.LLACommonUtil;
import com.lla.facades.address.LLAAddressFacade;
import com.lla.facades.address.ProvinceData;
import com.lla.facades.checkout.LLACheckoutFacade;
import com.lla.facades.product.data.ProductRegionData;
import com.lla.mulesoft.integration.service.LLAMulesoftStubApiService;
import com.lla.telcoaddon.forms.SopPaymentDetailsForm;
import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.acceleratorservices.payment.constants.PaymentConstants;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.PaymentDetailsForm;
import com.lla.telcoaddon.controllers.ControllerConstants;
import com.lla.telcoaddon.controllers.util.CartHelper;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CardTypeData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CashOnDeliveryPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.OnlinePaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PayInStorePaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.subscriptionfacades.SubscriptionFacade;
import de.hybris.platform.subscriptionfacades.data.SubscriptionPaymentData;
import de.hybris.platform.subscriptionfacades.exceptions.SubscriptionFacadeException;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import de.hybris.platform.util.Config;
import org.apache.commons.lang.StringUtils;
import org.llapayment.payment.LLaPaymentCheckoutFacadeImpl;
import org.llapayment.service.LlapaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping(value = "/checkout/multi/payment-method")
public class PaymentMethodCheckoutStepController extends AbstractCheckoutStepController
{
	private static final Logger LOG = LoggerFactory.getLogger(PaymentMethodCheckoutStepController.class);

	protected static final Map<String, String> SOPPAYMENTCARDTYPES = new HashMap<String, String>();
	protected static final String PAYMENT_METHOD = "payment-method";
	protected static final String ATTR_CART_DATA = "cartData";
	private final String[] DISALLOWED_FIELDS = new String[] {};
	private static final boolean STUBS_ENABLED = Config.getBoolean("stubs.api.enabled", false);


	@Resource(name = "cartService")
	private CartService cartService;

	@Autowired
	private CMSSiteService cmsSiteService;

	@Resource(name = "modelService")
	private ModelService modelService;
	@Autowired
	private LLACommonUtil llaCommonUtil;

	@Autowired
	private LLaPaymentCheckoutFacadeImpl llaPaymentCheckoutFacadeImpl;

	@Autowired
	private CustomerAccountService customerAccountService;

	@Autowired
	private LLAMulesoftStubApiService llaMulesoftStubApiService;
	@Autowired
	private LlapaymentService llapaymentService;

	@Autowired
	private LLAAddressFacade llaAddressFacade;
	@ModelAttribute("provinceList")
	public List<ProvinceData> getProvince()
	{
		return llaCommonUtil.isSiteCabletica()?llaAddressFacade.getAllProvince(): new ArrayList<ProvinceData>();
	}
	@Resource(name = "subscriptionFacade")
	private SubscriptionFacade subscriptionFacade;
	@Resource(name = "checkoutFacade")
	private LLACheckoutFacade llaCheckoutFacade;

	@ModelAttribute("billingCountries")
	public Collection<CountryData> getBillingCountries()
	{
		return getCheckoutFacade().getBillingCountries();
	}

	@ModelAttribute("cardTypes")
	public Collection<CardTypeData> getCardTypes()
	{
		return getCheckoutFacade().getSupportedCardTypes();
	}


	@ModelAttribute("areas")
	public List<ProductRegionData> getAreaList()
	{
		return  llaCommonUtil.isSiteJamaica()?llaCommonUtil.getJamaicaRegions(): Collections.emptyList();
	}


	@ModelAttribute("months")
	public List<SelectOption> getMonths()
	{
		final List<SelectOption> months = new ArrayList<SelectOption>();

		months.add(new SelectOption("01", "01"));
		months.add(new SelectOption("02", "02"));
		months.add(new SelectOption("03", "03"));
		months.add(new SelectOption("04", "04"));
		months.add(new SelectOption("05", "05"));
		months.add(new SelectOption("06", "06"));
		months.add(new SelectOption("07", "07"));
		months.add(new SelectOption("08", "08"));
		months.add(new SelectOption("09", "09"));
		months.add(new SelectOption("10", "10"));
		months.add(new SelectOption("11", "11"));
		months.add(new SelectOption("12", "12"));

		return months;
	}

	@ModelAttribute("startYears")
	public List<SelectOption> getStartYears()
	{
		final List<SelectOption> startYears = new ArrayList<SelectOption>();
		final Calendar calender = new GregorianCalendar();

		for (int i = calender.get(Calendar.YEAR); i > (calender.get(Calendar.YEAR) - 6); i--)
		{
			startYears.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
		}

		return startYears;
	}

	@ModelAttribute("expiryYears")
	public List<SelectOption> getExpiryYears()
	{
		final List<SelectOption> expiryYears = new ArrayList<SelectOption>();
		final Calendar calender = new GregorianCalendar();

		for (int i = calender.get(Calendar.YEAR); i < (calender.get(Calendar.YEAR) + 11); i++)
		{
			expiryYears.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
		}

		return expiryYears;
	}

	/**
	 * Required to initiate a subscription facade call.
	 *
	 * @return
	 */
	protected String getCurrentClientIpAddress()
	{
		final ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		final HttpServletRequest request = sra.getRequest();
		return request.getRemoteAddr();
	}

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateCheckoutStep(checkoutStep = PAYMENT_METHOD)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{

		if((llaCommonUtil.isSitePanama() && llaCommonUtil.hidePaymentMethod(cartService.getSessionCart())) || (llaCommonUtil.isSiteJamaica() && llaCommonUtil.hidePaymentSectionForJamaica()))
		return getCheckoutStep().nextStep();
		setupAddPaymentPage(model);
		final String siteId=cmsSiteService.getCurrentSite().getUid();
		if(STUBS_ENABLED)
		{
			llaMulesoftStubApiService.internalCreditCheck(siteId);
		}
		if(llaCommonUtil.isSiteJamaica()) {
			String errorMessage=getSessionService().getAttribute("paymentDeclined");
			if(StringUtils.isNotEmpty( errorMessage)){
				GlobalMessages.addErrorMessage(model, errorMessage);
			}
			getSessionService().removeAttribute("paymentDeclined");
		}
		// Use the checkout PCI strategy for getting the URL for creating new subscriptions.
		final CheckoutPciOptionEnum subscriptionPciOption = getCheckoutFlowFacade().getSubscriptionPciOption();
		String view = null;
		if(llaCommonUtil.isSitePanama() && llaCommonUtil.isPrepaidCart()){
			setupNonHopSopPage(model);
			view = ControllerConstants.Views.Pages.MultiStepCheckout.SilentOrderPostPage;
		}
		else if (CheckoutPciOptionEnum.HOP.equals(subscriptionPciOption))
		{
			// Redirect the customer to the HOP page or show error message if it fails (e.g. no HOP configurations).
			try
			{
				setupHopPage(model);
				view = ControllerConstants.Views.Pages.MultiStepCheckout.HostedOrderPostPage;
			}
			catch (final Exception e)
			{
				LOG.error("Failed to build beginCreateSubscription request", e);
				GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.addPaymentDetails.generalError");
			}
		}
		else if (CheckoutPciOptionEnum.SOP.equals(subscriptionPciOption))
		{
			// Build up the SOP form data and render page containing form
			try
			{
				final SopPaymentDetailsForm sopPaymentDetailsForm = new SopPaymentDetailsForm();
				setupSilentOrderPostPage(sopPaymentDetailsForm, model);
				view = ControllerConstants.Views.Pages.MultiStepCheckout.SilentOrderPostPage;
			}
			catch (final SubscriptionFacadeException e)
			{
				LOG.error("Failed to build beginCreateSubscription request", e);
				GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.addPaymentDetails.generalError");
			}
		}
		if(STUBS_ENABLED)
		{
			llaMulesoftStubApiService.servicibilityCheck(siteId);
		}
		if (view == null) //probably because an exception happened
		{
			// If not using HOP or SOP we need to build up the payment details form
			setupNonHopSopPage(model);
			view = ControllerConstants.Views.Pages.MultiStepCheckout.SilentOrderPostPage;
		}
		if(STUBS_ENABLED)
		{
			llaMulesoftStubApiService.externalCreditCheck(siteId);
		}
		return view;
	}

	protected void setupNonHopSopPage(final Model model)
	{
		final PaymentDetailsForm paymentDetailsForm = new PaymentDetailsForm();
		final AddressForm addressForm = new AddressForm();
		paymentDetailsForm.setBillingAddress(addressForm);
		model.addAttribute(paymentDetailsForm);

		final CartData cartData = llaCheckoutFacade.getCheckoutCart();
		cartData.setEntries(CartHelper.removeEmptyEntries(cartData.getEntries()));
		model.addAttribute(ATTR_CART_DATA, cartData);
		setCheckoutStepLinksForModel(model, getCheckoutStep());
		if (llaCommonUtil.isSitePanama() && null != cartData)
		{
			String allowedPaymentMethods = getSiteConfigService().getProperty("payment.method.allowed.panama");
			if(llaCommonUtil.isPrepaidCart()){
				allowedPaymentMethods = getSiteConfigService().getProperty("payment.method.allowed.prepaid.homeDelivery.panama");
			}
			else if (cartData.getDeliveryMode() != null && cartData.getDeliveryMode().getCode().equalsIgnoreCase("home-delivery"))
			{
				allowedPaymentMethods = getSiteConfigService().getProperty("payment.method.allowed.homeDelivery.panama");
			}
			else if (cartData.getDeliveryMode() != null && cartData.getDeliveryMode().getCode().equalsIgnoreCase("pickup"))
			{
				allowedPaymentMethods = getSiteConfigService().getProperty("payment.method.allowed.storePickUp.panama");
			}
			List<String> paymentMethodList = paymentMethodList = Stream.of(allowedPaymentMethods.split(",", -1)).collect(Collectors.toList());
			model.addAttribute("allowedPaymentModes", paymentMethodList);
		}
	}

	protected void setupHopPage(final Model model)
	{
		final PaymentData hostedOrderPageData = getPaymentFacade().beginHopCreateSubscription("/checkout/multi/hop/response",
				"/integration/merchant_callback");
		model.addAttribute("hostedOrderPageData", hostedOrderPageData);

		final boolean hopDebugMode = getSiteConfigService().getBoolean(PaymentConstants.PaymentProperties.HOP_DEBUG_MODE, false);
		model.addAttribute("hopDebugMode", Boolean.valueOf(hopDebugMode));
	}

	@RequestMapping(value =
			{ "/setPaymentType" }, method = RequestMethod.POST)
	@RequireHardLogIn
	public String setPaymentType(final Model model, final PaymentDetailsForm paymentDetailsForm, final BindingResult bindingResult)
			throws CMSItemNotFoundException {

		llapaymentService.setPaymentMode(paymentDetailsForm.getPaymentMethod());
		return getCheckoutStep().nextStep();
	}
	@RequestMapping(value =
			{ "/add" }, method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(final Model model, final PaymentDetailsForm paymentDetailsForm, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		getPaymentDetailsValidator().validate(paymentDetailsForm, bindingResult);
		setupAddPaymentPage(model);

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		cartData.setEntries(CartHelper.removeEmptyEntries(cartData.getEntries()));
		model.addAttribute(ATTR_CART_DATA, cartData);

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.paymentethod.formentry.invalid");
			return ControllerConstants.Views.Pages.MultiStepCheckout.SilentOrderPostPage;
		}

		final CCPaymentInfoData paymentInfoData = populatePaymentDetails(paymentDetailsForm);

		final AddressData addressData = createAddressData(paymentDetailsForm);
		if (addressData == null)
		{
			GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.createSubscription.billingAddress.noneSelectedMsg");
			return ControllerConstants.Views.Pages.MultiStepCheckout.SilentOrderPostPage;
		}

		getAddressVerificationFacade().verifyAddressData(addressData);
		paymentInfoData.setBillingAddress(addressData);

		final CCPaymentInfoData newPaymentSubscription = getCheckoutFacade().createPaymentSubscription(paymentInfoData);
		if (newPaymentSubscription != null && StringUtils.isNotBlank(newPaymentSubscription.getSubscriptionId()))
		{
			if (Boolean.TRUE.equals(paymentDetailsForm.getSaveInAccount()) && getUserFacade().getCCPaymentInfos(true).size() <= 1)
			{
				getUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
			}
			getCheckoutFacade().setPaymentDetails(newPaymentSubscription.getId());
		}
		else
		{
			GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.createSubscription.failedMsg");
			return ControllerConstants.Views.Pages.MultiStepCheckout.SilentOrderPostPage;
		}

		model.addAttribute("paymentId", newPaymentSubscription.getId());
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return getCheckoutStep().nextStep();
	}

	protected AddressData createAddressData(final PaymentDetailsForm paymentDetailsForm)
	{
		final AddressData addressData;
		if (Boolean.FALSE.equals(paymentDetailsForm.getNewBillingAddress()))
		{
			addressData = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();

			addressData.setBillingAddress(true); // mark this as billing address
		}
		else
		{
			final AddressForm addressForm = paymentDetailsForm.getBillingAddress();
			addressData = new AddressData();
			if (addressForm != null)
			{
				addressData.setId(addressForm.getAddressId());
				addressData.setTitleCode(addressForm.getTitleCode());
				addressData.setFirstName(addressForm.getFirstName());
				addressData.setLastName(addressForm.getLastName());
				addressData.setLine1(addressForm.getLine1());
				addressData.setLine2(addressForm.getLine2());
				addressData.setTown(addressForm.getTownCity());
				addressData.setPostalCode(addressForm.getPostcode());
				addressData.setCountry(getI18NFacade().getCountryForIsocode(addressForm.getCountryIso()));
				if (addressForm.getRegionIso() != null)
				{
					addressData.setRegion(getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso()));
				}

				addressData.setShippingAddress(Boolean.TRUE.equals(addressForm.getShippingAddress()));
				addressData.setBillingAddress(Boolean.TRUE.equals(addressForm.getBillingAddress()));
			}
		}
		return addressData;
	}

	protected CCPaymentInfoData populatePaymentDetails(final PaymentDetailsForm paymentDetailsForm)
	{
		final CCPaymentInfoData paymentInfoData = new CCPaymentInfoData();
		paymentInfoData.setId(paymentDetailsForm.getPaymentId());
		paymentInfoData.setCardType(paymentDetailsForm.getCardTypeCode());
		paymentInfoData.setAccountHolderName(paymentDetailsForm.getNameOnCard());
		paymentInfoData.setCardNumber(paymentDetailsForm.getCardNumber());
		paymentInfoData.setStartMonth(paymentDetailsForm.getStartMonth());
		paymentInfoData.setStartYear(paymentDetailsForm.getStartYear());
		paymentInfoData.setExpiryMonth(paymentDetailsForm.getExpiryMonth());
		paymentInfoData.setExpiryYear(paymentDetailsForm.getExpiryYear());
		if (Boolean.TRUE.equals(paymentDetailsForm.getSaveInAccount()) || getCheckoutCustomerStrategy().isAnonymousCheckout())
		{
			paymentInfoData.setSaved(true);
		}
		paymentInfoData.setIssueNumber(paymentDetailsForm.getIssueNumber());
		return paymentInfoData;
	}


	@RequestMapping(value = "/remove", method = RequestMethod.POST)
	@RequireHardLogIn
	public String remove(@RequestParam(value = "paymentInfoId") final String paymentMethodId,
						 final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		getUserFacade().unlinkCCPaymentInfo(paymentMethodId);
		GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
				"text.account.profile.paymentCart.removed");
		return getCheckoutStep().currentStep();
	}

	/**
	 * This method gets called when the "Use These Payment Details" button is clicked. It sets the selected payment
	 * method on the checkout facade and reloads the page highlighting the selected payment method.
	 *
	 * @param selectedPaymentMethodId
	 *           - the id of the payment method to use.
	 * @return - a URL to the page to load.
	 */
	@RequestMapping(value = "/choose", method = RequestMethod.GET)
	@RequireHardLogIn
	public String doSelectPaymentMethod(@RequestParam("selectedPaymentMethodId") final String selectedPaymentMethodId,final Model model,
										final RedirectAttributes redirectModel)
	{
		if (StringUtils.isNotBlank(selectedPaymentMethodId))
		{
			if(!(getCheckoutFacade().setPaymentDetails(selectedPaymentMethodId))){
				GlobalMessages.addErrorMessage(model, "checkout.error.authorization.failed");
				return getCheckoutStep().currentStep();
			}
		}
		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected CardTypeData createCardTypeData(final String code, final String name)
	{
		final CardTypeData cardTypeData = new CardTypeData();
		cardTypeData.setCode(code);
		cardTypeData.setName(name);
		return cardTypeData;
	}

	protected void setupAddPaymentPage(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("metaRobots", "noindex,nofollow");
		model.addAttribute("hasNoPaymentInfo", Boolean.valueOf(getCheckoutFlowFacade().hasNoPaymentInfo()));
		prepareDataForPage(model);
		if(!llaCommonUtil.isSiteJamaica()) {
			model.addAttribute(WebConstants.BREADCRUMBS_KEY,
					getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.paymentMethod.breadcrumb"));
		}
		final ContentPageModel contentPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		setCheckoutStepLinksForModel(model, getCheckoutStep());
	}

	protected void setupSilentOrderPostPage(final SopPaymentDetailsForm sopPaymentDetailsForm, final Model model)
			throws SubscriptionFacadeException
	{
		model.addAttribute("sopPaymentDetailsForm", sopPaymentDetailsForm);
		try
		{
			final PaymentData silentOrderPageData = getPaymentFacade().beginSopCreateSubscription("/checkout/multi/sop/response",
					"/integration/merchant_callback");
			model.addAttribute("silentOrderPageData", silentOrderPageData);
			sopPaymentDetailsForm.setParameters(silentOrderPageData.getParameters());
			model.addAttribute("paymentFormUrl", silentOrderPageData.getPostUrl());

			initialiseSubscriptionTransaction(silentOrderPageData);


		}
		catch (final IllegalArgumentException e)
		{
			model.addAttribute("paymentFormUrl", "");
			model.addAttribute("silentOrderPageData", null);
			LOG.warn("Failed to set up silent order post page ", e);
			GlobalMessages.addErrorMessage(model, "checkout.multi.sop.globalError");
		}
		final CartData cartData = llaCheckoutFacade.getCheckoutCart();
		cartData.setEntries(CartHelper.removeEmptyEntries(cartData.getEntries()));
		model.addAttribute("silentOrderPostForm", new PaymentDetailsForm());
		model.addAttribute(ATTR_CART_DATA, cartData);
		model.addAttribute("deliveryAddress", cartData.getDeliveryAddress());
		model.addAttribute("installationAddress", cartData.getInstallationAddress());
		model.addAttribute("paymentInfos", getUserFacade().getCCPaymentInfos(true));
		model.addAttribute("sopCardTypes", getSopCardTypes());
		if (StringUtils.isNotBlank(sopPaymentDetailsForm.getBillTo_country()))
		{
			model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(sopPaymentDetailsForm.getBillTo_country()));
			model.addAttribute("country", sopPaymentDetailsForm.getBillTo_country());
		}
	}

	/**
	 * @param silentOrderPageData
	 * @throws SubscriptionFacadeException
	 */
	protected void initialiseSubscriptionTransaction(final PaymentData silentOrderPageData) throws SubscriptionFacadeException
	{
		final String sopResponseUrl = silentOrderPageData.getPostUrl();
		Assert.notNull(sopResponseUrl, "SOP Response URL may not be null");
		final String subscriptionRestUrl = getSubscriptionFacade().hpfUrl();
		Assert.notNull(subscriptionRestUrl, "Subscription Rest URL may not be null");

		final String clientIpAddress = getCurrentClientIpAddress();
		final SubscriptionPaymentData initResult = getSubscriptionFacade().initializeTransaction(clientIpAddress,
				subscriptionRestUrl, sopResponseUrl, new HashMap<String, String>());
		final String sessionToken = initResult.getParameters().get("sessionTransactionToken");

		Assert.notNull(sessionToken, "Session token may not be null");

		getSessionService().setAttribute("authorizationRequestId", clientIpAddress);
		getSessionService().setAttribute("authorizationRequestToken", sessionToken);
	}

	protected Collection<CardTypeData> getSopCardTypes()
	{
		final Collection<CardTypeData> sopCardTypes = new ArrayList<CardTypeData>();

		final List<CardTypeData> supportedCardTypes = getCheckoutFacade().getSupportedCardTypes();
		for (final CardTypeData supportedCardType : supportedCardTypes)
		{
			// Add credit cards for all supported cards that have mappings for cybersource SOP
			if (SOPPAYMENTCARDTYPES.containsKey(supportedCardType.getCode()))
			{
				sopCardTypes
						.add(createCardTypeData(SOPPAYMENTCARDTYPES.get(supportedCardType.getCode()), supportedCardType.getName()));
			}
		}
		return sopCardTypes;
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(PAYMENT_METHOD);
	}

	/**
	 * @return the subscriptionFacade
	 */
	protected SubscriptionFacade getSubscriptionFacade()
	{
		return subscriptionFacade;
	}

	/**
	 * @param subscriptionFacade
	 *           the subscriptionFacade to set
	 */
	protected void setSubscriptionFacade(final SubscriptionFacade subscriptionFacade)
	{
		this.subscriptionFacade = subscriptionFacade;
	}

	static
	{
		// Map hybris card type to Cybersource SOP credit card
		SOPPAYMENTCARDTYPES.put("visa", "001");
		SOPPAYMENTCARDTYPES.put("master", "002");
	}

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		binder.setDisallowedFields(DISALLOWED_FIELDS);
	}

	@RequestMapping(value = "/saveCVV", method = RequestMethod.POST)
	@RequireHardLogIn
	public String saveCVVIntoCartModel(@RequestParam("cvvNumber") final String cvvNumber) {
		final CartModel cartModel = cartService.getSessionCart();
		cartModel.setCartDataCVV(cvvNumber);
		modelService.save(cartModel);
		modelService.refresh(cartModel);
		return "TRUE";
	}
}
