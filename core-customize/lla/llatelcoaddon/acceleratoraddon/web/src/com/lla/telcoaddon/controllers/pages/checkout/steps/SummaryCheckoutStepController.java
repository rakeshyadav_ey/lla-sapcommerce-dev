/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcoaddon.controllers.pages.checkout.steps;

import com.firstatlanticcommerce.schemas.gateway.Authorize3DSResponse;
import com.lla.core.service.order.LLACommerceCheckoutService;
import com.lla.core.util.LLACommonUtil;
import com.lla.facades.cart.LLACartFacade;
import com.lla.facades.checkout.LLACheckoutFacade;
import com.lla.facades.checkout.LLACheckoutFlowFacade;
import com.lla.llapayment.LLAPaymentForm;
import com.lla.telcoaddon.controllers.ControllerConstants;
import com.lla.telcoaddon.controllers.util.CartHelper;
import com.lla.telcoaddon.forms.PlaceOrderForm;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.commons.lang.StringUtils;
import org.llapayment.client.LLAPaymentClient;
import org.llapayment.payment.LLaPaymentCheckoutFacadeImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Checkout controller.
 */
@Controller
@RequestMapping(value = "/checkout/multi/summary")
public class SummaryCheckoutStepController extends AbstractCheckoutStepController
{
	protected static final String REDIRECT_URL_PAYMENT_METHOD = REDIRECT_PREFIX + "/checkout/multi/payment-method/add";
	private static final Logger LOG = LoggerFactory.getLogger(SummaryCheckoutStepController.class);
	public static final String PAGE_TYPE = "pageType";
	@Autowired
	private CartService cartService;

	@Autowired
	private LLAPaymentClient llaPaymentClient;

	@Autowired
	private CustomerAccountService customerAccountService;

	@Autowired
	private BaseStoreService baseStoreService;
	@Resource(name = "checkoutFacade")
	private LLACheckoutFacade llaCheckoutFacade;

	@Autowired
	private ModelService modelService;

	@Autowired
	private LLACommonUtil llaCommonUtil;

	@Autowired
	private LLaPaymentCheckoutFacadeImpl llaPaymentCheckoutFacadeImpl;

	private static final String SUMMARY = "summary";
	private static final String ATTR_ADDRESS_FORM = "addressForm";
	private final String[] DISALLOWED_FIELDS = new String[] {};
	private static final String FINALIZER_ORDER ="Finalizar orden";
	private static final String CHECKOUT="Checkout";


	@Resource(name = "checkoutFlowFacade")
	private LLACheckoutFlowFacade llaCheckoutFlowFacade;
	@Resource(name="cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "siteConfigService")
	private SiteConfigService siteConfigService;
	
	@Autowired
	private LLACommerceCheckoutService llaCommerceCheckoutService;
	@Autowired
	private LLACartFacade llaCartFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;


	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	@PreValidateCheckoutStep(checkoutStep = SUMMARY)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
	   	final CartData cartData = llaCheckoutFacade.getCheckoutCart();
		cartData.setEntries(CartHelper.removeEmptyEntries(cartData.getEntries()));
		if (cartData.getEntries() != null && !cartData.getEntries().isEmpty())
		{
			for (final OrderEntryData entry : cartData.getEntries())
			{
				if (entry.getProduct() != null)
				{
					final String productCode = entry.getProduct().getCode();
					final ProductData product = getProductFacade().getProductForCodeAndOptions(productCode,
							Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.CATEGORIES));
					entry.setProduct(product);
				}
			}
		}
		prepareDataForPage(model);
		model.addAttribute("cartData", cartData);
		model.addAttribute("allItems", cartData.getEntries());
		model.addAttribute("deliveryAddress", cartData.getDeliveryAddress());
		model.addAttribute("installationAddress", cartData.getInstallationAddress());
		model.addAttribute("deliveryMode", cartData.getDeliveryMode());
		model.addAttribute("paymentInfo", cartData.getPaymentInfo());
		model.addAttribute("selectedPlanData",llaCartFacade.getSelectedPlanInOrder());
		// Only request the security code if the SubscriptionPciOption is set to Default.
		final boolean requestSecurityCode = CheckoutPciOptionEnum.DEFAULT
				.equals(getCheckoutFlowFacade().getSubscriptionPciOption());
		model.addAttribute("requestSecurityCode", Boolean.valueOf(requestSecurityCode));

		model.addAttribute(new PlaceOrderForm());

		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		if(!llaCommonUtil.isSiteJamaica())
		{
			if(llaCommonUtil.isSitePuertorico()){
				List<Breadcrumb> breadcrumbList= getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.summary.breadcrumb");
				breadcrumbList.removeIf(s->s.getName().equalsIgnoreCase(FINALIZER_ORDER) || s.getName().equalsIgnoreCase(CHECKOUT));
				model.addAttribute(WebConstants.BREADCRUMBS_KEY,breadcrumbList);
				CartModel cart=cartService.getSessionCart();
				model.addAttribute("hideCreditCheck",cart.getIsRequiredServiceActive() || null != cart.getFalloutReasonValue());
			}
			else{
			model.addAttribute(WebConstants.BREADCRUMBS_KEY,
					getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.summary.breadcrumb"));
			}
		}

		if (llaCommonUtil.isSitePuertorico()) {
			model.addAttribute(PAGE_TYPE, PageType.ORDERSUMMARY);
		}
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());
		return ControllerConstants.Views.Pages.MultiStepCheckout.CheckoutSummaryPage;
	}

	/**
	 * Place order form.
	 *
	 * @param placeOrderForm
	 * @param model
	 * @param redirectModel
	 * @return
	 * @throws CMSItemNotFoundException
	 * @throws InvalidCartException
	 * @throws CommerceCartModificationException
	 */

	@RequestMapping(value = "/placeOrder")
	@RequireHardLogIn
	public String placeOrder(@ModelAttribute("placeOrderForm") final PlaceOrderForm placeOrderForm, final Model model,
							 final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException, InvalidCartException, CommerceCartModificationException
	{
		if (validateOrderForm(placeOrderForm, model))
		{
			return enterStep(model, redirectModel);
		}

		//Validate the cart
		if (validateCart(redirectModel))
		{
			// Invalid cart. Bounce back to the cart page.
			return REDIRECT_PREFIX + "/cart";
		}
		CartData cartData = getCartFacade().getSessionCart();
		CartModel cartModel = cartService.getSessionCart();
		final String cartCode= cartModel.getCode();
		final OrderData orderData;
		if(llaCommonUtil.isSiteJamaica() && cartData.getEntries().size() > 1){
			cartModel.setIsFMCBundle(true);
			modelService.save(cartModel);
			modelService.refresh(cartModel);
		}
		// authorize, if failure occurs don't allow to place the order
		if(cartModel.getTotalPrice() != 0 && ((siteConfigService.getBoolean("payment.enabled", false) && llaCommonUtil.isSiteJamaica()) || (siteConfigService.getBoolean("payment.enabled", false) && llaCommonUtil.isSiteCabletica())))
		{
			Authorize3DSResponse authorizeResponse = llaPaymentClient.createAuthRequest(cartData);
			if(null != authorizeResponse && null!= authorizeResponse.getAuthorize3DSResult() && null!= authorizeResponse.getAuthorize3DSResult().getValue()
					&& null!=authorizeResponse.getAuthorize3DSResult().getValue() && null!=authorizeResponse.getAuthorize3DSResult().getValue().getResponseCode()
					 && authorizeResponse.getAuthorize3DSResult().getValue().getResponseCode().getValue().equals("0"))
			{
				String htmlFormData = authorizeResponse.getAuthorize3DSResult().getValue().getHTMLFormData().getValue();
				model.addAttribute("htmlFormData",htmlFormData);
				//LOG.info(String.format("HTML Form Data for Cart code :: %s ::: %s",cartCode,htmlFormData));
				return ControllerConstants.Views.Pages.MultiStepCheckout.ThreeDSPostPage;
			}else {
				LOG.error("Invalid response code received from FAC ");
				GlobalMessages.addErrorMessage(model, "checkout.error.authorization.failed");
				return enterStep(model, redirectModel);
			}
		}else
		{
			try
			{
				orderData = llaCheckoutFacade.placeOrder();
			}
			catch (final InvalidCartException e)
			{
				LOG.error("Failed to place Order", e);
				GlobalMessages.addErrorMessage(model, "checkout.placeOrder.failed");
				return enterStep(model, redirectModel);
			}
			saveOrderDetails(cartCode, orderData);
			if (llaCommonUtil.isSiteCabletica() && null != sessionService.getAttribute(ATTR_ADDRESS_FORM)) {
				sessionService.removeAttribute(ATTR_ADDRESS_FORM);
			}
			return redirectToOrderConfirmationPage(orderData);
		}

	}

	@RequestMapping(value = "/threedsresponse", method = RequestMethod.POST)
	@RequireHardLogIn
	public String threedsForm(final HttpServletRequest request, final HttpServletResponse response, final Model model,
							  final RedirectAttributes redirectModel)throws CMSItemNotFoundException, InvalidCartException, CommerceCartModificationException
	{
		Map<String,String> resultMap = getRequestParameterMap(request);
		//LOG.info(String.format("Result Map After 3ds Response %s",resultMap));
		LLAPaymentForm formData = new LLAPaymentForm();
		CartModel cartModel = cartService.getSessionCart();
		final String cartCode=cartModel.getCode();
		if(null != resultMap)
		{
			populateThreeDSFormData(formData,resultMap);
			CreditCardPaymentInfoModel cardPaymentInfoModel=(CreditCardPaymentInfoModel) cartModel.getPaymentInfo();
			cardPaymentInfoModel.setSubscriptionId(formData.getReferenceNo());
			cardPaymentInfoModel.setNumber(formData.getPaddedCardNo());
		}
		if(org.apache.commons.lang3.StringUtils.isNotEmpty(formData.getResponseCode()) && (formData.getResponseCode().equals("2") || formData.getResponseCode().equals("3")))
		{
			sessionService.setAttribute("paymentDeclined", "checkout.error.authorization.declined");
			GlobalMessages.addErrorMessage(model, "checkout.error.authorization.failed");
			return getPaymentRedirect(model, redirectModel);
		}
		if (!authorizePayment(formData))
		{
			sessionService.setAttribute("paymentDeclined", "checkout.error.authorization.declined");
			GlobalMessages.addErrorMessage(model, "checkout.error.authorization.failed");
			return getPaymentRedirect(model, redirectModel);
		}
		final OrderData orderData;
		try
		{
			orderData = getCheckoutFacade().placeOrder();
		}
		catch (final InvalidCartException e)
		{
			LOG.error("Failed to place Order", e);
			GlobalMessages.addErrorMessage(model, "checkout.placeOrder.failed");
			sessionService.setAttribute("paymentDeclined", "checkout.placeOrder.failed");
			return getPaymentRedirect(model, redirectModel);
		}

		saveOrderDetails(cartCode, orderData);
		return redirectToOrderConfirmationPage(orderData);
	}

	private String getPaymentRedirect(Model model, RedirectAttributes redirectModel) throws CMSItemNotFoundException {
		if(llaCommonUtil.isSiteJamaica())
			return REDIRECT_URL_PAYMENT_METHOD;
		else
			return enterStep(model, redirectModel);
	}

	private void saveOrderDetails(String cartCode, OrderData orderData) {
		OrderModel orderModel=customerAccountService.getOrderForCode(orderData.getCode(),baseStoreService.getCurrentBaseStore());
		if(llaCommonUtil.isSiteJamaica()){
			orderModel.getEntries().stream().forEach(abstractOrderEntryModel -> {
				((OrderEntryModel) abstractOrderEntryModel).setSimCardSerialPrefix(siteConfigService.getString("orderentry.simprefix", "8901018"));
					modelService.save(abstractOrderEntryModel);

			});
		}
		orderModel.setCartNumber(cartCode);
		orderModel.setCalculated(Boolean.TRUE);
		modelService.save(orderModel);
		modelService.refresh(orderModel);
		/*if(null != orderModel && llaCommonUtil.isSiteCabletica()) {
			llaCommerceCheckoutService.executeCustomerCallBackRequest(((AbstractOrderModel) orderModel));
		}*/
	}

	private void populateThreeDSFormData(LLAPaymentForm formData, Map<String, String> resultMap) {
		LOG.info(String.format("Result Map Response Code: %s,Reason Code: %s,Reason Code Desc: %s ",resultMap.get("ResponseCode") , resultMap.get("ReasonCode"),resultMap.get("ReasonCodeDesc")));
		formData.setOrderID(resultMap.get("OrderID"));
		formData.setResponseCode(resultMap.get("ResponseCode"));
		formData.setReasonCode(resultMap.get("ReasonCode"));
		formData.setReasonCodeDesc(resultMap.get("ReasonCodeDesc"));
		if(null != resultMap.get("ReferenceNo")){
			formData.setReferenceNo(resultMap.get("ReferenceNo"));
		}
		if(null != resultMap.get("PaddedCardNo")){
			formData.setPaddedCardNo(resultMap.get("PaddedCardNo"));
		}
		if(null != resultMap.get("AuthCode")){
			formData.setAuthCode(resultMap.get("AuthCode"));
		}
		if(null != resultMap.get("OriginalResponseCode")){
			formData.setOriginalResponseCode(resultMap.get("OriginalResponseCode"));
		}
		if(null != resultMap.get("CAVVValue")){
			formData.setCAVVValue(resultMap.get("CAVVValue"));
		}
		if(null != resultMap.get("TransactionStain")){
			formData.setTransactionStain(resultMap.get("TransactionStain"));
		}
		if(null != resultMap.get("Signature")){
			formData.setSignature(resultMap.get("Signature"));
		}
		if(null != resultMap.get("SignatureMethod")){
			formData.setSignatureMethod(resultMap.get("SignatureMethod"));
		}
	}

	protected boolean authorizePayment(final LLAPaymentForm formData)
	{
		boolean isPaymentAuthorized = false;
		try
		{
			isPaymentAuthorized = llaPaymentCheckoutFacadeImpl.authorizePayment("",formData);
		}
		catch (final AdapterException ae)
		{
			// handle a case where a wrong paymentProvider configurations on the store
			// see getCommerceCheckoutService().getPaymentProvider()
			LOG.error(ae.getMessage(), ae);
		}
		return isPaymentAuthorized;
	}


	/**
	 * Validates the order form before to filter out invalid order states.
	 *
	 * @param placeOrderForm
	 *           The spring form of the order being submitted
	 * @param model
	 *           A spring Model
	 * @return True if the order form is invalid and false if everything is valid.
	 */
	protected boolean validateOrderForm(final PlaceOrderForm placeOrderForm, final Model model)
	{
		final String securityCode = placeOrderForm.getSecurityCode();
		boolean invalid = false;

		if (getCheckoutFlowFacade().hasNoDeliveryAddress() && llaCommonUtil.isSitePanama())
		{
			GlobalMessages.addErrorMessage(model, "checkout.deliveryAddress.notSelected");
			invalid = true;
		}

		if (llaCommonUtil.isSiteJamaica() && llaCheckoutFlowFacade.hasNoInstallationAddress())
		{
			GlobalMessages.addErrorMessage(model, "checkout.installationAddress.notSelected");
			invalid = true;
		}
		/*if ( llaCommonUtil.isSitePanama() && getCheckoutFlowFacade().hasNoDeliveryMode() && !llaCommonUtil.hideDeliveryMethod(cartService.getSessionCart()))
		{
			GlobalMessages.addErrorMessage(model, "checkout.deliveryMethod.notSelected");
			invalid = true;
		}
*/
		if (llaCommonUtil.isSiteJamaica() && getCheckoutFlowFacade().hasNoPaymentInfo() && !llaCommonUtil.hidePaymentSectionForJamaica())
		{
			GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.notSelected");
			invalid = true;
		}
		/*else if (llaCommonUtil.isSitePanama() && llaCheckoutFlowFacade.hasNoAdditionalPaymentInfo() && !llaCommonUtil.hidePaymentMethod(cartService.getSessionCart())){
			GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.notSelected");
			invalid = true;
		}*/
		else
		{
			// Only require the Security Code to be entered on the summary page if the SubscriptionPciOption is set to Default.
			if (llaCommonUtil.isSiteJamaica() && CheckoutPciOptionEnum.DEFAULT.equals(getCheckoutFlowFacade().getSubscriptionPciOption())
					&& StringUtils.isBlank(securityCode))
			{
				GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.noSecurityCode");
				invalid = true;
			}
		}

		if (!llaCommonUtil.isSitePuertorico() && !placeOrderForm.isTermsCheck1())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.terms.credit.check.not.accepted");
			invalid = true;
		}
		if (!placeOrderForm.isTermsCheck2())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.terms.not.accepted");
			invalid = true;
		}
		final CartData cartData = getCheckoutFacade().getCheckoutCart();

		if (!cartData.isCalculated())
		{
			LOG.error(
					String.format("Cart %s has a calculated flag of FALSE, placement of order can't continue", cartData.getCode()));
			GlobalMessages.addErrorMessage(model, "checkout.error.cart.notcalculated");
			invalid = true;
		}

		return invalid;
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		binder.setDisallowedFields(DISALLOWED_FIELDS);
	}

	protected CheckoutStep getCheckoutStep()
	{
		if(llaCommonUtil.isSiteJamaica())
			return getCheckoutStep("payment-method");
		return getCheckoutStep(SUMMARY);
	}


}
