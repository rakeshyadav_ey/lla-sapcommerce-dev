/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcoaddon.controllers.pages;

import com.lla.common.addon.idp.strategy.LLALoginStrategy;
import com.lla.core.util.LLACommonUtil;
import com.lla.facades.product.data.ProductRegionData;
import com.lla.facades.product.impl.LLATmaProductFacadeImpl;
import com.lla.telcoaddon.breadcrumb.TmaProductOfferingBreadcrumbBuilder;
import com.lla.telcoaddon.constants.LlatelcoaddonWebConstants;
import com.lla.telcoaddon.constants.WebConstants;
import com.lla.telcoaddon.controllers.TelcoControllerConstants;
import com.lla.telcoaddon.controllers.util.ProductDataHelper;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ReviewForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.ReviewValidator;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.acceleratorstorefrontcommons.variants.VariantSortStrategy;
import de.hybris.platform.b2ctelcofacades.product.TmaProductOfferFacade;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.services.TmaPoService;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.*;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.helper.ProductAndCategoryHelper;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.stream.Collectors;

import de.hybris.platform.product.ProductService;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.europe1.model.PriceRowModel;

import de.hybris.platform.subscriptionservices.model.RecurringChargeEntryModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionPricePlanModel;
import java.math.BigDecimal;
import org.apache.commons.lang.StringUtils;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.data.CartData;




/**
 * Controller for product details page.
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/**/p")
public class ProductPageController extends AbstractPageController
{
	public static final String PAGE_TYPE = "pageType";
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(ProductPageController.class);

	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String PRODUCT_CODE_PATH_VARIABLE_PATTERN = "/{productCode:.*}";
	private static final String REVIEWS_PATH_VARIABLE_PATTERN = "{numberOfReviews:.*}";
	private static final String PRODUCT_DATA = "product";
	private final String[] DISALLOWED_FIELDS = new String[] {};

	@Resource(name = "productModelUrlResolver")
	private UrlResolver<ProductModel> productModelUrlResolver;

	@Resource(name = "productFacade")
	private LLATmaProductFacadeImpl productFacade;

	@Resource(name = "tmaProductOfferFacade")
	private TmaProductOfferFacade productOfferFacade;

	@Resource(name = "productOfferingBreadcrumbBuilder")
	private TmaProductOfferingBreadcrumbBuilder productBreadcrumbBuilder;

	@Resource(name = "cmsPageService")
	private CMSPageService cmsPageService;

	@Resource(name = "variantSortStrategy")
	private VariantSortStrategy variantSortStrategy;

	@Resource(name = "reviewValidator")
	private ReviewValidator reviewValidator;

	@Resource(name = "categoryConverter")
	private Converter<CategoryModel, CategoryData> categoryConverter;

	@Resource(name = "commerceCategoryService")
	private CommerceCategoryService commerceCategoryService;

	@Resource(name = "categoryDataUrlResolver")
	private UrlResolver<CategoryData> categoryDataUrlResolver;

	@Resource(name = "tmaPoService")
	private TmaPoService tmaPoService;
	@Resource(name = "llaLoginStrategy")
	private LLALoginStrategy llaLoginStrategy;
	@Autowired
	private LLACommonUtil llaCommonUtil;
	
	@Autowired
	private SessionService sessionService;
	
	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "productService")
	private ProductService productService;

	@Autowired
	CommerceCommonI18NService commerceCommonI18NService;

	@Resource(name = "priceDataFactory")
	PriceDataFactory priceDataFactory;

	@Autowired
	CategoryService categoryService;

	@Resource(name = "productAndCategoryHelper")
	private ProductAndCategoryHelper productAndCategoryHelper;

	

	@ModelAttribute("authUrl")
	public String getAuthorizationUrl(final HttpServletRequest request)
	{
		request.getSession().setAttribute("llaRefererUrl",request.getRequestURL());
		return llaLoginStrategy.authUrlConstructor(request, "auth");
	}

	@ModelAttribute("areas")
	public List<ProductRegionData> getAreaList()
	{
		return  llaCommonUtil.isSiteJamaica()?llaCommonUtil.getJamaicaRegions(): Collections.emptyList();
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String productDetail(@PathVariable("productCode") final String productCode, final Model model,
			@RequestParam(value = "activeTab", required = false) final String activeTab,
			final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException, UnsupportedEncodingException
	{
		final TmaProductOfferingModel productModel = getTmaPoService().getPoForCode(productCode);
		final String redirection = checkRequestUrl(request, response, productModelUrlResolver.resolve(productModel));
		if (StringUtils.isNotEmpty(redirection))
		{
			return redirection;
		}
		if (llaCommonUtil.isSitePanama())
		{
			String bundle2pList=getSiteConfigService().getString("productpage.2pbundles.productList", null);
			String addonProducts=getSiteConfigService().getString("productpage.addonproduct.code", null);
			if(bundle2pList.contains(productCode))
				addonProducts=getSiteConfigService().getString("productpage.addonproduct.for.bundle2p.code", null);
			if(StringUtils.isNotEmpty(addonProducts)){
				List<ProductData> addonData= new ArrayList<>() ;
				List<String> addonProductList= Arrays.asList(StringUtils.split(addonProducts, ","));
				for(String addonProduct: addonProductList){
					final ProductData productData = getProductOfferFacade().getPoForCode(addonProduct, Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.CATEGORIES));
					addonData.add(productData);
				}
				super.guidedStep(model);
				model.addAttribute("addonProducts", addonData);
				model.addAttribute("bundles2pList", bundle2pList);
				model.addAttribute("addonFreeOrderQuantityMap", getStringLongMap(getSiteConfigService().getString("productpage.addonproduct.freeproduct.numbers", null)));
				model.addAttribute("addonMaxOrderQuantityMap", getStringLongMap(getSiteConfigService().getString("productpage.addonproduct.maxperorder.numbers", null)));
			}
			final CategoryModel cat = categoryService.getCategoryForCode("postpaid");

			final List<ProductModel> ss = getProductService().getProductsForCategory(cat);

			model.addAttribute("postpaidProducts", fetchPlansForDevices(ss));
			final CategoryModel cat2 = categoryService.getCategoryForCode("4Pbundles");

			final List<ProductModel> ss2 = getProductService().getProductsForCategory(cat2);

			model.addAttribute("fmcProducts", fetchPlansForDevices(ss2));
			
			model.addAttribute("cartData", cartFacade.getSessionCart());

			if(llaCommonUtil.checkDeviceProduct(productCode)) {
				sessionService.setAttribute("deviceProduct", Boolean.TRUE);
			}
			if(null!=productModel && productModel.getSupercategories().stream().anyMatch(r->r.getCode().contains("4Pbundles"))) {
				sessionService.setAttribute("fmcProduct", Boolean.TRUE);
			}
		}

		updatePageTitle(productModel, model);
		populateProductDetailForDisplay(productModel, model, request);
		model.addAttribute(new ReviewForm());
		final List<ProductReferenceData> productReferences = productFacade.getProductReferencesForCode(productCode,
				Arrays.asList(ProductReferenceTypeEnum.SIMILAR, ProductReferenceTypeEnum.ACCESSORIES),
				Arrays.asList(ProductOption.BASIC, ProductOption.PRICE), null);
		if(StringUtils.isNotEmpty(activeTab)){
			model.addAttribute("activeTab", activeTab);
		}
		model.addAttribute("productReferences", productReferences);
		final ProductData productData = (ProductData) model.asMap().get(PRODUCT_DATA);
		List<CategoryData> categories = productFacade.getProductCategories(productModel);
		if (llaCommonUtil.isSitePanama() && CollectionUtils.isNotEmpty(categories) && categories.size() > 2 && (categories.get(1).getCode().equalsIgnoreCase("bundle") || categories.get(1).getCode().equalsIgnoreCase("mobile"))) {
			model.addAttribute(PAGE_TYPE, StringUtils.join(new String[]{categories.get(1).getCode(), "_", productCode}));
		} else
			model.addAttribute(PAGE_TYPE, PageType.PRODUCT.name());
		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(productData.getKeywords());
		final String metaDescription = MetaSanitizerUtil.sanitizeDescription(productData.getDescription());
		setUpMetaData(model, metaKeywords, metaDescription);
		if(cartFacade.hasSessionCart()) {
			model.addAttribute("cartData", cartFacade.getSessionCart());
		}
		return getViewForPage(model);
	}

	private Map<String, Long> getStringLongMap(String addonQuantity) {
		return Arrays.stream(addonQuantity.split(","))
		.map(s -> s.split(":"))
		.collect(Collectors.toMap(s -> s[0], s -> Long.valueOf(s[1])));
	}

	/**
	 * Zoom images.
	 *
	 * @param productCode
	 * @param galleryPosition
	 * @param model
	 * @return
	 */
	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/zoomImages", method = RequestMethod.GET)
	public String showZoomImages(@PathVariable("productCode") final String productCode,
	@RequestParam(value = "galleryPosition", required = false) final String galleryPosition, final Model model,
	final HttpServletRequest request)
	{
		final ProductData productData = getProductOfferFacade().getPoForCode(productCode,
				Collections.singleton(ProductOption.GALLERY));
		final List<Map<String, ImageData>> images = getGalleryImages(productData);
		populateProductData(productData, model, request);
		if (galleryPosition != null)
		{
			try
			{
				model.addAttribute("zoomImageUrl", images.get(Integer.parseInt(galleryPosition)).get("zoom").getUrl());
			}
			catch (final IndexOutOfBoundsException | NumberFormatException ioebe)
			{
				LOG.info(ioebe);
				model.addAttribute("zoomImageUrl", "");
			}
		}
		return TelcoControllerConstants.Views.Fragments.Product.ZOOM_IMAGES_POPUP;
	}

	/**
	 * Quick view.
	 *
	 * @param productCode
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/quickView", method = RequestMethod.GET)
	public String showQuickView(@PathVariable("productCode") final String productCode, final Model model,
	final HttpServletRequest request)
	{
		final TmaProductOfferingModel productModel = getTmaPoService().getPoForCode(productCode);
		final ProductData productData = getProductOfferFacade().getPoForCode(productCode,
				Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION,
						ProductOption.CATEGORIES, ProductOption.PROMOTIONS, ProductOption.STOCK, ProductOption.REVIEW,
						ProductOption.VARIANT_FULL, ProductOption.DELIVERY_MODE_AVAILABILITY));

		sortVariantOptionData(productData);
		populateProductData(productData, model, request);
		getRequestContextData(request).setProduct(productModel);

		return TelcoControllerConstants.Views.Fragments.Product.QUICK_VIEW_POPUP;
	}

	/**
	 * Post review.
	 *
	 * @param productCode
	 * @param form
	 * @param result
	 * @param model
	 * @param request
	 * @param redirectAttrs
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/review", method =
	{ RequestMethod.GET, RequestMethod.POST })
	public String postReview(@PathVariable final String productCode, final ReviewForm form, final BindingResult result,
	final Model model, final HttpServletRequest request, final RedirectAttributes redirectAttrs)
			throws CMSItemNotFoundException
	{
		getReviewValidator().validate(form, result);

		final TmaProductOfferingModel productModel = getTmaPoService().getPoForCode(productCode);

		if (result.hasErrors())
		{
			updatePageTitle(productModel, model);
			GlobalMessages.addErrorMessage(model, "review.general.error");
			model.addAttribute("showReviewForm", Boolean.TRUE);
			populateProductDetailForDisplay(productModel, model, request);
			storeCmsPageInModel(model, getPageForProduct(productModel.getCode()));
			return getViewForPage(model);
		}

		final ReviewData review = new ReviewData();
		review.setHeadline(XSSFilterUtil.filter(form.getHeadline()));
		review.setComment(XSSFilterUtil.filter(form.getComment()));
		review.setRating(form.getRating());
		review.setAlias(XSSFilterUtil.filter(form.getAlias()));
		productFacade.postReview(productCode, review);
		GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.CONF_MESSAGES_HOLDER, "review.confirmation.thank.you.title");

		return REDIRECT_PREFIX + productModelUrlResolver.resolve(productModel);
	}

	/**
	 * Show review.
	 *
	 * @param productCode
	 * @param numberOfReviews
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/reviewhtml/"
			+ REVIEWS_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String reviewHtml(@PathVariable("productCode") final String productCode,
	@PathVariable("numberOfReviews") final String numberOfReviews, final Model model, final HttpServletRequest request)
	{
		final TmaProductOfferingModel productModel = getTmaPoService().getPoForCode(productCode);
		final List<ReviewData> reviews;
		final ProductData productData = getProductOfferFacade().getPoForCode(productCode,
				Arrays.asList(ProductOption.BASIC, ProductOption.REVIEW));

		if ("all".equals(numberOfReviews))
		{
			reviews = productFacade.getReviews(productCode);
		}
		else
		{
			final int reviewCount = Math.min(Integer.parseInt(numberOfReviews),
					(productData.getNumberOfReviews() == null ? 0 : productData.getNumberOfReviews().intValue()));
			reviews = productFacade.getReviews(productCode, Integer.valueOf(reviewCount));
		}

		getRequestContextData(request).setProduct(productModel);
		reviews.forEach(review -> {
			review.setHeadline(XSSFilterUtil.filter(review.getHeadline()));
			review.setComment(XSSFilterUtil.filter(review.getComment()));
			review.setAlias(XSSFilterUtil.filter(review.getAlias()));
		});
		model.addAttribute("reviews", reviews);
		model.addAttribute("reviewsTotal", productData.getNumberOfReviews());
		model.addAttribute(new ReviewForm());

		return TelcoControllerConstants.Views.Fragments.Product.REVIEWS_TAB;
	}

	/**
	 * Write review dialog.
	 *
	 * @param productCode
	 * @param model
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/writeReview", method = RequestMethod.GET)
	public String writeReview(@PathVariable final String productCode, final Model model) throws CMSItemNotFoundException
	{
		final TmaProductOfferingModel productModel = getTmaPoService().getPoForCode(productCode);
		model.addAttribute(new ReviewForm());
		setUpReviewPage(model, productModel);
		return TelcoControllerConstants.Views.Pages.Product.WRITE_REVIEW;
	}

	protected void setUpReviewPage(final Model model, final ProductModel productModel) throws CMSItemNotFoundException
	{
		final ProductData productData = getProductOfferFacade().getPoForCode(productModel.getCode(),
				Arrays.asList(ProductOption.BASIC, ProductOption.DESCRIPTION));
		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(productData.getKeywords());
		final String metaDescription = MetaSanitizerUtil.sanitizeDescription(productData.getDescription());
		setUpMetaData(model, metaKeywords, metaDescription);
		storeCmsPageInModel(model, getPageForProduct(productData.getCode()));
		model.addAttribute("product", productData);
		updatePageTitle(productModel, model);
	}

	/**
	 * Write review.
	 *
	 * @param productCode
	 * @param form
	 * @param result
	 * @param model
	 * @param request
	 * @param redirectAttrs
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/writeReview", method = RequestMethod.POST)
	public String writeReview(@PathVariable final String productCode, final ReviewForm form, final BindingResult result,
    final Model model, final HttpServletRequest request, final RedirectAttributes redirectAttrs)
			throws CMSItemNotFoundException
	{
		getReviewValidator().validate(form, result);

		final TmaProductOfferingModel productModel = getTmaPoService().getPoForCode(productCode);

		if (result.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "review.general.error");
			populateProductDetailForDisplay(productModel, model, request);
			setUpReviewPage(model, productModel);
			return TelcoControllerConstants.Views.Pages.Product.WRITE_REVIEW;
		}

		final ReviewData review = new ReviewData();
		review.setHeadline(XSSFilterUtil.filter(form.getHeadline()));
		review.setComment(XSSFilterUtil.filter(form.getComment()));
		review.setRating(form.getRating());
		review.setAlias(XSSFilterUtil.filter(form.getAlias()));
		productFacade.postReview(productCode, review);
		GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.CONF_MESSAGES_HOLDER, "review.confirmation.thank.you.title");

		return REDIRECT_PREFIX + productModelUrlResolver.resolve(productModel);
	}

	/**
	 * Unknown identifier error.
	 *
	 * @param exception
	 * @param request
	 * @return
	 */

	@ExceptionHandler(UnknownIdentifierException.class)
	public String handleUnknownIdentifierException(final UnknownIdentifierException exception, final HttpServletRequest request)
	{
		request.setAttribute("message", exception.getMessage());
		return FORWARD_PREFIX + "/404";
	}

	protected void updatePageTitle(final ProductModel productModel, final Model model)
	{
		storeContentPageTitleInModel(model, getPageTitleResolver().resolveProductPageTitle(productModel));
	}

	protected void populateProductDetailForDisplay(final TmaProductOfferingModel productModel, final Model model,
    final HttpServletRequest request) throws CMSItemNotFoundException
	{
		getRequestContextData(request).setProduct(productModel);

		final List<ProductOption> options = Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.SUMMARY,
				ProductOption.DESCRIPTION, ProductOption.SOLD_INDIVIDUALLY, ProductOption.GALLERY, ProductOption.CATEGORIES,
				ProductOption.REVIEW, ProductOption.PROMOTIONS, ProductOption.CLASSIFICATION, ProductOption.VARIANT_MATRIX,
				ProductOption.VARIANT_MATRIX_ALL_OPTIONS, ProductOption.STOCK, ProductOption.VOLUME_PRICES,
				ProductOption.DELIVERY_MODE_AVAILABILITY, ProductOption.SPO_BUNDLE_TABS, ProductOption.PRODUCT_SPEC_CHAR_VALUE_USE, ProductOption.PRODUCT_OFFERING_PRICES);
		final ProductData productData = getProductOfferFacade().getPoForCode(productModel.getCode(), options);
		sortVariantOptionData(productData);
		model.addAttribute("productCategories", productFacade.getProductCategories(productModel));
		storeCmsPageInModel(model, getPageForProduct(productData.getCode()));
		populateProductData(productData, model, request);

		final List<Breadcrumb> breadcrumbs = productBreadcrumbBuilder.getBreadcrumbs(productData.getCode());
		// Note: This is the index of the category above the product's supercategory
		int productSuperSuperCategoryIndex = breadcrumbs.size() - 3;
		final List<CategoryData> superCategories = new ArrayList<CategoryData>();
		if (productSuperSuperCategoryIndex == 0)
		{
			// The category at index 0 is never displayed as a supercategory; for
			// display purposes, the category at index 1 is the root category
			productSuperSuperCategoryIndex = 1;
		}
		if (productSuperSuperCategoryIndex > 0)
		{
			// When product has any supercategory
			final Breadcrumb productSuperSuperCategory = breadcrumbs.get(productSuperSuperCategoryIndex);
			final CategoryModel superSuperCategory = commerceCategoryService
					.getCategoryForCode(productSuperSuperCategory.getCategoryCode());
			for (final CategoryModel superCategory : superSuperCategory.getCategories())
			{
				final CategoryData categoryData = new CategoryData();
				categoryData.setName(superCategory.getName());
				categoryData.setUrl(categoryDataUrlResolver.resolve(categoryConverter.convert(superCategory)));
				superCategories.add(categoryData);
			}
		}

		model.addAttribute(LlatelcoaddonWebConstants.SUPERCATEGORIES_KEY, superCategories);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, productBreadcrumbBuilder.getBreadcrumbs(productData.getCode()));
	}



	protected void populateProductData(final ProductData productData, final Model model, final HttpServletRequest request)
	{
		ProductDataHelper.setCurrentProduct(request, productData.getCode());
		model.addAttribute("galleryImages", getGalleryImages(productData));
		model.addAttribute(PRODUCT_DATA, productData);
	}

	protected void sortVariantOptionData(final ProductData productData)
	{
		if (CollectionUtils.isNotEmpty(productData.getBaseOptions()))
		{
			for (final BaseOptionData baseOptionData : productData.getBaseOptions())
			{
				if (CollectionUtils.isNotEmpty(baseOptionData.getOptions()))
				{
					Collections.sort(baseOptionData.getOptions(), variantSortStrategy);
				}
			}
		}

		if (CollectionUtils.isNotEmpty(productData.getVariantOptions()))
		{
			Collections.sort(productData.getVariantOptions(), variantSortStrategy);
		}
	}

	protected List<Map<String, ImageData>> getGalleryImages(final ProductData productData)
	{
		final List<Map<String, ImageData>> galleryImages = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(productData.getImages()))
		{
			final List<ImageData> images = new ArrayList<>();
			for (final ImageData image : productData.getImages())
			{
				if (ImageDataType.GALLERY.equals(image.getImageType()))
				{
					images.add(image);
				}
			}
			Collections.sort(images, new Comparator<ImageData>()
			{
				@Override
				public int compare(final ImageData image1, final ImageData image2)
				{
					return image1.getGalleryIndex().compareTo(image2.getGalleryIndex());
				}
			});

			if (CollectionUtils.isNotEmpty(images))
			{
				int currentIndex = images.get(0).getGalleryIndex().intValue();
				Map<String, ImageData> formats = new HashMap<String, ImageData>();
				for (final ImageData image : images)
				{
					if (currentIndex != image.getGalleryIndex().intValue())
					{
						galleryImages.add(formats);
						formats = new HashMap<>();
						currentIndex = image.getGalleryIndex().intValue();
					}
					formats.put(image.getFormat(), image);
				}
				if (!formats.isEmpty())
				{
					galleryImages.add(formats);
				}
			}
		}
		return galleryImages;
	}

	@ResponseBody
	@RequestMapping(value ="/categoryProductJsonData/{planType}", method = RequestMethod.GET)
	public List<ProductData>  categoryProductData(@PathVariable("planType") final String categoryCode,final Model model)
	{
		List<ProductData> productDataList = new ArrayList<>();
		final Collection<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.BASIC,
				ProductOption.PRICE, ProductOption.DESCRIPTION, ProductOption.STOCK, ProductOption.VOLUME_PRICES,ProductOption.PRODUCT_SPECIFICATION,ProductOption.PRODUCT_OFFERING_PRICES);

		final CategoryModel category= categoryService.getCategoryForCode(categoryCode);
		if(category.getProducts() != null)
		{
			for(ProductModel product : category.getProducts()){
				productDataList.add(productFacade.getProductForCodeAndOptions(product.getCode(),PRODUCT_OPTIONS));
			}
		}
		return productDataList;
	}

	protected ReviewValidator getReviewValidator()
	{
		return reviewValidator;
	}

	protected AbstractPageModel getPageForProduct(final String productCode) throws CMSItemNotFoundException
	{
		return cmsPageService.getPageForProductCode(productCode);
	}

	protected TmaProductOfferFacade getProductOfferFacade()
	{
		return productOfferFacade;
	}

	protected TmaPoService getTmaPoService()
	{
		return tmaPoService;
	}

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		binder.setDisallowedFields(DISALLOWED_FIELDS);
	}

	protected ProductService getProductService()
	{
		return productService;
	}

	private HashMap<String, String> fetchRecurringPriceForBundle(final List<ProductModel> productModelList)
	{
		final HashMap<String, String> map = new HashMap<String, String>();
		for (final ProductModel item : productModelList)
		{

			final Collection<PriceRowModel> priceRowModelCollection = item.getEurope1Prices();
			for (final PriceRowModel priceRow : priceRowModelCollection)
			{
				if (priceRow instanceof SubscriptionPricePlanModel)
				{
					final SubscriptionPricePlanModel pricePlan = (SubscriptionPricePlanModel) priceRow;
					final Collection<RecurringChargeEntryModel> recurringChargeEntries = pricePlan.getRecurringChargeEntries();
					final RecurringChargeEntryModel model = recurringChargeEntries.iterator().next();
					final Double price = model.getPrice();
					final PriceData priceData = priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(price.doubleValue()),
							commerceCommonI18NService.getCurrentCurrency().getIsocode());
					map.put(item.getCode(), StringUtils.chomp(priceData.getFormattedValue(), ".00"));
					break;
				}
			}
		}
		return map;
	}

	private HashMap<String, ProductData> fetchPlansForDevices(final List<ProductModel> productModelList)
	{
		final HashMap<String, ProductData> map = new HashMap<String, ProductData>();
		final List<ProductOption> options = Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.SUMMARY,
				ProductOption.DESCRIPTION, ProductOption.SOLD_INDIVIDUALLY, ProductOption.GALLERY, ProductOption.CATEGORIES,
				ProductOption.REVIEW, ProductOption.PROMOTIONS, ProductOption.CLASSIFICATION, ProductOption.VARIANT_MATRIX,
				ProductOption.VARIANT_MATRIX_ALL_OPTIONS, ProductOption.STOCK, ProductOption.VOLUME_PRICES,
				ProductOption.DELIVERY_MODE_AVAILABILITY, ProductOption.SPO_BUNDLE_TABS, ProductOption.PRODUCT_SPEC_CHAR_VALUE_USE,
				ProductOption.PRODUCT_OFFERING_PRICES, ProductOption.PRODUCT_SPECIFICATION);
		for (final ProductModel productModel : productModelList)
		{
			ProductData productData = getProductOfferFacade().getPoForCode(productModel.getCode(), options);
			map.put(productModel.getCode(), productData);
		}
		final HashMap<String, ProductData> sequencedMap = map.entrySet().stream().sorted((e1, e2) -> e1.getValue().getSequenceId().compareTo(e2.getValue().getSequenceId()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		return sequencedMap;
	}
}
