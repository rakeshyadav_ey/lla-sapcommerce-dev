package com.lla.telcoaddon.controllers.pages.checkout.steps;
import com.lla.core.enums.ClickToCallReasonEnum;
import com.lla.core.service.creditscore.LLACreditScoreService;
import com.lla.core.util.LLACommonUtil;
import com.lla.core.util.LLAEncryptionUtil;
import com.lla.facades.cart.LLACartFacade;
import com.lla.facades.checkout.LLACheckoutFacade;
import com.lla.facades.customer.LLACustomerFacade;
import com.lla.facades.order.data.ContractTypeData;
import com.lla.facades.product.data.GenderData;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import com.lla.telcoaddon.controllers.ControllerConstants;
import com.lla.telcoaddon.controllers.util.CartHelper;
import com.lla.telcoaddon.forms.ContractTypeForm;
import com.lla.telcoaddon.service.LLAPersonalDetailService;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping(value = "/checkout/multi/contract-detail")
public class ContractDetailCheckoutStepController extends AbstractCheckoutStepController{
    private static final Logger LOG = LoggerFactory.getLogger(ContractDetailCheckoutStepController.class);
    protected static final String CONTRACT_DETAIL = "contract-detail";
    protected static final String ATTR_CART_DATA = "cartData";
    private static final String CREDIT_CHECK_RESPONSE="creditCheckResponse";
    private final String[] DISALLOWED_FIELDS = new String[] {};

    @Resource(name = "userFacade")
    private UserFacade userFacade;

    @Autowired
    private Converter<Gender, GenderData> genderConverter;
    @Resource(name="userService")
    private UserService userService;
    @Resource(name = "customerFacade")
    private LLACustomerFacade llaCustomerFacade;
    @Resource(name = "checkoutFacade")
    private LLACheckoutFacade llaCheckoutFacade;
    @Autowired
    private LLACommonUtil llaCommonUtil;
    @Autowired
    private EnumerationService enumerationService;
    @Autowired
    private CartService cartService;
    @Autowired
    private LLAPersonalDetailService llaPersonalDetailService;
    @Autowired
    private LLACartFacade llaCartFacade;

    @Autowired
    private LLACreditScoreService llaCreditScoreService;
    @Autowired
    private ModelService modelService;
    @Autowired
	private LLAMulesoftNotificationService llamulesoftNotificationService;
    @Autowired
    private LLAEncryptionUtil llaEncryptionUtil;
    @ModelAttribute("availableContractTypes")
    public List<ContractTypeData> getContractType()
    {
        List<ContractTypeData> contractTypeDataList=llaCommonUtil.getPossibleContractTypes();
        return contractTypeDataList;
    }
    @Override
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    @RequireHardLogIn
    @PreValidateCheckoutStep(checkoutStep = CONTRACT_DETAIL)
    public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
    {
        prepareDataForPage(model);
        CustomerModel customerModel=(CustomerModel)userService.getCurrentUser();
        final CartData cartData = llaCheckoutFacade.getCheckoutCart();
        cartData.setEntries(CartHelper.removeEmptyEntries(cartData.getEntries()));
        model.addAttribute(ATTR_CART_DATA, cartData);
        populateContractTypeForm(cartService.getSessionCart(), model);
        if(llaCommonUtil.isSitePuertorico()){
            CartModel cartModel=cartService.getSessionCart();
            llaCommonUtil.updateCreditScoreDetails(cartModel);
        }
        storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.contractTypeForm.breadcrumb.label"));
        model.addAttribute("metaRobots", "noindex,nofollow");
        model.addAttribute("selectedPlanData",llaCartFacade.getSelectedPlanInOrder());
        setCheckoutStepLinksForModel(model, getCheckoutStep());

        return ControllerConstants.Views.Pages.MultiStepCheckout.ContractDetailPage;


    }

    private void populateContractTypeForm(final CartModel cartModel, final Model model) {
        ContractTypeForm contractTypeForm=new ContractTypeForm();
        model.addAttribute("contractTypeForm",contractTypeForm);
    }
    @RequestMapping(value = "/back", method = RequestMethod.GET)
    @RequireHardLogIn
    @Override
    public String back(final RedirectAttributes redirectAttributes)
    {
        return getCheckoutStep().previousStep();
    }

    @RequestMapping(value = "/next", method = RequestMethod.GET)
    @RequireHardLogIn
    @Override
    public String next(final RedirectAttributes redirectAttributes)
    {
        return getCheckoutStep().nextStep();
    }

    @InitBinder
    public void initBinder(final WebDataBinder binder)
    {
        binder.setDisallowedFields(DISALLOWED_FIELDS);
    }


    protected CheckoutStep getCheckoutStep()
    {
        return getCheckoutStep(CONTRACT_DETAIL);
    }

    @RequestMapping(value="/update", method=RequestMethod.POST)
    public String updateContractDetails(final ContractTypeForm form,
                                                      BindingResult bindingResult, final Model model, final HttpServletRequest request,
                                                      final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException {

        CustomerModel customerModel=(CustomerModel)userService.getCurrentUser();
        final CartModel cart = cartService.getSessionCart();
        if (bindingResult.hasErrors())
        {
            prepareDataForPage(model);
            final CartData cartData = llaCheckoutFacade.getCheckoutCart();
            cartData.setEntries(CartHelper.removeEmptyEntries(cartData.getEntries()));
            model.addAttribute(ATTR_CART_DATA, cartData);
            GlobalMessages.addErrorMessage(model, "profile.error.formentry.invalid");
            storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
            setCheckoutStepLinksForModel(model, getCheckoutStep());
            return ControllerConstants.Views.Pages.MultiStepCheckout.ContractDetailPage;
        }
        llaPersonalDetailService.updateContractDetails(customerModel,form);
        return getCheckoutStep().nextStep();
    }
}
