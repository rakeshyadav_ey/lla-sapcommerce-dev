/**
 *
 */
package com.lla.telcoaddon.controllers.pages;


import com.lla.core.enums.ClickToCallReasonEnum;
import com.lla.core.enums.OriginSource;
import com.lla.core.model.PLPC2CFormModel;
import com.lla.core.util.LLACommonUtil;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.lla.core.service.order.LLACommerceCheckoutService;
import com.lla.mulesoft.integration.service.LLAMulesoftCallBackRequestService;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller

@RequestMapping(value = "/click")
public class ClickToCallFormController
{
	private static final Logger LOG = Logger.getLogger(ClickToCallFormController.class);
	@Autowired
	private LLACommerceCheckoutService llaCommerceCheckoutService;
	@Autowired
	private LLAMulesoftCallBackRequestService llaMulesoftCallBackRequestService;
	@Autowired
	private CartService cartService;
	@Autowired
	private LLACommonUtil llaCommonUtil;
	@Autowired
	private ModelService modelService;
	@Autowired
	private ConfigurationService configurationService;
	@Autowired
	private LLAMulesoftNotificationService llamulesoftNotificationService;

	@RequestMapping(value = "/clickToCallForm", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> onClickOnPlpForm(@RequestParam(value = "phoneNumber", required = true)
	final String phoneNumber,@RequestParam(value = "identityNumber", required = true)
	final String identityNumber,@RequestParam(value = "product", required = true)
	final String product,@RequestParam(value = "email", required = true)
	final String email) {
		PLPC2CFormModel clickCall = modelService.create(PLPC2CFormModel.class);
		clickCall.setEmail(email);
		clickCall.setIdentityNumber(identityNumber);
		clickCall.setPhoneNumber(phoneNumber);
		clickCall.setProduct(product);
		modelService.save(clickCall);
		final CartModel cart = cartService.getSessionCart();
		llaCommonUtil.launchFalloutProcess(cart, ClickToCallReasonEnum.NOT_SERVICEABLE, OriginSource.PLP, clickCall, StringUtils.EMPTY,StringUtils.EMPTY);
		LOG.info(String.format("Click to Call triggered for Location Serviceability of  customer %s", email));
		return new ResponseEntity<>(String.valueOf("true"), HttpStatus.OK);
	}

}
