/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcoaddon.controllers.cms;

import com.lla.telcoaddon.controllers.TelcoControllerConstants;
import com.lla.telcoaddon.controllers.util.ProductDataHelper;
import com.lla.telcoaddon.model.ProductReferencesAndClassificationsComponentModel;
import com.lla.telcoaddon.model.ProductReferencesAndClassificationsForDevicesComponentModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * Component controller that handles product references, feature compatible and vendor compatible products.
 */
@Controller("ProductReferencesAndClassificationsForDevicesComponentController")
@RequestMapping(value = TelcoControllerConstants.Actions.Cms.PRODUCT_REFERENCES_AND_CLASSIFICATIONS_FOR_DEVICES_COMPONENT)
public class ProductReferencesAndClassificationsForDevicesComponentController extends
		ProductReferencesAndClassificationsComponentController
{
	private static final Logger LOG = LoggerFactory
			.getLogger(ProductReferencesAndClassificationsForDevicesComponentController.class);

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final ProductReferencesAndClassificationsComponentModel component)
	{
		ServicesUtil.validateParameterNotNull(component, "CMSComponent must not be null");

		if (component instanceof ProductReferencesAndClassificationsForDevicesComponentModel)
		{
			final ProductReferencesAndClassificationsForDevicesComponentModel prComponent = (ProductReferencesAndClassificationsForDevicesComponentModel) component;

			final List<ProductData> referenceAndClassificationsProducts = getTelcoProductFacade()
					.getProductReferencesAndCompatibleProducts(
							ProductDataHelper.getCurrentProduct(request), prComponent.getProductReferenceTypes(),
							ProductReferencesAndClassificationsComponentController.PRODUCT_OPTIONS,
							prComponent.getMaximumNumberProducts(),
							prComponent.getClassAttributeAssignment());

			model.addAttribute("title", prComponent.getTitle());

			model.addAttribute("productAccessories", referenceAndClassificationsProducts);
		}
		else
		{
			LOG.info("Unsupported CMSComponent type for item [" + component
					+ "]: expected 'ProductReferencesAndClassificationsForDevicesComponentModel', but was '"
					+ component.getClass().getSimpleName() + "'");
		}
	}
}
