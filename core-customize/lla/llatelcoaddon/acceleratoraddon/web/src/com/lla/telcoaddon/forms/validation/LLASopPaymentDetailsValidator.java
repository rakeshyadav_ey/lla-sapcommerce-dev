package com.lla.telcoaddon.forms.validation;

import com.lla.core.util.LLACommonUtil;
import com.lla.telcoaddon.forms.SopPaymentDetailsForm;
import de.hybris.platform.acceleratorservices.util.CalendarHelper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component("llaSopPaymentDetailsValidator")
public class LLASopPaymentDetailsValidator implements Validator {
    private static final int MAX_FIELD_LENGTH = 255;
    @Autowired
    private LLACommonUtil llaCommonUtil;

    @Override
    public boolean supports(final Class<?> aClass)
    {
        return SopPaymentDetailsForm.class.equals(aClass);
    }

    @Override
    public void validate(final Object object, final Errors errors)
    {
        final SopPaymentDetailsForm form = (SopPaymentDetailsForm) object;

        String regex = "^(?:(?<visa>4[0-9]{12}(?:[0-9]{3})?)|" +
                "(?<mastercard>5[1-5][0-9]{14}))$";
        Pattern pattern = Pattern.compile(regex);

        final Calendar startOfCurrentMonth = CalendarHelper.getCalendarResetTime();
        startOfCurrentMonth.set(Calendar.DAY_OF_MONTH, 1);

        final Calendar expiration = CalendarHelper.parseDate(form.getCard_expirationMonth(), form.getCard_expirationYear());

        if(Integer.valueOf(form.getCard_expirationMonth()) < 1 && Integer.valueOf(form.getCard_expirationMonth()) > 12 )
        {
            errors.rejectValue("card_expiryMonthYear", "card.expirationMonthYear.invalid");
        }
        if (expiration != null && expiration.before(startOfCurrentMonth))
        {
            errors.rejectValue("card_expiryMonthYear", "card.expirationMonthYear.invalid");
        }
       
        if (form.getCard_cvNumber().length() > 4) {
            errors.rejectValue("card_cvNumber", "card.cvNumber.invalid");
        }
        if(llaCommonUtil.isSiteCabletica()){
            if (StringUtils.isBlank(form.getBillTo_country()))
            {
               ValidationUtils.rejectIfEmptyOrWhitespace(errors, "billTo_country", "address.country.invalid");
            }
            if(StringUtils.isBlank(form.getCard_accountNumber())){
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "card_accountNumber", "card.number.invalid");
            }
            Matcher matcher = pattern.matcher(form.getCard_accountNumber());
            if(!matcher.matches()){
                errors.rejectValue("card_accountNumber", "card.number.invalid");
            }
            validateStringFieldLength(form.getBillTo_building(), LLASopPaymentDetailsValidator.AddressField.BUILDING, MAX_FIELD_LENGTH, errors);
            validateStringFieldLength(form.getBillTo_district(), LLASopPaymentDetailsValidator.AddressField.DISTRICT, MAX_FIELD_LENGTH, errors);
            validateStringField(form.getBillTo_province(), LLASopPaymentDetailsValidator.AddressField.PROVINCE, MAX_FIELD_LENGTH, errors);
        }


    }

    protected static void validateStringField(final String addressField, final LLASopPaymentDetailsValidator.AddressField fieldType,
                                              final int maxFieldLength, final Errors errors)
    {
        if (addressField == null || StringUtils.isEmpty(addressField) || (StringUtils.length(addressField) > maxFieldLength))
        {
            errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
        }
    }


    protected static void validateStringFieldLength(final String field, final LLASopPaymentDetailsValidator.AddressField fieldType, final int maxFieldLength,
                                                    final Errors errors)
    {
        if (StringUtils.isNotEmpty(field) && StringUtils.length(field) > maxFieldLength)
        {
            errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
        }
    }

    protected enum AddressField
    {
        BUILDING("billTo_building", "address.building.invalid"), PROVINCE("billTo_province","address.province.invalid"),
        DISTRICT("billTo_district", "address.district.invalid");

        private final String fieldKey;
        private final String errorKey;

        private AddressField(final String fieldKey, final String errorKey)
        {
            this.fieldKey = fieldKey;
            this.errorKey = errorKey;
        }

        public String getFieldKey()
        {
            return fieldKey;
        }

        public String getErrorKey()
        {
            return errorKey;
        }

    }
}
