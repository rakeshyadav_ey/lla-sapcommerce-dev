/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcoaddon.controllers.misc;


import com.lla.core.model.LLABundleProductMappingModel;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateQuantityForm;
import de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.b2ctelcofacades.configurableguidedselling.EntryGroupFacade;
import de.hybris.platform.b2ctelcofacades.data.TmaBpoPreConfigData;
import de.hybris.platform.b2ctelcofacades.order.TmaCartFacade;
import de.hybris.platform.b2ctelcofacades.product.TmaBpoPreConfigFacade;
import de.hybris.platform.b2ctelcoservices.enums.TmaProcessType;
import de.hybris.platform.b2ctelcoservices.model.TmaBundledProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.services.TmaPoService;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.order.EntryGroup;
import de.hybris.platform.jalo.JaloObjectNoLongerValidException;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import de.hybris.platform.core.model.product.ProductModel;
import javax.annotation.Resource;
import javax.validation.Valid;
import de.hybris.platform.product.ProductService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import de.hybris.platform.category.model.CategoryModel;
import com.lla.core.util.LLACommonUtil;
import com.lla.facades.cart.impl.LLACartFacadeImpl;
import com.lla.telcoaddon.constants.LlatelcoaddonWebConstants;
import com.lla.telcoaddon.controllers.TelcoControllerConstants;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import com.lla.facades.productconfigurator.data.LLAPuertoRicoProductConfigDTO;
import com.lla.core.model.LLABundleProductMappingModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
/**
 * Controller for Add to Cart functionality which is not specific to a certain page.
 */
@Controller("TelcoAddToCartController")
public class AddToCartController extends AbstractController
{

	private static final Logger LOG = Logger.getLogger(AddToCartController.class);
	private static final long DEFAULT_QUANTITY = 1;

	private static final String BPO_PRECONFIG_INVALID_ERROR_MESSAGE = "bpo.preconfig.invalid.error.message";

	private static final String INVALID_PRE_CONFIG_CALL = "invalidPreConfigCall";

	private static final String MODIFIED_CART_DATA = "modifiedCartData";

	private static final String BASKET_INFORMATION_QUANTITY_NO_ITEMS_ADDED = "basket.information.quantity.noItemsAdded";

	private static final String BASKET_ERROR_OCCURRED = "basket.error.occurred";

	private static final String CART_ENTRY_WAS_NOT_CREATED_REASON = "Cart entry was not created. Reason: ";

	private static final String CART = "/cart";

	private static final String BPO_CONFIGURE_PATH = "/bpo/configure/";

	private static final String BASKET_ERROR_QUANTITY_INVALID = "basket.error.quantity.invalid";

	private static final String QUANTITY = "quantity";

	private static final String ERROR_MSG_TYPE = "errorMsg";
	
	private static final String REDIRECT_TO_CART_URL = REDIRECT_PREFIX + "/cart";
	
	private static final String PANAMA_CATEGORIES_CODES_TO_SHOW_ADDON_LIST = "categories.codes.to.show.addon.products";

	private final String[] DISALLOWED_FIELDS = new String[] {};

	protected static final String SUCCESS = "success";
	
	public static final String REDIRECT_TO_PLP_URL = REDIRECT_PREFIX + "/residential";
	
	private static final String CHANNEL_ADDON = "channelsaddoncabletica";

	public static final String PRODUCT_CONFIG_DTO = "productConfigDTO";

	public static final String REDIRECT_TO_HOMEPAGE = REDIRECT_PREFIX + "/configureProduct";
	
	public static final String REDIRECT_TO_PLP_BUNDLE_PAGE = REDIRECT_PREFIX + "/plp/bundle";
	
	public static final String POSTPAID = "postpaid";
	
	public static final String FOUR_P_BUNDLES = "4Pbundles";

	public static final String THREE_P_BUNDLES = "3Pbundles";

	public static final String TWO_P_INTERNET_TV = "2PInternetTv";

	public static final String TWO_P_INTERNET_TELEPHONE = "2PInternetTelephone";
	
	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "productFacade")
	private ProductFacade productFacade;

	@Resource(name = "cartFacade")
	private TmaCartFacade tmaCartFacade;

	@Resource(name = "entryGroupFacade")
	private EntryGroupFacade entryGroupFacade;

	@Resource(name = "tmaBpoPreConfigFacade")
	private TmaBpoPreConfigFacade tmaBpoPreConfigFacade;

	@Autowired
	private LLACartFacadeImpl llaCartFacadeImpl;

	@Autowired
	private CartService cartService;

	@Autowired
	private ModelService modelService;

	@Resource(name = "siteConfigService")
	private SiteConfigService siteConfigService;

	@Autowired
	private SessionService sessionService;

	@Autowired
	private LLACommonUtil llaCommonUtil;

	@Autowired
	private TmaPoService tmaPoService;

	@Autowired
	private ConfigurationService configurationService;
	
	@Resource(name = "commerceCategoryService")
	private CommerceCategoryService commerceCategoryService;
	
	@Resource(name = "productService")
	private ProductService productService;
	@Autowired
	private CalculationService calculationService;
	
	/**
	 * Adds a new {@link de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel} entry to cart. The
	 * entry can be added as a simple product offering, or also as part of a bundled product offering identified through
	 * the rootBpoCode parameter.
	 *
	 * @param productCodePost
	 *           {@link de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel#CODE}
	 * @param processType
	 *           represents the process flow in the context of which the entry is added to cart (Acquisition, Retention,
	 *           etc.)
	 * @param qty
	 *           quantity to be added;default value is 1
	 * @param rootBpoCode
	 *           {@link de.hybris.platform.b2ctelcoservices.model.TmaBundledProductOfferingModel#CODE} of the root
	 *           Bundled Product Offering, as part of which the
	 *           {@link de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel} is added
	 * @param cartGroupNo
	 *           specifies the cart entries group number where the entry to be added too; if -1, then a separate cart
	 *           entry group is created
	 * @param subscriptionTermId
	 *           specifies the identifier of the new subscription term
	 * @param subscriberIdentity
	 *           represents subscriber identity for an existing customer
	 * @param subscriberBillingId
	 *           represents subscriber billing system id for an existing customer
	 * @param model
	 *           the Spring model
	 * @return the path for AddToCart popup showing the new added entries
	 */
	@RequestMapping(value = "/cart/add", method = RequestMethod.POST, produces = "application/json")
	public String addSpoToCart(@RequestParam("productCodePost") final String productCodePost,
			@RequestParam(value = "processType") final String processType,
			@RequestParam(value = "qty", required = false, defaultValue = "1") final long qty,
		   	@RequestParam(value = "productCodePostList", required = false) final List<String>  productCodePostList,
		   	@RequestParam(value = "qtyList",required = false )final List<Long>  qtyList,
			@RequestParam(value = "planType",required = false) final String planType,
		   	@RequestParam(value = "rootBpoCode", required = false) final String rootBpoCode,
			@RequestParam(value = "cartGroupNo", required = false, defaultValue = "-1") final int cartGroupNo,
			@RequestParam(value = "subscriptionTermId", required = false) final String subscriptionTermId,
			@RequestParam(value = "subscriberIdentity", required = false) final String subscriberIdentity,
			@RequestParam(value = "subscriberBillingId", required = false) final String subscriberBillingId, final Model model)
	{
		if(llaCommonUtil.isSitePanama() && cartFacade.hasSessionCart() && cartFacade.getSessionCart().getEntries().size() > 0) {
			if(checkBundlePlanInCart(productCodePost,Boolean.FALSE,model)) {
   	 		return TelcoControllerConstants.Views.Fragments.Cart.ADD_TO_CART_POPUP; 
			}
		}
		if (qty <= 0) {
			model.addAttribute(ERROR_MSG_TYPE, BASKET_ERROR_QUANTITY_INVALID);
			model.addAttribute(QUANTITY, 0L);
		} else if (CollectionUtils.isNotEmpty(productCodePostList) && productCodePostList.size() > 0) {
			if (validateAddonProductandQuantity(productCodePostList, qtyList, model))
			{
				addPlansWithAddon(productCodePost, productCodePostList, qtyList, planType, qty, processType, rootBpoCode, cartGroupNo, subscriptionTermId,
						subscriberIdentity, subscriberBillingId, model);
			}
		} else {
			addProductOfferingToCart(productCodePost, qty, processType, rootBpoCode, cartGroupNo, subscriptionTermId,
					subscriberIdentity, subscriberBillingId, model);
		}
		model.addAttribute("product",
				getProductFacade().getProductForCodeAndOptions(productCodePost, Collections.singletonList(ProductOption.BASIC)));
		if (llaCommonUtil.isSitePanama()
				&& (llaCommonUtil.containsFirstLevelCategoryWithCode(tmaPoService.getPoForCode(productCodePost), "postpaid")
						|| llaCommonUtil.containsFirstLevelCategoryWithCode(tmaPoService.getPoForCode(productCodePost), "4Pbundles")))
		{
			sessionService.setAttribute("lastAddedPlanCode", productCodePost);
		}
		return TelcoControllerConstants.Views.Fragments.Cart.ADD_TO_CART_POPUP;
	}
	
	private Boolean checkBundlePlanInCart(final String productCodePost, final Boolean isPlanExist, final Model model) {
		String categoryCodes = configurationService.getConfiguration().getString(PANAMA_CATEGORIES_CODES_TO_SHOW_ADDON_LIST);
		List<String> categoryCodesList = Stream.of(categoryCodes.split(",", -1)).collect(Collectors.toList());
		final List<ProductOption> options = new ArrayList<>(Arrays.asList(ProductOption.BASIC,ProductOption.CATEGORIES,ProductOption.PRICE,ProductOption.PRODUCT_OFFERING_PRICES));
		ProductData newProduct = productFacade.getProductForCodeAndOptions(productCodePost, options);
		String newProductCategory = newProduct.getCategories().iterator().next().getCode();
		CartData cart = getCartFacade().getSessionCart();
		 for(OrderEntryData entry : cart.getEntries()) {
			 String category = entry.getProduct().getCategories().iterator().next().getCode();
 			 if(StringUtils.isNotEmpty(category) && StringUtils.isNotEmpty(newProductCategory)
 				 &&  (categoryCodesList.contains(category))
 				 &&  (categoryCodesList.contains(newProductCategory)))
 			 {
 				if(isPlanExist)
   			 {
   				try
					{
					sessionService.setAttribute("lastAddedPlanCode", productCodePost);
   					llaCartFacadeImpl.updateCartEntry(entry.getEntryNumber(), 0L);
					}
					catch (final JaloObjectNoLongerValidException | CommerceCartModificationException ex)
					{
						LOG.error("Could not update quantity of product with entry number: " + entry.getEntryNumber() + ".", ex);
					}
   			 } else {
   				 cart.setIsPlanExist(Boolean.TRUE);
   	   		 model.addAttribute("cartData", cart);
   	   	 	 model.addAttribute("oldProduct", entry.getProduct());
   	   	 	 model.addAttribute("newProduct", newProduct);
   				 return Boolean.TRUE;
   			 }
 			 }
		 }
		 return Boolean.FALSE;
	}
	

	@RequestMapping(value = "/addtocart", method = RequestMethod.GET)
	public String addToCart(@RequestParam(value = "productCodePost", required = false) final String productCodePost,
			@RequestParam(value = "bpoCodePost", required = false) final String bpoCodePost,
			@RequestParam(value = "processType", required = false, defaultValue = "DEVICE_ONLY") final String processType,
			@RequestParam(value = "qty", required = false, defaultValue = "1") final long qty,
			@RequestParam(value = "planType", required = false) final String planType,
			@RequestParam(value = "rootBpoCode", required = false) final String rootBpoCode,
			@RequestParam(value = "cartGroupNo", required = false, defaultValue = "-1") final int cartGroupNo,
			@RequestParam(value = "subscriptionTermId", required = false) final String subscriptionTermId,
			@RequestParam(value = "subscriberIdentity", required = false) final String subscriberIdentity,
			@RequestParam(value = "subscriberBillingId", required = false) final String subscriberBillingId,
			@RequestParam(value = "isPlanExist", required = false) final boolean isPlanExist,
			final Model model, final RedirectAttributes redirectModel) throws CalculationException {
		
		if(llaCommonUtil.isSitePanama() && cartFacade.hasSessionCart() && Boolean.valueOf(isPlanExist)) {
			if(checkBundlePlanInCart(productCodePost,isPlanExist,model)) {
				return addProductToCart(productCodePost, bpoCodePost, processType, qty, planType, rootBpoCode, cartGroupNo, subscriptionTermId,
						subscriberIdentity, subscriberBillingId, model, redirectModel);
			}
		}
		
		return addProductToCart(productCodePost, bpoCodePost, processType, qty, planType, rootBpoCode, cartGroupNo, subscriptionTermId,
				subscriberIdentity, subscriberBillingId, model, redirectModel);
		
	}
	
	private String addProductToCart(final String productCodePost, final String bpoCodePost, final String processType, final long qty, final String planType,
			final String rootBpoCode, final int cartGroupNo, final String subscriptionTermId, final String subscriberIdentity, final String subscriberBillingId,
			final Model model, final RedirectAttributes redirectModel) {	
		
		
		if (!StringUtils.isEmpty(productCodePost))
		{
			if (llaCommonUtil.isSiteCabletica()) {
				final CartModel cart = cartService.getSessionCart();
				final ProductModel product = productService.getProductForCode(productCodePost);
				final CategoryModel category = commerceCategoryService.getCategoryForCode(CHANNEL_ADDON);
				if(null != cart && !cart.getEntries().isEmpty() && !product.getSupercategories().contains(category)) {
					return REDIRECT_TO_PLP_URL+ "?cartRuleAlert=cartRuleAlert";
				}
			}
			if (llaCommonUtil.isSitePanama())
			{
				String categoryCodes = configurationService.getConfiguration().getString(PANAMA_CATEGORIES_CODES_TO_SHOW_ADDON_LIST);
				List<String> categoryCodesList = Stream.of(categoryCodes.split(",", -1)).collect(Collectors.toList());
				CartModel cart = cartService.getSessionCart();
				List<String> addonList = new ArrayList<String>();
				addonList.add(productCodePost);
				List<Long> qtyList = new ArrayList<Long>();
				qtyList.add(qty);
				if (null != cart && !CollectionUtils.isEmpty(cart.getEntries()))
				{
					for (AbstractOrderEntryModel entry : cart.getEntries())
					{
						String category = entry.getProduct().getSupercategories().iterator().next().getCode();
						if (POSTPAID.equals(category))
						{
							addProductOfferingToCart(productCodePost, qty, processType, rootBpoCode, cartGroupNo, subscriptionTermId,
									subscriberIdentity, subscriberBillingId, model);
						}
						for (final String categoryCode : categoryCodesList)
						{
							if (llaCommonUtil.containsFirstLevelCategoryWithCode(entry.getProduct(), categoryCode)
									|| llaCommonUtil.containsSecondLevelCategoryWithCode(entry.getProduct(), categoryCode))
							{
								addProductOfferingToCartForMobilePlans(productCodePost, planType, qty, processType, rootBpoCode,
										cartGroupNo, subscriptionTermId, subscriberIdentity, subscriberBillingId, model);
								associateAddonsWithMasterProducts(entry.getProduct().getCode(), addonList);
							}
						}
					}
				} else {
					addProductOfferingToCart(productCodePost, qty, processType, rootBpoCode, cartGroupNo, subscriptionTermId,
							subscriberIdentity, subscriberBillingId, model);
				}
			} else if (llaCommonUtil.isSitePuertorico())
			{
				return addToCart(productCodePost, qty, processType, rootBpoCode, cartGroupNo, subscriptionTermId,
						subscriberIdentity, subscriberBillingId, model);
			} else {
				addProductOfferingToCart(productCodePost, qty, processType, rootBpoCode, cartGroupNo, subscriptionTermId,
						subscriberIdentity, subscriberBillingId, model);
			}
		}else if (!StringUtils.isEmpty(bpoCodePost)) {
			final TmaBundledProductOfferingModel bpo=tmaPoService.getBpoForCode(bpoCodePost);
			List<String> e= new ArrayList<>();
			bpo.getChildren().stream().forEach(spo -> e.add(spo.getCode()));
			addBpoToCart(e, processType, bpoCodePost, cartGroupNo, subscriptionTermId, subscriberIdentity, subscriberBillingId, model, redirectModel);
		}
	   return REDIRECT_TO_CART_URL;

	}

	@RequestMapping(value = "/cart/addInCart", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity addToCartResponse(@RequestParam(value = "productCodePost", required = false) final String productCodePost,
											@RequestParam(value = "bpoCodePost", required = false) final String bpoCodePost,
											@RequestParam(value = "processType", required = false, defaultValue = "DEVICE_ONLY") final String processType,
											@RequestParam(value = "qty", required = false, defaultValue = "1") final long qty,
											@RequestParam(value = "planType", required = false) final String planType,
											@RequestParam(value = "rootBpoCode", required = false) final String rootBpoCode,
											@RequestParam(value = "cartGroupNo", required = false, defaultValue = "-1") final int cartGroupNo,
											@RequestParam(value = "subscriptionTermId", required = false) final String subscriptionTermId,
											@RequestParam(value = "subscriberIdentity", required = false) final String subscriberIdentity,
											@RequestParam(value = "subscriberBillingId", required = false) final String subscriberBillingId, final Model model, final RedirectAttributes redirectModel) throws CalculationException {
		if (!StringUtils.isEmpty(productCodePost))
		{
			if (llaCommonUtil.isSiteCabletica()) {
				addProductOfferingToCart(productCodePost, qty, processType, rootBpoCode, cartGroupNo, subscriptionTermId,
						subscriberIdentity, subscriberBillingId, model);
			}
		}
		else{
			return ResponseEntity.status(HttpStatus.FAILED_DEPENDENCY).build();
		}
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	private String addToCart(final String spoCode, final long quantity, final String processType, final String rootBpoCode,
			final int cartGroupNo, final String subscriptionTermId, final String subscriberId, final String subscriberBillingId,
			final Model model) 
	{
		final CategoryModel cat = productService.getProduct(spoCode).getSupercategories().iterator().next();
		final CartModel cart = cartService.getSessionCart();
		if (cat.getCode().equalsIgnoreCase("smartproduct"))
		{
			if (CollectionUtils.isNotEmpty(cart.getEntries()))
			{
				final List<AbstractOrderEntryModel> entries = new ArrayList<AbstractOrderEntryModel>();
				for (AbstractOrderEntryModel e : cart.getEntries())
				{
					if (!((e.getProduct().getSupercategories().iterator().next()).getCode()).equalsIgnoreCase("smartproduct"))
					{
						entries.add(e);
					}
				}
				cart.setEntries(entries);
				modelService.save(cart);
			}
		}
		if (spoCode.equals("HD_BOX"))
		{
			for (AbstractOrderEntryModel cartEntry : cart.getEntries())
			{
				if (cartEntry.getProduct().getCode().equals("DVR_BOX"))
				{
					try {
   					llaCartFacadeImpl.updateCartEntry(cartEntry.getEntryNumber(), 0);
   					llaCartFacadeImpl.removeDVRBundleFromCart(cart.getBundleProductMapping());
					}
					catch (final JaloObjectNoLongerValidException | CommerceCartModificationException ex)
					{
						LOG.error("Could not update quantity of product with entry number: " + cartEntry.getEntryNumber() + ".", ex);
					}
				}
			}
		}
		addProductOfferingToCart(spoCode, quantity, processType, rootBpoCode, cartGroupNo, subscriptionTermId, subscriberId,
				subscriberBillingId, model);
		if (spoCode.equals("DVR_BOX"))
		{
			for (AbstractOrderEntryModel cartEntry : cart.getEntries())
			{
				if (cartEntry.getProduct().getCode().equals("HD_BOX"))
				{
					try {
						llaCartFacadeImpl.updateCartEntry(cartEntry.getEntryNumber(), 0);
					}
					catch (final JaloObjectNoLongerValidException | CommerceCartModificationException ex)
					{
						LOG.error("Could not update quantity of product with entry number: " + cartEntry.getEntryNumber() + ".", ex);
					}
				}
			}
			if (null != cart.getBundleProductMapping() && CollectionUtils.isNotEmpty(cart.getBundleProductMapping()))
			{
				llaCartFacadeImpl.filterCartBundles(cart.getBundleProductMapping());
			}
		}
		return REDIRECT_TO_CART_URL;
	}
	
	private boolean validateAddonProductandQuantity(final List<String> productCodePostList, final List<Long> qtyList,
			final Model model)
	{
		final String addonMaxOrderQuantity = getSiteConfigService().getString("productpage.addonproduct.maxperorder.numbers", null);
		final Map<String, Long> addonMaxOrderQuantityMap = Arrays.stream(addonMaxOrderQuantity.split(","))
				.map(s -> s.split(":"))
				.collect(Collectors.toMap(s -> s[0], s -> Long.valueOf(s[1])));
		final Iterator<String> it1 = productCodePostList.iterator();
		final Iterator<Long> it2 = qtyList.iterator();
		while (it1.hasNext() && it2.hasNext()) {
			final Long addonQuantity=it2.next();
			if (addonMaxOrderQuantityMap.get(it1.next()) < addonQuantity) {
				model.addAttribute(ERROR_MSG_TYPE, BASKET_ERROR_QUANTITY_INVALID);
				model.addAttribute(QUANTITY, addonQuantity);
				return false;
			}
		}
		return true;
	}

	private void addPlansWithAddon(final String productCodePost, final List<String> productCodePostList, final List<Long> qtyList,
			final String planType, final long qty, final String processType, final String rootBpoCode, final int cartGroupNo,
			final String subscriptionTermId, final String subscriberIdentity, final String subscriberBillingId, final Model model)
	{

		final Iterator<String> it1 = productCodePostList.iterator();
		final Iterator<Long> it2 = qtyList.iterator();
		while (it1.hasNext() && it2.hasNext()) {
			final String addonCode=it1.next();
			final Long addonQty = it2.next();
			if(addonQty > 0)
			{
				addProductOfferingToCartForMobilePlans(addonCode, planType, addonQty, processType, rootBpoCode, cartGroupNo, subscriptionTermId,
						subscriberIdentity, subscriberBillingId, model);
			}
		}
		final CartModificationData cartModificationData = addProductOfferingToCartForMobilePlans(productCodePost, planType, qty, processType, rootBpoCode, cartGroupNo, subscriptionTermId,
				subscriberIdentity, subscriberBillingId, model);

		associateAddonsWithMasterProducts(productCodePost,productCodePostList);

		model.addAttribute(MODIFIED_CART_DATA, Collections.singleton(cartModificationData));
		model.addAttribute("entry", cartModificationData.getEntry());
		model.addAttribute("cartCode", cartModificationData.getCartCode());
		model.addAttribute(QUANTITY, qty);
	}

		private void associateAddonsWithMasterProducts(final String productCodePost, final List<String> productCodePostList)
	{

		final CartModel cartModel = cartService.getSessionCart();
		if (null != cartModel)
		{
			List<AbstractOrderEntryModel> childEntryList = new ArrayList<>();
			final AbstractOrderEntryModel masterEntry = cartModel.getEntries().stream()
					.filter(e -> e.getProduct().getCode().equals(productCodePost)).findFirst().get();
			if(CollectionUtils.isNotEmpty(masterEntry.getChildEntries())) {
				final List<AbstractOrderEntryModel> entryModels = new ArrayList<AbstractOrderEntryModel>(masterEntry.getChildEntries());
				childEntryList.addAll(entryModels);	
			}
			for (final AbstractOrderEntryModel cartEntry : cartModel.getEntries())
			{
				if (productCodePostList.contains(cartEntry.getProduct().getCode()))
				{
					cartEntry.setMasterEntry(masterEntry);
					childEntryList.add(cartEntry);
				}
			}
			masterEntry.setChildEntries(childEntryList);
		}
	
		modelService.saveAll();
	}
		
		@RequestMapping(value = "/addtocart", method = RequestMethod.POST)
		public String addProdToCart(@RequestParam(value = "productCodePost", required = false) final String productCodePost,
				@RequestParam(value = "bpoCodePost", required = false) final String bpoCodePost,
				@RequestParam(value = "processType", required = false, defaultValue = "DEVICE_ONLY") final String processType,
				@RequestParam(value = "qty", required = false, defaultValue = "1") final long qty,
				@RequestParam(value = "planType", required = false) final String planType,
				@RequestParam(value = "rootBpoCode", required = false) final String rootBpoCode,
				@RequestParam(value = "cartGroupNo", required = false, defaultValue = "-1") final int cartGroupNo,
				@RequestParam(value = "subscriptionTermId", required = false) final String subscriptionTermId,
				@RequestParam(value = "subscriberIdentity", required = false) final String subscriberIdentity,
				@RequestParam(value = "subscriberBillingId", required = false) final String subscriberBillingId, final Model model,
				final RedirectAttributes redirectModel) throws CalculationException
		{
			if (llaCommonUtil.isSiteJamaica() && cartFacade.hasSessionCart() && cartFacade.getSessionCart().getEntries().size() > 0)
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.error.multiple.bundle");
				return REDIRECT_TO_PLP_BUNDLE_PAGE;
			}

			return addProductToCart(productCodePost, bpoCodePost, processType, qty, planType, rootBpoCode, cartGroupNo, subscriptionTermId,
					subscriberIdentity, subscriberBillingId, model, redirectModel);
		}


	@RequestMapping(value = "/cart/add/mobileplans", method = RequestMethod.POST, produces = "application/json")
	public String addMobileWithPlans(@RequestParam("productCodePost") final String productCodePost,
							   @RequestParam(value = "processType") final String processType,
							   @RequestParam(value = "planType") final String planType,
							   @RequestParam(value = "plan") final String plan,
							   @RequestParam(value = "qty", required = false, defaultValue = "1") final long qty,
							   @RequestParam(value = "rootBpoCode", required = false) final String rootBpoCode,
							   @RequestParam(value = "cartGroupNo", required = false, defaultValue = "-1") final int cartGroupNo,
							   @RequestParam(value = "subscriptionTermId", required = false) final String subscriptionTermId,
							   @RequestParam(value = "subscriberIdentity", required = false) final String subscriberIdentity,
							   @RequestParam(value = "subscriberBillingId", required = false) final String subscriberBillingId, final Model model)
	{

		if (qty <= 0)
		{
			model.addAttribute(ERROR_MSG_TYPE, BASKET_ERROR_QUANTITY_INVALID);
			model.addAttribute(QUANTITY, 0L);
		}
		else
		{
			addProductOfferingToCart(plan, qty, processType, rootBpoCode, cartGroupNo, subscriptionTermId,
					subscriberIdentity, subscriberBillingId, model);
			
			if (llaCommonUtil.isSitePanama()
					&& (llaCommonUtil.containsFirstLevelCategoryWithCode(tmaPoService.getPoForCode(plan), "postpaid")
							|| llaCommonUtil.containsFirstLevelCategoryWithCode(tmaPoService.getPoForCode(plan), "4Pbundles")))
			{
				sessionService.setAttribute("lastAddedPlanCode", plan);
			}
			addProductOfferingToCart(productCodePost, qty, processType, rootBpoCode, cartGroupNo, subscriptionTermId,	subscriberIdentity, subscriberBillingId, model);
		}
		model.addAttribute("product",
				getProductFacade().getProductForCodeAndOptions(productCodePost, Collections.singletonList(ProductOption.BASIC)));

		return TelcoControllerConstants.Views.Fragments.Cart.ADD_TO_CART_POPUP;
	}



	/**
	 * Adds multiple {@link de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel} entries to cart, as
	 * part of a bundle product offering specified by the given rootBpoCode. The entries can be added under an existing
	 * cart entries group indicated by the cartGroupNo parameter, or under a new entry group .
	 *
	 * @param simpleProductOfferings
	 *           a list of {@link de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel#CODE}
	 * @param processType
	 *           represents the process flow in the context of which the entry is added to cart (Acquisition, Retention,
	 *           etc.)
	 * @param rootBpoCode
	 *           {@link de.hybris.platform.b2ctelcoservices.model.TmaBundledProductOfferingModel#CODE} of the root
	 *           Bundled Product Offering, as part of which the
	 *           {@link de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel} are added
	 * @param cartGroupNo
	 *           specifies the cart entries group number where the entries to be added; if -1, then a separate cart entry
	 *           group is created
	 * @param model
	 *           the Spring model
	 * @return the link to cart page
	 */
	@RequestMapping(value = "/cart/addBpo", method = RequestMethod.POST)
	public String addBpoToCart(@RequestParam(value = "simpleProductOfferings") final List<String> simpleProductOfferings,
			@RequestParam(value = "processType") final String processType,
			@RequestParam(value = "rootBpoCode") final String rootBpoCode,
			@RequestParam(value = "cartGroupNo", required = false, defaultValue = "-1") final int cartGroupNo,
			@RequestParam(value = "subscriptionTermId", required = false) final String subscriptionTermId,
			@RequestParam(value = "subscriberIdentity", required = false) final String subscriberIdentity,
			@RequestParam(value = "subscriberBillingId", required = false) final String subscriberBillingId,
							   final Model model,final RedirectAttributes redirectModel)
	{

		try
		{
			final List<CartModificationData> cartModifications =  new ArrayList();
			if(llaCommonUtil.isSitePuertorico() && cartFacade.hasSessionCart() && cartFacade.getSessionCart().getEntries().size() > 0){
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.error.multiple.bundle");
				return REDIRECT_TO_HOMEPAGE;
			}
		   for(String productCodePost:simpleProductOfferings ) {
			 cartModifications.add(addProductOfferingToCart(productCodePost, DEFAULT_QUANTITY, processType, rootBpoCode, cartGroupNo, subscriptionTermId,
						subscriberIdentity, subscriberBillingId, model));
			}
			addProductMappingToCart(simpleProductOfferings);
			model.addAttribute(MODIFIED_CART_DATA, cartModifications);
			for (final CartModificationData cartModification : cartModifications)
			{
				if (cartModification.getEntry() == null)
				{
					GlobalMessages.addErrorMessage(model, BASKET_INFORMATION_QUANTITY_NO_ITEMS_ADDED);
					model.addAttribute(ERROR_MSG_TYPE, BASKET_INFORMATION_QUANTITY_NO_ITEMS_ADDED);
					throw new CommerceCartModificationException(CART_ENTRY_WAS_NOT_CREATED_REASON + cartModification.getStatusCode());
				}
			}
		}
		catch (final CommerceCartModificationException | CalculationException ex)
		{
			model.addAttribute(ERROR_MSG_TYPE, BASKET_ERROR_OCCURRED);
			LOG.error("Couldn't add BPO of code " + rootBpoCode + " and spos with codes" + simpleProductOfferings, ex);
		}
		return REDIRECT_PREFIX + CART;
	}
	
	private void addProductMappingToCart(List<String> simpleProductOfferings) throws CalculationException {
		final LLAPuertoRicoProductConfigDTO productConfig = getLlaPuertoRicoProductConfig(simpleProductOfferings);
		sessionService.setAttribute(LlatelcoaddonWebConstants.PRODUCT_CONFIG, productConfig);
		List<LLABundleProductMappingModel> mapping=llaCartFacadeImpl.getBundleProductMappingForCart(productConfig);
		CartModel cart=cartService.getSessionCart();
		cart.setBundleProductMapping(mapping);
		calculationService.recalculate(cart);
		cart.setIsTelephoneFree(llaCommonUtil.isTelephoneFree(cart));
		modelService.save(cart);
		modelService.refresh(cart);
	}

	/**
	 * Adds a new {@link de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel} entry to cart for the
	 * BPO. The entry can be added as a simple product offering as a part of a bundled product offering identified
	 * through the rootBpoCode parameter and group number if any
	 *
	 * @param productCodePost
	 *           {@link de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel#CODE}
	 * @param qty
	 *           quantity to be added;default value is 1
	 * @param processType
	 *           represents the process flow in the context of which the entry is added to cart (Acquisition, Retention,
	 *           etc.)
	 * @param rootBpoCode
	 *           {@link de.hybris.platform.b2ctelcoservices.model.TmaBundledProductOfferingModel#CODE} of the root
	 *           Bundled Product Offering, as part of which the
	 *           {@link de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel} is added
	 * @param currentStep
	 *           Step id or the group Id from where the SPO is added to the BPO
	 * @param rootGroupNumber
	 *           specifies the cart entries group number where the entry to be added too; if -1, then a separate cart
	 *           entry group is created
	 * @param model
	 *           the Spring model
	 * @param query
	 *           The applied query on page
	 * @param page
	 *           The page number from which the product is added to the cart
	 * @return the path for bpo guided selling for the group appended with the group number
	 */
	@RequestMapping(value = "/cart/addSpo", method = RequestMethod.POST, produces = "application/json")
	public String addSpoToBpoConfiguration(@RequestParam("productCodePost") final String productCodePost,
			@RequestParam(value = "qty", required = false, defaultValue = "1") final long qty,
			@RequestParam(value = "processType") final String processType,
			@RequestParam(value = "rootBpoCode") final String rootBpoCode,
			@RequestParam(value = "currentStep", required = false) final String currentStep,
			@RequestParam(value = "rootGroupNumber", required = false, defaultValue = "-1") final int rootGroupNumber,
			final Model model, final RedirectAttributes redirectModel,
			@RequestParam(value = "q", required = false) final String query,
			@RequestParam(value = "page", required = false) final String page)
	{

		try
		{
			Integer groupNumber = null;
			EntryGroup rootEntryGroup = new EntryGroup();
			if (qty <= 0)
			{
				model.addAttribute(ERROR_MSG_TYPE, BASKET_ERROR_QUANTITY_INVALID);
				model.addAttribute(QUANTITY, Long.valueOf(0L));
			}
			else
			{
				final CartModificationData cartModification = addProductOfferingToCart(productCodePost, qty, processType, rootBpoCode,
						rootGroupNumber, Strings.EMPTY, Strings.EMPTY, Strings.EMPTY, model);

				if (cartModification.getEntry() == null || !cartModification.getStatusCode().equals(SUCCESS))
				{
					GlobalMessages.addErrorMessage(model, BASKET_INFORMATION_QUANTITY_NO_ITEMS_ADDED);
					model.addAttribute(ERROR_MSG_TYPE, BASKET_INFORMATION_QUANTITY_NO_ITEMS_ADDED);
					throw new CommerceCartModificationException(CART_ENTRY_WAS_NOT_CREATED_REASON + cartModification.getStatusCode());
				}

				for (final Integer parentGroup : cartModification.getEntry().getEntryGroupNumbers())
				{
					groupNumber = parentGroup;
				}
				rootEntryGroup = getEntryGroupFacade().getRootEntryGroup(groupNumber);

				model.addAttribute("groupNumber", rootEntryGroup.getGroupNumber());
			}
			model.addAttribute("product",
					getProductFacade().getProductForCodeAndOptions(productCodePost, Collections.singletonList(ProductOption.BASIC)));
			return this.extractJourneyRedirectUrl(XSSFilterUtil.filter(rootBpoCode), rootEntryGroup,
					XSSFilterUtil.filter(currentStep), XSSFilterUtil.filter(page), XSSFilterUtil.filter(query));
		}
		catch (final CommerceCartModificationException ex)
		{
			model.addAttribute(ERROR_MSG_TYPE, BASKET_ERROR_OCCURRED);
			LOG.error("Couldn't add SPO of code " + productCodePost + " as part of " + rootBpoCode, ex);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, BASKET_ERROR_OCCURRED);
			return REDIRECT_PREFIX + BPO_CONFIGURE_PATH + XSSFilterUtil.filter(rootBpoCode) + "/" + XSSFilterUtil.filter(currentStep)
					+ "/" + rootGroupNumber;
		}
	}

	private CartModificationData addProductOfferingToCart(final String spoCode, final long quantity, final String processType,
			final String rootBpoCode, final int cartGroupNo, final String subscriptionTermId, final String subscriberId,
			final String subscriberBillingId, final Model model)
	{
		CartModificationData cartModification = new CartModificationData();

		try
		{
			cartModification = getTmaCartFacade().addProductOfferingToCart(spoCode, quantity, processType, rootBpoCode, cartGroupNo,
					subscriptionTermId, subscriberId, subscriberBillingId);

			model.addAttribute(MODIFIED_CART_DATA, Collections.singleton(cartModification));

			if (cartModification.getQuantityAdded() == 0L)
			{
				GlobalMessages.addErrorMessage(model, "basket.information.quantity.noItemsAdded." + cartModification.getStatusCode());
				model.addAttribute(ERROR_MSG_TYPE, "basket.information.quantity.noItemsAdded." + cartModification.getStatusCode());
			}
			else if (cartModification.getQuantityAdded() < quantity)
			{
				GlobalMessages.addErrorMessage(model,
						"basket.information.quantity.reducedNumberOfItemsAdded." + cartModification.getStatusCode());
				model.addAttribute(ERROR_MSG_TYPE,
						"basket.information.quantity.reducedNumberOfItemsAdded." + cartModification.getStatusCode());

			}

			model.addAttribute("entry", cartModification.getEntry());
			model.addAttribute("cartCode", cartModification.getCartCode());
			model.addAttribute(QUANTITY, quantity);
		}
		catch (final CommerceCartModificationException ex)
		{
			model.addAttribute(ERROR_MSG_TYPE, BASKET_ERROR_OCCURRED);
			LOG.warn("Couldn't add product of code " + spoCode + " to cart.", ex);
		}
		final CartData cartData = getCartFacade().getSessionCart();
		model.addAttribute("cartData", cartData);
		return cartModification;
	}

	private CartModificationData addProductOfferingToCartForMobilePlans(final String spoCode, final String planType ,final long quantity,final String processType,
														  final String rootBpoCode, final int cartGroupNo, final String subscriptionTermId, final String subscriberId,
														  final String subscriberBillingId, final Model model)
	{
		CartModificationData cartModification = new CartModificationData();

		try
		{
			cartModification = llaCartFacadeImpl.addProductOfferingToCartForMobiles(spoCode, planType,quantity,processType, rootBpoCode, cartGroupNo,
					subscriptionTermId, subscriberId, subscriberBillingId);

			model.addAttribute(MODIFIED_CART_DATA, Collections.singleton(cartModification));

			if (cartModification.getQuantityAdded() == 0L)
			{
				GlobalMessages.addErrorMessage(model, "basket.information.quantity.noItemsAdded." + cartModification.getStatusCode());
				model.addAttribute(ERROR_MSG_TYPE, "basket.information.quantity.noItemsAdded." + cartModification.getStatusCode());
			}
			else if (cartModification.getQuantityAdded() < quantity)
			{
				GlobalMessages.addErrorMessage(model,
						"basket.information.quantity.reducedNumberOfItemsAdded." + cartModification.getStatusCode());
				model.addAttribute(ERROR_MSG_TYPE,
						"basket.information.quantity.reducedNumberOfItemsAdded." + cartModification.getStatusCode());

			}

			model.addAttribute("entry", cartModification.getEntry());
			model.addAttribute("cartCode", cartModification.getCartCode());
			model.addAttribute(QUANTITY, quantity);
		}
		catch (final CommerceCartModificationException ex)
		{
			model.addAttribute(ERROR_MSG_TYPE, BASKET_ERROR_OCCURRED);
			LOG.warn("Couldn't add product of code " + spoCode + " to cart.", ex);
		}
		final CartData cartData = getCartFacade().getSessionCart();
		model.addAttribute("cartData", cartData);
		return cartModification;
	}

	/**
	 * Removes the SPO added to the journey step
	 *
	 * @param entryNumber
	 *           entry number of the cart entry to be updated
	 * @param groupId
	 *           The groupId of the journey
	 * @param bpoCode
	 *           The parent BPO code for the journey
	 * @param cartGroupNumber
	 *           specifies the cart entries group number where the entry is to be removed from
	 * @param model
	 *           page model to be populated with information
	 * @param form
	 *           update quantity form specifying the new quantity of the product from the entry with the number given
	 * @param bindingResult
	 *           request binding result to retrieve validation errors from
	 * @param redirectModel
	 *           redirect model to be populated with information
	 * @return redirect url to the journey page
	 */

	@RequestMapping(value = "/cart/removeSpo", method = RequestMethod.POST)
	public String removeSpoFromJourneyStep(@RequestParam("entryNumber") final long entryNumber,
			@RequestParam("bpoCode") final String bpoCode, @RequestParam("groupId") final String groupId,
			@RequestParam("cartGroupNumber") final String cartGroupNumber, final Model model, @Valid final UpdateQuantityForm form,
			final BindingResult bindingResult, final RedirectAttributes redirectModel)
	{

		if (bindingResult.hasErrors())
		{
			populatePageModelWithErrors(model, bindingResult);
		}
		else if (getCartFacade().hasEntries())
		{
			try
			{
				final long newQuantity = form.getQuantity().longValue();
				final CartModificationData cartModification = getCartFacade().updateCartEntry(entryNumber, newQuantity);
				String alertMessageCode;
				if (cartModification.getQuantity() == newQuantity)
				{
					// Success in either removal or updating the quantity
					alertMessageCode = cartModification.getQuantity() == 0 ? "basket.page.message.remove"
							: "basket.page.message.update";
					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, alertMessageCode);
				}
			}
			catch (final JaloObjectNoLongerValidException | CommerceCartModificationException ex)
			{
				LOG.error("Could not update quantity of product with entry number: " + entryNumber + ".", ex);
			}
		}
		return REDIRECT_PREFIX + BPO_CONFIGURE_PATH + XSSFilterUtil.filter(bpoCode) + "/" + XSSFilterUtil.filter(groupId) + "/"
				+ XSSFilterUtil.filter(cartGroupNumber);
	}

	@RequestMapping(value = "/cart/preconfig/{preConfig}", method = RequestMethod.GET)
	public String addpreConfig(@PathVariable("preConfig") final String preConfig, final Model model,
			final RedirectAttributes redirectModel)
	{
		try
		{
			final TmaBpoPreConfigData tmaBpoPreConfigData = getTmaBpoPreConfigFacade().getPreConfigBpo(preConfig);
			if (tmaBpoPreConfigData != null && CollectionUtils.isNotEmpty(tmaBpoPreConfigData.getSpoList()))
			{
				final List<CartModificationData> cartModifications = getTmaCartFacade().addBpoSelectedOfferings(
						tmaBpoPreConfigData.getRootBpoCode(), tmaBpoPreConfigData.getSpoList(), TmaProcessType.ACQUISITION.getCode(),
						-1, Strings.EMPTY, Strings.EMPTY, Strings.EMPTY);
				model.addAttribute(MODIFIED_CART_DATA, cartModifications);

				for (final CartModificationData cartModification : cartModifications)
				{
					if (cartModification.getEntry() == null)
					{
						throw new CommerceCartModificationException(
								CART_ENTRY_WAS_NOT_CREATED_REASON + cartModification.getStatusCode());
					}
				}
			}

		}

		catch (final ModelNotFoundException | IllegalArgumentException modelNotFoundException)
		{
			LOG.info(modelNotFoundException);
			redirectModel.addFlashAttribute(INVALID_PRE_CONFIG_CALL, BPO_PRECONFIG_INVALID_ERROR_MESSAGE);
		}
		catch (final CommerceCartModificationException cartModificationException)
		{
			LOG.info(cartModificationException);
			redirectModel.addFlashAttribute(INVALID_PRE_CONFIG_CALL, BASKET_ERROR_OCCURRED);
		}
		return REDIRECT_PREFIX + CART;
	}


	private void populatePageModelWithErrors(final Model model, final BindingResult bindingResult)
	{
		for (final ObjectError error : bindingResult.getAllErrors())
		{
			final String errorMessageCode = "".equals(error.getCode()) ? BASKET_ERROR_QUANTITY_INVALID : error.getDefaultMessage();
			GlobalMessages.addErrorMessage(model, errorMessageCode);
		}
	}
	
	/**
	 * This generates the redirect url for the BPO guided selling journey , if the current Step is empty it redirects to
	 * the TmaEditEntryGroupController
	 *
	 * @param rootBpoCode
	 *           {@link de.hybris.platform.b2ctelcoservices.model.TmaBundledProductOfferingModel#CODE} of the root
	 *           Bundled Product Offering, as part of which the
	 *           {@link de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel} is added
	 * @param currentStep
	 *           The Current step or the Group of
	 *           {@link de.hybris.platform.b2ctelcoservices.model.TmaBundledProductOfferingModel} of the root Bundled
	 *           Product Offering, from where
	 *           {@link de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel} is added
	 * @param rootEntryGroup
	 *           The {@link de.hybris.platform.core.order.EntryGroup} to which
	 *           {@link de.hybris.platform.b2ctelcoservices.model.TmaBundledProductOfferingModel} is added to the cart
	 * @param page
	 *           The page number from which the product is added to the cart
	 * @param query
	 *           The applied query on page
	 * @return redirect url String
	 */
	private String extractJourneyRedirectUrl(final String rootBpoCode, final EntryGroup rootEntryGroup, final String currentStep,
			final String page, final String query)
	{
		if (!StringUtils.isBlank(currentStep))
		{

			if (!StringUtils.isBlank(page))
			{
				return REDIRECT_PREFIX + BPO_CONFIGURE_PATH + rootBpoCode + "/" + currentStep + "/" + rootEntryGroup.getGroupNumber()
						+ "?q=" + query + "&page=" + page;

			}
			return REDIRECT_PREFIX + BPO_CONFIGURE_PATH + rootBpoCode + "/" + currentStep + "/" + rootEntryGroup.getGroupNumber();
		}
		return REDIRECT_PREFIX + "/bpo/edit/configure/" + rootBpoCode + "/" + rootEntryGroup.getGroupNumber();
	}

	protected CartFacade getCartFacade()
	{
		return cartFacade;
	}

	protected void setCartFacade(final CartFacade cartFacade)
	{
		this.cartFacade = cartFacade;
	}

	protected ProductFacade getProductFacade()
	{
		return productFacade;
	}

	public void setProductFacade(final ProductFacade productFacade)
	{
		this.productFacade = productFacade;
	}

	protected TmaCartFacade getTmaCartFacade()
	{
		return tmaCartFacade;
	}

	public void setTmaCartFacade(final TmaCartFacade tmaCartFacade)
	{
		this.tmaCartFacade = tmaCartFacade;
	}

	protected EntryGroupFacade getEntryGroupFacade()
	{
		return entryGroupFacade;
	}

	public void setEntryGroupFacade(final EntryGroupFacade entryGroupFacade)
	{
		this.entryGroupFacade = entryGroupFacade;
	}

	public TmaBpoPreConfigFacade getTmaBpoPreConfigFacade()
	{
		return tmaBpoPreConfigFacade;
	}

	public void setTmaBpoPreConfigFacade(final TmaBpoPreConfigFacade tmaBpoPreConfigFacade)
	{
		this.tmaBpoPreConfigFacade = tmaBpoPreConfigFacade;
	}
	public SiteConfigService getSiteConfigService() {
		return siteConfigService;
	}

	@Required
	public void setSiteConfigService(final SiteConfigService siteConfigService)
	{
		this.siteConfigService = siteConfigService;
	}

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		binder.setDisallowedFields(DISALLOWED_FIELDS);
	}
	
	private LLAPuertoRicoProductConfigDTO getLlaPuertoRicoProductConfig(List<String> simpleProductOfferings) {
		LLAPuertoRicoProductConfigDTO productConfig = new LLAPuertoRicoProductConfigDTO();
    	for(String code : simpleProductOfferings) {
			ProductModel model = productService.getProductForCode(code);
			if (model.getSupercategories().iterator().next().getCode().equalsIgnoreCase("internet"))
				productConfig.setOneP(code);
			else if (model.getSupercategories().iterator().next().getCode().equalsIgnoreCase("tv"))
				productConfig.setTwoP(code);
			else
				productConfig.setThreeP(code);
		}
		return productConfig;
	}
}
