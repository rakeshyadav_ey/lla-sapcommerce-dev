package com.lla.telcoaddon.controllers.pages.checkout.steps;

import com.lla.core.enums.*;
import com.lla.core.util.LLACommonUtil;
import com.lla.core.util.LLAEncryptionUtil;
import com.lla.facades.cart.LLACartFacade;
import com.lla.facades.checkout.LLACheckoutFacade;
import com.lla.facades.customer.LLACustomerFacade;
import com.lla.facades.customer.data.DocumentTypeData;
import com.lla.facades.customer.data.DriverLicenceStateData;
import com.lla.facades.product.data.GenderData;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.service.LLAMulesoftDebtCheckApiService;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import com.lla.telcoaddon.controllers.ControllerConstants;
import com.lla.telcoaddon.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import com.lla.telcoaddon.controllers.util.CartHelper;
import com.lla.telcoaddon.forms.CustomerProfileForm;
import com.lla.telcoaddon.forms.validation.CustomerProfileFormValidator;
import com.lla.telcoaddon.service.LLAPersonalDetailService;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.lla.mulesoft.integration.response.LLAApiResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.type.TypeService;

@Controller
@RequestMapping(value = "/checkout/multi/profile-form")
public class ProfileFormCheckoutStepController  extends AbstractCheckoutStepController{
    private static final Logger LOG = LoggerFactory.getLogger(com.lla.telcoaddon.controllers.pages.checkout.steps.ProfileFormCheckoutStepController.class);
    protected static final String PROFILE_FORM = "profile-form";
    protected static final String ATTR_CART_DATA = "cartData";
    protected static final String CURRENT_STEP = "current-step";
    private static final String REDIRECT_TO_CALLBACK_URL = REDIRECT_PREFIX + "/callBack";
    private static final String REDIRECT_TO_ERROR_URL = REDIRECT_PREFIX + "/error";
    private final String[] DISALLOWED_FIELDS = new String[] {};

    protected static final String SSN_MASKED_VALUE = "ssn_masked";

    @Resource(name = "userFacade")
    private UserFacade userFacade;
    
    @Resource(name = "llaCartFacade")
 	private LLACartFacade llaCartFacade;

    @Autowired
    private Converter<Gender, GenderData> genderConverter;
    @Resource(name = "customerProfileFormValidator")
    private CustomerProfileFormValidator customerProfileFormValidator;
    @Resource(name="userService")
    private UserService userService;
    @Resource(name = "sessionService")
    private SessionService sessionService;
    @Resource(name = "customerFacade")
    private LLACustomerFacade llaCustomerFacade;
    @Resource(name = "checkoutFacade")
    private LLACheckoutFacade llaCheckoutFacade;
    @Autowired
    private LLACommonUtil llaCommonUtil;
    @Autowired
    private EnumerationService enumerationService;
    @Autowired
    private TypeService typeService;
    @Autowired
    private LLAPersonalDetailService llaPersonalDetailService;

    @Autowired
    private LLAEncryptionUtil llaEncryptionUtil;
    @Autowired
   	private CartService cartService;
   	@Autowired
   	private ConfigurationService configurationService;
   	@Autowired
   	private LLAMulesoftNotificationService llaMulesoftNotificationService;
   	@Autowired
    private LLAMulesoftDebtCheckApiService llaMulesoftDebtCheckApiService;
   	@Autowired
    private ModelService modelService;
    @ModelAttribute("titles")
    public Collection<TitleData> getTitles()
    {
        return userFacade.getTitles();
    }

     @ModelAttribute("genders")
    public List<GenderData> getGenders()
    {
        List<GenderData> genderCollection=new ArrayList<>();
        genderCollection.add(genderConverter.convert(Gender.valueOf("MALE")));
        genderCollection.add(genderConverter.convert(Gender.valueOf("FEMALE")));
        return genderCollection;
    }
    @ModelAttribute("documentTypes")
    public List<DocumentTypeData> getDocumentType()
    {

        List<DocumentTypeData> documentTypeDataList=llaCommonUtil.getCustomerDocumentTypes();
        if(llaCommonUtil.isSitePanama()){
            List<DocumentTypeData> refinedDocumentTypeDataList = documentTypeDataList.stream().filter(documentType -> !documentType.getCode().toString().equalsIgnoreCase(DocumentType.DRIVER_LICENSE.getCode())).collect(Collectors.toList());
            refinedDocumentTypeDataList=refinedDocumentTypeDataList.stream().filter(documentType -> !documentType.getCode().toString().equalsIgnoreCase(DocumentType.OTHER.getCode())).collect(Collectors.toList());
            refinedDocumentTypeDataList=refinedDocumentTypeDataList.stream().filter(documentType -> !documentType.getCode().toString().equalsIgnoreCase(DocumentType.CEDULARESIDENTIAL.getCode())).collect(Collectors.toList());
            refinedDocumentTypeDataList=refinedDocumentTypeDataList.stream().filter(documentType -> !documentType.getCode().toString().equalsIgnoreCase(DocumentType.CEDULANATIONAL.getCode())).collect(Collectors.toList());
            refinedDocumentTypeDataList.sort(Comparator.comparing(DocumentTypeData::getCode));
            return refinedDocumentTypeDataList;
        } else if(llaCommonUtil.isSitePuertorico()){
            List<DocumentTypeData> refinedDocumentTypeDataList = documentTypeDataList.stream().filter(documentType -> !documentType.getCode().toString().equalsIgnoreCase(DocumentType.CEDULA.getCode())).collect(Collectors.toList());
            refinedDocumentTypeDataList =refinedDocumentTypeDataList.stream().filter(documentType -> !documentType.getCode().toString().equalsIgnoreCase(DocumentType.PASSPORT.getCode())).collect(Collectors.toList());
            refinedDocumentTypeDataList=refinedDocumentTypeDataList.stream().filter(documentType -> !documentType.getCode().toString().equalsIgnoreCase(DocumentType.CEDULARESIDENTIAL.getCode())).collect(Collectors.toList());
            refinedDocumentTypeDataList=refinedDocumentTypeDataList.stream().filter(documentType -> !documentType.getCode().toString().equalsIgnoreCase(DocumentType.CEDULANATIONAL.getCode())).collect(Collectors.toList());
            List<DocumentTypeData> sortedCars = refinedDocumentTypeDataList.stream().sorted(Comparator.comparing(
                    DocumentTypeData::getCode,
                    Comparator.comparing((String x) -> x.equals("OTHER"))
                            .thenComparing(Comparator.naturalOrder())))
                    .collect(Collectors.toList());
            return refinedDocumentTypeDataList;
        }
        return ListUtils.EMPTY_LIST;
    }

    @ModelAttribute("driverLicenceStates")
    public List<DriverLicenceStateData> getDriverLicenceState()
    {
        return llaCommonUtil.getDriverLicenceStates();
    }

    @ModelAttribute("customerPresenceInBSS")
    public boolean validateCustomerExistenceInBSS(){
        CustomerModel customerModel=(CustomerModel)userService.getCurrentUser();
        return null!=customerModel.getPresenceInBSS()?customerModel.getPresenceInBSS():Boolean.FALSE;

    }
    @Override
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    @RequireHardLogIn
    @PreValidateCheckoutStep(checkoutStep = PROFILE_FORM)
    public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
    {
   	 if(llaCommonUtil.isSiteJamaica()){
 			CartData cartData=getCartFacade().getSessionCart();
 			if(cartData.getEntries().size() >= 2 && llaCommonUtil.cartContainsSameProduct(cartData)){
 				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, "cart.entries.product.quantity.error", null);
 				return REDIRECT_PREFIX + "/cart";
 			}
 			if(cartData.getEntries().size() >= 2 && !llaCommonUtil.cartContainsEntiresOfDistinctCategory(cartData)){
 				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, "cart.entries.quantity.error", null);
 				return REDIRECT_PREFIX + "/cart";
 			}
 		}
   	   if(llaCommonUtil.isSitePuertorico()){
   	       CartModel cartModel=cartService.getSessionCart();
   	       llaCommonUtil.updateCreditScoreDetails(cartModel);
       }

        return getPersonalDetailForm(model);
    }
    @RequestMapping(value = "/update", method = RequestMethod.GET)
    @RequireHardLogIn
    @PreValidateCheckoutStep(checkoutStep = PROFILE_FORM)
    public String enterStepPost(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
    {

        return getPersonalDetailForm(model);
   }


    private String getPersonalDetailForm(Model model) throws CMSItemNotFoundException {
        prepareDataForPage(model);
        CustomerModel customerModel=(CustomerModel)userService.getCurrentUser();
        model.addAttribute("trnNumber",customerModel.getTrnNumber());
        final CartData cartData = llaCheckoutFacade.getCheckoutCart();
        cartData.setEntries(CartHelper.removeEmptyEntries(cartData.getEntries()));
        model.addAttribute(ATTR_CART_DATA, cartData);
        populateProfileForm(customerModel, model);
        storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        if(!llaCommonUtil.isSiteJamaica()){
            model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.profileform.breadcrumb"));
        }
        model.addAttribute("metaRobots", "noindex,nofollow");
        model.addAttribute("selectedPlanData",llaCartFacade.getSelectedPlanInOrder());
        setCheckoutStepLinksForModel(model, getCheckoutStep());

        return ControllerConstants.Views.Pages.MultiStepCheckout.AddAdditionalProfileDetailsForm;
    }

    private void populateProfileForm(final CustomerModel customerModel,final Model model) {
        CustomerProfileForm customerProfileForm=new CustomerProfileForm();
        if(llaCommonUtil.isSiteJamaica())
        {
            customerProfileForm.setTrnNumber(customerModel.getTrnNumber());
            populateDOB(customerModel, customerProfileForm);
            if(null != customerModel.getGender() && null != customerModel.getGender().getCode()){
            customerProfileForm.setGender(customerModel.getGender().getCode());
             }
            if(null != customerModel.getTitle() && null != customerModel.getTitle().getCode()){
                customerProfileForm.setTitleCode(customerModel.getTitle().getCode());
            }
            //prePopulateAdditionalEmail(customerModel, customerProfileForm);
            populateCommonDetails(customerModel,customerProfileForm);
        }


        if(llaCommonUtil.isSitePanama() || llaCommonUtil.isSiteCabletica()){
            populatePanamaDetails(customerModel,customerProfileForm);
        }
        if(llaCommonUtil.isSitePuertorico()){
      	    populatePuertoricoDOB(customerModel, customerProfileForm);
           // populateDOB(customerModel, customerProfileForm);
            populateCommonDetails(customerModel,customerProfileForm);
        }


        model.addAttribute("customerProfileForm",customerProfileForm);
    }

    /**
	 * @param customerModel
	 * @param customerProfileForm
	 */
	private void populatePuertoricoDOB(CustomerModel customerModel, CustomerProfileForm customerProfileForm)
	{DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
   if(null != customerModel.getDateOfBirth()){
      String dob = dateFormat.format(customerModel.getDateOfBirth());
      customerProfileForm.setDob(dob);
  }}

	private void populateDOB(CustomerModel customerModel, CustomerProfileForm customerProfileForm) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if(null != customerModel.getDateOfBirth()){
            String dob = dateFormat.format(customerModel.getDateOfBirth());
            customerProfileForm.setDob(dob);
        }
    }

    private void populatePanamaDetails(CustomerModel customerModel, CustomerProfileForm form) {
        if(StringUtils.isNotEmpty(customerModel.getMobilePhoneSecondary())){
            form.setMobilePhoneSecondary(customerModel.getMobilePhoneSecondary());
        }
        populateCommonDetails(customerModel, form);
    }

    private void populateCommonDetails(CustomerModel customerModel, CustomerProfileForm form) {
        if(StringUtils.isNotEmpty(customerModel.getFirstName())){
            form.setFirstName(customerModel.getFirstName());
        }
        if(StringUtils.isNotEmpty(customerModel.getLastName())){
            form.setLastName(customerModel.getLastName());
        }
        if(StringUtils.isNotEmpty(customerModel.getFixedPhone())){
            form.setFixedPhone(customerModel.getFixedPhone());
        }
        if(StringUtils.isNotEmpty(customerModel.getMobilePhone())){
            form.setMobilePhone(customerModel.getMobilePhone());
        }
        if(StringUtils.isNotEmpty(customerModel.getDocumentType())){
            form.setDocumentType(customerModel.getDocumentType());
        }
        if(StringUtils.isNotEmpty(customerModel.getDocumentNumber())){
            form.setDocumentNumber(customerModel.getDocumentNumber());
        }
        if(StringUtils.isNotEmpty(customerModel.getAdditionalEmail())){
            form.setAdditionalEmail(customerModel.getAdditionalEmail());
        }
	if(StringUtils.isNotEmpty(customerModel.getSecondName())){
            form.setSecondName(customerModel.getSecondName());
        }
        if(StringUtils.isNotEmpty(customerModel.getSecondSurname())){
            form.setSecondSurname(customerModel.getSecondSurname());
        }
        if(StringUtils.isNotEmpty(customerModel.getDriverLicenceState())){
            form.setDriverLicenceState(customerModel.getDriverLicenceState());
        }else {
            form.setDriverLicenceState(DriverLicenceState.PUERTO_RICO.getCode().toString());
        }
//        if(customerModel.getCheckSSN() == false && customerModel.getCustomerSSN() == null){
//            form.setCheckSSN(true);
//        }else {
//            form.setCheckSSN(customerModel.getCheckSSN());
//        }
        if(StringUtils.isNotEmpty(customerModel.getEncryptedSSN())){
            form.setSsn(llaEncryptionUtil.doDecryption(customerModel.getEncryptedSSN()));    
        }
        else{
            prePopulateAdditionalEmail(customerModel, form);
        }
    }

    private void prePopulateAdditionalEmail(CustomerModel customerModel, CustomerProfileForm form) {
        if(CustomerType.GUEST.equals(customerModel.getType())){
            form.setAdditionalEmail(StringUtils.substringAfter(customerModel.getUid(),"|"));
        }else{
            form.setAdditionalEmail(customerModel.getUid());
        }
    }


    @RequestMapping(value = "/back", method = RequestMethod.GET)
    @RequireHardLogIn
    @Override
    public String back(final RedirectAttributes redirectAttributes)
    {
        return getCheckoutStep().previousStep();
    }

    @RequestMapping(value = "/next", method = RequestMethod.GET)
    @RequireHardLogIn
    @Override
    public String next(final RedirectAttributes redirectAttributes)
    {
        return getCheckoutStep().nextStep();
    }

    @InitBinder
    public void initBinder(final WebDataBinder binder)
    {
        binder.setDisallowedFields(DISALLOWED_FIELDS);
    }


    protected CheckoutStep getCheckoutStep()
    {
        return getCheckoutStep(PROFILE_FORM);
    }

    @RequestMapping(value="/update", method=RequestMethod.POST)
    public String addAdditionalCustomerProfileDetails(final CustomerProfileForm form,
                                                      BindingResult bindingResult, final Model model, final HttpServletRequest request,
                                                      final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException {

        CustomerModel customerModel=(CustomerModel)userService.getCurrentUser();
        customerProfileFormValidator.validate(form,bindingResult);
        customerProfileFormValidator.contactDetailsValidate(form,bindingResult);
        //customerModel.setCheckSSN(form.isCheckSSN());
        modelService.save(customerModel);
        modelService.refresh(customerModel);
        if (bindingResult.hasErrors())
        {
            prepareDataForPage(model);
            final CartData cartData = llaCheckoutFacade.getCheckoutCart();
            cartData.setEntries(CartHelper.removeEmptyEntries(cartData.getEntries()));
            model.addAttribute(ATTR_CART_DATA, cartData);
            GlobalMessages.addErrorMessage(model, "profile.error.formentry.invalid");
            storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
            setCheckoutStepLinksForModel(model, getCheckoutStep());
            return ControllerConstants.Views.Pages.MultiStepCheckout.AddAdditionalProfileDetailsForm;
        }
        processNewCustomerAdditionalDetails(customerModel,form);
        if (StringUtils.isNotEmpty(form.getAccountNumber()) && llaCommonUtil.isSiteJamaica())
  		  {
      	  llaCartFacade.setAccountNumberForCustomer(form.getAccountNumber());
  		  }
//        if(llaCommonUtil.isSitePuertorico())
//        {
//            CartModel cart = cartService.getSessionCart();
//            if(Objects.nonNull(cart) && BooleanUtils.isFalse(form.isCheckSSN()))
//            {
//                cart.setFalloutReasonValue(FalloutReasonEnum.CUSTOMER_WITHOUT_SSN);
//                modelService.save(cart);
//                modelService.refresh(cart);
//            }
//        }
//        String redirectToCallbackUrl = configurationService.getConfiguration().getBoolean("mulesoft.lcpr.debtcheck.api.execute") && llaCommonUtil.isSitePuertorico()?triggerDebtCheckCallForLCPR(form, model, request, redirectModel):getCheckoutStep().nextStep();
        return getCheckoutStep().nextStep();
    }

    /**
     * Trigger Debt Check Call for Customer
     * @param form
     * @param model
     * @param request
     * @param redirectModel
     * @return
     */
    private String triggerDebtCheckCallForLCPR(CustomerProfileForm form, Model model, HttpServletRequest request, RedirectAttributes redirectModel) {
        CartModel cart = cartService.getSessionCart();
        String houseKey = StringUtils.EMPTY;
        try{
            if(!form.isCheckSSN()) {
                final CustomerModel currentCustomer = (CustomerModel) cart.getUser();
                redirectModel.addFlashAttribute("currentstep", request.getRequestURL());
                storeCmsPageInModel(model, getContentPageForLabelOrId(GENERIC_CALL_BACK));
                setUpMetaDataForContentPage(model, getContentPageForLabelOrId(GENERIC_CALL_BACK));
                if (Objects.nonNull(cart.getSelectedAddress())
                        && StringUtils.isNotEmpty(cart.getSelectedAddress().getHouseNo()))
                {
                    houseKey = cart.getSelectedAddress().getHouseNo();
                }
                cart.setFalloutReasonValue(FalloutReasonEnum.CUSTOMER_WITHOUT_SSN);
                modelService.save(cart);
                modelService.refresh(cart);
                llaCommonUtil.launchFalloutProcess(cart, ClickToCallReasonEnum.DEBT, OriginSource.CHECKOUT, null, houseKey,
                        StringUtils.EMPTY);
                return REDIRECT_TO_CALLBACK_URL;
            }
            final String ssnEncryptedValue = llaEncryptionUtil.doEncryption(form.getSsn().replaceAll("-",""));
            sessionService.setAttribute(SSN_ENCRYPTED_VALUE, ssnEncryptedValue);
            LLAApiResponse llaDebtCheckApiResponse = llaMulesoftDebtCheckApiService.getDebtCheckForCustomer(cart,ssnEncryptedValue);
            
            //error page and call back page
            if(Objects.nonNull(llaDebtCheckApiResponse)) {
					if (Boolean.valueOf(llaDebtCheckApiResponse.isDebtBalanceFlag())){
						LOG.error("Customer has debt balance for Cart : " + cart.getCode() +	" User : " + cart.getUser().getUid());
						 cart.setDebtCheckFlag(Boolean.FALSE);
                       				 modelService.save(cart);
                       				 modelService.refresh(cart);
						final CustomerModel currentCustomer = (CustomerModel) cart.getUser();
						if (Objects.nonNull(cart.getSelectedAddress())
								&& StringUtils.isNotEmpty(cart.getSelectedAddress().getHouseNo()))
						{
							houseKey = cart.getSelectedAddress().getHouseNo();
						}
						cart.setFalloutReasonValue(FalloutReasonEnum.CUSTOMER_WITH_DEBT);
                      	modelService.save(cart);
						modelService.refresh(cart);
						llaCommonUtil.launchFalloutProcess(cart, ClickToCallReasonEnum.DEBT, OriginSource.CHECKOUT, null, houseKey,
								currentCustomer.getCustomerSSN());
						return REDIRECT_TO_CALLBACK_URL;

					} else if(StringUtils.isNotEmpty(llaDebtCheckApiResponse.getErrorCode()) && !llaDebtCheckApiResponse.getErrorCode().equals("404")) {
                            LOG.error("Error in Debt Check Response for Cart : " + cart.getCode() +	" User : " + cart.getUser().getUid());
                            cart.setFalloutReasonValue(FalloutReasonEnum.ERROR);
                            modelService.save(cart);
                            modelService.refresh(cart);
                            return  REDIRECT_TO_ERROR_URL;
					}
                else if(StringUtils.isNotEmpty(llaDebtCheckApiResponse.getErrorCode())  && MALFORMED_JSON_CODE.equalsIgnoreCase(llaDebtCheckApiResponse.getErrorCode()))
                {
                    cart.setFalloutReasonValue(FalloutReasonEnum.ERROR);
                    modelService.save(cart);
                    modelService.refresh(cart);
                    LOG.info(String.format("Error parsing JSON for servicability API for response %s , error is %s",llaDebtCheckApiResponse.getMessage(),llaDebtCheckApiResponse.getDescription()));
                    return REDIRECT_TO_ERROR_URL;
                }
					else {
                           LOG.info(String.format("Zero Debt Found for Customer :%s for Cart :%s",cart.getUser().getUid(),cart.getCode()));
                           LOG.info("Moving to Next Step");
                           cart.setDebtCheckFlag(Boolean.TRUE);
                           modelService.save(cart);
                           modelService.refresh(cart);
                           return getCheckoutStep().nextStep();
	            }
            }
        } catch (Exception exception) {
            LOG.error(String.format("Exception occured while making Debt Check API Call for LCPR::: %s",exception));
            if(LOG.isDebugEnabled()){
                exception.printStackTrace();
            }
            return  REDIRECT_TO_CALLBACK_URL;
        }

        return getCheckoutStep().nextStep();
    }

    protected void processNewCustomerAdditionalDetails(final CustomerModel customerModel, final CustomerProfileForm form)
    {
         llaPersonalDetailService.updatePersonalDetails(customerModel,form);
    }
    protected String getNextUrl()
    {
    	 String url = getCheckoutStep().nextStep();
    	 String s[] = url.split(":");
    	 return s[1];
    }
}
