/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcoaddon.controllers.pages.checkout.steps;

import com.lla.core.util.LLACommonUtil;
import com.lla.facades.cart.LLACartFacade;
import com.lla.facades.checkout.LLACheckoutFacade;
import com.lla.facades.product.data.ProductRegionData;
import de.hybris.platform.acceleratorfacades.payment.PaymentFacade;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.customer.CustomerLocationService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutGroup;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCheckoutController;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.AddressValidator;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.PaymentDetailsValidator;
import de.hybris.platform.acceleratorstorefrontcommons.forms.verification.AddressVerificationResultHandler;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.TitleData;

import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.apache.commons.lang.BooleanUtils;


/**
 * Checkout wizard controller.
 */
public abstract class AbstractCheckoutStepController extends AbstractCheckoutController implements CheckoutStepController
{
	protected static final String MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL = "multiStepCheckoutSummary";
	protected static final String REDIRECT_URL_ADD_DELIVERY_ADDRESS = REDIRECT_PREFIX + "/checkout/multi/delivery-address/add";
	protected static final String REDIRECT_URL_CHOOSE_DELIVERY_METHOD = REDIRECT_PREFIX + "/checkout/multi/delivery-method/choose";
	protected static final String REDIRECT_URL_ADD_PAYMENT_METHOD = REDIRECT_PREFIX + "/checkout/multi/payment-method/add";
	protected static final String REDIRECT_URL_SUMMARY = REDIRECT_PREFIX + "/checkout/multi/summary/view";
	protected static final String REDIRECT_URL_CART = REDIRECT_PREFIX + "/cart";
	protected static final String REDIRECT_URL_ERROR = REDIRECT_PREFIX + "/checkout/multi/hop-error";

	private static final String HIDEPROGRESSBAR="hideProgressBar";
	private static final String PROGRESSBARFLAG="progressBarFlag";

	protected static final String GENERIC_CALL_BACK = "/callBack";
	protected static final String CALLBACK_URL="/callBack";
	protected static final String SSN_ENCRYPTED_VALUE = "ssn_encrypted";
	protected static final String MALFORMED_JSON_CODE="422";
	private static final String INTERNET_TV = "2PInternetTv";
	private static final String INTERNET_TV_TELEPHONIA = "3Pbundles";
	private static final String INTERNET_TELEPHONIA = "2PInternetTelephone";
	private static final String INTERNET_TV_TELEPHONIA_MOBILE = "4Pbundles";
	private static final String POSTPAID = "postpaid";
	private static final String DEVICES = "devices";

	@Resource(name = "paymentDetailsValidator")
	private PaymentDetailsValidator paymentDetailsValidator;

	@Resource(name = "productFacade")
	private ProductFacade productFacade;

	@Resource(name = "multiStepCheckoutBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Resource(name = "paymentFacade")
	private PaymentFacade paymentFacade;

	@Resource(name = "addressValidator")
	private AddressValidator addressValidator;

	@Resource(name = "customerLocationService")
	private CustomerLocationService customerLocationService;

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "addressVerificationResultHandler")
	private AddressVerificationResultHandler addressVerificationResultHandler;

	@Resource(name = "contentPageBreadcrumbBuilder")
	private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

	@Resource(name = "checkoutFlowGroupMap")
	private Map<String, CheckoutGroup> checkoutFlowGroupMap;

	@Resource(name="cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "llaCommonUtil")
	private LLACommonUtil llaCommonUtil;

	@Autowired
	private LLACheckoutFacade llaCheckoutFacade;

	@Autowired
	private SessionService sessionService;

	@Autowired
	private CartService cartService;


	
	@Resource(name = "llaCartFacade")
	private LLACartFacade llaCartFacade;
	

	@ModelAttribute("titles")
	public Collection<TitleData> getTitles()
	{
		return getUserFacade().getTitles();
	}

	@ModelAttribute("countries")
	public Collection<CountryData> getCountries()
	{
		return getCheckoutFacade().getDeliveryCountries();
	}

	@ModelAttribute("countryDataMap")
	public Map<String, CountryData> getCountryDataMap()
	{
		final Map<String, CountryData> countryDataMap = new HashMap<String, CountryData>();
		for (final CountryData countryData : getCountries())
		{
			countryDataMap.put(countryData.getIsocode(), countryData);
		}
		return countryDataMap;
	}

	@ModelAttribute("areas")
	public List<ProductRegionData> getAreaList()
	{
		return  llaCommonUtil.isSiteJamaica()?llaCommonUtil.getJamaicaRegions(): Collections.emptyList();
	}
	/**
	 * Checkout steps.
	 *
	 * @return model attribute
	 */
	@ModelAttribute("checkoutSteps")
	public List<CheckoutSteps> addCheckoutStepsToModel()
	{
		final CheckoutGroup checkoutGroup = getCheckoutFlowGroupMap().get(getCheckoutFacade().getCheckoutFlowGroupForCheckout());
		final Map<String, CheckoutStep> progressBarMap = checkoutGroup.getCheckoutProgressBar();
		final List<CheckoutSteps> checkoutSteps = new ArrayList<>(progressBarMap.size());

		for (final Map.Entry<String, CheckoutStep> entry : progressBarMap.entrySet())
		{
			final CheckoutStep checkoutStep = entry.getValue();
			checkoutSteps.add(new CheckoutSteps(checkoutStep.getProgressBarId(),
					StringUtils.remove(checkoutStep.currentStep(), REDIRECT_PREFIX), Integer.valueOf(entry.getKey())));
		}
		return checkoutSteps;
	}

	/**
	 * Gets progressbarflag.
	 *
	 * @return the progressbarflag
	 */
	@ModelAttribute(HIDEPROGRESSBAR)
	public String getProgressbarflag() {
		if (null != sessionService.getAttribute(PROGRESSBARFLAG)) {
			return sessionService.getAttribute(PROGRESSBARFLAG).toString();
		}
		return StringUtils.EMPTY;
	}

	protected void prepareDataForPage(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("siteId",cmsSiteService.getCurrentSite().getUid());
		model.addAttribute("hideInstallationAddress",false);
		model.addAttribute("isOmsEnabled", Boolean.valueOf(getSiteConfigService().getBoolean("oms.enabled", false)));
		model.addAttribute("supportedCountries", getCartFacade().getDeliveryCountries());
		model.addAttribute("expressCheckoutAllowed", Boolean.valueOf(getCheckoutFacade().isExpressCheckoutAllowedForCart()));
		model.addAttribute("taxEstimationEnabled", Boolean.valueOf(getCheckoutFacade().isTaxEstimationEnabledForCart()));
		model.addAttribute("c2cTimerCOnCheckout",getConfigurationService().getConfiguration().getString("c2c.timerC.checkoutPage.step2_3"));
		model.addAttribute("c2cTimerBOnCheckout",getConfigurationService().getConfiguration().getString("c2c.timerB.checkoutPage.step1"));
		model.addAttribute("c2cTimerForLcprApi",getConfigurationService().getConfiguration().getString("lcpr.api.connection.timeout"));
		model.addAttribute("c2cTimerForCallBack",getConfigurationService().getConfiguration().getString("lcpr.api.callBack.connection.timeout"));
		if(llaCommonUtil.isSitePanama()){

			model.addAttribute("freeSim", llaCommonUtil.hideDeliveryMethod(cartService.getSessionCart()) ? false : true);
			model.addAttribute("freeShipping", llaCommonUtil.hideDeliveryMethod(cartService.getSessionCart()) ? false : true);
			model.addAttribute("freeInstalltion", llaCommonUtil.isFreeInstallationAvailable(cartService.getSessionCart()) ? true : false);
			model.addAttribute("cartCalculationMessage", llaCommonUtil.getCartCalculationMessage(getCartFacade().getSessionCart()));
			model.addAttribute("pageType", PageType.CHECKOUT.name());

			//guided steps
			super.guidedStep(model);
			if(BooleanUtils.isTrue(sessionService.getAttribute("deviceProductCheckout"))) {
			 	model.addAttribute("deviceProduct", Boolean.TRUE);	
				sessionService.setAttribute("deviceProduct", Boolean.TRUE);
				sessionService.setAttribute("fmcProduct", Boolean.FALSE);
			}
		}
		if(llaCommonUtil.isSiteJamaica() || llaCommonUtil.isSitePuertorico() || llaCommonUtil.isSiteCabletica()) {
			model.addAttribute("pageType", PageType.CHECKOUT.name());
		}
	}

	protected CheckoutStep getCheckoutStep(final String currentController)
	{
		if(!llaCommonUtil.isSiteVTR()) {
			llaCartFacade.lastVisitedStep(currentController);
		}
		final CheckoutGroup checkoutGroup = getCheckoutFlowGroupMap().get(getCheckoutFacade().getCheckoutFlowGroupForCheckout());
		return checkoutGroup.getCheckoutStepMap().get(currentController);
	}
	

	protected void setCheckoutStepLinksForModel(final Model model, final CheckoutStep checkoutStep)
	{
		model.addAttribute("previousStepUrl", StringUtils.remove(checkoutStep.previousStep(), REDIRECT_PREFIX));
		model.addAttribute("nextStepUrl", StringUtils.remove(checkoutStep.nextStep(), REDIRECT_PREFIX));
		model.addAttribute("currentStepUrl", StringUtils.remove(checkoutStep.currentStep(), REDIRECT_PREFIX));
		model.addAttribute("progressBarId", checkoutStep.getProgressBarId());

	}

	protected Map<String, String> getRequestParameterMap(final HttpServletRequest request)
	{
		final Map<String, String> map = new HashMap<String, String>();

		final Enumeration myEnum = request.getParameterNames();
		while (myEnum.hasMoreElements())
		{
			final String paramName = (String) myEnum.nextElement();
			final String paramValue = request.getParameter(paramName);
			map.put(paramName, paramValue);
		}

		return map;
	}

	/**
	 * Checkout steps DTO.
	 */
	public static class CheckoutSteps
	{
		private final String progressBarId;
		private final String url;
		private final Integer stepNumber;

		public CheckoutSteps(final String progressBarId, final String url, final Integer stepNumber)
		{
			this.progressBarId = progressBarId;
			this.url = url;
			this.stepNumber = stepNumber;
		}

		public String getProgressBarId()
		{
			return progressBarId;
		}

		public String getUrl()
		{
			return url;
		}

		public Integer getStepNumber()
		{
			return stepNumber;
		}
	}

	@Override
	protected CartFacade getCartFacade()
	{
		return cartFacade;
	}

	protected ProductFacade getProductFacade()
	{
		return productFacade;
	}

	protected PaymentDetailsValidator getPaymentDetailsValidator()
	{
		return paymentDetailsValidator;
	}

	protected ResourceBreadcrumbBuilder getResourceBreadcrumbBuilder()
	{
		return resourceBreadcrumbBuilder;
	}

	protected PaymentFacade getPaymentFacade()
	{
		return paymentFacade;
	}

	protected AddressValidator getAddressValidator()
	{
		return addressValidator;
	}

	protected CustomerLocationService getCustomerLocationService()
	{
		return customerLocationService;
	}

	protected AddressVerificationResultHandler getAddressVerificationResultHandler()
	{
		return addressVerificationResultHandler;
	}

	public ContentPageBreadcrumbBuilder getContentPageBreadcrumbBuilder()
	{
		return contentPageBreadcrumbBuilder;
	}

	public Map<String, CheckoutGroup> getCheckoutFlowGroupMap()
	{
		return checkoutFlowGroupMap;
	}

}
