/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcoaddon.controllers.pages;

import com.lla.telcoaddon.breadcrumb.TmaSearchBreadcrumbBuilder;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.data.RequestContextData;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.SearchBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCategoryPageController;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.b2ctelcofacades.product.TmaPoVariantFacade;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.FacetRefinement;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * Controller for category page
 *
 * @since 1810
 */
@Controller
@RequestMapping(value = "/**/c")
public class TmaCategoryPageController extends AbstractCategoryPageController
{
	private static final String SEARCH_PAGE_DATA = "searchPageData";
	protected static final String DYNAMIC_CATEGORY_CODE_PATH_VARIABLE_PATTERN = "/dynamic/{categoryCode:.*}";
	protected static final String DYNAMIC_CATEGORY_CODE_PATH_VARIABLE_DATA = "/dynamic/data/{categoryCode:.*}";
	private final String[] DISALLOWED_FIELDS = new String[] {};

	@Resource(name = "tmaSearchBreadcrumbBuilder")
	private TmaSearchBreadcrumbBuilder tmaSearchBreadcrumbBuilder;

	@Resource(name = "tmaPoVariantFacade")
	private TmaPoVariantFacade tmaPoVariantFacade;

	@RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String category(@PathVariable("categoryCode") final String categoryCode, // NOSONAR
			@RequestParam(value = "q", required = false) final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws UnsupportedEncodingException
	{
		final String resultsPage = performSearchAndGetResultsPage(categoryCode, searchQuery, page, showMode, sortCode, model,
				request, response);
		final Object searchPageDataObj = model.asMap().get(SEARCH_PAGE_DATA);
		if (searchPageDataObj != null)
		{
			final ProductCategorySearchPageData searchPageData = (ProductCategorySearchPageData) searchPageDataObj;
			final List groupedResults = tmaPoVariantFacade.groupByBaseProduct(searchPageData.getResults());
			searchPageData.setResults(groupedResults);
			populateModel(model, searchPageData, showMode);
		}
		return resultsPage;
	}

	@ResponseBody
	@RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN + "/facets", method = RequestMethod.GET)
	public FacetRefinement<SearchStateData> getFacets(@PathVariable("categoryCode") final String categoryCode,
			@RequestParam(value = "q", required = false) final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode) throws UnsupportedEncodingException
	{
		return performSearchAndGetFacets(categoryCode, searchQuery, page, showMode, sortCode);
	}

	@ResponseBody
	@RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN + "/results", method = RequestMethod.GET)
	public SearchResultsData<ProductData> getResults(@PathVariable("categoryCode") final String categoryCode,
			@RequestParam(value = "q", required = false) final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode) throws UnsupportedEncodingException
	{
		return performSearchAndGetResultsData(categoryCode, searchQuery, page, showMode, sortCode);
	}

	/**
	 * Provides both page html and products to the category page for limited products for the fetch more functionality
	 * @param categoryCode The category code for which the product is to be used
	 * @param searchQuery The query to search for the page
	 * @param productsToDisplay Number of products to be displayed in the initial page load (default to 12)
	 * @param page The page content - starts from 0
	 * @param showMode The mode of display
	 * @param sortCode Whether the data is to be sorted or not
	 * @param model CMS Page model
	 * @param request HTTP Request object
	 * @param response HTTP Response object
	 * @return HTML for the page + initial data for the category
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = DYNAMIC_CATEGORY_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String dynamictCategory(@PathVariable("categoryCode") final String categoryCode, // NOSONAR
						   @RequestParam(value = "q", required = false) final String searchQuery,
						   @RequestParam(value = "displaySize", required = false, defaultValue = "12") final Integer productsToDisplay,
						   @RequestParam(value = "page", defaultValue = "0") final int page,
						   @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
						   @RequestParam(value = "sort", required = false) final String sortCode, final Model model,
						   final HttpServletRequest request, final HttpServletResponse response) throws UnsupportedEncodingException
	{
		final CategoryModel category = getCommerceCategoryService().getCategoryForCode(categoryCode);
		final CategoryPageModel categoryPage = getCategoryPage(category);
		final CategorySearchEvaluator categorySearch = new CategorySearchEvaluator(categoryCode, searchQuery, page, showMode,
				sortCode, categoryPage);

		categorySearch.doSearch();

		final ProductCategorySearchPageData searchPageData = categorySearch.getSearchPageData();
		final boolean showCategoriesOnly = categorySearch.isShowCategoriesOnly();

		if(searchPageData.getResults().size() <= productsToDisplay)
		{
			searchPageData.setResults(searchPageData.getResults());
			model.addAttribute("showDynamicProducts", Boolean.FALSE);
		}
		else{
			searchPageData.setResults(searchPageData.getResults().subList(0,productsToDisplay));
			model.addAttribute("showDynamicProducts", Boolean.TRUE);
		}
		storeCmsPageInModel(model, categorySearch.getCategoryPage());
		populateModel(model, searchPageData, showMode);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, tmaSearchBreadcrumbBuilder.getBreadcrumbs(categoryCode, searchPageData));
		model.addAttribute("showCategoriesOnly", Boolean.valueOf(showCategoriesOnly));
		model.addAttribute("categoryName", category.getName());
		model.addAttribute("pageType", PageType.CATEGORY.name());
		model.addAttribute("userLocation", getCustomerLocationService().getUserLocation());

		updatePageTitle(category, model);

		final RequestContextData requestContextData = getRequestContextData(request);
		requestContextData.setCategory(category);
		requestContextData.setSearch(searchPageData);

		if (searchQuery != null)
		{
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_FOLLOW);
		}

		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(
				category.getKeywords().stream().map(keywordModel -> keywordModel.getKeyword()).collect(Collectors.toSet()));
		final String metaDescription = MetaSanitizerUtil.sanitizeDescription(category.getDescription());
		setUpMetaData(model, metaKeywords, metaDescription);

		return getViewPage(categorySearch.getCategoryPage());
	}

	/**
	 * Responsible for only returning products specified in the range (in the parameters)
	 * @param categoryCode The Category for the page
	 * @param searchQuery Search query in the category page
	 * @param page The starting position of the page from which the data is to be taken, starts from 1
	 * @param productsToDisplay	Number of products to be displayed. Defaulted to 12
	 * @param showMode Whether the data is to be displayed in Page or single format
	 * @param sortCode Whether the data is sorted or not
	 * @return List of products in JSON format.
	 * @throws UnsupportedEncodingException
	 */
	@ResponseBody
	@RequestMapping(value = DYNAMIC_CATEGORY_CODE_PATH_VARIABLE_DATA, method = RequestMethod.GET)
	public List<ProductData> getAddMoreButtonData(@PathVariable("categoryCode") final String categoryCode,
						  @RequestParam(value = "q", required = false) final String searchQuery,
						  @RequestParam(value = "page", defaultValue = "1") final int page,
						  @RequestParam(value = "displaySize", required = false, defaultValue = "12") final Integer productsToDisplay,
						  @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
						  @RequestParam(value = "sort", required = false) final String sortCode) throws UnsupportedEncodingException
	{
		final SearchQueryData searchQueryData = new SearchQueryData();
		final SearchStateData searchState = new SearchStateData();
		searchQueryData.setValue(searchQuery);
		searchState.setQuery(searchQueryData);
		final PageableData pageableData = createPageableData(page, productsToDisplay, sortCode, showMode);
		ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData = getProductSearchFacade().categorySearch(categoryCode, searchState, pageableData);
		return searchPageData.getResults();
	}

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		binder.setDisallowedFields(DISALLOWED_FIELDS);
	}
}
