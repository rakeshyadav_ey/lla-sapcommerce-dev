package com.lla.telcoaddon.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateProfileForm;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import java.util.Date;

public  class CustomerProfileForm extends UpdateProfileForm {
    private String dob;
    private String  gender;
    private String lastName;
    private String secondName;
    private String secondSurname;
    private String trnNumber;
    private String mobilePhone;
    private String mobilePhoneSecondary;
    private String fixedPhone;
    private String documentType;
    private String documentNumber;
    private String driverLicenceState;
    private String accountNumber;
    private String ssn;
    private boolean checkSSN;

    private String additionalEmail;

    public String getAdditionalEmail() {
        return additionalEmail;
    }

    public void setAdditionalEmail(String additionalEmail) {
        this.additionalEmail = additionalEmail;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getMobilePhoneSecondary() {
        return mobilePhoneSecondary;
    }

    public void setMobilePhoneSecondary(String mobilePhoneSecondary) {
        this.mobilePhoneSecondary = mobilePhoneSecondary;
    }

    public String getFixedPhone() {
        return fixedPhone;
    }

    public void setFixedPhone(String fixedPhone) {
        this.fixedPhone = fixedPhone;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }
    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }


    public String getTrnNumber() {
        return trnNumber;
    }

    public void setTrnNumber(String trnNumber) {
        this.trnNumber = trnNumber;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public String getAccountNumber() {
       return accountNumber;
   }

   public void setAccountNumber(String accountNumber) {
       this.accountNumber = accountNumber;
   }

	public String getSsn() {
		return ssn;
	}
	
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

    public boolean isCheckSSN() {
        return checkSSN;
    }

    public void setCheckSSN(boolean checkSSN) {
        this.checkSSN = checkSSN;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public String getDriverLicenceState() {
        return driverLicenceState;
    }

    public void setDriverLicenceState(String driverLicenceState) {
        this.driverLicenceState = driverLicenceState;
    }
}
