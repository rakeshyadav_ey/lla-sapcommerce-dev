package com.lla.telcoaddon.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.order.CartService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.lla.core.util.LLACommonUtil;
import com.lla.facades.cart.LLACartFacade;
import com.lla.facades.checkout.LLACheckoutFacade;
import com.lla.facades.customer.LLACustomerFacade;
import com.lla.telcoaddon.controllers.ControllerConstants;
import com.lla.telcoaddon.controllers.util.CartHelper;

import com.lla.telcoaddon.forms.AccountNumberForm;



/**
 *
 */

/**
 * @author GF986ZE
 *
 */
@Controller
@RequestMapping(value = "/checkout/multi/account-number")
public class AccountNumberCheckoutStepController extends AbstractCheckoutStepController
{
    protected static final String ACCOUNT_NUMBER = "account-number";
    protected static final String ATTR_CART_DATA = "cartData";

    @Resource(name = "checkoutFacade")
    private LLACheckoutFacade llaCheckoutFacade;

    @Autowired
    private LLACartFacade llaCartFacade;

    @Autowired
    private CartService cartService;


    @Override
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    @RequireHardLogIn
    @PreValidateCheckoutStep(checkoutStep = ACCOUNT_NUMBER)
    public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
    {
        if (cartService.getSessionCart().getIsCustomerExisting() !=null && cartService.getSessionCart().getIsCustomerExisting())
        {
            prepareDataForPage(model);
            final CartData cartData = llaCheckoutFacade.getCheckoutCart();
            cartData.setEntries(CartHelper.removeEmptyEntries(cartData.getEntries()));
            model.addAttribute(ATTR_CART_DATA, cartData);
            storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
            model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                    getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.accountnumber.breadcrumb"));
            model.addAttribute("metaRobots", "noindex,nofollow");
            setCheckoutStepLinksForModel(model, getCheckoutStep(ACCOUNT_NUMBER));
            model.addAttribute("accountNumberForm", new AccountNumberForm());
            return ControllerConstants.Views.Pages.MultiStepCheckout.AddAccountNumberPage;
        }
        return getCheckoutStep().nextStep();

    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String addAccountNumberDetails(final AccountNumberForm form, final Model model, final HttpServletRequest request,
                                          final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
    {

        if (StringUtils.isNotEmpty(form.getAccountNumber()))
        {
            llaCartFacade.setAccountNumberForCustomer(form.getAccountNumber());

        }
        return getCheckoutStep().nextStep();

    }

    protected CheckoutStep getCheckoutStep()
    {
        return getCheckoutStep(ACCOUNT_NUMBER);
    }

    @RequestMapping(value = "/back", method = RequestMethod.GET)
    @RequireHardLogIn
    @Override
    public String back(final RedirectAttributes redirectAttributes)
    {
        return getCheckoutStep().previousStep();
    }

    @RequestMapping(value = "/next", method = RequestMethod.GET)
    @RequireHardLogIn
    @Override
    public String next(final RedirectAttributes redirectAttributes)
    {
        return getCheckoutStep().nextStep();
    }


}