/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcoaddon.controllers.pages.checkout.steps;

import atg.taglib.json.util.JSONArray;
import com.lla.core.enums.ClickToCallReasonEnum;
import com.lla.core.enums.FalloutReasonEnum;
import com.lla.core.enums.OriginSource;
import com.lla.core.service.address.LLAAddressService;
import com.lla.core.service.order.LLACommerceCheckoutService;
import com.lla.core.util.LLACommonUtil;
import com.lla.facades.address.LLAAddressFacade;
import com.lla.facades.address.ProvinceData;
import com.lla.facades.cart.LLACartFacade;
import com.lla.facades.checkout.LLACheckoutFacade;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.response.LLAApiResponse;
import com.lla.mulesoft.integration.service.LLAFuzzyAddressNormalizationApiService;
import com.lla.mulesoft.integration.service.LLAMulesoftServiceabilityService;
import com.lla.telcoaddon.controllers.ControllerConstants;
import com.lla.telcoaddon.controllers.util.CartHelper;
import com.lla.telcotmfwebservices.v2.dto.NormalizedAddress;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.util.*;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import de.hybris.platform.servicelayer.session.SessionService;

/**
 * Delivery address page controller.
 */
@Controller
@RequestMapping(value = "/checkout/multi/delivery-address")
public class DeliveryAddressCheckoutStepController extends AbstractCheckoutStepController
{
	private static final Logger LOG = Logger.getLogger(DeliveryAddressCheckoutStepController.class);
	protected static final String DELIVERY_ADDRESS = "delivery-address";
	protected static final String ATTR_CART_DATA = "cartData";
	protected static final String ATTR_COUNTRY = "country";
	protected static final String ATTR_REGIONS = "regions";
	protected static final String ATTR_DELIVERY_ADDRESSES = "deliveryAddresses";
	protected static final String ATTR_NO_ADDRESS = "noAddress";
	protected static final String ATTR_ADDRESS_FORM = "addressForm";
	protected static final String ATTR_SHOW_SAVE_TO_ADDRESS_BOOK = "showSaveToAddressBook";
	protected static final String SITE_ID="siteId";
	private static final String ERROR_URL ="/error" ;
	public static String PREPAID_READONLY = "PREPAID_READONLY";
	private final String[] DISALLOWED_FIELDS = new String[] {};
	public static final String CART_COMPATIBILITY_ERROR_PREPAID = "cart.checkout.compatibility.prepaid";
	public static final String CART_COMPATIBILITY_ERROR_PRODUCT_COUNT = "cart.checkout.compatibility.prepaid.product.count";
	public static final String RURAL_RESIDENCE_ENUM = "RuralResidenceEnum";
	public static final String URBAN_RESIDENCE_ENUM = "UrbanResidenceEnum";
	public static final String WALKUP ="WALKUP";
	public static final String URBAN ="URBAN";
	public static final String RURAL ="RURAL";
	public static final String ENABLE_NORMALIZATION_API="puertorico.normalized.address.api.enabled";
	public static final String ENABLE_SERVICABILITY_API="puertorico.servicability.enable";
	public static final String BAD_REQUEST="400";
	public static final String NOT_FOUND="404";
	public static final String OK="200";

	@Resource(name="cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "checkoutFacade")
	private LLACheckoutFacade llaCheckoutFacade;
	@Autowired
	private LLACommerceCheckoutService llaCommerceCheckoutService;
	@Autowired
	private LLACommonUtil llaCommonUtil;

	@Autowired
	private LLAAddressFacade llaAddressFacade;

	@Autowired
	private LLAAddressService llaAddressService;
	@Autowired
	private UserService userService;

	@Autowired
	private CartService cartService;

	@Autowired
	private LLACartFacade llaCartFacade;
	
	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Autowired
	LLAMulesoftServiceabilityService llaMulesoftServiceabilityService;
    @Resource
    private LLAFuzzyAddressNormalizationApiService llaMulesoftAddressNormalizationApiService;

	@Autowired
	protected ConfigurationService configurationService;

	@Autowired
	private EnumerationService enumerationService;

	@Autowired
	private ModelService modelService;

    @ModelAttribute("provinceList")
	public List<ProvinceData> getProvince()
	{
		return llaCommonUtil.isSiteCabletica()?llaAddressFacade.getAllProvince(): new ArrayList<ProvinceData>();
	}
	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateCheckoutStep(checkoutStep = DELIVERY_ADDRESS)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		if(llaCommonUtil.isPrepaidCart()){
			if(!llaCommonUtil.isContainsOnlyPrepaidProducts()) {
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, CART_COMPATIBILITY_ERROR_PREPAID, null);
				return REDIRECT_PREFIX + "/cart";
			}else if(llaCommonUtil.getCartAddedProductCount() > 5){
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, CART_COMPATIBILITY_ERROR_PRODUCT_COUNT, null);
				return REDIRECT_PREFIX + "/cart";
			}
		}
		AddressForm addressForm = new AddressForm();
		if (llaCommonUtil.isSitePuertorico()) {
			final AddressData previousSelectedAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
			if (Objects.nonNull(previousSelectedAddress)) {
				addressForm = previousSelectedAddressForm(previousSelectedAddress);
			}
			CartModel cart=cartService.getSessionCart();
			llaCommonUtil.updateCreditScoreDetails(cart);
			cart.setFalloutReasonValue(null);
			modelService.save(cart);
			modelService.refresh(cart);
		}
		getCheckoutFacade().setDeliveryAddressIfAvailable();

		final CartData cartData =  llaCheckoutFacade.getCheckoutCart();
		cartData.setEntries(CartHelper.removeEmptyEntries(cartData.getEntries()));
		model.addAttribute(ATTR_COUNTRY, getSiteConfigService().getProperty("website.code"));
		model.addAttribute(ATTR_CART_DATA, cartData);
		model.addAttribute(ATTR_DELIVERY_ADDRESSES, getDeliveryAddresses(cartData.getDeliveryAddress()));
		model.addAttribute(ATTR_NO_ADDRESS, Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
		model.addAttribute("selectedPlanData",llaCartFacade.getSelectedPlanInOrder());

		if (llaCommonUtil.isSiteCabletica() && null!=sessionService.getAttribute(ATTR_ADDRESS_FORM))
		{
			addressForm = sessionService.getAttribute(ATTR_ADDRESS_FORM);
			if(sessionService.getAttribute("selectedLattitude") != null){
				model.addAttribute("setLatitutde", sessionService.getAttribute("selectedLattitude"));
			}

			if(sessionService.getAttribute("selectedLongitude") != null){
				model.addAttribute("setLongitude", sessionService.getAttribute("selectedLongitude"));
			}
		}
		if(llaCommonUtil.isPrepaidCart()){
			addressForm.setDistrict(getSiteConfigService().getProperty("address.prepaid.default.district.name"));
			addressForm.setProvince(getSiteConfigService().getProperty("address.prepaid.default.province.name"));
		}
		if(llaCommonUtil.isPrepaidCart()) {
			model.addAttribute(PREPAID_READONLY,"true");
		}else {
			model.addAttribute(PREPAID_READONLY,"false");
		}
		model.addAttribute(ATTR_ADDRESS_FORM, addressForm);
		model.addAttribute(ATTR_SHOW_SAVE_TO_ADDRESS_BOOK, Boolean.TRUE);
		model.addAttribute(SITE_ID,cmsSiteService.getCurrentSite().getUid());

		if(llaCommonUtil.isSitePuertorico()){
		    model.addAttribute("urbanResidenceTypes",llaCommonUtil.getPossibleResidenceTypes(URBAN_RESIDENCE_ENUM));
           model.addAttribute("ruralResidenceTypes",llaCommonUtil.getPossibleResidenceTypes(RURAL_RESIDENCE_ENUM));
        }

		if(StringUtils.isNotEmpty(addressForm.getBuilding())){
		  model.addAttribute("buildings", getList(addressForm.getBuilding())); 
		}
		if(StringUtils.isNotEmpty(addressForm.getDistrict())){
		  model.addAttribute("districts", getList(addressForm.getDistrict())); 
		}
		if(StringUtils.isNotEmpty(addressForm.getNeighbourhood())){
			model.addAttribute("neighbourhoods", getList(addressForm.getNeighbourhood()));
		}
		prepareDataForPage(model);
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.deliveryAddress.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
	}
	
	List<ProvinceData> getList(String str){
	   ProvinceData data = new ProvinceData();
	   data.setCode(str);
	   data.setName(str);
	   return Collections.singletonList(data);
	}

	/**
	 * Add string.
	 *
	 * @param addressForm   the address form
	 * @param bindingResult the binding result
	 * @param model         the model
	 * @param redirectModel the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException the cms item not found exception
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		addressForm.setCountryIso(getSiteConfigService().getProperty("website.code"));
		getAddressValidator().validate(addressForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			return setupBindingResults(model);
		}
		setUpAddress(addressForm,bindingResult,model);
		// Verify the address data.
		/*
		 * final AddressVerificationResult<AddressVerificationDecision> verificationResult =
		 * getAddressVerificationFacade() .verifyAddressData(newAddress); final boolean addressRequiresReview =
		 * getAddressVerificationResultHandler().handleResult(verificationResult, newAddress, model, redirectModel,
		 * bindingResult, getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(),
		 * "checkout.multi.address.updated");
		 *
		 * if (addressRequiresReview) { storeCmsPageInModel(model,
		 * getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL)); setUpMetaDataForContentPage(model,
		 * getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL)); return
		 * ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage; }
		 */

		return getCheckoutStep().nextStep();
	}

	/**
	 * Add address string.
	 *
	 * @param addressForm   the address form
	 * @param bindingResult the binding result
	 * @param model         the model
	 * @param redirectModel the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException the cms item not found exception
	 */
	@RequestMapping(value = "/addAddress", method = RequestMethod.POST)
	@RequireHardLogIn
	@ResponseBody
	public String addAddress(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		LLAApiResponse llaFuzzyApiResponse=null;
		CartModel cart = cartService.getSessionCart();
		addressForm.setCountryIso(getSiteConfigService().getProperty("website.code"));
		getAddressValidator().validate(addressForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			return setupBindingResults(model);
		}
		setUpAddress(addressForm,bindingResult,model);

		CustomerModel customerModel=(CustomerModel)cartService.getSessionCart().getUser();
		final AddressData newAddress = constructNewAddress(addressForm,customerModel);
		//call to address normalization api
		if (llaCommonUtil.isSitePuertorico() && getConfigurationService().getConfiguration().getBoolean(ENABLE_NORMALIZATION_API))
		{
			try {
				llaFuzzyApiResponse = llaMulesoftAddressNormalizationApiService.getNormalizedAddressList(formatAddress(newAddress));

				if (CollectionUtils.isNotEmpty(llaFuzzyApiResponse.getAddressList()))
				{
					LOG.info(String.format("Retrieved Normalized address for Cart ::: %s having User id ::%s ", cartService.getSessionCart().getCode(), cartService.getSessionCart().getGuid()));
					List<String> dataList = llaCommonUtil.getNormalizedAddresses(llaFuzzyApiResponse.getAddressList());
					return String.join("|", dataList);
				}
				else if(Objects.nonNull(llaFuzzyApiResponse) && StringUtils.isNotEmpty(llaFuzzyApiResponse.getErrorCode()))
				{										
					if (NOT_FOUND.equalsIgnoreCase(llaFuzzyApiResponse.getErrorCode()))
					{
						LOG.info(String.format("Getting error in normalized address api call with Error code is ::%s and description is :: %s",llaFuzzyApiResponse.getErrorCode(),llaFuzzyApiResponse.getDescription()));
						//LOG.info(String.format("Launching fallout process for cart %s and redirecting to callBack Page as %s at %s", cartService.getSessionCart().getGuid(), ClickToCallReasonEnum.UNAVAILABLE_NORMALIZED_ADDRESS, OriginSource.CHECKOUT));
						LOG.info(String.format("Skipping C2C call for cart %s and user %s as customer flow should continue while normalization api fails", cartService.getSessionCart().getCode(),cartService.getSessionCart().getUser().getUid()));
						if (Objects.nonNull(cart)) {
							cart.setFalloutReasonValue(FalloutReasonEnum.ADDRESS_NOT_FOUND);
							modelService.save(cart);
							modelService.refresh(cart);
						}
						//llaCommonUtil.launchFalloutProcess(cart, ClickToCallReasonEnum.UNAVAILABLE_NORMALIZED_ADDRESS, OriginSource.CHECKOUT, null, StringUtils.EMPTY, StringUtils.EMPTY);
						//return CALLBACK_URL;
						return getCheckoutStep().nextStep();
					}
				}
				else
				{
					LOG.error(String.format(" Exception during Normalized Address API request, error received is  :: %s", llaFuzzyApiResponse.getMessage()));
					cart.setFalloutReasonValue(FalloutReasonEnum.ERROR);
					modelService.save(cart);
					modelService.refresh(cart);
					return ERROR_URL;
				}
				return ERROR_URL;
			}
			catch (LLAApiException e)
			{
				LOG.error(String.format(" LLA API Exception during Normalized Address API request  :: %s", e.getMessage()));
				return ERROR_URL;
			}
			catch (Exception exception)
			{
				LOG.error(String.format(" Exception during Normalized Address API call  :: %s", exception.getMessage()));
				if(LOG.isDebugEnabled()){
					exception.printStackTrace();
				}
				cart.setFalloutReasonValue(FalloutReasonEnum.ERROR);
				modelService.save(cart);
				modelService.refresh(cart);
				return ERROR_URL;
			}
		}
		else{
			LOG.info("Skipping Normalized Address API Call for Puertorico");
			return getCheckoutStep().nextStep();
		}
	}

	/**
	 * setUpAddress.
	 *
	 * @param addressForm   the address form
	 * @param bindingResult the binding result
	 * @param model         the model
	 * @throws CMSItemNotFoundException the cms item not found exception
	 */
	private void setUpAddress(final AddressForm addressForm, final BindingResult bindingResult, final Model model)throws CMSItemNotFoundException
	{
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute(ATTR_CART_DATA, cartData);
		model.addAttribute(ATTR_DELIVERY_ADDRESSES, getDeliveryAddresses(cartData.getDeliveryAddress()));
		this.prepareDataForPage(model);
		storeAddresses(addressForm, model);

		CustomerModel customerModel=(CustomerModel)cartService.getSessionCart().getUser();
		final AddressData newAddress = constructNewAddress(addressForm,customerModel);
		adjustAddressVisibility(addressForm, newAddress);

		getUserFacade().addAddress(newAddress);

		final AddressData previousSelectedAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
		// Set the new address as the selected checkout delivery address
		getCheckoutFacade().setDeliveryAddress(newAddress);

		if (previousSelectedAddress != null && !previousSelectedAddress.isVisibleInAddressBook())
		{ // temporary address should be removed
			getUserFacade().removeAddress(previousSelectedAddress);
		}
	}

	/**
	 * setupBindingResults.
	 *
	 * @param model         the model
	 * @return the string
	 * @throws CMSItemNotFoundException the cms item not found exception
	 */
	private String setupBindingResults(final Model model) throws CMSItemNotFoundException
	{
		GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setCheckoutStepLinksForModel(model, getCheckoutStep());
		return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
	}

	protected void adjustAddressVisibility(final AddressForm addressForm, final AddressData newAddress)
	{
		if (addressForm.getSaveInAddressBook() != null)
		{
			newAddress.setVisibleInAddressBook(addressForm.getSaveInAddressBook().booleanValue());
			if (addressForm.getSaveInAddressBook().booleanValue() && getUserFacade().isAddressBookEmpty())
			{
				newAddress.setDefaultAddress(true);
			}
		}
		else if (getCheckoutCustomerStrategy().isAnonymousCheckout())
		{
			newAddress.setDefaultAddress(true);
			newAddress.setVisibleInAddressBook(true);
		}
	}

	/**
	 * Construct new address address data.
	 *
	 * @param addressForm   the address form
	 * @param customerModel the customer model
	 * @return the address data
	 */
	protected AddressData constructNewAddress(final AddressForm addressForm, final CustomerModel customerModel)
	{
		final AddressData newAddress = new AddressData();
		newAddress.setTitleCode(addressForm.getTitleCode());
		if(llaCommonUtil.isSitePanama() || llaCommonUtil.isSiteCabletica()) {
			newAddress.setFirstName(customerModel.getFirstName());
			newAddress.setLastName(customerModel.getLastName());
			newAddress.setPhone(customerModel.getMobilePhone());
			newAddress.setEmail(customerModel.getAdditionalEmail());
		}else{
			newAddress.setFirstName(addressForm.getFirstName());
			newAddress.setLastName(addressForm.getLastName());
			newAddress.setPhone(addressForm.getPhone());
		}
		if(StringUtils.isNotEmpty(addressForm.getLine1())){
			newAddress.setLine1(addressForm.getLine1());
		}
		if(StringUtils.isNotEmpty(addressForm.getLine2())){
			newAddress.setLine2(addressForm.getLine2());
		}
		newAddress.setTown(addressForm.getTownCity());
		newAddress.setPostalCode(addressForm.getPostcode());
		newAddress.setBillingAddress(false);
		newAddress.setShippingAddress(true);
		newAddress.setInstallationAddress(false);
		newAddress.setStreetname(addressForm.getStreetname());
		newAddress.setAppartment(addressForm.getAppartment());
		newAddress.setBuilding(addressForm.getBuilding());
		if(llaCommonUtil.isSitePuertorico()){
			newAddress.setStreetNo(addressForm.getStreetNumber());
			newAddress.setBlock(addressForm.getBlock());
			newAddress.setTypeOfResidence(addressForm.getTypeOfResidence());
			newAddress.setArea(addressForm.getArea());
			newAddress.setPlotNo(addressForm.getPlotNo());
			newAddress.setKilometer(addressForm.getKilometer());
		}
		if(llaCommonUtil.isSiteCabletica()){
			newAddress.setNeighbourhood(StringUtils.isNotBlank(addressForm.getNeighbourhood())?llaAddressService.getNeighbourHoodByUID(addressForm.getNeighbourhood()):StringUtils.EMPTY);
			newAddress.setDistrict(StringUtils.isNotBlank(addressForm.getDistrict())?llaAddressService.getDistrictByUID(addressForm.getDistrict()):StringUtils.EMPTY);
			newAddress.setProvince(StringUtils.isNotBlank(addressForm.getProvince())?llaAddressService.getProvinceByUID(addressForm.getProvince()):StringUtils.EMPTY);
			newAddress.setBuilding(StringUtils.isNotBlank(addressForm.getBuilding())?llaAddressService.getCantonByUID(addressForm.getBuilding()):StringUtils.EMPTY);
			newAddress.setLandMark(addressForm.getLandMark());
		}else {
			newAddress.setNeighbourhood(addressForm.getNeighbourhood());
			newAddress.setDistrict(addressForm.getDistrict());
			newAddress.setProvince(addressForm.getProvince());
			newAddress.setServiceable(addressForm.getIsServiceable());
		}
		if (addressForm.getCountryIso() != null)
		{
			final CountryData countryData = getI18NFacade().getCountryForIsocode(addressForm.getCountryIso());
			newAddress.setCountry(countryData);
		}
		if (addressForm.getRegionIso() != null && !StringUtils.isEmpty(addressForm.getRegionIso()))
		{
			final RegionData regionData = getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso());
			newAddress.setRegion(regionData);
		}

		return newAddress;
	}

	protected void storeAddresses(final AddressForm addressForm, final Model model)
	{
		if (StringUtils.isNotBlank(addressForm.getCountryIso()))
		{
			model.addAttribute(ATTR_REGIONS, getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
			model.addAttribute(ATTR_COUNTRY, addressForm.getCountryIso());
		}

		model.addAttribute(ATTR_NO_ADDRESS, Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
		model.addAttribute(ATTR_SHOW_SAVE_TO_ADDRESS_BOOK, Boolean.TRUE);
	}

	/**
	 * Edit resource.
	 *
	 * @param editAddressCode
	 * @param model
	 * @param redirectAttributes
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	@RequireHardLogIn
	public String editAddressForm(@RequestParam("editAddressCode") final String editAddressCode, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
		if (getCheckoutStep().checkIfValidationErrors(validationResults))
		{
			return getCheckoutStep().onValidation(validationResults);
		}

		AddressData addressData = null;
		if (StringUtils.isNotEmpty(editAddressCode))
		{
			addressData = getCheckoutFacade().getDeliveryAddressForCode(editAddressCode);
		}

		final AddressForm addressForm = new AddressForm();
		final boolean hasAddressData = addressData != null;
		if (hasAddressData)
		{
			addressForm.setAddressId(addressData.getId());
			addressForm.setTitleCode(addressData.getTitleCode());
			addressForm.setFirstName(addressData.getFirstName());
			addressForm.setLastName(addressData.getLastName());
			addressForm.setLine1(addressData.getLine1());
			addressForm.setLine2(addressData.getLine2());
			addressForm.setTownCity(addressData.getTown());
			addressForm.setPostcode(addressData.getPostalCode());
			addressForm.setCountryIso(addressData.getCountry().getIsocode());
			addressForm.setSaveInAddressBook(Boolean.valueOf(addressData.isVisibleInAddressBook()));
			addressForm.setShippingAddress(Boolean.valueOf(addressData.isShippingAddress()));
			addressForm.setBillingAddress(Boolean.valueOf(addressData.isBillingAddress()));
			addressForm.setAppartment(addressData.getAppartment());
			addressForm.setBuilding(addressData.getBuilding());
			addressForm.setStreetname(addressData.getStreetname());
			addressForm.setNeighbourhood(addressData.getNeighbourhood());
			addressForm.setDistrict(addressData.getDistrict());
			addressForm.setIsServiceable(addressData.getServiceable());
			addressForm.setProvince(addressData.getProvince());
			addressForm.setPhone(addressData.getPhone());
			if (addressData.getRegion() != null && !StringUtils.isEmpty(addressData.getRegion().getIsocode()))
			{
				addressForm.setRegionIso(addressData.getRegion().getIsocode());
			}
		}

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute(ATTR_CART_DATA, cartData);
		model.addAttribute(ATTR_DELIVERY_ADDRESSES, getDeliveryAddresses(cartData.getDeliveryAddress()));
		if (StringUtils.isNotBlank(addressForm.getCountryIso()))
		{
			model.addAttribute(ATTR_REGIONS, getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
			model.addAttribute(ATTR_COUNTRY, addressForm.getCountryIso());
		}
		model.addAttribute(ATTR_NO_ADDRESS, Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
		model.addAttribute("edit", Boolean.valueOf(hasAddressData));
		model.addAttribute(ATTR_ADDRESS_FORM, addressForm);
		if (addressData != null)
		{
			model.addAttribute(ATTR_SHOW_SAVE_TO_ADDRESS_BOOK, Boolean.valueOf(!addressData.isVisibleInAddressBook()));
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.deliveryAddress.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
	}

	/**
	 * Save resource.
	 *
	 * @param addressForm
	 * @param bindingResult
	 * @param model
	 * @param redirectModel
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@RequireHardLogIn
	public String edit(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		getAddressValidator().validate(addressForm, bindingResult);
		if (StringUtils.isNotBlank(addressForm.getCountryIso()))
		{
			model.addAttribute(ATTR_REGIONS, getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
			model.addAttribute(ATTR_COUNTRY, addressForm.getCountryIso());
		}
		model.addAttribute(ATTR_NO_ADDRESS, Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
			storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}

		final AddressData newAddress = constructNewAddress(addressForm,(CustomerModel) cartService.getSessionCart().getUser());
		newAddress.setId(addressForm.getAddressId());
		newAddress.setVisibleInAddressBook(
				addressForm.getSaveInAddressBook() == null ? true : addressForm.getSaveInAddressBook().booleanValue());
		newAddress.setDefaultAddress(getUserFacade().isAddressBookEmpty() || getUserFacade().getAddressBook().size() == 1
				|| Boolean.TRUE.equals(addressForm.getDefaultAddress()));

		// Verify the address data.
		final AddressVerificationResult<AddressVerificationDecision> verificationResult = getAddressVerificationFacade()
				.verifyAddressData(newAddress);
		final boolean addressRequiresReview = getAddressVerificationResultHandler().handleResult(verificationResult, newAddress,
				model, redirectModel, bindingResult, getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(),
				"checkout.multi.address.updated");

		if (addressRequiresReview)
		{
			setAttributesForAddressReview(addressForm, model);
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}

		getUserFacade().editAddress(newAddress);
		getCheckoutFacade().setDeliveryModeIfAvailable();
		getCheckoutFacade().setDeliveryAddress(newAddress);

		return getCheckoutStep().nextStep();
	}

	protected void setAttributesForAddressReview(final AddressForm addressForm, final Model model) throws CMSItemNotFoundException
	{
		if (StringUtils.isNotBlank(addressForm.getCountryIso()))
		{
			model.addAttribute(ATTR_REGIONS, getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
			model.addAttribute(ATTR_COUNTRY, addressForm.getCountryIso());
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));

		if (StringUtils.isNotEmpty(addressForm.getAddressId()))
		{
			final AddressData addressData = getCheckoutFacade().getDeliveryAddressForCode(addressForm.getAddressId());
			if (addressData != null)
			{
				model.addAttribute(ATTR_SHOW_SAVE_TO_ADDRESS_BOOK, Boolean.valueOf(!addressData.isVisibleInAddressBook()));
				model.addAttribute("edit", Boolean.TRUE);
			}
		}
	}

	/**
	 * Delete resource.
	 *
	 * @param addressCode
	 * @param redirectModel
	 * @param model
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/remove", method =
	{ RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public String removeAddress(@RequestParam("addressCode") final String addressCode, final RedirectAttributes redirectModel,
			final Model model) throws CMSItemNotFoundException
	{
		final AddressData addressData = new AddressData();
		addressData.setId(addressCode);
		getUserFacade().removeAddress(addressData);
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.removed");
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute(ATTR_ADDRESS_FORM, new AddressForm());

		return getCheckoutStep().currentStep();
	}

	/**
	 * Select address from list.
	 *
	 * @param addressForm
	 * @param redirectModel
	 * @return
	 */

	@RequestMapping(value = "/select", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doSelectSuggestedAddress(final AddressForm addressForm, final RedirectAttributes redirectModel)
	{
		final Set<String> resolveCountryRegions = org.springframework.util.StringUtils
				.commaDelimitedListToSet(Config.getParameter("resolve.country.regions"));

		final AddressData selectedAddress = new AddressData();
		selectedAddress.setId(addressForm.getAddressId());
		selectedAddress.setTitleCode(addressForm.getTitleCode());
		selectedAddress.setFirstName(addressForm.getFirstName());
		selectedAddress.setLastName(addressForm.getLastName());
		selectedAddress.setLine1(addressForm.getLine1());
		selectedAddress.setLine2(addressForm.getLine2());
		selectedAddress.setTown(addressForm.getTownCity());
		selectedAddress.setPostalCode(addressForm.getPostcode());
		selectedAddress.setBillingAddress(false);
		selectedAddress.setShippingAddress(true);
		selectedAddress.setAppartment(addressForm.getAppartment());
		selectedAddress.setBuilding(addressForm.getBuilding());
		selectedAddress.setStreetname(addressForm.getStreetname());
		if(!llaCommonUtil.isSiteCabletica()){
			selectedAddress.setNeighbourhood(addressForm.getNeighbourhood());
			}
		selectedAddress.setDistrict(addressForm.getDistrict());
		selectedAddress.setServiceable(addressForm.getIsServiceable());
		selectedAddress.setProvince(addressForm.getProvince());
		final CountryData countryData = getI18NFacade().getCountryForIsocode(addressForm.getCountryIso());
		selectedAddress.setCountry(countryData);

		if (resolveCountryRegions.contains(countryData.getIsocode()) && addressForm.getRegionIso() != null
				&& !StringUtils.isEmpty(addressForm.getRegionIso()))
		{
			final RegionData regionData = getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso());
			selectedAddress.setRegion(regionData);
		}

		if (addressForm.getSaveInAddressBook() != null)
		{
			selectedAddress.setVisibleInAddressBook(addressForm.getSaveInAddressBook().booleanValue());
		}

		if (Boolean.TRUE.equals(addressForm.getEditAddress()))
		{
			getUserFacade().editAddress(selectedAddress);
		}
		else
		{
			getUserFacade().addAddress(selectedAddress);
		}

		final AddressData previousSelectedAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
		// Set the new address as the selected checkout delivery address
		getCheckoutFacade().setDeliveryAddress(selectedAddress);
		if (previousSelectedAddress != null && !previousSelectedAddress.isVisibleInAddressBook())
		{ // temporary address should be removed
			getUserFacade().removeAddress(previousSelectedAddress);
		}

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "checkout.multi.address.added");

		return getCheckoutStep().nextStep();
	}


	/**
	 * This method gets called when the "Use this Address" button is clicked. It sets the selected delivery address on
	 * the checkout facade - if it has changed, and reloads the page highlighting the selected delivery address.
	 *
	 * @param selectedAddressCode
	 *           - the id of the delivery address.
	 *
	 * @return - a URL to the page to load.
	 */
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	@RequireHardLogIn
	public String doSelectDeliveryAddress(@RequestParam("selectedAddressCode") final String selectedAddressCode, final Model model,
			final RedirectAttributes redirectAttributes)
	{
		ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
		if(llaCommonUtil.isSitePanama() && llaCommonUtil.isPrepaidCart()) {
			final AddressData selectedAddressData = getCheckoutFacade().getDeliveryAddressForCode(selectedAddressCode);
			if(null != selectedAddressData.getDistrict() && !selectedAddressData.getDistrict().equalsIgnoreCase(getSiteConfigService().getProperty("address.prepaid.default.district.name"))){
				validationResults =ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
			}
		}
		if (getCheckoutStep().checkIfValidationErrors(validationResults))
		{
			return getCheckoutStep().onValidation(validationResults);
		}
		if (StringUtils.isNotBlank(selectedAddressCode))
		{
			cleanupTempAddress(selectedAddressCode);
		}
		return getCheckoutStep().nextStep();
	}

	protected void cleanupTempAddress(final String selectedAddressCode)
	{
		final AddressData selectedAddressData = getCheckoutFacade().getDeliveryAddressForCode(selectedAddressCode);
		if (selectedAddressData != null)
		{
			final AddressData cartCheckoutDeliveryAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
			if (isAddressIdChanged(cartCheckoutDeliveryAddress, selectedAddressData))
			{
				getCheckoutFacade().setDeliveryAddress(selectedAddressData);
				if (cartCheckoutDeliveryAddress != null && !cartCheckoutDeliveryAddress.isVisibleInAddressBook())
				{ // temporary address should be removed
					getUserFacade().removeAddress(cartCheckoutDeliveryAddress);
				}
			}
		}
	}

	@GetMapping(value="/getAddressFields")
	@ResponseBody
	public JSONArray  getAddressFields(@RequestParam(value="prevNode",required = true) final String prevNode, @RequestParam(value="nextNode",required = true) final String nextNode, final Model model)
	{

		List returnList=llaAddressFacade.getAddressValues(prevNode,nextNode);
		JSONArray array = new JSONArray(returnList);
		return array;
	}

	/**
	 * format Address.
	 * @param newAddress
	 * @return string
	 */
	private String formatAddress(AddressData newAddress){
		if (Objects.nonNull(newAddress) && StringUtils.isNotEmpty(newAddress.getTypeOfResidence()))
		{
			String typeOfResidence = newAddress.getTypeOfResidence().toUpperCase();
			LinkedList<String> addressList = new LinkedList<>();

			switch (typeOfResidence) {
				case WALKUP:
					addressList.add(newAddress.getStreetname());
					addressList.add(newAddress.getStreetNo());
					addressList.add(newAddress.getLine1());
					addressList.add(newAddress.getAppartment());
					break;

				case URBAN:
					addressList.add(newAddress.getStreetname());
					addressList.add(newAddress.getBlock());
					addressList.add(newAddress.getBuilding());
					addressList.add(newAddress.getLine1());
					addressList.add(newAddress.getAppartment());
					break;

				case RURAL:
					addressList.add(newAddress.getArea());
					addressList.add(newAddress.getPlotNo());
					if(StringUtils.isNotEmpty(newAddress.getLine2())){
						addressList.add(newAddress.getLine2());
					}
					if(StringUtils.isNotEmpty(newAddress.getLine1())){
						addressList.add(newAddress.getLine1());
					}
					addressList.add(newAddress.getKilometer());
					break;

				default:
					break;
			}

			addressList.add(newAddress.getTown());
			addressList.add(newAddress.getPostalCode());
			String[] listSize = new String[addressList.size()];

			return String.join(" ", addressList.toArray(listSize));
		}

		return Strings.EMPTY;
	}

	/**
	 * Add address data.
	 *
	 * @param address       the address
	 * @param model         the model
	 * @param redirectModel the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException the cms item not found exception
	 */
	@RequestMapping(value = "/check-serviceability", method = RequestMethod.POST)
	@RequireHardLogIn
	@ResponseBody
	public String add(@RequestBody final AddressData address, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		boolean isServicable = false;
		CartModel cart = cartService.getSessionCart();
		if(configurationService.getConfiguration().getBoolean(ENABLE_SERVICABILITY_API))
           {
			   if (Objects.nonNull(address) && StringUtils.isNotEmpty(address.getHouseNo())) {
				   try {
					   LOG.info(String.format("Checking servicability for house key %s for cart %s", address.getHouseNo(), cartService.getSessionCart().getGuid()));
					   llaAddressFacade.setSelectedAddress(address);
					   LLAApiResponse response = llaMulesoftServiceabilityService.checkServiceability(address.getHouseNo());

					   if (Objects.nonNull(response) && (StringUtils.isNotEmpty(response.getStatusCode()) && response.getStatusCode().equalsIgnoreCase(OK)))
					   {
						LOG.info(String.format("The status code: %s and  house id : %s for cart :%s",response.getStatusCode(),address.getHouseNo(),cartService.getSessionCart().getCode()));
						   if (Objects.nonNull(cart) && Boolean.TRUE.equals(cart.getIsRequiredServiceActive())) {
							   LOG.info(String.format("The house key %s for cart %s in not servicable as service is already active.", address.getHouseNo(), cartService.getSessionCart().getGuid()));
							   //LOG.info(String.format("Launching fallout process for cart %s and redirecting to callBack Page as %s at %s", cartService.getSessionCart().getGuid(), ClickToCallReasonEnum.NOT_SERVICEABLE, OriginSource.CHECKOUT));
							   LOG.info(String.format("Skipping C2C call for cart %s and user %s as customer flow should continue while serviceability api fails", cartService.getSessionCart().getGuid(),cartService.getSessionCart().getUser().getUid()));
							   cart.setFalloutReasonValue(FalloutReasonEnum.SERVICE_INSTALLED);
							   modelService.save(cart);
							   modelService.refresh(cart);
							   //llaCommonUtil.launchFalloutProcess(cart, ClickToCallReasonEnum.NOT_SERVICEABLE, OriginSource.CHECKOUT, null, address.getHouseNo(), StringUtils.EMPTY);
							   //return CALLBACK_URL;
								return getCheckoutStep().nextStep();
						   }
						   else {
							   LOG.info(String.format("The house key %s for cart %s servicable", address.getHouseNo(), cartService.getSessionCart().getGuid()));
							   return getCheckoutStep().nextStep();
						   }						   
					   }
					   else if(Objects.nonNull(response) && StringUtils.isNotEmpty(response.getErrorCode())) {
						   if (BAD_REQUEST.equalsIgnoreCase(response.getErrorCode()))
						   {
							   LOG.info(String.format("The house key %s for cart %s in not servicable", address.getHouseNo(), cartService.getSessionCart().getGuid()));
							   //LOG.info(String.format("Launching fallout process for cart %s and redirecting to callBack Page as %s at %s", cartService.getSessionCart().getGuid(), ClickToCallReasonEnum.NOT_SERVICEABLE, OriginSource.CHECKOUT));
							   LOG.info(String.format("Skipping C2C call for cart %s and user %s as customer flow should continue while serviceability api fails", cartService.getSessionCart().getGuid(),cartService.getSessionCart().getUser().getUid()));
							   if (Objects.nonNull(cart)) {
								   cart.setFalloutReasonValue(FalloutReasonEnum.NO_COVERAGE);
								   modelService.save(cart);
								   modelService.refresh(cart);
							   }
							   //llaCommonUtil.launchFalloutProcess(cart, ClickToCallReasonEnum.NOT_SERVICEABLE, OriginSource.CHECKOUT, null, address.getHouseNo(), StringUtils.EMPTY);
							   //return CALLBACK_URL;
							   return getCheckoutStep().nextStep();
						   }
						  else if(MALFORMED_JSON_CODE.equalsIgnoreCase(response.getErrorCode()))
						   {
							   cart.setFalloutReasonValue(FalloutReasonEnum.ERROR);
							   modelService.save(cart);
							   modelService.refresh(cart);
							   LOG.info(String.format("Error parsing JSON for servicability API for response %s , error is %s",response.getMessage(),response.getDescription()));
							   return ERROR_URL;
						   }
						   else
						   {
							   LOG.error(String.format(" Exception during Serviciablity api request, error received is  :: %s", response.getMessage()));
							   cart.setFalloutReasonValue(FalloutReasonEnum.ERROR);
							   modelService.save(cart);
							   modelService.refresh(cart);
							   return ERROR_URL;
						   }
					   }
					   else
						   {
							   LOG.error(String.format(" Exception during Serviciablity api request, error received is  :: %s", response.getMessage()));
							   cart.setFalloutReasonValue(FalloutReasonEnum.ERROR);
							   modelService.save(cart);
							   modelService.refresh(cart);
							   return ERROR_URL;
						   }
				   }
				   catch (LLAApiException e)
				   {
					   LOG.error(String.format(" Exception during Serviciablity api request  :: %s", e.getMessage()));
					   return ERROR_URL;
				   }
				   catch (Exception e)
				   {
					   LOG.error(String.format(" Exception during Serviciablity api call  :: %s", e.getMessage()));
					   cart.setFalloutReasonValue(FalloutReasonEnum.ERROR);
					   modelService.save(cart);
					   modelService.refresh(cart);
					   return ERROR_URL;
				   }
			   }
			   else {
				   cart.setFalloutReasonValue(FalloutReasonEnum.ERROR);
				   modelService.save(cart);
				   modelService.refresh(cart);
				   return ERROR_URL;
			   }
		   }
           else
		   {
		   	  LOG.info("Skipping Serviceability API Call for puertorico");
			   llaAddressFacade.setSelectedAddress(address);
			   return getCheckoutStep().nextStep();
		   }

	}

	@RequestMapping(value = "/no-address-found", method = RequestMethod.POST)
	@RequireHardLogIn
	@ResponseBody
	public String noAddressFound(final Model model,
					  final RedirectAttributes redirectModel)throws CMSItemNotFoundException
	{
		CartModel cart = cartService.getSessionCart();
		if(Objects.nonNull(cart))
		{
			cart.setFalloutReasonValue(FalloutReasonEnum.CANNOT_FIND_ADDRESS);
			modelService.save(cart);
			modelService.refresh(cart);
			if (llaCommonUtil.isSitePuertorico()) {
				return getCheckoutStep().nextStep();
			}

			LOG.info(String.format("Launching fallout process for cart %s and redirecting to callBack Page as %s at %s as customer cannot find any address.", cartService.getSessionCart().getGuid(), ClickToCallReasonEnum.NOT_SERVICEABLE, OriginSource.CHECKOUT));
			llaCommonUtil.launchFalloutProcess(cart, ClickToCallReasonEnum.NOT_SERVICEABLE, OriginSource.CHECKOUT, null, Strings.EMPTY, StringUtils.EMPTY);
		}
		return CALLBACK_URL;
	}

	/**
	 * previousSelectedAddressForm
	 * @param previousSelectedAddress
	 * @return addressForm
	 */
	private AddressForm previousSelectedAddressForm(AddressData previousSelectedAddress){

		String typeOfResidence = previousSelectedAddress.getTypeOfResidence().toUpperCase();
		AddressForm addressForm=new AddressForm();

		switch (typeOfResidence) {
			case WALKUP:
				addressForm.setStreetname(previousSelectedAddress.getStreetname());
				addressForm.setStreetNumber(previousSelectedAddress.getStreetNo());
				addressForm.setLine1(previousSelectedAddress.getLine1());
				addressForm.setAppartment(previousSelectedAddress.getAppartment());
				break;

			case URBAN:
				addressForm.setStreetname(previousSelectedAddress.getStreetname());
				addressForm.setBlock(previousSelectedAddress.getBlock());
				addressForm.setBuilding(previousSelectedAddress.getBuilding());
				addressForm.setLine1(previousSelectedAddress.getLine1());
				addressForm.setLine2(previousSelectedAddress.getLine2());
				addressForm.setAppartment(previousSelectedAddress.getAppartment());
				addressForm.setTypeOfResidence(previousSelectedAddress.getTypeOfResidence().toUpperCase());
				break;

			case RURAL:
				addressForm.setArea(previousSelectedAddress.getArea());
				addressForm.setPlotNo(previousSelectedAddress.getPlotNo());
				addressForm.setLine2(previousSelectedAddress.getLine2());
				addressForm.setLine1(previousSelectedAddress.getLine1());
				addressForm.setKilometer(previousSelectedAddress.getKilometer());
				addressForm.setTypeOfResidence(previousSelectedAddress.getTypeOfResidence().toUpperCase());
				break;

			default:
				break;
		}

		addressForm.setTownCity(previousSelectedAddress.getTown());
		addressForm.setPostcode(previousSelectedAddress.getPostalCode());

		return addressForm;
	}

	/*@RequestMapping(value = "/clickToCallRequest", method = {RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public ResponseEntity getCallBack(final Model model, final HttpStatus httpStatus, final HttpServletRequest request,
			final HttpServletResponse response)
	{
		boolean isCallRequestPlaced = false;
		final CartModel cartModel = cartService.getSessionCart();
		if (null != cartModel)
		{
			try {
				isCallRequestPlaced = llaCommerceCheckoutService.executeCustomerCallBackRequest(((AbstractOrderModel) cartModel));
			} catch (LLAApiException exception) {
				exception.printStackTrace();
			}
		}
		if(!isCallRequestPlaced) {
			return new ResponseEntity(httpStatus.BAD_REQUEST);
		}
		return new ResponseEntity(httpStatus.OK);
	}*/

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		binder.setDisallowedFields(DISALLOWED_FIELDS);
	}


	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(DELIVERY_ADDRESS);
	}

}
