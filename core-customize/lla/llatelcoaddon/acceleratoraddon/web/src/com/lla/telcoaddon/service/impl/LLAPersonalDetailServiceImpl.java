package com.lla.telcoaddon.service.impl;

import com.lla.core.enums.ContractType;
import com.lla.core.enums.OriginSource;
import com.lla.core.util.LLACommonUtil;
import com.lla.core.util.LLAEncryptionUtil;
import com.lla.mulesoft.integration.service.LLAMulesoftDebtCheckApiService;
import com.lla.telcoaddon.forms.ContractTypeForm;
import com.lla.telcoaddon.forms.CustomerProfileForm;
import com.lla.telcoaddon.service.LLAPersonalDetailService;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.lla.mulesoft.integration.response.LLAApiResponse;
import com.lla.mulesoft.integration.exception.LLAApiException;

import org.springframework.validation.Errors;
import com.lla.core.enums.DocumentType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.lla.core.enums.ClickToCallReasonEnum;
import de.hybris.platform.servicelayer.user.UserService;
import com.lla.core.service.creditscore.LLACreditScoreService;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import de.hybris.platform.order.exceptions.CalculationException;

public class LLAPersonalDetailServiceImpl implements LLAPersonalDetailService {
    private static final Logger LOG = LoggerFactory.getLogger(LLAPersonalDetailServiceImpl.class);
    protected static final String SSN = "ssnValue";
    
    @Autowired
    private FlexibleSearchService flexibleSearchService;
    @Autowired
    private LLACommonUtil llaCommonUtil;
    @Autowired
    private CartService cartService;

    @Autowired
    private ModelService modelService;
    @Autowired
	private LLAMulesoftDebtCheckApiService llaMulesoftDebtCheckApiService;
    
    @Autowired
    private LLAEncryptionUtil llaEncryptionUtil;
    
    @Autowired
 	 private UserService userService;
    @Autowired
 	 private LLACreditScoreService llaCreditScoreService;
    @Autowired
    private LLAMulesoftNotificationService llamulesoftNotificationService;
    @Autowired
 	 private SessionService sessionService;

    /**
     * Update Contact Details of the Customer
     * @param customerModel
     * @param email
     * @param  mobilePhone
     * @param documentType
     * @param documentNumber
     */
    @Override
    public void updateContactDetails(CustomerModel customerModel, String email, String mobilePhone, String documentType, String documentNumber) {
       customerModel.setAdditionalEmail(email.trim());
       customerModel.setMobilePhone(mobilePhone.trim());
       if(!llaCommonUtil.isSiteJamaica()) {
           customerModel.setFixedPhone(mobilePhone.trim());
       }
        if(llaCommonUtil.isSitePuertorico() && StringUtils.isNotEmpty(customerModel.getName())) {
            String name = customerModel.getName();
            String splitData[] = name.split("\\s", 2);
            customerModel.setFirstName(splitData[0].replaceAll("\\s+", " ").trim());
            if (splitData.length > 1){
                customerModel.setLastName(StringUtils.isNotEmpty(splitData[1]) ? splitData[1].replaceAll("\\s+", " ").trim() : StringUtils.EMPTY);
            }else{
                customerModel.setLastName(StringUtils.EMPTY);
            }
        }
       if(documentType != null && documentNumber != null){
      	 customerModel.setDocumentNumber(documentNumber.trim());
         customerModel.setDocumentType(documentType.trim());
       }
       modelService.save(customerModel);
   }

    /**
     * Update Credit Check Details
     * @return
     */
    @Override
	 public LLAApiResponse updateCreditCheckDetails() throws LLAApiException {
        LLAApiResponse llaCreditCheckApiResponse=null;
		 try
		 {
			 CustomerModel customerModel = (CustomerModel) userService.getCurrentUser();
			 llaCreditCheckApiResponse = llaCreditScoreService.checkCreditScore();
			 if (null != llaCreditCheckApiResponse && llaCreditCheckApiResponse.getCreditScore() >0)
			 {
				 LOG.info("LLAApiResponse for user : " + customerModel.getUid() + " credit score : " + llaCreditCheckApiResponse.getCreditScore());
			 }else if(null!=llaCreditCheckApiResponse || StringUtils.isNotEmpty(llaCreditCheckApiResponse.getErrorCode()) || StringUtils.isNotEmpty(llaCreditCheckApiResponse.getMessage()) ||StringUtils.isNotEmpty(llaCreditCheckApiResponse.getDescription())){

                 LOG.error(String.format("Empty or error response retrieved for Credit Check API Call for customer ::%s Cart :::%s for Market ::%s", customerModel.getUid(),cartService.getSessionCart().getCode(),cartService.getSessionCart().getSite().getUid())) ;
             }
		 } catch (LLAApiException exception) {
             LOG.error(String.format("Exception in making Credit Check API Call due to :%s", exception.getMessage()));
             throw new LLAApiException("Exception in making Credit Check API Call");
         }
         return llaCreditCheckApiResponse;
     }

    /**
     * Update Personal Details of Customer
     * @param customerModel
     * @param form
     */

    @Override
    public void updatePersonalDetails(CustomerModel customerModel, CustomerProfileForm form) {
   	  if(!StringUtils.isEmpty(form.getFirstName()) && !StringUtils.isEmpty(form.getLastName())) {
   		 customerModel.setLastName(form.getLastName().replaceAll("\\s+"," ").trim());
          customerModel.setFirstName(form.getFirstName().replaceAll("\\s+"," ").trim());
   	  }
 	  else{
          if(llaCommonUtil.isSitePuertorico() && StringUtils.isNotEmpty(customerModel.getName())) {
              String name = customerModel.getName();
              String splitData[] = name.split("\\s", 2);
              customerModel.setFirstName(splitData[0].replaceAll("\\s+", " ").trim());
              if (splitData.length > 1){
                  customerModel.setLastName(StringUtils.isNotEmpty(splitData[1]) ? splitData[1].replaceAll("\\s+", " ").trim() : StringUtils.EMPTY);
              }else{
                  customerModel.setLastName(StringUtils.EMPTY);
              }
          }
      }
        
   	    boolean isSitePuertorico = llaCommonUtil.isSitePuertorico();
        if(llaCommonUtil.isSitePuertorico()){
            SimpleDateFormat dateFormat = isSitePuertorico? new SimpleDateFormat("dd-MM-yyyy"): new SimpleDateFormat("yyyy-MM-dd");
            String dateString= StringUtils.EMPTY;
            try {
                Date dob=dateFormat.parse(form.getDob());
                customerModel.setDateOfBirth(dob);
            } catch (ParseException e) {
                LOG.error("Date Parsing Error");
            }
//            if(StringUtils.isNotEmpty(form.getSsn()))
//            {
//                final String ssnValue = llaEncryptionUtil.doMasking(form.getSsn().replaceAll("-",""));
//                customerModel.setCustomerSSN(ssnValue);
//            }
        }
	    if(llaCommonUtil.isSiteJamaica()) {
            customerModel.setTrnNumber(form.getTrnNumber().trim());
	        customerModel.setAdditionalEmail(customerModel.getType().equals(CustomerType.GUEST)? org.apache.commons.lang.StringUtils.substringAfter(customerModel.getUid(),"|"):customerModel.getUid());
            customerModel.setFirstName(form.getFirstName().trim());
            customerModel.setFixedPhone(StringUtils.isNotEmpty(form.getFixedPhone()) ? form.getFixedPhone().trim(): Strings.EMPTY);
        }
        if(llaCommonUtil.isSitePanama() || llaCommonUtil.isSitePuertorico()){
            customerModel.setDocumentType(form.getDocumentType());
            customerModel.setDocumentNumber(form.getDocumentNumber());
            persistLCPRPersonalDetails(customerModel, form);
            if(llaCommonUtil.isSitePanama()){
                if(form.getMobilePhoneSecondary() != null) {
                    customerModel.setMobilePhoneSecondary(form.getMobilePhoneSecondary().trim());
                }
            }
            customerModel.setFirstName(form.getFirstName().replaceAll("\\s+"," ").trim());
            updateDeliveryAddress(form,customerModel);
        }
        String name[]={customerModel.getFirstName(),customerModel.getLastName()};
        customerModel.setName(org.apache.commons.lang.StringUtils.join(name," "));
        modelService.save(customerModel);
        modelService.refresh(customerModel);
    }

    /**
     * Persist Personal Details for Puertorico
     * @param customerModel
     * @param form
     */
    private void persistLCPRPersonalDetails(CustomerModel customerModel, CustomerProfileForm form) {
        if(llaCommonUtil.isSitePuertorico()){
            if(!StringUtils.isEmpty(form.getSecondName())) {
                customerModel.setSecondName(form.getSecondName().replaceAll("\\s+"," ").trim());
            }
            if(!StringUtils.isEmpty(form.getSecondSurname())) {
                customerModel.setSecondSurname(form.getSecondSurname().replaceAll("\\s+"," ").trim());
            }
            if(form.getDriverLicenceState() != null && !StringUtils.isEmpty(form.getDriverLicenceState())) {
                customerModel.setDriverLicenceState(form.getDriverLicenceState().replaceAll("\\(", "").replaceAll("-", "").replaceAll("\\)", ""));
            }
            if(form.getFixedPhone() != null && !StringUtils.isEmpty(form.getFixedPhone())) {
                customerModel.setFixedPhone(form.getFixedPhone().replaceAll("\\(", "").replaceAll("-", "").replaceAll("\\)", ""));
            }
            if(form.getAdditionalEmail() != null && !StringUtils.isEmpty(form.getAdditionalEmail())) {
                customerModel.setAdditionalEmail(form.getAdditionalEmail());
            }

//            if(StringUtils.isNotEmpty(form.getSsn()))
//            {
//                final String ssnValue = llaEncryptionUtil.doMasking(form.getSsn().replaceAll("-",""));
//                customerModel.setCustomerSSN(ssnValue);
//            }
            updateSelectedAddress("SELECTED_ADDRESS");
        }
    }

    @Override
    public void updateContractDetails(CustomerModel customerModel, ContractTypeForm form) {
       final CartModel cartModel = cartService.getSessionCart();
       cartModel.setContractType(ContractType.valueOf(form.getContractType()));
       modelService.save(cartModel);

    }

    /**
     * Update Delivery Address
     * @param form
     * @param customerModel
     */
    private void updateDeliveryAddress(CustomerProfileForm form, CustomerModel customerModel) {
   	 
   	  final CartModel cartModel = cartService.getSessionCart();
   	  if(null != cartModel && null !=cartModel.getDeliveryAddress()){
   		  
   		  AddressModel address = cartModel.getDeliveryAddress();
   		  address.setFirstname(form.getFirstName().trim());
   		  address.setLastname(form.getLastName().trim());
   		  address.setPhone1(form.getMobilePhone());
   		  address.setPhone2(form.getFixedPhone());
   		  address.setEmail(form.getAdditionalEmail()!=null?form.getAdditionalEmail():customerModel.getAdditionalEmail());
   		  address.setCellphone(form.getFixedPhone());
   		  modelService.save(address);
   		  modelService.refresh(address);
   		  
   	  }
    }

    /**
     * Update Personal Details in Selected Address
     */
    public void updateSelectedAddress(final String addressType) {

        final CartModel cartModel = cartService.getSessionCart();
        CustomerModel customerModel=(CustomerModel)cartModel.getUser();
        AddressModel address=null;
        switch (addressType){
            case "SELECTED_ADDRESS":
                if(null!=cartModel.getSelectedAddress()){
			address=cartModel.getSelectedAddress();
		}else{
		   address=cartModel.getDeliveryAddress();
		}
			
                updatePersonalDetailsInAddress(customerModel, address);
                break;
            case "CREDIT_ADDRESS":
                address=cartModel.getCreditCheckAddress();
                updatePersonalDetailsInAddress(customerModel, address);
                break;
        }
    }

    /**
     * Update Customer Name Emails in Address
     * @param customerModel
     * @param address
     */
    private void updatePersonalDetailsInAddress(CustomerModel customerModel, AddressModel address) {
        address.setFirstname(customerModel.getFirstName());
        address.setLastname(customerModel.getLastName());
        address.setMiddlename(customerModel.getSecondName());
        address.setMiddlename2(customerModel.getSecondSurname());
        address.setEmail(customerModel.getAdditionalEmail());
        address.setCellphone(customerModel.getMobilePhone());
        modelService.save(address);
        modelService.refresh(address);
    }
    
    /**
	  * Update Customer SSN
	  */
	 @Override
	 public void updateCustomerSSN(final String ssn)
	 {
		 final CartModel cartModel = cartService.getSessionCart();
		 CustomerModel customerModel = (CustomerModel) cartModel.getUser();
		 if (StringUtils.isNotEmpty(ssn))
		 {
			 final String ssnValue = llaEncryptionUtil.doMasking(ssn.replaceAll("-", ""));
			 customerModel.setCustomerSSN(ssnValue);
			 modelService.save(customerModel);
			 modelService.refresh(customerModel);
		 }
	 }

}
