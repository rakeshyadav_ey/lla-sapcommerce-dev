package com.lla.telcoaddon.service;

import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.telcoaddon.forms.ContractTypeForm;
import com.lla.telcoaddon.forms.CustomerProfileForm;
import de.hybris.platform.core.model.user.CustomerModel;
import org.springframework.validation.Errors;
import com.lla.mulesoft.integration.response.LLAApiResponse;

public interface LLAPersonalDetailService {

  /**
   * Update Personal Details Form
   * @param customerModel
   * @param form
   */
	void updatePersonalDetails(CustomerModel customerModel, CustomerProfileForm form);
  
   /**
   * Update Contract Details
   * @param customerModel
   * @param form
   */
  void updateContractDetails(CustomerModel customerModel, ContractTypeForm form);

  /**
   * Update Customer Contact Details
   * @param customerModel
   * @param email
   * @param  mobilePhone
   */
  void updateContactDetails(CustomerModel customerModel,String email, String mobilePhone, String documentType, String documentNumber); 

  /**
   * Update Credit Check Details
   * @return LLAApiResponse
   */
  LLAApiResponse updateCreditCheckDetails() throws LLAApiException;

  /**
   * Update Personal Details in Address
   */
   void updateSelectedAddress(final String addressType) ;
   
   /**
	 * Update Customer SSN
    */
	void updateCustomerSSN(final String ssn);
}
