package com.lla.telcoaddon.forms.validation;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.lla.telcoaddon.forms.CustomerSSNForm;

@Component("customerSSNFormValidator")
public class CustomerSSNFormValidator implements Validator {
    private static final Logger LOG = Logger.getLogger(CustomerSSNFormValidator.class);
    
    @Override
    public boolean supports(Class<?> aClass) {
        return CustomerSSNForm.class.equals(aClass);
    }
    
    @Override
    public void validate(final Object object, final Errors errors)
    {
   	 final CustomerSSNForm customerSSNForm = (CustomerSSNForm)object;
   	 String ssn= customerSSNForm.getSsn();
   	 if (ssn == null || StringUtils.isEmpty(ssn)) {
          errors.rejectValue("ssn", "register.ssn.lcpr.empty");
   	 } else if (ssn.length() > 11 || ssn.length() < 11) {
          errors.rejectValue("ssn", "register.ssn.lcpr.invalid");
   	 }
   	 
    }
    
}