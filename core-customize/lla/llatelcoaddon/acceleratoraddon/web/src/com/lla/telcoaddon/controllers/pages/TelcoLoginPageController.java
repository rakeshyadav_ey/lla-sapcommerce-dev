/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcoaddon.controllers.pages;

import com.lla.core.util.LLACommonUtil;
import com.lla.facades.product.data.ProductRegionData;
import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;

import com.lla.common.addon.idp.strategy.LLALoginStrategy;
import com.lla.telcoaddon.controllers.ControllerConstants;
import com.lla.telcoaddon.controllers.TelcoControllerConstants;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Collections;
import java.util.List;


/**
 * Login Controller. Handles login and register for the account flow.
 */
@Controller
@RequestMapping(value = "/login")
public class TelcoLoginPageController extends AbstractLoginPageController
{
	private final String[] DISALLOWED_FIELDS = new String[] {};
	@Resource(name = "llaLoginStrategy")
	private LLALoginStrategy llaLoginStrategy;
	@Autowired
	private ConfigurationService configurationService;
	@Autowired
	private LLACommonUtil llaCommonUtil;
	@Override
	protected String getView()
	{
		return TelcoControllerConstants.Views.Pages.Account.ACCOUNT_LOGIN_PAGE;
	}

	@ModelAttribute("authUrl")
	public String getAuthorizationUrl(final HttpServletRequest request)
	{
		request.getSession().setAttribute("llaRefererUrl",ROOT);
		return llaLoginStrategy.authUrlConstructor(request, "auth");
	}

	@ModelAttribute("areas")
	public List<ProductRegionData> getAreaList()
	{
		return  llaCommonUtil.isSiteJamaica()?llaCommonUtil.getJamaicaRegions(): Collections.emptyList();
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		if (httpSessionRequestCache.getRequest(request, response) != null)
		{
			return httpSessionRequestCache.getRequest(request, response).getRedirectUrl();
		}
		return "/";
	}

	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId("login");
	}

	private HttpSessionRequestCache httpSessionRequestCache;

	@Autowired
	@Qualifier("httpSessionRequestCache")
	public void setHttpSessionRequestCache(final HttpSessionRequestCache accHttpSessionRequestCache)
	{
		this.httpSessionRequestCache = accHttpSessionRequestCache;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String doLogin(@RequestHeader(value = "referer", required = false) final String referer,
			@RequestParam(value = "error", defaultValue = "false") final boolean loginError, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final HttpSession session)
			throws CMSItemNotFoundException
	{
		if (!loginError)
		{
			storeReferer(referer, request, response);
		}
		if (configurationService.getConfiguration().getBoolean("lla.customer.login.idp.enabled", true)){
			return REDIRECT_PREFIX + ROOT;
		}else{
			return getDefaultLoginPage(loginError, session, model);
		}
	}

	protected void storeReferer(final String referer, final HttpServletRequest request, final HttpServletResponse response)
	{
		if (StringUtils.isNotBlank(referer))
		{
			httpSessionRequestCache.saveRequest(request, response);
		}
	}

	/**
	 * Registration.
	 *
	 * @param referer
	 * @param form
	 * @param bindingResult
	 * @param model
	 * @param request
	 * @param response
	 * @param redirectModel
	 * @return
	 * @throws CMSItemNotFoundException
	 */

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String doRegister(@RequestHeader(value = "referer", required = false) final String referer, final RegisterForm form,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		return processRegisterUserRequest(referer, form, bindingResult, model, request, response, redirectModel);
	}

	/**
	 * Terms and Condition pop-up
	 *
	 * @param model
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/register/termsandconditions")
	public String getTermsAndConditions(final Model model) throws CMSItemNotFoundException
	{
		final ContentPageModel pageForRequest = getCmsPageService().getPageForLabel("/termsAndConditions");
		storeCmsPageInModel(model, pageForRequest);
		setUpMetaDataForContentPage(model, pageForRequest);
		return ControllerConstants.Views.Fragments.Checkout.TermsAndConditionsPopup;
	}

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		binder.setDisallowedFields(DISALLOWED_FIELDS);
	}
}
