/**
 *
 */
package com.lla.telcoaddon.forms;

/**
 * @author GF986ZE
 *
 */
public class AccountNumberForm
{
    private String accountNumber;

    /**
     * @return the accountNumber
     */
    public String getAccountNumber()
    {
        return accountNumber;
    }

    /**
     * @param accountNumber
     *           the accountNumber to set
     */
    public void setAccountNumber(final String accountNumber)
    {
        this.accountNumber = accountNumber;
    }



}