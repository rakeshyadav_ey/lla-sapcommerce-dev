package com.lla.telcoaddon.forms.validation;
import com.lla.core.enums.DocumentType;
import com.lla.core.util.LLACommonUtil;
import com.lla.telcoaddon.forms.CustomerProfileForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateProfileForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.ProfileValidator;
import de.hybris.platform.commons.enums.DocumentTypeEnum;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Years;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;

@Component("customerProfileFormValidator")
public class CustomerProfileFormValidator implements Validator {
    private static final Logger LOG = Logger.getLogger(CustomerProfileFormValidator.class);
    @Autowired
    private ConfigurationService configurationService;

    @Override
    public boolean supports(Class<?> aClass) {
        return CustomerProfileForm.class.equals(aClass);
    }

    @Autowired
    private LLACommonUtil llaCommonUtil;

    public void contactDetailsValidate(final Object object, final Errors errors){
        final CustomerProfileForm customerProfileForm = (CustomerProfileForm) object;
        if(llaCommonUtil.isSitePanama()){
            if(StringUtils.isEmpty(customerProfileForm.getMobilePhone()) || !validatePhone(customerProfileForm.getMobilePhone())){
                errors.rejectValue("mobilePhone", "register.mobilePhone.invalid");
            }
            if(StringUtils.isEmpty(customerProfileForm.getAdditionalEmail()) || !validateEmail(customerProfileForm.getAdditionalEmail())){
                errors.rejectValue("additionalEmail", "register.additionalEmail.invalid");
            }
        }
    }
    public void contactEmailValidate(final Object object, final Errors errors){
        final CustomerProfileForm customerProfileForm = (CustomerProfileForm) object;
        if(llaCommonUtil.isSitePanama()){
            if(StringUtils.isEmpty(customerProfileForm.getAdditionalEmail()) || !validateEmail(customerProfileForm.getAdditionalEmail())){
                errors.rejectValue("additionalEmail", "register.additionalEmail.invalid");
            }
        }
    }
    @Override
    public void validate(final Object object, final Errors errors)
    {
        final CustomerProfileForm customerProfileForm = (CustomerProfileForm) object;
        if(llaCommonUtil.isSiteJamaica())
            validateJamaicaProfileForm(errors, customerProfileForm);
        if(llaCommonUtil.isSitePuertorico())
            validateLCPRProfileForm(errors, customerProfileForm);
        if(llaCommonUtil.isSitePanama())
            validatePanamaProfileForm(errors, customerProfileForm);
        if(llaCommonUtil.isSiteCabletica()) validateCableticaProfileForm(errors, customerProfileForm);
    }

    private void validateCableticaProfileForm(Errors errors, CustomerProfileForm customerProfileForm) {
		 
        if(StringUtils.isEmpty(customerProfileForm.getFirstName()) || validateName(customerProfileForm.getFirstName())){
            errors.rejectValue("firstName", "register.firstName.invalid");
        }
        if(StringUtils.isEmpty(customerProfileForm.getLastName()) || validateName(customerProfileForm.getLastName())){
            errors.rejectValue("lastName", "profile.lastName.invalid");
        }

        /*   if(StringUtils.isEmpty(customerProfileForm.getMobilePhone()) || !validatePhone(customerProfileForm.getMobilePhone())){
               errors.rejectValue("mobilePhone", "register.mobilePhone.invalid");
           }*/
    }

    private void validatePanamaProfileForm(Errors errors, CustomerProfileForm customerProfileForm) {
        if(StringUtils.isEmpty(customerProfileForm.getDocumentType()) || !validateDocumentTypeSupportedValue(customerProfileForm.getDocumentType())){
            errors.rejectValue("documentType", "register.documentType.invalid");
        }
        if(StringUtils.isEmpty(customerProfileForm.getDocumentNumber()) || validateCedulaID(customerProfileForm.getDocumentNumber(), customerProfileForm.getDocumentType()) ){
            errors.rejectValue("documentNumber", "register.document.cedula.id.invalid");
        }
        if(StringUtils.isEmpty(customerProfileForm.getDocumentNumber()) || validatePassportID(customerProfileForm.getDocumentNumber(), customerProfileForm.getDocumentType())){
            errors.rejectValue("documentNumber", "register.document.passport.id.invalid");
        }
        if(StringUtils.isEmpty(customerProfileForm.getFirstName()) || validateName(customerProfileForm.getFirstName())){
            errors.rejectValue("firstName", "register.firstName.invalid");
        }
        if(StringUtils.isEmpty(customerProfileForm.getLastName()) || validateName(customerProfileForm.getLastName())){
            errors.rejectValue("lastName", "profile.lastName.invalid");
        }
    }

    private void validateLCPRProfileForm(Errors errors, CustomerProfileForm customerProfileForm) {
        //String phoneNumber= customerProfileForm.getFixedPhone().replaceAll("\\(","").replaceAll("-","").replaceAll("\\)","");
        String ssn= customerProfileForm.getSsn();
        if(StringUtils.isEmpty(customerProfileForm.getDocumentType()) || !validateDocumentTypeSupportedValue(customerProfileForm.getDocumentType())){
            errors.rejectValue("documentType", "register.documentType.invalid");
        }
        if(StringUtils.isEmpty(customerProfileForm.getDocumentNumber()) && customerProfileForm.getDocumentType().equalsIgnoreCase(DocumentType.DRIVER_LICENSE.getCode())){
            errors.rejectValue("documentNumber", "register.document.driver.license.empty");
        }


           /* if(StringUtils.isEmpty(customerProfileForm.getDocumentNumber()) && customerProfileForm.getDocumentType().equalsIgnoreCase(DocumentType.PASSPORT.getCode())){
                errors.rejectValue("documentNumber", "register.document.passport.empty");
            } */


        if((StringUtils.isNotEmpty(customerProfileForm.getDocumentNumber()) && !customerProfileForm.getDocumentType().equalsIgnoreCase("OTHER") && customerProfileForm.getDocumentNumber().length() > 25) && validateDL_PassportID(customerProfileForm.getDocumentNumber(), customerProfileForm.getDocumentType())){
            errors.rejectValue("documentNumber", "register.document.driver.license.id.invalid");
        }


        /*
        if(customerProfileForm.isCheckSsn() && StringUtils.isEmpty(customerProfileForm.getFixedPhone()) || phoneNumber.length()>10 || phoneNumber.length()<10||  !validatePhone(phoneNumber)){
            errors.rejectValue("fixedPhone", "register.fixedPhone.lcpr.invalid");
        }
        if(customerProfileForm.isCheckSsn() && StringUtils.isEmpty(customerProfileForm.getAdditionalEmail()) || !validateEmail(customerProfileForm.getAdditionalEmail())){
            errors.rejectValue("additionalEmail", "register.additionalEmail.lcpr.invalid");
        }
        */
        if(null== customerProfileForm.getDob() || StringUtils.isEmpty(customerProfileForm.getDob().trim())){
            errors.rejectValue("dob", "register.dob.lcpr.invalid");
        }
        if(StringUtils.isNotEmpty(customerProfileForm.getDob()) &&  !validateDOBPuertoricoFormat (customerProfileForm.getDob())){
            errors.rejectValue("dob", "register.dob.puertorico.invalid");
        }

//        if(customerProfileForm.isCheckSSN()) {
//            if (ssn == null || StringUtils.isEmpty(ssn)) {
//                errors.rejectValue("ssn", "register.ssn.lcpr.empty");
//            } else if (ssn.length() > 11 || ssn.length() < 11) {
//                errors.rejectValue("ssn", "register.ssn.lcpr.invalid");
//            }
//        }


         /*   if(StringUtils.isNotEmpty(customerProfileForm.getDob()) &&  !isUser18Older (customerProfileForm.getDob())){
                errors.rejectValue("dob", "register.dob.after.older18");
            }
*/
        if(configurationService.getConfiguration().getBoolean("lcpr.profile.form.name.validate")){
            if(StringUtils.isEmpty(customerProfileForm.getFirstName()) || validateName(customerProfileForm.getFirstName())){
                errors.rejectValue("firstName", "register.firstName.lcpr.invalid");
            }
            if(StringUtils.isEmpty(customerProfileForm.getLastName()) || validateName(customerProfileForm.getLastName())){
                errors.rejectValue("lastName", "profile.lastName.lcpr.invalid");
            }

            if(StringUtils.isNotEmpty(customerProfileForm.getSecondName()) && validateName(customerProfileForm.getSecondName())){
                errors.rejectValue("secondName", "profile.secondName.lcpr.invalid");
            }
            if(StringUtils.isNotEmpty(customerProfileForm.getSecondSurname()) && validateName(customerProfileForm.getSecondSurname())){
                errors.rejectValue("secondSurname", "profile.secondSurName.lcpr.invalid");
            }
            /*String updatedLastName= customerProfileForm.getLastName().replaceAll("\\s+"," ").trim();
            String updatedFirstName= customerProfileForm.getFirstName().replaceAll("\\s+"," ").trim();
            String updatedSecondName= customerProfileForm.getSecondName().replaceAll("\\s+"," ").trim();
            String updatedSecondSurName= customerProfileForm.getSecondSurname().replaceAll("\\s+"," ").trim();
            String name[]={updatedFirstName,updatedLastName,updatedSecondName,updatedSecondSurName};
            if(StringUtils.join(name," ").length()>26){
                errors.rejectValue("lastName", "profile.name.length.invalid");
                errors.rejectValue("firstName", "profile.name.length.invalid");
                errors.rejectValue("secondName", "profile.secondName.lcpr.invalid");
                errors.rejectValue("secondSurname", "profile.secondSurName.lcpr.invalid");
            }*/
        }
    }

    private void validateJamaicaProfileForm(Errors errors, CustomerProfileForm customerProfileForm) {

        if(StringUtils.isNotEmpty(customerProfileForm.getFixedPhone()))
         {
           String additionalContactNo = customerProfileForm.getFixedPhone().replaceAll("[\\(|-|\\)|\\s]", "").trim();
             if(additionalContactNo.length()>10 || additionalContactNo.length()<10||  !validatePhone(additionalContactNo)){
                 errors.rejectValue("fixedPhone", "register.mobilePhone.invalid");
             }
         }
        if (StringUtils.isEmpty(customerProfileForm.getTrnNumber()) || StringUtils.length(customerProfileForm.getTrnNumber()) > 11
                || !validateTrnPattern(customerProfileForm.getTrnNumber()))
        {
            errors.rejectValue("trnNumber", "register.trnNumber.invalid");
        }

        if(StringUtils.isEmpty(customerProfileForm.getFirstName()) || validateName(customerProfileForm.getFirstName())){
            errors.rejectValue("firstName", "register.firstName.invalid");
        }
        if(StringUtils.isEmpty(customerProfileForm.getLastName()) || validateName(customerProfileForm.getLastName())){
            errors.rejectValue("lastName", "profile.lastName.invalid");
        }
    }

    private boolean isUser18Older(String dob) {
        DateTime dateTime=DateTime.parse(dob);
        DateTime now = new DateTime();
        Years age = Years.yearsBetween(dateTime, now);
        return age.getYears() >= 18;
    }

    /** Validate Email
     *
     * @param email
     * @return
     */
    private boolean validateEmail(String email) {
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     *  Validate Phone
     * @param phone
     * @return
     */
    private boolean validatePhone(String phone) {
        Pattern digit = Pattern.compile("^[0-9]+$");
        Matcher hasDigit = digit.matcher(phone);
        return  hasDigit.find();

    }

    /**
     *  Validate Passport Id
     * @param documentNumber
     * @param documentType
     * @return
     */

    private boolean validatePassportID(final String documentNumber,final  String documentType) {
        Boolean returnFlag = Boolean.FALSE;
        if (documentType.equalsIgnoreCase(DocumentType.PASSPORT.getCode()) && documentNumber.length() > 20) {
            return Boolean.TRUE;
        }
        return returnFlag.booleanValue();
    }

    /**
     *
     * @param documentNumber
     * @param documentType
     * @return
     */
    private boolean validateDL_PassportID(final String documentNumber,final  String documentType) {
        if (documentType.equalsIgnoreCase(DocumentType.DRIVER_LICENSE.getCode())) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
    /**
     * Validate Cedula ID Number
     * @param documentNumber
     * @param documentType
     * @return
     */
	 
    private boolean validateCedulaID(final String documentNumber,final  String documentType) {
       Boolean returnFlag = Boolean.FALSE;
       if(documentType.equalsIgnoreCase(DocumentType.CEDULA.getCode()) ){
           Pattern regex = Pattern.compile("[$&+,:;=\\\\?@#|/'<>.^*()%!\\s]");
           returnFlag= regex.matcher(documentNumber).find() ;
       }
       return returnFlag;
   }
	  

    /**
     * Validate Cedula ID Number
     * @param documentNumber
     * @return
     */
    private boolean validateCedulaNationalID(final String documentNumber) {
        Pattern regex = Pattern.compile("[0][0-9]{9}$");
        return  regex.matcher(documentNumber).find();
    }
    
    /**
     * Validate Document Type
     * @param documentType
     * @return
     */
    private boolean validateDocumentTypeSupportedValue(String documentType) {
        return documentType.equalsIgnoreCase(DocumentType.CEDULA.getCode()) || documentType.equalsIgnoreCase(DocumentType.PASSPORT.getCode()) || documentType.equalsIgnoreCase(DocumentType.DRIVER_LICENSE.getCode())  || documentType.equalsIgnoreCase(DocumentType.OTHER.getCode())
                || documentType.equalsIgnoreCase(DocumentType.CEDULARESIDENTIAL.getCode()) || documentType.equalsIgnoreCase(DocumentType.CEDULANATIONAL.getCode());

    }

    /**
     * Validate Name
     * @param name
     * @return
     */
    private boolean validateName(String name) {

        Pattern digit = Pattern.compile("[0-9]");
        Pattern special = Pattern.compile ("[!@#$%&*()_+=|<>?:;{}\\[\\]~]");
        Matcher hasDigit = digit.matcher(name);
        Matcher hasSpecial = special.matcher(name);

        return  hasDigit.find() || hasSpecial.find();
    }

    /**
     * Validate Date Of Birth
     * @param dob
     * @return
     */
    private boolean validateDOBIsAfterTodaysDate(String dob) {
        Date currentDate = new Date();
        try
        {
            SimpleDateFormat sdfrmt = new SimpleDateFormat("yyyy-MM-dd");
            Date dobDate = sdfrmt.parse(dob);
            if(dobDate.after(currentDate)){
                LOG.error("Date of Birth should not be greater then Today's date");
                return false;
            }
            LOG.error(dob+" is valid date format");
            return true;
        }
        /* Date format is invalid */
        catch (ParseException exception)
        {
            LOG.error(dob+" is Invalid Date format");
        }
        return false;
    }

    /**
     * Validate DOB Format
     * @param dob
     * @return
     */
    private boolean validateDOBFormat(String dob) {
        SimpleDateFormat sdfrmt = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            Date dobDate = sdfrmt.parse(dob);
            Date currentDate =new Date();
            if(dobDate.after(currentDate)){
                LOG.error("Date of Birth should not be greater then Today's date");
                return false;
            }
            LOG.error(dob+" is valid date format");
            return true;
        }
        /* Date format is invalid */
        catch (ParseException exception)
        {
            LOG.error(dob+" is Invalid Date format");
            return false;
        }
    }

    private boolean validateDOBPuertoricoFormat(String dob) {
        int years = 0;
        final SimpleDateFormat sdfrmt = new SimpleDateFormat("dd-MM-yyyy");
        final Calendar currentCalender = Calendar.getInstance();
        final Calendar dobCalender = Calendar.getInstance();
        sdfrmt.setLenient(false);
        try
        {
            final Date dobDate = sdfrmt.parse(dob);
            final Date currentDate =new Date();
            currentCalender.setTime(currentDate);
            dobCalender.setTime(dobDate);
            years = currentCalender.get(Calendar.YEAR) - dobCalender.get(Calendar.YEAR);
            if ((years < 18 || years > 99) || dobDate.after(currentDate))
            {
                LOG.error("Date of Birth should not be greater then Today's date");
                return false;
            }

            LOG.error(dob+" is valid date format");
            return true;
        }
        /* Date format is invalid */
        catch (final ParseException exception)
        {
            LOG.error(dob+" is Invalid Date format");
            return false;
        }
    }

    /**
     * Validate TRN Pattern
     * @param patternToTest
     * @return
     */
    private boolean validateTrnPattern(String patternToTest) {
        String regex="[0-9]{3}-[0-9]{3}-[0-9]{3}";
        Pattern r = Pattern.compile(regex);
        Matcher m = r.matcher(patternToTest);
        if (m.find( )) {
            LOG.info("TRN Validated");
            return true;
        }
        LOG.info("TRN Not Validated");
        return false;
    }

    /***
     *  Validate Gender Value
     * @param form
     * @return
     */
    private boolean validateGenderSupportedValue(CustomerProfileForm form) {
        return  form.getGender().equalsIgnoreCase("M") || form.getGender().equalsIgnoreCase("male")
                ||form.getGender().equalsIgnoreCase("F")||form.getGender().equalsIgnoreCase("female");
    }
}
