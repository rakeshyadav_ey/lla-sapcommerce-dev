package de.hybris.platform.storefront.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.AbstractCheckoutStepValidator;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public class DefaultContactDetailsCheckoutStepValidator extends AbstractCheckoutStepValidator {
        private static final Logger LOG = LoggerFactory.getLogger(DefaultContactDetailsCheckoutStepValidator.class); // NOSONAR

    @Override
    public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
    {
        if (!getCheckoutFlowFacade().hasValidCart())
        {
            LOG.info("Missing, empty or unsupported cart");
            return ValidationResults.REDIRECT_TO_CART;
        }
        return ValidationResults.SUCCESS;
    }
}
