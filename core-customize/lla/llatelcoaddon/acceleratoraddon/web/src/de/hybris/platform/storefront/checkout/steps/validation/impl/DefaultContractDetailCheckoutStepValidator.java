package de.hybris.platform.storefront.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.AbstractCheckoutStepValidator;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public class DefaultContractDetailCheckoutStepValidator   extends AbstractCheckoutStepValidator {
        private static final Logger LOG = LoggerFactory.getLogger(DefaultContractDetailCheckoutStepValidator.class); // NOSONAR
    @Override
    public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
    {
        return ValidationResults.SUCCESS;
    }
}
