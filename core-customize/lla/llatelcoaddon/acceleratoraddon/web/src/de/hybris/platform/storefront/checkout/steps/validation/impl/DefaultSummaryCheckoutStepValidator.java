/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.storefront.checkout.steps.validation.impl;

import com.lla.core.util.LLACommonUtil;
import de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.AbstractCheckoutStepValidator;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.data.CartData;

import javax.annotation.Resource;

import de.hybris.platform.order.CartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import com.lla.facades.checkout.LLACheckoutFlowFacade;

/**
 * Default implementation of checkout summary validator.
 */
public class DefaultSummaryCheckoutStepValidator extends AbstractCheckoutStepValidator
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultSummaryCheckoutStepValidator.class); // NOSONAR
	@Resource(name="cmsSiteService")
	private CMSSiteService cmsSiteService;
	
	@Resource(name = "checkoutFlowFacade")
	private LLACheckoutFlowFacade llaCheckoutFlowFacade;

	@Resource(name = "llaCommonUtil")
	private LLACommonUtil llaCommonUtil;

	@Autowired
	private CartService cartService;
	@Override
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		final ValidationResults checkoutFlow = validateCheckoutFlow(redirectAttributes);
		if (checkoutFlow != null)
		{
			return checkoutFlow;
		}

		final CartData cartData = getCheckoutFacade().getCheckoutCart();

		if (!getCheckoutFacade().hasShippingItems())
		{
			cartData.setDeliveryAddress(null);
		}

		return ValidationResults.SUCCESS;
	}

	protected ValidationResults validateCheckoutFlow(final RedirectAttributes redirectAttributes)
	{
		final CheckoutFlowFacade checkoutFlowFacade = getCheckoutFlowFacade();
		if (!checkoutFlowFacade.hasValidCart())
		{
			LOG.info("Missing, empty or unsupported cart");
			return ValidationResults.REDIRECT_TO_CART;
		}
		
		if(cmsSiteService.getCurrentSite().getUid().equalsIgnoreCase("jamaica") && llaCheckoutFlowFacade.hasNoInstallationAddress()) {
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.installationAddress.notprovided");
			return ValidationResults.REDIRECT_TO_INSTALLATION_ADDRESS;
		}

		if (!cmsSiteService.getCurrentSite().getUid().equalsIgnoreCase("jamaica") && checkoutFlowFacade.hasNoDeliveryAddress() && !llaCommonUtil.containsOnlyMobileProducts(cartService.getSessionCart()))
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}

	/*	if (llaCommonUtil.isSitePanama() && !llaCommonUtil.hideDeliveryMethod(cartService.getSessionCart()) && llaCheckoutFlowFacade.hasNoDeliveryMode())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.deliveryMethod.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_METHOD;
		}
*/
		if (checkoutFlowFacade.hasNoPaymentInfo() && llaCommonUtil.isSiteJamaica() && !llaCommonUtil.hidePaymentSectionForJamaica())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.paymentDetails.notprovided");
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}
	 /*  if (llaCheckoutFlowFacade.hasNoAdditionalPaymentInfo() && llaCommonUtil.isSitePanama() && !llaCommonUtil.hidePaymentMethod(cartService.getSessionCart()))
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.paymentDetails.notprovided");
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}*/
		return null;
	}
}
