package de.hybris.platform.storefront.checkout.steps.validation.impl;

import com.lla.core.util.LLACommonUtil;
import com.lla.facades.checkout.LLACheckoutFlowFacade;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.AbstractCheckoutStepValidator;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.order.CartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;

public class DefaultAccountNumberCheckoutStepValidator extends AbstractCheckoutStepValidator {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultAccountNumberCheckoutStepValidator.class); // NOSONAR
    @Resource(name = "checkoutFlowFacade")
    private LLACheckoutFlowFacade llaCheckoutFlowFacade;
    @Resource(name="cmsSiteService")
    private CMSSiteService cmsSiteService;

    @Resource(name = "llaCommonUtil")
    private LLACommonUtil llaCommonUtil;
    @Autowired
    private CartService cartService;
    @Override
    public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
    {
        if (!getCheckoutFlowFacade().hasValidCart())
        {
            LOG.info("Missing, empty or unsupported cart");
            return ValidationResults.REDIRECT_TO_CART;
        }
		/*
		 * if(cmsSiteService.getCurrentSite().getUid().equalsIgnoreCase("jamaica") &&
		 * llaCheckoutFlowFacade.hasNoInstallationAddress()) { GlobalMessages.addFlashMessage(redirectAttributes,
		 * GlobalMessages.INFO_MESSAGES_HOLDER, "checkout.multi.installationAddress.notprovided"); return
		 * ValidationResults.REDIRECT_TO_INSTALLATION_ADDRESS; }
		 */
        if (getCheckoutFlowFacade().hasNoPaymentInfo() && llaCommonUtil.isSiteJamaica() && !llaCommonUtil.hidePaymentSectionForJamaica())
        {
            GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
                    "checkout.multi.paymentDetails.notprovided");
            return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
        }
        if (llaCheckoutFlowFacade.hasNoAdditionalPaymentInfo() && llaCommonUtil.isSitePanama() && !llaCommonUtil.hidePaymentMethod(cartService.getSessionCart()))
        {
            GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
                    "checkout.multi.paymentDetails.notprovided");
            return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
        }

        return ValidationResults.SUCCESS;
    }
}
