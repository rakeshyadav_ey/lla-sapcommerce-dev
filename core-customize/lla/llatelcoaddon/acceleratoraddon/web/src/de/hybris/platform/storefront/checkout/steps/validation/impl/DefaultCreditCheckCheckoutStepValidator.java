package de.hybris.platform.storefront.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.AbstractCheckoutStepValidator;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.core.model.order.CartModel;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;	
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.lla.core.enums.ContractType;

public class DefaultCreditCheckCheckoutStepValidator extends AbstractCheckoutStepValidator {
        private static final Logger LOG = LoggerFactory.getLogger(DefaultCreditCheckCheckoutStepValidator.class); // NOSONAR


	@Resource(name="cmsSiteService")
	private CMSSiteService cmsSiteService;
	
	@Resource(name="cartService")
	private CartService cartService;
	

    @Override
    public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	 {
   	 CartModel cart = cartService.getSessionCart();
   	 ContractType contractType = cart.getContractType();
   	 if (!getCheckoutFlowFacade().hasValidCart())
       {
           LOG.info("Missing, empty or unsupported cart");
           return ValidationResults.REDIRECT_TO_CART;
       }

		 if (cmsSiteService.getCurrentSite().getUid().equalsIgnoreCase("puertorico")
				 && (null == contractType))
		 {
			 GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					 "checkout.multi.contractType.notprovided");
			 return ValidationResults.REDIRECT_TO_CONTRACT_DETAIL;
		 }
		 return ValidationResults.SUCCESS;
	 }
} 