/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.storefront.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.AbstractCheckoutStepValidator;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.lla.core.util.LLACommonUtil;
import com.lla.facades.checkout.LLACheckoutFlowFacade;

/**
 * Default implementation of payment validator.
 */
public class DefaultPaymentCheckoutStepValidator extends AbstractCheckoutStepValidator
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPaymentCheckoutStepValidator.class); // NOSONAR


	@Resource(name="cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "checkoutFlowFacade")
	private LLACheckoutFlowFacade llaCheckoutFlowFacade;

	@Resource(name = "llaCommonUtil")
	private LLACommonUtil llaCommonUtil;


	@Autowired
	private CartService cartService;


	@Override
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		if (!getCheckoutFlowFacade().hasValidCart())
		{
			LOG.info("Missing, empty or unsupported cart");
			return ValidationResults.REDIRECT_TO_CART;
		}
		if(llaCommonUtil.isSiteJamaica() && llaCheckoutFlowFacade.hasNoInstallationAddress()) {
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.installationAddress.notprovided");
			return ValidationResults.REDIRECT_TO_INSTALLATION_ADDRESS;
		}

		if (llaCheckoutFlowFacade.hasNoDeliveryAddress() && llaCommonUtil.isSitePanama())

		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}
		if (llaCommonUtil.isSitePanama() && !llaCommonUtil.hideDeliveryMethod(cartService.getSessionCart()) && llaCheckoutFlowFacade.hasNoDeliveryMode())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.deliveryMode.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_METHOD;
		}

		return ValidationResults.SUCCESS;
	}
}
