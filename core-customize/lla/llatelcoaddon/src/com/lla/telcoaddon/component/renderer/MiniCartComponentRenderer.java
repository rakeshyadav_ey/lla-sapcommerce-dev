/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcoaddon.component.renderer;

import com.lla.core.util.LLACommonUtil;
import de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel;
import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;
import com.lla.telcoaddon.constants.LlatelcoaddonConstants;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.jsp.PageContext;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Renderer for mini cart component - it shows only the total items count
 */
public class MiniCartComponentRenderer<C extends MiniCartComponentModel> extends DefaultAddOnCMSComponentRenderer<C>
{
   private static final String COMPONENT = "component";
   private static final String TOTAL_ITEMS = "totalItems";
   private CartFacade cartFacade;
   public static final String FOUR_P_BUNDLES = "4Pbundles";
   public static final String THREE_P_BUNDLES = "3Pbundles";
   public static final String TWO_P_INTERNET_TV = "2PInternetTv";
   public static final String TWO_P_INTERNET_TELEPHONE = "2PInternetTelephone";

   @Resource(name = "llaCommonUtil")
   private LLACommonUtil llaCommonUtil;
   @Override
   protected Map<String, Object> getVariablesToExpose(final PageContext pageContext, final C component)
   {
      final Map<String, Object> model = super.getVariablesToExpose(pageContext, component);
      final CartData cartData = getCartFacade().getMiniCart();
      Boolean planExist= Boolean.FALSE;
      model.put(TOTAL_ITEMS, cartData.getTotalUnitCount());
      model.put(COMPONENT, component);

      if (llaCommonUtil.isSitePanama() && cartFacade.hasSessionCart() && cartData.getEntries().size() > 0) {
         for (OrderEntryData entry : cartData.getEntries()) {
            String category = entry.getProduct().getCategories().iterator().next().getCode();
            if (StringUtils.isNotEmpty(category) && (category.equals(FOUR_P_BUNDLES) || category.equals(THREE_P_BUNDLES) || category.equals(TWO_P_INTERNET_TV) || category.equals(TWO_P_INTERNET_TELEPHONE))) {
               planExist=Boolean.TRUE;
               model.put("oldProductName", entry.getProduct().getName());
               break;
            }
         }
         model.put("PlanExist", planExist);
      }

      return model;
   }

   @Override
   protected String getAddonUiExtensionName(final C component)
   {
      return LlatelcoaddonConstants.EXTENSIONNAME;
   }

   protected CartFacade getCartFacade()
   {
      return cartFacade;
   }

   @Required
   public void setCartFacade(final CartFacade cartFacade)
   {
      this.cartFacade = cartFacade;
   }
}


