# ---------------------------------------------------------------------------
# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# ---------------------------------------------------------------------------

#
# Import the Solr configuration for the B2CTelco store
#

################################################################################################################################
# As of 1810 this solr considers eligibility - as a consequence all prices are now indexed (instead of the offerings)
# You can still find the old solr.impex under solr_woeligibility.impex, however you will need to adjust the spring beans.
#
# Note:
# as classification is defined as sample data some SolrIndexedProperties are defined under solr.impex under sample data.
################################################################################################################################

#######################
# used as import data #
#######################
$productCatalog = jamaicaProductCatalog
$catalogVersions = catalogVersions(catalog(id), version);

$solrIndexedType = jamaicaPriceType
$prefix = jamaica

$facetSearchCfg = jamaicaIndex
$searchCfg = jamaicaSearchConfig
$indexCfg = jamaicaIndexerConfig

$indexBaseSite = jamaica
$indexLanguages = en
$indexCurrencies = JMD

###################
# used in headers #
###################
$hIndexType = solrIndexedType(identifier)[unique = true, default = jamaicaPriceType]
$hIndexTypes = solrIndexedTypes(identifier)
$hId = identifier[unique = true]
$hName = name[unique = true]
$hCode = code[unique = true]
$hType = type(code)
$hJob = job(code)
$hSortable = sortableType(code)
$hDefaultSortOrder = defaultSortOrder;
$hMulti = multiValue[default = false]
$hProvider = fieldValueProvider
$hProviderParam = valueProviderParameter
$hProviderParams = valueProviderParameters[map-delimiter = |]
$hParams = solrIndexerQueryParameters(name)
$hPage = pageSize[unique = true]
$hDescription = description[unique = true]
$hLangFallback = enabledLanguageFallbackMechanism
$hCurrency = currency[default = false];
$hLocalized = localized[default = false];
$hSpellcheck = useForSpellchecking[default = false];
$hAutoComplete = useForAutocomplete[default = false];

$hSearchCfg = solrSearchConfig(description)
$hServerCfg = solrServerConfig(name)
$hIndexCfg = solrIndexConfig(name)
$hFacetSearchCfg = facetSearchConfig(name)

###################
### data import ###
###################


################################################################################################################################
#
# 1. DEFINE INDEXED TYPE
#
################################################################################################################################
# - type
# - properties
# - facet search config
################################################################################################################################


################################################################################################################################
# Indexed type
################################################################################################################################
INSERT_UPDATE SolrIndexedType; $hId; $hType; variant; defaultFieldValueProvider;sorts(&sortRefID)
; $solrIndexedType ; PriceRow ; false ; modelAttributesValueResolver; sortRef1,sortRef2,sortRef3,sortRef4,sortRef7,sortRef8

################################################################################################################################
# Non-facet properties
################################################################################################################################
$productType = getProduct() instanceof T(de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel)
$spoEl = $productType ? getProduct().getCode() : getAffectedProductOffering().getCode();
$userPriceGroup = ug == null ? '' : ug;
$currency = currency.isocode;
$bpoEl = $productType ? '' : getProduct().getCode();
INSERT_UPDATE SolrIndexedProperty; $hIndexType; $hName; $hType; $hSortable; $hMulti; $hProvider; $hProviderParam;
; ; itemtype       ; string ; ;      ;                          ;                 ;
; ; priceCode      ; string ; ;      ; tmaPriceCodeResolver     ;                 ;
# # code = spo code not price code - used across lots of populators :(
; ; code           ; string ; ;      ; springELValueProvider    ; $spoEl          ;
; ; spo            ; string ; ;      ; springELValueProvider    ; $spoEl          ;
; ; bpo            ; string ; ;      ; springELValueProvider    ; $bpoEl          ;
; ; terms          ; string ; ; true ; tmaPriceTermCodeResolver ;                 ;
; ; channel        ; string ; ; true ; tmaPriceChannelResolver  ;                 ;
; ; process        ; string ; ; true ; tmaPriceProcessResolver  ;                 ;
; ; currency       ; string ; ;      ; springELValueProvider    ; $currency       ;
; ; userpricegroup ; string ; ;      ; springELValueProvider    ; $userPriceGroup ;
################################################################################################################################
# Non-facet properties - used for searching - include some boosting factors.
################################################################################################################################
# The following are retrieved from the product.
################################################################################################################################
$prodNameEl = $productType ? getProduct().getName(#lang) : getAffectedProductOffering().getName(#lang)
$prodDescEl = $productType ? getProduct().getDescription(#lang) : getAffectedProductOffering().getDescription(#lang)
$prodSummaryEl = $productType ? getProduct().getSummary(#lang) : getAffectedProductOffering().getSummary(#lang)
$prodManuNameEl = $productType ? getProduct().getManufacturerName() : getAffectedProductOffering().getManufacturerName()
$prodManuAidEl = $productType ? getProduct().getManufacturerAID() : getAffectedProductOffering().getManufacturerAID()
$prodEanEl = $productType ? getProduct().getEan() : getAffectedProductOffering().getEan()
$prodIndividualEl = $productType ? getProduct().getSoldIndividually() : getAffectedProductOffering().getSoldIndividually()
INSERT_UPDATE SolrIndexedProperty; $hIndexType; $hName; $hType; $hProvider; $hProviderParam; $hSortable; $hCurrency; $hLocalized; $hMulti; $hSpellcheck; $hAutoComplete; ftsPhraseQuery[default = false]; ftsPhraseQueryBoost; ftsQuery[default = false]; ftsQueryBoost; ftsFuzzyQuery[default = false]; ftsFuzzyQueryBoost; ftsWildcardQuery[default = false]; ftsWildcardQueryType(code)[default = POSTFIX]; ftsWildcardQueryBoost; ftsWildcardQueryMinTermLength
; ; name                ; text    ; springELValueProvider    ; $prodNameEl       ; sortabletext ; ; true ; ; true ; true ; true ; 100 ; true ; 50  ; true ; 25 ;      ; ;    ;
; ; description         ; text    ; springELValueProvider    ; $prodDescEl       ; sortabletext ; ; true ; ; true ; true ; true ; 100 ; true ; 50  ; true ; 25 ;      ; ;    ;
; ; summary             ; text    ; springELValueProvider    ; $prodSummaryEl    ;              ; ; true ; ;      ;      ;      ;     ;      ;     ;      ;    ;      ; ;    ;
; ; manufacturerName    ; text    ; springELValueProvider    ; $prodManuNameEl   ;              ; ;      ; ; true ; true ; true ; 80  ; true ; 40  ; true ; 20 ;      ; ;    ;
; ; manufacturerAID     ; string  ; springELValueProvider    ; $prodManuAidEl    ;              ; ;      ; ;      ;      ;      ;     ;      ;     ;      ;    ;      ; ;    ;
; ; ean                 ; string  ; springELValueProvider    ; $prodEanEl        ;              ; ;      ; ; true ; true ;      ;     ; true ; 100 ;      ;    ; true ; ; 50 ; 3
; ; keywords            ; text    ; sppKeywordsResolver      ;                   ;              ; ; true ; ; true ; true ; true ; 40  ; true ; 20  ; true ; 10 ;      ; ;    ;
; ; soldIndividually    ; boolean ; springELValueProvider    ; $prodIndividualEl ;              ; ;      ; ;      ;

; ; img-515Wx515H       ; string  ; sppImage515ValueProvider ;                   ;              ; ;      ; ;      ;
; ; img-300Wx300H       ; string  ; sppImage300ValueProvider ;                   ;              ; ;      ; ;      ;
; ; img-96Wx96H         ; string  ; sppImage96ValueProvider  ;                   ;              ; ;      ; ;      ;
; ; img-65Wx65H         ; string  ; sppImage65ValueProvider  ;                   ;              ; ;      ; ;      ;
; ; img-30Wx30H         ; string  ; sppImage30ValueProvider  ;                   ;              ; ;      ; ;      ;

; ; reviewAvgRating     ; double  ; sppReviewAvgProvider     ;                   ;              ; ; true ; ;      ;
; ; numberOfReviews     ; int     ; sppReviewCntProvider     ;                   ;              ; ; true ; ;      ;

; ; inStockFlag         ; boolean ; sppStockFlagProvider     ;                   ;              ; ;      ; ;      ;
; ; stockLevelStatus    ; string  ; sppStockStatusProvider   ;                   ;              ; ;      ; ;      ;
; ; pickupAvailableFlag ; boolean ; sppPickupFlagProvider    ;                   ;              ; ;      ; ;      ;
; ; url                 ; string  ; sppUrlProvider           ;                   ;              ; ; true ; ;      ;


INSERT_UPDATE SolrIndexedProperty; $hIndexType; $hName; $hType; $hProvider; $hProviderParams; $hSortable; $hCurrency; $hLocalized; $hMulti; $hSpellcheck; $hAutoComplete; facet[default = false]; facetType(code); facetSort(code); priority; facetDisplayNameProvider; customFacetSortProvider; rangeSets(name)
; ; termLimits ; string ; tmaPriceTermNameResolver ; ; ; true ; true ; true ; ; ; ; MultiSelectOr ; Alpha ; 1000 ; ; ; ;

# todo - see if fts default values match this header
INSERT_UPDATE SolrIndexedProperty; $hIndexType; $hName; $hType; $hProvider; $hProviderParam; $hSortable; $hCurrency; $hLocalized; $hMulti; $hSpellcheck; $hAutoComplete; ftsPhraseQuery[default = false]; ftsPhraseQueryBoost; ftsQuery[default = false]; ftsQueryBoost; ftsFuzzyQuery[default = false]; ftsFuzzyQueryBoost; ftsWildcardQuery[default = false]; ftsWildcardQueryType(code)[default = POSTFIX]; ftsWildcardQueryBoost; ftsWildcardQueryMinTermLength
; ; billingTimes ; string ; tmaPriceFreqResolver ; ; ; ; true ; true


################################################################################################################################
# PRICE INFORMATION
################################################################################################################################
# Various Prices Information Indexed -> All values are taken from the current indexed price (be it a price or a price override).
#
# price      - the priceRange
# - iPhone_x : 500$ - 799$
#   -> if defined as PriceRow              : will use the priceRow.value
#   -> if defined as SubscriptionPricePlan : will use paynow one-time-charge value
# - salsaS   :  n/a
#   -> as it is used in facet comparison is weired to compare a one-time-charge with a recurring-fee
#
# priceRcFirst - the recurrent charge for the first cycle defined (some prices might involve a lower RC for the first months)
# - iPhone_x :  n/a
# - salsaS   :  10$
#
# priceRcLast - the recurrent charge for the last cycle defined
# - iPhone_x :  n/a
# - salsaS   :  20$
#
# priceOtc
# - iPhone_x : 789$
# - salsaS   :  n/a
#
# priceValue - not indexed anymore
################################################################################################################################
# Define price range set
INSERT_UPDATE SolrValueRange; &rangeValueRefID; solrValueRangeSet(name)[unique = true]; name[unique = true]; from; to
                            ; rangeRefJMD1  ; jamaicaPriceRangeJMD ; $0-$99.99       ; 0    ; 99.99
                            ; rangeRefJMD2  ; jamaicaPriceRangeJMD ; $100-$199.99    ; 100  ; 199.99
                            ; rangeRefJMD3  ; jamaicaPriceRangeJMD ; $200-$299.99    ; 200  ; 299.99
                            ; rangeRefJMD4  ; jamaicaPriceRangeJMD ; $300-$399.99    ; 300  ; 399.99
                            ; rangeRefJMD5  ; jamaicaPriceRangeJMD ; $400-$499.99    ; 400  ; 499.99
                            ; rangeRefJMD6  ; jamaicaPriceRangeJMD ; $500-$599.99    ; 500  ; 599.99
                            ; rangeRefJMD7  ; jamaicaPriceRangeJMD ; $600-$699.99    ; 600  ; 699.99
                            ; rangeRefJMD8  ; jamaicaPriceRangeJMD ; $700-$799.99    ; 700  ; 799.99
                            ; rangeRefJMD9  ; jamaicaPriceRangeJMD ; $800-$899.99    ; 800  ; 899.99
                            ; rangeRefJMD10 ; jamaicaPriceRangeJMD ; $900-$999.99    ; 900  ; 999.99
                            ; rangeRefJMD11 ; jamaicaPriceRangeJMD ; $1,000-$10,000 ; 1000 ; 10000
                            ; rangeRefJMD12 ; jamaicaPriceRangeJMD ; $10,000-$20,000 ; 10000 ; 20000
                            ; rangeRefJMD13 ; jamaicaPriceRangeJMD ; $20,000-$30,000 ; 20000 ; 30000
                            ; rangeRefJMD14 ; jamaicaPriceRangeJMD ; $30,000-$40,000 ; 30000 ; 40000
                            ; rangeRefJMD15 ; jamaicaPriceRangeJMD ; $40,000-$50,000 ; 40000 ; 50000
                            ; rangeRefJMD16 ; jamaicaPriceRangeJMD ; $50,000-$60,000 ; 50000 ; 60000
                            ; rangeRefJMD17 ; jamaicaPriceRangeJMD ; $60,000-$70,000 ; 60000 ; 70000
                            ; rangeRefJMD18 ; jamaicaPriceRangeJMD ; $70,000-$80,000 ; 70000 ; 80000
                            ; rangeRefJMD19 ; jamaicaPriceRangeJMD ; $80,000-$90,000 ; 80000 ; 90000
                            ; rangeRefJMD20 ; jamaicaPriceRangeJMD ; $90,000-$100,000 ; 90000 ; 100000

INSERT_UPDATE SolrValueRangeSet; name[unique = true]; qualifier; type; solrValueRanges(&rangeValueRefID)
; jamaicaPriceRangeJMD ; JMD ; double ;rangeRefJMD1,rangeRefJMD2,rangeRefJMD3,rangeRefJMD4,rangeRefJMD5,rangeRefJMD6,rangeRefJMD7,rangeRefJMD8,rangeRefJMD9,rangeRefJMD10,rangeRefJMD11,rangeRefJMD12,rangeRefJMD13,rangeRefJMD14,rangeRefJMD15,rangeRefJMD16,rangeRefJMD17,rangeRefJMD18,rangeRefJMD19,rangeRefJMD20


INSERT_UPDATE SolrIndexedProperty; $hIndexType; $hName; $hType; $hProvider; $hProviderParam; $hSortable; $hCurrency; $hLocalized; $hMulti; $hSpellcheck; $hAutoComplete; facet[default = false]; facetType(code); facetSort(code); priority; facetDisplayNameProvider; customFacetSortProvider; rangeSets(name)
; ; price        ; double ; sppRecurrentChargeResolver   ; first ; ; true ; ; ; ; ; true  ; MultiSelectOr ; Alpha ; 4000 ; ; ; jamaicaPriceRangeJMD
; ; priceOtc     ; double ; sppOneTimeChargeResolver   ; paynow ; ; true ; ; ; ; ; false ;
; ; priceRcFirst ; double ; sppRecurrentChargeResolver ; first  ; ; true ; ; ; ; ; false ;
; ; priceRcLast  ; double ; sppRecurrentChargeResolver ; last   ; ; true ; ; ; ; ; false ;
; ; priceValue   ; double ; tmaPoPriceValueProvider   ; ; ; true ; ; ; ; ; false ;               ;       ;      ; ; ; ; ; ;

# Category fields
INSERT_UPDATE SolrIndexedProperty; $hIndexType; $hName; $hType; $hProvider; $hProviderParam; $hSortable; $hCurrency; $hLocalized; $hMulti; $hSpellcheck; $hAutoComplete; categoryField[default = true]; ftsPhraseQuery[default = false]; ftsPhraseQueryBoost; ftsQuery[default = false]; ftsQueryBoost; ftsFuzzyQuery[default = false]; ftsFuzzyQueryBoost
; ; categoryName ; text ; sppCategoryNameProvider ; ; ; ; true ; true ; true  ; true  ; true ; true ; 40 ; true ; 20 ; true ; 10
; ; brandName    ; text ; sppBrandNameProvider    ; ; ; ; true ; true ; false ; false ; true ;      ;    ;      ;    ;      ;

# Category facets
INSERT_UPDATE SolrIndexedProperty; $hIndexType; $hName; $hType; $hProvider; $hProviderParam; $hLocalized; multiValue[default = true]; facet[default = true]; facetType(code); facetSort(code); priority; categoryField[default = true]; facetDisplayNameProvider; visible[default = false]
; ; allCategories         ; string ; sppAllCategoryProvider  ; ;      ;       ; ; Refine        ; Alpha ; -9999 ;       ;                                  ;
; ; categoryPath          ; string ; sppCategoryPathProvider ; ;      ;       ; ; Refine        ; Alpha ; -9999 ;       ;                                  ;
; ; category              ; string ; sppCategoryCodeProvider ; ;      ;       ; ; Refine        ; Alpha ; 6000  ;       ; categoryFacetDisplayNameProvider ;
; ; brand                 ; string ; sppBrandCodeProvider    ; ;      ;       ; ; Refine        ; Alpha ; 5000  ;       ; categoryFacetDisplayNameProvider ; true

; ; productOfferingGroups ; string ; sppPoGroupProvider      ; ;      ;       ; ; MultiSelectOr ; Alpha ; 6000  ; false ;                                  ;
; ; hasParentBpos         ; string ; sppBpoParentProvider    ; ;      ; false ; ; Refine        ; Alpha ; 7000  ; false ;                                  ;
; ; pscvDescription       ; string ; sppPscvDescProvider     ; ; true ; true  ; ; MultiSelectOr ; Alpha ; 7000  ; false ;                                  ;
; ; parentBundledPo       ; string ;sppParentBundlePoProvider; ;      ;       ; ; MultiSelectOr ; Alpha ; 6000  ; false ;  ;
; ; isBundled             ; boolean; sppIsBundledProvider    ; ; false; false ; ;               ;       ; 7000  ; false ;  ;
; ; productSpecification  ; string ; sppProductSpecProvider  ; ; false; false ; ;               ;       ; 7000  ; false ;  ;
; ; approvalStatus        ; string ;sppApprovalStatusProvider; ; false; false ; ;               ;       ; 7000  ; false ;  ;

INSERT_UPDATE SolrIndexedProperty; $hIndexType; $hName; $hType; $hProvider; $hProviderParam; $hSortable; $hCurrency; $hLocalized; $hMulti; facet[default = true]; facetType(code); facetSort(code); priority[default = 10000]; visible; useForSpellchecking[default = false]; useForAutocomplete[default = false]; facetDisplayNameProvider; customFacetSortProvider; topValuesProvider; rangeSets(name)
; ; availableInStores ; string ; sppStoresProvider ; ; ; ; ; true ; ; MultiSelectOr ; Custom ; ; false ; ; ; pointOfServiceFacetDisplayNameProvider ; distanceAttributeSortProvider

#; $solrIndexedType ; primaryPromotionCode             ; string  ;              ;      ;      ;      ;      ;      ; promotionCodeValueProvider              ;                 ;      ;     ;      ;     ;      ;    ;      ; ;    ;
#; $solrIndexedType ; primaryPromotionBanner           ; string  ;              ;      ;      ;      ;      ;      ; promotionImageValueProvider             ;                 ;      ;     ;      ;     ;      ;    ;      ; ;    ;



################################################################################################################################
#
# 2. SOLR FACET SEARCH CONFIG
#
################################################################################################################################
# - searchConfig  - telcoSearchConfig
# - indexConfig   - Default
# - serverConfig  - Default
################################################################################################################################
INSERT_UPDATE SolrSearchConfig; &Item; $hPage; $hDescription; $hDefaultSortOrder; legacyMode; restrictFieldsInResponse;
; $searchCfg ; 20 ; $searchCfg ; score,pk ; false ; true ;

INSERT_UPDATE SolrIndexerQuery; $hIndexType; $hId; $hType; query; user(uid); $hParams; injectCurrentDate[default = true]; injectCurrentTime[default = true]; injectLastIndexTime[default = true]
; $solrIndexedType ; $prefix-fullQuery   ; full   ;"select {pr:pk} from {PriceRow as pr} WHERE {pr.catalogVersion} IN({{(SELECT {cv.PK} FROM {catalogversion as cv} Where {cv.catalog} IN ({{SELECT {c.PK} FROM {catalog as c} where {c.id} ='jamaicaProductCatalog'}})}}) AND {pr.product} NOT IN ({{SELECT {po.pk} FROM {TmaOperationalProductOffering as po}}} ) AND {pr.product} NOT IN ({{SELECT {vp.pk} FROM {TmaPoVariant as vp}}}) AND {pr.product} IN ({{SELECT {sp.pk} FROM {TmaSimpleProductOffering as sp} where {sp.approvalStatus} = ({{select {at.pk} from {ArticleApprovalStatus as at} where {at.code} ='approved'}})}} )" ; anonymous ; ;; ; false ;
; $solrIndexedType ; $prefix-updateQuery ; update ; "

SELECT DISTINCT prtbl.pk FROM (
    {{
        SELECT {pr:pk} from {SubscriptionPricePlan as pr}
            WHERE
                 ({pr.affectedProductOffering} IS NOT NULL AND {pr.affectedProductOffering} IN (
                        {{
                        SELECT DISTINCT tbl.pk FROM (
                        {{
                            SELECT DISTINCT {p:PK} AS pk, {p:code} AS code
                            FROM {TmaProductOffering AS p LEFT JOIN CustomerReview AS cr ON {cr:product}={p:PK} }
                            WHERE {p:varianttype} IS NULL AND ({p:modifiedtime} >= ?lastIndexTime OR {cr:modifiedtime} >= ?lastIndexTime)
                            AND {p:catalog} IN ({{SELECT {c:PK} FROM {catalog as c} where {c:id} = '$productCatalog'}})
                        }}
                        UNION
                        {{
                            SELECT {p:PK}  AS pk, {p:code} AS code FROM {TmaProductOffering AS p} WHERE {p:code} IN (
                                {{
                                    SELECT DISTINCT {sl:productCode} FROM {StockLevel AS sl} WHERE {sl:modifiedtime} >= ?lastIndexTime
                                }}
                            )
                        }}
                        ) tbl
                        }}
                    )
                 )
    }}
    UNION
    {{
        SELECT {pr:pk} from {PriceRow as pr}
            WHERE
                ({pr.product} IN (
                        {{
                        SELECT DISTINCT tbl.pk FROM (
                        {{
                            SELECT DISTINCT {p:PK} AS pk, {p:code} AS code
                            FROM {TmaProductOffering AS p LEFT JOIN CustomerReview AS cr ON {cr:product}={p:PK} }
                            WHERE {p:varianttype} IS NULL AND ({p:modifiedtime} >= ?lastIndexTime OR {cr:modifiedtime} >= ?lastIndexTime)
                            AND {p:catalog} IN ({{SELECT {c:PK} FROM {catalog as c} where {c:id} = '$productCatalog'}})
                        }}
                        UNION
                        {{
                            SELECT {p:PK}  AS pk, {p:code} AS code FROM {TmaProductOffering AS p} WHERE {p:code} IN (
                                {{
                                    SELECT DISTINCT {sl:productCode} FROM {StockLevel AS sl} WHERE {sl:modifiedtime} >= ?lastIndexTime
                                }}
                            )
                        }}
                        ) tbl
                        }}
                    )
                     AND {pr.product} NOT IN ({{SELECT {po.pk} FROM {TmaOperationalProductOffering as po}}})
                 )
    }}
) prtbl

" ; anonymous ; ; ; ;   ;

INSERT_UPDATE SolrFacetSearchConfig; $hName; $hSearchCfg; $hIndexCfg; $hServerCfg; indexNamePrefix; $hIndexTypes; $catalogVersions; description; $hLangFallback; languages(isocode); currencies(isocode);
; $facetSearchCfg ; $searchCfg ; Default ; Default ; $prefix ; $solrIndexedType ; $productCatalog:Online ; Jamaica Solr index ; true ; $indexLanguages ; $indexCurrencies ;

UPDATE BaseSite; uid[unique = true]; solrFacetSearchConfiguration(name)
; $indexBaseSite ; $facetSearchCfg

################################################################################################################################
# 2.1 CronJob
################################################################################################################################
# - job      - solrIndexerJob
# - trigger  -
################################################################################################################################
INSERT_UPDATE SolrIndexerCronJob; $hCode; $hJob; singleExecutable; sessionLanguage(isocode); active; $hFacetSearchCfg; indexerOperation(code)
; full-jamaicaIndex-cronJob   ; solrIndexerJob ; false ; en ; true ; $facetSearchCfg ; full
; update-jamaicaIndex-cronJob ; solrIndexerJob ; false ; en ; true ; $facetSearchCfg ; update

################################################################################################################################
#
# 3. SEARCH QUERY
#
################################################################################################################################
# - template
# - sorts
# - diaply facets
################################################################################################################################
# Search query template
INSERT_UPDATE SolrSearchQueryTemplate; name[unique = true]; indexedType(identifier)[unique = true]; ftsQueryBuilder
; DEFAULT ; $solrIndexedType ; defaultFreeTextQueryBuilder

################################################################################################################################
# Define the available sorts
################################################################################################################################
INSERT_UPDATE SolrSort; indexedType(identifier)[unique = true]; &sortRefID; code[unique = true]; useBoost
; $solrIndexedType ; sortRef1 ; relevance  ; true
; $solrIndexedType ; sortRef2 ; topRated   ; false
; $solrIndexedType ; sortRef3 ; name-asc   ; false
; $solrIndexedType ; sortRef4 ; name-desc  ; false
#; $solrIndexedType ; sortRef5 ; payNowPrice-asc  ; false
#; $solrIndexedType ; sortRef6 ; payNowPrice-desc ; false
; $solrIndexedType ; sortRef7 ; recurringPrice-asc  ; false
; $solrIndexedType ; sortRef8 ; recurringPrice-desc ; false

# Define the sort fields
INSERT_UPDATE SolrSortField; sort(indexedType(identifier), code)[unique = true]; fieldName[unique = true]; ascending[unique = true]
; $solrIndexedType:relevance  ; inStockFlag     ; false
; $solrIndexedType:relevance  ; score           ; false
; $solrIndexedType:topRated   ; inStockFlag     ; false
; $solrIndexedType:topRated   ; reviewAvgRating ; false
; $solrIndexedType:name-asc   ; name            ; true
; $solrIndexedType:name-desc  ; name            ; false
#; $solrIndexedType:payNowPrice-asc  ; priceOtc      ; true
#; $solrIndexedType:payNowPrice-desc ; priceOtc      ; false
; $solrIndexedType:recurringPrice-asc  ; priceRcFirst      ; true
; $solrIndexedType:recurringPrice-desc ; priceRcFirst      ; false

# Category search query facets
INSERT_UPDATE SolrSearchQueryProperty; indexedProperty(name, solrIndexedType(identifier))[unique = true]; searchQueryTemplate(name, indexedType(identifier))[unique = true][default = DEFAULT:$solrIndexedType]; facet[default = true]; facetType(code); includeInResponse[default = true]; facetDisplayNameProvider; facetSortProvider; facetTopValuesProvider
                                     ; brand:$solrIndexedType         ; ; ; Refine ; ; categoryFacetDisplayNameProvider ; ;
