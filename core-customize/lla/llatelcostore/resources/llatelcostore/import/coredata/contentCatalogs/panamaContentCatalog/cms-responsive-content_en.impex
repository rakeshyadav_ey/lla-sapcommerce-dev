# ---------------------------------------------------------------------------
# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# ---------------------------------------------------------------------------

#
# Import CMS content for the Responsive B2C Telco site
#

$contentCatalog=panamaContentCatalog
$contentCV=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalog]),CatalogVersion.version[default=Staged])[default=$contentCatalog:Staged]

# Language
$lang=en

# Functional Content Pages
INSERT_UPDATE ContentPage;$contentCV[unique=true];uid[unique=true];name;masterTemplate(uid,$contentCV);label;defaultPage[default='true'];approvalStatus(code)[default='approved'];homepage[default='false']
 ;;guidedselling-select-device;Select Device Page;BundleSelectionAddOnPageTemplate;guidedselling-select-device

# Content Pages
UPDATE ContentPage;$contentCV[unique=true];uid[unique=true];title[lang=$lang]
 ;;add-edit-address;"Add/Edit Address"
 ;;address-book;"Address Book"
 ;;cartPage;"Your Shopping Cart"
 ;;checkout-login;"Proceed to Checkout"
 ;;faq;"Frequently Asked Questions"
 ;;homepage;"Homepage"
 ;;login;"Login"
 ;;multiStepCheckoutSummaryPage;"Checkout"
 ;;notFound;"Not Found"
 ;;order;"Order Details"
 ;;orderConfirmationPage;"Order Confirmation"
 ;;orders;"Order History"
 ;;payment-details;"Payment Details"
 ;;payment-details-manage-subscriptions;"Payment Details Manage Subscriptions"
 ;;profile;"Profile"
 ;;searchEmpty;"No Results"
 ;;storefinderPage;"StoreFinder"
 ;;termsAndConditions;"Terms and Conditions"
 ;;update-email;"Update Email"
 ;;update-profile;"Update Profile"
 ;;updatePassword;"Update Forgotten Password"
 ;;bundleselection-plan;"Service Plan Selection"
 ;;bundleselection-extra;"Service Add Ons Selection"
 ;;guidedselling-select-device;"Product Selection"
 ;;privacyPolicy;"Privacy Policy"
 ;;termsOfService;"Terms of Service"
 ;;internetPlans;"Internet Plans"
 ;;triple_play;"Triple Play"
 ;;complete_package;"Complete Package"
 ;;bundle; "Bundle"

# Category Pages
UPDATE CategoryPage;$contentCV[unique=true];uid[unique=true];title[lang=$lang]
;;SBGProductsCategoryPage;"TV Channels"
UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=$lang];
                             ; 						;HomePage_ProductsTypeParagraphComponent;All the experiences you need

UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]              ; content[lang = $lang];
                            ;                          ; PostSales_Header                ; Need Help?
                            ;                          ; FeaturePromoParagraph_component ; Featured Promotions
UPDATE CMSImageTextComponent;$contentCV[unique=true];uid[unique=true];text[lang=$lang] ;description[lang=en]; url[lang = $lang]
                            ;                       ;PostSales1      ;Live Chat;;https://www.cwpanama.com/chatonline;
                            ;                       ;PostSales2      ;Mi Mas App;;https://www.cwpanama.com/mimasapp;
                            ;                       ;PostSales3      ;Top Up and Pay;;https://pagos.cwpanama.com/?_ga=2.137210288.102677563.1600930490-120017937.1600930489;
                            ;                       ;PostSales4      ;Online Store;;/panamastorefront
                             
UPDATE FooterComponent;$contentCV[unique=true];uid;footerTopHeadlineText[lang=$lang]
					  ;	                      ;FooterComponent;"¿Need Help?"
					  

# CMS Link Components
UPDATE CMSLinkComponent;$contentCV[unique=true];uid[unique=true];linkName[lang=$lang]
;;MobileCategoryLink;"Mobile"
####
;;OnlineShopLink;"Online Store"
;;OnlineShopProductsLink;"Products"
;;PostpaidLink;"Postpaid"
;;PrepaidLink;"Prepaid"
;;ResidentialLink;"Residential"
;;PlansLink;"Plans"
;;Plans2Link;"Plans"
;;DevicesLink;"Devices"
;;Bundle3PLink;"Internet TV & Telephony"
;;Bundle4PLink; "Fixed + Mobile"
####
;;BundleCategoryLink;"Bundles"
;;MobilePlansCategoryLink;"Mobile plans"
;;PrepaidCategoryLink;"Prepaid"
;;PostpaidCategoryLink;"Postpaid"
;;RoamingCategoryLink;"Roaming"
;;DevicesCategoryLink;"Devices"
;;PhonesCategoryLink;"Phones"
;;AboutCategoryLink;"About"
;;4GLTECategoryLink;"4GLTE"
;;FlowAppsCategoryLink;"Flow Apps"
;;QuestionsCategoryLink;"Questions"
;;InternetCategoryLink;"Broadband"
;;TVCategoryLink;"TV"
;;HomePhoneCategoryLink;"Home Phone"
;;SupportCategoryLink;"Support"
;;InternetTVPhoneCategoryLink;DO IT Bundles
#;;FlameBundleCategoryLink;Do It Now
#;;FireBundleCategoryLink;Do It More
;;BlazeBundleCategoryLink;Do It All
;;HboGoBundleCategoryLink;Do It Now
;;TorchBundleCategory;Do It More
;;InternetPhoneCategoryLink;Internet+ Phone
;;MegaPlusCategoryLink;Mega Plus Plan
;;MegaExtremeCategoryLink;Mega Extreme Plan
;;BrowseTalkCategoryLink;Browse & Talk Plus
;;BroadbandInternetCategoryLink;Broadband Internet
;;EssentialCategoryLink;Essential
;;PlusCategoryLink;Plus
;;MaxCategoryLink;Max
#;;TurboCategoryLink;Turbo
;;InternetDevicesCategoryLink;Devices
;;InternetRouterCategoryLink;Router
;;InternetSpeedCategoryLink;Speed Test
;;InternetAboutCategoryLink;About
;;InternetRateAdjustmentLink;Rate Adjustment Notice
;;TVPlansCategoryLink;Plans
;;TVPlusCategoryLink;TV Plus
;;TVMaxCategoryLink;TV Max
;;TVWatchAllCategoryLink;TV Watch All
;;TVAddonCategoryLink;Addons
;;TVPremiumChannelCategoryLink;Premium Channels
;;TVSportPackCategoryLink;Sport Packs
;;TVFlowExpCategoryLink;Flow Experience
;;TVFlowtoGoCategoryLink;Flow to Go
;;TVFlowOnDemandCategoryLink;Flow On Demand
#;;TVHboGoCategoryLink;HBO Go
;;TVAboutCategoryLink;About
;;TVQuestionsCategoryLink;Questions
;;HomePhonePlanCategoryLink;Plans
;;HomePhoneBasicCategoryLink;Basic
;;HomePhoneEssentialCategoryLink;Essential
;;HomePhonePlusCategoryLink;Plus
;;HomePhoneCardServicesCategoryLink;Card Services
;;HomePhoneInternationalCategoryLink;International Plans
;;HomePhoneWorldPakCategoryLink;WorldPak
;;HomePhoneWorldPakLiteCategoryLink;WorldPak Lite
;;HomePhoneInternationalRatesCategoryLink;International rates
;;HomePhoneAboutCategoryLink;About
;;HomePhoneFAQCategoryLink;HomePhone FAQ
;;HomePhoneRateAdjustmentCategoryLink;Rate Adjustment Notice
;;HomePhoneExpressNotificationCategoryLink;Home Phone Express Notification
;;Footer_TVPlusCategoryLink;TV Plus
;;Footer_TVMaxCategoryLink;TV Max
;;Footer_TVWatchAllCategoryLink;TV Watch All
;;Footer_TVPremiumChannelCategoryLink;Premium Channels
;;Footer_TVSportPackCategoryLink;Sport Packs
#;;Footer_TVFlowExpCategoryLink;Flow Experience
;;Footer_TVFlowtoGoCategoryLink;Flow to Go
;;Footer_TVFlowOnDemandCategoryLink;Flow On Demand
#;;Footer_TVHboGoCategoryLink;HBO Go
;;Footer_TVAboutCategoryLink;About
#;;Footer_PrepaidCategoryLink;Prepaid
;;Footer_PostpaidCategoryLink;Postpaid
;;Footer_RoamingCategoryLink;Roaming
;;Footer_DevicesCategoryLink;Device Catalogue
;;Footer_AboutCategoryLink;About
;;Footer_InternetRouterCategoryLink;Routers
;;Footer_InternetAboutCategoryLink;About
;;Footer_HomePhoneCardServicesCategoryLink;Card Services
;;Footer_HomePhoneAboutCategoryLink;Overview
;;SupportBillingCategoryLink;Billing
;;SupportMobileAppsCategoryLink;Mobile Apps
;;SupportSwitchtoFlowCategoryLink;Switch to Flow
;;SupportImportantNotificationCategoryLink;Important notifications
;;SupportMobileCategoryLink;Mobile
;;SupportBundleCategoryLink;Bundle
;;SupportHomePhoneCategoryLink;Home Phones
;;SupportTVCategoryLink;TV
;;SupportServicesCategoryLink;Services
;;SupportOtherCategoryLink;Others
;;HomeLink;Home
;;Footer_TVOverviewCategoryLink;Overview
;;Footer_TVChannelLineupCategoryLink;Channel Lineup
;;Footer_TVAddoOnsCategoryLink; Add ons
;;Footer_FlowHomeCategoryLink;Home
;;Footer_FlowDiffCategoryLink; The Flow Difference
;;Footer_MobileTopUpLink;Mobile Top Up
;;Footer_OurAppsLink;Our Apps
;;Footer_RechargePartnerLink;Recharge Partners
;;Footer_InternetOverviewLink;Overview
;;Footer_InternetBrowseAndTalkLink;Browse & Talk
;;Footer_InternetEquipmentLink;Equipment
;;Footer_InternetSpeedTestLink;Speed Test
;;Footer_HomePhoneInternationalRatesCategoryLink;International Rates
;;Footer_HomePhoneCardServiceCategoryLink;Card Services
;;Footer_InternetTVPhoneCategoryLink;Bundles

;;MobilePrepaidInformationLink;"+Information"
;;MobilePrepaidLink; Prepaid
;;MobilePrepaidCheckYourBalanceLink;Check Your Balance
;;MobilePrepaidPromotionsLink;Deals
;;MobilePrepaidRechargeNowLink;Top Up and Pay 
;;MobilePrepaidBuyNewLineLink;Buy a new line
;;MobilePostpaidLink;Postpaid
;;MobilePostpaidInformationLink;Postpaid Information
;;MobilePostpaidUnlimitedPlanLink;"Unlimited Plan"
;;MobilePostpaidCompletePackageLink;Bundles
;;MobilePostpaidPromotionsLink;Deals
;;MobilePostpaidPayOnlineLink;Pay Online
;;MobilePostpaidBuyNowLink;"¡Buy Now!"
;;MobileServicesLink;Services
;;MobileServicesLTECoverageLink;LTE Coverage
;;MobileServicesRoamingLink;Roaming
;;MobileServicesVoiceOverWifiLink;Voice over WIFI
;;MobileServicesMobileAssistanceLink;Mobile Assistance
;;MobileServicesMobileTipsLink;Mobile Tips
;;MobileServicesOtherServicesLink;Other Services
;;MobileCellPhonesLink;Cell Phones
;;MobileCellPhonesBuyNowLink;"¡Buy Now!"
;;MobileCellPhonesFinancingLink;Financed Phone
;;MobileCellPhonesMobileInsuranceLink;Mobile Insurance
;;MobileCellPhonesDigitalCatalogueLink;Digital Catalogue
;;ResidencialLink;Residential
;;ResidentialAlwaysMoreLink;"¡Always More!"
;;ResidentialChooseYourPlanLink;Choose Your Plan
;;ResidentialInternetLink;Internet
;;ResidentialEntertainmentLink;Entertainment
;;ResidentialAlwaysMoreInformationLink;"+Information"
;;ResidentialAlwaysMoreCoverageLink;Coverage
;;ResidentialAlwaysMoreChannelsLink;Channels
;;ResidentialAlwaysMorePromotionsLink;Deals
;;ResidentialAssistedInstalationLink;Assisted Installation
;;ResidentialChooseYourPlanInformationLink;Bundles
;;ResidentialChooseYourPlan1000MBTVHDLink;1000 MB+TV HD*+ 200Mins.
;;ResidentialChooseYourPlan600MVTVHDLink;600 MB+TV HD*+ 200Mins.
;;ResidentialChooseYourPlan400MBTVHDLink;400 MB+TV HD*+ 200Mins.
;;ResidentialChooseYourPlan200MBTVHDLink;200 MB+TV HD*+ 200Mins.
;;ResidentialChooseYourPlanCompletePackageLink;Set Up your plan
;;ResidentialInternetCalculateCosumptionLink;Estimate your usage
;;ResidentialInternetADSLTelephonyLink;Internet, ADSL and Telephony
;;ResidentialEntertainmentTVTotalLink;"+TV Total"
;;ResidentialEntertainmentTVGOAppLink;"+TV GO App"
;;ResidentialEntertainmentVideoOnDemandLink;Video on Demand
;;ResidentialEntertainmentPlayLink;Plataformat GO/Play
;;ResidentialInternetRechargePrepaidTVLink;Top Up your TV prepaid
;;CompletePackageCetegoryLink;"Bundles"
;;CompletePackageIamPlusMobileLink; I am +Mobile
;;CompletePackageIamPlusMobileInformationLink; Information
;;CompletePackageWantToBePlusMobileLink; I want to be +Mobile
;;CPWantToBePlusMobileMeetBuyLink;Learn more & Buy
;;HelpCentreCetegoryLink;Help Center
;;HelpCentreMyAccountLink;My Account
;;HelpCentreAccessMyAccountLink;Access to my Account
;;HelpCentreMyMoreAppLink;"Mi Más App"
;;HelpCentreMobileBillingLink;Mobile Billing
;;HelpCentreResidentialBillingLink;Residential Billing
;;HelpCentreCommercialBillingLink;Commercial Billing
;;HelpCentreOnlinePaymentsLink;Online Payments
;;HelpCentreRechargeNowLink;Online Top Up
;;HelpCentreMobileServicePaymentLink;Mobile service Payment
;;HelpCentreResidentialServicePaymentLink;Residential service Payment
;;HelpCentreRecurringPaymentsLink;Affiliation to recurring payments
;;HelpCentreMyCWPEmailLink;My CWP Email
;;HelpCentreAccessMyEmailLink;Access my Email
;;HelpCentreChangePasswordLink;Change Password
;;HelpCentreNeedHelpLink;"¿Need Help?"
;;HelpCentreCustomerServiceLink;Customer Service
;;HelpCentrePlanSolidarioLink;Plan Solidario
;;HelpCentreAttentionViaLivechatLink;LIVECHAT Assistance
;;HelpCentreTelephoneNumbersLink;Cellphone numbers
;;HelpCentreStoresAndTimingsLink;Schedule and Stores
;;HelpCentrePaymentCentresLink;Payment Centres
;;HelpCentreServiceWorkshopsLink;Service Workshops
;;HelpCentreHowActivateParentalControlLink;"¿How to activate parental control?"
;;HelpCentreSpeedTestLink;Speed Test
;;HelpCentreTutorialVideosLink;Tutorials
;;HelpCentreMobileDataTransferLink;"Mobile Data Transfer"
;;HelpCentreAboutUsLCategoryLink;About Us
;;HelpCentreAboutUsLink;How We Are
;;HelpCentreMissionAndValuesLink;Mission and values
;;HelpCentreOperatingResultsLink;Operating Results
;;HelpCentrePressRoomLink;Press room
;;HelpCentreJoinOurTeamLink;Join Us
;;HelpCentreOnlineAttentionLink;Online Assistance
;;HelpCentreAboutUsStoresAndTimingsLink;"Stores and Hours"
;;HelpCentreAboutUsPaymentCentresLink;Payment Centres
;;HelpCentreContactUsLink;Contact Us
;;HelpCentreAboutUsTelephoneNumbersLink;Telephone Numbers

;;Footer2HomeLink;Home
;;Footer2OnlinestoreLink;Online store
;;Footer2ServicesLink;Services
;;Footer2ServicesMobileLink;Mobile
;;Footer2ServicesInternetLink;Internet
;;Footer2ServicesTVHDLink;TV HD
;;Footer2ServicesTelephoneLink;Telephone
;;Footer2ServicesBusinessLink;Business
;;Footer2AboutLink;About Us
;;Footer2AboutAboutUsLink;"Who we are?"
;;Footer2AboutMissionAndValuesLink;"Our Mision & Values"
;;Footer2AboutOperatingResultsLink;Operating Results
;;Footer2AboutPressRoomLink;New
;;Footer2AboutJoinOurTeamLink;Join Us
;;Footer2UsefulInformationLink;Useful Information
;;Footer2UsefulInformationOnlineAttentionLink;Online Assistance
;;Footer2UsefulInformationStoresAndTimingsLink;Schedule & Stores
;;Footer2UsefulInformationPaymentCentresLink;Pay Centers
;;Footer2UsefulInformationTelephoneNumbersLink;Phone Numbers
;;Footer2FollowUsLink;Follow Us
;;Footer2FollowUsFacebookLink;Facebook
;;Footer2FollowUsTwitterLink;Twitter
;;Footer2FollowUsInstagramLink;Instagram
;; PrivacyPolicyLink; "Privacy Policies"
;;TermsOfUseLink;"Terms of Use"
;;TermsConditionLink; "Terms and Conditions"
;;GoToTopLink;"Go To Top"
;;GotoMenuLink	; "Go to Menu"
;;Bundle4P300Link; "Complete Package 400"
;;Bundle4P600Link; "Complete Package 600"
UPDATE LLABundleProductCMSComponent; $contentCV[unique = true]  ; uid[unique = true]                                ; headlineText[lang = $lang]                                                                                                                                                                                                    ; name               ; subheading[lang = $lang]
                                   ;                            ; bundleproduct_postpaid                            ; "Our Plans"                                                                                                                                                                                                                   ; Postpaid           ; "Activate your smartphone on a <span class=""special-text"">+Móvil</span> plan, or buy a plan with a new smartphone."
                                   ;                            ; bundleproduct_prepaid                             ; "Our Prepaid Plans"                                                                                                                                                                                                           ; Prepaid            ; "Activate your pre paid smartphone on a <span class=""special-text"" style="""">+Móvil</span> plan, or buy a plan with a new smartphone."
                                   ;                            ; bundleproduct_internetTvPhone                     ;                                                                                                                                                                                                                               ; Panama3Pbundles    ;
                                   ;                            ; bundleproduct_internetPhone                       ;                                                                                                                                                                                                                               ; Panama4Pbundles    ;
                                   ;                            ; product_device_plan_plp                           ; "Buying a plan, you can also add your Smartphone."                                                                                                                                                                            ; DevicePlan         ; "To buy any device, you must to choose one of our plans of <a class=""textPrimaryColor"" href="""">B/.35</a> or <a class=""textPrimaryColor"" href="""">B/.40</a>"
                                   ;                            ; product_device_fmc_pdp                            ; "Buying <span class=""special-text"">BUNDLES</span> <span class=""subhead-black"">you can add</span><br/> your favorite <span class=""subhead-black"">Smartphone</span> in a <span class=""subhead-black"">low price!</span>" ; DeviceFMC          ; "If you want to add any Smartphone, first you may to choose a Bundles complete package of <a href='../$lang/p/FMC_400' class=""textPrimaryColor"">B/.89</a> or <a href='../$lang/p/FMC_600' class=""textPrimaryColor"">B/.95</a>"
                                   ;                            ; bundleproduct_homepage                            ; "enjoy the best plans here with <span class=""special-text"" style="""">+</span>Móvil"

UPDATE LLASimpleResponsiveBannerComponent;$contentCV[unique=true];uid[unique=true];bannerContent[lang=$lang]
                                         ;;TelcoHompageResponsiveBannerComponent1; "<div class=""home-banner-cta"">            <a href='/panamastorefront/panama/$lang/p/FMC_400' class=""banner-btn-first"">Complete Package 400</a>            <a href='/panamastorefront/panama/$lang/p/FMC_600' class=""banner-btn-second"">Complete Package 600</a>       </div>"
                                         ;;TelcoHompageEnjoyPlanResponsiveBannerComponent1;"<div class=""promotion-cta""><p class=""heading""><span>Postpaid plans</span> <span>connect to your ones</span><span>without leaving home!</span></p><p class=""description"">You can choose the kind of plan do you need, and you have the freedom to change it, whenever you want.</p><a href='/panamastorefront/panama/$lang/plp/postpaid' class=""promotion-button"">Information</a></div>"

UPDATE LLACMSImageButtonTextComponent; $contentCV[unique = true]; uid[unique = true]    ; imageHeadLineText[lang = $lang]             ; imageSubHeadlineText[lang = $lang]                                                                              ; buttonCTAText[lang = $lang]
                                     ;                          ; HP_MobileBanner       ; Mobile                                      ; With LTE, you can enjoy everything you love faster under the best coverage of Panama.                           ; +Information
                                     ;                          ; HP_BundleBanner       ; Bundles                                     ; Better experiences in your home with best services                                                              ; See More
                                     ;                          ; HP_InternetBanner     ; Internet                                    ; With service WiFi Total, one of our technicians will ensure that you have a strong signal throughout your home. ; +Information
                                     ;                          ; HP_TVBanner           ; TV                                          ; The best of entertainment whenever and wherever you want.                                                       ; +Information
                                     ;                          ; HP_HomePhoneBanner    ; Home Phone                                  ; Not only will you have clear communication, but also local and national long distance calling plans.            ; +Information
                                     ;                          ; PostPaidPromo1        ; Postpaid plans with unlimited data          ; You will find a variety of plans adjusted to your budget.                                                       ; +Information
                                     ;                          ; PostPaidPromo2        ; Stay connected with our prepaid plans.      ; Social networks are downloaded without interruptions so that your moments continue without interruptions.       ; +Information
                                     ;                          ; PostPaidPromo3        ; The signal of Panama, Now faster than ever! ; With LTE, you can enjoy everything you love faster under the best coverage of Panama.                           ; +Information
                                     ;                          ; Bundle3pPromo         ; The best entertainment where ever you want. ; With a single click of your remote control, you can enjoy the best in VOD entertainment.                        ; +Information

UPDATE LLASimpleResponsiveBannerComponent; $contentCV[unique = true]; uid[unique = true]                 ; bannerText[$lang]
                                         ;                          ; PostpaidBanner                     ; PostPaid Plans
                                         ;                          ; PrepaidBanner                      ; Prepaid Plans
                                         ;                          ; InternetTVPhoneResponsiveBanner    ; RESIDENTIALS PACKS
                                         ;                          ; InternetPhoneResponsiveBanner      ; RESIDENTIALS PACKS
                                         ;                          ; MobileBanner                       ; Mobile


UPDATE CMSNavigationNode ; uid[unique = true]              ; title[lang = $lang]            ; $contentCV[unique = true]   

                                ; Footer2HomeNavNode           	  ; Start          ;                                                                                                                                                                                                                                                                                
                                ; Footer2OnlinestoreNavNode       ; Online Store       ;                                                                                                                                                                                                                                                                     
                                ; Footer2ServicesNavNode          ; Services            ;                                                                                      
                                ; Footer2AboutUsNavNode           ; About Us        ;                                                                                                                         
                                ; Footer2UsefulInformationNavNode ; Useful Information       ;                                                       
                                ; Footer2FollowUsNavNode          ; Follow Us   ;
                                
UPDATE CMSLinkComponent;$contentCV[unique=true];uid[unique=true];linkName[lang=$lang]
;;Footer2HomeLink;"Home"
;;Footer2OnlinestoreLink;"Online Store"
;;Footer2ServicesLink;"Services"
;;Footer2AboutLink;"About Us"
;;Footer2UsefulInformationLink;"Quick Information"
;;Footer2FollowUsLink;"Follow Us:"


INSERT_UPDATE ContentPage ; $contentCV[unique = true] ; uid[unique = true]      ; name
                          ;                           ; prepaid                 ; MOBILE PREPAID PLANS
                          ;                           ; postpaid                ; MOBILE POSTPAID PLANS
                          ;                           ; triple_play             ; Bundle Internet TV Phone
                          ;                           ; complete_package        ; Bundle Internet Phone
                          ;                           ; bundle                  ; Bundle PLP



