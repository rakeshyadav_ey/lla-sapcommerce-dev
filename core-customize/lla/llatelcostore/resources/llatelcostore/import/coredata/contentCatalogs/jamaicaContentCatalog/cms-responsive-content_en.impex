# ---------------------------------------------------------------------------
# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# ---------------------------------------------------------------------------

#
# Import CMS content for the Responsive B2C Telco site
#

$contentCatalog=jamaicaContentCatalog
$contentCV=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalog]),CatalogVersion.version[default=Staged])[default=$contentCatalog:Staged]

# Language
$lang=en

# Functional Content Pages
INSERT_UPDATE ContentPage;$contentCV[unique=true];uid[unique=true];name;masterTemplate(uid,$contentCV);label;defaultPage[default='true'];approvalStatus(code)[default='approved'];homepage[default='false']
 ;;guidedselling-select-device;Select Device Page;BundleSelectionAddOnPageTemplate;guidedselling-select-device

# Content Pages


UPDATE ContentPage;$contentCV[unique=true];uid[unique=true];title[lang=$lang]
 ;;add-edit-address;"Add/Edit Address"
 ;;address-book;"Address Book"
 ;;cartPage;"Your Shopping Cart"
 ;;checkout-login;"Proceed to Checkout"
 ;;faq;"Frequently Asked Questions"
 ;;homepage;"Homepage"
 ;;login;"Login"
 ;;multiStepCheckoutSummaryPage;"Checkout"
 ;;notFound;"Not Found"
 ;;order;"Order Details"
 ;;orderConfirmationPage;"Order Confirmation"
 ;;orders;"Order History"
 ;;payment-details;"Payment Details"
 ;;payment-details-manage-subscriptions;"Payment Details Manage Subscriptions"
 ;;profile;"Profile"
 ;;searchEmpty;"No Results"
 ;;storefinderPage;"StoreFinder"
 ;;termsAndConditions;"Terms and Conditions"
 ;;update-email;"Update Email"
 ;;update-profile;"Update Profile"
 ;;updatePassword;"Update Forgotten Password"
 ;;bundleselection-plan;"Service Plan Selection"
 ;;bundleselection-extra;"Service Add Ons Selection"
 ;;guidedselling-select-device;"Product Selection"
 ;;privacyPolicy;"Privacy Policy"
 ;;termsOfService;"Terms of Service"
 ;;internetPlans;"Internet Plans"
 ;;internet_tv_phone;"Internet Tv Phone"
 ;;internet_phone;"Internet Phone"
 ;;bundle; "Bundles"

# Category Pages
UPDATE CategoryPage;$contentCV[unique=true];uid[unique=true];title[lang=$lang]
;;SBGProductsCategoryPage;"TV Channels"
UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=$lang];
                             ; 						;HomePage_ProductsTypeParagraphComponent;All the experiences you need
							 ;						;bundle_PromoMessageParagraphComponent;*Special 3-month introductory price available for new sign-ups until June 30th, 2021. Standard billing will start ninety (90) days after service installation.

UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=$lang];
                             ; 						;PostSales_Header;STILL GOT ANY DOUBT? HOW CAN WE HELP YOU?
UPDATE CMSImageTextComponent;$contentCV[unique=true];uid[unique=true];text[lang=$lang] ;description[lang=en]
                            ;                       ;PostSales1      ;Pay Bill;
			    ;                       ;PostSales2      ;Top Up;
                            ;                       ;PostSales3      ;Support;
                            ;                       ;PostSales4      ;Find Store;
                            ;                       ;PostSales5      ;FAQ;
                             
UPDATE FooterComponent;$contentCV[unique=true];uid;footerTopHeadlineText[lang=$lang]
					  ;	                      ;FooterComponent;"How can we help you today?"
					  

# CMS Link Components
INSERT_UPDATE CMSLinkComponent;$contentCV[unique=true];uid[unique=true];linkName[lang=$lang]
;;MobileCategoryLink;"Mobile"
;;BundleCategoryLink;"Bundles"
;;MobileAboutMobileCategoryLink;"About Mobile"
;;MobilePlansCategoryOverviewLink;"Overview"
;;MobilePlansCategoryTopLink;"Top Up"
;;MobilePlansCategoryPromotionsLink;"Promotions"
;;MobilePlansCategoryLink;"plans"
;;MobilePrepaidCategoryLink;"Prepaid Plans"
;;MobilePostpaidCategoryLink;"Postpaid Plans"
;;MobileDevicesCategoryLink;"Devices"
;;MobilePhonesCategoryLink;"Phones"
;;MobileAddOnCategoryLink;"Add-ons"
;;MobileFlowAppsCategoryLink;"Apps"
;;MobileRoamingCategoryLink;"Roaming"
;;MobileFlowLendCategoryLink;"Flow Lend"
;;MobileAboutCategoryLink;"About"
;;Mobile4GLTECategoryLink;"4GLTE"
;;MobileQuestionsCategoryLink;"Questions?"
;;MobileFaqCategoryLink;"Mobile FAQ"
;;MobilePreRateAdjCategoryLink;"Prepaid Rate Adjustment"
;;MobilePosRateAdjCategoryLink;"Postpaid Rate Adjustment"
;;MobileRecPartnersCategoryLink;"Recharge Partners"
;;MobileSwitchFlowCategoryLink;"Switch to FLOW"
;;InternetCategoryLink;"Broadband"
;;InternetBroadCategoryLink;"About Broadband"
;;InternetOverCategoryLink;Overview
;;InternetPlansCategoryLink;Plans
;;InternetBrowseCategoryLink;Browse & Talk
;;InternetBundlesCategoryLink;Bundles
;;InternetToolsCategoryLink;Tools & Devices
;;InternetRoutersCategoryLink;Routers
;;InternetSpeedCategoryLink;Speed test
;;InternetQuesCategoryLink;Questions?
;;InternetRateAdjNotCategoryLink;Rate adjustment Notice
;;TVCategoryLink;"TV"
;;TVAboutFlowCategoryLink;"About Flow Evo"
;;TVOverviewCategoryLink;"Overview"
;;TVChannelsCategoryLink;"Channels"
;;TVLineupCategoryLink;Lineup
;;TVAddonsCategoryLink;"Add ons"
;;TVSportsPackCategoryLink;"Sports Pack"
;;TVWaysToWatchCategoryLink;Ways to Watch
;;TVFlowToGoCategoryLink;Flow ToGo
;;TVFlowonDemandCategoryLink;Flow On Demand
;;TVHboFeaturesCategoryLink;HBO GO® Features
;;TVCurFeaturesCategoryLink;CuriosityStream Features
;;TVQuestionsCategoryLink;"Questions?"
;;TVFaqCategoryLink;TV FAQ
;;TVRateAdjNoticeCategoryLink;Rate adjustment Notice
;;HomePhoneCategoryLink;"Home Phone"
;;HomeAboutCategoryLink;"About Home Phone"
;;HomePhoneOverviewCategoryLink;"Overview"
;;HomePhonePlansCategoryLink;"Plans"
;;HomePhoneCardSerCategoryLink;"Card Services"
;;HomePhoneBundlesCategoryLink;"Bundles"
;;HomePhoneRatesCategoryLink;"Rates"
;;HomePhoneInterCategoryLink;"International"
;;HomePhoneQuestionsCategoryLink;"Questions?"
;;HomePhoneFaqCategoryLink;"Home Phone FAQ"
;;HomePhoneRateAdjCategoryLink;"Rate Adjustment Notice"
;;HomePhoneExpNotCategoryLink;"Home Phone Express Notification"


;;BundlePlansCategoryLink;"Plans"
;;BundleDoitBundlesCategoryLink;"DO IT Bundles"
;;BundleDoitBundlesMobileCategoryLink;"DO IT Bundles + Mobile"

;;SupportCategoryLink;"Support"
;;SupportAboutCategoryLink;"About Support"
;;SupportOverCategoryLink;"Overview"
;;SupportFaqCategoryLink;"FAQ"
;;SupportHomeEmptyCategoryLink;"&nbsp;"
;;SupportMobileCategoryLink;"Mobile"
;;SupportBundlesCategoryLink;"Bundles"
;;SupportTvCategoryLink;"TV"
;;SupportBroadbrandCategoryLink;"Broadband"
;;SupportHomePhnCategoryLink;"Home Phone"
;;SupportBillingCategoryLink;"Billing"
;;SupportMobileappsCategoryLink;"Mobile Apps"
;;SupportSwtFlowCategoryLink;"Switch to Flow"
;;SupportImpNotCategoryLink;"Important notifications"
;;SupporFlowLendtCategoryLink;"Flow Lend"

;;InternetTVPhoneCategoryLink;DO IT Bundles
#;;FlameBundleCategoryLink;Do It Now
#;;FireBundleCategoryLink;Do It More
;;BlazeBundleCategoryLink;Do It All
;;HboGoBundleCategoryLink;Do It Now
;;TorchBundleCategory;Do It More
;;InternetPhoneCategoryLink;Internet+ Phone
;;MegaPlusCategoryLink;Mega Plus Plan
;;MegaExtremeCategoryLink;Mega Extreme Plan
;;BrowseTalkCategoryLink;Browse & Talk Plus
;;BroadbandInternetCategoryLink;Broadband Internet
;;EssentialCategoryLink;Essential
;;PlusCategoryLink;Plus
;;MaxCategoryLink;Max
#;;TurboCategoryLink;Turbo
;;InternetDevicesCategoryLink;Devices
;;InternetRouterCategoryLink;Router
;;InternetSpeedCategoryLink;Speed Test
;;InternetAboutCategoryLink;About
;;InternetRateAdjustmentLink;Rate Adjustment Notice
;;TVPlansCategoryLink;Plans
;;TVPlusCategoryLink;TV Plus
;;TVMaxCategoryLink;TV Max
;;TVWatchAllCategoryLink;TV Watch All
;;TVAddonCategoryLink;Addons
;;TVPremiumChannelCategoryLink;Premium Channels
;;TVSportPackCategoryLink;Sport Packs
;;TVFlowExpCategoryLink;Flow Experience
;;TVFlowtoGoCategoryLink;Flow to Go
;;TVFlowOnDemandCategoryLink;Flow On Demand
#;;TVHboGoCategoryLink;HBO Go
;;TVAboutCategoryLink;About
;;TVQuestionsCategoryLink;"Questions?"
;;HomePhonePlanCategoryLink;Plans
;;HomePhoneBasicCategoryLink;Basic
;;HomePhoneEssentialCategoryLink;Essential
;;HomePhonePlusCategoryLink;Plus
;;HomePhoneCardServicesCategoryLink;Card Services
;;HomePhoneInternationalCategoryLink;International Plans
;;HomePhoneWorldPakCategoryLink;WorldPak
;;HomePhoneWorldPakLiteCategoryLink;WorldPak Lite
;;HomePhoneInternationalRatesCategoryLink;International rates
;;HomePhoneAboutCategoryLink;About
;;HomePhoneFAQCategoryLink;Home Phone FAQ
;;HomePhoneRateAdjustmentCategoryLink;Rate Adjustment Notice
;;HomePhoneExpressNotificationCategoryLink;Home Phone Express Notification
;;Footer_TVPlusCategoryLink;TV Plus
;;Footer_TVMaxCategoryLink;TV Max
;;Footer_TVWatchAllCategoryLink;TV Watch All
;;Footer_TVPremiumChannelCategoryLink;Premium Channels
;;Footer_TVSportPackCategoryLink;Sport Packs
#;;Footer_TVFlowExpCategoryLink;Flow Experience
;;Footer_TVFlowtoGoCategoryLink;Flow to Go
;;Footer_TVFlowOnDemandCategoryLink;Flow On Demand
#;;Footer_TVHboGoCategoryLink;HBO Go
;;Footer_TVAboutCategoryLink;About
;;Footer_PrepaidCategoryLink;Prepaid Plans
;;Footer_PostpaidCategoryLink;Postpaid Plans
;;Footer_RoamingCategoryLink;Roaming
;;Footer_DevicesCategoryLink;Device Catalogue
;;Footer_AboutCategoryLink;About
;;Footer_InternetRouterCategoryLink;Routers
;;Footer_InternetAboutCategoryLink;About
;;Footer_HomePhoneCardServicesCategoryLink;Card Services
;;Footer_HomePhoneAboutCategoryLink;Overview
;;SupportBillingCategoryLink;Billing
;;SupportMobileAppsCategoryLink;Mobile Apps
;;SupportSwitchtoFlowCategoryLink;Switch to Flow
;;SupportImportantNotificationCategoryLink;Important notifications
;;SupportMobileCategoryLink;Mobile
;;SupportBundleCategoryLink;Bundle
;;SupportHomePhoneCategoryLink;Home Phones
;;SupportTVCategoryLink;TV
;;SupportServicesCategoryLink;Services
;;SupportOtherCategoryLink;Others
;;HomeLink;Home
;;Footer_TVOverviewCategoryLink;Overview
;;Footer_TVChannelLineupCategoryLink;Channel Lineup
;;Footer_TVAddoOnsCategoryLink; Add ons
;;Footer_FlowHomeCategoryLink;Home
;;Footer_FlowDiffCategoryLink; The Flow Difference
;;Footer_MobileTopUpLink;Mobile Top Up
;;Footer_OurAppsLink;Our Apps
;;Footer_RechargePartnerLink;Recharge Partners
;;Footer_InternetOverviewLink;Overview
;;Footer_InternetBrowseAndTalkLink;Browse & Talk
;;Footer_InternetEquipmentLink;Equipment
;;Footer_InternetSpeedTestLink;Speed Test
;;Footer_HomePhoneInternationalRatesCategoryLink;International Rates
;;Footer_HomePhoneCardServiceCategoryLink;Card Services
;;Footer_InternetTVPhoneCategoryLink;Bundles

UPDATE LLABundleProductCMSComponent; $contentCV[unique = true]  ; uid[unique = true]                                ; headlineText[lang = $lang]                  ; name               ; subheading[$lang]
                                   ;                            ; bundleproduct_homepage              ; "Live more Flow"                            ;                    ;
                                   ;                            ; bundleproduct_internet              ; "Internet Plans"                            ; Internet           ; Say hello to fast, reliable and hassle-free surfing, sharing and streaming across all your devices at blazing fast speeds of up to 100Mbps.
                                   ;                            ; bundleproduct_homePhones            ; "Home Phone Plans"                          ; HomePhone          ; Find the perfect home plan for you and your family.
                                   ;                            ; bundleproduct_internationalPhones   ; "Home Phone International Plans"            ; International      ; Keep in touch with people around the world.
                                   ;                            ; bundleproduct_prepaid               ; "Mobile Prepaid Plans"                      ; Prepaid            ;
                                   ;                            ; bundleproduct_postpaid              ; "Mobile Postpaid Plans"                     ; Postpaid           ;

                                   ;                            ; bundleproduct_roamingprepaid        ; "Roaming - Prepaid"                     ; Roaming            ;
                                   ;                            ; bundleproduct_roamingpostpaid       ; "Roaming - Postpaid"                    ; Roaming            ;
                                   ;                            ; bundleproduct_internetTvPhone       ; "Choose your bundle and light up your life" ; 3Pbundles          ; Find the perfect entertainment plan for your and your family!
                                   ;                            ; bundleproduct_internetPhone         ; "Choose your bundle and light up your life" ; 2Pbundles          ; Find the perfect entertainment plan for your and your family!

                                   ;                            ; bundleproduct_tvplans               ; "TV Packages"                               ; TV                 ; Say hello to fast, reliable and hassle-free surfing, sharing and streaming across all your devices at blazing fast speeds of up to 100Mbps.
                                   ;                            ; bundleproduct_tvplansaddon               ; "Premium TV Movies"                               ; TvAddon                 ; Say hello to fast, reliable and hassle-free surfing, sharing and streaming across all your devices at blazing fast speeds of up to 100Mbps.
                                   ;                            ; bundleproduct_addition_comobos_tvplansaddon       ; "ADDITIONAL COMBOS"                         ; additional_combos  ;


UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=$lang];
                            ;                       ;Internet_tv_phonePLPBottomParagraphComponent;"<div class=""container bundle-plan-list""><h3 class=""headline col-xs-12"">B&T Plans details</h3>
	<div class=""col-sm-6 col-xs-12"">
		<ul>
			<li>
				<p><span>Talk credit</span> can be used to call local FLOW (formerly LIME mobile &amp; landline), FLOW Barbados, LIME Caribbean* (mobile &amp; landline), USA (mobile &amp; landline), Canada (mobile &amp; landline), UK (landline only), China (mobile &amp; landline)**, India (mobile &amp; landline), Spain (landline only).</p>
			</li>
			<li>
				<p>For <span>international calls</span>, &nbsp;dial 103 before the number you wish to call. For USA, Canada, FLOW Barbados &amp; LIME Caribbean*, dial 103 + 1 + area code + your party's number. Example: 103 1 954 999 9999. For the rest of the world, dial 103 + 011 + country code + your party's number. Example: 103 011 44 209 999 9999.</p>
			</li>
			<li>
				<p>Use a <span>JUST TALK Calling Card</span> for calls outside of the Browse &amp; Talk plan such as other local mobile/landline and international destinations. Use it also once the monthly allotted talk credit is used up.</p>
			</li>
		</ul>
	</div>
	<div class=""col-sm-6 col-xs-12"">
		<ul>
			<li>
				<p>Check <span>your balance</span> anytime.&nbsp;Just dial toll free to <span><a href=""tel:1-888-429-5987"">1-888-429-5987</a></span>.</p>
			</li>
			<li>
				<p>Remember to <span>pay on time</span>, to benefit from talk credit. If the bill payment is made through an agency, remember it should be made at least 03 days prior to the due date.</p>
			</li>
			<li>
				<p>Allocated <span>talk credit expires</span> after 30 days.</p>
			</li>
			<li>
				<p>Check the <span>rates</span> per minute in the table below.</p>
			</li>
		</ul>
	</div>
</div><div class=""container bundle-rates"">
		<h3 class=""headline col-xs-12"">B&amp;T Rates</h3>
		<table class=""table table-striped"">
			<thead>
				<tr>
					<th>Destination</th>
					<th>Charge per minute</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>FLOW Barbados &amp; LIME Caribbean Mobile*</td>
					<td>$ 20.00</td>
				</tr>
				<tr>
					<td>FLOW Barbados &amp; LIME Caribbean Landline*</td>
					<td>$ 10.00</td>
				</tr>
				<tr>
					<td>FLOW Home Phone (Formerly LIME)</td>
					<td>$ 1.05</td>
				</tr>
				<tr>
					<td>FLOW Mobile</td>
					<td>$ 2.85</td>
				</tr>
				<tr>
					<td>USA, Canada, India (Mobile &amp; Landline)</td>
					<td>$ 11.00</td>
				</tr>
				<tr>
					<td>China (Mobile &amp; Landline) - Excluding Hong Kong</td>
					<td>$ 10.00</td>
				</tr>
				<tr>
					<td>UK (Landline Only)</td>
					<td>$ 11.00</td>
				</tr>
				<tr>
					<td>Spain (Landline Only)</td>
					<td>$ 10.00</td>
				</tr>
			</tbody>
		</table>
		<p class=""disclaimer"">*LIME Caribbean destinations: Anguilla, Antigua (Mobile Only), BVI, Cayman, Dominica, Grenada, Montserrat, St. Kitts, St. Lucia, St. Vincent, Turks &amp; Caicos. GCT and Special Telephone Call Tax not included.</p>
	</div>";
