# ---------------------------------------------------------------------------
# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# ---------------------------------------------------------------------------

# ImpEx for Product Specifications

$productCatalog = panamaProductCatalog
$catalogVersion = catalogversion(catalog(id[default = $productCatalog]), version[default = 'Staged'])[unique = true, default = $productCatalog:Staged]
$defaultApprovalStatus = approvalstatus(code)[default = 'check']


INSERT_UPDATE TmaProductSpecCharacteristicValue ; id[unique = true]          ; value                           ; unitOfMeasure(id)                   ; productSpecCharacteristic(id, $catalogVersion) ; $catalogVersion
                                                ; 500_min                    ; 500                             ; postpaid_phone_minutes              ; voice                                          ;
                                                ; unlimited_min              ; Minutos                         ; phone_minutes_postpaid              ; voice                                          ;
                                               # ; 200_landline               ; 200                             ; phone_minutes                       ; voice                                          ;
                                               ; unlimited_sms              ; SMS                             ; postpaid_limited                     ; sms                                            ;
                                                ; 100_sms                    ; 100                             ; postpaid_sms                        ; sms                                            ;
                                                ; 150_mb                     ; 150                             ; mbps                                ; data_volume                                    ;
                                                ; 250_mb                     ; 250                             ; mbps                                ; data_volume                                    ;
                                                ; 300_mb                     ; 300                             ; mbps                                ; data_volume                                    ;
                                                #; 400_mb                     ; 400                             ; mbps                                ; data_volume                                    ;
                                               # ; 600_mb                     ; 600                             ; mbps                                ; data_volume                                    ;
                                               # ; 1000_mb                    ; 1000                            ; mbps                                ; data_volume                                    ;
                                                ; 186_channels               ; 186                             ; channels                            ; sd_channels                                    ;
                                                ; TV_187_channels            ; TV                              ; 3pChannels                          ; sd_channels                                    ;
                                                ; share_data                 ; 2GB                            ; postpaid_gb                          ; data_volume                                   ;
#Prepaid specs 
                                                ; unlimited_data             ; Data                            ; limited                                ; sim_card                                    ;
                                                ; 1500_data                  ; 1500                            ; mbps                                ; sim_card                                    ;
                                                ; unliminted_min_movil       ; unlimited                       ; phone_minutes                       ; mobile_icon                                    ;
                                                ; 300_min_movil              ; 300                             ; phone_minutes                       ; mobile_icon                                    ;
                                                ; whatsapp_30dtenure         ; 30                              ; unit                                ; whatsapp                                       ;
                                               #  ; plan_limits                ; Plan                            ; plan_limit                          ; limit                                     ;
                                                  ; flexible_contract          ; Contrato                       ; postpaid_contract                  ; contract                                      ;
                                                  ; data_share                 ; 4.5GB                          ; postpaid_gb                       ; data_volume                                    ;
                                                  ; TV_187_channels_withChannelImage            ; TV                              ; 3pChannels                          ; sd_channels                                    ;

												; 200_landline               ; 200                             ;                        ; voice                                          ;
                                               ; 1000_mb                    ; 1000                            ;                                 ; data_volume                                    ;
                                                ; 1000_mb_CP                 ; 1000                            ;                                 ; data_volume                                    ;
                                                ; 206_channels               ; 206                             ; channels                            ; sd_channels                                    ;
                                               ; 198_channels               ; 198                             ; channels                            ; sd_channels                                    ;
                                               ; 400_mb                     ; 400                             ;                                 ; data_volume                                    ;
                                               ; 600_mb                     ; 600                             ;                                 ; data_volume                                    ;
                                                ; 600_mb_CP                  ; 600                             ;                                 ; data_volume                                    ;
                                                ; plan_limits                ; Plan                            ; plan_limit                          ; limit                                     ;
                                               ; 100_mb                     ; 100                             ;                                 ; data_volume                                    ;
                                               


INSERT_UPDATE TmaProductSpecCharacteristicValue ; id[unique = true]          ; value                           ; unitOfMeasure(id)                   ; productSpecCharacteristic(id, $catalogVersion) ; $catalogVersion
												; 20_sms					 ; 20							   ; postpaid_sms						 ; sms											  ;
												; 20_plan_contract_flexible	 ; 12, 18 or 24                    ; postpaid_contract                   ; contract 									  ;
												; network_minutes			 ; 100							   ; phone_minutes				 		 ; voice										  ;
												; data_included				 ; 5							   ; gb						 			 ; data_volume									  ;
												; data_social				 ; 15							   ; gb						 			 ; data_volume									  ;
                                                ; 20_plan_share_data         ; 5                               ; gb                         		 ; data_volume                                    ;
                                                ; 25_plan_100_sms            ; 100                             ; postpaid_sms                        ; sms                                            ;
												; 25_plan_contract_flexible	 ; 12, 18 or 24                    ; postpaid_contract                   ; contract 									  ;
												; 25_plan_network_minutes	 ; 250							   ; phone_minutes				         ; voice										  ;
                                                ; 25_unlimited_data          ; Data                            ; postpaid_unlimited                  ; data_volume                                    ;
                                                ; 25_plan_share_data         ; 5                               ; gb                         		 ; data_volume                                    ;
												

INSERT_UPDATE TmaProductSpecCharacteristic      ; id[unique = true]          ; $catalogVersion
                                                ; sms                        ;
                                                ; voice                      ;
                                                ; data_volume                ;
                                                ; data_speed                 ;
                                                ; sd_channels                ;
                                                ; hd_channels                ;
                                                ; tv_sessions                ;
                                                ; upload_speed               ;
                                                ; download_speed             ;
                                                ; msisdn                     ;
                                                ; landline                   ;
                                                ; channels                   ;
                                                ; whatsapp                   ;
                                                ; mobile_icon                ;
                                                ; sim_card                   ;
                                                ; 3pChannels                ;
                                                ; plan_limit                 ;
                                                ; limit                      ;
                                                ; contract                    ;
                                                ; months					 ;
                                                
                                               
INSERT_UPDATE TmaAtomicProductSpecification     ; id[unique = true]          ; productSpecificationTypes(code) ; onlineDate[dateformat = dd.MM.yyyy] ; offlineDate[dateformat = dd.MM.yyyy]           ; brand; approvalStatus(code)[default = 'approved']; productSpecCharacteristics(id, $catalogVersion); cfsSpecs(id, $catalogVersion); $catalogVersion;
                                                ; postpaidPlans              ;                                 ; 12.08.2017                          ; 30.12.2024                                     ;      ;                                           ;
                                                ; prepaidPlans               ;                                 ; 12.08.2017                          ; 30.12.2024                                     ;      ;                                           ;
                                                ; bundle                     ;                                 ; 12.08.2017                          ; 30.12.2024                                     ;      ;                                           ;
                                                ; addon                      ;                                 ; 12.08.2017                          ; 30.12.2022                                     ;      ;                                           ;
