# ---------------------------------------------------------------------------
# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# ---------------------------------------------------------------------------

# ImpEx for Importing Category Classifications into B2C Telco Store

# Macros / Replacement Parameter definitions
$productCatalog = puertoricoProductCatalog
$catalogVersion = catalogversion(catalog(id[default = $productCatalog]), version[default = 'Staged'])[unique = true, default = $productCatalog:Staged]
$classCatalogVersion = catalogversion(catalog(id[default = 'puertoricoClassification']), version[default = '1.0'])[unique = true, default = 'puertoricoClassification:1.0']
$classSystemVersion = systemVersion(catalog(id[default = 'puertoricoClassification']), version[default = '1.0'])[unique = true]
$class = classificationClass(ClassificationClass.code, $classCatalogVersion)[unique = true]
$categories = source(code, $classCatalogVersion)[unique = true]
$supercategories = target(code, $catalogVersion)[unique = true]
$attribute = classificationAttribute(code, $classSystemVersion)[unique = true]
$unit = unit(code, $classSystemVersion)
$lang = en

################################################# PEACOCK -Wave 1 ###############################################################################
INSERT_UPDATE ClassificationClass; $classCatalogVersion; code[unique = true]  ; allowedPrincipals(uid)[default = 'customergroup']
                                 ;                     ; 1000
                                 ;                     ; 2000
                                 ;                     ; 10000
                                 ;                     ; 20000
                                 ;                     ; 30000
                                 ;                     ; 40000

INSERT_UPDATE CategoryCategoryRelation; $categories; $supercategories
                                      ; 1000       ; smartPhones
                                      ; 1000       ; tablets
                                      ; 1000       ; smartwatches
                                      ; 1000       ; hotspot
                                      ; 2000       ; smartPhones
                                      ; 2000       ; tablets
                                      ; 2000       ; smartwatches
                                      ; 2000       ; hotspot
                                      ; 10000      ; smartPhones
                                      ; 10000      ; tablets
                                      ; 10000      ; smartwatches
                                      ; 10000      ; hotspot
                                      ; 20000      ; smartPhones
                                      ; 20000      ; tablets
                                      ; 20000      ; smartwatches                                      
                                      ; 20000      ; hotspot
                                      ; 30000      ; smartPhones
                                      ; 30000      ; tablets
                                      ; 30000      ; smartwatches
                                      ; 30000      ; hotspot
                                      ; 40000      ; smartPhones
                                      ; 40000      ; tablets
                                      ; 40000      ; smartwatches
                                      ; 40000      ; hotspot
									  
# Insert Classification Attributes
INSERT_UPDATE ClassificationAttribute; $classSystemVersion; code[unique = true]
;; speed, 1001                                   
;; resistane, 1002                               
;; battery, 1003                                 
;; screen, 1004                                  
;; rearcamera, 1005                              
;; ram, 1006                                     
;; operatingSystem, 1007                         
;; memory, 1008                                  
;; octaCore, 1009                                
;; screenType, 1010                              
;; frontcamera, 1011
;; speed-1, 1012
;; title1, 2001                                  
;; paragraph1, 2002                              
;; Rear Camera, 10001                            
;; Front Camera, 10002                           
;; Operating System, 10003                       
;; Processor, 10004                              
;; Internal Memory, 10005                        
;; Display, 10006                                
;; Battery, 10007                                
;; Music, 20001                                  
;; Music Player, 20002                           
;; Audio & Video Recording, 20003                
;; Apps & Downloads, 20004                       
;; Web Browser, 20005                            
;; Streaming, 20006                              
;; USB, 30001                                    
;; Wi-Fi, 30002                                  
;; Wi-Fi Calling, 30003                          
;; Mobile Internet, 30004                        
;; Bluetooth, 30005                              
;; Messaging, 30006                              
;; Fingerprint/Face ID, 40001                    
;; Micro/Nano/E Sim, 40002                       
;; USB/USB-C Cable, 40003                        
;; Device, 40004                                 
;; Color, 40005                                  
;; Color Code, 40006   

INSERT_UPDATE ClassAttributeAssignment; $class ; $attribute                                    ; position ; $unit ; attributeType(code[default = string]); multiValued[default = false]; range[default = false]; localized[default = true]
                                      ; 1000   ; speed, 1001                                   ; 1        ;       ;
                                      ; 1000   ; resistane, 1002                               ; 2        ;       ;
                                      ; 1000   ; battery, 1003                                 ; 3        ;       ;
                                      ; 1000   ; screen, 1004                                  ; 4        ;       ;
                                      ; 1000   ; rearcamera, 1005                              ; 5        ;       ;
                                      ; 1000   ; ram, 1006                                     ; 6        ;       ;
                                      ; 1000   ; operatingSystem, 1007                         ; 7        ;       ;
                                      ; 1000   ; memory, 1008                                  ; 8        ;       ;
                                      ; 1000   ; octaCore, 1009                                ; 9        ;       ;
                                      ; 1000   ; screenType, 1010                              ; 10       ;       ;
                                      ; 1000   ; frontcamera, 1011                             ; 11       ;       ;
                                      ; 1000   ; speed-1, 1012                                 ; 12       ;       ;
				      ; 2000   ; title1, 2001                                  ; 13       ;       ;
                                      ; 2000   ; paragraph1, 2002                              ; 14       ;       ;
                                      ; 10000  ; Rear Camera, 10001                            ; 15       ;       ;
                                      ; 10000  ; Front Camera, 10002                           ; 16       ;       ;
                                      ; 10000  ; Operating System, 10003                       ; 17       ;       ;
                                      ; 10000  ; Processor, 10004                              ; 18       ;       ;
                                      ; 10000  ; Internal Memory, 10005                        ; 19       ;       ;
                                      ; 10000  ; Display, 10006                                ; 20       ;       ;
                                      ; 10000  ; Battery, 10007                                ; 21       ;       ;
                                      ; 20000  ; Music, 20001                                  ; 22       ;       ;
                                      ; 20000  ; Music Player, 20002                           ; 23       ;       ;
                                      ; 20000  ; Audio & Video Recording, 20003                ; 24       ;       ;
                                      ; 20000  ; Apps & Downloads, 20004                       ; 25       ;       ;
                                      ; 20000  ; Web Browser, 20005                            ; 26       ;       ;
                                      ; 20000  ; Streaming, 20006                              ; 27       ;       ;
                                      ; 30000  ; USB, 30001                                    ; 28       ;       ;
                                      ; 30000  ; Wi-Fi, 30002                                  ; 29       ;       ;
                                      ; 30000  ; Wi-Fi Calling, 30003                          ; 30       ;       ;
                                      ; 30000  ; Mobile Internet, 30004                        ; 31       ;       ;
                                      ; 30000  ; Bluetooth, 30005                              ; 32       ;       ;
                                      ; 30000  ; Messaging, 30006                              ; 33       ;       ;
                                      ; 40000  ; Fingerprint/Face ID, 40001                    ; 34       ;       ;
                                      ; 40000  ; Micro/Nano/E Sim, 40002                       ; 35       ;       ;
                                      ; 40000  ; USB/USB-C Cable, 40003                        ; 36       ;       ;
                                      ; 40000  ; Device, 40004                                 ; 37       ;       ;
                                      ; 40000  ; Color, 40005                                  ; 38       ;       ;
                                      ; 40000  ; Color Code, 40006                             ; 39       ;       ;
