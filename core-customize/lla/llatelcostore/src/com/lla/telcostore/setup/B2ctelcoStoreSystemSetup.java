/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcostore.setup;

import de.hybris.platform.b2ctelcoservices.setup.CoreSystemSetup;
import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.commerceservices.setup.events.CoreDataImportedEvent;
import de.hybris.platform.commerceservices.setup.events.SampleDataImportedEvent;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.util.Utilities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.lla.telcostore.constants.LlatelcostoreConstants;


/**
 * System setup.
 */
@SystemSetup(extension = LlatelcostoreConstants.EXTENSIONNAME)
public class B2ctelcoStoreSystemSetup extends AbstractSystemSetup
{
	private static final String IMPORT_CORE_DATA = "importCoreData";
	private static final String IMPORT_SAMPLE_DATA = "importSampleData";
	private static final String ACTIVATE_SOLR_CRON_JOBS = "activateSolrCronJobs";
	private static final String COUNTRY = "country";

	@Autowired
	private ConfigurationService configurationService;
	private CoreDataImportService coreDataImportService;
	private SampleDataImportService sampleDataImportService;
	private CoreSystemSetup coreSystemSetup;

	@SystemSetupParameterMethod
	@Override
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<>();

		params.add(createCountriesSelectionSystemSetupParameter(COUNTRY,"Select Country"));
		params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", true));
		params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_DATA, "Import Sample Data", true));
		params.add(createBooleanSystemSetupParameter(ACTIVATE_SOLR_CRON_JOBS, "Activate Solr Cron Jobs", true));

		return params;
	}

	/**
	 * This method will be called during the system initialization.
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.ALL)
	public void createLLAProjectData(final SystemSetupContext context)
	{
		final String storeName = context.getParameter(context.getExtensionName() + "_" + COUNTRY);
		createProjectData(context,storeName);
	}

	public void createProjectData(final SystemSetupContext context,final String storeName)
	{
		final List<ImportData> importData = new ArrayList<>();

		final ImportData b2ctelcoImportData = new ImportData();
		b2ctelcoImportData.setProductCatalogName(storeName);
		b2ctelcoImportData.setContentCatalogNames(Arrays.asList(storeName));
		b2ctelcoImportData.setStoreNames(Arrays.asList(storeName));
		importData.add(b2ctelcoImportData);

		getCoreDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new CoreDataImportedEvent(context, importData));

		getSampleDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new SampleDataImportedEvent(context, importData));

		if (Registry.getCurrentTenant() != null && "api".equalsIgnoreCase(Registry.getCurrentTenant().getTenantID()))
		{
			importTestData();
		}

		// sync the telco content catalog again as sbg is using the telco content catalog, too
		executeCatalogSyncJob(context,storeName+"contentCatalog");

		// as promotions are not catalog aware,
		// we must import them into the online catalog which is only possible after the synchronization
		// as some online products are required
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/promotions/promotions-engine-configuration.impex",
						LlatelcostoreConstants.EXTENSIONNAME, storeName), false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/promotions.impex",
						LlatelcostoreConstants.EXTENSIONNAME, storeName), false);

		executeSolrIndexerCronJob(storeName+"Index", true);

//		getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/customerInventory/customerInventory.impex",
//				LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/common/user-groups.impex", LlatelcostoreConstants.EXTENSIONNAME), false);

		getSetupImpexService().importImpexFile(
				String.format("/%s/import/coredata/productimport/essentialdata-SimpleProductOffering.impex",
						LlatelcostoreConstants.EXTENSIONNAME),
				true);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/coredata/productimport/projectdata-SimpleProductOffering.impex",
						LlatelcostoreConstants.EXTENSIONNAME),
				true);

		getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/cronjobs/jobs.impex",LlatelcostoreConstants.EXTENSIONNAME),
				true);
		getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/user-groups.impex",LlatelcostoreConstants.EXTENSIONNAME),
				true);
		getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/notification-email-process.impex",LlatelcostoreConstants.EXTENSIONNAME),
				true);
		getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/fallout-process.impex",LlatelcostoreConstants.EXTENSIONNAME),
				true);
		if (StringUtils.isNotEmpty(storeName) && storeName.equalsIgnoreCase("jamaica"))
		{
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/stores/jamaica/jamaica-split-order-process.impex",LlatelcostoreConstants.EXTENSIONNAME),
					true);
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/stores/jamaica/jamaica-order-process.impex",LlatelcostoreConstants.EXTENSIONNAME),
					true);
		}
		if (!StringUtils.isEmpty(storeName) && storeName.equalsIgnoreCase("panama"))
		{
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/stores/panama/panama-address-mapping.impex",LlatelcostoreConstants.EXTENSIONNAME),
					true);
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/stores/panama/panama-split-order-process.impex",LlatelcostoreConstants.EXTENSIONNAME),
					true);
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/stores/panama/panama-order-process.impex",LlatelcostoreConstants.EXTENSIONNAME),
					true);
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/stores/panama/delivery-modes.impex",LlatelcostoreConstants.EXTENSIONNAME),
					true);
		}
		if (!StringUtils.isEmpty(storeName) && storeName.equalsIgnoreCase("puertorico")) {
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/productCatalogs/puertoricoProductCatalog/LLAConfiguratorMessage.impex",LlatelcostoreConstants.EXTENSIONNAME),
					true);
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/productCatalogs/puertoricoProductCatalog/LLAProductMapping.impex",LlatelcostoreConstants.EXTENSIONNAME),
					true);
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/productCatalogs/puertoricoProductCatalog/LLAServiceCodeMapping.impex",LlatelcostoreConstants.EXTENSIONNAME),
					true);
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/stores/puertorico/puertorico-order-process.impex",LlatelcostoreConstants.EXTENSIONNAME),
					true);
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/stores/puertorico/residenceTypes_en.impex",LlatelcostoreConstants.EXTENSIONNAME),
					true);
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/stores/puertorico/residenceTypes_es.impex",LlatelcostoreConstants.EXTENSIONNAME),
					true);
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/stores/puertorico/point-of-service_es.impex",LlatelcostoreConstants.EXTENSIONNAME),
					true);
		}
		if (!StringUtils.isEmpty(storeName) && storeName.equalsIgnoreCase("vtr")) {
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/productCatalogs/vtrProductCatalog/productsfeatures.impex", LlatelcostoreConstants.EXTENSIONNAME),
					true);
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/productCatalogs/vtrProductCatalog/tvChannels.impex", LlatelcostoreConstants.EXTENSIONNAME),
					true);
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/productCatalogs/vtrProductCatalog/channelmedia.impex", LlatelcostoreConstants.EXTENSIONNAME),
					true);
			getSetupImpexService().importImpexFile(
					String.format("/%s/import/sampledata/stores/vtr/vtr-order-process.impex", LlatelcostoreConstants.EXTENSIONNAME),
					true);
			getSetupImpexService().importImpexFile(
					String.format("/%s/import/sampledata/stores/vtr/VTRServiceCodeMapping.impex", LlatelcostoreConstants.EXTENSIONNAME),
					true);
			getSetupImpexService().importImpexFile(
					String.format("/%s/import/sampledata/stores/vtr/promotions.impex", LlatelcostoreConstants.EXTENSIONNAME),
					true);
			getSetupImpexService().importImpexFile(
					String.format("/%s/import/sampledata/stores/vtr/usergroups.impex", LlatelcostoreConstants.EXTENSIONNAME),
					true);
		}
		if (!StringUtils.isEmpty(storeName) && storeName.equalsIgnoreCase("cabletica")) {
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/stores/cabletica/cabletica-order-process.impex",LlatelcostoreConstants.EXTENSIONNAME),
					true);
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/productCatalogs/cableticaProductCatalog/LLACableticaRegion.impex",LlatelcostoreConstants.EXTENSIONNAME),
					true);
			getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/productCatalogs/cableticaProductCatalog/usageunits_en.impex",LlatelcostoreConstants.EXTENSIONNAME),
					true);

		}

	}

	@SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.UPDATE)
	public void updateHybrisTablesColumnsSize()
	{
		Connection conn = null;
		PreparedStatement pstmt = null;
		try
		{
			conn = Registry.getCurrentTenant().getDataSource().getConnection();
			pstmt = conn.prepareStatement("alter table tmaprodspeccharvalueslp alter column p_ctaspecdescription varchar(255)");
			pstmt.execute();
		}
		catch (final SQLException e)
		{
			System.out.println("Unable to alter database column!");
		}
		finally
		{
			Utilities.tryToCloseJDBC(conn, pstmt, null);
		}
	}


	private void importTestData()
	{
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/solr.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/customers.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/promotiongroups.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/cmssites.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/basestores.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/catalogs.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/categories.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/productofferings.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/stocklevels.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/prices.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
//		getSetupImpexService().importImpexFile(
//				String.format("/%s/import/sampledata/apidata/reviews.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/addresses.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/paymentdata.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/coupons.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/promotions.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/shoppingcarts.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/orders.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/subscriptiondata.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/policies.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/apidata/pos-stocklevel.impex", LlatelcostoreConstants.EXTENSIONNAME), false);
	}

	public CoreDataImportService getCoreDataImportService()
	{
		return coreDataImportService;
	}

	@Required
	public void setCoreDataImportService(final CoreDataImportService coreDataImportService)
	{
		this.coreDataImportService = coreDataImportService;
	}

	public SampleDataImportService getSampleDataImportService()
	{
		return sampleDataImportService;
	}

	@Required
	public void setSampleDataImportService(final SampleDataImportService sampleDataImportService)
	{
		this.sampleDataImportService = sampleDataImportService;
	}

	public CoreSystemSetup getCoreSystemSetup()
	{
		return coreSystemSetup;
	}

	@Autowired
	public void setCoreSystemSetup(final CoreSystemSetup coreSystemSetup)
	{
		this.coreSystemSetup = coreSystemSetup;
	}


	public ConfigurationService getConfigurationService() {
		return configurationService;
	}

	public void setConfigurationService(final ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}

	public SystemSetupParameter createCountriesSelectionSystemSetupParameter(final String key, final String label)
	{
		final SystemSetupParameter syncProductsParam = new SystemSetupParameter(key);
		syncProductsParam.setLabel(label);
		String setupCountries[]=getConfigurationService().getConfiguration().getString("project.data.setup.countries").split(",");
		for(String country:setupCountries){
			if(country.equalsIgnoreCase("jamaica"))
		     	syncProductsParam.addValue(country,true);
			else
				syncProductsParam.addValue(country,false);
		}
	   return syncProductsParam;
	}

}
