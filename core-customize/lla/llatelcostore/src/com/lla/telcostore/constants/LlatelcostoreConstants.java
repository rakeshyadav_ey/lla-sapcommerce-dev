/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.telcostore.constants;

/**
 * Constants.
 */
@SuppressWarnings("PMD")
public final class LlatelcostoreConstants
{
	public static final String EXTENSIONNAME = "llatelcostore";

	private LlatelcostoreConstants()
	{
		//empty
	}
}
