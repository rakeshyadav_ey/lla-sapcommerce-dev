package com.lla.mulesoft.integration.helper;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.lla.core.enums.LLATechnologyEnum;
import com.lla.core.enums.SourceEPC;
import de.hybris.platform.b2ctelcoservices.model.TmaBillingAccountModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class LlaMulesoftIntegrationHelper {
    private static final Logger LOG = Logger.getLogger(LlaMulesoftIntegrationHelper.class);
    @Autowired
    private ModelService modelService;
    public void mapOrderBillingAccountToCustomer(OrderModel order, CustomerModel customer)
    {
        if(null != order.getBillingAccount()) {
            TmaBillingAccountModel account = order.getBillingAccount();
            List<String> existingAccounts = new ArrayList<>();
            if(account.getBillingSystemId().equalsIgnoreCase(SourceEPC.CERILLION.toString())){
                existingAccounts = new ArrayList<>(customer.getCerillionAccounts());
                existingAccounts.remove(account.getBillingAccountId());
                existingAccounts.add(account.getBillingAccountId());
                customer.setCerillionAccounts(existingAccounts);
            }else if(account.getBillingSystemId().equalsIgnoreCase(SourceEPC.LIBERATE.toString())){
                existingAccounts = new ArrayList<>(customer.getLiberateAccounts());
                existingAccounts.remove(account.getBillingAccountId());
                existingAccounts.add(account.getBillingAccountId());
                customer.setLiberateAccounts(existingAccounts);
            }
            modelService.save(customer);
            modelService.refresh(customer);
        }
    }

    /**
     *  @param serviceResponse
     * @param customerModel
     * @param sourceEpc
     */
    public void mapBSSAccountToCustomer(final String serviceResponse, final CustomerModel customerModel, String sourceEpc, final OrderModel order)  {

        if(serviceResponse!=null && StringUtils.isNotEmpty(serviceResponse)) {
            JsonObject jsonObject = new JsonParser().parse(serviceResponse).getAsJsonObject();

            if(null!=jsonObject) {
                if (sourceEpc.equals(SourceEPC.CERILLION.toString())) {
                    LOG.info("Setting Customer Number for Cerillion order");
                    final String customerNo = jsonObject.get("customerNo").getAsString();
                    customerModel.setCerillionCustomerNo(customerNo);
                }else if(sourceEpc.equals(SourceEPC.LIBERATE.toString())){
                    populateLiberateAccountDetails(customerModel, jsonObject);
                } else if(sourceEpc.equals(SourceEPC.CSG.toString())){
                    populateCSGDetails(customerModel,jsonObject,order);
                }
                customerModel.setPresenceInBSS(Boolean.TRUE);
                modelService.save(customerModel);
                modelService.refresh(customerModel);
                LOG.info(String.format("Account Created in BSS for Customer :: %s",customerModel.getUid()));
            }
        }
    }

    private void populateCSGDetails(CustomerModel customerModel, JsonObject jsonObject, OrderModel order) {
        final String customerNo = jsonObject.get("id").getAsString();
        customerModel.setCsgCustomerNo(customerNo);
      
        JsonArray array = jsonObject.get("characteristic").getAsJsonArray();
        for (int i = 0; i < array.size(); i++) {
            JsonElement element = array.get(i);
            JsonObject idObject = element.getAsJsonObject();
            String responseCharacteristicsName = idObject.get("name").getAsString();
            String responseCharacteristicsVal = idObject.get("value").getAsString();
            switch(responseCharacteristicsName){
                case "businessUnit":
                     order.setBusinessUnit(responseCharacteristicsVal);
                     break;
                case "locationId":
                     order.setLocationId(responseCharacteristicsVal);
                     break;
                case "jobType":
                     order.setJobType(responseCharacteristicsVal);
                     break;
                case "dropType":
                      order.setDropType(LLATechnologyEnum.valueOf(responseCharacteristicsVal));
                      break;
            }
        }
        modelService.save(customerModel);
        modelService.save(order);
    }

    /**
     *  Populate Liberate Account Details
     * @param customerModel
     * @param jsonObject
     */
    private void populateLiberateAccountDetails(CustomerModel customerModel, JsonObject jsonObject) {
        final String customerNo = jsonObject.get("id").getAsString();
        customerModel.setLiberateCustomerNo(customerNo);
        JsonArray array = jsonObject.get("account").getAsJsonArray();
        LOG.info("JSOn Array" + array);
        List<String> liberateAccounts = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(customerModel.getLiberateAccounts())){
            liberateAccounts = new ArrayList<>(customerModel.getLiberateAccounts());
        }
        for (int i = 0; i < array.size(); i++) {
            JsonElement element = array.get(i);
            JsonObject idObject = element.getAsJsonObject();
            String liberateAccountId = idObject.get("id").getAsString();
            liberateAccounts.remove(liberateAccountId);
            liberateAccounts.add(liberateAccountId);
        }
        customerModel.setLiberateAccounts(liberateAccounts);
    }
}
