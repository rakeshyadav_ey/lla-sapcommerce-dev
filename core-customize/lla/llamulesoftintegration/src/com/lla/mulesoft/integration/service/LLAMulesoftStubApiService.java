package com.lla.mulesoft.integration.service;

import de.hybris.platform.commercefacades.order.data.OrderData;

public interface LLAMulesoftStubApiService {

    void internalCreditCheck(final String siteUid);

    void servicibilityCheck(final String siteUid);

    void externalCreditCheck(final String siteUid);
}
