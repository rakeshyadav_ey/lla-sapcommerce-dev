package com.lla.mulesoft.integration.service.impl;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.lla.mulesoft.integration.constants.LlamulesoftintegrationConstants;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.service.LLAMulesoftBillingAccountService;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import com.lla.telcotmfwebservices.v2.dto.*;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import javax.mail.Address;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class LLAMulesoftBillingAccountServiceImpl extends LLAAbstractService implements LLAMulesoftBillingAccountService {
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;
    @Override
    public String createBillingAccount(OrderModel order) throws LLAApiException {
        String billingAccountID = "";
        final String siteID =configurationService.getConfiguration().getString(new StringBuffer(LlamulesoftintegrationConstants.WEBSITE).append(LlamulesoftintegrationConstants.DECIMAL).append(order.getSite().getUid()).append(LlamulesoftintegrationConstants.DECIMAL).append(LlamulesoftintegrationConstants.CODE).toString());
        final String siteCode=order.getSite().getUid();
        HttpHeaders standardHeaders =  setupHeaders(siteID);
        if(LOG.isDebugEnabled()){
            LOG.debug("Billing Account generation start.");
        }
        standardHeaders.set("bss","cerillion");
        BillingAccount billingAccount = generateBillingObject(order);
        String postBody = new Gson().toJson(billingAccount);
        URI billingAccount_post_url = URI.create(apiUrlConstructor(getAPIServer(),siteID, LlamulesoftintegrationConstants.BILLING_ACCOUNT_SOURCE_EPC_CERILLION));
        ResponseEntity<String> serviceResponse=null;
        RestTemplate restTemplate_new=this.createRestTemplate(this.createRequestFactory(configurationService.getConfiguration().getString("max.pool.connection")));
        HttpEntity<String> httpEntity = new HttpEntity<>(postBody, standardHeaders);
        try {
            serviceResponse = restTemplate_new.exchange(billingAccount_post_url, HttpMethod.POST, httpEntity, String.class);
            if (serviceResponse.getStatusCode() == HttpStatus.CREATED) {
                LOG.info(String.format("Generated Billing Account! for Order %s",order.getCode()));
                billingAccountID = new JsonParser().parse(serviceResponse.getBody()).getAsJsonObject().get("accountNo").getAsString();
            } else {
                LOG.error("Error in Creating Billing Account. Server HTTP response: "+serviceResponse.getStatusCodeValue());
            }
        }catch(HttpClientErrorException.BadRequest badRequest){
            LOG.error(String.format("Create Billing Record  failed for customer %s and Order %s due to Bad Request :: %s", order.getUser().getUid(), order.getCode(),badRequest.getResponseBodyAsString()));

        }catch(HttpClientErrorException clientEx){
            LOG.error(String.format("Create customer failed for customer %s and Order %s due to Client Error  Exception :: %s", order.getUser().getUid(), order.getCode(),clientEx.getMessage()));

        }catch(ResourceAccessException ioException){
            LOG.error(String.format("Exception in making Api Call to Create Billing Account in BSS for Market:: %s due to  :: %s",siteCode,ioException.getMessage()));
            throw new  LLAApiException("End Resource is not accessible !!! Exception in making Api Call");
        }
        finally {
            HttpComponentsClientHttpRequestFactory requestFactory=(HttpComponentsClientHttpRequestFactory) restTemplate_new.getRequestFactory();
            LOG.info("Releasing current client connection for Billing Account Creation Request");
            try {
                requestFactory.destroy();
            } catch (Exception exception) {
                LOG.info(String.format("Error in releasing Connection obtained for Create Billing Account Call to BSS for Market  %s and due to %s  ",siteCode,exception));
            }
        }
        return billingAccountID;
    }

    private BillingAccount generateBillingObject(OrderModel order) {
        BillingAccount account = new BillingAccount();
        account.setAccountType(configurationService.getConfiguration().getString("llamulesoftintegration.billing.accountType"));
        account.setName(configurationService.getConfiguration().getString("llamulesoftintegration.billing.accountName"));
        BillStructure structure = new BillStructure();
        BillPresentationMediaRefOrValue presentationMedia = new BillPresentationMediaRefOrValue();
        List<BillPresentationMediaRefOrValue> media = new ArrayList<>();
        presentationMedia.setId(order.getCode());
        presentationMedia.setName(configurationService.getConfiguration().getString("llamulesoftintegration.billing.billingMedia"));
        media.add(presentationMedia);
        structure.setPresentationMedia(media);
        account.setBillStructure(structure);
        final CustomerModel customer = (CustomerModel)order.getUser();
        account.setContact(setContacts(customer, order.getInstallationAddress()));
        List<RelatedParty> relatedParties = new ArrayList<>();
        RelatedParty party = new RelatedParty();
        party.setId(customer.getCerillionCustomerNo());
        party.setName(StringUtils.join(new String[]{customer.getFirstName(), customer.getLastName()}, " "));
        relatedParties.add(party);
        account.setRelatedParty(relatedParties);
        return account;
    }

    private List<Contact> setContacts(final CustomerModel customer, final AddressModel addressModel) {
        final List<Contact> contacts = new ArrayList<>();
        Contact contact = new Contact();
        contact.setContactName(StringUtils.join(new String[]{customer.getFirstName(), customer.getLastName()}, " "));
        contact.setContactType("primary");
        contact.setPartyRoleType("Customer");
        final List<ContactMedium> contactMediumList = new ArrayList<>();
        final ContactMedium postalAddress=new ContactMedium();
        final MediumCharacteristic postalCharacteristic = new MediumCharacteristic();
        postalCharacteristic.setEmailAddress(CustomerType.GUEST.equals(customer.getType())?StringUtils.substringAfter(customer.getUid(),"|"):customer.getUid());
        postalCharacteristic.setStreet1(addressModel.getLine1());
        postalCharacteristic.setStreet2(addressModel.getLine2());
        postalCharacteristic.setStateOrProvince(addressModel.getArea());
        postalCharacteristic.setCity(addressModel.getTown());
        postalCharacteristic.setCountry(addressModel.getCountry().getName());
        postalCharacteristic.setPostCode(addressModel.getPostalcode());
        postalAddress.setCharacteristic(postalCharacteristic);
        postalAddress.setMediumType(configurationService.getConfiguration().getString("llamulesoftintegration.billing.postalcodeMediumType"));
        postalAddress.setPreferred(Boolean.TRUE);
        contactMediumList.add(postalAddress);
        if(StringUtils.isNotEmpty(addressModel.getPhone1())) {
            final ContactMedium phoneNumber = new ContactMedium();
            final MediumCharacteristic phoneCharacteristic = new MediumCharacteristic();
            phoneCharacteristic.setPhoneNumber(addressModel.getPhone1());
            phoneCharacteristic.setType(configurationService.getConfiguration().getString("llamulesoftintegration.billing.phonecharacteristic"));
            phoneNumber.setCharacteristic(phoneCharacteristic);
            phoneNumber.setMediumType(configurationService.getConfiguration().getString("llamulesoftintegration.billing.telephoneMediumType"));
            phoneNumber.setPreferred(Boolean.FALSE);
            contactMediumList.add(phoneNumber);
        }
        ContactMedium emailMedium = new ContactMedium();
        emailMedium.setMediumType(configurationService.getConfiguration().getString("llamulesoftintegration.billing.emailcharacteristic"));
        emailMedium.setPreferred(Boolean.TRUE);
        MediumCharacteristic emailCharacteristic = new MediumCharacteristic();
        emailCharacteristic.setEmailAddress(CustomerType.GUEST.equals(customer.getType())?StringUtils.substringAfter(customer.getUid(),"|"):customer.getUid());
        emailCharacteristic.setType(configurationService.getConfiguration().getString("llamulesoftintegration.billing.emailcharacteristic"));
        emailMedium.setCharacteristic(emailCharacteristic);
        contactMediumList.add(emailMedium);

        contact.setContactMedium(contactMediumList);
        contacts.add(contact);
        return contacts;
    }
}
