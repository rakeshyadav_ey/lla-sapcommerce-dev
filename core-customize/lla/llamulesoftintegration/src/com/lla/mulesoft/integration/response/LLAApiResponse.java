package com.lla.mulesoft.integration.response;

import com.lla.telcotmfwebservices.v2.dto.NormalizedAddress;

import java.util.List;

public class LLAApiResponse
{

	private String errorCode;
	private String message;
	private String description;
	private int creditScore;
	private boolean debtBalanceFlag;
	private String isServicable;
	private List<NormalizedAddress> addressList;
	private String statusCode;

	public LLAApiResponse() {
	}

	/**
	 *
	 */
	public LLAApiResponse(final String errorCode, final String message, final String descriptionfinal, boolean debtBalanceFlag, final String isServicable,final List<NormalizedAddress> addressList)
	{
		super();
		this.errorCode = errorCode;
		this.message = message;
		this.description = description;
		this.debtBalanceFlag = debtBalanceFlag;
		this.isServicable = isServicable;
		this.addressList=addressList;
	}

	public LLAApiResponse(final int creditScore)
	{
		this.creditScore = creditScore;
	}

	/**
	*
	*/
	public LLAApiResponse(final boolean debtBalanceFlag)
	{
		super();
		this.debtBalanceFlag = debtBalanceFlag;
	}

	public LLAApiResponse(List<NormalizedAddress> addressList) {
		super();
		this.addressList = addressList;
	}

	public LLAApiResponse(final String isServicable)
	{
		this.isServicable = isServicable;
	}

	public LLAApiResponse(String isServicable, String statusCode) {
		this.isServicable = isServicable;
		this.statusCode = statusCode;
	}

	public int getCreditScore()
	{
		return creditScore;
	}

	public void setCreditScore(final int creditScore)
	{
		this.creditScore = creditScore;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(final String description)
	{
		this.description = description;
	}

	public String getErrorCode()
	{
		return errorCode;
	}

	public void setErrorCode(final String errorCode)
	{
		this.errorCode = errorCode;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}

	/**
	 * @return the debtBalanceFlag
	 */
	public boolean isDebtBalanceFlag()
	{
		return debtBalanceFlag;
	}

	/**
	 * @param debtBalanceFlag
	 *           the debtBalanceFlag to set
	 */
	public void setDebtBalanceFlag(final boolean debtBalanceFlag)
	{
		this.debtBalanceFlag = debtBalanceFlag;
	}

public String getIsServicable() {
		return isServicable;
	}

	public void setIsServicable(String isServicable) {
		this.isServicable = isServicable;
	}

	/**
	 * Gets address list.
	 *
	 * @return the address list
	 */
	public List<NormalizedAddress> getAddressList() {
		return addressList;
	}

	/**
	 * Sets address list.
	 *
	 * @param addressList the address list
	 */
	public void setAddressList(List<NormalizedAddress> addressList) {
		this.addressList = addressList;
	}

	/**
	 * Gets status code.
	 *
	 * @return the status code
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * Sets status code.
	 *
	 * @param statusCode the status code
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
}

