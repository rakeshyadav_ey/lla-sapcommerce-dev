package com.lla.mulesoft.integration.service.impl;


import com.lla.core.enums.ClickToCallReasonEnum;
import de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel;
import de.hybris.platform.catalog.CatalogVersionService;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.net.URI;


import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import com.google.gson.Gson;
import com.lla.mulesoft.integration.constants.LlamulesoftintegrationConstants;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.service.LLAMulesoftCallBackRequestService;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import com.lla.telcotmfwebservices.v2.dto.Customer;
import com.lla.mulesoft.integration.dto.ClickCallServicablity;
import org.springframework.web.client.RestTemplate;

public class LLAMulesoftCallBackRequestServiceImpl extends LLAAbstractService implements LLAMulesoftCallBackRequestService
{


	private static final Logger LOG = Logger.getLogger(LLAMulesoftCallBackRequestServiceImpl.class);

	@Autowired
	private EnumerationService enumerationService;

	@Autowired
	private CatalogVersionService catalogVersionService;
	@Autowired
	private ProductService productService;
	@Autowired
	private BaseSiteService baseSiteService;
	@Autowired
	private UserService userService;
	@Autowired
	private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

	private final HttpHeaders headers = new HttpHeaders();

	/**
	 * Create Customer Call Back Request - -- Make call to Mulesoft Customer API
	 * @param abstractOrderModel
	 * @param clickCallServicablity
	 * @param clickToCallReasonEnum
	 * @return
	 * @throws LLAApiException
	 */
	@Override
	public String createCallBackRequest(final AbstractOrderModel abstractOrderModel, ClickCallServicablity clickCallServicablity, ClickToCallReasonEnum clickToCallReasonEnum) throws Exception
	{
		LOG.info("Starting Mulesoft Connectivity");
		final String siteID = configurationService.getConfiguration()
				.getString(new StringBuffer(LlamulesoftintegrationConstants.WEBSITE).append(LlamulesoftintegrationConstants.DECIMAL)
						.append(abstractOrderModel.getSite().getUid()).append(LlamulesoftintegrationConstants.DECIMAL)
						.append(LlamulesoftintegrationConstants.CODE).toString());
		final CustomerModel customerData = (CustomerModel) abstractOrderModel.getUser();
		final HttpHeaders apiHeaders = setupCallRequestHeaders(siteID);
		String requestBody=generateRequestBody(abstractOrderModel, clickCallServicablity,clickToCallReasonEnum);
		final HttpEntity<String> httpEntity = new HttpEntity<>(requestBody,apiHeaders);
		final URI click_to_call_url = URI
				.create(apiUrlConstructor(getAPIServerForCallBack(), siteID, LlamulesoftintegrationConstants.CLICK_TO_CALL));

		LOG.info(String.format("C2C Request ::: %s",requestBody));
		LOG.info(String.format("Attempting to make Call Back Request service API ::: %s for Market ::%s",httpEntity.getBody(),siteID));
		RestTemplate restTemplate_new=this.createRestTemplate(this.createRequestFactory(configurationService.getConfiguration().getString("max.pool.connection")));

		ResponseEntity<String> serviceResponse = null;
		try
		{
			setMuleSoftConnectionTimeout(restTemplate_new);
			serviceResponse = restTemplate_new.exchange(click_to_call_url, HttpMethod.POST, httpEntity, String.class);
			if (serviceResponse.getStatusCode() == HttpStatus.OK)
			{
				LOG.info(String.format("Genesys C2C Call Back Request %s Created for Market ::%s", customerData.getUid(),siteID));
			}
			else
			{
				LOG.error("Error in creating Call Back Request. Server HTTP response: " + serviceResponse.getStatusCodeValue());
				//validateAPIResponse(serviceResponse);
				return StringUtils.EMPTY;
			}
		}
		catch (final HttpClientErrorException.BadRequest badRequest)
		{
			LOG.error(String.format("Create call back request failed for customer %s due to Bad Request :: %s",
					customerData.getUid(), badRequest.getResponseBodyAsString()));
			return StringUtils.EMPTY;
		}
		catch (final HttpClientErrorException clientEx)
		{
			LOG.error(String.format("Create call back request failed for customer %s due to Client Error Exception  :: %s",
					customerData.getUid(), clientEx.getMessage()));
			return StringUtils.EMPTY;
		}
		catch (final HttpServerErrorException.BadGateway clientEx)
		{
			LOG.error(String.format("Create call back request failed for customer %s due to bad Gateway  :: %s and Code %s",
					customerData.getUid(), clientEx.getMessage(),clientEx.getStatusCode()));
			return StringUtils.EMPTY;
		}
		catch (final HttpServerErrorException.InternalServerError clientEx)
		{
			LOG.error(String.format("Create call back request failed for customer %s due to Internal Server Error  :: %s and Code %s",
					customerData.getUid(), clientEx.getMessage(),clientEx.getStatusCode()));
			return StringUtils.EMPTY;
		}
	    catch (final HttpServerErrorException.GatewayTimeout clientEx)
		{
			LOG.error(String.format("Create call back request failed for customer %s due to  Gateway Timeout  :: %s and Code %s",
					customerData.getUid(), clientEx.getMessage(),clientEx.getStatusCode()));
			return StringUtils.EMPTY;
		}
		catch (final HttpServerErrorException.ServiceUnavailable clientEx)
		{
			LOG.error(String.format("Create call back request failed for customer %s due to bad Service Unavailability  :: %s and Code %s",
					customerData.getUid(), clientEx.getMessage(),clientEx.getStatusCode()));
			return StringUtils.EMPTY;
		}
		catch (final HttpServerErrorException.NotImplemented clientEx)
		{
			LOG.error(String.format("Create call back request failed for customer %s due to no implementation  :: %s and Code %s",
					customerData.getUid(), clientEx.getMessage(),clientEx.getStatusCode()));
			return StringUtils.EMPTY;
		}
	    catch (final ResourceAccessException ioException)
		{
			/* throw new TmaApiException(ioException.getMessage()) ; */
			LOG.error(String.format("Exception in making Api Call :: %s", ioException.getMessage()));
			throw new LLAApiException("Exception in making Api Call for Genesys C2C for Market "+siteID);
		}
		finally {
			HttpComponentsClientHttpRequestFactory requestFactory=(HttpComponentsClientHttpRequestFactory) restTemplate_new.getRequestFactory();
			LOG.info("Releasing current client connection for Click to Call Request");
			requestFactory.destroy();
		}
		return null != serviceResponse ? serviceResponse.getBody() : StringUtils.EMPTY;
	}

	private HttpHeaders setupCallRequestHeaders(final String siteId)
	{
		headers.set("Cache-Control", "no-cache");
		headers.set("Content-Type", "application/json");
		headers.set("client_id", configurationService.getConfiguration().getString("cabletica.mulesoft.clicktocall.api.clientid"));
		headers.set("client_secret",
				configurationService.getConfiguration().getString("cabletica.mulesoft.clicktocall.api.clientsecret"));
		headers.set("channelId", configurationService.getConfiguration().getString("cabletica.mulesoft.clicktocall.api.channelId"));
		headers.set("Correlation-ID", configurationService.getConfiguration().getString("cabletica.api.clicktocall.correlationId"));
		return headers;
	}


	private String generateRequestBody(final AbstractOrderModel abstractOrderModel, final ClickCallServicablity clickCallServicablity, final ClickToCallReasonEnum clickToCallReasonEnum)
	{

		final CustomerModel commerceCustomer = (CustomerModel) abstractOrderModel.getUser();
		final Customer customer = new Customer();
		//userService.setCurrentUser(commerceCustomer);
		catalogVersionService.setSessionCatalogVersion(abstractOrderModel.getSite().getUid()+"ProductCatalog","Online");
		baseSiteService.setCurrentBaseSite(abstractOrderModel.getSite(),Boolean.TRUE);
		if(null!=clickCallServicablity){
			TmaSimpleProductOfferingModel poSpo=(TmaSimpleProductOfferingModel)productService.getProductForCode(catalogVersionService.getSessionCatalogVersionForCatalog(abstractOrderModel.getSite().getUid()+"ProductCatalog"),clickCallServicablity.getProduct());
			customer.setPhoneNumber(clickCallServicablity.getPhoneNumber());
			customer.setIdentityNumber(clickCallServicablity.getIdentityNumber());
			customer.setEmail(clickCallServicablity.getEmail());
			customer.setProduct(new StringBuffer(poSpo.getName(getCurrentLocale(abstractOrderModel))).append("_").append(enumerationService.getEnumerationName(clickToCallReasonEnum, getCurrentLocale(abstractOrderModel))).toString());
		}else{
			customer.setFirstName(StringUtils.isNotEmpty(commerceCustomer.getFirstName())?commerceCustomer.getFirstName().trim():StringUtils.EMPTY);
			customer.setLastName(StringUtils.isNotEmpty(commerceCustomer.getLastName())?commerceCustomer.getLastName().trim():StringUtils.EMPTY);
			customer.setEmail(commerceCustomer.getAdditionalEmail());
			customer.setPhoneNumber(commerceCustomer.getMobilePhone());
			customer.setIdentityNumber(commerceCustomer.getDocumentNumber());
			customer.setProduct(new StringBuffer((llaMulesoftIntegrationUtil.getProductForC2C(abstractOrderModel)).getName(getCurrentLocale(abstractOrderModel))).append("_").append(getLocalizedReason(abstractOrderModel, clickToCallReasonEnum)).toString());
		}
		final String responseJson = new Gson().toJson(customer);
		LOG.info(String.format("Create Call Back Request Request JSON %s", responseJson));
		return responseJson;
	}

	private String getLocalizedReason(AbstractOrderModel abstractOrderModel, ClickToCallReasonEnum clickToCallReasonEnum) {
		if (abstractOrderModel instanceof CartModel)
		{
			if(null != clickToCallReasonEnum)
				return enumerationService.getEnumerationName(clickToCallReasonEnum, getCurrentLocale(abstractOrderModel));
			else
				return "_Lead";
		}
		else
		{
			return abstractOrderModel.getCode();
		}
	}
}
