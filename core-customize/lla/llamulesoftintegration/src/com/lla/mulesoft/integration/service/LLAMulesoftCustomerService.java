package com.lla.mulesoft.integration.service;

import com.lla.mulesoft.integration.exception.LLAApiException;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import org.apache.http.conn.ConnectTimeoutException;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;

public interface LLAMulesoftCustomerService {
    /**
     * Create Customer in BSS System
     * @param orderData
     * @param sourceEpc
     * @return
     * @throws LLAApiException
     */
    String createCustomerInBSS(OrderModel orderData, final String sourceEpc) throws LLAApiException;

    /**
     * Validate User is Existing or New
     * @param cartModel
     * @return
     * @throws Exception
     */
    boolean isNewCustomer (CartModel cartModel) throws Exception;
}