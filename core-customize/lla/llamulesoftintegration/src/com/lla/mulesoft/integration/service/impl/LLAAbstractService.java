package com.lla.mulesoft.integration.service.impl;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.lla.core.enums.CustomerStatusEnum;
import com.lla.mulesoft.integration.constants.LlamulesoftintegrationConstants;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.interceptor.RestInterceptor;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.SSLContext;

import de.hybris.platform.site.BaseSiteService;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.lang.Nullable;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Locale;
import java.util.Objects;
public class LLAAbstractService {
	public static final Logger LOG = Logger.getLogger(LLAAbstractService.class);
	protected static final String CABLETICA_SITE_ID = "website.baseSite.uid.cabletica";
	@Autowired
	public ConfigurationService configurationService;

	@Autowired
    private SiteConfigService siteConfigService;

	protected RestTemplate restTemplate;
	protected HttpHeaders headers = new HttpHeaders();
    public static final int CLOSE_IDLE_CONNECTION_WAIT_TIME_SECS = 120;
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

	@Autowired
	private CMSSiteService cmsSiteService;

    @Autowired
    private ModelService modelService;
    @Autowired
    private BaseSiteService baseSiteService;
    @Autowired
    private CommonI18NService commonI18NService;

    public ModelService getModelService() {
        return modelService;
    }
    public LLAAbstractService(){
        try {
            TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
            SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
                    .loadTrustMaterial(null, acceptingTrustStrategy)
                    .build();
            SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
            CloseableHttpClient httpClient = HttpClients.custom()
                    .setSSLSocketFactory(csf)
                    .build();
            HttpComponentsClientHttpRequestFactory requestFactory =
                    new HttpComponentsClientHttpRequestFactory();
            requestFactory.setHttpClient(httpClient);
            restTemplate = new RestTemplate(requestFactory);
        }catch (Exception ex){
            restTemplate = new RestTemplate();
        }
      //  restTemplate.getInterceptors().add(new RestInterceptor());
    }
    @Bean
    @Qualifier("llaRestService")
    public RestTemplate createRestTemplate(ClientHttpRequestFactory factory) {
        RestTemplate restTemplate = new RestTemplate(factory);
     //   restTemplate.getInterceptors().add(new RestInterceptor());
        return restTemplate;
    }
    @Bean
    public ClientHttpRequestFactory createRequestFactory(@Value("${llamulesoftintegration.mulesoft.api.timeout}") String maxConn) {
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        int maxPoolConnection=configurationService.getConfiguration().getInt("max.pool.connection");
        connectionManager.setMaxTotal(maxPoolConnection);
        connectionManager.setDefaultMaxPerRoute(100);
        int timeoutValue=configurationService.getConfiguration().getInt("llamulesoftintegration.mulesoft.api.timeout");
        RequestConfig config = RequestConfig.custom()
                .setConnectionRequestTimeout(timeoutValue)
                .setConnectTimeout(timeoutValue)
                .setSocketTimeout(timeoutValue).setStaleConnectionCheckEnabled(true).build();
        CloseableHttpClient httpClient = HttpClientBuilder.create().setConnectionManager(connectionManager)
                .setDefaultRequestConfig(config).build();
        return new HttpComponentsClientHttpRequestFactory(httpClient);
    }

    /**
     * Get API Server Url
     * @return
     */
    protected String getAPIServer(){
        return configurationService.getConfiguration().getString(LlamulesoftintegrationConstants.MULESOFT_ENDPOINT_SERVER);
    }
    
    /**
     * Get API Server Url
     * @return
     */
    protected String getAPIServerForCallBack(){
        return configurationService.getConfiguration().getString(LlamulesoftintegrationConstants.MULESOFT_ENDPOINT_SERVER_CALL_BACK);
    }

    /**
     * Get API Server Url
     * @return
     */
    protected String getFuzzyAPIServer(){
        return configurationService.getConfiguration().getString(LlamulesoftintegrationConstants.FUZZY_ADDRESS_NORMALIZATION_URL);
    }
    protected String getAddressBusinessUnit(){
        return configurationService.getConfiguration().getString(LlamulesoftintegrationConstants.PUERTORICO_FUZZY_ADDRESS_BUSINESS_ID);
    }

    protected String getBusinessUnit()
    {
        return configurationService.getConfiguration().getString(new StringBuffer(cmsSiteService.getCurrentSite().getUid()).append(".api.business.unit").toString());
    }

    /**
     *
     * @param serverUrl
     * @param businessUnit
     * @param endPointResolver
     * @return
     */
    protected String apiUrlConstructor(final String serverUrl, final String businessUnit,final String endPointResolver){
        return new StringBuffer(serverUrl).append(businessUnit).append(configurationService.getConfiguration().getString(endPointResolver)).toString();
     }

    /**
     *
     * @param serverUrl
     * @param businessUnit
     * @param endPointResolver
     * @param parameter
     * @return
     */
    protected String apiUrlConstructor(final String serverUrl, final String businessUnit,final String endPointResolver,final String parameter){
        return new StringBuffer(serverUrl).append(businessUnit).append(configurationService.getConfiguration().getString(endPointResolver)).append(parameter).toString();
    }

    /**
     *  Setting up Headers
     */
    protected HttpHeaders setupHeaders(final String siteId){
        headers.set("Cache-Control", "no-cache");
        //headers.set("Content-Type", "application/json");
        switch(siteId){
            case "JM":
            	headers.set("Content-Type", "application/json");
                headers.set("client-id",configurationService.getConfiguration().getString("mulesoft.api.clientid"));
                headers.set("client-secret", configurationService.getConfiguration().getString("mulesoft.api.clientsecret"));
                headers.set("lob", configurationService.getConfiguration().getString("mulesoft.api.lineofbusiness"));
                break;
            case "PA":
            	headers.set("Content-Type", "application/json");
                headers.set("client-id",configurationService.getConfiguration().getString("panama.mulesoft.api.clientid"));
                headers.set("client-secret", configurationService.getConfiguration().getString("panama.mulesoft.api.clientsecret"));
                headers.set("channelId", configurationService.getConfiguration().getString("panama.mulesoft.api.channelId"));
                headers.set("lob", configurationService.getConfiguration().getString("mulesoft.api.lineofbusiness"));
                break;
            case "PR":
                headers.set("client-id",configurationService.getConfiguration().getString("puertorico.mulesoft.api.clientid"));
                headers.set("client-secret", configurationService.getConfiguration().getString("puertorico.mulesoft.api.clientsecret"));
                headers.set("channelId", configurationService.getConfiguration().getString("puertorico.mulesoft.api.channelId"));
                headers.set("lob", configurationService.getConfiguration().getString("mulesoft.api.lineofbusiness"));
                headers.set("correlationId","correlationId");
                break;
            case "CL":
            	headers.set("Content-Type", "application/json");
            	  headers.set("client-id",configurationService.getConfiguration().getString("vtr.mulesoft.api.clientid"));
                 headers.set("client-secret",configurationService.getConfiguration().getString("vtr.mulesoft.api.clientsecret"));
                 headers.set("channelId", configurationService.getConfiguration().getString("vtr.mulesoft.api.channelId"));
                 headers.set("lob", configurationService.getConfiguration().getString("mulesoft.api.lineofbusiness"));
                 break;
            case "CR":
                headers.set("client-id",configurationService.getConfiguration().getString("cabletica.mulesoft.api.clientid"));
                headers.set("client-secret", configurationService.getConfiguration().getString("cabletica.mulesoft.api.clientsecret"));
                headers.set("channelId", configurationService.getConfiguration().getString("cabletica.mulesoft.api.channelId"));
                break;
        }
        return headers;
    }

    public RestTemplate setMuleSoftConnectionTimeout( RestTemplate restTemplate)
    {


        HttpComponentsClientHttpRequestFactory reqFactory = (HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory();
        if(Objects.nonNull(reqFactory))
        {
            reqFactory.setConnectTimeout(configurationService.getConfiguration().getInt("llamulesoftintegration.mulesoft.api.timeout"));
            reqFactory.setReadTimeout(configurationService.getConfiguration().getInt("llamulesoftintegration.mulesoft.api.timeout"));
            reqFactory.setConnectionRequestTimeout(configurationService.getConfiguration().getInt("llamulesoftintegration.mulesoft.api.timeout"));
        }
        restTemplate.setRequestFactory(reqFactory);
        return restTemplate;
    }

  /**
     *
     * @return
     */
    protected boolean isServiceAvailable(){
        ResponseEntity<String> response;
        boolean flag=false;
        try {
            response = restTemplate.getForEntity(getAPIServer(), String.class);

            if(response.getStatusCode().value()==200){
                LOG.info("Able to connect with the server. with domain esb.lla.com");
               flag=true;
            }
        }catch(HttpClientErrorException hex){
            LOG.info("Not Able to connect with the server. with domain esb.lla.com" + hex.getMessage());
            hex.printStackTrace();
            if(hex.getRawStatusCode() == 404)
                flag=false;
        }catch(ResourceAccessException rex){
            LOG.info("Unable to connect: with domain esb.lla.com", rex);
            rex.printStackTrace();
            flag=false;
        }
        return flag;
    }


    /**
     *
     * @return
     */
    protected boolean isServiceAvailableWithIP(){
        ResponseEntity<String> response;
        boolean flag=false;
        try {
            response = restTemplate.getForEntity("https://186.190.241.28", String.class);

            if(response.getStatusCode().value()==200){
                LOG.info("Able to connect with the server. with domain 186.190.241.28");
                flag=true;
            }
        }catch(HttpClientErrorException hex){
            LOG.info("Not Able to connect with the server.with domain 186.190.241.28 "+ hex.getMessage());
            hex.printStackTrace();
            if(hex.getRawStatusCode() == 404)
                flag=false;
        }catch(ResourceAccessException rex){
            LOG.info("Unable to connect: with domain 186.190.241.28" , rex);
            rex.printStackTrace();
            flag=false;
        }
        return flag;
    }
    /**
     *  Validate API Response
     * @param serviceResponse
     * @throws LLAApiException
     */
    protected void validateAPIResponse(final ResponseEntity<String> serviceResponse) {
        if(StringUtils.isNotEmpty(serviceResponse.getBody())){
            final String responseJson=serviceResponse.getBody();
            final JsonObject jsonObject = new JsonParser().parse(responseJson).getAsJsonObject();
            final JsonArray arrObject = jsonObject.getAsJsonArray("errors");
            if(null!=arrObject && !arrObject.isJsonNull()){
                LOG.error("Invalid Response");
            }
        }
    }

    protected String convertToRfc3339TimeStamp(Date date){
        if(null!=date) {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").format(date);
        }
        return "";
    }

    /**
     *
     * @param address
     * @param order
     * @return Formatted Address
     */
    protected String provideAddressText(final AddressModel address, final AbstractOrderModel order){
        String area=StringUtils.isNotEmpty(address.getArea())?address.getArea():StringUtils.EMPTY;
        String neighbourhood=StringUtils.isNotEmpty(address.getNeighbourhood())?address.getNeighbourhood():StringUtils.EMPTY;
        String province=StringUtils.isNotEmpty(address.getProvince())?address.getProvince():StringUtils.EMPTY;
        String houseNumber=StringUtils.isNotEmpty(address.getAppartment())?address.getAppartment():StringUtils.EMPTY;
        String building=StringUtils.isNotEmpty(address.getBuilding())?address.getBuilding():StringUtils.EMPTY;
        String district = StringUtils.isNotEmpty(address.getDistrict())?address.getDistrict():StringUtils.EMPTY;
        String landMark=StringUtils.isNotEmpty(address.getLandMark())?address.getLandMark():StringUtils.EMPTY;
        String streetName = StringUtils.isNotEmpty(address.getStreetname())?address.getStreetname():StringUtils.EMPTY;
        String streetNo = StringUtils.isNotEmpty(address.getStreetnumber())?address.getStreetnumber():StringUtils.EMPTY;
        String streetExt = StringUtils.isNotEmpty(address.getStreetExtension())?address.getStreetExtension():StringUtils.EMPTY;
        String regionName = StringUtils.isNotEmpty(address.getRegionName())?address.getRegionName():StringUtils.EMPTY;
        String line1 = StringUtils.isNotEmpty(address.getLine1())?address.getLine1():StringUtils.EMPTY;
        String line2 = StringUtils.isNotEmpty(address.getLine2())?address.getLine2():StringUtils.EMPTY;
        String town = StringUtils.isNotEmpty(address.getTown())?address.getTown():StringUtils.EMPTY;
        String postalCode = StringUtils.isNotEmpty(address.getPostalcode())?address.getPostalcode():StringUtils.EMPTY;
        String country = Objects.nonNull(address.getCountry()) && StringUtils.isNotEmpty(address.getCountry().getIsocode())?address.getCountry().getIsocode():StringUtils.EMPTY;

        if(llaMulesoftIntegrationUtil.isVtrOrder(order.getSite()))
        {
        	if(StringUtils.isNotEmpty(streetExt)) {
        		return StringUtils.join(new String[]{
                        streetName,streetNo,",",district,",",regionName
                }," ");
        	}
        	else
        	{
        		return StringUtils.join(new String[]{
                        streetName,streetNo,",",streetExt,",",district,",",regionName
                }," ");
        	}
        }
            
        else if(!llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite()))
        return StringUtils.join( new String[]{
                houseNumber, building,
                line1, line2, area, neighbourhood,
                town,district, province, landMark, postalCode,
                country
        }, " ");
        else{
            return StringUtils.join( new String[]{
                    province, building,district ,neighbourhood, landMark}, " ");
        }
    }

    protected HttpHeaders setupCRHeaders(final String siteId) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Cache-Control", "no-cache");
        headers.set("channelId", configurationService.getConfiguration().getString("cabletica.mulesoft.api.channelId"));
        headers.set("client-id", configurationService.getConfiguration().getString("mulesoft.api.clientid"));
        headers.set("client-secret", configurationService.getConfiguration().getString("mulesoft.api.clientsecret"));
        return  headers;
    }

    protected String getDocumentType(final CustomerModel customerData) {
        switch (customerData.getDocumentType()) {
            case "CEDULA":
            case "CEDULANATIONAL":
                return configurationService.getConfiguration().getString("llamulesoftintegration.customer.cedula");
            case "PASSPORT":
                return configurationService.getConfiguration().getString("llamulesoftintegration.customer.passport");
            case "CEDULARESIDENTIAL":
                return configurationService.getConfiguration().getString("llamulesoftintegration.customer.cedula_residential");
        }
        return null;
    }

    protected void saveCurentCustomerStatus(String status, CustomerModel customerData) {
        final String upperCaseStatus=StringUtils.upperCase(status);
        switch (upperCaseStatus) {
            case "ACTIVE":
                customerData.setCustomerStatus(CustomerStatusEnum.ACTIVE);
                break;
            case "INACTIVE":
                customerData.setCustomerStatus(CustomerStatusEnum.INACTIVE);
                break;
            case "PROSPECT":
                customerData.setCustomerStatus(CustomerStatusEnum.PROSPECT);
                break;
            case "SUSPEND":
                customerData.setCustomerStatus(CustomerStatusEnum.SUSPENDED);
                break;
            }
        if (null == customerData.getCustomerStatus()) {
            customerData.setCustomerStatus(CustomerStatusEnum.NEW);
        }
        modelService.save(customerData);
    }

    /**
     * Get Current locale
     * @return  locale
     */
    public Locale getCurrentLocale(final AbstractOrderModel orderModel){
        BaseSiteModel baseSite =orderModel.getSite();
        Locale locale = commonI18NService.getLocaleForLanguage(baseSite.getDefaultLanguage());
        return locale;
    }
}
