package com.lla.mulesoft.integration.util;

import com.google.common.collect.Lists;
import com.lla.core.enums.LLATechnologyEnum;
import com.lla.core.model.LLABundleProductMappingModel;
import com.lla.core.model.LLAProductMapperModel;
import com.lla.core.model.LLAServiceCodeMappingModel;
import com.lla.core.model.VTRServiceCodeMappingModel;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import com.lla.mulesoft.integration.constants.LlamulesoftintegrationConstants;
import com.lla.telcotmfwebservices.v2.dto.Customer;
import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.AddressModel;

import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.*;

public class LLAMulesoftIntegrationUtil {

    private static final Logger LOG = Logger.getLogger(LLAMulesoftIntegrationUtil.class);
    private static final String FIND_SERVICECODE_BY_TECHNOLOGY_AND_BUSINESSUNIT = "SELECT {ls:" + LLAServiceCodeMappingModel.PK
            + "} FROM {" + LLAServiceCodeMappingModel._TYPECODE + " AS ls JOIN " + LLATechnologyEnum._TYPECODE + " AS lt ON {ls:"
            + LLAServiceCodeMappingModel.TECHNOLOGY + "}={lt:" + EnumerationValueModel.PK + "}}" + " WHERE {lt:"
            + EnumerationValueModel.CODE + "}=?technology and {ls:"+ LLAServiceCodeMappingModel.PACKAGEID+"}=?packageId";

    protected static final String PANAMA_SITE_ID = "website.baseSite.uid.panama";
    protected static final String LCPR_SITE_ID = "website.baseSite.uid.puertorico";
    protected static final String VTR_SITE_ID = "website.vtr";

    protected static final String CABLETICA_SITE_ID = "website.baseSite.uid.cabletica";
	private static final Object GET_PRODUCT_CODE_MAPPING = "SELECT {code.pk} FROM {VTRServiceCodeMapping AS code JOIN LLATechnologyEnum AS tech ON {tech.pk} = {code.technology}} WHERE {tech.code} = ?technology";
    @Resource(name = "commerceCategoryService")
    private CommerceCategoryService commerceCategoryService;

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ProductService productService;
    @Autowired
    private CatalogVersionService catalogVersionService;
    @Autowired
    private BaseSiteService baseSiteService;
    @Autowired
    private ConfigurationService configurationService;
    @Autowired
    private FlexibleSearchService flexibleSearchService;

    @Autowired
    private ModelService modelService;

	@Autowired
    private EmailService emailService;

    @Autowired
    private EnumerationService enumerationService;

    @Autowired
    private CommonI18NService commonI18NService;

    public AddressModel fetchAddressForOrderProvisioning(final OrderModel orderModel){
        AddressModel address=null;
        if(isJamaicaOrder(orderModel.getSite())){
      	   address= orderModel.getInstallationAddress();
        }else if(isPanamaOrder(orderModel.getSite())){
            address = orderModel.getDeliveryAddress();
        }
        return address;
    }

    public boolean isCellularOrder(final OrderModel order)
    {
        setupEnv(order);
        for(final AbstractOrderEntryModel entry: order.getEntries()){
            final ProductModel product = productService.getProductForCode(entry.getProduct().getCode());
            boolean isMobileField = containsSecondLevelCategoryWithCode(product, "mobile");
            if(!isMobileField)
                return false;
        }
        return true;
    }

    public String fetchCompanyCode(final OrderModel order)
    {
        Configuration configuration= configurationService.getConfiguration();
        setupEnv(order);
        String response = "";
        String companyCode = "";
        for(final AbstractOrderEntryModel entry: order.getEntries()){
            final ProductModel product = productService.getProductForCode(entry.getProduct().getCode());

            if(containsSecondLevelCategoryWithCode(product, "bundle")){
                companyCode = configuration.getString("llamulesoftintegration.customer.bundleCompanyCode");
                break;
            }else if(containsFirstLevelCategoryWithCode(product, "postpaid")){
                companyCode = configuration.getString("llamulesoftintegration.customer.postpaidCompanyCode");
                break;
            }else if(containsFirstLevelCategoryWithCode(product, "prepaid")){
                companyCode = configuration.getString("llamulesoftintegration.customer.prepaidCompanyCode");
                break;
            }
            if(StringUtils.isNotEmpty(response) && !companyCode.equals(response)) {
                LOG.warn("Since order does not contain products under same category, not sending order category");
                return StringUtils.EMPTY;
            }

        }
        response = companyCode;
        return response;
    }

    public void setupEnv(final OrderModel order){
        catalogVersionService.setSessionCatalogVersion(order.getSite().getUid()+"ProductCatalog","Online");
        catalogVersionService.setSessionCatalogVersion(order.getSite().getUid()+"ProductCatalog","Staged");
        LOG.info(null!=order.getSite()?order.getSite().getUid()+" mapped to order":"No Site Mapped");
        baseSiteService.setCurrentBaseSite(order.getSite(),false);
    }

    public boolean containsFirstLevelCategoryWithCode(final ProductModel product, final String categoryCode)
    {
        final Collection<CategoryModel> allCategories = product.getSupercategories();
        final CategoryModel category = categoryService.getCategoryForCode(categoryCode);
        if (allCategories.contains(category))
        {
            return true;
        }
        return false;
    }
    public boolean containsSecondLevelCategoryWithCode(final ProductModel product, final String categoryCode)
    {
        final Collection<CategoryModel> allCategories = product.getSupercategories();
        final CategoryModel category = categoryService.getCategoryForCode(categoryCode);
        for (final CategoryModel cat : allCategories)
        {
            final Collection<CategoryModel> superCat = cat.getAllSupercategories();
            if (superCat.contains(category))
            {
                return true;
            }
        }
        return false;
    }


    public Boolean isJamaicaOrder(BaseSiteModel baseSiteModel){
        return baseSiteModel.getUid().equalsIgnoreCase(configurationService.getConfiguration().getString("website.jamaica"))?Boolean.TRUE:Boolean.FALSE;
    }

    public Boolean isPanamaOrder(BaseSiteModel baseSiteModel){
        return baseSiteModel.getUid().equalsIgnoreCase(configurationService.getConfiguration().getString(PANAMA_SITE_ID))?Boolean.TRUE:Boolean.FALSE;
    }
    public Boolean isPuertoricoOrder(BaseSiteModel baseSiteModel){
        return baseSiteModel.getUid().equalsIgnoreCase(configurationService.getConfiguration().getString(LCPR_SITE_ID))?Boolean.TRUE:Boolean.FALSE;
    }

    public Boolean isVtrOrder(BaseSiteModel baseSiteModel){
        return baseSiteModel.getUid().equalsIgnoreCase(configurationService.getConfiguration().getString(VTR_SITE_ID))?Boolean.TRUE:Boolean.FALSE;
    }

    public Boolean isCableticaOrder(BaseSiteModel baseSiteModel){
        return baseSiteModel.getUid().equalsIgnoreCase(configurationService.getConfiguration().getString(CABLETICA_SITE_ID))?Boolean.TRUE:Boolean.FALSE;
    }


    public boolean isPrepaidOrder(OrderModel orderModel){
        catalogVersionService.setSessionCatalogVersion(orderModel.getSite().getUid()+"ProductCatalog","Online");
        for(AbstractOrderEntryModel entryModel:orderModel.getEntries()){
        	final ProductModel product = productService.getProductForCode(catalogVersionService.getSessionCatalogVersionForCatalog(orderModel.getSite().getUid()+"ProductCatalog"), entryModel.getProduct().getCode());
            for(CategoryModel categoryModel:product.getSupercategories()){
                if(categoryModel.getCode().equalsIgnoreCase(configurationService.getConfiguration().getString("category.prepaid.name.panama", "prepaid"))){
                    return true;
                }
            }
        }
        return  false;
    }

    public String getProductType(final OrderModel order){
        for(AbstractOrderEntryModel entry : order.getEntries()) {
           final LLAProductMapperModel productMapperModel=entry.getProductMapper();
           if(null!=productMapperModel){
               return productMapperModel.getProductType();
           }
        }
        return StringUtils.EMPTY;
    }


    public LLAServiceCodeMappingModel getTechnologyMapping(final LLABundleProductMappingModel bundleMapping, final OrderModel orderModel){
         final Map<String, String> parameters = new HashMap<>(2);
        parameters.put(LLAServiceCodeMappingModel.TECHNOLOGY, bundleMapping.getTechnology().getCode());
        parameters.put(LLAServiceCodeMappingModel.PACKAGEID, bundleMapping.getPackageId());
        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_SERVICECODE_BY_TECHNOLOGY_AND_BUSINESSUNIT);
        query.addQueryParameters(parameters);
        SearchResult searchResult=flexibleSearchService.search(query);
        List<LLAServiceCodeMappingModel> listServiceCodes=searchResult.getResult();
        if(CollectionUtils.isNotEmpty(searchResult.getResult())){
            List<LLAServiceCodeMappingModel> retrievedList=(List<LLAServiceCodeMappingModel>)searchResult.getResult();
            for(LLAServiceCodeMappingModel item:retrievedList){
                if(item.getBusinessUnit().contains(orderModel.getBusinessUnit())){
                    return item;
                }
            }
        }

        return  null;
    }
    
    public String formatPhoneNumber(String phoneNumber)
    {
        String formattedPhone=   phoneNumber.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3");
        return formattedPhone;
    }

    public String formatSSNNumber(String ssnNumber)
    {
        if(StringUtils.isNotEmpty(ssnNumber))
        {
            String formattedSSNNumber=   new StringBuilder(ssnNumber.substring(0,3)).append("-").append(ssnNumber.substring(3,5)).append("-").append(ssnNumber.substring(5)).toString();
            return formattedSSNNumber;
        }
        return StringUtils.EMPTY;
    }

    public List<VTRServiceCodeMappingModel> getProductCodeMappingForTechnology(final String technology)
 	{
 		validateParameterNotNull(technology, "No technology provided");
 		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_PRODUCT_CODE_MAPPING.toString());
       query.addQueryParameter("technology",technology);
       final SearchResult<VTRServiceCodeMappingModel> productCodeMappingResult=flexibleSearchService.search(query);
       if(null!=productCodeMappingResult && null!=productCodeMappingResult.getResult()){
           final List<VTRServiceCodeMappingModel> itemList= productCodeMappingResult.getResult();
           return CollectionUtils.isNotEmpty(itemList)?itemList:null;
       }
 		return null;
 	}

    
    public List<String> getEmailAddressesOfEmployees(final List<EmployeeModel> employees)
 	{

 		final List<String> emails = new ArrayList<>();

 		for (final EmployeeModel employee : employees)
 		{
 			for (final AddressModel address : Lists.newArrayList(employee.getAddresses()))
 			{
 				if (StringUtils.isNotBlank(address.getEmail()))
 				{
 					final EmailAddressModel emailAddress = emailService.getOrCreateEmailAddressForEmail(address.getEmail(),
 							employee.getName());
 					emails.add(emailAddress.getEmailAddress());
 				}
 			}
 		}
 		return emails;
 	}

 	public List<EmployeeModel> getEmployeesInUserGroup(final String userGroup)
 	{
 		final StringBuilder query = new StringBuilder();
 		query.append(" SELECT 	{e.").append(EmployeeModel.PK).append("} ");
 		query.append(" FROM 		{").append(PrincipalGroupModel._PRINCIPALGROUPRELATION).append(" AS pgr ");
 		query.append(" 	JOIN ").append(UserGroupModel._TYPECODE).append(" AS pg ");
 		query.append(" 		ON {pgr:target} = {pg:pk} ");
 		query.append(" 	JOIN ").append(EmployeeModel._TYPECODE).append("! AS e ");
 		query.append(" 		ON {pgr:source} = {e:pk} ");
 		query.append(" } ");
 		query.append(" WHERE {pg:uid} = ?userGroup ");

 		final Map<String, Object> params = new HashMap<>();
 		params.put("userGroup", userGroup);

 		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString(), params);
 		final SearchResult<EmployeeModel> result = flexibleSearchService.search(searchQuery);
 		return result.getResult();
 	}
     public boolean isValidMainProductPresent(VTRServiceCodeMappingModel mainProduct)
     {
        return Objects.nonNull(mainProduct) && CollectionUtils.isNotEmpty(mainProduct.getDiscountedSeibelProductCode());
     }

    public List<String> vtrOptionalAddonProducts()
    {
        return Arrays.asList(configurationService.getConfiguration().getString("vtr.main.products.include.optional").split(","));

    }

    public TmaSimpleProductOfferingModel getProductForC2C(AbstractOrderModel abstractOrderModel) {
        final CategoryModel category = commerceCategoryService.getCategoryForCode(LlamulesoftintegrationConstants.ADDONS);
        for (final AbstractOrderEntryModel entry : abstractOrderModel.getEntries())
        {
            TmaSimpleProductOfferingModel poSpo=(TmaSimpleProductOfferingModel)productService.getProductForCode(catalogVersionService.getSessionCatalogVersionForCatalog(abstractOrderModel.getSite().getUid()+"ProductCatalog"),entry.getProduct().getCode());
            if (!poSpo.getSupercategories().contains(category))
            {
                return poSpo;
            }
        }
        return null;
    }

    public Locale getCurrentLocale(final AbstractOrderModel orderModel){
        BaseSiteModel baseSite =orderModel.getSite();
        Locale locale = commonI18NService.getLocaleForLanguage(baseSite.getDefaultLanguage());
        return locale;
    }

}
