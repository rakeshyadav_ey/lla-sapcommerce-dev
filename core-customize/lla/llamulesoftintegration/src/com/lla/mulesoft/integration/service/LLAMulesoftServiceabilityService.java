package com.lla.mulesoft.integration.service;

import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.response.LLAApiResponse;

public interface LLAMulesoftServiceabilityService {
    /**
     * Check Location is Servicable for Latitude and Longitude
     * @param longitude
     * @param latitude
     * @return
     * @throws Exception
     */
	boolean checkServiceability(Double longitude, final Double latitude) throws Exception;
    LLAApiResponse checkServiceability(String houseNo) throws Exception;
 }