package com.lla.mulesoft.integration.service.impl;

import com.google.gson.Gson;
import com.lla.core.enums.ClickToCallReasonEnum;
import com.lla.core.enums.DocumentType;
import com.lla.core.enums.DriverLicenceState;
import com.lla.core.model.*;
import com.lla.mulesoft.integration.constants.LlamulesoftintegrationConstants;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import com.lla.telcotmfwebservices.v2.dto.CommunicationMessage;
import com.lla.telcotmfwebservices.v2.dto.CommunicationRequestCharacteristic;
import com.lla.telcotmfwebservices.v2.dto.Receiver;
import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.b2ctelcoservices.model.TmaPoVariantModel;
import de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.subscriptionservices.model.RecurringChargeEntryModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionPricePlanModel;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import org.apache.commons.configuration.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class LLAMulesoftNotificationServiceImpl extends LLAAbstractService implements LLAMulesoftNotificationService {

    public static final String PHONE_CATEGORY_NAME = "phone.category.name";
    public static final String TV_CATEGORY_NAME = "tv.category.name";
    public static final String INTERNET_CATEGORY_NAME = "internet.category.name";
    public static final String PHONE_CATEGORY_CODE = "phone";
    public static final String TV_CATEGORY_CODE = "tv";
    public static final String INTERNET_CATEGORY_CODE = "internet";
    public static final String PLUS = " + ";
    public static final String MONTHLY_PRICE = "price.per.month";
 	public static final String TRIPLE_CATEGORY_CODE = "triple";
 	public static final String INTERNETTV_CATEGORY_CODE = "2PinternetTv";
 	public static final String PHONETV_CATEGORY_CODE = "2PphoneTv";
 	public static final String INTERNETPHONE_CATEGORY_CODE = "2PinternetPhone";
 	public static final String TELEVISION_CATEGORY_CODE = "television";
 	public static final String TELEPHONE_CATEGORY_CODE = "telephonia";
 	public static final String ADDON_CATEGORY_CODE = "addon";
 	public static final String VIDEO="TV";
 	public static final String PHONE="VOICE";
 	public static final String INTERNET="INTERNET";
 	public static final String EMPTY_STRING=" ";
 	public static final String DVR_BOX="DVR_BOX";
 	public static final String SPACE=" ";
	public static final String THREE="3.0";

    @Autowired
    private CatalogVersionService catalogVersionService;
    @Autowired
    private ProductService productService;

    @Autowired
    private BaseSiteService baseSiteService;
    @Autowired
    private UserService userService;

    @Autowired
    private I18NService i18nService;
    @Autowired
    private CommonI18NService commonI18NService;
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;
	
	@Autowired
	private EmailService emailService;

	@Autowired
	private FlexibleSearchService flexibleSearchService;

    @Autowired
    private EnumerationService enumerationService;

    @Override
    public String triggerCSA_Notification(OrderModel order, List<String> mailIds, final String ssnValue)  throws  LLAApiException{
        LOG.info("Generating notification for CSA");
        final String siteID =configurationService.getConfiguration().getString(new StringBuffer(LlamulesoftintegrationConstants.WEBSITE).append(LlamulesoftintegrationConstants.DECIMAL).append(order.getSite().getUid()).append(LlamulesoftintegrationConstants.DECIMAL).append(LlamulesoftintegrationConstants.CODE).toString());
        final String siteNotificationPath=generateEndPointResolver(order,siteID,"CSA");
        URI placeOrder = URI.create(apiUrlConstructor(getAPIServer(),siteID,siteNotificationPath));
        CommunicationMessage message = generateCSARequestBody(order, mailIds, ssnValue);
        return sendNotification(message, placeOrder,siteID);
    }

    @Override
	 public String generateKPINotifications(final AbstractOrderModel cartModel, final String notificationType,
			 final ClickToCallReasonEnum clickToCallReasonEnum) throws LLAApiException
	{
	    LOG.info("Generating KPI notification");
		final String siteID = configurationService.getConfiguration()
		     	.getString(new StringBuffer(LlamulesoftintegrationConstants.WEBSITE).append(LlamulesoftintegrationConstants.DECIMAL)
				.append(cartModel.getSite().getUid()).append(LlamulesoftintegrationConstants.DECIMAL)
				.append(LlamulesoftintegrationConstants.CODE).toString());
		final String endPointResolver = generateKPIEndPointResolver(siteID, notificationType);
		final URI notification_post_url = URI.create(apiUrlConstructor(getAPIServer(), siteID, endPointResolver));
		final CommunicationMessage message = generateNotificationRequestBody(cartModel, clickToCallReasonEnum);
		return sendNotification(message, notification_post_url, siteID);
	 }
   
   @Override
	public String generateFreshdeskFalloutNotifications(final CartModel cartModel, final String notificationType,
			final ClickToCallReasonEnum clickToCallReasonEnum, final String ssnValue, final String houseKey) throws LLAApiException
	{
		LOG.info(String.format("Generating Freshdesk Fallout notification ::: %s" , clickToCallReasonEnum.getCode()));
		final String siteID = configurationService.getConfiguration()
				.getString(new StringBuffer(LlamulesoftintegrationConstants.WEBSITE).append(LlamulesoftintegrationConstants.DECIMAL)
						.append(cartModel.getSite().getUid()).append(LlamulesoftintegrationConstants.DECIMAL)
						.append(LlamulesoftintegrationConstants.CODE).toString());

		final String endPointResolver = generateFalloutEndPointResolver(siteID, notificationType);
		final URI notification_post_url = URI.create(apiUrlConstructor(getAPIServer(), siteID, endPointResolver));
		final CommunicationMessage message = generateFreshdeskFalloutNotificationRequestBody(cartModel, clickToCallReasonEnum,
				ssnValue,houseKey);
		return sendFalloutNotification(message, notification_post_url, siteID);
	}

	/**
	 * Generate end point for fallback API
	 * @param siteID
	 * @param notificationType
	 * @return
	 */
	private String generateFalloutEndPointResolver(final String siteID, final String notificationType)
	{
		String endPointResolver = StringUtils.EMPTY;
		final String createConstant = new StringBuffer("CSA_").append(notificationType).toString();
		switch (siteID)
		{
			case "PR":
				endPointResolver = new StringBuffer("PUERTORICO_").append(createConstant).append("_NOTIFICATION").toString();
				break;
		}
		try
		{
			final Field field = Class.forName("com.lla.mulesoft.integration.constants.LlamulesoftintegrationConstants")
					.getField(endPointResolver);
			field.setAccessible(true);
			endPointResolver = field.get(null).toString();
		}
		catch (NoSuchFieldException | IllegalAccessException e)
		{
			LOG.error(e);
		}
		catch (final ClassNotFoundException e)
		{
			LOG.error(e);
		}
		return endPointResolver;
	}

	 private String generateKPIEndPointResolver(final String siteID, final String notificationType)
	 {
		String endPointResolver = StringUtils.EMPTY;
		final String createConstant = new StringBuffer("CSA_").append(notificationType).toString();
		switch (siteID)
		{
			case "CR":
				endPointResolver = new StringBuffer("CABLETICA_").append(createConstant).append("_NOTIFICATION").toString();
				break;
		}
		try
		{
			final Field field = Class.forName("com.lla.mulesoft.integration.constants.LlamulesoftintegrationConstants")
					.getField(endPointResolver);
			field.setAccessible(true);
			endPointResolver = field.get(null).toString();
		}
		catch (NoSuchFieldException | IllegalAccessException e)
		{
		    e.printStackTrace();
		}
		catch (final ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		return endPointResolver;
	}
	 
	 /**
	  * Generate request body for Fallback API
	 * @param cartModel
	 * @param clickToCallReasonEnum
	 * @param ssnValue
	 * @param houseKey
	  * @return
	 */
	protected CommunicationMessage generateFreshdeskFalloutNotificationRequestBody(final CartModel cartModel,
																				   final ClickToCallReasonEnum clickToCallReasonEnum, final String ssnValue, String houseKey)
		{
			final CustomerModel customer = (CustomerModel) cartModel.getUser();
			final Locale locale = commonI18NService.getLocaleForLanguage(cartModel.getSite().getDefaultLanguage());
			final String reason = enumerationService.getEnumerationName(clickToCallReasonEnum, locale);
			final CommunicationMessage message = new CommunicationMessage();
			message.setSubject(StringUtils.join(new String[]
			{ configurationService.getConfiguration().getString("llamulesoftintegration.freshdesk.fallout.notification.subject"),
					" " + cartModel.getCode() }));
			LOG.info("Email Subject : " + message.getSubject());
			return getFreshdeskFalloutNotificationCommunicationMessage(customer, cartModel, message, reason, ssnValue,houseKey);
		}

	 
		/**
		 * @param customerModel
		 * @param cartModel
		 * @param message
		 * @param reason
		 * @param ssnValue
		 * @param houseKey
		 * @return
		 */
		protected CommunicationMessage getFreshdeskFalloutNotificationCommunicationMessage(final CustomerModel customerModel,
																						   final CartModel cartModel, final CommunicationMessage message, final String reason, final String ssnValue, String houseKey)
		{
			final List<Receiver> recievers = new ArrayList<>();
			final Receiver receiver = new Receiver();
			List<EmployeeModel> csaAgents = new ArrayList<>();

			csaAgents = llaMulesoftIntegrationUtil.getEmployeesInUserGroup("backofficeworkflowadmingroup").stream()
					.filter(employee -> employee.getGroups().contains(userService.getUserGroupForUID("puertorico")))
					.collect(Collectors.toList());

			final List<String> emailsList = llaMulesoftIntegrationUtil.getEmailAddressesOfEmployees(csaAgents);

			receiver.setEmail(StringUtils.join(emailsList.toArray(), ","));
			recievers.add(receiver);
			message.setReceiver(recievers);

			message.setType("email");
			final List<CommunicationRequestCharacteristic> characteristic = new ArrayList<>();

			updateDocumentTypeDetails(customerModel, characteristic);

			updateCustomerNameDetails(customerModel, characteristic,cartModel);

			// ORDER_NO
			if (cartModel.getCode() != null)
			{
				final CommunicationRequestCharacteristic orderNo = new CommunicationRequestCharacteristic();
				orderNo.setName(configurationService.getConfiguration()
						.getString("llamulesoftintegration.freshdesk.fallout.notification.ordernumber"));
				orderNo.setValue(cartModel.getCode());
				characteristic.add(orderNo);
			}

			// AMOUNT
			if (cartModel != null)
			{
				final CommunicationRequestCharacteristic amount = new CommunicationRequestCharacteristic();
				amount.setName(
						configurationService.getConfiguration().getString("llamulesoftintegration.freshdesk.fallout.notification.amount"));
				final Double totalValue = calculateTotalWithTax(cartModel, cartModel.getTotalPrice(), cartModel.getTotalTax());
				amount.setValue(StringUtils.join(new String[]
				{ cartModel.getCurrency().getSymbol(), totalValue.toString() }));
				if (llaMulesoftIntegrationUtil.isCableticaOrder(cartModel.getSite()))
				{
					amount.setValue(StringUtils.join(new String[]
					{ cartModel.getCurrency().getSymbol(), String.valueOf(totalValue.intValue()) }));
				}
				characteristic.add(amount);
			}

			// ORDER_DATE
			updateOrderDateDetails(cartModel, characteristic);

			// ADDRESS
			final CommunicationRequestCharacteristic add = new CommunicationRequestCharacteristic();
			add.setName(
					configurationService.getConfiguration().getString("llamulesoftintegration.freshdesk.fallout.notification.address"));
				final AddressModel deliveryAddress = Objects.nonNull(cartModel.getDeliveryAddress())?cartModel.getDeliveryAddress():new AddressModel();
				add.setValue(provideAddressText(deliveryAddress, cartModel));
				characteristic.add(add);

			// PHONE
			if (StringUtils.isNotEmpty(customerModel.getFixedPhone()))
			{
				final CommunicationRequestCharacteristic phone = new CommunicationRequestCharacteristic();
				phone.setName(
						configurationService.getConfiguration().getString("llamulesoftintegration.freshdesk.fallout.notification.phone"));
				phone.setValue(customerModel.getFixedPhone());
				characteristic.add(phone);
			}
			else {
				final CommunicationRequestCharacteristic phone = new CommunicationRequestCharacteristic();
				phone.setName(
						configurationService.getConfiguration().getString("llamulesoftintegration.freshdesk.fallout.notification.phone"));
				phone.setValue(StringUtils.isNotEmpty(cartModel.getPhoneNumber()) ? cartModel.getPhoneNumber():StringUtils.EMPTY);
				characteristic.add(phone);
			}

			// EMAIL
			if (StringUtils.isNotEmpty(customerModel.getAdditionalEmail()))
			{
				final CommunicationRequestCharacteristic emailId = new CommunicationRequestCharacteristic();
				emailId.setName(
						configurationService.getConfiguration().getString("llamulesoftintegration.freshdesk.fallout.notification.email"));
				emailId.setValue(customerModel.getAdditionalEmail());
				characteristic.add(emailId);
			}
			else{
				final CommunicationRequestCharacteristic emailId = new CommunicationRequestCharacteristic();
				emailId.setName(
						configurationService.getConfiguration().getString("llamulesoftintegration.freshdesk.fallout.notification.email"));
				emailId.setValue(StringUtils.isNotEmpty(cartModel.getCustomerEmail())?cartModel.getCustomerEmail(): StringUtils.EMPTY);
				characteristic.add(emailId);
			}

			// CONTRACT_STATUS

			updateContractStatusDetails(cartModel, characteristic);

			// DOB
			if (customerModel.getDateOfBirth() != null)
			{
				final CommunicationRequestCharacteristic dob = new CommunicationRequestCharacteristic();
				dob.setName(
						configurationService.getConfiguration().getString("llamulesoftintegration.freshdesk.fallout.notification.dob"));
				dob.setValue(StringUtils.isNotEmpty(String.valueOf(customerModel.getDateOfBirth()))?new SimpleDateFormat("dd/MM/yyyy").format(customerModel.getDateOfBirth()):StringUtils.EMPTY);
				characteristic.add(dob);
			}

			// CHECKOUT_STEP
			if (reason != null && StringUtils.isNotEmpty(reason))
			{
				final CommunicationRequestCharacteristic checkoutStep = new CommunicationRequestCharacteristic();
				checkoutStep.setName(configurationService.getConfiguration()
						.getString("llamulesoftintegration.freshdesk.fallout.notification.checkoutstep"));
				checkoutStep.setValue(reason);
				characteristic.add(checkoutStep);
			}

			// SSN_CHECK

			updateSSNDetails(customerModel, cartModel, reason, ssnValue, characteristic);
			//house key

			updateHouseDetails(cartModel, houseKey, characteristic);

			// DEBT_CHECK

			updateDebtCheckDetails(ssnValue,cartModel, reason, characteristic);
			List<String> allCategories = new ArrayList<>();
			final CommunicationRequestCharacteristic packRequested = new CommunicationRequestCharacteristic();
			packRequested.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.packrequested"));
           //Pack Amount
			final CommunicationRequestCharacteristic packAmount = new CommunicationRequestCharacteristic();
			packAmount.setName(StringUtils.join(new String[] {
					configurationService.getConfiguration().getString("llamulesoftintegration.notification.packamount") }));
			packAmount.setValue(StringUtils.join(new String[]{cartModel.getCurrency().getSymbol(),(null != ((AbstractOrderModel) cartModel).getBundlePrice())?String.valueOf(((AbstractOrderModel)cartModel).getBundlePrice().doubleValue()):Double.valueOf(0.0).toString()}));
			characteristic.add(packAmount);

			// PRODUCT_NAME1
			int i = 0;
			for (final AbstractOrderEntryModel entry : cartModel.getEntries())
			{
				final Collection<CategoryModel> supercategoriesList = productService.getProductForCode(entry.getProduct().getCode())
						.getSupercategories();
				for (final CategoryModel categoryModel : supercategoriesList)
				{

					if(!categoryModel.getCode().contains("addon") && !categoryModel.getCode().contains("smartproduct")) {
						allCategories.add(StringUtils.capitalize(categoryModel.getCode()));

				/*	final CommunicationRequestCharacteristic productSuperCategoryName = new CommunicationRequestCharacteristic();
					productSuperCategoryName.setName(StringUtils.join(new String[]{configurationService.getConfiguration().getString("llamulesoftintegration.freshdesk.fallout.notification.productSuperCategoryName"), String.valueOf(i + 1)}));
					productSuperCategoryName.setValue(categoryModel.getCode());*/
						final ProductModel product = productService.getProductForCode(entry.getProduct().getCode());
						final CommunicationRequestCharacteristic productName = new CommunicationRequestCharacteristic();
						productName.setName(StringUtils.join(new String[]{configurationService.getConfiguration().getString("llamulesoftintegration.freshdesk.fallout.notification.productname"), String.valueOf(i + 1)}));
						productName.setValue(product.getName());
						//characteristic.add(productSuperCategoryName);
						characteristic.add(productName);
						i++;
						break;
					}
				}
			}
			String packRequestedValue = String.join(PLUS, allCategories);
			packRequested.setValue(packRequestedValue);
			characteristic.add(packRequested);
			generateLCPRAddonEntries(cartModel,characteristic,1);
			if(llaMulesoftIntegrationUtil.isPuertoricoOrder(cartModel.getSite())) {
				//video
				updateActiveServiceDetails(characteristic, cartModel.getServiceQualResultRequest());
				addPackAmountDetails(cartModel, characteristic);
				addAdditionalCustomerDetails(customerModel,characteristic);

			}
			message.setCharacteristic(characteristic);
			return message;
		}

	private void addPackAmountDetails(AbstractOrderModel cartModel, List<CommunicationRequestCharacteristic> characteristic)
	{

			final CommunicationRequestCharacteristic installationFee = new CommunicationRequestCharacteristic();
			installationFee.setName(StringUtils.join(new String[] { configurationService.getConfiguration()
					.getString("llamulesoftintegration.notification.installationfee") }));
		if (null != ((AbstractOrderModel) cartModel).getCreditScoreLevel()) {
			CreditScoreModel creditScoreModel=(cartModel).getCreditScoreLevel();
			installationFee
					.setValue(StringUtils.join(new String[]{cartModel.getCurrency().getSymbol(),null!=creditScoreModel && null!=creditScoreModel.getInstallationFee() ?String.valueOf(creditScoreModel.getInstallationFee().doubleValue()):Double.valueOf(0.0).toString()}));
		}
		else
			installationFee.setValue(StringUtils.join(new String[]{cartModel.getCurrency().getSymbol(),Double.valueOf(0.0).toString()}));
		characteristic.add(installationFee);

		final CommunicationRequestCharacteristic packAmountDueToday = new CommunicationRequestCharacteristic();
		packAmountDueToday.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.pack.amount.today"));
		packAmountDueToday.setValue(StringUtils.join(new String[]{cartModel.getCurrency().getSymbol(), null!=cartModel.getPackAmountDueToday() ?String.valueOf(cartModel.getPackAmountDueToday().doubleValue()):Double.valueOf(0.0).toString()}));
		characteristic.add(packAmountDueToday);

		final CommunicationRequestCharacteristic packAmountDueFirstBill = new CommunicationRequestCharacteristic();
		packAmountDueFirstBill.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.pack.amount.first.bill"));
		packAmountDueFirstBill.setValue(StringUtils.join(new String[] { cartModel.getCurrency().getSymbol(), null != cartModel.getPackAmountDueFirstBill() ? String.valueOf(cartModel.getPackAmountDueFirstBill().doubleValue()) : Double.valueOf(0.0).toString() }));
		   characteristic.add(packAmountDueFirstBill);

		CreditScoreModel creditScoreModel = cartModel.getCreditScoreLevel();

		final CommunicationRequestCharacteristic dvrDeposit = new CommunicationRequestCharacteristic();
		dvrDeposit.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.dvr.deposit"));
		if(Objects.nonNull(creditScoreModel))
		{
			dvrDeposit.setValue(StringUtils.join(new String[]{cartModel.getCurrency().getSymbol(),null!=creditScoreModel.getDvrDeposit()?creditScoreModel.getDvrDeposit().toString():Double.valueOf(0.0).toString()}));
		}
		else

			dvrDeposit.setValue(StringUtils.join(new  String[]{cartModel.getCurrency().getSymbol(),Double.valueOf(0.0).toString()}));
		characteristic.add(dvrDeposit);

		final CommunicationRequestCharacteristic oneTimeCharges = new CommunicationRequestCharacteristic();
		oneTimeCharges.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.one.time.charges"));
		if(Objects.nonNull(creditScoreModel))
		{
			oneTimeCharges.setValue(StringUtils.join(new String[]{cartModel.getCurrency().getSymbol(),null!=creditScoreModel.getOnetimeCharges()?creditScoreModel.getOnetimeCharges().toString():Double.valueOf(0.0).toString()}));
		}
		else
			oneTimeCharges.setValue(StringUtils.join(new String[]{cartModel.getCurrency().getSymbol(),Double.valueOf(0.0).toString()}));
		characteristic.add(oneTimeCharges);
	}

	private void updateActiveServiceDetails(List<CommunicationRequestCharacteristic> characteristic, List<ServiceQualificationResultModel> serviceQualResultRequest) {
		final CommunicationRequestCharacteristic serviceVideo = new CommunicationRequestCharacteristic();
		serviceVideo.setName(StringUtils.join(new String[]{configurationService.getConfiguration()
				.getString("llamulesoftintegration.notification.servicevideo")}));
		String serviceVideoValue = EMPTY_STRING;
		if (CollectionUtils.isNotEmpty(serviceQualResultRequest) && Objects.nonNull(serviceQualResultRequest.get(0))) {
			List<ServiceQualificationItemModel> item = serviceQualResultRequest.get(0).getServiceQualificationItems();
			if (CollectionUtils.isNotEmpty(item)) {
				serviceVideoValue = addServices(item.get(0), VIDEO);
			}

		}
		serviceVideo.setValue(serviceVideoValue);
		characteristic.add(serviceVideo);

		//internet
		final CommunicationRequestCharacteristic serviceHsd = new CommunicationRequestCharacteristic();
		serviceHsd.setName(StringUtils.join(new String[]{
				configurationService.getConfiguration().getString("llamulesoftintegration.notification.servicehsd")}));
		String serviceInternetValue = EMPTY_STRING;
		if (CollectionUtils.isNotEmpty(serviceQualResultRequest) && Objects.nonNull(serviceQualResultRequest.get(0))) {
			List<ServiceQualificationItemModel> item = serviceQualResultRequest.get(0).getServiceQualificationItems();
			if (CollectionUtils.isNotEmpty(item)) {
				serviceInternetValue = addServices(item.get(0), INTERNET);
			}

		}
		serviceHsd.setValue(serviceInternetValue);
		characteristic.add(serviceHsd);

		//phone
		final CommunicationRequestCharacteristic serviceVoice = new CommunicationRequestCharacteristic();
		serviceVoice.setName(StringUtils.join(new String[]{configurationService.getConfiguration()
				.getString("llamulesoftintegration.notification.servicevoice")}));
		String servicePhoneValue = EMPTY_STRING;
		if (CollectionUtils.isNotEmpty(serviceQualResultRequest) && Objects.nonNull(serviceQualResultRequest.get(0))) {
			List<ServiceQualificationItemModel> item = serviceQualResultRequest.get(0).getServiceQualificationItems();
			if (CollectionUtils.isNotEmpty(item)) {
				servicePhoneValue = addServices(item.get(0), PHONE);



			}

		}
		serviceVoice.setValue(servicePhoneValue);
		characteristic.add(serviceVoice);
	}

	private void updateDocumentTypeDetails(CustomerModel customerModel, List<CommunicationRequestCharacteristic> characteristic) {
		if (StringUtils.isNotEmpty(customerModel.getDocumentType()) &&!customerModel.getDocumentType().equalsIgnoreCase(DocumentType.OTHER.getCode()))
		{
			final CommunicationRequestCharacteristic idType = new CommunicationRequestCharacteristic();
			idType.setName(
					configurationService.getConfiguration().getString("llamulesoftintegration.freshdesk.fallout.notification.idType"));
			idType.setValue(enumerationService.getEnumerationName(enumerationService.getEnumerationValue(DocumentType.class, customerModel.getDocumentType()), new Locale("es")));
			characteristic.add(idType);

			final CommunicationRequestCharacteristic idNumber = new CommunicationRequestCharacteristic();
			idNumber.setName(configurationService.getConfiguration()
					.getString("llamulesoftintegration.freshdesk.fallout.notification.idNumber"));
			idNumber.setValue(customerModel.getDocumentNumber());
			characteristic.add(idNumber);

			final CommunicationRequestCharacteristic driverLicenseState = new CommunicationRequestCharacteristic();
			driverLicenseState.setName(configurationService.getConfiguration()
					.getString("llamulesoftintegration.notification.driver.license.state"));
			driverLicenseState.setValue(StringUtils.isNotEmpty(customerModel.getDriverLicenceState())?
					enumerationService.getEnumerationName(enumerationService.getEnumerationValue(DriverLicenceState.class,
							customerModel.getDriverLicenceState()), new Locale("es")) : Strings.EMPTY);
			characteristic.add(driverLicenseState);
		}
	}

	private void updateCustomerNameDetails(CustomerModel customerModel, List<CommunicationRequestCharacteristic> characteristic,final CartModel cartModel) {

			String customerFirstName = null;
			String customerLastName = null;

		if(Objects.nonNull(cartModel)&& StringUtils.isNotEmpty(cartModel.getName()))
		{
            if(StringUtils.containsWhitespace(cartModel.getName())){
	            String names[]=cartModel.getName().split(SPACE);
	            customerFirstName=names[0];
	            customerLastName=names[1];
            }
			else{
	            customerFirstName=cartModel.getName();
	            customerLastName=EMPTY_STRING;
            }
		}

			// CUSTOMER_NAME
		if (StringUtils.isNotEmpty(customerModel.getFirstName()) && StringUtils.isNotEmpty(customerModel.getLastName()))
		{
			final CommunicationRequestCharacteristic name = new CommunicationRequestCharacteristic();
			name.setName(configurationService.getConfiguration()
					.getString("llamulesoftintegration.freshdesk.fallout.notification.customername"));
			name.setValue(StringUtils.join(new String[]
			{ customerModel.getFirstName(), " ", customerModel.getLastName() }));
			characteristic.add(name);
		}
		// FIRST_NAME
		if (StringUtils.isNotEmpty(customerModel.getFirstName()))
		{
			final CommunicationRequestCharacteristic firstName = new CommunicationRequestCharacteristic();
			firstName.setName(configurationService.getConfiguration()
					.getString("llamulesoftintegration.freshdesk.fallout.notification.firstname"));
			firstName.setValue(customerModel.getFirstName());
			characteristic.add(firstName);
		}
		else {
			final CommunicationRequestCharacteristic firstName = new CommunicationRequestCharacteristic();
			firstName.setName(configurationService.getConfiguration()
					.getString("llamulesoftintegration.freshdesk.fallout.notification.firstname"));
			firstName.setValue(StringUtils.isNotEmpty(customerFirstName)?customerFirstName:StringUtils.EMPTY);
			characteristic.add(firstName);
		}

		// LAST_NAME
		if (StringUtils.isNotEmpty(customerModel.getLastName()))
		{
			final CommunicationRequestCharacteristic lastName = new CommunicationRequestCharacteristic();
			lastName.setName(configurationService.getConfiguration()
					.getString("llamulesoftintegration.freshdesk.fallout.notification.lastname"));
			lastName.setValue(customerModel.getLastName());
			characteristic.add(lastName);
		}
		else {
			final CommunicationRequestCharacteristic lastName = new CommunicationRequestCharacteristic();
			lastName.setName(configurationService.getConfiguration()
					.getString("llamulesoftintegration.freshdesk.fallout.notification.lastname"));
			lastName.setValue(StringUtils.isNotEmpty(customerLastName)?customerLastName:StringUtils.EMPTY);
			characteristic.add(lastName);
		}
	}

	private void updateOrderDateDetails(CartModel cartModel, List<CommunicationRequestCharacteristic> characteristic) {
		if (cartModel.getDate() != null)
		{
			final CommunicationRequestCharacteristic orderDate = new CommunicationRequestCharacteristic();
			orderDate.setName(configurationService.getConfiguration()
					.getString("llamulesoftintegration.freshdesk.fallout.notification.orderdate"));
			String dateFormat = "MM/dd/yyyy";
			if (llaMulesoftIntegrationUtil.isCableticaOrder(cartModel.getSite()))
			{
				dateFormat = "dd/MM/yyyy";
			}
			orderDate.setValue(new SimpleDateFormat(dateFormat).format(cartModel.getDate()));
			characteristic.add(orderDate);
		}
	}

	private void updateContractStatusDetails(CartModel cartModel, List<CommunicationRequestCharacteristic> characteristic) {
		final CommunicationRequestCharacteristic contractDetails = new CommunicationRequestCharacteristic();
		contractDetails.setName(configurationService.getConfiguration()
				.getString("llamulesoftintegration.freshdesk.fallout.notification.contractstatus"));
		if (cartModel.getContractType() != null)
		{
			contractDetails.setValue(enumerationService.getEnumerationName(cartModel.getContractType(), new Locale("es")));
		}
		else
		{
			contractDetails.setValue(EMPTY_STRING);
		}
		characteristic.add(contractDetails);
	}

	private void updateDebtCheckDetails(final String ssnValue,CartModel cartModel, String reason, List<CommunicationRequestCharacteristic> characteristic) {
		final CommunicationRequestCharacteristic debtCheck = new CommunicationRequestCharacteristic();
		debtCheck.setName(StringUtils.join(new String[]
		{ configurationService.getConfiguration().getString("llamulesoftintegration.notification.debtcheck") }));
		if(reason.equalsIgnoreCase(enumerationService.getEnumerationName(ClickToCallReasonEnum.UNAVAILABLE_NORMALIZED_ADDRESS, getCurrentLocale(cartModel))) || reason.equalsIgnoreCase(enumerationService.getEnumerationName(ClickToCallReasonEnum.NOT_SERVICEABLE, getCurrentLocale(cartModel))) || StringUtils.isEmpty(ssnValue))
		{
			debtCheck.setValue(EMPTY_STRING);
		}
		  else if (((AbstractOrderModel) cartModel).getDebtCheckFlag())
		{
			debtCheck.setValue(
					configurationService.getConfiguration().getString("llamulesoftintegration.notification.debtcheck.no.name"));
		}
		else
		{
			debtCheck.setValue(
					configurationService.getConfiguration().getString("llamulesoftintegration.notification.debtcheck.yes.name"));
		}
		characteristic.add(debtCheck);
	}

	private void updateHouseDetails(CartModel cartModel, String houseKey, List<CommunicationRequestCharacteristic> characteristic) {
		final CommunicationRequestCharacteristic houseNo = new CommunicationRequestCharacteristic();
		houseNo.setName(configurationService.getConfiguration()
				.getString("llamulesoftintegration.freshdesk.fallout.notification.houseKey"));
		if(StringUtils.isNotEmpty(houseKey)|| (Objects.nonNull(cartModel.getSelectedAddress()) && StringUtils.isNotEmpty(cartModel.getSelectedAddress().getHouseNo())))
		{
		   houseNo.setValue(StringUtils.isNotEmpty(houseKey)? houseKey : cartModel.getSelectedAddress().getHouseNo());

	   }
		else
			houseNo.setValue(EMPTY_STRING);
		characteristic.add(houseNo);
	}

	private void updateSSNDetails(CustomerModel customerModel, CartModel cartModel, String reason, String ssnValue, List<CommunicationRequestCharacteristic> characteristic) {
		final CommunicationRequestCharacteristic ssnCheck = new CommunicationRequestCharacteristic();

		if(reason.equalsIgnoreCase(enumerationService.getEnumerationName(ClickToCallReasonEnum.UNAVAILABLE_NORMALIZED_ADDRESS, getCurrentLocale(cartModel))) || reason.equalsIgnoreCase(enumerationService.getEnumerationName(ClickToCallReasonEnum.NOT_SERVICEABLE, getCurrentLocale(cartModel))))
		{
			ssnCheck.setName(configurationService.getConfiguration()
					.getString("llamulesoftintegration.freshdesk.fallout.notification.ssncheck"));
			ssnCheck.setValue(EMPTY_STRING);
			characteristic.add(ssnCheck);

		}
		else if ((ssnValue != null && StringUtils.isNotEmpty(ssnValue)) ||  StringUtils.isNotEmpty(customerModel.getCustomerSSN()))
		{

			ssnCheck.setName(configurationService.getConfiguration()
					.getString("llamulesoftintegration.freshdesk.fallout.notification.ssncheck"));
			ssnCheck.setValue(
					configurationService.getConfiguration().getString("llamulesoftintegration.notification.ssncheck.yes.name"));
			characteristic.add(ssnCheck);

			final CommunicationRequestCharacteristic ssn = new CommunicationRequestCharacteristic();

			ssn.setName(
					configurationService.getConfiguration().getString("llamulesoftintegration.freshdesk.fallout.notification.ssnid"));
			ssn.setValue(StringUtils.isNotEmpty(customerModel.getCustomerSSN())?llaMulesoftIntegrationUtil.formatSSNNumber(customerModel.getCustomerSSN()): ssnValue);
			characteristic.add(ssn);
		}
		else
		{
			ssnCheck.setName(configurationService.getConfiguration()
					.getString("llamulesoftintegration.freshdesk.fallout.notification.ssncheck"));
			ssnCheck.setValue(
					configurationService.getConfiguration().getString("llamulesoftintegration.notification.ssncheck.no.name"));
			characteristic.add(ssnCheck);
		}
	}


	private String sendFalloutNotification(final CommunicationMessage message, final URI uri, final String siteID)
				throws LLAApiException
		{
			final String postBody = new Gson().toJson(message);
			LOG.info(String.format("Request ::: %s", postBody));
			final HttpHeaders standardHeaders = setupHeaders(siteID);
			standardHeaders.set("channelId", "ecom");
			standardHeaders.set("Content-Type", "application/json");
			final HttpEntity<String> httpEntity = new HttpEntity<>(postBody, standardHeaders);
			ResponseEntity<String> serviceResponse = null;
			try
			{
				serviceResponse = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, String.class);
				if (serviceResponse.getStatusCode() == HttpStatus.OK)
				{
					LOG.info("Email notification sent!");
					return serviceResponse.getBody();
				}
				else
				{
					LOG.error("Error in sending Notification.Server HTTP response: " + serviceResponse.getStatusCodeValue());
					LOG.error("Error Response " + serviceResponse.getBody());
					return StringUtils.EMPTY;
				}

			}
			catch (final HttpClientErrorException.BadRequest badRequest)
			{
				LOG.error(badRequest.getResponseBodyAsString());
			}
			catch (final HttpClientErrorException clientEx)
			{
				LOG.error(clientEx.getMessage());
			}
			catch (final ResourceAccessException ioException)
			{
				LOG.error("Exception in making Api Call", ioException);
				throw new LLAApiException("Exception in making Api Call");
			}
			return StringUtils.EMPTY;
		}

	protected CommunicationMessage generateNotificationRequestBody(final AbstractOrderModel cartModel,
			final ClickToCallReasonEnum clickToCallReasonEnum)
	{
		final CustomerModel customer = (CustomerModel) cartModel.getUser();
		String reason = enumerationService.getEnumerationName(clickToCallReasonEnum, getCurrentLocale(cartModel));
		final CommunicationMessage message = new CommunicationMessage();
		message.setSubject(new StringBuffer(configurationService.getConfiguration().getString("llamulesoftintegration.KPI.es.notification.subject")).append(" ").append(cartModel.getCode()).append("_").append(reason).toString());
		LOG.info("Subject ::: "+ message.getSubject());
		return getNotificationCommunicationMessage(customer, cartModel, message);
	}

	protected CommunicationMessage getNotificationCommunicationMessage(final CustomerModel customerModel,
			final AbstractOrderModel cartModel, final CommunicationMessage message)
	{
		final List<Receiver> recievers = new ArrayList<>();
		final Receiver receiver = new Receiver();
		List<EmployeeModel> csaAgents = new ArrayList<>();

		csaAgents = llaMulesoftIntegrationUtil.getEmployeesInUserGroup("backofficeworkflowadmingroup").stream()
				.filter(employee -> employee.getGroups().contains(userService.getUserGroupForUID("cabletica")))
				.collect(Collectors.toList());

		final List<String> emailsList = llaMulesoftIntegrationUtil.getEmailAddressesOfEmployees(csaAgents);

		receiver.setEmail(StringUtils.join(emailsList.toArray(), ","));
		recievers.add(receiver);
		message.setReceiver(recievers);

		message.setType("email");
		final List<CommunicationRequestCharacteristic> characteristic = new ArrayList<>();
		final CommunicationRequestCharacteristic cartID = new CommunicationRequestCharacteristic();
		cartID.setName(configurationService.getConfiguration().getString("llamulesoftintegration.KPI.notification.cartID"));
		cartID.setValue(cartModel.getCode());
		characteristic.add(cartID);

		final CommunicationRequestCharacteristic idType = new CommunicationRequestCharacteristic();
		idType.setName(configurationService.getConfiguration().getString("llamulesoftintegration.KPI.notification.idType"));
		idType.setValue(enumerationService.getEnumerationName(enumerationService.getEnumerationValue(DocumentType.class, customerModel.getDocumentType()),getCurrentLocale(cartModel)));
		characteristic.add(idType);

		final CommunicationRequestCharacteristic idNumber = new CommunicationRequestCharacteristic();
		idNumber.setName(configurationService.getConfiguration().getString("llamulesoftintegration.KPI.notification.idNumber"));
		idNumber.setValue(customerModel.getDocumentNumber());
		characteristic.add(idNumber);

		final CommunicationRequestCharacteristic phone = new CommunicationRequestCharacteristic();
		phone.setName(configurationService.getConfiguration().getString("llamulesoftintegration.KPI.notification.phone"));
		phone.setValue(customerModel.getMobilePhone());
		characteristic.add(phone);

		final CommunicationRequestCharacteristic email = new CommunicationRequestCharacteristic();
		email.setName(configurationService.getConfiguration().getString("llamulesoftintegration.KPI.notification.email"));
		email.setValue(customerModel.getContactEmail());
		characteristic.add(email);

		final CommunicationRequestCharacteristic amount = new CommunicationRequestCharacteristic();
		amount.setName(configurationService.getConfiguration().getString("llamulesoftintegration.KPI.notification.amount"));
		amount.setValue(cartModel.getTotalPrice().toString());
		characteristic.add(amount);

		int i = 0;
		for (final AbstractOrderEntryModel entry : cartModel.getEntries())
		{
			final List<String> categoryList = new ArrayList();
			final CommunicationRequestCharacteristic productSuperCategoryName = new CommunicationRequestCharacteristic();
			productSuperCategoryName.setName(configurationService.getConfiguration()
					.getString("llamulesoftintegration.KPI.notification.productSuperCategoryName")+ (i + 1));

			final Collection<CategoryModel> supercategoriesList = productService.getProductForCode(entry.getProduct().getCode())
					.getSupercategories();
			for (final CategoryModel categoryModel : supercategoriesList)
			{
				productSuperCategoryName.setValue(categoryModel.getName(getCurrentLocale(cartModel)));

				final ProductModel product = productService.getProductForCode(entry.getProduct().getCode());

				final CommunicationRequestCharacteristic productName = new CommunicationRequestCharacteristic();

				productName.setName(configurationService.getConfiguration()
						.getString("llamulesoftintegration.KPI.notification.productName") + (i + 1));

				productName.setValue(product.getName(getCurrentLocale(cartModel)));

				characteristic.add(productSuperCategoryName);
				characteristic.add(productName);
				i++;
				break;
			}
		}

		message.setCharacteristic(characteristic);
		return message;

	 }	

    @Override
    public String generateOrderCancelNotification(OrderModel order)  throws  LLAApiException {
        LOG.info("Generating order cancel notification");
        final String siteID =configurationService.getConfiguration().getString(new StringBuffer(LlamulesoftintegrationConstants.WEBSITE).append(LlamulesoftintegrationConstants.DECIMAL).append(order.getSite().getUid()).append(LlamulesoftintegrationConstants.DECIMAL).append(LlamulesoftintegrationConstants.CODE).toString());
        String endPointResolver= generateEndPointResolver(order, siteID, "CANCEL");
        URI notification_post_url = URI.create(apiUrlConstructor(   getAPIServer(),siteID,endPointResolver));
        CommunicationMessage message = generateCancelRequestBody(order);
        return sendNotification(message, notification_post_url, siteID);
    }

    private String generateEndPointResolver(OrderModel order, String siteID, String notificationType) {
        String endPointResolver=StringUtils.EMPTY;
        String createConstant=new StringBuffer(notificationType).append("_ORDER_NOTIFICATION").toString();
        switch (siteID){
            case "JM":
                endPointResolver =createConstant;
                break;
            case"PA":
                 if(llaMulesoftIntegrationUtil.isPrepaidOrder(order)){
                     endPointResolver = new StringBuffer("PANAMA_PREPAID_").append(createConstant).append(notificationType.equalsIgnoreCase("CSA")?StringUtils.EMPTY:"_"+order.getLanguage().getIsocode().toUpperCase()).toString();
                 }else{
                     endPointResolver = new StringBuffer("PANAMA_").append(createConstant).append(notificationType.equalsIgnoreCase("CSA")?StringUtils.EMPTY:"_"+order.getLanguage().getIsocode().toUpperCase()).toString();
                 }

                 break;

            case "PR":
	            if((Objects.nonNull(order.getFalloutReasonValue()) || BooleanUtils.isTrue(order.getIsRequiredServiceActive())) && !notificationType.equalsIgnoreCase("CSA"))
	            {
		            endPointResolver=new StringBuffer("PUERTORICO_").append(createConstant).append("_FALLBACK_").append(order.getLanguage().getIsocode().toUpperCase()).toString();
	            }else {
		            endPointResolver = new StringBuffer("PUERTORICO_").append(createConstant)
				            .append(notificationType.equalsIgnoreCase("CSA") ? StringUtils.EMPTY : "_" + order.getLanguage().getIsocode().toUpperCase()).toString();
	            }
            	break;
			case "CL":
				endPointResolver = new StringBuffer("VTR_").append(createConstant)
						.append(notificationType.equalsIgnoreCase("CSA") ? StringUtils.EMPTY
								: "_" + order.getLanguage().getIsocode().toUpperCase())
						.toString();
				break;
            case "CR":
                endPointResolver=new StringBuffer("CABLETICA_").append(createConstant).append(notificationType.equalsIgnoreCase("CSA")?StringUtils.EMPTY:"_"+order.getLanguage().getIsocode().toUpperCase()).toString();
                break;
        }
        try {
            Field field = Class.forName("com.lla.mulesoft.integration.constants.LlamulesoftintegrationConstants").getField(endPointResolver);
            field.setAccessible(true);
            endPointResolver=field.get(null).toString();

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return endPointResolver;
    }


    @Override
    public String sendOrderProvisionMail(OrderModel order)  throws  LLAApiException {
        LOG.info("Generating order released notification");
        final String siteID =configurationService.getConfiguration().getString(new StringBuffer(LlamulesoftintegrationConstants.WEBSITE).append(LlamulesoftintegrationConstants.DECIMAL).append(order.getSite().getUid()).append(LlamulesoftintegrationConstants.DECIMAL).append(LlamulesoftintegrationConstants.CODE).toString());
        final String endPointResolver=generateEndPointResolver(order,siteID,"RELEASE");
        URI notification_post_url = URI.create(apiUrlConstructor(   getAPIServer(),siteID,endPointResolver));
        CommunicationMessage message = generateProvisionRequestBody(order);
        return sendNotification(message, notification_post_url, siteID);
    }


    @Override
    public String generateOrderNotification(OrderModel orderModel) throws  LLAApiException{
        
        int i=0;
        int attemptCount=configurationService.getConfiguration().getInt("test.retries.attempt");
        while(i<attemptCount-18){
            isServiceAvailable();
            isServiceAvailableWithIP();
            try {
                Thread.sleep(configurationService.getConfiguration().getLong("test.retries.sleep.time"));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
        }
        LOG.info("Generating order notification");
        final String siteID =configurationService.getConfiguration().getString(new StringBuffer(LlamulesoftintegrationConstants.WEBSITE).append(LlamulesoftintegrationConstants.DECIMAL).append(orderModel.getSite().getUid()).append(LlamulesoftintegrationConstants.DECIMAL).append(LlamulesoftintegrationConstants.CODE).toString());
        final String endPointResolver=generateEndPointResolver(orderModel,siteID,"PLACE");
        URI notification_post_url = URI.create(apiUrlConstructor(getAPIServer(),siteID,endPointResolver));
        CommunicationMessage message = generateRequestBody(orderModel);
        return sendNotification(message, notification_post_url, siteID);
    }

    /**
     * Send Notification Call to Agent or Customer
     * @param message
     * @param uri
     * @param siteID
     * @return
     * @throws LLAApiException
     */
    private String sendNotification(CommunicationMessage message, URI uri, String siteID) throws LLAApiException
    {
        String postBody = new Gson().toJson(message);
        LOG.info(String.format("Request ::: %s",postBody));
        HttpHeaders standardHeaders =  setupHeaders(siteID);
        standardHeaders.set("channelId", "ecom");
		standardHeaders.set("Content-Type", "application/json");
        HttpEntity<String> httpEntity = new HttpEntity<>(postBody, standardHeaders);
        RestTemplate restTemplate_new=this.createRestTemplate(this.createRequestFactory(configurationService.getConfiguration().getString("max.pool.connection")));

        ResponseEntity<String> serviceResponse=null;
        try{
            //setMuleSoftConnectionTimeout(restTemplate_new);
            serviceResponse= restTemplate_new.exchange(uri, HttpMethod.POST,httpEntity,String.class);
            if(serviceResponse.getStatusCode()== HttpStatus.OK){
                 LOG.info("Email notification sent!");
                 return serviceResponse.getBody();
            }else{
                LOG.error("Error in sending Notification.Server HTTP response: "+serviceResponse.getStatusCodeValue());
                LOG.error("Error Response "+ serviceResponse.getBody());
                return StringUtils.EMPTY;
            }

        }catch(HttpClientErrorException.BadRequest badRequest){
            LOG.error(badRequest.getResponseBodyAsString());
            LOG.error("BadRequest in making Api Call",badRequest);
        }catch(HttpClientErrorException clientEx){
            LOG.error(clientEx.getMessage());
            LOG.error("HttpClientErrorException in making Api Call",clientEx);
        }catch(ResourceAccessException ioException){
            LOG.error("Exception in making Api Call",ioException);
            throw new LLAApiException("Exception in making Api Call");
        }
        catch (final HttpServerErrorException.BadGateway clientEx)
        {
            LOG.error(String.format("KPI Notification failed due to bad Gateway  :: %s and Code %s", clientEx.getMessage(),
                    clientEx.getStatusCode()));
            return StringUtils.EMPTY;
        }
        catch (final HttpServerErrorException.InternalServerError clientEx)
        {
            LOG.error(
                    String.format("KPI Notification failed due to Internal Server Error  :: %s and Code %s",
                            clientEx.getMessage(), clientEx.getStatusCode()));
            return StringUtils.EMPTY;
        }
        catch (final HttpServerErrorException.GatewayTimeout clientEx)
        {
            LOG.error(String.format("KPI Notification failed due to  Gateway Timeout  :: %s and Code %s",
                    clientEx.getMessage(), clientEx.getStatusCode()));
            return StringUtils.EMPTY;
        }
        catch (final HttpServerErrorException.ServiceUnavailable clientEx)
        {
            LOG.error(String.format(
                    "KPI Notification failed due to Service Unavailability  :: %s and Code %s", clientEx.getMessage(),
                    clientEx.getStatusCode()));
            return StringUtils.EMPTY;
        }
        catch (final HttpServerErrorException.NotImplemented clientEx)
        {
            LOG.error(String.format("KPI Notification failed due to no implementation  :: %s and Code %s",
                    clientEx.getMessage(), clientEx.getStatusCode()));
            return StringUtils.EMPTY;
        }
        finally {
            HttpComponentsClientHttpRequestFactory requestFactory=(HttpComponentsClientHttpRequestFactory) restTemplate_new.getRequestFactory();
            LOG.info("Releasing current client connection for Notification Call Request");
            try {
                requestFactory.destroy();
            } catch (Exception excep) {
                LOG.error(String.format("Error in releasing Connection obtained for Notification Request for Market ::: %s due to ::: %s ",siteID,excep));
            }
        }
        return StringUtils.EMPTY;
    }

    /**
     * Generate Request Body
     * @param order
     * @return
     */
    private CommunicationMessage generateRequestBody(OrderModel order){
        CustomerModel customer = (CustomerModel)order.getUser();
        final CommunicationMessage message = new CommunicationMessage();

        if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())){
            message.setSubject(StringUtils.join(new String[]{"Order Confirmed: ", order.getCode()}));
        }else if(llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite()) || llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite()) || llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite())){
            setEmailSubject(order, order.getCode(), message,"confirmed");
        }
        else if(llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())){
      	  message.setSubject(configurationService.getConfiguration().getString("vtr.order.confirmed.es.notification.subject"));
      	  return generatePlaceOrderMessage(order,customer,message,null!=order.getParentOrder()?Boolean.TRUE:Boolean.FALSE);
        }
        return getCommunicationMessage(order, customer, message, null!=order.getParentOrder()?Boolean.TRUE:Boolean.FALSE);
    }

    protected CommunicationMessage generateCancelRequestBody(OrderModel order){
        CustomerModel customer = (CustomerModel)order.getUser();
        final CommunicationMessage message = new CommunicationMessage();
        if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())){
        message.setSubject(StringUtils.join(new String[]{"Order Rejected: ",order.getCode()}));
        }else if(llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())|| llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite()) || llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite())){
            setEmailSubject(order, order.getCode(),message,"rejected");
        }
        else if (llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())){
      	  	message.setSubject(configurationService.getConfiguration().getString("vtr.order.rejected.es.notification.subject"));
    		   return generateOrderCancelledMessage(order, customer, message, null != order.getParentOrder() ? Boolean.TRUE : Boolean.FALSE);
    		  }
        return getCommunicationMessage(order, customer, message, null!=order.getParentOrder()?Boolean.TRUE:Boolean.FALSE);
    }

    protected CommunicationMessage generateProvisionRequestBody(OrderModel order){
        CustomerModel customer = (CustomerModel)order.getUser();
        final CommunicationMessage message = new CommunicationMessage();
        if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())){
            message.setSubject(StringUtils.join(new String[]{"Order Released: ",order.getParentOrder().getCode()}));
        }else if(llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite()) || llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite()) || llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite())){
            setEmailSubject(order,order.getCode(), message,"release");
        }
        else if (llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())){
     		 message.setSubject(configurationService.getConfiguration().getString("vtr.order.released.es.notification.subject"));
     		 return generateOrderConfirmationMessage(order, customer, message, null != order.getParentOrder() ? Boolean.TRUE : Boolean.FALSE);
     	  }
        return getCommunicationMessage(order, customer, message, null!=order.getParentOrder()?Boolean.TRUE:Boolean.FALSE);
    }

    private void setEmailSubject(OrderModel order, final String code, CommunicationMessage message,final String notificationType) {
        StringBuilder configSubject=new StringBuilder("order").append(".").append(notificationType).append(".").append(order.getLanguage().getIsocode()).append(".").append("notification.subject");
	    StringBuilder fallBackSubject=new StringBuilder("order").append(".").append("fallback").append(".").append(order.getLanguage().getIsocode()).append(".").append("notification.subject");

        if(llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())){
          message.setSubject(configurationService.getConfiguration().getString(configSubject.toString()));
        }else if(llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite())){
            if(isFallbackOrder(order) && !notificationType.equalsIgnoreCase("agent")){
	            fallBackSubject=new StringBuilder("lcpr").append(".").append(fallBackSubject);
	            prepareSubject(order, message, notificationType, fallBackSubject);
            }
            else {
	            configSubject = new StringBuilder("lcpr").append(configSubject);
	            prepareSubject(order, message, notificationType, configSubject);
            }

        }else if(llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite())){
            configSubject=new StringBuilder("cabletica").append(configSubject);
            prepareSubject(order, message, notificationType, configSubject);
        }
    }

    private void prepareSubject(OrderModel order, CommunicationMessage message, String notificationType, StringBuilder configSubject) {
        Configuration configuration = configurationService.getConfiguration();
        if(notificationType.equalsIgnoreCase("agent")){
      	  message.setSubject(StringUtils.join(new String[]{configuration.getString(configSubject.toString()), " ", order.getCode()}));
        }
        else if(!notificationType.equalsIgnoreCase("agent")) {
      	  message.setSubject(configuration.getString(configSubject.toString()));
        }

    }

    protected CommunicationMessage getCommunicationMessage(OrderModel order, CustomerModel customer, CommunicationMessage message, Boolean hasParentCode) {
        final List<Receiver> recievers = new ArrayList<>();
        Receiver receiver = new Receiver();
        final CustomerModel customerModel = (CustomerModel) order.getUser();
        receiver.setEmail(CustomerType.GUEST.equals(customerModel.getType()) ? StringUtils.substringAfter(customerModel.getUid(), "|") : order.getUser().getUid());
        recievers.add(receiver);
        message.setReceiver(recievers);
        message.setType("email");
        List<CommunicationRequestCharacteristic> characteristic = new ArrayList<>();
        CommunicationRequestCharacteristic name = new CommunicationRequestCharacteristic();
        name.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customerName"));
        name.setValue(StringUtils.join(new String[]{customer.getFirstName(), " ", customer.getLastName()}));
        characteristic.add(name);

        CommunicationRequestCharacteristic orderNo = new CommunicationRequestCharacteristic();
        orderNo.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.orderNo"));
        orderNo.setValue(hasParentCode ? order.getParentOrder().getCode() : order.getCode());
        characteristic.add(orderNo);
        
        if(!llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite()) && !isFallbackOrder(order))
        {
      	  CommunicationRequestCharacteristic hyperLink = new CommunicationRequestCharacteristic();
           hyperLink.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.hyperlink"));
           final String storefrontUlr=configurationService.getConfiguration().getString(
                   new StringBuffer(LlamulesoftintegrationConstants.WEBSITE).append(LlamulesoftintegrationConstants.DECIMAL).append(order.getSite().getUid()).append(LlamulesoftintegrationConstants.DECIMAL).append("https").toString());
           if (llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())) {
               hyperLink.setValue(StringUtils.join(new String[]{storefrontUlr, "/", order.getSite().getUid(), "/", order.getLanguage().getIsocode()}));
           } else{
               hyperLink.setValue(storefrontUlr);
           }
           characteristic.add(hyperLink);
        }

        CommunicationRequestCharacteristic amount = new CommunicationRequestCharacteristic();
        amount.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.amount"));
        Double totalValue= calculateTotalWithTax(order,order.getTotalPrice(),order.getTotalTax());
        amount.setValue(StringUtils.join(new String[]{order.getCurrency().getSymbol(),totalValue.toString()}));
        if(llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite())){
            amount.setValue(StringUtils.join(new String[]{order.getCurrency().getSymbol(),String.valueOf(totalValue.intValue())}));
        }
        characteristic.add(amount);
        CommunicationRequestCharacteristic orderDate = new CommunicationRequestCharacteristic();
        orderDate.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.orderDate"));
        String dateFormat="MM/dd/yyyy";
        if(llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite())){
            dateFormat="dd/MM/yyyy";
        }
        orderDate.setValue(new SimpleDateFormat(dateFormat).format(order.getDate()));
        characteristic.add(orderDate);


        if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite()) && null != order.getPaymentInfo() && null!=order.getPaymentInfo().getBillingAddress())
        {
            CommunicationRequestCharacteristic address = new CommunicationRequestCharacteristic();
            address.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.billingAddress"));
            AddressModel billingAddress=order.getInstallationAddress();
            address.setValue(provideAddressText(billingAddress,order));
            characteristic.add(address);
        }

        CommunicationRequestCharacteristic add = new CommunicationRequestCharacteristic();
        add.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.address"));
        if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())){
            add.setValue(provideAddressText(order.getInstallationAddress(),order));
        }else if(llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite()) || llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite()) || llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite())){
                  AddressModel deliveryAddress=order.getDeliveryAddress();
                  add.setValue(provideAddressText(deliveryAddress,order));
         }
        characteristic.add(add);

        if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite()) || llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite()) || llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite())) {
            for(int i=0;i<order.getEntries().size();i++){
                AbstractOrderEntryModel entry = order.getEntries().get(i);
                CommunicationRequestCharacteristic prod = new CommunicationRequestCharacteristic();
                prod.setName(StringUtils.join(new String[]{configurationService.getConfiguration().getString("llamulesoftintegration.notification.produtName"), String.valueOf(i+1)}));
                prod.setValue(entry.getProduct().getName(commonI18NService.getLocaleForLanguage(order.getLanguage())));
                characteristic.add(prod);
	                CommunicationRequestCharacteristic qty = new CommunicationRequestCharacteristic();
	                qty.setName(StringUtils.join(new String[] { configurationService.getConfiguration().getString("llamulesoftintegration.notification.qty"), String.valueOf(i + 1) }));
	                qty.setValue(String.valueOf(entry.getQuantity()));
	                characteristic.add(qty);
	                CommunicationRequestCharacteristic amt = new CommunicationRequestCharacteristic();
	                amt.setName(StringUtils.join(new String[] { configurationService.getConfiguration().getString("llamulesoftintegration.notification.productAmount"), String.valueOf(i + 1) }));
	                amt.setValue(llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite()) ? StringUtils.join(new String[] { order.getCurrency().getSymbol(), String.valueOf(entry.getTotalPrice().intValue()) }) : entry.getTotalPrice().toString());
	                characteristic.add(amt);
            }
        }else {
            int returnCount=generateLCPRBundleOrderEntries(order,characteristic);
            generateLCPRAddonEntries(order, characteristic,returnCount);
        }


       if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())) {
            CommunicationRequestCharacteristic card = new CommunicationRequestCharacteristic();
            card.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.cardDetails"));
            card.setValue(null!=order.getPaymentInfo()?((CreditCardPaymentInfoModel) order.getPaymentInfo()).getNumber():"XXXXX");
            characteristic.add(card);
        }

       if(llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite()))
	   {
		   List<String> allCategories = new ArrayList<>();
		   final CommunicationRequestCharacteristic packRequested = new CommunicationRequestCharacteristic();
		   packRequested.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.packrequested"));
		   for(int i=0;i<order.getEntries().size();i++){
			   AbstractOrderEntryModel entry = order.getEntries().get(i);
			   Collection<CategoryModel> supercategoriesList=productService.getProductForCode(catalogVersionService.getSessionCatalogVersionForCatalog(order.getSite().getUid()+"ProductCatalog"),entry.getProduct().getCode()).getSupercategories();

			   for(CategoryModel categoryModel:supercategoriesList){
				   if(!categoryModel.getCode().contains("addon") && !categoryModel.getCode().contains("smartproduct")) {
					   allCategories.add(StringUtils.capitalize(categoryModel.getCode()));
				   }
			   }
		   }
		   String packRequestedValue = String.join("+", allCategories);
		   packRequested.setValue(packRequestedValue);
		   characteristic.add(packRequested);

		   final CommunicationRequestCharacteristic packAmount = new CommunicationRequestCharacteristic();
		   packAmount.setName(StringUtils.join(new String[] {
				   configurationService.getConfiguration().getString("llamulesoftintegration.notification.packamount") }));
		   packAmount.setValue(StringUtils.join(new String[]{order.getCurrency().getSymbol(),(null != ((AbstractOrderModel) order).getBundlePrice())?String.valueOf(((AbstractOrderModel)order).getBundlePrice().doubleValue()):Double.valueOf(0.0).toString()}));
		   characteristic.add(packAmount);

		   if((order instanceof OrderModel) && !isFallbackOrder(order)) {
			   final CommunicationRequestCharacteristic amountDueToday = new CommunicationRequestCharacteristic();
			   amountDueToday.setName(StringUtils.join(new String[] { configurationService.getConfiguration().getString("llamulesoftintegration.notification.amountduetoday") }));
			   amountDueToday.setValue(StringUtils.join(new String[] { order.getCurrency().getSymbol(),
					   (null != ((AbstractOrderModel) order).getAmountDueToday()) ?
							   String.valueOf(((AbstractOrderModel) order).getAmountDueToday().doubleValue()) :
							   Double.valueOf(0.0).toString() }));
			   characteristic.add(amountDueToday);

			   final CommunicationRequestCharacteristic amountDueFirstBill = new CommunicationRequestCharacteristic();
			   amountDueFirstBill.setName(StringUtils.join(new String[] { configurationService.getConfiguration().getString("llamulesoftintegration.notification.amountduefirstbill") }));
			   amountDueFirstBill.setValue(StringUtils.join(new String[] { order.getCurrency().getSymbol(),
					   (null != ((AbstractOrderModel) order).getAmountDueFirstBill()) ?
							   String.valueOf(((AbstractOrderModel) order).getAmountDueFirstBill().doubleValue()) :
							   Double.valueOf(0.0).toString() }));
			   characteristic.add(amountDueFirstBill);
		  addPackAmountDetails(order,characteristic);
		   }
	   }
        message.setCharacteristic(characteristic);
        return message;
    }

   private void generateLCPRAddonEntries(AbstractOrderModel order, List<CommunicationRequestCharacteristic> characteristic, int returnCount) {
		int i=1;
		for(AbstractOrderEntryModel entry: order.getEntries()){

			Collection<CategoryModel> supercategoriesList = productService.getProductForCode(entry.getProduct().getCode()).getSupercategories();

			for (CategoryModel categoryModel : supercategoriesList) {
				if (categoryModel.getCode().equalsIgnoreCase("channeladdon") || categoryModel.getCode().equalsIgnoreCase("addon") || categoryModel.getCode().equalsIgnoreCase("smartproduct") || categoryModel.getCode().equalsIgnoreCase("wifiaddon") || categoryModel.getCode().equalsIgnoreCase("tvaddon")) {
					final ProductModel product = productService.getProductForCode(entry.getProduct().getCode());
					if (!DVR_BOX.equalsIgnoreCase(entry.getProduct().getCode())) {
						CommunicationRequestCharacteristic prod = new CommunicationRequestCharacteristic();
						prod.setName(StringUtils.join(new String[]{configurationService.getConfiguration().getString("llamulesoftintegration.notification.addonName"), String.valueOf(i + 1)}));
						if (order instanceof OrderModel) {
							prod.setValue(product.getName(commonI18NService.getLocaleForLanguage(((OrderModel) order).getLanguage())));
						} else {
							prod.setValue(product.getName(getCurrentLocale(order)));
						}
						characteristic.add(prod);
						if((order instanceof OrderModel) && !isFallbackOrder((OrderModel) order)) {
							CommunicationRequestCharacteristic qty = new CommunicationRequestCharacteristic();
							qty.setName(StringUtils.join(new String[] { configurationService.getConfiguration().getString("llamulesoftintegration.notification.qty"), String.valueOf(i + 1) }));
							qty.setValue("1");
							characteristic.add(qty);
						}
						CommunicationRequestCharacteristic amt = new CommunicationRequestCharacteristic();
						amt.setName(StringUtils.join(new String[]{configurationService.getConfiguration().getString("llamulesoftintegration.notification.addonAmount"), String.valueOf(i + 1)}));
						//amt.setValue(String.valueOf(order.getAddonProductsTotalPrice()));
						final Collection<PriceRowModel> priceRowModelCollection = product.getOwnEurope1Prices();
						for (final PriceRowModel pricePlanModel : priceRowModelCollection) {
							if (pricePlanModel instanceof SubscriptionPricePlanModel) {
								final Collection<RecurringChargeEntryModel> recurringChargeEntries = ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
								for (final RecurringChargeEntryModel recurringChargeEntry : recurringChargeEntries) {
									amt.setValue(StringUtils.join(new String[]{order.getCurrency().getSymbol(),String.valueOf(recurringChargeEntry.getPrice())}));
									characteristic.add(amt);
									break;
								}
							}
							break;
						}
						characteristic.add(amt);
						i++;
					}
				}
			}
		}
  }

    private int generateLCPRBundleOrderEntries(final OrderModel orderModel, List<CommunicationRequestCharacteristic> characteristic) {

       // catalogVersionService.setSessionCatalogVersion(orderModel.getSite().getUid()+"ProductCatalog","Staged");
        LOG.info(null!=orderModel.getSite()?orderModel.getSite().getUid()+" mapped to order":"No Site Mapped");
        baseSiteService.setCurrentBaseSite(orderModel.getSite(),false);
		catalogVersionService.setSessionCatalogVersion(orderModel.getSite().getUid() + "ProductCatalog", "Online");



		final StringBuilder bundlePrductName = new StringBuilder();
         List<String> categoryList=new ArrayList();
         int i=0;
         for(AbstractOrderEntryModel entry:orderModel.getEntries())
         {
          //  CommunicationRequestCharacteristic category = new CommunicationRequestCharacteristic();
            //category.setName(StringUtils.join(new String[]{configurationService.getConfiguration().getString("llamulesoftintegration.notification.productsupercategoryname"), String.valueOf(i + 1)}));

            Collection<CategoryModel> supercategoriesList = productService.getProductForCode(catalogVersionService.getSessionCatalogVersionForCatalog(orderModel.getSite().getUid()+"ProductCatalog"),entry.getProduct().getCode()).getSupercategories();

            for (CategoryModel categoryModel : supercategoriesList) {
                if(categoryModel.getCode().equalsIgnoreCase(INTERNET_CATEGORY_CODE) || categoryModel.getCode().equalsIgnoreCase(TV_CATEGORY_CODE) || categoryModel.getCode().equalsIgnoreCase(PHONE_CATEGORY_CODE)) {
	                //category.setValue(StringUtils.join(new String[]{StringUtils.capitalize(categoryModel.getCode()), " ", "Product"}));
	                //characteristic.add(category);
	                final ProductModel product = productService.getProductForCode(
			                catalogVersionService.getSessionCatalogVersionForCatalog(orderModel.getSite().getUid() + "ProductCatalog"),
			                entry.getProduct().getCode());
	                CommunicationRequestCharacteristic prod = new CommunicationRequestCharacteristic();
	                prod.setName(StringUtils.join(new String[] { configurationService.getConfiguration().getString("llamulesoftintegration.notification.produtName"), String.valueOf(i + 1) }));
	                prod.setValue(product.getName());
	                characteristic.add(prod);
                  //check for fallback order
                    if(!isFallbackOrder(orderModel))
                    {
	                    CommunicationRequestCharacteristic qty = new CommunicationRequestCharacteristic();
	                    qty.setName(StringUtils.join(new String[] { configurationService.getConfiguration().getString("llamulesoftintegration.notification.qty"), String.valueOf(i + 1) }));
	                    qty.setValue("1");
	                    characteristic.add(qty);
	                    CommunicationRequestCharacteristic amt = new CommunicationRequestCharacteristic();
	                    amt.setName(StringUtils.join(new String[] { configurationService.getConfiguration().getString("llamulesoftintegration.notification.productAmount"), String.valueOf(i + 1) }));
	                    final Collection<PriceRowModel> priceRowModelCollection = product.getOwnEurope1Prices();
	                    for (final PriceRowModel pricePlanModel : priceRowModelCollection) {
		                    if (pricePlanModel instanceof SubscriptionPricePlanModel) {
			                    final Collection<RecurringChargeEntryModel> recurringChargeEntries = ((SubscriptionPricePlanModel) pricePlanModel)
					                    .getRecurringChargeEntries();
			                    for (final RecurringChargeEntryModel recurringChargeEntry : recurringChargeEntries) {
				                    amt.setValue(String.valueOf(recurringChargeEntry.getPrice()));
				                    characteristic.add(amt);
				                    break;
			                    }
		                    }
		                    break;
	                    }
                    }
	                i++;
	                break;
                }
            }
        }

        return  i;
    }

    private CommunicationMessage generateCSARequestBody(OrderModel order, List<String> mails, final String ssnValue) {
        final CommunicationMessage message = new CommunicationMessage();
        CustomerModel customer = (CustomerModel)order.getUser();

		populateSubjectLine(order, message);
		populateReceiverList(mails, message);

		List<CommunicationRequestCharacteristic> characteristic = new ArrayList<>();
        if (!llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())) {
			CommunicationRequestCharacteristic name = new CommunicationRequestCharacteristic();
			name.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customerName"));
			name.setValue(StringUtils.join(new String[]{customer.getFirstName(), " ", customer.getLastName()}));
			characteristic.add(name);

			CommunicationRequestCharacteristic orderNo = new CommunicationRequestCharacteristic();
			orderNo.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.orderNo"));
			orderNo.setValue(order.getCode());
			characteristic.add(orderNo);

			CommunicationRequestCharacteristic hyperLink = new CommunicationRequestCharacteristic();
			hyperLink.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.hyperlink"));
			hyperLink.setValue(configurationService.getConfiguration().getString("notification.backoffice.url"));
			characteristic.add(hyperLink);
			CommunicationRequestCharacteristic amount = new CommunicationRequestCharacteristic();
			amount.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.amount"));
			Double totalValue= calculateTotalWithTax(order,order.getTotalPrice(),order.getTotalTax());
			amount.setValue(StringUtils.join(new String[]{order.getCurrency().getSymbol(),totalValue.toString()}));
			if(llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite())){
				amount.setValue(StringUtils.join(new String[]{order.getCurrency().getSymbol(),String.valueOf(totalValue.intValue())}));
			}
			characteristic.add(amount);
        }
        setOrderDateForCSRNotification(order, characteristic);

		populateDeliveryAddressForCSRNotification(order, characteristic);
		if(!llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite()) && !llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())) {
			CommunicationRequestCharacteristic customerID = new CommunicationRequestCharacteristic();
			customerID.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customerNo"));
			customerID.setValue(CustomerType.GUEST.equals(customer.getType()) ? StringUtils.substringAfter(customer.getUid(), "|") : order.getUser().getUid());
			characteristic.add(customerID);
       }

        if (llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())) {
			populateVTROrderDetails(order, characteristic);
		}

        generateAdditionalCSRequestBody(order, customer, characteristic, ssnValue);

        message.setCharacteristic(characteristic);
        return message;

    }

	private void populateReceiverList(List<String> mails, CommunicationMessage message) {
		final List<Receiver> recievers = new ArrayList<>();
		Receiver receiver = new Receiver();
		receiver.setEmail(StringUtils.join(mails.toArray(), ","));
		recievers.add(receiver);
		message.setReceiver(recievers);
		message.setType("email");
	}

	private void populateSubjectLine(OrderModel order, CommunicationMessage message) {
		if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())){
			message.setSubject("Notification: CS Agent Place Order: "+ order.getCode());
		}else if(llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())|| llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite()) || llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite())){
			setEmailSubject(order, order.getCode(), message,"agent");
		}
		else if (llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())) {
		   String rutNumber = ((CustomerModel) order.getUser()).getRutNumber();
		   String addressId = ((CustomerModel) order.getUser()).getAddressId();
		   if (StringUtils.isNotEmpty(rutNumber) && StringUtils.isNotEmpty(addressId)) {
			   message.setSubject((new StringBuilder(rutNumber).append("/").append(addressId)).toString());
		   }
	   }
	}

	private void populateVTROrderDetails(OrderModel order, List<CommunicationRequestCharacteristic> characteristic) {
		CommunicationRequestCharacteristic commerceOrderNo = new CommunicationRequestCharacteristic();
		commerceOrderNo.setName(configurationService.getConfiguration().getString("vtr.llamulesoftintegration.notification.orderNo.commerce"));
		commerceOrderNo.setValue(order.getCode());
		characteristic.add(commerceOrderNo);
		if(StringUtils.isNotEmpty(order.getBSSOrderId())) {
			CommunicationRequestCharacteristic seibelOrderNo = new CommunicationRequestCharacteristic();
			seibelOrderNo.setName(configurationService.getConfiguration().getString("vtr.llamulesoftintegration.notification.orderNo.crm"));
			seibelOrderNo.setValue(order.getBSSOrderId());
			characteristic.add(seibelOrderNo);
		}
		CommunicationRequestCharacteristic fullAddress = new CommunicationRequestCharacteristic();
		fullAddress.setName(configurationService.getConfiguration().getString("vtr.llamulesoftintegration.notification.customer.address"));
		fullAddress.setValue(provideAddressText(order.getDeliveryAddress(), order));
		characteristic.add(fullAddress);
	}

	private void populateDeliveryAddressForCSRNotification(OrderModel order, List<CommunicationRequestCharacteristic> characteristic) {
		if (!llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())) {
        CommunicationRequestCharacteristic address = new CommunicationRequestCharacteristic();
        address.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.address"));
        if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())){
            address.setValue(provideAddressText(order.getInstallationAddress(), order));
        }else{
            address.setValue(provideAddressText(order.getDeliveryAddress(), order));

        }
        characteristic.add(address);
        }
	}

	private void setOrderDateForCSRNotification(OrderModel order, List<CommunicationRequestCharacteristic> characteristic) {
		CommunicationRequestCharacteristic orderDate = new CommunicationRequestCharacteristic();
		orderDate.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.orderDate"));
		if(llaMulesoftIntegrationUtil.isVtrOrder(order.getSite()))
		{
		  final String pattern = "d MMMMM yyyy";
			 final SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, new Locale("es"));
			 orderDate.setValue(dateFormat.format(order.getDate()));
		}
		else {
		String dateFormat="MM/dd/yyyy";
		if(llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite())){
			dateFormat="dd/MM/yyyy";
		}
		orderDate.setValue(new SimpleDateFormat(dateFormat).format(order.getDate()));
		}
		characteristic.add(orderDate);
	}

	/**
     * Generate  CS AGEnt Additional Request Body
     * @param order
     * @param customer
     * @param characteristic
     */
    private void generateAdditionalCSRequestBody(OrderModel order, CustomerModel customer, List<CommunicationRequestCharacteristic> characteristic, final String ssnValue) {
        if(llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite()) ||  llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite()) || llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())){
            CommunicationRequestCharacteristic firsName = new CommunicationRequestCharacteristic();
            firsName.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customer.firstName"));
            firsName.setValue(customer.getFirstName());
            characteristic.add(firsName);

            CommunicationRequestCharacteristic lastName = new CommunicationRequestCharacteristic();
            lastName.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customer.lastName"));
            lastName.setValue(customer.getLastName());
            characteristic.add(lastName);

            CommunicationRequestCharacteristic emailId = new CommunicationRequestCharacteristic();
            emailId.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customer.email"));
            emailId.setValue(customer.getAdditionalEmail());
            characteristic.add(emailId);

            CommunicationRequestCharacteristic phone = new CommunicationRequestCharacteristic();
            phone.setName(llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())?configurationService.getConfiguration().getString("vtr.llamulesoftintegration.notification.customer.phone"):configurationService.getConfiguration().getString("llamulesoftintegration.notification.customer.phone"));
            phone.setValue(llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite())?llaMulesoftIntegrationUtil.formatPhoneNumber(customer.getFixedPhone()):customer.getMobilePhone());
            characteristic.add(phone);
          if(llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite())) {
              CommunicationRequestCharacteristic contractDetails = new CommunicationRequestCharacteristic();
              contractDetails.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.order.contractType"));
              contractDetails.setValue(enumerationService.getEnumerationName(order.getContractType(), new Locale("es")));
              characteristic.add(contractDetails);

              CommunicationRequestCharacteristic dob = new CommunicationRequestCharacteristic();
              dob.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customer.dob"));
              dob.setValue(new SimpleDateFormat("dd/MM/yyyy").format(customer.getDateOfBirth()));
              characteristic.add(dob);

			  addAdditionalCustomerDetails(customer, characteristic);


			  final CommunicationRequestCharacteristic houseNo = new CommunicationRequestCharacteristic();
				  houseNo.setName(configurationService.getConfiguration()
						  .getString("llamulesoftintegration.freshdesk.fallout.notification.houseKey"));
			  if(Objects.nonNull(order.getSelectedAddress()) )
			  {
				  houseNo.setValue(StringUtils.isNotEmpty(order.getSelectedAddress().getHouseNo())?order.getSelectedAddress().getHouseNo():EMPTY_STRING);
				  characteristic.add(houseNo);
			  }

				  final CommunicationRequestCharacteristic driverLicenseState = new CommunicationRequestCharacteristic();
				  driverLicenseState.setName(configurationService.getConfiguration()
						  .getString("llamulesoftintegration.notification.driver.license.state"));
				  driverLicenseState.setValue(StringUtils.isNotEmpty(customer.getDriverLicenceState())?
                                  enumerationService.getEnumerationName(enumerationService.getEnumerationValue(DriverLicenceState.class,
                                          customer.getDriverLicenceState()), new Locale("es")) : Strings.EMPTY);
				  characteristic.add(driverLicenseState);
			 // int returnCount=generateLCPRBundleOrderEntries(order,characteristic);
			  generateLCPRAddonEntries(order, characteristic,1);

          }
        if (!DocumentType.OTHER.getCode().equalsIgnoreCase(customer.getDocumentType()) && !llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())){
            CommunicationRequestCharacteristic idType = new CommunicationRequestCharacteristic();
            idType.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customer.idtype"));
            idType.setValue(enumerationService.getEnumerationName(enumerationService.getEnumerationValue(DocumentType.class, customer.getDocumentType()), new Locale("es")));
            characteristic.add(idType);

            CommunicationRequestCharacteristic idNumber = new CommunicationRequestCharacteristic();
            idNumber.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customer.idnumber"));
            idNumber.setValue(customer.getDocumentNumber());
            characteristic.add(idNumber);
        }
        if (llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())) {
           CommunicationRequestCharacteristic rutNumber = new CommunicationRequestCharacteristic();
           rutNumber.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customer.rutNumber"));
           rutNumber.setValue(customer.getRutNumber());
           characteristic.add(rutNumber);

           CommunicationRequestCharacteristic addressId = new CommunicationRequestCharacteristic();
           addressId.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customer.addressId"));
           addressId.setValue(customer.getAddressId());
           characteristic.add(addressId);
       }
        if(llaMulesoftIntegrationUtil.isVtrOrder(order.getSite()))
        {
      	  setVtrOrderEntryDetails(order,characteristic);
        }
        else {
			final CommunicationRequestCharacteristic packRequested = new CommunicationRequestCharacteristic();
			packRequested.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.packrequested"));
			for(int i=0;i<order.getEntries().size();i++){
            AbstractOrderEntryModel entry = order.getEntries().get(i);

            CommunicationRequestCharacteristic category = new CommunicationRequestCharacteristic();
            category.setName(StringUtils.join(new String[]{configurationService.getConfiguration().getString("llamulesoftintegration.notification.productsupercategoryname"), String.valueOf(i+1)}));

            Collection<CategoryModel> supercategoriesList=productService.getProductForCode(entry.getProduct().getCode()).getSupercategories();
            for(CategoryModel categoryModel:supercategoriesList){
                if(categoryModel.getCode().contains("addon") || categoryModel.getCode().contains("smartproduct")){
                    if(llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite())){
				category.setValue(StringUtils.capitalize(entry.getProduct().getName(commonI18NService.getLocaleForLanguage(order.getLanguage()))));
			}
                	else{
                    	category.setValue(configurationService.getConfiguration().getString(new StringBuilder("llamulesoftintegration.notification.productsupercat.addon.text").append(".").append(order.getLanguage().getIsocode()).toString()));
			}
                }else if (llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite()))
						{

							for(CategoryModel categories:supercategoriesList)
							{
								category.setValue(StringUtils.join(new String[]
										{ StringUtils.capitalize(categoryModel.getCode()) }));
							}

						}
                else{
               	 if(llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite())){
			category.setValue(StringUtils.join(new String[] { StringUtils.capitalize(categoryModel.getName(commonI18NService.getLocaleForLanguage(order.getLanguage()))) }));
			packRequested.setValue(StringUtils.join(new String[] { StringUtils.capitalize(categoryModel.getName(commonI18NService.getLocaleForLanguage(order.getLanguage()))) }));
			}
                	else{
                		category.setValue(StringUtils.join(new String[] { StringUtils.capitalize(categoryModel.getCode()) }));
                	}
               	      if(!llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite()) && !llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite()))
					  {
						  packRequested.setValue(StringUtils.join(new String[] { StringUtils.capitalize(categoryModel.getCode()) }));
						  characteristic.add(packRequested);
					  }
               
            }
            characteristic.add(category);
            CommunicationRequestCharacteristic prod = new CommunicationRequestCharacteristic();
				if(!categoryModel.getCode().contains("addon") && !categoryModel.getCode().contains("smartproduct")) {
					prod.setName(StringUtils.join(new String[]{configurationService.getConfiguration().getString("llamulesoftintegration.notification.productname"), String.valueOf(i + 1)}));
					prod.setValue(entry.getProduct().getName(commonI18NService.getLocaleForLanguage(order.getLanguage())));
					characteristic.add(prod);
				}
	break;
        }
        }
			if(llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite()))
			{
				List<String> allCategories = new ArrayList<>();
				     for(int i=0;i<order.getEntries().size();i++)
				     {
					AbstractOrderEntryModel entry = order.getEntries().get(i);
					Collection<CategoryModel> supercategoriesList=productService.getProductForCode(entry.getProduct().getCode()).getSupercategories();

					for(CategoryModel categoryModel:supercategoriesList){
						if(!categoryModel.getCode().contains("addon") && !categoryModel.getCode().contains("smartproduct")) {
							allCategories.add(StringUtils.capitalize(categoryModel.getCode()));
						}
					}
				}
				String packRequestedValue = String.join("+", allCategories);
				packRequested.setValue(packRequestedValue);
				characteristic.add(packRequested);
			}
        }
        if (llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite())) {
     		// final CommunicationRequestCharacteristic packRequested = new
     		// CommunicationRequestCharacteristic();
     		// packRequested.setName(StringUtils.join(new
     		// String[]{configurationService.getConfiguration().getString("llamulesoftintegration.notification.packrequested")}));
     		// packRequested.setValue(((AbstractOrderModel)order).get
     		// characteristic.add(packRequested);

     		final CommunicationRequestCharacteristic packAmount = new CommunicationRequestCharacteristic();
     		packAmount.setName(StringUtils.join(new String[] {
     				configurationService.getConfiguration().getString("llamulesoftintegration.notification.packamount") }));
     		packAmount.setValue(StringUtils.join(new String[]{order.getCurrency().getSymbol(),((AbstractOrderModel) order).getBundlePrice().toString()}));
     		characteristic.add(packAmount);

     		final CommunicationRequestCharacteristic amountDueToday = new CommunicationRequestCharacteristic();
     		amountDueToday.setName(StringUtils.join(new String[] { configurationService.getConfiguration()
     				.getString("llamulesoftintegration.notification.amountduetoday") }));
     		amountDueToday.setValue(StringUtils.join(new String[]{order.getCurrency().getSymbol(),((AbstractOrderModel) order).getAmountDueToday().toString()}));
     		characteristic.add(amountDueToday);

     		final CommunicationRequestCharacteristic amountDueFirstBill = new CommunicationRequestCharacteristic();
     		amountDueFirstBill.setName(StringUtils.join(new String[] { configurationService.getConfiguration()
     				.getString("llamulesoftintegration.notification.amountduefirstbill") }));
     		if(Objects.nonNull(order.getFalloutReasonValue()) )
     		{
		        amountDueFirstBill.setValue(StringUtils.join(new String[] { order.getCurrency().getSymbol(),Double.valueOf(3.0).toString() }));
	        }
     		else {
		        amountDueFirstBill.setValue(StringUtils.join(new String[] { order.getCurrency().getSymbol(),
				        ((AbstractOrderModel) order).getAmountDueFirstBill().toString() }));
	        }
     		characteristic.add(amountDueFirstBill);

     		final CommunicationRequestCharacteristic checkoutStep = new CommunicationRequestCharacteristic();
     		checkoutStep.setName(StringUtils.join(new String[] { configurationService.getConfiguration()
     				.getString("llamulesoftintegration.notification.checkoutstep") }));
     		checkoutStep.setValue("Place Order");
     		characteristic.add(checkoutStep);

			updateActiveServiceDetails(characteristic, order.getServiceQualResultRequest());

			populateSSNDetailsForCSR(customer, characteristic, ssnValue);

			final CommunicationRequestCharacteristic debtCheck = populateDebtCheckForCSR(order);
			characteristic.add(debtCheck);
     		if (null != ((AbstractOrderModel) order).getCreditScoreLevel()) {
     			final CommunicationRequestCharacteristic creditScore = new CommunicationRequestCharacteristic();
     			creditScore.setName(StringUtils.join(new String[] { configurationService.getConfiguration()
     					.getString("llamulesoftintegration.notification.creditscore") }));
     			creditScore.setValue(((AbstractOrderModel) order).getCreditScoreLevel().getLetterMapping());
     			characteristic.add(creditScore);
     		}

			addPackAmountDetails(order,characteristic);

		}
        }
    }

	/**
	 * Populate Debt Check
	 * @param order
	 * @return
	 */
	private CommunicationRequestCharacteristic populateDebtCheckForCSR(OrderModel order) {
		final CommunicationRequestCharacteristic debtCheck = new CommunicationRequestCharacteristic();
		debtCheck.setName(StringUtils.join(new String[] { configurationService.getConfiguration().getString("llamulesoftintegration.notification.debtcheck") }));
		//Setting blank if order having fallback
		if((order instanceof  OrderModel) && isFallbackOrder(order)){
			debtCheck.setValue(EMPTY_STRING);
		}
		else {
			if (((AbstractOrderModel) order).getDebtCheckFlag()) {
				debtCheck.setValue(configurationService.getConfiguration().getString("llamulesoftintegration.notification.debtcheck.no.name"));
			} else {
				debtCheck.setValue(configurationService.getConfiguration().getString("llamulesoftintegration.notification.debtcheck.yes.name"));
			}
		}
		return debtCheck;
	}

	private void populateSSNDetailsForCSR(CustomerModel customer, List<CommunicationRequestCharacteristic> characteristic, String ssnValue) {
		final CommunicationRequestCharacteristic ssnCheck = new CommunicationRequestCharacteristic();
		if ((ssnValue != null && StringUtils.isNotEmpty(ssnValue)) || (StringUtils.isNotEmpty(customer.getCustomerSSN()))) {

			ssnCheck.setName(configurationService.getConfiguration()
					.getString("llamulesoftintegration.freshdesk.fallout.notification.ssncheck"));
			ssnCheck.setValue(configurationService.getConfiguration()
					.getString("llamulesoftintegration.notification.ssncheck.yes.name"));
			characteristic.add(ssnCheck);

			final CommunicationRequestCharacteristic ssn = new CommunicationRequestCharacteristic();
			ssn.setName(configurationService.getConfiguration()
					.getString("llamulesoftintegration.freshdesk.fallout.notification.ssnid"));
			ssn.setValue(StringUtils.isNotEmpty(ssnValue)? ssnValue :llaMulesoftIntegrationUtil.formatSSNNumber(customer.getCustomerSSN()));
			characteristic.add(ssn);
		} else {
			ssnCheck.setName(configurationService.getConfiguration()
					.getString("llamulesoftintegration.freshdesk.fallout.notification.ssncheck"));
			ssnCheck.setValue(configurationService.getConfiguration()
					.getString("llamulesoftintegration.notification.ssncheck.no.name"));
			characteristic.add(ssnCheck);
		}
	}

	private void addAdditionalCustomerDetails(CustomerModel customer, List<CommunicationRequestCharacteristic> characteristic) {
		CommunicationRequestCharacteristic middleName = new CommunicationRequestCharacteristic();
		middleName.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customer.middle.name"));
		if(StringUtils.isNotEmpty(customer.getSecondName()))
		{
			middleName.setValue(customer.getSecondName());
			characteristic.add(middleName);
		}

		CommunicationRequestCharacteristic secondSurname = new CommunicationRequestCharacteristic();
		secondSurname.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customer.second.surname"));
		if(StringUtils.isNotEmpty(customer.getSecondSurname()))
		{
			secondSurname.setValue(customer.getSecondSurname());
			characteristic.add(secondSurname);
		}
	}

	/*
       method to add service details for servicable address
     */
	private String addServices(ServiceQualificationItemModel serviceQualificationItemModel, String services)
	{

		String value = EMPTY_STRING;
		ServiceModel service = serviceQualificationItemModel.getService();
		List<ServiceFeatureModel> feature = service.getFeature();
		if(CollectionUtils.isNotEmpty(feature))
		{
			for (ServiceFeatureModel f:feature) {
				if(services.equalsIgnoreCase(f.getName()))
				{
					List<FeatureCharacteristicsModel> characterstics = f.getFeatureCharacteristics();
					if(CollectionUtils.isNotEmpty(characterstics))
					{
						value = characterstics.get(0).getValue();

					}
				}

			}
		}
		return value;
	}

	private Double calculateTotalWithTax(final AbstractOrderModel order,Double orderTotalWithoutTax, Double orderTax){
        if(llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite())){
            BigDecimal totalPrice = BigDecimal.valueOf(order.getOrderMonthlyPrice());
            return Double.valueOf(totalPrice.doubleValue());
        }else if(llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite())){

            BigDecimal bundleMonthlyPrice= BigDecimal.valueOf(null!=order.getBundlePrice()?order.getBundlePrice():0.0);
            BigDecimal smartProtectMonthlyPrice= BigDecimal.valueOf(null!=order.getSmartProtectPrice()?order.getSmartProtectPrice():0.0);
            BigDecimal channelAddonOnMonthlyPrice= BigDecimal.valueOf(null!=order.getChannelsAddonTotalPrice()?order.getChannelsAddonTotalPrice():0.0);
            BigDecimal addonOnMonthlyPrice= BigDecimal.valueOf(null!=order.getAddonProductsTotalPrice()?order.getAddonProductsTotalPrice():0.0);

            BigDecimal totalMonthlyPrice=bundleMonthlyPrice.add(smartProtectMonthlyPrice).add(channelAddonOnMonthlyPrice).add(addonOnMonthlyPrice);
            return Double.valueOf(totalMonthlyPrice.doubleValue());
        }else{
            BigDecimal totalPrice = BigDecimal.valueOf(orderTotalWithoutTax.doubleValue());
            BigDecimal totalOrderPriceWithTax = null!=orderTax?totalPrice.add(BigDecimal.valueOf(orderTax.doubleValue())):totalPrice;
            return Double.valueOf(totalOrderPriceWithTax.doubleValue());

        }
    }

	@Override
	public String generatePaymentNotification(final AbstractOrderModel order) throws LLAApiException
	{
		LOG.info("Generating debt amount notification");
		final String siteID = configurationService.getConfiguration()
				.getString(new StringBuffer(LlamulesoftintegrationConstants.WEBSITE).append(LlamulesoftintegrationConstants.DECIMAL)
						.append(order.getSite().getUid()).append(LlamulesoftintegrationConstants.DECIMAL)
						.append(LlamulesoftintegrationConstants.CODE).toString());
		final String endPointResolver = generateEndPointResolver((OrderModel) order, siteID, "PAYMENT");
		final URI notification_post_url = URI.create(apiUrlConstructor(getAPIServer(), siteID, endPointResolver));
		final CommunicationMessage message = generatePaymentRequestBody(order);
		return sendNotification(message, notification_post_url, siteID);

	}

	/**
	 *Generate Payment Request Body
	 *@param order
	 */
	protected CommunicationMessage generatePaymentRequestBody(final AbstractOrderModel order)
	{
		final CustomerModel customer = (CustomerModel) order.getUser();
		final CommunicationMessage message = new CommunicationMessage();
		message.setSubject(configurationService.getConfiguration().getString("order.payment.es.notification.subject"));
		return getPendingPaymentMessage(order, customer, message,
				null != ((OrderModel) order).getParentOrder() ? Boolean.TRUE : Boolean.FALSE);
	}

	/**
	 *Generate Pending Payment Request Body
	 *@param customer
	 *@param order
	 *@param message
	 */
	private CommunicationMessage getPendingPaymentMessage(final AbstractOrderModel order, final CustomerModel customer,
			final CommunicationMessage message, final Boolean boolean1)
	{
		final List<Receiver> recievers = new ArrayList<>();
		final Receiver receiver = new Receiver();
		final CustomerModel customerModel = (CustomerModel) order.getUser();
		receiver
				.setEmail(CustomerType.GUEST.equals(customerModel.getType()) ? StringUtils.substringAfter(customerModel.getUid(), "|")
						: order.getUser().getUid());
		recievers.add(receiver);
		message.setReceiver(recievers);
		message
				.setType(configurationService.getConfiguration().getString("llamulesoftintegration.customer.emailContactMediumType"));

		final List<CommunicationRequestCharacteristic> characteristic = new ArrayList<>();
		final CommunicationRequestCharacteristic orderNo = new CommunicationRequestCharacteristic();
		orderNo.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.orderNo"));
		orderNo.setValue(order.getCode());
		characteristic.add(orderNo);

		final CommunicationRequestCharacteristic orderDate = new CommunicationRequestCharacteristic();
		orderDate.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.orderDate"));
		orderDate.setValue(new SimpleDateFormat("MM/dd/yyyy").format(order.getDate()));
		characteristic.add(orderDate);

		final CommunicationRequestCharacteristic name = new CommunicationRequestCharacteristic();
		name.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customerFirstName"));
		name.setValue(StringUtils.join(new String[]
		{ customer.getFirstName() }));
		characteristic.add(name);

		final CommunicationRequestCharacteristic orderPaymentLink = new CommunicationRequestCharacteristic();
		orderPaymentLink
				.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.order.payment.link"));
		orderPaymentLink.setValue(configurationService.getConfiguration().getString("notification.payment.url"));
		characteristic.add(orderPaymentLink);

		message.setCharacteristic(characteristic);
		return message;
	}
	
	/**
	 *Generate Order Cancel Request Body Message
	 *@param customer
	 *@param order
	 *@param message
	 */
	private CommunicationMessage generateOrderCancelledMessage(final OrderModel order, final CustomerModel customer,
			final CommunicationMessage message, final Boolean hasParentCode)
	{
		String productLogo = StringUtils.EMPTY;
		List<CommunicationRequestCharacteristic> characteristic = setVtrCommunicationRequest(order, message, hasParentCode);
		final CommunicationRequestCharacteristic name = new CommunicationRequestCharacteristic();
		name.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customerFirstName"));
		name.setValue(StringUtils.join(new String[]
		{ customer.getFirstName() }));
		characteristic.add(name);
		message.setCharacteristic(characteristic);
		return message;
	}
	
	/**
	 *Generate Place Order Request Body Message
	 *@param order 
	 *@param customer
	 *@param message
	 */
	private CommunicationMessage generatePlaceOrderMessage(OrderModel order, CustomerModel customer, CommunicationMessage message,
			Boolean hasParentCode)
	{		
		final List<CommunicationRequestCharacteristic> characteristic = setVtrCommunicationRequest(order, message, hasParentCode);
		final CommunicationRequestCharacteristic vtrOrderDate = new CommunicationRequestCharacteristic();
		vtrOrderDate.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.orderDate"));
		vtrOrderDate.setValue(new SimpleDateFormat("dd/MM/yyyy").format(order.getDate()));
		characteristic.add(vtrOrderDate);
		final CommunicationRequestCharacteristic vtrCustomerFirstName = new CommunicationRequestCharacteristic();
	   vtrCustomerFirstName.setName(
			configurationService.getConfiguration().getString("vtr.llamulesoftintegration.notification.customerFirstName"));
	   vtrCustomerFirstName.setValue(customer.getFirstName());
	   characteristic.add(vtrCustomerFirstName);
	   setVtrOrderEntryDetails(order, characteristic);
	   message.setCharacteristic(characteristic);
	  return message;
	 }

	  /**
		 *Generate Order Confirmation Request Body 
		 *@param order
		 *@param customer
		 *@param message 
		 */
		private CommunicationMessage generateOrderConfirmationMessage(OrderModel order, CustomerModel customer,
				CommunicationMessage message, Boolean hasParentCode)
		{
		final List<CommunicationRequestCharacteristic> characteristic = setVtrCommunicationRequest(order, message, hasParentCode);
      final CommunicationRequestCharacteristic vtrCustomerFirstName = new CommunicationRequestCharacteristic();
      vtrCustomerFirstName.setName(configurationService.getConfiguration().getString("vtr.llamulesoftintegration.notification.customerFirstName"));
      vtrCustomerFirstName.setValue(customer.getFirstName());
      characteristic.add(vtrCustomerFirstName);
      final CommunicationRequestCharacteristic vtrOrderDate = new CommunicationRequestCharacteristic();
		final String pattern = "d MMMMM yyyy";
		final SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, new Locale("es"));
		vtrOrderDate.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.orderDate"));
		vtrOrderDate.setValue(dateFormat.format(order.getDate()));
		characteristic.add(vtrOrderDate);
      final CommunicationRequestCharacteristic vtrCustomerFullName = new CommunicationRequestCharacteristic();
      vtrCustomerFullName.setName(configurationService.getConfiguration().getString("vtr.llamulesoftintegration.notification.customerFullName"));
      vtrCustomerFullName.setValue(StringUtils.join(new String[]{customer.getFirstName(), " ", customer.getLastName()}));
      characteristic.add(vtrCustomerFullName);
      setVtrCustomerFullAddress(order, characteristic);
      setVtrOrderEntryDetails(order, characteristic);
      message.setCharacteristic(characteristic);
      return message;
		}

	/**
	 *Generate Vtr Communication Request Body
	 *@param order
	 *@param message 
	 */
	private List<CommunicationRequestCharacteristic> setVtrCommunicationRequest(OrderModel order, CommunicationMessage message,
			Boolean hasParentCode)
	{
		final List<Receiver> recievers = new ArrayList<>();
		Receiver receiver = new Receiver();
		final CustomerModel customerModel = (CustomerModel) order.getUser();
		receiver.setEmail(CustomerType.GUEST.equals(customerModel.getType()) ? StringUtils.substringAfter(customerModel.getUid(), "|") : order.getUser().getUid());
		recievers.add(receiver);
		message.setReceiver(recievers);
		message.setType("email");
		List<CommunicationRequestCharacteristic> characteristic = new ArrayList<>();
		 CommunicationRequestCharacteristic vtrOrderNo = new CommunicationRequestCharacteristic();
	    vtrOrderNo.setName(configurationService.getConfiguration().getString("vtr.llamulesoftintegration.notification.orderNo"));
	    vtrOrderNo.setValue(hasParentCode ? order.getParentOrder().getCode() : order.getCode());
	    characteristic.add(vtrOrderNo);
		return characteristic;
	}
	
	/**
	 * Generate Vtr Order Entry Details
	 * @param order
	 * @param characteristic
	 */
	private void setVtrOrderEntryDetails(final OrderModel order, final List<CommunicationRequestCharacteristic> characteristic)
	{
		catalogVersionService.setSessionCatalogVersion(order.getSite().getUid() + "ProductCatalog", "Online");
		for(int i = 0; i < order.getEntries().size(); i++){
      		final AbstractOrderEntryModel entry = order.getEntries().get(i);
      	 final Collection<CategoryModel> supercategories = entry.getProduct().getSupercategories();
      	 for (CategoryModel categoryModel : supercategories)
			{
      		 if(categoryModel.getCode().contains(TRIPLE_CATEGORY_CODE)
 						|| categoryModel.getCode().contains(INTERNETTV_CATEGORY_CODE)
 						|| categoryModel.getCode().contains(PHONETV_CATEGORY_CODE) 
 						|| categoryModel.getCode().contains(INTERNETPHONE_CATEGORY_CODE) 
 						|| categoryModel.getCode().contains(TELEVISION_CATEGORY_CODE)
 						|| categoryModel.getCode().contains(TELEPHONE_CATEGORY_CODE)
 						|| categoryModel.getCode().contains(INTERNET_CATEGORY_CODE)) {
      				final CommunicationRequestCharacteristic vtrProd = new CommunicationRequestCharacteristic();
      			 vtrProd.setName(
    						configurationService.getConfiguration().getString("vtr.llamulesoftintegration.notification.main.product.name"));
    				vtrProd.setValue(entry.getProduct().getName(commonI18NService.getLocaleForLanguage(order.getLanguage())));
    				characteristic.add(vtrProd);
      		 }
    			else if (categoryModel.getCode().contains(ADDON_CATEGORY_CODE))
   			{
   				final CommunicationRequestCharacteristic vtrAddonProd = new CommunicationRequestCharacteristic();
   				vtrAddonProd.setName(StringUtils.join(new String[]
   				{ configurationService.getConfiguration().getString("vtr.llamulesoftintegration.notification.produtName"),
   						String.valueOf(i-1) }));
   				vtrAddonProd.setValue(entry.getProduct().getName(commonI18NService.getLocaleForLanguage(order.getLanguage())));
   				characteristic.add(vtrAddonProd);

   				final CommunicationRequestCharacteristic vtrAddonQty = new CommunicationRequestCharacteristic();
   				vtrAddonQty.setName(StringUtils.join(new String[]
   				{ configurationService.getConfiguration().getString("vtr.llamulesoftintegration.notification.addonquantity.name"),
   						String.valueOf(i - 1) }));
   				vtrAddonQty.setValue(entry.getQuantity().toString());
   				characteristic.add(vtrAddonQty);
   			}
			}
       }
	  }
	

	/**
	 * Generate Customer Address for VTR
	 * @param order
	 * @param characteristic
	 */
	private void setVtrCustomerFullAddress(final OrderModel order, final List<CommunicationRequestCharacteristic> characteristic)
	{
		final CommunicationRequestCharacteristic fullAddress = new CommunicationRequestCharacteristic();
		fullAddress.setName(
				configurationService.getConfiguration().getString("vtr.llamulesoftintegration.notification.customer.address"));
		fullAddress.setValue(provideAddressText(order.getDeliveryAddress(), order));
		characteristic.add(fullAddress);
	}

   @Override
   public String triggerC2C_AgentNotification( String lang, String baseSite, String requestNumber, String customerName, String planCode, Integer numberOfLines, String deviceCode, String customerLastName, String phoneNumber, String email, String colorCode, String capacityCode) throws LLAApiException {
       LOG.info("Generating notification for Agent - C2C");
       final String siteID =configurationService.getConfiguration().getString(new StringBuffer(LlamulesoftintegrationConstants.WEBSITE).append(LlamulesoftintegrationConstants.DECIMAL).append(baseSite).append(LlamulesoftintegrationConstants.DECIMAL).append(LlamulesoftintegrationConstants.CODE).toString());
       URI placeOrder = URI.create(apiUrlConstructor(getAPIServer(),siteID,LlamulesoftintegrationConstants.PUERTORICO_C2C_NEW_CSA_ORDER_NOTIFICATION));
       CommunicationMessage message = generateC2CNew_RequestBody(baseSite,"agent",lang,requestNumber,customerLastName,customerName,planCode,deviceCode,numberOfLines,phoneNumber,email,colorCode,capacityCode);
       return sendNotification(message, placeOrder,siteID);
   }

   @Override
   public String triggerC2C_CustomerNotification( String lang, String baseSite, String requestNumber, String customerName, String planCode, Integer numberOfLines, String deviceCode, String customerLastName, String phoneNumber, String email, String colorCode, String capacityCode) throws LLAApiException {
       LOG.info("Generating notification for Customer - C2C");
       final String siteID =configurationService.getConfiguration().getString(new StringBuffer(LlamulesoftintegrationConstants.WEBSITE).append(LlamulesoftintegrationConstants.DECIMAL).append(baseSite).append(LlamulesoftintegrationConstants.DECIMAL).append(LlamulesoftintegrationConstants.CODE).toString());
       final String siteNotificationPath=new StringBuffer("puertorico.api.notification.c2cnew.customer.").append(lang).toString();
       URI placeOrder = URI.create(apiUrlConstructor(getAPIServer(),siteID,siteNotificationPath));
       CommunicationMessage message = generateC2CNew_RequestBody(baseSite,"customer",lang,requestNumber,customerLastName,customerName,planCode,deviceCode,numberOfLines,phoneNumber,email,colorCode,capacityCode);
       return sendNotification(message, placeOrder,siteID);
   }

	
	private CommunicationMessage generateC2CNew_RequestBody(String baseSite, String notificationType, String lang, String requestNumber, String customerLastName, String customerName, String planCode, String deviceCode, Integer numberOfLines, String phoneNumber, String email, String colorCode, String capacityCode) {
      final CommunicationMessage message = new CommunicationMessage();
      final List<String> mails = new ArrayList<String>();
      List<CommunicationRequestCharacteristic> characteristicList = new ArrayList<>();
      if(notificationType.equalsIgnoreCase("agent")) {
          message.setSubject(StringUtils.join(new String[]{configurationService.getConfiguration().getString("llamulesoftintegration.notification.c2c.subject"), " ", requestNumber}));
          List<EmployeeModel> csaAgents= llaMulesoftIntegrationUtil.getEmployeesInUserGroup("backofficeworkflowadmingroup").stream().filter(employee -> employee.getGroups().contains(userService.getUserGroupForUID("puertorico"))).collect(Collectors.toList());
          mails.addAll(llaMulesoftIntegrationUtil.getEmailAddressesOfEmployees(csaAgents));
          final CommunicationRequestCharacteristic name = new CommunicationRequestCharacteristic();
          name.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customerName"));
          name.setValue(customerName);
          characteristicList.add(name);

          final CommunicationRequestCharacteristic lastName = new CommunicationRequestCharacteristic();
          lastName.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customerLastName"));
          lastName.setValue(customerLastName);
          characteristicList.add(lastName);

      } else if(notificationType.equalsIgnoreCase("customer")){
          message.setSubject(configurationService.getConfiguration().getString(new StringBuffer("llamulesoftintegration.notification.c2c.customer.subject").append(LlamulesoftintegrationConstants.DECIMAL).append(lang).toString()));
          mails.add(email);
          final CommunicationRequestCharacteristic name = new CommunicationRequestCharacteristic();
          name.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customerName"));
          name.setValue(new StringBuffer(customerName).append(",").append(customerLastName).toString());
          characteristicList.add(name);
      }
		populateReceiverList(mails, message);
		populateC2CNewCharacteristics(baseSite,notificationType, lang, requestNumber, customerName, planCode, deviceCode, numberOfLines, phoneNumber, email, colorCode, capacityCode, characteristicList);
      message.setCharacteristic(characteristicList);
      return  message;
  }
	
	private void populateC2CNewCharacteristics(String baseSite, String notificationType, String lang, String requestNumber, String customerName, String planCode, String deviceCode, Integer numberOfLines, String phoneNumber, String email, String colorCode, String capacityCode, List<CommunicationRequestCharacteristic> characteristicList) {
      final CommunicationRequestCharacteristic req_Number = new CommunicationRequestCharacteristic();
      req_Number.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.requestNumber"));
      req_Number.setValue(requestNumber);
      characteristicList.add(req_Number);

      final CommunicationRequestCharacteristic progress = new CommunicationRequestCharacteristic();
      progress.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.progress"));
      progress.setValue(configurationService.getConfiguration().getString("llamulesoftintegration.notification.progressState"));
      characteristicList.add(progress);
      final String dateFormat = "dd-MM-yyyy";
      final CommunicationRequestCharacteristic req_Date = new CommunicationRequestCharacteristic();
      req_Date.setName(notificationType.equals("agent") ? configurationService.getConfiguration().getString("llamulesoftintegration.notification.requestDateTime") : configurationService.getConfiguration().getString("llamulesoftintegration.notification.requestDate"));
      req_Date.setValue(notificationType.equals("agent") ? new SimpleDateFormat(dateFormat).format(new Date()) : "Today");
      characteristicList.add(req_Date);

      final CommunicationRequestCharacteristic orderDate = new CommunicationRequestCharacteristic();
      orderDate.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.orderDate"));
      orderDate.setValue(new SimpleDateFormat(dateFormat).format(new Date()));
      characteristicList.add(orderDate);

      final CommunicationRequestCharacteristic phone = new CommunicationRequestCharacteristic();
      phone.setName(configurationService.getConfiguration().getString("vtr.llamulesoftintegration.notification.customer.phone"));
      phone.setValue(phoneNumber);
      characteristicList.add(phone);

      final CommunicationRequestCharacteristic emailAddress = new CommunicationRequestCharacteristic();
      emailAddress.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.customer.e_mail"));
      emailAddress.setValue(email);
      characteristicList.add(emailAddress);
      populatePlanDetails(baseSite,lang, numberOfLines, planCode, characteristicList);
      if (StringUtils.isNotEmpty(deviceCode)){
          populateDeviceDetails(baseSite,lang, deviceCode, colorCode, capacityCode, characteristicList);
      }
  }	
	
   private void populatePlanDetails(String baseSite,String lang, Integer noLines, String planCode, List<CommunicationRequestCharacteristic> characteristicList) {
      userService.setCurrentUser(userService.getAdminUser());
      catalogVersionService.setSessionCatalogVersion(baseSite+"ProductCatalog","Online");
      baseSiteService.setCurrentBaseSite(baseSite,Boolean.TRUE);
	    TmaSimpleProductOfferingModel poSpo=(TmaSimpleProductOfferingModel)productService.getProductForCode(catalogVersionService.getSessionCatalogVersionForCatalog(baseSite+"ProductCatalog"),planCode);

      final CommunicationRequestCharacteristic device = new CommunicationRequestCharacteristic();
      device.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.planName"));
      device.setValue(poSpo.getName(Locale.forLanguageTag(lang)));
      characteristicList.add(device);

      final CommunicationRequestCharacteristic numLines = new CommunicationRequestCharacteristic();
      numLines.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.numberOfLines"));
      numLines.setValue(String.valueOf(noLines));
      characteristicList.add(numLines);

      final Collection<PriceRowModel> priceRowModelCollection=poSpo.getEurope1Prices();
      final CommunicationRequestCharacteristic monthlyPricePlan = new CommunicationRequestCharacteristic();
      monthlyPricePlan.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.device.autopay.monthlyPrice"));
      for (final PriceRowModel pricePlanModel: priceRowModelCollection) {
          if (pricePlanModel instanceof SubscriptionPricePlanModel) {
              final Collection <RecurringChargeEntryModel> recurringChargeEntries = ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
              for (final RecurringChargeEntryModel recurringChargeEntry: recurringChargeEntries) {
                  if (recurringChargeEntry.getQty().intValue() == noLines.intValue()){
                      monthlyPricePlan.setValue(recurringChargeEntry.getCurrency().getSymbol()+String.valueOf(recurringChargeEntry.getPrice().doubleValue()));
                      break;
                  }
              }
          }
      }
      characteristicList.add(monthlyPricePlan);
  }

  private void populateDeviceDetails(String baseSite,String lang,String deviceCode, String colorCode, String capacityCode, List<CommunicationRequestCharacteristic> characteristicList) {
      userService.setCurrentUser(userService.getAdminUser());
      catalogVersionService.setSessionCatalogVersion(baseSite+"ProductCatalog","Online");
      baseSiteService.setCurrentBaseSite(baseSite,Boolean.TRUE);
	    TmaPoVariantModel poVariantModel=(TmaPoVariantModel)productService.getProductForCode(catalogVersionService.getSessionCatalogVersionForCatalog(baseSite+"ProductCatalog"),deviceCode);
	    final CommunicationRequestCharacteristic device = new CommunicationRequestCharacteristic();
      device.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.device.name"));
      device.setValue(poVariantModel.getName(Locale.forLanguageTag(lang)));
      characteristicList.add(device);

      final CommunicationRequestCharacteristic color= new CommunicationRequestCharacteristic();
      color.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.device.color"));
      color.setValue(colorCode);
      characteristicList.add(color);

      final CommunicationRequestCharacteristic capacity = new CommunicationRequestCharacteristic();
      capacity.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.device.capacity"));
      capacity.setValue(capacityCode);
      characteristicList.add(capacity);


      final Collection<PriceRowModel> priceRowModelCollection=poVariantModel.getEurope1Prices();
      final CommunicationRequestCharacteristic monthlyPricePlan = new CommunicationRequestCharacteristic();
      monthlyPricePlan.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.device.monthlyPrice.installmentplan"));
      for (final PriceRowModel pricePlanModel: priceRowModelCollection) {
          if (pricePlanModel instanceof SubscriptionPricePlanModel) {
              final Collection <RecurringChargeEntryModel> recurringChargeEntries = ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
              for (final RecurringChargeEntryModel recurringChargeEntry: recurringChargeEntries) {
                      monthlyPricePlan.setValue(recurringChargeEntry.getCurrency().getSymbol()+String.valueOf(recurringChargeEntry.getPrice().doubleValue()));
                      break;
              }
          }
      }
      characteristicList.add(monthlyPricePlan);

      final CommunicationRequestCharacteristic monthlyPriceText = new CommunicationRequestCharacteristic();
      monthlyPriceText.setName(configurationService.getConfiguration().getString("llamulesoftintegration.notification.device.monthlyPrice"));
      monthlyPriceText.setValue(configurationService.getConfiguration().getString(new StringBuffer("llamulesoftintegration.notification.device.monthlyPrice.value.").append(lang).toString()));
      characteristicList.add(monthlyPriceText);

  }

  private boolean isFallbackOrder(OrderModel order){
	  return Objects.nonNull(order.getFalloutReasonValue()) || BooleanUtils.isTrue(order.getIsRequiredServiceActive());
  }

}
