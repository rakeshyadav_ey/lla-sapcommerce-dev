package com.lla.mulesoft.integration.service.impl;

import com.google.gson.Gson;
import com.lla.core.enums.LLAPOType;
import com.lla.core.enums.LLATechnologyEnum;
import com.lla.core.enums.SourceEPC;
import com.lla.core.model.LLABundleProductMappingModel;
import com.lla.core.model.LLAProductMapperModel;
import com.lla.core.model.LLAServiceCodeMappingModel;
import com.lla.core.model.LLAServiceCodeModel;
import com.lla.mulesoft.integration.constants.LlamulesoftintegrationConstants;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.service.LLAMulesoftOrderingService;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import com.lla.telcotmfwebservices.v2.dto.*;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.net.URI;
import java.util.*;

import com.lla.core.model.VTRServiceCodeMappingModel;

import de.hybris.platform.catalog.CatalogVersionService;
import org.springframework.web.client.RestTemplate;

public class LLAMulesoftOrderingServiceImpl extends LLAAbstractService implements LLAMulesoftOrderingService {

    public static final String EXTENSOR_PRODUCT ="extensor_nextgen";
    public static final String EXTENSOR_SIEBEL_CODE="vtr.order.provisioning.extensor.siebel.code";
    List<VTRServiceCodeMappingModel> productCodeMappingList = new ArrayList<>();
    VTRServiceCodeMappingModel mainProduct;

    @Autowired
    LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

    @Autowired
    private FlexibleSearchService flexibleSearchService;
    @Autowired
    private ModelService modelService;
    @Autowired
    private CatalogVersionService catalogVersionService;


    @Override
    public String placeOrder(OrderModel order,final String sourceEpc) throws LLAApiException {
        final String siteID =configurationService.getConfiguration().getString(new StringBuffer(LlamulesoftintegrationConstants.WEBSITE).append(LlamulesoftintegrationConstants.DECIMAL).append(order.getSite().getUid()).append(LlamulesoftintegrationConstants.DECIMAL).append(LlamulesoftintegrationConstants.CODE).toString());
        final String siteCode=order.getSite().getUid();
        HttpHeaders standardHeaders = setupStandardHeaders(order, siteID);
        final URI orderProvisioningURL = URI.create(apiUrlConstructor(getAPIServer(),siteID, LlamulesoftintegrationConstants.PROVISION_ORDER));
        if(LOG.isDebugEnabled()){
            LOG.debug("Order Provisioning start.");
        }
        ProductOrder productOrder = populateOrderObject(order,siteID,sourceEpc);
        String orderBody=new Gson().toJson(productOrder);
        orderBody = orderBody.replace("attype","@type");
        orderBody = orderBody.replace("atbaseType","@type");
        if(llaMulesoftIntegrationUtil.isVtrOrder(order.getSite()))
          orderBody = orderBody.replace("atreferredType","@referredType");
        else
            orderBody = orderBody.replace("atreferredType","@type");
        if(llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())) {
            orderBody = reformattedRequiredAttributes(orderBody);
        }
        LOG.info("Provisioning order with following values:");
        LOG.info(orderBody);
        HttpEntity<String> httpEntity = new HttpEntity<>(orderBody, standardHeaders);
        RestTemplate restTemplate_new=this.createRestTemplate(this.createRequestFactory(configurationService.getConfiguration().getString("max.pool.connection")));
        ResponseEntity<ProductOrder> serviceResponse=null;
        try {

            if(llaMulesoftIntegrationUtil.isVtrOrder(order.getSite()))
            {
                restTemplate_new = setConnectionTimeout(restTemplate_new);
            }
            serviceResponse = restTemplate_new.exchange(orderProvisioningURL, HttpMethod.POST, httpEntity, ProductOrder.class);
            LOG.info("Order provisioned with response:"+serviceResponse.getBody());
            ProductOrder responseOrder = serviceResponse.getBody();
            if(null!=responseOrder && StringUtils.isNotEmpty(responseOrder.getId())){
                LOG.info("Order punched in BSS with ID: " + responseOrder.getId());
                return responseOrder.getId();
            }
        }catch(HttpClientErrorException.BadRequest badRequest){
            LOG.error(String.format("Create Provisioning Failed for customer %s and Order %s due to Bad Request :: %s", order.getUser().getUid(), order.getCode(),badRequest.getResponseBodyAsString()));

        }catch(HttpClientErrorException clientEx){
            LOG.error(String.format("Create Provisioning Failed for customer %s and Order %s due to Client Error Exception  :: %s", order.getUser().getUid(), order.getCode(),clientEx.getMessage()));

        }catch(ResourceAccessException ioException){
            LOG.error(String.format("Exception in making Api Call to Create Provisioning Order for Market:: %s due to  :: %s",siteCode,ioException.getMessage()));
            throw new LLAApiException("Unable to Provision Order ... Error in making API Call");
        }
        finally {
            HttpComponentsClientHttpRequestFactory requestFactory=(HttpComponentsClientHttpRequestFactory) restTemplate_new.getRequestFactory();
            LOG.info("Releasing current client connection for Order Provisioning Creation Request");
            try {
                requestFactory.destroy();
            } catch (Exception exception) {
                LOG.info(String.format("Error in releasing Connection obtained for Order Provisioning Creation for Market  %s and due to %s  ",siteCode,exception));
            }
        }
        return StringUtils.EMPTY;
    }

    private RestTemplate setConnectionTimeout( RestTemplate restTemplate)
    {
        HttpComponentsClientHttpRequestFactory reqFactory = (HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory();
        if(Objects.nonNull(reqFactory))
        {
           // reqFactory.setConnectionRequestTimeout(configurationService.getConfiguration().getInt("llamulesoftintegration.vtr.order.provisioning.timeout"));
            reqFactory.setConnectTimeout(configurationService.getConfiguration().getInt("llamulesoftintegration.vtr.order.provisioning.timeout"));
            reqFactory.setReadTimeout(configurationService.getConfiguration().getInt("llamulesoftintegration.vtr.order.provisioning.timeout"));
        }
        restTemplate.setRequestFactory(reqFactory);
        return restTemplate;
    }

    private String reformattedRequiredAttributes(String orderBody) {
        orderBody =  orderBody.replace("exchangeIdUnderscoreTP","exchangeId_TP");
        orderBody =  orderBody.replace("exchangeIdUnderscoreTI","exchangeId_TI");
        orderBody =  orderBody.replace("exchangeIdUnderscoreDS","exchangeId_DS");
        orderBody =  orderBody.replace("exchangeIdUnderscoreMO","exchangeId_MO");
        orderBody =  orderBody.replace("numberAreaCodeUnderscoreMO","numberAreaCode_MO");
        orderBody = orderBody.replace("numberAreaCodeUnderscoreTI","numberAreaCode_TI");
        orderBody = orderBody.replace("numberAreaCodeUnderscoreTP","numberAreaCode_TP");
        orderBody = orderBody.replace("numberAreaCodeUnderscoreDS","numberAreaCode_DS");
        return orderBody;
    }

    private HttpHeaders setupStandardHeaders(OrderModel order, String siteID) {
        HttpHeaders standardHeaders =  setupHeaders(siteID);
        if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())) {
            standardHeaders.set("Correlation-ID", configurationService.getConfiguration().getString("llamulesoftintegration.order.correlationID"));
        } else if (llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())){
            standardHeaders.set("Correlation-ID", configurationService.getConfiguration().getString("llamulesoftintegration.order.correlationID.panama"));
        }else if(llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite())){
            standardHeaders.set("Correlation-ID", configurationService.getConfiguration().getString("llamulesoftintegration.order.correlationID.puertorico"));
        }else if(llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())){
           standardHeaders.set("Correlation-ID", configurationService.getConfiguration().getString("llamulesoftintegration.order.correlationID.vtr"));
        }
        return standardHeaders;
    }

    private ProductOrder populateOrderObject(OrderModel order, final String siteId, final String sourceEpc) {
        ProductOrder ord = new ProductOrder();
        ord=populateOrderCategory(order, ord);

        SourceEPC orderSystem;
        if(llaMulesoftIntegrationUtil.isVtrOrder(order.getSite()))
        {
            orderSystem = populateVTRProductsAndAccountInOrder(ord, order,sourceEpc);
        }
        else
        {
             orderSystem = populateProductsAndAccountInOrder(ord, order,sourceEpc);
        }
        AddressModel orderAddress = llaMulesoftIntegrationUtil.fetchAddressForOrderProvisioning(order);
        ServiceAddress serviceAddress = new ServiceAddress();
        String addressID = order.getAddressUID();
        if(null == addressID)
            addressID = "";
        if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())) {
            serviceAddress.setAddrNo(addressID.trim());
            serviceAddress.setAddrType(configurationService.getConfiguration().getString("llamulesoftintegration.order.addressType"));
            serviceAddress.setAddrLine1(null!=orderAddress.getLine1()?orderAddress.getLine1().trim():StringUtils.EMPTY);
            serviceAddress.setAddrLine2(null!=orderAddress.getLine2()?orderAddress.getLine2().trim():StringUtils.EMPTY);
            serviceAddress.setAddrLine3(null!=orderAddress.getTown()?orderAddress.getTown().trim():StringUtils.EMPTY);
            serviceAddress.setAddrLine4(null!=orderAddress.getArea()?orderAddress.getArea().trim():StringUtils.EMPTY);
            serviceAddress.setAddrLine5(null!=orderAddress.getCountry() && null!=orderAddress.getCountry().getName()?orderAddress.getCountry().getName().trim():StringUtils.EMPTY);
            serviceAddress.setAddrPostCode(null!=orderAddress.getPostalcode()?orderAddress.getPostalcode():StringUtils.EMPTY);
        } else if(llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())){
            serviceAddress.setAddrType(configurationService.getConfiguration().getString("llamulesoftintegration.order.addressType.panama"));
            String addressLine1 = StringUtils.EMPTY;
			if (!StringUtils.isEmpty(orderAddress.getAppartment()) && !StringUtils.isEmpty(orderAddress.getBuilding()))
			{
				addressLine1 = new StringBuffer(orderAddress.getBuilding())
						.append("-").append(orderAddress.getAppartment()).toString();
			}
			else if (StringUtils.isEmpty(orderAddress.getAppartment()) && !StringUtils.isEmpty(orderAddress.getBuilding()))
			{
				addressLine1 = new StringBuffer(orderAddress.getBuilding()).toString();
			}
			else if (!StringUtils.isEmpty(orderAddress.getAppartment()) && StringUtils.isEmpty(orderAddress.getBuilding()))
			{
				addressLine1 = new StringBuffer(orderAddress.getAppartment()).toString();
			}
			serviceAddress.setAddrLine1(addressLine1);
            serviceAddress.setAddrLine2(orderAddress.getNeighbourhood());
            serviceAddress.setAddrLine3(orderAddress.getTown());
            serviceAddress.setAddrLine4(orderAddress.getDistrict());
            serviceAddress.setAddrLine5(orderAddress.getProvince());
        }
        if(!llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())) {
      	  ord.setServiceAddress(serviceAddress);
           ord.setPriority(PriorityType._1);
        }
        if(null!=order.getRequestedStartDate()) {
            ord.setRequestedStartDate(convertToRfc3339TimeStamp(order.getRequestedStartDate()));
        }
        if(null!=order.getRequestedEndDate()) {
            ord.setRequestedCompletionDate(convertToRfc3339TimeStamp(order.getRequestedEndDate()));
        }
        
        OrderCharacteristics characteristics = new OrderCharacteristics();
        characteristics.setCisEmpId(configurationService.getConfiguration().getString("llamulesoftintegration.order.cisuser"));
        characteristics.setDepttCode(order.getDepttCode());
        characteristics.setSiteCode(order.getSiteCode());
        if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())) {
            characteristics.setSourceOfApplication(configurationService.getConfiguration().getString("llamulesoftintegration.order.applicationSource"));
            characteristics.setSalesRepId(configurationService.getConfiguration().getString("llamulesoftintegration.order.salesRepId"));
            characteristics.setCarrierUniqueServId(configurationService.getConfiguration().getString("llamulesoftintegration.order.carrierUniqueServiceId"));
            characteristics.setLastLineNumber(configurationService.getConfiguration().getString("llamulesoftintegration.order.lastLineNo"));
            characteristics.setExchangeId(order.getExchangeId());
            StringBuilder numberAreaCode = new StringBuilder();
            if(SourceEPC.CERILLION.equals(orderSystem)) {
                numberAreaCode.append(order.getNetworkId());
            }else {
                numberAreaCode.append(order.getNumberAreaCode());
            }
            characteristics.setNumberAreaCode(numberAreaCode.toString());
        }else if (llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())){

            characteristics.setCisEmpId(configurationService.getConfiguration().getString("llamulesoftintegration.order.cisuser.panama"));
            characteristics.setSourceOfApplication(configurationService.getConfiguration().getString("llamulesoftintegration.order.applicationSource.panama"));
            characteristics.setSalesRepId(configurationService.getConfiguration().getString("llamulesoftintegration.order.salesRepId.panama"));
            characteristics.setExchangeIdUnderscoreTP(order.getExchangeId_TP());
            characteristics.setNumberAreaCodeUnderscoreTP(order.getNumberAreaCode_TP());
            characteristics.setExchangeIdUnderscoreTI(order.getExchangeId_TI());
            characteristics.setNumberAreaCodeUnderscoreTI(order.getNumberAreaCode_TI());

            if("HFC".equalsIgnoreCase(llaMulesoftIntegrationUtil.getProductType(order))){
                characteristics.setExchangeIdUnderscoreMO(order.getExchangeId_MO());
                characteristics.setNumberAreaCodeUnderscoreMO(order.getNumberAreaCode_MO());
            }else if("FTTH".equalsIgnoreCase(llaMulesoftIntegrationUtil.getProductType(order))){
                characteristics.setExchangeIdUnderscoreDS(order.getExchangeId_DS());
                characteristics.setNumberAreaCodeUnderscoreDS(order.getNumberAreaCode_DS());
            }
        }

        if(!llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())) {
      	  ord.setOrderCharacteristics(characteristics);
      	  ord.setAtbaseType("ProductOrder");
        }
        return ord;
    }

    private SourceEPC populateVTRProductsAndAccountInOrder(ProductOrder ord, OrderModel order, String sourceEpc)
    {
        List<ProductOrderItem> items = new ArrayList<>();
        SourceEPC orderSource = null;
        List<Note> notes = new ArrayList<>();
        String technologyServiced = order.getDeliveryAddress().getTechnologyServiced().getCode();

        for(AbstractOrderEntryModel entry: order.getEntries())
        {

            catalogVersionService.setSessionCatalogVersion(order.getSite().getUid() + "ProductCatalog", "Online");
           if(!llaMulesoftIntegrationUtil.containsFirstLevelCategoryWithCode(entry.getProduct(),LlamulesoftintegrationConstants.INSTALLATION))
           {
               ProductOrderItem item = new ProductOrderItem();
               List<String> discountedItems = new ArrayList<>();
               List<String> additionalExtensorCodes = new ArrayList<>();
               
               
               VTRServiceCodeMappingModel seibelProduct = null;


               // TBD after setting technology at Order Level
               if (Objects.nonNull(order.getDeliveryAddress()) && Objects.nonNull(order.getDeliveryAddress().getTechnologyServiced()))
               {
                   productCodeMappingList = llaMulesoftIntegrationUtil.getProductCodeMappingForTechnology(technologyServiced);
                   if (null != productCodeMappingList && CollectionUtils.isNotEmpty(productCodeMappingList))
                   {
                       for (VTRServiceCodeMappingModel codeMap : productCodeMappingList)
                       {
                           if ((codeMap.getHybrisProductCode()).equals(entry.getProduct().getCode()) && Objects.nonNull(codeMap.getTechnology()) && technologyServiced.equalsIgnoreCase(codeMap.getTechnology().getCode()))
                           {
                               item.setId(codeMap.getSeibelProductCode());
                               seibelProduct = codeMap;
                               if (CollectionUtils.isNotEmpty(codeMap.getDiscountedSeibelProductCode()))
                               {
                                   codeMap.getDiscountedSeibelProductCode().forEach(code -> discountedItems.add(code));
                               }
                               if (CollectionUtils.isNotEmpty(codeMap.getAdditionalExtensorCode()))
                               {
                                   codeMap.getAdditionalExtensorCode().forEach(code -> additionalExtensorCodes.add(code));
                               }
                               if(codeMap.getIsBundle())
                               {
                                   mainProduct = codeMap;
                               }
                           }
                       }
                   }
               }

               if (EXTENSOR_PRODUCT.equals(entry.getProduct().getCode())
                       && entry.getQuantity().intValue() < 3 && llaMulesoftIntegrationUtil.isValidMainProductPresent(mainProduct))
               {
            	   if(technologyServiced.equalsIgnoreCase(LLATechnologyEnum.FTTH.getCode())) {
            		   item.setQuantity(entry.getQuantity().intValue() + 1);
            	   }else {
            		   item.setQuantity(entry.getQuantity().intValue());
            	   }
               }
               else
                   item.setQuantity(entry.getQuantity().intValue());
               item.setAction("add");

               if (Objects.nonNull(seibelProduct) && seibelProduct.getIsBundle())
               {
                   Product product = new Product();
                   product.setIsBundle(Boolean.TRUE);
                   product.setAtbaseType("Product");


                   if ( CollectionUtils.isNotEmpty(entry.getProduct().getSupercategories()))
                   {
                       final List<Characteristic> productCharacteristic = new ArrayList<>();
                       Characteristic productChar = new Characteristic();

                       if (llaMulesoftIntegrationUtil.containsFirstLevelCategoryWithCode(entry.getProduct(), LlamulesoftintegrationConstants.DEVICES_CATEGORY)) {
                           productChar.setValue("MOBILE");
                           item.setAttype("Package");
                       } else if (llaMulesoftIntegrationUtil.containsFirstLevelCategoryWithCode(entry.getProduct(), LlamulesoftintegrationConstants.ADDON)) {
                           productChar.setValue("FIXED");
                           item.setAttype("AddOn");
                       } else if (llaMulesoftIntegrationUtil.containsSecondLevelCategoryWithCode(entry.getProduct(), LlamulesoftintegrationConstants.BUNDLE)) {
                           productChar.setValue("FIXED");
                           item.setAttype("Bundle");
                       } else {
                           productChar.setValue("FIXED");
                           item.setAttype("Package");
                       }

                       productChar.setName(configurationService.getConfiguration().getString("llamulesoftintegration.order.productType"));
                       productChar.setValueType("string");
                       LOG.info("productChar: " + productChar);
                       productCharacteristic.add(productChar);
                       product.setProductCharacteristic(productCharacteristic);
                   }

                   item.setProduct(product);
               }
               else
               {
                   item.setAttype(setAtType(entry,seibelProduct));
               }
               if (!llaMulesoftIntegrationUtil.containsSecondLevelCategoryWithCode(entry.getProduct(), LlamulesoftintegrationConstants.BUNDLE) && !llaMulesoftIntegrationUtil.containsFirstLevelCategoryWithCode(entry.getProduct(), LlamulesoftintegrationConstants.INSTALLATION))
               {
                   List<ProductOrderItemRelationship> productOrderItemRelationship = createProductOrderItemRelationship(seibelProduct, item, Optional.ofNullable(seibelProduct.getCategoryId()));
                   item.setProductOrderItemRelationship(productOrderItemRelationship);
               }
                   if (CollectionUtils.isNotEmpty(seibelProduct.getAdditionalSeibelProductCode()))
                   {
                       addAditionalProducts(seibelProduct, item, entry);
                   }



               if (CollectionUtils.isNotEmpty(discountedItems))
               {

                   discountedItems.forEach(discount -> {
                       ProductOrderItem discountedItem = new ProductOrderItem();

                      for( VTRServiceCodeMappingModel product: productCodeMappingList)
                      {
                          if(discount.equalsIgnoreCase(product.getSeibelProductCode()))
                          {
                              discountedItem.setId(product.getSeibelProductCode());
                              if(EXTENSOR_PRODUCT.equalsIgnoreCase(entry.getProduct().getCode()))
                              {

                                  discountedItem.setQuantity(getExtensorQuantity((OrderModel) entry.getOrder(),llaMulesoftIntegrationUtil.isValidMainProductPresent(mainProduct)));
                              }
                              else
                                 discountedItem.setQuantity(entry.getQuantity().intValue());
                              discountedItem.setAction("add");
                              // discountedItem.setProduct(product);
                              discountedItem.setProductOrderItemRelationship(createProductOrderItemRelationship(product,
                                      discountedItem, product.getIsBundle() ? Optional.empty() : Optional.ofNullable(product.getCategoryId())));

                              discountedItem.setAttype(setAtType(entry,product));
                              items.add(discountedItem);
                          }
                      }


                   });

               }
               
               if (CollectionUtils.isNotEmpty(additionalExtensorCodes) && technologyServiced.equalsIgnoreCase(LLATechnologyEnum.HFC.getCode()))
               {
            	   additionalExtensorCodes.forEach(code -> {
                      ProductOrderItem additionalExtensorItem = new ProductOrderItem();
                      for( VTRServiceCodeMappingModel product: productCodeMappingList)
                      {
                          if(code.equalsIgnoreCase(product.getSeibelProductCode()))
                          {
                        	  additionalExtensorItem.setId(product.getSeibelProductCode());
                           	  additionalExtensorItem.setQuantity(1);//1
                              additionalExtensorItem.setAction("add");
                              List<ProductOrderItemRelationship> productOrderItemRelationships = new ArrayList<>();
                              ProductOrderItemRelationship productOrderItemRelationship = new ProductOrderItemRelationship();
                              productOrderItemRelationship.setId(configurationService.getConfiguration().getString(EXTENSOR_SIEBEL_CODE));
                              productOrderItemRelationship.setRelationshipType("BaseProduct");
                              productOrderItemRelationships.add(productOrderItemRelationship);
                              additionalExtensorItem.setProductOrderItemRelationship(productOrderItemRelationships);
                              additionalExtensorItem.setAttype(setAtType(entry,product));
                              items.add(additionalExtensorItem);
                          }
                      }
                   });

           }
               if (StringUtils.isNotEmpty(seibelProduct.getOfertaNational())) {


                   VTRServiceCodeMappingModel finalSeibelProduct = seibelProduct;
                   productCodeMappingList.forEach(mapping -> {
                       if (finalSeibelProduct.getOfertaNational().equalsIgnoreCase(mapping.getHybrisProductCode())) {
                           ProductOrderItem ofertaNational = new ProductOrderItem();
                           ofertaNational.setId(mapping.getSeibelProductCode());
                           ofertaNational.setAction("add");
                           ofertaNational.setQuantity(1);
                           ofertaNational.setAttype("Package");
                           if (CollectionUtils.isNotEmpty(mapping.getAdditionalSeibelProductCode())) {
                               mapping.getAdditionalSeibelProductCode().forEach(code -> {
                                   addAditionalProducts(mapping, ofertaNational, entry);
                               });
                           }
                           items.add(ofertaNational);
                       }
                   });

               }



               items.add(item);
           }
        }
       // setVtrMiscellaneousAddons(order, items, ord);
        if(technologyServiced.equalsIgnoreCase(LLATechnologyEnum.FTTH.getCode())) {
        	checkExtensorProduct(order, productCodeMappingList,items);
        }

        if (llaMulesoftIntegrationUtil.isVtrOrder(order.getSite()))
        {
            populateRelatedParty(order, ord);
            ord.setAtbaseType("ProductOrder");
        }

        CustomerModel customer = (CustomerModel)order.getUser();
        BillingAccountRef billingAccount = new BillingAccountRef();
        if(llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())) {
            Note note = setBillingAccountAndNote(billingAccount,order,customer);
            notes.add(note);
        }else if(null!=order.getBillingAccount()){
            billingAccount.setId(order.getBillingAccount().getBillingAccountId());
        }else if(null != orderSource && orderSource.equals(SourceEPC.LIBERATE) && CollectionUtils.isNotEmpty(customer.getLiberateAccounts())){
            billingAccount.setId(customer.getLiberateAccounts().get(customer.getLiberateAccounts().size()-1));
        }else if(orderSource.equals(SourceEPC.CERILLION) && CollectionUtils.isNotEmpty(customer.getCerillionAccounts())){
            billingAccount.setId(customer.getCerillionAccounts().get(customer.getCerillionAccounts().size()-1));
        }
        ord.setBillingAccount(billingAccount);
        ord.setNote(notes);
        ord.setProductOrderItem(items);
        return orderSource;
    }

    private void checkExtensorProduct(OrderModel order, List<VTRServiceCodeMappingModel> productCodeMappingList, List<ProductOrderItem> items)
    {
        boolean containsFreeExtensor = (llaMulesoftIntegrationUtil.isValidMainProductPresent(mainProduct));
        boolean isExtensor = order.getEntries().stream().anyMatch(entryModel -> EXTENSOR_PRODUCT.equals(entryModel.getProduct().getCode()));
        if(containsFreeExtensor && !isExtensor)
        {
            for (VTRServiceCodeMappingModel mappingList: productCodeMappingList)
            {
                if(EXTENSOR_PRODUCT.equalsIgnoreCase(mappingList.getHybrisProductCode()))
                {
                    List<ProductOrderItem> productOrderItems = new ArrayList<>();
                    ProductOrderItem extensor = new ProductOrderItem();
                    extensor.setId(mappingList.getSeibelProductCode());
                    extensor.setAction("add");
                    extensor.setQuantity(1);
                    extensor.setAttype("Addon");
                    extensor.setProductOrderItemRelationship(createProductOrderItemRelationship(mappingList,extensor, Optional.ofNullable(mappingList.getCategoryId())));
                    if(CollectionUtils.isNotEmpty(mappingList.getAdditionalSeibelProductCode()))
                    {
                        mappingList.getAdditionalSeibelProductCode().forEach(code -> {
                            ProductOrderItem productOrderItem = new ProductOrderItem();
                            productOrderItem.setId(code);
                            productOrderItem.setAction("add");
                            productOrderItem.setQuantity(1);
                            productOrderItem.setProductOrderItemRelationship(createProductOrderItemRelationship(mappingList, productOrderItem, Optional.empty()));
                            productOrderItem.setAttype("Addon");
                            productOrderItems.add(productOrderItem);
                            extensor.setProductOrderItem(productOrderItems);
                        });
                    }
                    if(CollectionUtils.isNotEmpty(mappingList.getDiscountedSeibelProductCode()))
                    {
                        mappingList.getDiscountedSeibelProductCode().forEach(discount -> {
                            ProductOrderItem discountedItem = new ProductOrderItem();

                            for( VTRServiceCodeMappingModel product: productCodeMappingList)
                            {
                                if(discount.equalsIgnoreCase(product.getSeibelProductCode()))
                                {
                                    discountedItem.setId(product.getSeibelProductCode());

                                    discountedItem.setQuantity(1);
                                    discountedItem.setAction("add");
                                    // discountedItem.setProduct(product);
                                    discountedItem.setProductOrderItemRelationship(createProductOrderItemRelationship(product,
                                            discountedItem, product.getIsBundle() ? Optional.empty() : Optional.ofNullable(product.getCategoryId())));

                                    discountedItem.setAttype("Addon");
                                    items.add(discountedItem);
                                }
                            }

                        });
                    }
                    items.add(extensor);
                }
            }


        }
    }

    private String setAtType(AbstractOrderEntryModel entry, VTRServiceCodeMappingModel seibelProduct)
    {
        if(seibelProduct.getHybrisProductCode().equalsIgnoreCase(entry.getProduct().getCode()) && llaMulesoftIntegrationUtil.containsFirstLevelCategoryWithCode(entry.getProduct(), LlamulesoftintegrationConstants.ADDON))
        {
            return "Addon";
        }
         else if (!seibelProduct.getHybrisProductCode().equalsIgnoreCase(entry.getProduct().getCode()) && !llaMulesoftIntegrationUtil.containsFirstLevelCategoryWithCode(entry.getProduct(), LlamulesoftintegrationConstants.ADDON) && seibelProduct.getIsBundle())
         {
            return "Package";
        }
       return "Addon";
    }

    private List<ProductOrderItemRelationship> createProductOrderItemRelationship(VTRServiceCodeMappingModel seibelProduct, ProductOrderItem item, Optional<String> categoryId)
    {
        List<ProductOrderItemRelationship> productOrderItemRelationships = new ArrayList<>();
        ProductOrderItemRelationship productOrderItemRelationship = new ProductOrderItemRelationship();
        if(categoryId.isPresent())
        {
            productOrderItemRelationship.setId(categoryId.get());
            productOrderItemRelationship.setRelationshipType("Category");

        }
       /* else if("1-10CGOT".equalsIgnoreCase(item.getId()))
        {
            productOrderItemRelationship.setId("Banda Ancha");
            productOrderItemRelationship.setRelationshipType("Category");
        }*/
        else
        {
           productOrderItemRelationship.setId(seibelProduct.getSeibelProductCode());
           productOrderItemRelationship.setRelationshipType("BaseProduct");
        }

        productOrderItemRelationships.add(productOrderItemRelationship);

        return productOrderItemRelationships;
    }

    private void addAditionalProducts(VTRServiceCodeMappingModel seibelProduct, ProductOrderItem item, AbstractOrderEntryModel entry)
    {
        List<ProductOrderItem> productOrderItems = new ArrayList<>();
        seibelProduct.getAdditionalSeibelProductCode().forEach(code ->{
            ProductOrderItem productOrderItem = new ProductOrderItem();
            productOrderItem.setId(code);
            productOrderItem.setAction("add");
            if(EXTENSOR_PRODUCT.equalsIgnoreCase(entry.getProduct().getCode()))
            {

                 productOrderItem.setQuantity(getExtensorQuantity((OrderModel) entry.getOrder(),llaMulesoftIntegrationUtil.isValidMainProductPresent(mainProduct)));
            }
            else
               productOrderItem.setQuantity(entry.getQuantity().intValue());
            productOrderItem.setProductOrderItemRelationship(createProductOrderItemRelationship(seibelProduct, item, Optional.empty()));
            productOrderItem.setAttype(item.getAttype());
            productOrderItems.add(productOrderItem);
            item.setProductOrderItem(productOrderItems);
        });


        item.setProductOrderItem(productOrderItems);

    }

    private ProductOrder populateOrderCategory(OrderModel order, ProductOrder ord) {
        switch(order.getSite().getUid()){
            case "jamaica":
                ord.setCategory(configurationService.getConfiguration().getString("llamulesoftintegration.order.orderCategory"));
                ord.setDescription(configurationService.getConfiguration().getString("llamulesoftintegration.order.orderDescription"));
                break;
            case "panama":
                ord.setCategory(configurationService.getConfiguration().getString("llamulesoftintegration.order.orderCategory.panama"));
                break;
            case "puertorico" :
                ord.setCategory(configurationService.getConfiguration().getString("llamulesoftintegration.order.orderCategory.puertorico"));
                break;
         }
         return ord;
    }

    private SourceEPC populateProductsAndAccountInOrder(ProductOrder ord, OrderModel order, String sourceEpc) {
        List<ProductOrderItem> items = new ArrayList<>();
        SourceEPC orderSource = null;
        List<Note> notes = new ArrayList<>();
        for(AbstractOrderEntryModel entry: order.getEntries())
        {
            LLAProductMapperModel productMapper = entry.getProductMapper();
            ProductOrderItem item = new ProductOrderItem();
            ProductOrderItem discountedItem = null;
            /*if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())){
             item.setId(productMapper.getSourceSystemCode());
            } else if(llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())){
                if(null==entry.getProductMapper() && entry.getProduct().getCode().equalsIgnoreCase("SETTOP_BOX")){
                    item.setId(configurationService.getConfiguration().getString("product.settopbox.bsscode"));
                }else{
                    item.setId(productMapper.getSourceSystemCode());
                }
            }*/
             if (null!=productMapper) {
            	item.setId(productMapper.getSourceSystemCode());
            }


             item.setQuantity(entry.getQuantity().intValue());
            item.setAction("add");
            Product product = new Product();
            if(null==productMapper && llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())) {
                    product.setIsBundle(Boolean.FALSE);
                    product.setAtbaseType("AddonProduct");
            }else if(null!=productMapper && llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite()) && productMapper.getInputType().equals(LLAPOType.ADDON)) {
                product.setIsBundle(Boolean.FALSE);
                product.setAtbaseType("AddonProduct");
            }else if(null!=productMapper && productMapper.getInputType().equals(LLAPOType.BUNDLEID)){
                    product.setIsBundle(Boolean.TRUE);
                    product.setAtbaseType("Product");
             }else{
                    product.setIsBundle(Boolean.FALSE);
                product.setAtbaseType("Product");
             }

            if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite()) && null!=((OrderEntryModel)entry).getSimNumber()) {
                List<Characteristic> productCharacteristic = new ArrayList<>();
                Characteristic simPrefix = new Characteristic();
                simPrefix.setName(configurationService.getConfiguration().getString("llamulesoftintegration.order.simCardPrefixField"));
                simPrefix.setValueType("string");
                simPrefix.setValue(((OrderEntryModel) entry).getSimCardSerialPrefix());
                productCharacteristic.add(simPrefix);
                Characteristic simCardNo = new Characteristic();
                simCardNo.setName(configurationService.getConfiguration().getString("llamulesoftintegration.order.simCardNumberField"));
                simCardNo.setValueType("string");
                simCardNo.setValue(((OrderEntryModel) entry).getSimNumber());
                productCharacteristic.add(simCardNo);
                product.setProductCharacteristic(productCharacteristic);
            }


            item.setProduct(product);
            item.setAttype("ProductOrderItem");

            if(null != discountedItem ) {
            	discountedItem.setQuantity(entry.getQuantity().intValue());
            	discountedItem.setAction("add");
            	discountedItem.setProduct(product);
            	discountedItem.setAttype(item.getAttype());
            	items.add(discountedItem);
            }
            items.add(item);
            if(null != productMapper && llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())) {
            	orderSource = productMapper.getSourceEPC();
            }else if(llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())) {
            	orderSource = SourceEPC.valueOf(sourceEpc);
            } else if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())) {
                orderSource = productMapper.getSourceEPC();
            }      
        }

        CustomerModel customer = (CustomerModel)order.getUser();
        BillingAccountRef billingAccount = new BillingAccountRef();
        if(llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())) {
      	  Note note = setBillingAccountAndNote(billingAccount,order,customer);
      	  notes.add(note);
        }else if(null!=order.getBillingAccount()){
            billingAccount.setId(order.getBillingAccount().getBillingAccountId());
    	  }else if(null != orderSource && orderSource.equals(SourceEPC.LIBERATE) && CollectionUtils.isNotEmpty(customer.getLiberateAccounts())){
            billingAccount.setId(customer.getLiberateAccounts().get(customer.getLiberateAccounts().size()-1));
        }else if(orderSource.equals(SourceEPC.CERILLION) && CollectionUtils.isNotEmpty(customer.getCerillionAccounts())){
            billingAccount.setId(customer.getCerillionAccounts().get(customer.getCerillionAccounts().size()-1));
        }
        ord.setBillingAccount(billingAccount);
        ord.setNote(notes);
        ord.setProductOrderItem(items);
        return orderSource;
    }



    /**
     *
     */
    private int getExtensorQuantity(OrderModel order, boolean isValidMainProductPresent)
    {
        boolean isExtensor = order.getEntries().stream().anyMatch(entryModel -> EXTENSOR_PRODUCT.equals(entryModel.getProduct().getCode()));
        if(isExtensor)
        {
            for( AbstractOrderEntryModel entry : order.getEntries())
            {
                if(EXTENSOR_PRODUCT.equalsIgnoreCase(entry.getProduct().getCode()) )
                {
                    if(isValidMainProductPresent)
                    {
                    	if(order.getDeliveryAddress().getTechnologyServiced().getCode().equalsIgnoreCase(LLATechnologyEnum.HFC.getCode())) {
                    		return entry.getQuantity().intValue();
                    	}else {
                    		return entry.getQuantity().intValue() == 3 ? entry.getQuantity().intValue() : entry.getQuantity().intValue()+1;
                    	}
                    }
                    else if(!isValidMainProductPresent)
                    {
                        return entry.getQuantity().intValue();
                    }
                }
            }
        }

        return 0;
    }

    @Override
    public String lcprPlaceOrder(OrderModel order,final String sourceEpc) throws LLAApiException {
        final String siteID =configurationService.getConfiguration().getString(new StringBuffer(LlamulesoftintegrationConstants.WEBSITE).append(LlamulesoftintegrationConstants.DECIMAL).append(order.getSite().getUid()).append(LlamulesoftintegrationConstants.DECIMAL).append(LlamulesoftintegrationConstants.CODE).toString());
        HttpHeaders standardHeaders = setupStandardHeaders(order, siteID);
        final URI orderProvisioningURL = URI.create(apiUrlConstructor(getAPIServer(),siteID, LlamulesoftintegrationConstants.PROVISION_ORDER));
        ProductOrder productOrder = populateLCPROrderObject(order,siteID,sourceEpc);
        String orderBody=new Gson().toJson(productOrder);
        orderBody = orderBody.replace("attype","@type");
        orderBody = orderBody.replace("atreferredType","@referredType");

        LOG.info("Provisioning order with following values:");
        LOG.info(orderBody);
        HttpEntity<String> httpEntity = new HttpEntity<>(orderBody, standardHeaders);
        RestTemplate restTemplate_new=this.createRestTemplate(this.createRequestFactory(configurationService.getConfiguration().getString("max.pool.connection")));
        ResponseEntity<ProductOrder> serviceResponse=null;
        try {
            serviceResponse = restTemplate_new.exchange(orderProvisioningURL, HttpMethod.POST, httpEntity, ProductOrder.class);
            LOG.info("Order provisioned with response:"+serviceResponse.getBody());
            ProductOrder responseOrder = serviceResponse.getBody();
            if(null!=responseOrder && StringUtils.isNotEmpty(responseOrder.getId())){
                LOG.info("Order punched in BSS with ID: " + responseOrder.getId());
                final String billingAccountId=responseOrder.getBillingAccount().getId();
                final CustomerModel customer=(CustomerModel) order.getUser();
                List<String> csgAccounts = new ArrayList<>();
                if(CollectionUtils.isNotEmpty(customer.getCsgAccounts())){
                    csgAccounts = new ArrayList<>(customer.getCsgAccounts());
                }
                csgAccounts.add(billingAccountId);
                customer.setCsgAccounts(csgAccounts);
                modelService.save(customer);
                modelService.refresh(customer);
                LOG.info(String.format("Billing Account :: %s created for Order :: %s  ",billingAccountId,order.getCode()));
                return responseOrder.getId();
            }
        }catch(HttpClientErrorException.BadRequest badRequest){
            LOG.error(String.format("Create Provisioning Failed for customer %s and Order %s due to Bad Request :: %s", order.getUser().getUid(), order.getCode(),badRequest.getResponseBodyAsString()));

        }catch(HttpClientErrorException clientEx){
            LOG.error(String.format("Create Provisioning Failed for customer %s and Order %s due to Client Error Exception  :: %s", order.getUser().getUid(), order.getCode(),clientEx.getMessage()));

        }catch(ResourceAccessException ioException){
            LOG.error(String.format("Exception in making Api Call :: %s",ioException.getMessage()));
            throw new LLAApiException("Unable to Provision Order ... Error in making API Call");
        }
        return StringUtils.EMPTY;
    }


    /**
     * Populate LCPR Product Order
     * @param order
     * @param siteID
     * @param sourceEpc
     * @return
     */
    private ProductOrder populateLCPROrderObject(OrderModel order, String siteID, String sourceEpc)  throws LLAApiException {
        ProductOrder ord = new ProductOrder();
        ord=populateOrderCategory(order, ord);
        AddressModel orderAddress = llaMulesoftIntegrationUtil.fetchAddressForOrderProvisioning(order);
        ServiceAddress serviceAddress = new ServiceAddress();
        serviceAddress.setAddrNo(order.getLocationId());
        ord.setServiceAddress(serviceAddress);
        if(null!=order.getRequestedEndDate()) {
            ord.setRequestedCompletionDate(convertToRfc3339TimeStamp(order.getRequestedEndDate()));
        }
        ord.setRequestedCompletionDate(configurationService.getConfiguration().getString("llamulesoftintegration.order.puertorico.requestedcompletiondate"));
        OrderCharacteristics characteristics = new OrderCharacteristics();
       // characteristics.setSalesRepId(order.getSalesRepId());
        characteristics.setSalesRepId(configurationService.getConfiguration().getString("llamulesoftintegration.order.salesRepId.puertorico"));
	ord.setOrderCharacteristics(characteristics);
        LLAServiceCodeMappingModel serviceCode=populateProductOrderItem(ord,order);
        if(null!=serviceCode){
            ord.setNote(populateOrderNoteObject(serviceCode.getReasonCode().getCode(),order));
            populateRelatedParty(order,ord);
            ord.setAttype("ProductOrder");
            return ord;
        }else{
            LOG.error(String.format("No Service Code available for the  business unit %s and technology %s",order.getBusinessUnit(),order.getDropType().getCode()));
            throw new LLAApiException("No service code available");
        }


    }

    /**
     *  Generate Product ORder Item for API
     * @param ord
     * @param order
     * @return
     */
    private LLAServiceCodeMappingModel populateProductOrderItem(ProductOrder ord, OrderModel order)  {
        AbstractOrderModel abstractOrder=(AbstractOrderModel)order;
        List<ProductOrderItem> items = new ArrayList<>();
        List<LLABundleProductMappingModel> bundleProductMappingList=abstractOrder.getBundleProductMapping();
        Optional<LLABundleProductMappingModel> filteredBundleTechnology=bundleProductMappingList.stream().filter(bundleMapping->bundleMapping.getTechnology().equals(order.getDropType())).findFirst();
        LLABundleProductMappingModel bundleProductMappingModel=filteredBundleTechnology.get();
        LLAServiceCodeMappingModel serviceCodeMapping=llaMulesoftIntegrationUtil.getTechnologyMapping(bundleProductMappingModel,order);
        if(null!=serviceCodeMapping){
            final Collection<String> serviceCodeList=serviceCodeMapping.getServiceCodesList();
            for(String serviceCode:serviceCodeList){
                String serviceCodeToken[]=serviceCode.split(":");
                createProductOrderItem(order, items, serviceCodeMapping, serviceCodeToken[0],serviceCodeToken[1]);
            }
                addMiscellaneousProductsItems(order,items,serviceCodeMapping,null);


        }
        ord.setProductOrderItem(items);
        return serviceCodeMapping;
    }

    /**
     * Create Product Order Item
     * @param order
     * @param items
     * @param serviceCodeMapping
     * @param serviceCode
     */
    private void createProductOrderItem(OrderModel order, List<ProductOrderItem> items, LLAServiceCodeMappingModel serviceCodeMapping, String serviceCode,String serviceType) {
        ProductOrderItem poItem=new ProductOrderItem();
        poItem.setId(serviceType.equalsIgnoreCase("Package")? serviceCodeMapping.getPackageId(): fetchServiceCode(order, serviceCode));

        poItem.setQuantity(1);
        poItem.setAction("add");
        Product product=new Product();
        poItem.setAttype(serviceType);
        /*switch(serviceCodeToken[1]){
            case "Package":
                poItem.setAttype(serviceCode.getProductType().getCode());
                break;
            case "AddonProduct":
                poItem.setAttype(serviceCode.getProductType().getCode());
                break;
            case "CallTypeProduct":
                poItem.setAttype(serviceCode.getProductType().getCode());
                break;
        }
*/
        product.setId(fetchServiceCode(order, serviceCode));
        product.setAttype("Product");
        product.setIsBundle(serviceType.equalsIgnoreCase("Package")?Boolean.TRUE:Boolean.FALSE);
        List<Characteristic> productCharacteristicList=new ArrayList();
        Characteristic characteristic=new Characteristic();
        characteristic.setName("ServiceType");
        characteristic.setValue(getLobCode(serviceCode));
        characteristic.setValueType("String");
        productCharacteristicList.add(characteristic);
        product.setProductCharacteristic(productCharacteristicList);
        poItem.setProduct(product);
        items.add(poItem);
    }

    private String getLobCode(String serviceCode) {
        return getMiscellaneousServiceCode(serviceCode).getLobCode();
    }

    /**
     *  Add Miscellaneous Product Order Items at API
     * @param order
     * @param productOrderItems
     * @param serviceCodeMapping
     * @param serviceCode
     */
    private void addMiscellaneousProductsItems(OrderModel order,List<ProductOrderItem> productOrderItems, LLAServiceCodeMappingModel serviceCodeMapping, LLAServiceCodeModel serviceCode)  {
        for(AbstractOrderEntryModel entryModel:order.getEntries()){

            switch (entryModel.getProduct().getCode()){
                case "smart_protect_5":
                    String smart_pr_5_code=configurationService.getConfiguration().getString(new StringBuilder("lcpr.miscellaneous.servicecode.").append(entryModel.getProduct().getCode()).toString());
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,smart_pr_5_code,"AddonProduct");
                    break;
                case "smart_protect_3":
                    String smart_pr_3_code=configurationService.getConfiguration().getString(new StringBuilder("lcpr.miscellaneous.servicecode.").append(entryModel.getProduct().getCode()).toString());
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,smart_pr_3_code,"AddonProduct");
                    break;
                case "RL4PL":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RL4PL","AddonProduct");
                    break;

                case "RL4HD":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RL4HD","AddonProduct");
                    break;

                case "RL1SP":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RL1SP","AddonProduct");
                    break;

                case "RL1ES":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RL1ES","AddonProduct");
                    break;

                case "RL1ED":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RL1ED","AddonProduct");
                    break;

                case "RL1KD":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RL1KD","AddonProduct");
                    break;

                case "RLMSP":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RLMSP","AddonProduct");
                    break;

                case "RLMEN":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RLMEN","AddonProduct");
                    break;

                case "RLMKD":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RLMKD","AddonProduct");
                    break;

                case "RLMMO":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RLMMO","AddonProduct");
                    break;

                case "RLMDN":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RLMDN","AddonProduct");
                    break;

                case "RLMHT":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RLMHT","AddonProduct");
                    break;

                case "RLMLE":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RLMLE","AddonProduct");
                    break;
                case "RLMAC":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RLMAC","AddonProduct");
                    break;

                case "RP1HB":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RP1HB","AddonProduct");
                    break;

                case "RP3S8":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RP3S8","AddonProduct");
                    break;
                case "RP4SZ":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RP4SZ","AddonProduct");
                    break;

                case "RP6VM":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RP6VM","AddonProduct");
                    break;

                case "RP7CL":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RP7CL","AddonProduct");
                    break;
		case "RP2CX":
                    createProductOrderItem(order,productOrderItems,serviceCodeMapping,"RP2CX","AddonProduct");
                    break;
			    
                case "HUB_TV_BOX":
                    //String hub_tv_box_code=configurationService.getConfiguration().getString(new StringBuilder("lcpr.miscellaneous.servicecode.").append(entryModel.getProduct().getCode().toLowerCase()).toString());
                    mapOptionalServiceCodesForHD_HUBTV(order, productOrderItems, serviceCodeMapping);
                    break;
                case "HD_BOX":
                   // String hd_box_code=configurationService.getConfiguration().getString(new StringBuilder("lcpr.miscellaneous.servicecode.").append(entryModel.getProduct().getCode().toLowerCase()).toString());
                    mapOptionalServiceCodesForHD_HUBTV(order, productOrderItems, serviceCodeMapping);
                    break;
            }
        }
    }

    /**
     * Map Optional Service Codes
     * @param order
     * @param productOrderItems
     * @param serviceCodeMapping
     *
     */
    private void mapOptionalServiceCodesForHD_HUBTV(OrderModel order, List<ProductOrderItem> productOrderItems, LLAServiceCodeMappingModel serviceCodeMapping) {
        final Collection<String> optionalListServiceCodes = serviceCodeMapping.getOptionalServiceCodesList();
        for(String optionalServiceCode:optionalListServiceCodes){
            String optionalServiceCodeToken[]=optionalServiceCode.split(":");
            createProductOrderItem(order, productOrderItems, serviceCodeMapping, optionalServiceCodeToken[0],optionalServiceCodeToken[1]);
        }

    }

    /**
     * Get Miscellaneous Service Code
     * @param miscServiceCode
     * @return
     */
    private LLAServiceCodeModel getMiscellaneousServiceCode(String miscServiceCode) {
      final LLAServiceCodeModel llaServiceCodeModel=new LLAServiceCodeModel();
      llaServiceCodeModel.setServiceCode(miscServiceCode);
      LLAServiceCodeModel receivedServiceCode=flexibleSearchService.getModelByExample(llaServiceCodeModel);
      return receivedServiceCode;

    }

    /**
     * Fetch Service Code
     * @param order
     * @param serviceCode
     * @return
     */
    private String fetchServiceCode(OrderModel order, String serviceCode) {
        String serviceCodeToken[]=serviceCode.split(":");
        return serviceCodeToken[0].equalsIgnoreCase(configurationService.getConfiguration().getString("installation.servicecode.tier1")) ? order.getInstallationServiceCode() : serviceCodeToken[0];
    }

    /**
     * Populate Related Party
     * @param order
     * @param productOrder
     */
    private void populateRelatedParty(OrderModel order,ProductOrder productOrder) {
        RelatedPartyRef relatedParty = new RelatedPartyRef();
        CustomerModel customer = (CustomerModel) order.getUser();
        if(llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())) {
      	  relatedParty.setId(customer.getCustomerID());
        }
        else {
        relatedParty.setId(customer.getCsgCustomerNo());
        }
        relatedParty.setName(customer.getName());
        relatedParty.setAtreferredType("Customer");
        productOrder.addRelatedPartyItem(relatedParty);

    }

    /**
     *  Populate Order Note
     * @param reasonCode
     * @param orderModel
     * @return
     */

    private List<Note> populateOrderNoteObject(String reasonCode, final OrderModel orderModel) {
        List<Note> listOrderNote=new ArrayList();
        Note orderReasonCode=new Note();
        orderReasonCode.setAttype(configurationService.getConfiguration().getString("llamulesoftintegration.order.puertorico.orderreasoncode.text"));
        orderReasonCode.setText(reasonCode);
        Note campaignCode=new Note();
        campaignCode.setText(configurationService.getConfiguration().getString("llamulesoftintegration.order.puertorico.campaignCode"));
        campaignCode.setAttype(configurationService.getConfiguration().getString("llamulesoftintegration.order.puertorico.campaigncode.text"));

        Note jobComment=new Note();
        jobComment.setText(configurationService.getConfiguration().getString("llamulesoftintegration.order.puertorico.jobcomment"));
        jobComment.setAttype(configurationService.getConfiguration().getString("llamulesoftintegration.order.puertorico.jobcomment.text"));

        Note creditLimit=new Note();
        creditLimit.setAttype(configurationService.getConfiguration().getString("llamulesoftintegration.order.puertorico.creditlimit.text"));
        creditLimit.setText(orderModel.getCreditLimit().toString());

        Note jobType=new Note();
        jobType.setText(orderModel.getJobType());
        jobType.setAttype(configurationService.getConfiguration().getString("llamulesoftintegration.order.puertorico.jobtype.text"));

        listOrderNote.add(orderReasonCode);
        listOrderNote.add(campaignCode);
        listOrderNote.add(jobComment);
        listOrderNote.add(creditLimit);
        listOrderNote.add(jobType);
        return listOrderNote;
    }
    
    private Note setBillingAccountAndNote(final BillingAccountRef billingAccount ,final OrderModel orderModel,final CustomerModel customer) {
   	 Note note = null;

   	 if(null != customer.getBillingAccounts() && CollectionUtils.isNotEmpty(customer.getBillingAccounts())) {
   	 billingAccount.setId(customer.getBillingAccounts().iterator().next().getBillingAccountId());
   	 billingAccount.setAttype("BillingAccount");
   	 }

   	 if(null != customer.getServiceAccounts() && CollectionUtils.isNotEmpty(customer.getServiceAccounts())) {
   		 note = new Note();
   		 note.setText(customer.getServiceAccounts().iterator().next().getServiceAccountId());
   		 note.setAttype("ServiceAccountId");
   	 }

   	 return note;
    }


}