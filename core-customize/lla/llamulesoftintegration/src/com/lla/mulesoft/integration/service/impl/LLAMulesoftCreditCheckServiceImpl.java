package com.lla.mulesoft.integration.service.impl;

import com.google.gson.*;
import com.lla.mulesoft.integration.constants.LlamulesoftintegrationConstants;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.response.LLAApiResponse;
import com.lla.mulesoft.integration.service.LLAMulesoftCreditCheckService;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import com.lla.telcotmfwebservices.v2.dto.CreditCheck;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import joptsimple.internal.Strings;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import de.hybris.platform.servicelayer.session.SessionService;


import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Iterator;

/**
 * The type Lla mulesoft credit check service.
 */

public class LLAMulesoftCreditCheckServiceImpl extends LLAAbstractService implements LLAMulesoftCreditCheckService {

    private static final Logger LOG = Logger.getLogger(LLAMulesoftCreditCheckServiceImpl.class);
    
    private static final String SSN_ENCRYPTED_VALUE = "ssn_encrypted";
    public static final String ENABLE_NORMALIZATION_API="puertorico.normalized.address.api.enabled";
    private static final String PLUS="[+]";
    private static final String PER_20 ="%20";
    private static final String PER_2C="%2C";
    private static final String SEMICOLON=",";
    private static final String MALFORMED_JSON_CODE="422";


    @Autowired
    ConfigurationService configurationService;
    @Autowired
 	 private SessionService sessionService;
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;


    /**
     * Customer credit check -  -- Make call to Mulesoft Credit check API
     *
     * @param orderModel
     * @return
     */
    @Override
    public LLAApiResponse customerCreditCheck(AbstractOrderModel orderModel,final String encryptedSSN) throws LLAApiException {

        LOG.info("Starting Customer credit check");
        final CustomerModel customer = (CustomerModel) orderModel.getUser();
        final String businessUnit=getBusinessUnit();
        final String siteID = configurationService.getConfiguration().getString(
                new StringBuffer(LlamulesoftintegrationConstants.WEBSITE).append(LlamulesoftintegrationConstants.DECIMAL)
                        .append(orderModel.getSite().getUid()).append(LlamulesoftintegrationConstants.DECIMAL)
                        .append(LlamulesoftintegrationConstants.CODE).toString());

        final String creditCheckEndPoint = apiUrlConstructor(getAPIServerForCallBack(), siteID,
                LlamulesoftintegrationConstants.CUSTOMER_CREDIT_CHECK_URL);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(creditCheckEndPoint)
                .queryParam("searchCriteria", customerCreditCheckRequest(orderModel));

        final HttpHeaders standardHeaders = setupCreditCheckHeaders(siteID,encryptedSSN);
        final HttpEntity<String> httpEntity = new HttpEntity<>(standardHeaders);
        ResponseEntity<String> serviceResponse = null;
        RestTemplate restTemplate_new=this.createRestTemplate(this.createRequestFactory(configurationService.getConfiguration().getString("max.pool.connection")));
        try {
            serviceResponse = restTemplate_new.exchange(builder.build(true).toUri(), HttpMethod.GET, httpEntity, String.class);
            if (serviceResponse.getStatusCode() == HttpStatus.OK) {

                LOG.info(String.format("Credit Check Retrieved from Mulesoft for Customer :: %s", customer.getUid()));
                final int creditScore =Integer.parseInt(new JsonParser().parse(serviceResponse.getBody()).getAsJsonObject().get("creditScore").getAsString());
                LOG.info(String.format("Credit Score Retrieved from Mulesoft :: %s", creditScore));
                return new LLAApiResponse(creditScore);
            }

        } catch (HttpClientErrorException.BadRequest badRequest) {
            LOG.error(String.format("Credit check request failed for customer %s due to Bad Request :: %s", customer.getUid(),
                    badRequest.getResponseBodyAsString()));
            return setCreditCheckResponse(badRequest.getResponseBodyAsString());

        } catch (HttpClientErrorException clientEx) {
            LOG.error(String.format("Credit check request failed for customer %s due to Client Error Exception  :: %s", customer.getUid(),
                    clientEx.getMessage()));
            return setCreditCheckResponse(clientEx.getResponseBodyAsString());

        }
        catch (final HttpServerErrorException.ServiceUnavailable clientEx) {
            LOG.error(String.format("Credit check request failed for customer %s due to bad Service Unavailability :: %s and Code %s",
                    customer.getUid(), clientEx.getMessage(),clientEx.getStatusCode()));
            return setCreditCheckResponse(clientEx.getResponseBodyAsString());
        }
        catch (final HttpServerErrorException.BadGateway clientEx)
        {
            LOG.error(String.format("Credit check request failed due to bad Gateway  :: %s and Code %s", clientEx.getMessage(),
                    clientEx.getStatusCode()));
            return setCreditCheckResponse(clientEx.getResponseBodyAsString());
        }catch (final HttpServerErrorException.InternalServerError clientEx) {
            LOG.error(String.format("Credit check request failed for customer %s due to Internal Server Error  :: %s and Code %s",
                    customer.getUid(), clientEx.getMessage(), clientEx.getStatusCode()));
            return setCreditCheckResponse(clientEx.getResponseBodyAsString());

        }
        catch (final HttpServerErrorException.GatewayTimeout clientEx)
        {
            LOG.error(String.format("Credit check request failed  due to  Gateway Timeout  :: %s and Code %s",
                    clientEx.getMessage(), clientEx.getStatusCode()));
            return setCreditCheckResponse(clientEx.getResponseBodyAsString());

        }
        catch (final HttpServerErrorException.NotImplemented clientEx)
        {
            LOG.error(String.format("Debt check request failed  due to no implementation  :: %s and Code %s",
                    clientEx.getMessage(), clientEx.getStatusCode()));
            return setCreditCheckResponse(clientEx.getResponseBodyAsString());
        }
        catch (ResourceAccessException ioException) {
            LOG.error(String.format("Exception in making Credit Check API Call due to %s ", ioException));
            throw new LLAApiException("Exception in making Credit Check API Call");
        }
        finally {
            HttpComponentsClientHttpRequestFactory requestFactory=(HttpComponentsClientHttpRequestFactory) restTemplate_new.getRequestFactory();
            LOG.info(String.format("Releasing current client connection for Credit Check Request for Market ::: %s",businessUnit));
            try{
               requestFactory.destroy();
            }catch (Exception exception){
                LOG.info(String.format("Error in Releasing current client connection for Credit Check Request for Market ::: %s",businessUnit));

            }
        }
        return null;
    }

    /**
     * @param orderModel
     * @return String
     */
    private String customerCreditCheckRequest(final AbstractOrderModel orderModel) {

        String encodedCreditCheck=Strings.EMPTY;
        AddressModel addressModel = new AddressModel();
        CreditCheck creditCheck = new CreditCheck();

        if(null != orderModel.getCreditCheckAddress())
        {
     		 addressModel = orderModel.getCreditCheckAddress();
        }else if(configurationService.getConfiguration().getBoolean(ENABLE_NORMALIZATION_API))
        {
     		 addressModel = orderModel.getSelectedAddress();
        }
        else{
            addressModel=orderModel.getDeliveryAddress();
        }

        if (StringUtils.isNotBlank(addressModel.getFirstname())) {
            creditCheck.setFirstName(addressModel.getFirstname());
        }
        if (StringUtils.isNotBlank(addressModel.getMiddlename())) {
            creditCheck.setMiddleName(addressModel.getMiddlename());
        }
        if (StringUtils.isNotBlank(addressModel.getLastname())) {
            creditCheck.setLastName(addressModel.getLastname());
        }
        if (StringUtils.isNotBlank(addressModel.getStreetname())) {
            creditCheck.setStreetName(addressModel.getStreetname());
        }
        if (StringUtils.isNotBlank(addressModel.getTown())) {
            creditCheck.setCity(addressModel.getTown());
        }
        if (StringUtils.isNotBlank(addressModel.getProvince())) {
            creditCheck.setState(addressModel.getProvince());
        }
        if (StringUtils.isNotBlank(addressModel.getPostalcode())) {
            creditCheck.setPostalCode(addressModel.getPostalcode());
        }

        final String creditCheckJson = new Gson().toJson(creditCheck);
        LOG.info(String.format("Credit Check Request JSON %s", creditCheckJson));
        encodedCreditCheck = URLEncoder.encode(creditCheckJson, StandardCharsets.UTF_8).replaceAll(PLUS, PER_20);
        return encodedCreditCheck.replaceAll(PER_2C, SEMICOLON);
    }

    private HttpHeaders setupCreditCheckHeaders(String siteID, String encryptedSSN) {
        switch (siteID) {
            case "PR":

                headers.set("businessId",
                        configurationService.getConfiguration().getString("puertorico.mulesoft.creditCheck.api.businessId"));
                headers.set("correlationId",
                        configurationService.getConfiguration().getString("puertorico.mulesoft.creditCheck.api.correlationId"));
                 headers.set("x-ssn", encryptedSSN);
                headers.set("channelId",
                        configurationService.getConfiguration().getString("puertorico.mulesoft.creditCheck.api.channelId"));
                headers.set("client_id", configurationService.getConfiguration().getString("puertorico.mulesoft.creditCheck.api.clientid"));
                headers.set("client_secret",
                        configurationService.getConfiguration().getString("puertorico.mulesoft.creditCheck.api.client_secret"));
                break;
        }

        return headers;
    }

    private LLAApiResponse setCreditCheckResponse(String serviceResponse) {

        LLAApiResponse llaCreditCheckApiResponse = null;
        if (null != serviceResponse)
        {
              try  {
                  JsonArray errorNode = (JsonArray) new JsonParser().parse(serviceResponse).getAsJsonObject().get("errors");
                  JsonElement jsonElementIterator = errorNode.get(0);
                  String errorCode = jsonElementIterator.getAsJsonObject().get("code").getAsString();
                  String description = jsonElementIterator.getAsJsonObject().get("description").getAsString();
                  String message = jsonElementIterator.getAsJsonObject().get("message").getAsString();
                  llaCreditCheckApiResponse = new LLAApiResponse(errorCode, message, description, false, Strings.EMPTY, Collections.emptyList());

              }

            catch (JsonSyntaxException ex)
            {
                String errorCode = MALFORMED_JSON_CODE;
                String description = ex.getMessage();
                String message = serviceResponse;
                llaCreditCheckApiResponse = new LLAApiResponse(errorCode, message, description, false, Strings.EMPTY, Collections.emptyList());

            }
        }
        return llaCreditCheckApiResponse;

    }

}

