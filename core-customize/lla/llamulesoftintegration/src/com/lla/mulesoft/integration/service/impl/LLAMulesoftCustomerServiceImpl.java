package com.lla.mulesoft.integration.service.impl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.lla.core.enums.DocumentType;
import com.lla.core.enums.SourceEPC;
import com.lla.mulesoft.integration.constants.LlamulesoftintegrationConstants;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.service.LLAMulesoftCustomerService;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import com.lla.telcotmfwebservices.v2.dto.Characteristic;
import com.lla.telcotmfwebservices.v2.dto.ContactMedium;
import com.lla.telcotmfwebservices.v2.dto.Customer;
import com.lla.telcotmfwebservices.v2.dto.MediumCharacteristic;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


/**
 *
 */
public class LLAMulesoftCustomerServiceImpl  extends LLAAbstractService implements LLAMulesoftCustomerService {


    private static final Logger LOG = Logger.getLogger(LLAMulesoftCustomerServiceImpl.class);
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;
    @Autowired
    private SiteConfigService siteConfigService;

    @Autowired
    private ModelService modelService;
    private LLAMulesoftCustomerServiceImpl(){
        super();
    }
    /**
     * Create Customer in BSS -  -- Make call to Mulesoft Customer API
     *
     * @param orderData
     * @return
     * @throws LLAApiException
     */

    @Override
    public String createCustomerInBSS(final OrderModel orderData, final String sourceEpc) throws LLAApiException {
        LOG.info("Starting Mulesoft Connectivity");
        final String siteID =configurationService.getConfiguration().getString(new StringBuffer(LlamulesoftintegrationConstants.WEBSITE).append(LlamulesoftintegrationConstants.DECIMAL).append(orderData.getSite().getUid()).append(LlamulesoftintegrationConstants.DECIMAL).append(LlamulesoftintegrationConstants.CODE).toString());
        final String siteCode=orderData.getSite().getUid();
        final CustomerModel customerData = (CustomerModel)orderData.getUser();
        final HttpHeaders apiHeaders = setupHeaders(siteID);
        final HttpEntity<String> httpEntity = new HttpEntity<>(createCustomerRequest(orderData,sourceEpc, siteID), apiHeaders);
        final URI customer_create_url = URI.create(apiUrlConstructor(getAPIServer(),siteID, LlamulesoftintegrationConstants.CREATE_CUSTOMER));
        LOG.info(String.format("Attempting to make customer create service API Request for Market %s",siteCode));
        RestTemplate restTemplate_new=this.createRestTemplate(this.createRequestFactory(configurationService.getConfiguration().getString("max.pool.connection")));

        ResponseEntity<String> serviceResponse=null;
        try {
              serviceResponse = restTemplate_new.exchange(customer_create_url, HttpMethod.POST, httpEntity, String.class);
            if(!siteID.equalsIgnoreCase(configurationService.getConfiguration().getString("vtr.api.business.unit"))){
		    if (serviceResponse.getStatusCode() == HttpStatus.CREATED) {
			LOG.info(String.format("Customer %s Created in BSS ", customerData.getUid()));      
		    } 
		    else {
			LOG.error("Error in creating customer in BSS!.Server HTTP response: "+serviceResponse.getStatusCodeValue());
			validateAPIResponse(serviceResponse);
			return StringUtils.EMPTY;
		    }
	    }
		
	     if(siteID.equalsIgnoreCase(configurationService.getConfiguration().getString("vtr.api.business.unit")) && serviceResponse.getStatusCodeValue() == 202) {
            	setCorrelationId(customerData, serviceResponse);
            	return serviceResponse.toString();
            }else  if(siteID.equalsIgnoreCase(configurationService.getConfiguration().getString("vtr.api.business.unit")) && serviceResponse.getStatusCodeValue() != 202){
		LOG.error("Error in creating customer in BSS!.Server HTTP response: "+serviceResponse.getStatusCodeValue());
			validateAPIResponse(serviceResponse);
			return StringUtils.EMPTY;     
	     }	
        }catch(HttpClientErrorException.BadRequest badRequest){
            LOG.error(String.format("Create customer failed for customer %s and Order %s due to Bad Request :: %s", customerData.getUid(), orderData.getCode(),badRequest.getResponseBodyAsString()));

        }catch(HttpClientErrorException clientEx){
           LOG.error(String.format("Create customer failed for customer %s and Order %s due to Client Error Exception  :: %s", customerData.getUid(), orderData.getCode(),clientEx.getMessage()));

        }catch(ResourceAccessException ioException){
         /*  throw new TmaApiException(ioException.getMessage()) ; */
            LOG.error(String.format("Exception in making Api Call to Create Customer in BSS for Market:: %s due to  :: %s",siteCode,ioException.getMessage()));
            throw new LLAApiException("Exception in making Api Call");
        }
        finally {
            HttpComponentsClientHttpRequestFactory requestFactory=(HttpComponentsClientHttpRequestFactory) restTemplate_new.getRequestFactory();
            LOG.info("Releasing current client connection for Customer Creation Request");
            try {
                requestFactory.destroy();
            } catch (Exception exception) {
                LOG.info(String.format("Error in releasing Connection obtained for Create Customer Call to BSS for Market  %s and due to %s  ",siteCode,exception));
            }
        }
        return null!=serviceResponse?serviceResponse.getBody():StringUtils.EMPTY;
    }


    /**
     * Validate User is Existing or New
     * @param cartModel
     * @return
     * @throws Exception
     */
    @Override
    public boolean isNewCustomer(CartModel cartModel) throws Exception {
        RestTemplate restTemplate_new=this.createRestTemplate(this.createRequestFactory(configurationService.getConfiguration().getString("max.pool.connection")));
        LOG.info("Starting Mulesoft Connectivity for Customer Account Check");
        final String siteID =configurationService.getConfiguration().getString(new StringBuffer(LlamulesoftintegrationConstants.WEBSITE).append(LlamulesoftintegrationConstants.DECIMAL).append(cartModel.getSite().getUid()).append(LlamulesoftintegrationConstants.DECIMAL).append(LlamulesoftintegrationConstants.CODE).toString());
        final String customerValidationEndPoint = apiUrlConstructor(getAPIServer(),getBusinessUnit(),LlamulesoftintegrationConstants.CABLETICA_CUSTOMER_VALIDATION);
        final CustomerModel customerData = (CustomerModel)cartModel.getUser();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(customerValidationEndPoint)
                .queryParam(LlamulesoftintegrationConstants.IDENTITY_NUMBER,customerData.getDocumentNumber().replace("-", ""))
                .queryParam(LlamulesoftintegrationConstants.IDENTITY_TYPE,getDocumentType(customerData));
        HttpHeaders standardHeaders =  setupCRHeaders(siteID);
        HttpEntity<String> requestEntity = new HttpEntity<String>(standardHeaders);
        ResponseEntity<String> serviceResponse=null;
        boolean returnFlag=false;
        try
        {
            serviceResponse=restTemplate_new.exchange(builder.build().encode().toUri(), HttpMethod.GET, requestEntity, String.class);

            if (serviceResponse.getStatusCode() == HttpStatus.OK)
            {
                LOG.info(String.format("Existing customer check api Response Retrieved ::: %s for Cart %s ::: Uid :::%s", serviceResponse.getBody(),cartModel.getCode(),cartModel.getUser().getUid()));
                final ObjectMapper mapper = new ObjectMapper();
                mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                if (!StringUtils.isEmpty(serviceResponse.getBody()))
                {
                    try
                    {
                        final List<Customer> customerResponseList = Arrays
                                .asList(mapper.readValue(serviceResponse.getBody(), Customer[].class));

                        if (!customerResponseList.isEmpty() && null !=customerResponseList.get(0)) {
                            if ((!customerResponseList.get(0).getStatus().equals("Active"))) {
                                LOG.info("Existing Customer API id :" + customerResponseList.get(0).getId() + " status :" + customerResponseList.get(0).getStatus());
                                saveCurentCustomerStatus(customerResponseList.get(0).getStatus(), customerData);
                                returnFlag= true;
                            } else {
                                saveCurentCustomerStatus(customerResponseList.get(0).getStatus(), customerData);
                                returnFlag= false;
                            }
                        }
                    }
                    catch (final JsonProcessingException e)
                    {
                        LOG.error("Error Parsing Existing customer Response", e);
                        returnFlag = false;
                    }
                }
            }
            else
            {
                LOG.error("Error in Retrieving Existing customer Response from Mulesoft server !!! ");
                returnFlag= false;
            }

        }
        catch (final HttpServerErrorException.InternalServerError clientEx) {
            LOG.error(String.format("Customer check request failed for customer %s due to internal Server error :: %s and Code %s",
                    customerData.getUid(), clientEx.getMessage(), clientEx.getStatusCode()));
            returnFlag= false;
        }catch (final HttpServerErrorException.ServiceUnavailable clientEx) {
            LOG.error(String.format("Customer check request failed for customer %s due to bad Service Unavailability :: %s and Code %s",
                    customerData.getUid(), clientEx.getMessage(),clientEx.getStatusCode()));
            returnFlag= false;
        }catch (final HttpServerErrorException.BadGateway clientEx)
        {
            LOG.error(String.format("Customer check request failed for customer %s due to bad Gateway  :: %s and Code %s",
                    customerData.getUid(), clientEx.getMessage(),clientEx.getStatusCode()));
            returnFlag = false;
        } catch (final HttpServerErrorException.NotImplemented clientEx)
        {
            LOG.error(String.format("Customer check request failed for customer %s due to no implementation  :: %s and Code %s",
                    customerData.getUid(), clientEx.getMessage(),clientEx.getStatusCode()));
            returnFlag = false;
        } catch (final HttpServerErrorException.GatewayTimeout clientEx)
        {
            LOG.error(String.format("Customer check request failed for customer %s due to  Gateway Timeout  :: %s and Code %s",
                    customerData.getUid(), clientEx.getMessage(),clientEx.getStatusCode()));
            returnFlag = false;
        } catch (final HttpClientErrorException.BadRequest badRequest)
        {
            LOG.error(badRequest.getResponseBodyAsString());
            returnFlag = false;
        } catch (final HttpClientErrorException clientEx)
        {
            LOG.error(clientEx.getMessage());
            if(clientEx.getRawStatusCode() == 404){
                LOG.info(String.format("Existing customer api reply with no 404 (No Record found) for Cart::: %s UID :::%s having document number :: %s ",cartModel.getCode(),cartModel.getUser().getUid(),customerData.getDocumentNumber()));
                saveCurentCustomerStatus("New", customerData);
                returnFlag= true;
            }else{
              returnFlag = false;
            }
        } catch (final ResourceAccessException ioException)
        {
            LOG.error(String.format("Exception in making Api Call for Current Customer :: %s due to ::: %s", cartModel.getUser().getUid(),ioException));
            returnFlag = false;
        }
        finally {
            HttpComponentsClientHttpRequestFactory requestFactory=(HttpComponentsClientHttpRequestFactory) restTemplate_new.getRequestFactory();
            LOG.info("Releasing current client connection for Customer Check Request");
            requestFactory.destroy();
        }
        return returnFlag;
    }



	/**
	 * 
	 */
	private void setCorrelationId(final CustomerModel customerData, ResponseEntity<String> serviceResponse)
	{
		final HttpHeaders headers = serviceResponse.getHeaders();
		final String correlationId = headers.getFirst(configurationService.getConfiguration().getString("vtr.correlationId.value"));
		customerData.setCorrelationId(correlationId);
		modelService.save(customerData);
	}



    private String createCustomerRequest(final OrderModel orderData, final String sourceEpc, String market) {

        final CustomerModel commerceCustomer = (CustomerModel)orderData.getUser();
        final Customer customer=new Customer();
        customer.setName(new StringBuffer(commerceCustomer.getFirstName().trim()).append(" ").append(commerceCustomer.getLastName().trim()).toString());

        populateContactMediumList(commerceCustomer, customer,orderData);

        populateCharacteristicsList(commerceCustomer, sourceEpc, customer, market, orderData);

        final String responseJson= new Gson().toJson(customer);
        LOG.info(String.format("Customer Create Request JSON %s",responseJson));
        return responseJson;
    }

    /**
     *
     * @param commerceCustomer
     * @param customer
     */
    private void populateContactMediumList(final CustomerModel commerceCustomer, final Customer customer, final OrderModel order) {
        final List<ContactMedium> contactMediumList=new ArrayList<>();
        Configuration configuration= configurationService.getConfiguration();
        final ContactMedium contactMedium=new ContactMedium();
        final MediumCharacteristic contactCharacteristic=new MediumCharacteristic();
        AddressModel addressModel=llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())?order.getInstallationAddress():order.getDeliveryAddress();
        if(!llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())){
        populateStreetDetails(contactCharacteristic, addressModel,order.getSite());
        
        contactMedium.setMediumType(configuration.getString("llamulesoftintegration.customer.addressContactMedium"));
        if(llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())||llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite())){
            contactMedium.setPreferred(Boolean.valueOf(configuration.getString("llamulesoftintegration.customer.addressPreferred")));
        }
        contactMedium.setCharacteristic(contactCharacteristic);
        contactMediumList.add(contactMedium);
        }
       if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite()) || llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())) {
           final ContactMedium phoneMedium = new ContactMedium();
           phoneMedium.setMediumType(configuration.getString("llamulesoftintegration.customer.phoneContactMedium"));
           if (llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())) {
               phoneMedium.setPreferred(Boolean.valueOf(configuration.getString("llamulesoftintegration.customer.mobilephone.preferred")));

           }
           MediumCharacteristic phoneCharacteristic = new MediumCharacteristic();
           phoneCharacteristic.setPhoneNumber(commerceCustomer.getMobilePhone());
           phoneCharacteristic.setContactType(configuration.getString("llamulesoftintegration.customer.phoneContactType"));
           phoneMedium.setCharacteristic(phoneCharacteristic);
           contactMediumList.add(phoneMedium);
       }
        if(llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite()) || llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite())){
            final ContactMedium homePhoneMedium = new ContactMedium();
            homePhoneMedium.setMediumType(configuration.getString("llamulesoftintegration.customer.phoneContactMedium"));
            homePhoneMedium.setPreferred(Boolean.valueOf(configuration.getString("llamulesoftintegration.customer.homephone.preferred")));
            MediumCharacteristic homephoneCharacteristic = new MediumCharacteristic();
            homephoneCharacteristic.setPhoneNumber(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())?addressModel.getPhone1():commerceCustomer.getFixedPhone());
            homephoneCharacteristic.setContactType(configuration.getString("llamulesoftintegration.customer.homephoneContactType"));
            homePhoneMedium.setCharacteristic(homephoneCharacteristic);
            contactMediumList.add(homePhoneMedium);
        }
        
        if(llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())){
      	  final ContactMedium mobileMedium = new ContactMedium();
      	  mobileMedium.setMediumType(configuration.getString("llamulesoftintegration.customer.mobileContactMedium"));
      	  MediumCharacteristic mobileCharacteristic = new MediumCharacteristic();
      	  mobileCharacteristic.setPhoneNumber(commerceCustomer.getMobilePhone());
      	  mobileCharacteristic.setContactType(configuration.getString("llamulesoftintegration.customer.homephoneContactType"));
      	  mobileMedium.setCharacteristic(mobileCharacteristic);
      	  contactMediumList.add(mobileMedium);      	  
        }
        

        final ContactMedium emailMedium = new ContactMedium();
        emailMedium.setMediumType(configuration.getString("llamulesoftintegration.customer.emailContactMedium"));
        if(llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite()) || llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite())){
            emailMedium.setPreferred(Boolean.valueOf(configurationService.getConfiguration().getString("llamulesoftintegration.customer.emailPreferred")));
        }
        MediumCharacteristic emailMediumCharacteristic = new MediumCharacteristic();
        emailMediumCharacteristic.setEmailAddress(CustomerType.GUEST.equals(commerceCustomer.getType())?StringUtils.substringAfter(commerceCustomer.getUid(),"|"):commerceCustomer.getUid());
        //emailMediumCharacteristic.setEmailAddress(commerceCustomer.getUid());
        emailMediumCharacteristic.setContactType(configuration.getString("llamulesoftintegration.customer.emailContactMediumType"));
        emailMedium.setCharacteristic(emailMediumCharacteristic);
        contactMediumList.add(emailMedium);
        customer.setContactMedium(contactMediumList);
        
       
        
    }

    /**
     * Populate Street Details
     * @param contactCharacteristic
     * @param addressModel
     * @param site
     */
    private void populateStreetDetails(MediumCharacteristic contactCharacteristic, AddressModel addressModel, BaseSiteModel site) {

        if(llaMulesoftIntegrationUtil.isJamaicaOrder(site)){
            contactCharacteristic.setStreet1(addressModel.getLine1());
         contactCharacteristic.setStreet2(addressModel.getLine2());
         contactCharacteristic.setStateOrProvince(addressModel.getArea());
         contactCharacteristic.setPostCode(addressModel.getPostalcode());
         contactCharacteristic.setCity(addressModel.getTown());
        }else if(llaMulesoftIntegrationUtil.isPanamaOrder(site)){
            String street1 = populatePanamaStreet1(addressModel);
            contactCharacteristic.setStreet1(street1);
            contactCharacteristic.setStreet2(addressModel.getNeighbourhood());
            contactCharacteristic.setStateOrProvince(addressModel.getProvince());
            contactCharacteristic.setCity(addressModel.getDistrict());
        }else if(llaMulesoftIntegrationUtil.isPuertoricoOrder(site)){
            contactCharacteristic.setStreet1(addressModel.getLine1());
            contactCharacteristic.setStreet2(addressModel.getLine2());
            contactCharacteristic.setCity(addressModel.getTown());
            contactCharacteristic.setPostCode(addressModel.getPostalcode());
            contactCharacteristic.setStateOrProvince("PR");
        }
        contactCharacteristic.setCountry(addressModel.getTown());

    }

    /**
     * Populate Panama Street 1
     * @param addressModel
     * @return
     */
    private String populatePanamaStreet1(AddressModel addressModel) {
        StringBuffer street1=new StringBuffer(StringUtils.EMPTY);
        if(StringUtils.isNotEmpty(addressModel.getAppartment())){
          street1.append(addressModel.getAppartment()).append(" ");
        }

        if(StringUtils.isNotEmpty(addressModel.getBuilding())){
            street1.append(addressModel.getBuilding()).append(" ");
        }

        if(StringUtils.isNotEmpty(addressModel.getStreetname())){
            street1.append(addressModel.getStreetname());
        }

        String maxLength = configurationService.getConfiguration().getString("llamulesoftintegration.panama.address.street1.lengthlimit");
        int mlength = Integer.parseInt(maxLength);
        if(maxLength != null && StringUtils.isNotEmpty(maxLength) && street1.length()>mlength){
            return street1.substring(0, mlength);
        }
        return street1.toString();
    }

    /**
     *  @param customerData
     * @param customer
     * @param  sourceEpc
     * @param order
     * @param market
     */
    private void    populateCharacteristicsList(final CustomerModel customerData, final String sourceEpc, final Customer customer, String market, OrderModel order) {
        final List<Characteristic> characteristicList=new ArrayList<>();
        Configuration configuration= configurationService.getConfiguration();
       if(llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())) {
           populateDOBCharacterstics(customerData, characteristicList, configuration);
        }
       if(llaMulesoftIntegrationUtil.isPuertoricoOrder(order.getSite())){
           populateDOBCharacterstics(customerData, characteristicList, configuration);
           final Characteristic salutation = new Characteristic();
           salutation.setName(configuration.getString("llamulesoftintegration.customer.salutation"));
           salutation.setValue(configuration.getString("llamulesoftintegration.customer.salutation.code"));
           characteristicList.add(salutation);

           final Characteristic dlState = new Characteristic();
           dlState.setName(configuration.getString("llamulesoftintegration.customer.dlstate"));
           dlState.setValue(configuration.getString("llamulesoftintegration.customer.dlstate.code"));
           characteristicList.add(dlState);

           final Characteristic privacy = new Characteristic();
           privacy.setName(configuration.getString("llamulesoftintegration.customer.privacy"));
           privacy.setValue("Y");
           characteristicList.add(privacy);
           final Characteristic type = new Characteristic();
           type.setName("type");
           type.setValue(configuration.getString("llamulesoftintegration.customer.type"));
           characteristicList.add(type);

           final Characteristic fName = new Characteristic();
           fName.setName(configuration.getString("llamulesoftintegration.customer.firstName"));
           fName.setValue(customerData.getFirstName());
           characteristicList.add(fName);

           final Characteristic lName = new Characteristic();
           lName.setName(configuration.getString("llamulesoftintegration.customer.lastName"));
           lName.setValue(customerData.getLastName());
           characteristicList.add(lName);
       }

        if(llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite()) || llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())) {
            final Characteristic type = new Characteristic();
            type.setName("type");
            //type.setValueType("customerType");
            type.setValue(configuration.getString("llamulesoftintegration.customer.type"));
            if (llaMulesoftIntegrationUtil.isCellularOrder(order)) {
                type.setValue(configuration.getString("llamulesoftintegration.customer.cellulartype"));
            }
            characteristicList.add(type);
        }
        if(!llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())){
        if(sourceEpc.equalsIgnoreCase(SourceEPC.LIBERATE.getCode())) {
            final Characteristic companyCode = new Characteristic();
            companyCode.setName("companyCode");
            //companyCode.setValueType("String");
            String code = llaMulesoftIntegrationUtil.isJamaicaOrder(order.getSite())?llaMulesoftIntegrationUtil.fetchCompanyCode(order):configurationService.getConfiguration().getString("llamulesoftintegration.customer.panama.companycode");
            if(StringUtils.isEmpty(code)){
                LOG.error("Order does not have products from same category; will affect provisioning");
            }else{
                companyCode.setValue(code);
                characteristicList.add(companyCode);
            }
        }

        final Characteristic sourceEPC=new Characteristic();
        sourceEPC.setName(configuration.getString("llamulesoftintegration.customer.sourceEPC"));
        //sourceEPC.setValueType("Identifying the BSS System");
        // Need to discuss how we are going to send SourceEpc while creating customer
        sourceEPC.setValue(sourceEpc);
        characteristicList.add(sourceEPC);
        }

        if(llaMulesoftIntegrationUtil.isPanamaOrder(order.getSite())){
            final Characteristic nationality=new Characteristic();
            nationality.setName(configuration.getString("llamulesoftintegration.customer.nationality"));
            if(null != customerData.getDocumentType() && DocumentType.PASSPORT.toString().equalsIgnoreCase(customerData.getDocumentType()))
                nationality.setValue(configuration.getString("llamulesoftintegration.customer.passport.nationalityValue"));
            else
                nationality.setValue(configuration.getString("llamulesoftintegration.customer.cedula.nationalityValue"));

            characteristicList.add(nationality);

            final Characteristic billStatusArea=new Characteristic();
            billStatusArea.setName(configuration.getString("llamulesoftintegration.customer.billStatusArea"));
            billStatusArea.setValue(order.getBillStatusArea());
            characteristicList.add(billStatusArea);

        }
        if(llaMulesoftIntegrationUtil.isVtrOrder(order.getSite())){
      	  populateCharecteristicVTR(customerData, characteristicList, configuration);
        }

        populateIdProof(customerData, market, characteristicList);

        customer.setCharacteristic(characteristicList);
    }



	/**
	 *
	 Populating Charecteristic for VTR
	 */
	private void populateCharecteristicVTR(final CustomerModel customerData, final List<Characteristic> characteristicList,
			Configuration configuration)
	{
		populateVTRDOBCharacterstics(customerData, characteristicList, configuration);
		  final Characteristic type = new Characteristic();
		  type.setName("customerType");
		  type.setValue(configuration.getString("llamulesoftintegration.customer.type"));
		  characteristicList.add(type);
		  
		  final Characteristic sourceVTREPC=new Characteristic();
		  sourceVTREPC.setName(configuration.getString("llamulesoftintegration.customer.sourceEPC"));
		  sourceVTREPC.setValue(SourceEPC.SIEBEL.getCode());
		  characteristicList.add(sourceVTREPC);
		  
		  final Characteristic fName = new Characteristic();
		  fName.setName(configuration.getString("llamulesoftintegration.customer.firstName"));
		  fName.setValue(customerData.getFirstName());
		  characteristicList.add(fName);

		  final Characteristic lName = new Characteristic();
		  lName.setName(configuration.getString("llamulesoftintegration.customer.lastName"));
		  lName.setValue(customerData.getLastName());
		  characteristicList.add(lName);
	
		  final Characteristic addressId = new Characteristic();
		  addressId.setName(configuration.getString("llamulesoftintegration.customer.addressId"));
		  addressId.setValue(customerData.getAddressId());
		  characteristicList.add(addressId);
	
	
	}

    /**
	 * 
	 */
	private void populateVTRDOBCharacterstics(CustomerModel customerData, List<Characteristic> characteristicList,
			Configuration configuration)
	{
		final Characteristic dob=new Characteristic();
		dob.setName(configuration.getString("llamulesoftintegration.customer.dateOfBirth"));
		dob.setValueType("DateTime");
		dob.setValue(new SimpleDateFormat("yyyy-MM-dd").format(customerData.getDateOfBirth()));
		characteristicList.add(dob);
	}



	private void populateDOBCharacterstics(CustomerModel customerData, List<Characteristic> characteristicList, Configuration configuration) {
        final SimpleDateFormat dateFormatter = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.US);

        final Characteristic dob=new Characteristic();
        dob.setName(configuration.getString("llamulesoftintegration.customer.dateOfBirth"));
        //dob.setValueType("yyyy-MM-dd");
        dob.setValue(configuration.getString("llamulesoftintegration.customer.dateOfBirth.constant"));
        characteristicList.add(dob);
    }

    /**
     *  Populate Customer Id Proof details
     * @param customerData
     * @param market
     * @param characteristicList
     */
    private void populateIdProof(CustomerModel customerData, String market, List<Characteristic> characteristicList) {
        final Characteristic customerIDProof = new Characteristic();
        customerIDProof.setName(configurationService.getConfiguration().getString("llamulesoftintegration.customer.idProof"));
        if(market.equals("JM")) {
            customerIDProof.setValueType(configurationService.getConfiguration().getString("llamulesoftintegration.customer.proofType"));
            customerIDProof.setValue(customerData.getTrnNumber());
    
        } else if (market.equals("PA")){
            switch(customerData.getDocumentType()){
                case "CEDULA":
                         customerIDProof.setValueType(configurationService.getConfiguration().getString("llamulesoftintegration.customer.cedula"));
                         break;
                case "PASSPORT":
                         customerIDProof.setValueType(configurationService.getConfiguration().getString("llamulesoftintegration.customer.passport"));
                         break;

            }
            customerIDProof.setValue(customerData.getDocumentNumber());
        }else if (market.equals("PR")){
            customerIDProof.setValueType(configurationService.getConfiguration().getString("llamulesoftintegration.customer.driverLicense"));
            customerIDProof.setValue(customerData.getDocumentNumber());
           }
        else if (market.equals("CL")) {
      	  customerIDProof.setValueType(configurationService.getConfiguration().getString("llamulesoftintegration.customer.proofValueType"));
           customerIDProof.setValue(customerData.getRutNumber());
        }
        characteristicList.add(customerIDProof);

    }
}
