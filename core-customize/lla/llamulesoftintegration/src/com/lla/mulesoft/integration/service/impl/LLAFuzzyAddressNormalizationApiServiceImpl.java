package com.lla.mulesoft.integration.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.lla.mulesoft.integration.constants.LlamulesoftintegrationConstants;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.response.LLAApiResponse;
import com.lla.mulesoft.integration.service.LLAFuzzyAddressNormalizationApiService;
import com.lla.telcotmfwebservices.v2.dto.HitsRef;
import com.lla.telcotmfwebservices.v2.dto.NormalizedAddress;
import com.lla.telcotmfwebservices.v2.dto.Root;
import de.hybris.platform.util.Config;
import de.hybris.platform.util.Utilities;
import org.apache.catalina.util.URLEncoder;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URI;
import java.util.*;

/**
 * The type Lla Fuzzy Address Normalization Api service.
 */
public class LLAFuzzyAddressNormalizationApiServiceImpl extends LLAAbstractService implements LLAFuzzyAddressNormalizationApiService {

    private static final Logger LOG = Logger.getLogger(LLAFuzzyAddressNormalizationApiServiceImpl.class);
    /**
     * The constant RESPONSE_FLAG.
     */
    public static  String RESPONSE_FLAG = Config.getParameter("puertorico.normalized.address.localResponse.enabled");
    /**
     * The constant RESPONSE_FILE.
     */
    public static final String RESPONSE_FILE="/resources/NormalizationApiResponse.json";
    /**
     * The constant EXTENSION_NAME.
     */
    public static final String EXTENSION_NAME="llamulesoftintegration";
    /**
     * The constant _SOURCE.
     */
    public static final String _SOURCE="_source";
    /**
     * The constant SOURCE.
     */
    public static final String SOURCE="source";
    /**
     * The constant _ID.
     */
    public static final String _ID="_id";
    /**
     * The constant ID.
     */
    public static final String ID="id";

    /**
     * @param addressData the address data
     * @return
     * @throws LLAApiException
     */
    @Override
    public LLAApiResponse getNormalizedAddressList(String addressData) throws Exception {
        LOG.info(String.format("Generating Address Normalization request for Address %s ", addressData));
        final String businessUnit=getBusinessUnit();
        LLAApiResponse llaFuzzyApiResponse=null;
        List<NormalizedAddress> normalizedAddressList = new ArrayList<>();
        final String parameter = new StringBuffer(
                configurationService.getConfiguration()
                        .getString(LlamulesoftintegrationConstants.FUZZY_ADDRESS_NORMALIZATION_PARAMETER))
                .append(getEncodedParameter(addressData)).toString();
        final URI normalization_get_url = URI
                .create(apiUrlConstructor(getFuzzyAPIServer(),getAddressBusinessUnit(),
                        LlamulesoftintegrationConstants.FUZZY_ADDRESS_NORMALIZATION_QUERY,
                        parameter));
        ResponseEntity<String> serviceResponse = null;
        RestTemplate restTemplate_new=this.createRestTemplate(this.createRequestFactory(configurationService.getConfiguration().getString("max.pool.connection")));

        try {
            if (Boolean.valueOf(RESPONSE_FLAG)) {
                return getLocalResponse();
            } else {
                serviceResponse = restTemplate_new.exchange(normalization_get_url, HttpMethod.GET, new HttpEntity<>(new HttpHeaders()), String.class);

                if (serviceResponse.getStatusCode() == HttpStatus.OK) {
                    LOG.info(String.format("Address Normalization Api Response Retrieved %s ::: ", serviceResponse.getBody()));

                    final ObjectMapper mapper = new ObjectMapper();
                    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                    mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

                    JsonObject jsonObject = new JsonParser().parse(serviceResponse.getBody()).getAsJsonObject();
                    String replaceString = jsonObject.toString().replace(_SOURCE, SOURCE);
                    Root rootMapper = mapper.readValue(replaceString.replace(_ID, ID), Root.class);

                    if (Objects.nonNull(rootMapper) && Objects.nonNull(rootMapper.getHits()) && CollectionUtils.isNotEmpty(rootMapper.getHits().getHits())) {
                        List<HitsRef> hitsRefList = rootMapper.getHits().getHits();
                        LOG.info(String.format("Fuzzy Address Normalization List Size %s ::: ", hitsRefList.size()));

                        for (HitsRef hitRef : hitsRefList) {
                            normalizedAddressList.add(hitRef.getSource());
                        }
                        return new LLAApiResponse(normalizedAddressList);
                    }
                } else {
                    LOG.error("Error in Retrieving normalized address api Response !!! ");
                    return setFuzzyApiResponse(serviceResponse.getBody());
                }
            }
        }catch (final HttpServerErrorException.ServiceUnavailable clientEx) {
            LOG.error(String.format("Address Normalization request failed due to bad Service Unavailability  :: %s and Code %s", clientEx.getMessage(),clientEx.getStatusCode()));
            return setFuzzyApiResponse(clientEx.getResponseBodyAsString());

        }catch (final HttpServerErrorException.BadGateway clientEx) {
            LOG.error(String.format("Address Normalization request failed due to bad Gateway  :: %s and Code %s", clientEx.getMessage(), clientEx.getStatusCode()));
            return setFuzzyApiResponse(clientEx.getResponseBodyAsString());

        }catch (final HttpServerErrorException.InternalServerError clientEx) {
            LOG.error(String.format("Address Normalization request failed  due to Internal Server Error  :: %s and Code %s", clientEx.getMessage(), clientEx.getStatusCode()));
            return setFuzzyApiResponse(clientEx.getResponseBodyAsString());

        }catch (final HttpServerErrorException.NotImplemented clientEx) {
            LOG.error(String.format("Address Normalization request failed  due to no implementation  :: %s and Code %s", clientEx.getMessage(), clientEx.getStatusCode()));
            return setFuzzyApiResponse(clientEx.getResponseBodyAsString());

        }catch (final HttpServerErrorException.GatewayTimeout clientEx) {
            LOG.error(String.format("Address Normalization request failed  due to  Gateway Timeout  :: %s and Code %s", clientEx.getMessage(), clientEx.getStatusCode()));
            return setFuzzyApiResponse(clientEx.getResponseBodyAsString());

        }catch (final HttpClientErrorException.BadRequest badRequest) {
            LOG.error(String.format("Address Normalization request  failed due to  Bad Request  :: %s",badRequest.getResponseBodyAsString()));
            return setFuzzyApiResponse(badRequest.getResponseBodyAsString());

        }catch (final HttpClientErrorException clientEx) {
            LOG.error(String.format("Address Normalization request failed  due to  forbidden  :: %s and Code %s", clientEx.getMessage(), clientEx.getStatusCode()));

            return setFuzzyApiResponse(clientEx.getResponseBodyAsString());

        }catch (final ResourceAccessException ioException) {
            LOG.error(String.format("Exception in making Address Normalization API Call :: %s", ioException.getMessage()));
            throw new LLAApiException("Exception in making Address Normalization API Call");

        }catch (JsonMappingException e) {
            LOG.error(String.format("Address Normalization request failed  due to  JSON Mapping exception  :: %s", e.getMessage()));

        }catch (JsonProcessingException e) {
            LOG.error(String.format("Address Normalization request failed  due to  JSON Processing exception  :: %s", e.getMessage()));
        }
        finally {
            HttpComponentsClientHttpRequestFactory requestFactory=(HttpComponentsClientHttpRequestFactory) restTemplate_new.getRequestFactory();
            LOG.info(String.format("Releasing current client connection for Address Normalization Request for Market ::: %s",businessUnit));
            requestFactory.destroy();
        }
        return llaFuzzyApiResponse;
    }

    private String getEncodedParameter(String parameter) {
        return StringUtils.isNotEmpty(parameter)?new URLEncoder().encode(parameter,"UTF-8") :Strings.EMPTY;
    }

    /**
     * get normalized api local response
     * @return LLAApiResponse
     */
    private LLAApiResponse getLocalResponse(){
        try
        {
            FileReader reader = new FileReader(Utilities.getExtensionInfo(EXTENSION_NAME).getExtensionDirectory().toString().concat(RESPONSE_FILE));
            JsonObject jsonObject = new JsonParser().parse(reader).getAsJsonObject();
            String replaceString =jsonObject.toString().replace(_SOURCE,SOURCE);
            List<NormalizedAddress> normalizedAddressList=new ArrayList<>();
            final ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            LOG.info(String.format("Address Normalization Api Response Retrieved %s ::: ", jsonObject.toString()));
            Root rootMapper=mapper.readValue(replaceString.replace(_ID,ID), Root.class);

            if(Objects.nonNull(rootMapper) && Objects.nonNull(rootMapper.getHits())){
                for (HitsRef hitRef:rootMapper.getHits().getHits())
                {
                    normalizedAddressList.add(hitRef.getSource());
                }
            }
            if (CollectionUtils.isNotEmpty(normalizedAddressList)) {
                LOG.info(String.format("Fuzzy Address Normalization List Size %s ::: ", normalizedAddressList.size()));

                return new LLAApiResponse(normalizedAddressList);
            }
            else {

                LOG.error("Error in Retrieving normalized address api Response !!! ");
                return new LLAApiResponse(Collections.EMPTY_LIST);
            }
        } catch (FileNotFoundException e) {
            LOG.error("Error in Retrieving normalized address api Response !!! ",e.getCause());
        } catch (JsonMappingException e) {
            LOG.error("Error in Retrieving normalized address api Response !!! ",e.getCause());
        } catch (JsonProcessingException e) {
            LOG.error("Error in Retrieving normalized address api Response !!! ",e.getCause());
        }
        return new LLAApiResponse(Collections.EMPTY_LIST);
    }
    /**
     * set fuzzy api response
     * @return LLAApiResponse
     */
    private LLAApiResponse setFuzzyApiResponse(String serviceResponse) {

        LLAApiResponse llaFuzzyApiResponse = null;
        if (null != serviceResponse) {
            JsonArray errorNode= (JsonArray) new JsonParser().parse(serviceResponse).getAsJsonObject().get("errors");
            JsonElement jsonElementIterator=errorNode.get(0);
            String errorCode = jsonElementIterator.getAsJsonObject().get("code").getAsString();
            String description = jsonElementIterator.getAsJsonObject().get("description").getAsString();
            String message =jsonElementIterator.getAsJsonObject().get("message").getAsString();
            llaFuzzyApiResponse = new LLAApiResponse(errorCode, message, description,false,Strings.EMPTY, Collections.emptyList());
        }
        return llaFuzzyApiResponse;
    }
}

