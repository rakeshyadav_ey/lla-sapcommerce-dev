package com.lla.mulesoft.integration.service.impl;

import com.lla.mulesoft.integration.constants.LlamulesoftintegrationConstants;
import com.lla.mulesoft.integration.service.LLAMulesoftStubApiService;
import de.hybris.platform.commercefacades.order.data.OrderData;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.net.URI;

public class LLAMulesoftStubApiServiceImpl extends LLAAbstractService implements LLAMulesoftStubApiService {

    @Override
    public void internalCreditCheck(final String siteUid)
    {
        LOG.info("Calling internal credit check Stub API");
        try {
            final String siteID =configurationService.getConfiguration().getString(new StringBuffer("website").append(".").append(siteUid).append(".").append("code").toString());

            HttpHeaders standardHeaders = setupHeaders(siteID);
            standardHeaders.set("Correlation-ID","11111111-1111-1111-11111111");
            HttpEntity<String> httpEntity = new HttpEntity<>(standardHeaders);

            final URI uri = URI.create(apiUrlConstructor(getAPIServer() ,siteID, LlamulesoftintegrationConstants.INTERNAL_CREDIT_CHECK_URL));
            HttpEntity<String> response = restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    httpEntity,
                    String.class);
            if(LOG.isDebugEnabled()){
                LOG.debug(response.getBody());
            }
            LOG.info("Internal Credit Check stub API called");

        }catch(HttpClientErrorException.BadRequest badRequest){
            LOG.error(badRequest.getResponseBodyAsString());
        }catch(HttpClientErrorException clientEx){
            LOG.error(clientEx.getMessage());
        }catch(ResourceAccessException ioException){
            LOG.error("Exception in making Api Call",ioException);
        }
    }

    @Override
    public void servicibilityCheck(final String siteUid)
    {
        try {
            LOG.info("Calling serviceability check Stub API");
            final String siteID =configurationService.getConfiguration().getString(new StringBuffer("website").append(".").append(siteUid).append(".").append("code").toString());

            HttpHeaders standardHeaders = setupHeaders(siteID);
            String body = "{\"description\": \"Maximum download/upload speed for access at an address\",\"externalId\": \"SQ101\",\"expectedQualificationDate\": \"2017-10-25T12:13:16.361Z\",\"provideAlternative\": true,\"provideOnlyAvailable\": false,\"provideUnavailabilityReason\": false,\"serviceQualificationItem\": [{\"id\": \"1\",\"service\": {\"serviceSpecification\": {\"id\": \"111\",\"href\": \"https://api:8080/telco-api/serviceCatalogManagement/v1/serviceSpecification/111\",\"name\": \"CFS_Access\"},\"serviceCharacteristic\": [{\"name\": \"downloadSpeed\",\"value\": \"\"},{\"name\": \"uploadSpeed\",\"value\": \"\"}],\"place\": [{\"href\": \"https://api:8080/telco-api/geographicAddressManagement/v1/geographicAddress/25511\",\"id\": \"25511\",\"name\": \"160 de Versailles Avenue 75016 Paris France\",\"role\": \"installationAddress\",\"@type\": \"GeographicAddress\"}],\"expectedServiceAvailabilityDate\": \"2017-10-27T12:14:16.361Z\"}}],\"relatedParty\": [{\"id\": \"14\",\"href\": \"https://api:8080/telco-api/partyManagement/v1/party/14\",\"role\": \"requester\",\"name\": \"John Doe\",\"@referredType\": \"Individual\"}]}";
            HttpEntity<String> httpEntity = new HttpEntity<>(body, standardHeaders);
            final URI uri = URI.create(apiUrlConstructor(getAPIServer() ,siteID, LlamulesoftintegrationConstants.SERVICIBILTY_CHECK_URL));
            HttpEntity<String> response = restTemplate.exchange(
                    uri,
                    HttpMethod.POST,
                    httpEntity,
                    String.class);
            if(LOG.isDebugEnabled()){
                LOG.debug(response.getBody());
            }
            LOG.info("Servicability Check API called");
        }catch (Exception ex){
            LOG.error("Unable to make external credit check Stub API.", ex);
        }
    }

    @Override
    public void externalCreditCheck(final String siteUid)
    {
        try {
            LOG.info("Calling external credit check Stub API");
            final String siteID =configurationService.getConfiguration().getString(new StringBuffer("website").append(".").append(siteUid).append(".").append("code").toString());
            HttpHeaders standardHeaders = setupHeaders(siteID);
            HttpEntity<String> httpEntity = new HttpEntity<>(standardHeaders);
            final URI uri = URI.create(apiUrlConstructor(getAPIServer() ,siteID, LlamulesoftintegrationConstants.EXTERNAL_CREDIT_CHECK_URL));
            HttpEntity<String> response = restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    httpEntity,
                    String.class);
            if(LOG.isDebugEnabled()){
                LOG.debug(response.getBody());
            }
            LOG.info("External credit check API called");
        }catch (Exception ex){
            LOG.error("Unable to make external credit check Stub API.", ex);
        }
    }
}
