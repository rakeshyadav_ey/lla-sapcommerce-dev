package com.lla.mulesoft.integration.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lla.mulesoft.integration.constants.LlamulesoftintegrationConstants;
import com.lla.mulesoft.integration.dto.PlanServices;
import com.lla.mulesoft.integration.dto.ServiceProducts;
import com.lla.mulesoft.integration.dto.ViewPlanResponse;
import com.lla.mulesoft.integration.service.LLAMulesoftViewPlanService;
import de.hybris.platform.b2ctelcoservices.enums.TmaAccessType;
import de.hybris.platform.b2ctelcoservices.jalo.TmaSubscribedProduct;
import de.hybris.platform.b2ctelcoservices.jalo.TmaSubscriptionAccess;
import de.hybris.platform.b2ctelcoservices.model.TmaBillingAccountModel;
import de.hybris.platform.b2ctelcoservices.model.TmaSubscribedProductModel;
import de.hybris.platform.b2ctelcoservices.model.TmaSubscriptionAccessModel;
import de.hybris.platform.b2ctelcoservices.model.TmaSubscriptionBaseModel;
import de.hybris.platform.b2ctelcoservices.services.TmaBillingAccountService;
import de.hybris.platform.b2ctelcoservices.services.TmaSubscribedProductService;
import de.hybris.platform.b2ctelcoservices.services.TmaSubscriptionAccessService;
import de.hybris.platform.b2ctelcoservices.services.TmaSubscriptionBaseService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.SetUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.params.BasicHttpParams;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.*;

public class LLAMulesoftViewPlanServiceImpl extends  LLAAbstractService implements LLAMulesoftViewPlanService {

    private static final Logger LOG = Logger.getLogger(LLAMulesoftViewPlanServiceImpl.class);
    @Override
    public String getCustomerPlanDetails(List<String> accountIds, final String customerId) {

        if(CollectionUtils.isNotEmpty(accountIds)){
           LOG.info(String.format("Starting Mulesoft Connectivity for Retrieval of Subscribed Plans of Customer %s for accountID %s",
                    customerId,String.join(", ", accountIds)));
            final String viewPlanEndPoint = apiUrlConstructor(getAPIServer(),getBusinessUnit(),LlamulesoftintegrationConstants.VIEW_PLANS);
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(viewPlanEndPoint)
                    .queryParam("accountNo",String.join(", ", accountIds));


            HttpHeaders standardHeaders =  setupHeaders("JM");
            standardHeaders.set("correlation-ID","11111111-1111-1111-11111111");
            HttpEntity<String> requestEntity = new HttpEntity<String>(standardHeaders);
            ResponseEntity<String> serviceResponse=null;
            try
            {
                serviceResponse=restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, requestEntity, String.class);
                if(serviceResponse.getStatusCode()== HttpStatus.OK){
                    LOG.info(String.format("Subscription Plan Retrieved from Mulesoft for Customer :: %s",customerId));
                    LOG.info(String.format("Subscription Plan Retrieved %s ::: ",serviceResponse.getBody()));
                    return serviceResponse.getBody();
                }else{
                    LOG.error("Error in Retrieving View Plan Response from Mulesoft server !!! ");
                   return StringUtils.EMPTY;
                }

            }catch(HttpClientErrorException.BadRequest badRequest){
                LOG.error(badRequest.getResponseBodyAsString());
            }catch(HttpClientErrorException clientEx){
                LOG.error(clientEx.getMessage());
            }catch(ResourceAccessException ioException){
                LOG.error("Exception in making Api Call",ioException);
            }
         }
        return StringUtils.EMPTY;

    }
}
