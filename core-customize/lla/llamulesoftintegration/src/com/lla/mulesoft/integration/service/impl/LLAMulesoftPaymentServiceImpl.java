package com.lla.mulesoft.integration.service.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lla.core.enums.SourceEPC;
import com.lla.core.model.LLAProductMapperModel;
import com.lla.mulesoft.integration.constants.LlamulesoftintegrationConstants;
import com.lla.mulesoft.integration.service.LLAMulesoftPaymentService;
import com.lla.telcotmfwebservices.v2.dto.*;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.SplitOrderProcessModel;
import de.hybris.platform.product.ProductService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class LLAMulesoftPaymentServiceImpl  extends LLAAbstractService implements LLAMulesoftPaymentService {

    @Resource(name = "productService")
    private ProductService productService;

    @Autowired
    private CategoryService categoryService;

    @Override
    public String processPayment(OrderModel order, SplitOrderProcessModel orderProcessModel) throws ResourceAccessException{
        LOG.info(String.format("Generating process payment request for order %s ",order.getCode()));
        final CustomerModel customer = (CustomerModel)order.getUser();
        final String siteCode=order.getSite().getUid();
        Payment message = generateRequestBody(order,customer);
        Gson gson = new GsonBuilder().setLenient().create();
        String postBody = gson.toJson(message);
        postBody = postBody.replace("attype","@type");
        final String siteID =configurationService.getConfiguration().getString(new StringBuffer(LlamulesoftintegrationConstants.WEBSITE).append(LlamulesoftintegrationConstants.DECIMAL).append(order.getSite().getUid()).append(LlamulesoftintegrationConstants.DECIMAL).append(LlamulesoftintegrationConstants.CODE).toString());
        HttpHeaders standardHeaders =  setupHeaders(siteID);
        LOG.info(String.format("Process Payment Request Content-Type Header for order %s is %s",order.getCode(),standardHeaders.get("Content-Type")));
        LOG.info(String.format("Process Payment Request JSON for order %s is %s",order.getCode(),postBody));
        HttpEntity<String> httpEntity = new HttpEntity<>(postBody, standardHeaders);
        final String parameter = new StringBuffer(configurationService.getConfiguration().getString(LlamulesoftintegrationConstants.PROCESS_PAYMENT_PARAMETER)).append(fetchCustomerAccountId(order)).toString();
        URI process_payment_post_url = URI.create(apiUrlConstructor(getAPIServer(),siteID,LlamulesoftintegrationConstants.PROCESS_PAYMENT,parameter));
        RestTemplate restTemplate_new=this.createRestTemplate(this.createRequestFactory(configurationService.getConfiguration().getString("max.pool.connection")));

        ResponseEntity<String> serviceResponse=null;
        try{
            serviceResponse= restTemplate_new.exchange(process_payment_post_url, HttpMethod.POST,httpEntity,String.class);
            if(serviceResponse.getStatusCode()== HttpStatus.CREATED){
                LOG.info(String.format("Process Payment Complete for order %s ",order.getCode()));
            }else{
                LOG.error(String.format("Error in processing payment. Server HTTP response :- %s",serviceResponse.getStatusCodeValue()));
                validateAPIResponse(serviceResponse);
                return StringUtils.EMPTY;
            }
        }catch(HttpClientErrorException.BadRequest badRequest){
            LOG.error(String.format("Post payment for order :: %s failed due to Bad request :: %s ",order.getCode(),badRequest.getResponseBodyAsString()));
        }catch(HttpClientErrorException clientEx){
            LOG.error(String.format("Post payment for order :: %s failed due to Client error exception :: %s ",order.getCode(),clientEx.getMessage()));
        }catch(ResourceAccessException ioException){
            /*  throw new TmaApiException(ioException.getMessage()) ; */
            LOG.info(String.format("Response from the post payment api is :: %s",serviceResponse));
            LOG.error(String.format("Exception in making Post payment api Call due to :: %s ",ioException.getMessage()));
            throw ioException;
        }
        finally {
            HttpComponentsClientHttpRequestFactory requestFactory=(HttpComponentsClientHttpRequestFactory) restTemplate_new.getRequestFactory();
            LOG.info("Releasing current client connection for Post Payment Service Call Request");
            try {
                requestFactory.destroy();
            } catch (Exception exception) {
                LOG.error(String.format("Error in releasing Connection obtained for  Post Payment Service Call Request for  Market  %s and due to %s  ",siteCode,exception));
            }
        }
        return null!=serviceResponse?serviceResponse.getBody():StringUtils.EMPTY;
    }

    private Payment generateRequestBody(OrderModel order, CustomerModel customer){
        final Payment message = new Payment();
        message.setCorrelatorId(order.getPaymentTransactions().get(0).getRequestToken());
        message.setName(configurationService.getConfiguration().getString(LlamulesoftintegrationConstants.PROCESS_PAYMENT_ECOM));
        message.setDescription(new StringBuffer(configurationService.getConfiguration().getString(LlamulesoftintegrationConstants.PROCESS_PAYMENT_EC)).append(customer.getFlowId()+fetchCustomerAccountId(order)).append("_").append(order.getCode()).toString());

        populatePayment(message,order);
        populateChannel(message);
        populateAccount(message,order);
        populatePaymentItem(message,order);
        populatePaymentMethod(message,order);
        populatePayer(message,customer);

        return message;
    }

    private void populatePayment(Payment message, OrderModel order)
    {
        PaymentMoney amount = new PaymentMoney();
        amount.setAmount(order.getSubtotal().intValue());
        message.setAmount(amount);

        PaymentMoney totalTax = new PaymentMoney();
        totalTax.setAmount(order.getTotalTax().intValue());
        message.setTaxAmount(totalTax);

        PaymentMoney totalPrice = new PaymentMoney();
        totalPrice.setAmount(order.getTotalPrice().intValue());
        message.setTotalAmount(totalPrice);
    }

    private void populateChannel(Payment message)
    {
        ChannelRef channelRef = new ChannelRef();
        channelRef.setId(configurationService.getConfiguration().getString(LlamulesoftintegrationConstants.PROCESS_PAYMENT_ECOM));
        channelRef.setName(configurationService.getConfiguration().getString(LlamulesoftintegrationConstants.PROCESS_PAYMENT_ECOM));
        message.setChannel(channelRef);
    }

    private void populateAccount(Payment message, OrderModel order)
    {
        AccountRef accountRef = new AccountRef();
        accountRef.setId(fetchCustomerAccountId(order));
        message.setAccount(accountRef);
    }

    private void populatePaymentItem(Payment message, OrderModel order)
    {
        List<PaymentItem> listOfPaymentItem = new ArrayList<>();

        PaymentItem paymentItem = new PaymentItem();

        PaymentMoney amount = new PaymentMoney();
        amount.setAmount(order.getSubtotal().intValue());
        paymentItem.setAmount(amount);

        PaymentMoney tax = new PaymentMoney();
        tax.setAmount(order.getTotalTax().intValue());
        paymentItem.setTaxAmount(tax);

        PaymentMoney totalAmount = new PaymentMoney();
        totalAmount.setAmount(order.getTotalPrice().intValue());
        paymentItem.setTotalAmount(totalAmount);

        EntityRef entityRef = new EntityRef();
        entityRef.setId(order.getCode());
        entityRef.setReferredType(configurationService.getConfiguration().getString(LlamulesoftintegrationConstants.PROCESS_PAYMENT_DEPOSIT));
        entityRef.setName(configurationService.getConfiguration().getString(LlamulesoftintegrationConstants.PROCESS_PAYMENT_TARIFF_CODE));
        paymentItem.setItem(entityRef);

        listOfPaymentItem.add(paymentItem);

        message.setPaymentItem(listOfPaymentItem);
    }

    private void populatePaymentMethod(Payment message, OrderModel order)
    {
        PaymentMethod paymentMethod = new PaymentMethod();
        CreditCardPaymentInfoModel paymentInfo = (CreditCardPaymentInfoModel) order.getPaymentInfo();
        paymentMethod.setAttype(paymentInfo.getType().toString());
        List<BankCardType> bankCardTypeList = new ArrayList<>();
        bankCardTypeList.add(populateBankCardType(order));
        paymentMethod.setDetails(bankCardTypeList);
        message.setPaymentMethod(paymentMethod);
    }

    private void populatePayer(Payment message, CustomerModel customer)
    {
        RelatedParty payer = new RelatedParty();
        if(StringUtils.isNotEmpty(customer.getCerillionCustomerNo()))
        {
            payer.setId(customer.getCerillionCustomerNo());
        }
        else
        {
            payer.setId(customer.getLiberateCustomerNo());
        }
        message.setPayer(payer);
    }

    private String fetchCustomerAccountId(OrderModel order)
    {
        final CustomerModel customer = (CustomerModel)order.getUser();
        SourceEPC orderSource = order.getEntries().get(0).getProductMapper().getSourceEPC();
        if(null!=order.getBillingAccount()){
            return order.getBillingAccount().getBillingAccountId();
        }
        else if(orderSource.equals(SourceEPC.CERILLION) && CollectionUtils.isNotEmpty(customer.getCerillionAccounts())) {
            return customer.getCerillionAccounts().get(customer.getCerillionAccounts().size()-1);
        }
        else if(orderSource.equals(SourceEPC.LIBERATE) && CollectionUtils.isNotEmpty(customer.getLiberateAccounts())){
            return customer.getLiberateAccounts().get(customer.getLiberateAccounts().size()-1);
        }
        LOG.info(String.format("Cannot fetch Billing Account for order %s in Post payment API",order.getCode()));
        return StringUtils.EMPTY;
    }

    private BankCardType populateBankCardType(OrderModel order)
    {
        BankCardType bankCardType = new BankCardType();
        for(AbstractOrderEntryModel entry: order.getEntries())
        {
            LLAProductMapperModel productMapper = entry.getProductMapper();
            SourceEPC orderSource = productMapper.getSourceEPC();
            if(orderSource.equals(SourceEPC.CERILLION))
            {
                bankCardType.setType(BankCardType.TypeEnum.OC);
            }
            else if(orderSource.equals(SourceEPC.LIBERATE))
            {
                bankCardType.setType(BankCardType.TypeEnum.E);
            }
            break;
        }
        return bankCardType;
    }
}
