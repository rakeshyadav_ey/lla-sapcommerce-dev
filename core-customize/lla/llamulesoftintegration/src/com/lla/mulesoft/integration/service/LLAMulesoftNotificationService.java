package com.lla.mulesoft.integration.service;
import de.hybris.platform.core.model.order.CartModel;
import com.lla.mulesoft.integration.exception.LLAApiException;
import de.hybris.platform.acceleratorcms.model.components.ClickToCallModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import java.util.List;
import com.lla.core.enums.ClickToCallReasonEnum;


public interface LLAMulesoftNotificationService
{

	/**
	 * Generate Order Notification to CUstomer
	 * @param orderModel
	 * @return
	 * @throws LLAApiException
	 */
    String generateOrderNotification(OrderModel orderModel) throws LLAApiException;

	/**
	 * Generate CS AGent Notification
	 * @param order
	 * @param mailIds
	 * @param ssnValue
	 * @return
	 * @throws LLAApiException
	 */
	String triggerCSA_Notification(OrderModel order, List<String> mailIds,final String ssnValue) throws LLAApiException;
	/**
	 * Generate Order Cancel Notification
	 * @param order
	 * @return
	 * @throws LLAApiException
	 */
	String generateOrderCancelNotification(OrderModel order) throws LLAApiException;

	/**
	 * Generate Fallout KPI Notification For Cabletica
	 * @param cartModel
	 * @param notificationType
	 * @param clickToCallReasonEnum
	 * @return
	 * @throws LLAApiException
	 */
	String generateKPINotifications(final AbstractOrderModel cartModel, String notificationType,
									final ClickToCallReasonEnum clickToCallReasonEnum) throws LLAApiException;

	/**
	 * Send Order Release or Provision Email to Customer
	 * @param order
	 * @return
	 * @throws LLAApiException
	 */
	String sendOrderProvisionMail(OrderModel order) throws LLAApiException;

	/**
	 * Generate Payment Notification Email
	 * @param order
	 * @return
	 * @throws LLAApiException
	 */
	String generatePaymentNotification(AbstractOrderModel order) throws LLAApiException;

	/**
	 * Generate Freshdesk Fallout Notification
	 * @param cartModel
	 * @param notificationType
	 * @param clickToCallReasonEnum
	 * @param ssnValue
	 * @param houseKey
	 * @return
	 * @throws LLAApiException
	 */
	String generateFreshdeskFalloutNotifications(final CartModel cartModel, String notificationType,
			final ClickToCallReasonEnum clickToCallReasonEnum, String ssnValue,String houseKey) throws LLAApiException;

	/**
	 * Generate C2C Agent Notification
	 *
	 * @param lang
	 * @param baseSite
	 * @param requestNumber
	 * @param customerName
	 * @param planCode
	 * @param numberOfLines
	 * @param deviceCode
	 * @param customerLastName
	 * @param phoneNumber
	 * @param email
	 * @param colorCode
	 * @param capacityCode
	 * @return
	 * @throws LLAApiException
	 */
	String triggerC2C_AgentNotification( String lang, String baseSite, String requestNumber, String customerName, String planCode, Integer numberOfLines, String deviceCode, String customerLastName, String phoneNumber, String email, String colorCode, String capacityCode) throws LLAApiException;

	/**
	 * Generate C2C Agent Notification
	 *
	 *
	 * @param lang
	 * @param baseSite
	 * @param requestNumber
	 * @param customerName
	 * @param planCode
	 * @param numberOfLines
	 * @param deviceCode
	 * @param customerLastName
	 * @param phoneNumber
	 * @param email
	 * @param colorCode
	 * @param capacityCode
	 * @return
	 * @throws LLAApiException
	 */
	String triggerC2C_CustomerNotification(String lang, String baseSite, String requestNumber, String customerName, String planCode, Integer numberOfLines, String deviceCode, String customerLastName, String phoneNumber, String email, String colorCode, String capacityCode) throws LLAApiException;
}
