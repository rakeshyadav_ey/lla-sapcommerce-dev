package com.lla.mulesoft.integration.service;

import com.lla.mulesoft.integration.exception.LLAApiException;
import de.hybris.platform.core.model.order.OrderModel;

public interface LLAMulesoftBillingAccountService {
    /**
     *  Create Billing Account for Cerllion
     * @param order
     * @return
     * @throws LLAApiException
     */
    String createBillingAccount(OrderModel order) throws LLAApiException;
}
