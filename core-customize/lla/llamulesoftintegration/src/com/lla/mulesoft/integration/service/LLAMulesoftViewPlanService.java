package com.lla.mulesoft.integration.service;

import java.util.List;

public interface LLAMulesoftViewPlanService {
    String getCustomerPlanDetails(final List<String> accountIds, final String customerId);
}
