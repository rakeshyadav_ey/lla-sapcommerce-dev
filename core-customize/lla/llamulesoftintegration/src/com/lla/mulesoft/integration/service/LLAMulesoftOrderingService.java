package com.lla.mulesoft.integration.service;

import com.lla.mulesoft.integration.exception.LLAApiException;
import de.hybris.platform.core.model.order.OrderModel;

public interface LLAMulesoftOrderingService {
    /**
     * Send Place Order Provision Request to Downstream System
     * @param order
     * @param sourceEpc
     * @return
     * @throws LLAApiException
     */
    String placeOrder(OrderModel order, String sourceEpc) throws LLAApiException;

    /**
     * Send Place Order Provision Request for LCPR to Downstream System
     * @param order
     * @param sourceEpc
     * @return
     * @throws LLAApiException
     */
    String lcprPlaceOrder(OrderModel order, String sourceEpc) throws LLAApiException;
}
