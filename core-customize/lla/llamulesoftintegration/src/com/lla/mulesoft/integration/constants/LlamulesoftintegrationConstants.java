/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.mulesoft.integration.constants;

/**
 * Global class for all Llamulesoftintegration constants. You can add global constants for your extension into this
 * class.
 */
public final class LlamulesoftintegrationConstants extends GeneratedLlamulesoftintegrationConstants
{
	public static final String EXTENSIONNAME = "llamulesoftintegration";
	public static final String MULESOFT_ENDPOINT_SERVER = "mulesoft.endpoint.server";
	public static final String MULESOFT_CUSTOMER_REQUEST_URL = "/esb-test/customer";
	public static final String PLACE_ORDER_NOTIFICATION = "api.notification.placeorder";
	public static final String PANAMA_PLACE_ORDER_NOTIFICATION_EN = "panama.api.notification.placeorder.eng";
	public static final String PANAMA_PLACE_ORDER_NOTIFICATION_ES = "panama.api.notification.placeorder.esp";
	public static final String PUERTORICO_PLACE_ORDER_NOTIFICATION_EN = "puertorico.api.notification.placeorder.eng";
	public static final String PUERTORICO_PLACE_ORDER_NOTIFICATION_ES = "puertorico.api.notification.placeorder.esp";

	public static final String CABLETICA_PLACE_ORDER_NOTIFICATION_ES = "cabletica.api.notification.placeorder.esp";

	public static final String CABLETICA_RELEASE_ORDER_NOTIFICATION_ES = "cabletica.api.notification.releaseorder.esp";
	public static final String CABLETICA_CANCEL_ORDER_NOTIFICATION_ES = "cabletica.api.notification.cancelorder.esp";
	

	public static final String CABLETICA_CSA_LEAD_NOTIFICATION = "cabletica.api.kpi.notification.csa.abandoned.cart";
	public static final String CABLETICA_CSA_DEBT_NOTIFICATION = "cabletica.api.kpi.notification.csa.debt";
	public static final String CABLETICA_CSA_CURRENT_CUSTOMER_NOTIFICATION = "cabletica.api.kpi.notification.csa.existing.customer";
	public static final String CABLETICA_CSA_NOT_SERVICEABLE_NOTIFICATION = "cabletica.api.kpi.notification.csa.noserviceability";

	public static final String PANAMA_PREPAID_PLACE_ORDER_NOTIFICATION_EN = "panama.api.notification.prepaid.placeorder.eng";
	public static final String PANAMA_PREPAID_PLACE_ORDER_NOTIFICATION_ES = "panama.api.notification.prepaid.placeorder.esp";

	public static final String CANCEL_ORDER_NOTIFICATION = "api.notification.cancelOrder";
	public static final String PANAMA_CANCEL_ORDER_NOTIFICATION_EN = "panama.api.notification.cancelOrder.eng";
	public static final String PANAMA_CANCEL_ORDER_NOTIFICATION_ES = "panama.api.notification.cancelOrder.esp";

	public static final String PUERTORICO_CANCEL_ORDER_NOTIFICATION_EN = "puertorico.api.notification.cancelOrder.eng";
	public static final String PUERTORICO_CANCEL_ORDER_NOTIFICATION_ES = "puertorico.api.notification.cancelOrder.esp";

	public static final String PANAMA_PREPAID_CANCEL_ORDER_NOTIFICATION_EN = "panama.api.notification.prepaid.cancelOrder.eng";
	public static final String PANAMA_PREPAID_CANCEL_ORDER_NOTIFICATION_ES = "panama.api.notification.prepaid.cancelOrder.esp";


	public static final String CSA_ORDER_NOTIFICATION = "api.notification.csa";
	public static final String PANAMA_CSA_ORDER_NOTIFICATION = "panama.api.notification.csa";
	public static final String PUERTORICO_CSA_ORDER_NOTIFICATION = "puertorico.api.notification.csa";
	
	public static final String PUERTORICO_CSA_FRESHDESKFALLOUT_NOTIFICATION = "puertorico.api.notification.freshdeskfallout.esp";

	public static final String PUERTORICO_C2C_NEW_CSA_ORDER_NOTIFICATION = "puertorico.api.notification.c2cnew.csa";

	public static final String PUERTORICO_PLACE_ORDER_NOTIFICATION_FALLBACK_ES="puertorico.api.notification.placeorder.fallback.esp";
	public static final String PUERTORICO_PLACE_ORDER_NOTIFICATION_FALLBACK_EN="puertorico.api.notification.placeorder.fallback.eng";

	public static final String PUERTORICO_C2C_NEW_CUSTOMER_ORDER_NOTIFICATION_EN = "puertorico.api.notification.c2cnew.customer.eng";
	public static final String PUERTORICO_C2C_NEW_CUSTOMER_ORDER_NOTIFICATION_ES = "puertorico.api.notification.c2cnew.customer.esp";

	public static final String CABLETICA_CSA_ORDER_NOTIFICATION = "cabletica.api.notification.csa";
	public static final String CABLETICA_SERVICEABILITY = "cabletica.api.serviceablity";
	public static final String CABLETICA_SERVICEABILITY_PARAMETER = "cabletica.api.serviceablity.parameter";


	public static final String PANAMA_PREPAID_CSA_ORDER_NOTIFICATION = "panama.api.notification.prepaid.csa";

	public static final String BILLING_ACCOUNT_SOURCE_EPC_CERILLION = "api.create.billing.account.cerillion";
	public static final String CREATE_CUSTOMER = "api.create.customer.url";
	public static final String PANAMA_CREATE_CUSTOMER = "panama.api.create.customer.url";
	public static final String PROVISION_ORDER = "api.provision.order.url";
	public static final String RELEASE_ORDER_NOTIFICATION = "api.notification.releaseOrder";
	public static final String PANAMA_RELEASE_ORDER_NOTIFICATION_EN = "panama.api.notification.releaseOrder.eng";
	public static final String PANAMA_RELEASE_ORDER_NOTIFICATION_ES = "panama.api.notification.releaseOrder.esp";

	public static final String PUERTORICO_RELEASE_ORDER_NOTIFICATION_EN = "puertorico.api.notification.releaseOrder.eng";
	public static final String PUERTORICO_RELEASE_ORDER_NOTIFICATION_ES = "puertorico.api.notification.releaseOrder.esp";

	public static final String PANAMA_PREPAID_RELEASE_ORDER_NOTIFICATION_EN = "panama.api.notification.prepaid.releaseOrder.eng";
	public static final String PANAMA_PREPAID_RELEASE_ORDER_NOTIFICATION_ES = "panama.api.notification.prepaid.releaseOrder.esp";

	public static final String VTR_PAYMENT_ORDER_NOTIFICATION_ES = "vtr.api.notification.payament.pending.esp";
	public static final String VTR_PLACE_ORDER_NOTIFICATION_ES = "vtr.api.notification.placeorder.esp";
	public static final String VTR_CANCEL_ORDER_NOTIFICATION_ES = "vtr.api.notification.cancelorder.esp";
	public static final String VTR_RELEASE_ORDER_NOTIFICATION_ES = "vtr.api.notification.releaseorder.esp";
	public static final String VTR_CSA_ORDER_NOTIFICATION = "vtr.api.notification.csa";

	public static final String EXTERNAL_CREDIT_CHECK_URL = "api.external.creditcheck.url";
	public static final String INTERNAL_CREDIT_CHECK_URL = "api.internal.creditcheck.url";
	public static final String SERVICIBILTY_CHECK_URL = "api.servicibility.check.url";
	public static final String VIEW_PLANS = "api.view.plans.url";
	public static final String PROCESS_PAYMENT = "api.process.payment.url";
	public static final String PROCESS_PAYMENT_PARAMETER = "api.process.payment.url.parameter";
	public static final String PROCESS_PAYMENT_ECOM = "llamulesoftintegration.payment.ecom";
	public static final String PROCESS_PAYMENT_DEPOSIT = "llamulesoftintegration.payment.deposit";
	public static final String PROCESS_PAYMENT_EC = "llamulesoftintegration.payment.ec";
	public static final String PROCESS_PAYMENT_TARIFF_CODE = "llamulesoftintegration.payment.tariff.code";
	public static final String WEBSITE = "website";
	public static final String DECIMAL = ".";
	public static final String CODE = "code";
	public static final String CLICK_TO_CALL ="api.create.callback.request.url";
	public static final String MULESOFT_ENDPOINT_SERVER_CALL_BACK="mulesoft.endpoint.server.callback";
	public static final String DEVICES_CATEGORY = "devices";
	public static final String ADDON = "addon";
	public static final String ADDONS = "addons";
	public static final String BUNDLE = "bundle";
	public static final String HFC_TECHNOLOGY = "HFC";
	public static final String FTTH_TECHNOLOGY = "FTTH";
	public static final String INSTALLATION = "installation";

	public static final String DUMMY_IDENTITY_NUMBER = "cabletica.dummy.identity.number";

	public static final String CUSTOMER_CREDIT_CHECK_URL= "api.customer.creditcheck.url";
	public static final String CABLETICA_CUSTOMER_VALIDATION = "mulesoft.endpoint.server.customer";
	public static final String CABLETICA_DEBT_CHECK ="mulesoft.endpoint.server.debtCheck";
    public static final String IDENTITY_TYPE = "identityType";
	public static final String IDENTITY_NUMBER = "identityNumber";
	public static final String FUZZY_ADDRESS_NORMALIZATION_URL="fuzzy.addressNormalization.api.endPoint";
	public static final String FUZZY_ADDRESS_NORMALIZATION_PARAMETER = "fuzzy.addressNormalization.api.parameter";
	public static final String FUZZY_ADDRESS_NORMALIZATION_QUERY = "fuzzy.addressNormalization.api.query";
	public static final String PUERTORICO_FUZZY_ADDRESS_BUSINESS_ID ="puertorico.fuzzy.addressNormalization.api.businessId";
	public static final String PUERTORICO_SERVICEABILITY = "puertorico.api.serviceablity";
	public static final String PUERTORICO_SERVICEABILITY_PARAMETER = "puertorico.api.serviceablity.parameter";
	private LlamulesoftintegrationConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
