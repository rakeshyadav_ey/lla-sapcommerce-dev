package com.lla.mulesoft.integration.service;

import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.response.LLAApiResponse;
import com.lla.telcotmfwebservices.v2.dto.NormalizedAddress;

import java.util.List;

/**
 * The interface Lla fuzzy address normalization api service.
 */
public interface LLAFuzzyAddressNormalizationApiService {

    /**
     * Gets normalized address list.
     *
     * @param addressData the address data
     * @return the normalized address list
     * @throws Exception the lla api exception
     */
    LLAApiResponse getNormalizedAddressList(final String addressData) throws Exception;
}
