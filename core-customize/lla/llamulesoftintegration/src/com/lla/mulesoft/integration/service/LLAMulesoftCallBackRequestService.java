package com.lla.mulesoft.integration.service;

import com.lla.core.enums.ClickToCallReasonEnum;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import com.lla.mulesoft.integration.dto.ClickCallServicablity;
import com.lla.mulesoft.integration.exception.LLAApiException;


public interface LLAMulesoftCallBackRequestService
{
	/**
	 * Create Call Back Request To Genesys
	 * @param abstractOrderModel
	 * @param clickCallServicablity
	 * @param clickToCallReasonEnum
	 * @return
	 * @throws Exception
	 */
	String createCallBackRequest(AbstractOrderModel abstractOrderModel, ClickCallServicablity clickCallServicablity, ClickToCallReasonEnum clickToCallReasonEnum) throws Exception;
	

}