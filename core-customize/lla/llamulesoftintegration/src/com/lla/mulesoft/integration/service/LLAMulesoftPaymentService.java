package com.lla.mulesoft.integration.service;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.SplitOrderProcessModel;
import org.springframework.web.client.ResourceAccessException;

public interface LLAMulesoftPaymentService {
    /**
     * Process Payment Service Request
     * @param order
     * @param orderProcessModel
     * @return
     * @throws ResourceAccessException
     */
    String processPayment(OrderModel order, SplitOrderProcessModel orderProcessModel) throws ResourceAccessException;
}
