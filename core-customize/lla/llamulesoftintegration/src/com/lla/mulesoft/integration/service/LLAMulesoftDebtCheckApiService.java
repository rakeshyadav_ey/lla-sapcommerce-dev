package com.lla.mulesoft.integration.service;

import de.hybris.platform.core.model.order.CartModel;
import org.apache.http.conn.ConnectTimeoutException;

import com.lla.mulesoft.integration.response.LLAApiResponse;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;

public interface LLAMulesoftDebtCheckApiService {
    /**
     * Get Debt Check Details of Customer
     * @param cart
     * @param encryptedSSN
     * @return
     * @throws Exception
     */
    LLAApiResponse getDebtCheckForCustomer(CartModel cart, String encryptedSSN) throws Exception;

}
