package com.lla.mulesoft.integration.service;

import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.response.LLAApiResponse;
import de.hybris.platform.core.model.order.AbstractOrderModel;

public interface LLAMulesoftCreditCheckService {
    /**
     * Perform Customer Credit Check
     * @param orderModel
     * @param encryptedSSN
     * @return
     * @throws LLAApiException
     *
     */
    LLAApiResponse customerCreditCheck(final AbstractOrderModel orderModel, final String encryptedSSN) throws LLAApiException;
}
