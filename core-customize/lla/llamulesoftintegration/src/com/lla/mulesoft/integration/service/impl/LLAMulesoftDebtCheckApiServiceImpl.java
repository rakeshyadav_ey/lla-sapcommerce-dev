package com.lla.mulesoft.integration.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonSyntaxException;
import com.lla.mulesoft.integration.constants.LlamulesoftintegrationConstants;
import com.lla.mulesoft.integration.response.LLAApiResponse;
import com.lla.mulesoft.integration.service.LLAMulesoftDebtCheckApiService;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import com.lla.telcotmfwebservices.v2.dto.AccountBalance;
import com.lla.telcotmfwebservices.v2.dto.BillingAccount;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.commons.lang.StringUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.net.ssl.SSLException;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class LLAMulesoftDebtCheckApiServiceImpl extends LLAAbstractService implements LLAMulesoftDebtCheckApiService {

    private static final Logger LOG = Logger.getLogger(LLAMulesoftDebtCheckApiServiceImpl.class);
    private static final String MALFORMED_JSON_CODE="422";

    @Autowired
    private ModelService modelService;

    @Autowired
    private CommonI18NService commonI18NService;
    @Autowired
    private SessionService sessionService;
    @Autowired
 	 private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;
    @Override
    public LLAApiResponse getDebtCheckForCustomer(CartModel cart, String encryptedSSN) throws Exception {
        LOG.info("Starting Mulesoft Connectivity");
        LLAApiResponse llaDebtCheckApiResponse = null;
        final String businessUnit=getBusinessUnit();
        final String debtCheckEndPoint = apiUrlConstructor(getAPIServer(),getBusinessUnit(),LlamulesoftintegrationConstants.CABLETICA_DEBT_CHECK);
        final CustomerModel customerData = (CustomerModel)cart.getUser();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(debtCheckEndPoint)
                .queryParam(LlamulesoftintegrationConstants.IDENTITY_NUMBER,customerData.getDocumentNumber().replace("-", ""))
                .queryParam("balanceCheck","true")
                .queryParam(LlamulesoftintegrationConstants.IDENTITY_TYPE,
    						(llaMulesoftIntegrationUtil.isPuertoricoOrder(cart.getSite())) ? "SSN" : getDocumentType(customerData));


        HttpHeaders standardHeaders = null;
  		if (llaMulesoftIntegrationUtil.isCableticaOrder(cart.getSite()))
  		{
  			standardHeaders = setupCRHeaders("CR");
  		}
  		standardHeaders = setupHeaders(getBusinessUnit());
  		if (llaMulesoftIntegrationUtil.isPuertoricoOrder(cart.getSite()))
		{
            headers.set("client-id",configurationService.getConfiguration().getString("puertorico.mulesoft.api.clientid"));
            headers.set("client-secret", configurationService.getConfiguration().getString("puertorico.mulesoft.api.clientsecret"));
            headers.set("channelId", configurationService.getConfiguration().getString("puertorico.mulesoft.api.channelId"));
            headers.set("lob", configurationService.getConfiguration().getString("mulesoft.api.lineofbusiness"));
           // headers.set("correlationId","correlationId");
            headers.set("x-tax-identifier",encryptedSSN);
			headers.set("businessId", "PR");
		}
        HttpEntity<String> requestEntity = new HttpEntity<String>(standardHeaders);
        ResponseEntity<String> serviceResponse=null;
        boolean returnFlag=false;
        RestTemplate restTemplate_new=this.createRestTemplate(this.createRequestFactory(configurationService.getConfiguration().getString("max.pool.connection")));

        try
        {
          //  setMuleSoftConnectionTimeout(restTemplate_new);
            serviceResponse=restTemplate_new.exchange(builder.build().encode().toUri(), HttpMethod.GET, requestEntity, String.class);
            LOG.info(String.format("Debt Check Api Response Retrieved for Market %s ::: Response Body ::%s", businessUnit, serviceResponse.getBody()));
            if (serviceResponse.getStatusCode() == HttpStatus.OK)
            {
               // LOG.info(String.format("Debt Check Api Response Retrieved for Market %s ::: Response Body ::%s", businessUnit, serviceResponse.getBody()));
                final ObjectMapper mapper = new ObjectMapper();
                mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                if (!StringUtils.isEmpty(String.valueOf(serviceResponse.getBody())))
                {
                    try
                    {
                        final List<BillingAccount> customerResponseList = Arrays
                                .asList(mapper.readValue(serviceResponse.getBody(), BillingAccount[].class));

                        if (!customerResponseList.isEmpty())
                        {
                            LOG.info("Billing Account Id :"+customerResponseList.get(0).getId());
                            returnFlag = checkForDebtBalance(customerResponseList, cart, getBusinessUnit());
                            return new LLAApiResponse(returnFlag);
                        }
                    }
                    catch (final JsonProcessingException excep)
                    {
                        LOG.error(String.format("Error Parsing Debt Check Response for Market ::%s due to :: %s", businessUnit,excep));
                    }
                }
            }
            else 
            {
                LOG.error(String.format("Error in Retrieving Debt Check Response from Mulesoft server for Market :::%s ",businessUnit));
                return setDebtCheckResponse(serviceResponse.getBody());
            }
        }
        catch (final HttpServerErrorException.ServiceUnavailable clientEx) {
            LOG.error(String.format("Debt check request failed for customer %s due to bad Service Unavailability :: %s and Code %s",
                    customerData.getUid(), clientEx.getMessage(),clientEx.getStatusCode()));
            return setDebtCheckResponse(clientEx.getResponseBodyAsString());
        }
        catch (final HttpServerErrorException.BadGateway clientEx)
        {
            LOG.error(String.format("Debt check request failed due to bad Gateway  :: %s and Code %s", clientEx.getMessage(),
                    clientEx.getStatusCode()));
            return setDebtCheckResponse(clientEx.getResponseBodyAsString());
        }
        catch (final HttpServerErrorException.InternalServerError clientEx)
        {
            LOG.error(String.format("Debt check request failed  due to Internal Server Error  :: %s and Code %s",
                            clientEx.getMessage(), clientEx.getStatusCode()));
            return setDebtCheckResponse(clientEx.getResponseBodyAsString());
        }
        catch (final HttpServerErrorException.GatewayTimeout clientEx)
        {
            LOG.error(String.format("Debt check request failed  due to  Gateway Timeout  :: %s and Code %s",
                    clientEx.getMessage(), clientEx.getStatusCode()));
            return setDebtCheckResponse(clientEx.getResponseBodyAsString());

        }
        catch (final HttpServerErrorException.NotImplemented clientEx)
        {
            LOG.error(String.format("Debt check request failed  due to no implementation  :: %s and Code %s",
                    clientEx.getMessage(), clientEx.getStatusCode()));
            return setDebtCheckResponse(clientEx.getResponseBodyAsString());
        }
        catch (final HttpClientErrorException.BadRequest badRequest)
        {
            LOG.error(badRequest.getResponseBodyAsString());
            return setDebtCheckResponse(badRequest.getResponseBodyAsString());
        }
        catch (final HttpClientErrorException clientEx)
        {
            LOG.error(clientEx.getMessage());
            if(clientEx.getRawStatusCode() == 404){
                LOG.info(String.format("Debt Check api reply with 404 (No Record found) for document number ::%s and Market :: %s ",customerData.getDocumentNumber(),businessUnit));
                return setDebtCheckResponse(clientEx.getResponseBodyAsString());
            }
        }
        catch (final ResourceAccessException ioException)
        {
            LOG.error(String.format("Exception in making Debt check API Call  for Market ::: %s due to ::: %s", businessUnit,ioException));
            return setDebtCheckResponse(ioException.getMessage());
        }
        finally {
            HttpComponentsClientHttpRequestFactory requestFactory=(HttpComponentsClientHttpRequestFactory) restTemplate_new.getRequestFactory();
            LOG.info(String.format("Releasing current client connection for Debt Check Request for Market ::: %s",businessUnit));
            requestFactory.destroy();
        }
        return llaDebtCheckApiResponse;
    }

    boolean checkForDebtBalance(final List<BillingAccount> customerResponseList, final CartModel cart, final String siteId)
 	{
 		for (final BillingAccount billingAccount : customerResponseList)
 		{
 			for (final AccountBalance balance : billingAccount.getAccountBalance())
 			{
 				if (siteId.equalsIgnoreCase("PR"))
 				{
 					if (null != balance.getAmount() && null != balance.getBalanceType() && balance.getBalanceType().equalsIgnoreCase("DelinquentBalance")
 							&& null != balance.getAmount().getUnit() && balance.getAmount().getUnit().equals(cart.getCurrency().getIsocode())
 							&& Double.valueOf(balance.getAmount().getValue()) > 0)
 					{
 						LOG.info("Found Debt: " + balance.getAmount().getValue() + " for Account :" + billingAccount.getId());
 						cart.setDebtCheckFlag(true);
 						modelService.save(cart);
 						modelService.refresh(cart);
 						return true;
 					}
 				}
 				else   if (null != balance.getAmount() && balance.getAmount().getUnit().equals(cart.getCurrency().getIsocode()) && Integer.valueOf(balance.getAmount().getValue()) > 0) {
                    LOG.info("Found Debt: "+balance.getAmount().getValue() +" for Account :"+billingAccount.getId());
                    cart.setDebtCheckFlag(true);
                    modelService.save(cart);
                    modelService.refresh(cart);
                    return true;
                }
 			}
 		}
 		return false;
 	}
    
   private LLAApiResponse setDebtCheckResponse(String serviceResponse) {

       LLAApiResponse llaDebtCheckApiResponse = null;
       if (null != serviceResponse) {

               try {
                   JsonArray errorNode = (JsonArray) new JsonParser().parse(serviceResponse).getAsJsonObject().get("errors");
                   JsonElement jsonElementIterator = errorNode.get(0);
                   String errorCode = jsonElementIterator.getAsJsonObject().get("code").getAsString();
                   String description = jsonElementIterator.getAsJsonObject().get("description").getAsString();
                   String message = jsonElementIterator.getAsJsonObject().get("message").getAsString();
                   if (getBusinessUnit().equalsIgnoreCase("PR")) {
                       llaDebtCheckApiResponse = new LLAApiResponse(errorCode, message, description, false, Strings.EMPTY, Collections.emptyList());
                   } else {
                       llaDebtCheckApiResponse = new LLAApiResponse(errorCode, message, description, true, Strings.EMPTY, Collections.emptyList());
                   }
               }

               catch (JsonSyntaxException ex)
               {
                   String errorCode = MALFORMED_JSON_CODE;
                   String description = ex.getMessage();
                   String message = serviceResponse;
                   if (getBusinessUnit().equalsIgnoreCase("PR"))
                   {
                       llaDebtCheckApiResponse = new LLAApiResponse(errorCode, message, description, false, Strings.EMPTY, Collections.emptyList());
                   }
                   else
                   {
                       llaDebtCheckApiResponse = new LLAApiResponse(errorCode, message, description, true, Strings.EMPTY, Collections.emptyList());
                   }
               }

           }
       return llaDebtCheckApiResponse;
       }


    
    
}
