package com.lla.mulesoft.integration.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.lla.core.model.*;
import com.lla.mulesoft.integration.constants.LlamulesoftintegrationConstants;
import com.lla.mulesoft.integration.response.LLAApiResponse;
import com.lla.mulesoft.integration.service.LLAMulesoftServiceabilityService;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import com.lla.telcotmfwebservices.v2.dto.*;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import joptsimple.internal.Strings;
import net.minidev.json.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.apache.log4j.Logger;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public class LLAMulesoftServiceabilityServiceImpl extends LLAAbstractService implements LLAMulesoftServiceabilityService
{
	private static final String CR = "CR";
	private static final Logger LOG = Logger.getLogger(LLAMulesoftServiceabilityServiceImpl.class);
	private static final String INTERNET="internet";
	private static final String TV="tv";
	private static final String PHONE="phone";
	private static final String VOICE="voice";
	private static final String ACTIVE="ACTIVE";
	private static final String PRIORITY ="priority";
	private static final int ONE =1;
	private static final String THREE ="3";
	private static final String MALFORMED_JSON_CODE="422";



	@Autowired
	private CartService cartService;
	@Autowired
	private CMSSiteService cmsSiteService;
	@Autowired
	private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

	/**
	 * Check Location is Servicable for Latitude and Longitude
	 * @param longitude
	 * @param latitude
	 * @return
	 * @throws Exception
	 */
	@Override
	public boolean checkServiceability(final Double latitude, final Double longitude) throws Exception {
		final HttpHeaders standardHeaders = setupCallRequestHeader(CR);
		 final HttpEntity<String> requestEntity = new HttpEntity<String>(standardHeaders);

		final String parameter = new StringBuffer(
				 configurationService.getConfiguration()
						 .getString(LlamulesoftintegrationConstants.CABLETICA_SERVICEABILITY_PARAMETER))
						 .append(fetchLatitudeLongitude(latitude, longitude)).toString();


		 final URI serviceability_get_url = URI
				 .create(apiUrlConstructor(getAPIServerForCallBack(), getBusinessUnit(),
						 LlamulesoftintegrationConstants.CABLETICA_SERVICEABILITY,
						 parameter));
		 ResponseEntity<String> serviceResponse = null;

		LLAApiResponse response = callServicabilityAPI(latitude, longitude, StringUtils.EMPTY, requestEntity, serviceability_get_url, serviceResponse);
		return Boolean.valueOf(response.getIsServicable());
    }

	/**
	 * Call Mulesoft Servicability API
	 * @param latitude
	 * @param longitude
	 * @param houseNo
	 * @param requestEntity
	 * @param serviceability_get_url
	 * @param serviceResponse
	 * @return
	 * @throws Exception
	 */
	private LLAApiResponse callServicabilityAPI(Double latitude, Double longitude, String houseNo, HttpEntity<String> requestEntity, URI serviceability_get_url, ResponseEntity<String> serviceResponse)
			throws Exception {
		boolean returnFlag=false;
		LLAApiResponse servicabilityResponse = new LLAApiResponse();
		RestTemplate restTemplate_new=this.createRestTemplate(this.createRequestFactory(configurationService.getConfiguration().getString("max.pool.connection")));
		try
		{
			serviceResponse = restTemplate_new.exchange(serviceability_get_url, HttpMethod.GET, requestEntity, String.class);
			if (serviceResponse.getStatusCode() == HttpStatus.OK)
			{
				LOG.info(String.format("Serviceability Response Retrieved %s ::: ", serviceResponse.getBody()));
				final ObjectMapper mapper = new ObjectMapper();
				mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
				if (!StringUtils.isEmpty(serviceResponse.getBody()))
				{
					try
					{
						final List<CheckServiceQualification> serviceabilityResponseList = Arrays
								.asList(mapper.readValue(serviceResponse.getBody(), CheckServiceQualification[].class));

						if (!serviceabilityResponseList.isEmpty()
								&& !serviceabilityResponseList.get(0).getServiceQualificationItem().isEmpty())
						{
							LOG.info(String.format("Location is serviceable for Latitude :: %s and Longitude : %s",latitude,longitude));
							persistServiceAbilityResponseData(latitude,longitude,StringUtils.EMPTY,serviceabilityResponseList.get(0));
							if(Objects.nonNull(cartService.getSessionCart())&& llaMulesoftIntegrationUtil.isPuertoricoOrder(cartService.getSessionCart().getSite()))
							{
								returnFlag = checkIfServiceIsActive(serviceabilityResponseList.get(0),latitude,longitude);
								servicabilityResponse = new LLAApiResponse(String.valueOf(returnFlag),String.valueOf(serviceResponse.getStatusCode().value()));
							}
							if(Objects.nonNull(cartService.getSessionCart()) && llaMulesoftIntegrationUtil.isCableticaOrder(cartService.getSessionCart().getSite())){
								returnFlag= checkPriorityValue(serviceabilityResponseList.get(0), latitude, longitude);
								servicabilityResponse = new LLAApiResponse(String.valueOf(returnFlag));
							}
						}
					}
					catch (final JsonProcessingException e)
					{
						LOG.error("Error Parsing View Plan Response", e);
						servicabilityResponse = setServicabilityResponse(e.toString());
					}
				}
			}
			else
			{
				LOG.error("Error in Retrieving Serviceability Response from Mulesoft server !!! ");
				servicabilityResponse = new LLAApiResponse(String.valueOf(false));
			}
		}
		catch (final HttpServerErrorException.ServiceUnavailable clientEx)
		{
			LOG.error(String.format("Serviceablity request failed due to bad Service Unavailability  :: %s and Code %s", clientEx.getMessage(),clientEx.getStatusCode()));
			servicabilityResponse = setServicabilityResponse(clientEx.getResponseBodyAsString());
			//returnFlag=false;
		}
		catch (final HttpServerErrorException.BadGateway clientEx)
		{
			LOG.error(String.format("Serviceability request failed due to bad Gateway  :: %s and Code %s", clientEx.getMessage(),
					clientEx.getStatusCode()));
			servicabilityResponse = setServicabilityResponse(clientEx.getResponseBodyAsString());
			//returnFlag=false;
		}
		catch (final HttpServerErrorException.InternalServerError clientEx)
		{
			LOG.error(
					String.format("Serviceability request failed  due to Internal Server Error  :: %s and Code %s",
							clientEx.getMessage(), clientEx.getStatusCode()));
			servicabilityResponse = setServicabilityResponse(clientEx.getResponseBodyAsString());
			//returnFlag=false;
		}
		catch (final HttpServerErrorException.NotImplemented clientEx)
		{
			LOG.error(String.format("Create call back request failed  due to no implementation  :: %s and Code %s",
					clientEx.getMessage(), clientEx.getStatusCode()));
			servicabilityResponse = setServicabilityResponse(clientEx.getResponseBodyAsString());
			//returnFlag=false;
		}
		catch (final HttpServerErrorException.GatewayTimeout clientEx)
		{
			LOG.error(String.format("Serviceability request failed  due to  Gateway Timeout  :: %s and Code %s",
					clientEx.getMessage(), clientEx.getStatusCode()));
			servicabilityResponse = setServicabilityResponse(clientEx.getResponseBodyAsString());
			//returnFlag=false;
		}
		catch (final HttpClientErrorException.BadRequest badRequest)
		{
			LOG.error(badRequest.getResponseBodyAsString());
			servicabilityResponse = setServicabilityResponse(badRequest.getResponseBodyAsString());
			//returnFlag=false;
		}
		catch (final HttpClientErrorException clientEx)
		{
			LOG.error(clientEx.getMessage());
			servicabilityResponse = setServicabilityResponse(clientEx.getResponseBodyAsString());
			//returnFlag=false;
		}
		catch (final ResourceAccessException ioException)
		{
			LOG.error("Exception in making Api Call", ioException);
			servicabilityResponse = setServicabilityResponse(ioException.toString());
			//returnFlag=false;
		}
		finally {
			HttpComponentsClientHttpRequestFactory requestFactory=(HttpComponentsClientHttpRequestFactory) restTemplate_new.getRequestFactory();
			LOG.info("Releasing current client connection for Servicable Request");
			requestFactory.destroy();
		}
		return servicabilityResponse;
	}

	@Override
	public LLAApiResponse checkServiceability(String houseNo) throws Exception {

		final HttpHeaders standardHeaders = setupCallRequestHeader("PR");
		final HttpEntity<String> requestEntity = new HttpEntity<String>(standardHeaders);
		final String parameter = new StringBuffer(
				configurationService.getConfiguration()
						.getString(LlamulesoftintegrationConstants.PUERTORICO_SERVICEABILITY_PARAMETER))
				.append(fetchHouseNo(houseNo)).toString();

		final URI serviceability_get_url = URI
				.create(apiUrlConstructor(getAPIServerForCallBack(), getBusinessUnit(),
						LlamulesoftintegrationConstants.PUERTORICO_SERVICEABILITY,
						parameter));
		ResponseEntity<String> serviceResponse = null;
		return callServicabilityAPI(Double.valueOf(0),Double.valueOf(0),houseNo,requestEntity, serviceability_get_url,serviceResponse);
	}

	/**
	 * Save ServiceAbility Response
	 * @param latitude
	 * @param longitude
	 * @param checkServiceQualification
	 * @param houseNo
	 */
	private void persistServiceAbilityResponseData(Double latitude,Double longitude, String houseNo, CheckServiceQualification checkServiceQualification) {
		if(null!=checkServiceQualification && (configurationService.getConfiguration().getBoolean("persist.serviceability.result") ||configurationService.getConfiguration().getBoolean("puertorico.persist.serviceability.result") ))
		{
			CartModel cartModel=cartService.getSessionCart();
			List<ServiceQualificationResultModel> prevServiceQualResultList=cartModel.getServiceQualResultRequest();
			List<ServiceQualificationResultModel> updatedServiceQualResultList=new ArrayList<>(prevServiceQualResultList);
			ServiceQualificationResultModel serviceQualResult=getModelService().create(ServiceQualificationResultModel.class);
			if(null!=latitude && null!=longitude) {
				serviceQualResult.setLatitude(latitude);
				serviceQualResult.setLongitude(longitude);
			}
			if(StringUtils.isNotEmpty(houseNo))
			{
				serviceQualResult.setHouseNo(houseNo);
			}
			serviceQualResult.setSite(cartService.getSessionCart().getSite());
			serviceQualResult.setQueryServiceQualificationDate(checkServiceQualification.getCheckServiceQualificationDate());
			List<ServiceQualificationItemModel> serviceList = createServiceQualificationItem(checkServiceQualification);
			serviceQualResult.setServiceQualificationItems(serviceList);
			getModelService().save(serviceQualResult);
			updatedServiceQualResultList.add(prevServiceQualResultList.size(),serviceQualResult);
			cartModel.setServiceQualResultRequest(updatedServiceQualResultList);
			getModelService().save(cartModel);
			getModelService().refresh(cartModel);
		}
	}

	/**
	 * Create Service Qualification Items
	 * @param checkServiceQualification
	 * @return
	 */
	private List<ServiceQualificationItemModel> createServiceQualificationItem(CheckServiceQualification checkServiceQualification) {
		List<CheckServiceQualificationItem> serviceQualificationItemList= checkServiceQualification.getServiceQualificationItem();
		List<ServiceQualificationItemModel> serviceList=new ArrayList<>();
		for(CheckServiceQualificationItem item:serviceQualificationItemList){
			ServiceQualificationItemModel serviceItem=getModelService().create(ServiceQualificationItemModel.class);
			serviceItem.setId(item.getId());
			serviceItem.setExpectedServiceAvailabilityDate(item.getExpectedServiceAvailabilityDate());
			ServiceModel serviceModel =getModelService().create(ServiceModel.class);
			serviceModel.setServiceType(item.getService().getServiceType());
			serviceModel.setState(item.getService().getState().name());
			serviceModel.setFeature(populateServiceFeatures(item.getService()));
			serviceModel.setServiceCharacteristics( populateServiceCharacteristics(item.getService()));
			getModelService().save(serviceModel);
			serviceItem.setService(serviceModel);
			getModelService().save(serviceItem);
			serviceList.add(serviceItem);
		}
		return serviceList;
	}

	private List<ServiceCharacteristicsModel> populateServiceCharacteristics(ServiceRefOrValue item) {
		List<Characteristic> characteristicList= item.getServiceCharacteristic();
		List<ServiceCharacteristicsModel> serviceCharacteristicsModelList=new ArrayList<>();
		for(Characteristic characteristic:characteristicList){
			ServiceCharacteristicsModel characteristicsModel=getModelService().create(ServiceCharacteristicsModel.class);
			characteristicsModel.setName(characteristic.getName());

			characteristicsModel.setName(characteristic.getName());
			characteristicsModel.setValue(characteristic.getValue());
				getModelService().save(characteristicsModel);
			 serviceCharacteristicsModelList.add(characteristicsModel);
			}

		return serviceCharacteristicsModelList;
	}

	private List<ServiceFeatureModel> populateServiceFeatures(ServiceRefOrValue item) {

		List<ServiceFeatureModel> serviceFeatureModelList=new ArrayList<>();
		for(Feature feature:item.getFeature()){
		   ServiceFeatureModel featureModel=getModelService().create(ServiceFeatureModel.class);
		   featureModel.setName(feature.getName());
		   List<FeatureCharacteristicsModel> featureCharacteristicsModelList=new ArrayList<>();
		   for(Characteristic characteristic:feature.getFeatureCharacteristic()){
			   FeatureCharacteristicsModel model= getModelService().create(FeatureCharacteristicsModel.class);
			   model.setName(characteristic.getName());
			   model.setValue(characteristic.getValue());
			   getModelService().save(model);
			   featureCharacteristicsModelList.add(model);
		   }
		   featureModel.setFeatureCharacteristics(featureCharacteristicsModelList);
		   getModelService().save(featureModel);
		   serviceFeatureModelList.add(featureModel);
	   }
		return serviceFeatureModelList;
	}

	private HttpHeaders setupCallRequestHeader(final String siteId)
	 {
		 headers.set("client_id", configurationService.getConfiguration().getString("cabletica.mulesoft.clicktocall.api.clientid"));
		 headers.set("client_secret",
				 configurationService.getConfiguration().getString("cabletica.mulesoft.clicktocall.api.clientsecret"));
		 headers.set("channelId",
				 configurationService.getConfiguration().getString("cabletica.mulesoft.clicktocall.api.channelId"));
		 headers.set("Correlation-ID",
				 configurationService.getConfiguration().getString("cabletica.api.clicktocall.correlationId"));
		 return headers;
	 }

	 private String fetchLatitudeLongitude(final Double lat, final Double longi)
	 {
		 final JSONObject location = new JSONObject();
		 location.put("latitude", Double.toString(lat));
		 location.put("longitude", Double.toString(longi));

		 try
		 {
			 return URLEncoder.encode(location.toString(), StandardCharsets.UTF_8.toString());
		 }
		 catch (final UnsupportedEncodingException ex)
		 {
			 throw new RuntimeException(ex.getCause());
		 }
	 }

	 /** check priority value for three
	  * @param checkServiceQualification
	  * @return boolean
	  **/
	 private boolean checkPriorityValue(CheckServiceQualification checkServiceQualification, final Double latitude, final Double longitude ){

		 LOG.info(String.format("Inside check priority function for latitude ::: %s and longitude :: %s ::: ", latitude, longitude));
	 	List<CheckServiceQualificationItem> serviceQualificationItems=checkServiceQualification.getServiceQualificationItem();
		   boolean priorityCheckFlag=false;
		    for (CheckServiceQualificationItem item:serviceQualificationItems) {
			 if(Objects.nonNull(item.getService()) &&  CollectionUtils.isNotEmpty(item.getService().getFeature())){
			 	List<Feature>featureList=item.getService().getFeature();
				 for (Feature feature:featureList) {
					 if(CollectionUtils.isNotEmpty(feature.getFeatureCharacteristic())){
					 	 Set<Characteristic> characteristics=feature.getFeatureCharacteristic()
							     .parallelStream().filter(p->p.getName().equalsIgnoreCase(PRIORITY)).collect(Collectors.toSet());
                         if(CollectionUtils.isNotEmpty(characteristics)&& characteristics.size()==ONE && characteristics.toString().contains(THREE)){
	                         LOG.info(String.format("Location is non-serviceable as priority value is %s for latitude ::: %s and longitude :: %s :::",THREE, latitude, longitude));
	                         priorityCheckFlag=false;
                         }
                         else {
							 LOG.info(String.format("Location is serviceable for latitude ::: %s and longitude :: %s , as area has priority other than 3 too", latitude, longitude));
	                         priorityCheckFlag=true;
                         }
					 }
				 }
			 }
		 }
		 return priorityCheckFlag;
	 }

	/**
	 *  Check if Any Service is already active in Area or not
	 * @param checkServiceQualification
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	private boolean checkIfServiceIsActive(CheckServiceQualification checkServiceQualification, Double latitude, Double longitude)
	{
		CartModel cart = cartService.getSessionCart();
		boolean isInternetActive = false;
		boolean isTelivisionActive = false;
		boolean isPhoneActive= false;
		if(Objects.nonNull(cart))
		{
			List<AbstractOrderEntryModel> cartEntries = cart.getEntries();
			if(CollectionUtils.isNotEmpty(cartEntries))
			{
				for( AbstractOrderEntryModel entry: cart.getEntries())
				{
					CategoryModel categories= ((List<CategoryModel>) entry.getProduct().getSupercategories()).get(0);

					switch (StringUtils.lowerCase(categories.getCode())){
						case INTERNET:
							isInternetActive = checkIfCategoryIsServicable(checkServiceQualification,INTERNET);
							break;
						case TV:
							isTelivisionActive = checkIfCategoryIsServicable(checkServiceQualification,TV);
							break;
						case PHONE:
							isPhoneActive = checkIfCategoryIsServicable(checkServiceQualification,VOICE);
							break;
					}
				}
			}
			if (isInternetActive || isTelivisionActive || isPhoneActive)
			{
				cart.setIsRequiredServiceActive(true);
				LOG.info(String.format("Location is non-serviceable as other services are active in area for latitude ::: %s and longitude :: %s :::", latitude, longitude));
				getModelService().save(cart);
			}
			else {
				cart.setIsRequiredServiceActive(false);
				LOG.info(String.format("Location is serviceable as other services are not active in area for latitude ::: %s and longitude :: %s :::", latitude, longitude));
				getModelService().save(cart);
			}
		}
		return cart.getIsRequiredServiceActive();
	}

	/**
	 * Check For Category Service Active
	 * @param checkServiceQualification
	 * @param code
	 * @return
	 */
	private boolean checkIfCategoryIsServicable(CheckServiceQualification checkServiceQualification, String code)
	{
		boolean isCategoryActive = false;
		List<Feature> features = checkServiceQualification.getServiceQualificationItem().get(0).getService().getFeature();
		for (Feature feature : features)
		{
			if(feature.getName().equalsIgnoreCase(code))
			{

				isCategoryActive= feature.getFeatureCharacteristic().stream().anyMatch(characterstic -> ACTIVE.equalsIgnoreCase(characterstic.getValue()));
			}
		}
		return isCategoryActive;
	}

	private String fetchHouseNo(String houseNo)
	{
		final JSONObject houseNumber = new JSONObject();
		houseNumber.put("id",houseNo);
		try {
			return URLEncoder.encode(houseNumber.toString(), StandardCharsets.UTF_8.toString());
		}
		catch (final UnsupportedEncodingException ex)
		{
			throw new RuntimeException(ex.getCause());
		}
	}

	/**
	 *  Generate Serviciability Response
	 * @param serviceResponse
	 * @return
	 */
	private LLAApiResponse setServicabilityResponse(String serviceResponse) {

		LLAApiResponse llaServicabilityAPIResponse = null;
		if (null != serviceResponse)
		{
			try

			{
				JsonArray errorNode = (JsonArray) new JsonParser().parse(serviceResponse).getAsJsonObject().get("errors");
				JsonElement jsonElementIterator = errorNode.get(0);
				String errorCode = jsonElementIterator.getAsJsonObject().get("code").getAsString();
				String description = jsonElementIterator.getAsJsonObject().get("description").getAsString();
				String message = jsonElementIterator.getAsJsonObject().get("message").getAsString();
				String isServicable = String.valueOf(false);
				llaServicabilityAPIResponse = new LLAApiResponse(errorCode, message, description, false, isServicable, Collections.EMPTY_LIST);
			}
			catch (JsonSyntaxException ex)
			{
				String errorCode = MALFORMED_JSON_CODE ;
				String description = ex.getMessage();
				String message = serviceResponse;
				String isServicable = String.valueOf(false);
				llaServicabilityAPIResponse = new LLAApiResponse(errorCode, message, description, false, isServicable, Collections.EMPTY_LIST);

			}
		}
		return llaServicabilityAPIResponse;

	}
}
