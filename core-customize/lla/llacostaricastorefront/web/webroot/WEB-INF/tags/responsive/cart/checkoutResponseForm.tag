<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<div class="hide">
	<div id="callResponseFormId" class="callResponseForm">
	    <p class="frmHeading"><spring:theme code="checkout.thankYouForOrder.text" /></p>
	    <p class="frmMessage"><spring:theme code="checkout.agent.contact.you.text"/></p>
	    <button type="button" class="callResponseBtn btn btn-primary btn-block">
	        <spring:theme code="checkout.botton.understood.text"/>
	    </button>
	    <input type="hidden" id="redirectUrl" name="redirectUrl" value="${cartRedirectURL}">
	 </div> 
</div>