<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>

<c:set var="entryGroupNumber" value="1"/>
<spring:url value="/cart/deleteCart" var="bpoDeleteAction" htmlEscape="false">
    <spring:param name="selectedCategory" value="${activeCategoryName}"/>
</spring:url>

 <div class="container">
                <c:forEach items="${allProductDatas.entrySet()}" var="entry">
                    <c:forEach items="${categoriesList}" var="category">
                        <c:if test="${entry.key eq category}">
                            <div class="tab-content-wrapper container ${category eq activeCategoryName ? '':'display-none'}" id="${entry.key}">
                                <c:if test="${category eq 'tvcabletica'}">
                                    <p class="televisonTitle"><spring:theme code="text.category.telivision.headText"/></p>
                                    <p class="televisionSelect"><spring:theme code="text.category.telivision.selectBox"/>
                                        <select name="code" id="plp-region-selector" class="form-control">
                                            <option disabled selected value=""}><spring:theme code="text.category.telivision.selectBox.defaultText"/></option>
                                            <c:forEach items="${regions}" var="region">
                                                <option value="${region.isocode}"}>${region.isocode}</option>
                                            </c:forEach>
                                        </select>
                                    </p>
                                </c:if>
                                
                                <div class="cta-box-container">
                               
                                    <c:forEach items="${entry.value}" var="productData" varStatus="productIndex">
                                        <c:choose>
                                            <c:when test="${category eq 'tvcabletica'}">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ctaDetails">
                                            </c:when>
                                            <c:otherwise>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            </c:otherwise>
                                        </c:choose>
                                            <div class="cta-box <c:if test="${productInCart == productData.code}">selected</c:if>">
                                                <div class="ctabox__selected--blur"></div>
                                                <p class="ctaTtitle">${productData.name}</p>
                                                <c:if test="${category ne 'internetcabletica' && category ne 'tvcabletica'}">
                                                <div class="specImage">
                                                            <cms:pageSlot position="PlanesBanner6" var="feature">
                                                                <cms:component component="${feature}" element="div" class="no-space yComponentWrapper"/>
                                                            </cms:pageSlot>
                                                         </div>
                                                         </c:if>
                                                <div class="content">
                                                    <c:if test="${category eq 'tvcabletica'}">
                                                        <div class="bestProduct">
                                                            <c:if test="${productData.name eq 'PACK DIAMANTE'}">
                                                                <p class="recommendedProduct"><spring:theme code="text.product.recommended"/></p>
                                                            </c:if>
                                                        </div>
                                                    </c:if>
                                                    <div class="cta-specs">
                                                        <c:if test="${category ne 'tvcabletica'}">
                                                            <c:forEach var="specCharValue" items="${productData.productSpecCharValueUses}">
                                                                <c:forEach items="${specCharValue.productSpecCharacteristicValues}" var="spcchar">
                                                                    <p class="specs ${spcchar.productSpecCharacteristic} ${(spcchar.productSpecCharacteristic eq 'data_volume' && category eq 'internetcabletica') ? ' iconWifiOrange':''}"">
                                                                        <c:choose>
                                                                            <c:when test="${category eq 'internetcabletica' && spcchar.productSpecCharacteristic eq 'data_volume'}">
                                                                                <span class="spec-text">${spcchar.ctaSpecDescription}</span>
                                                                            </c:when>
                                                                            <c:when test="${category ne 'internetcabletica' && spcchar.productSpecCharacteristic eq 'data_volume'}">
                                                                                <span class="spec-text">${spcchar.ctaSpecDescription}</span>
                                                                                <span class="colorBlue">${spcchar.value}</span><span> ${spcchar.unitOfMeasurment}</span>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <span class="colorBlue">${spcchar.value}</span> <span>${spcchar.unitOfMeasurment}</span>
                                                                                <span class="spec-text">${spcchar.ctaSpecDescription}</span>
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </p>
                                                                </c:forEach>
                                                            </c:forEach>
                                                        </c:if>
                                                        <c:if test="${category eq 'tvcabletica'}">
                                                            <div class="list">
                                                                <c:forEach var="specCharValue" items="${productData.productSpecCharValueUses}">
                                                                    <c:forEach items="${specCharValue.productSpecCharacteristicValues}" var="spcchar">
                                                                    <p class="listItem ${spcchar.productSpecCharacteristic}">
                                                                        <span class="colorBlue">${spcchar.value}</span> <span>${spcchar.unitOfMeasurment}</span>
                                                                        <span class="spec-text">${spcchar.ctaSpecDescription}</span>
                                                                    </p>
                                                                    </c:forEach>
                                                                </c:forEach>
                                                            </div>
                                                        </c:if>
                                                    </div>
                                                    <c:if test="${category eq 'tvcabletica' && productData.code eq 'tv_PACK_DIAMANTE'}">
                                                        <div class="specImage">
                                                            <cms:pageSlot position="PlanesBanner5" var="feature">
                                                                <cms:component component="${feature}" element="div" class="no-space yComponentWrapper"/>
                                                            </cms:pageSlot>
                                                         </div>
                                                    </c:if>
                                                    <c:if test="${category ne 'internetcabletica' && category ne 'tvcabletica'}">
                                                        <hr class="ctaHr">
                                                    </c:if>
                                                    <div class="divPricenButtons">
                                                        <input type="hidden" class="pageProducts"
                                                            <c:if test="${productData.name ne null}"> data-attr-name="${productData.name}" </c:if>
                                                            <c:if test="${productData.code ne null}"> data-attr-id="${productData.code}" </c:if>
                                                            <c:if test="${category ne 'tvcabletica' && productData.monthlyPrice.value ne null}"> data-attr-price='<fmt:formatNumber value="${productData.monthlyPrice.value}" pattern="#,##0" />' </c:if>
                                                            <c:if test="${category eq 'tvcabletica'}"> data-attr-price="0" </c:if>
                                                            <c:if test="${category ne null}"> data-attr-category="${category}" </c:if>
                                                            <c:if test="${productData.categories[0].parentCategoryName ne null}"> data-attr-primarycategory="${productData.categories[0].parentCategoryName}" </c:if>
                                                            <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
                                                        />                                                        	
                                                        <div class="priceInfo">
                                                            <c:choose>
                                                                <c:when test="${category eq 'tvcabletica'}">
                                                                    <p class="ctaPrice tvPrice" id="${productData.code}"><span class="priceData"></span><span class="ctaIVA display-none"><spring:theme code="text.plp.price"/></span> <span class="ctaPeriod display-none"> <spring:theme code="text.price.monthly"/></span></p>
                                                                </c:when>
                                                                <c:otherwise>
                                                                <fmt:setLocale value="en_US" scope="session" />
                                                                    <p class="ctaPrice">${currentCurrency.symbol}<fmt:formatNumber value="${productData.monthlyPrice.value}" pattern="#,##0" /> <span><spring:theme code="text.plp.price"/></span></p>
                                                                    <p class="ctaPeriod"><spring:theme code="text.price.monthly"/></p>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </div>
										                <c:choose>
                                                            <c:when test="${not empty productInCart && productData.code eq productInCart}">
                                                                <c:if test="${not empty entryGroupNumber}">
                                                                    <form:form id="deleteBpoForm${entryGroupNumber}" action="${bpoDeleteAction}" method="post" modelAttribute="deleteBpoForm'${entryGroupNumber}" class="ctabox-form ctabox-removefromcart-form deleteForm">
                                                                        <input type="hidden" name="groupNumber" value="${entryGroupNumber}" />
                                                                        <spring:theme code="text.iconCartRemove" var="iconCartRemove" text="REMOVE" />
                                                                        <div class="ctabox-removeFromCart-section">
                                                                            <button type="submit" class="btn cta-submit-btn ctabox-removeFromCart-btn submitRemoveBundle" id="${entryGroupNumber}">
                                                                                <spring:theme code="ctaBox.removeFromCart.button.text" />
                                                                            </button>
                                                                        </div>
                                                                    </form:form>
                                                                </c:if>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <form:form id="addonAddtoCart" action="${contextPath}/addtocart" method="get" class="ctabox-form ctabox-addtocart-form ctaBox-clickToCallBtn">
                                                                    <input type="hidden" name="productCodePost" id="productCodePost" value="${productData.code}" />
                                                                    <input type="hidden" name="qty" value="1">
                                                                    <input type="hidden" name="initialQuantity" value="1">
                                                                    <input type="hidden" name="processType" value="DEVICE_ONLY">
                                                                    <div class="ctabox-addToCart-section">
                                                                        <button type="submit"
                                                                            class="btn cta-submit-btn ctabox-addToCart-btn ${category eq 'tvcabletica' ? 'ctaDisabledBtn':''}" disabled="disabled">
                                                                            <spring:theme code="ctaBox.addToCart.button.text" />
                                                                        </button>
                                                                    </div>
                                                                  
                                                                </form:form>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                             </div>
                        </c:if>
                    </c:forEach>
                </c:forEach>
            </div>
         </div>
<input type="hidden" id="currencySymbol" value="${currentCurrency.symbol}" />

<product:productInfoDialog />