<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
 
 <ul class="selection-state">
            <li class="active"><p class="level"><span class="process-state">1</span><span class="process-info"><spring:theme code="selection.level1.text"/></span></p></li>
            <li><p class="level"><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></p></li>
            <li><p class="level"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level3.text"/></span></p></li>
</ul>