<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>

<div class="hide">
    <div id="infoModalFormID" class="infoModalForm">
        <div class="infoFormLeft col-lg-6 col-sm-12">
            <p class="frmtitle"><spring:theme code="text.you.requested"/>:</p>
            <div class="selectedPlanDsc"></div>
            <div class="selectPlanPrice">
                <span></span>
            </div>
            <p class="selectPlanPriceText"><spring:theme code="text.free.installation" /></p>
            <div class="s-text4 text-center" id="aviso">
                <spring:theme code="text.information.text1" />&nbsp;<strong style="color: #ff6801;">
                <spring:theme code="text.new.service" /></strong><spring:theme code="text.information.text2" />
            </div>
            <div class="infoFooterNote">
                <spring:theme code="text.oprative.hrs.msg" /> <strong><spring:theme code="text.mon.to.sunday" /></strong> <spring:theme code="text.time" />
            </div>
        </div>
        <div id="infoFromContent" class="infoFormRight col-lg-6 col-sm-12">
            	<div class="infoFromRightTitle"> <spring:theme code="text.enter.your.data.quote" /> </div>

                <form name="ClickCallForm1" id="ClickCallForm1" action="${contextPath}/click/clickToCallForm" method="post" modelAttribute="clickCallForm">
                    <div class="form-group">
                        <label for="phoneNumber" class="control-label"> <spring:theme code="text.form.telephone.num" /> </label>
                        <input type="text" class="form-control" id="phoneNumber" name="phoneNumber" maxlength="8" placeholder="Ej: 4700-7777" value="" />
                    </div>
                    <div class="form-group">
                        <label for="identityNumber" class="control-label"><spring:theme code="text.identification.card" /> </label>
                        <input type="text" class="form-control" id="identityNumber" path="identityNumber" name="identityNumber" aria-describedby="cedulafield" placeholder="Ej: 101110111" autocomplete="off" value="" />
                    </div>
                    <div class="form-group">
                        <label for="email" class="control-label"> <spring:theme code="text.mail" /> </label>
                        <input type="email" class="form-control" id="email" path="email" name="email" placeholder="rod08@gmail.com" autocomplete="off" value="" />
                    </div>  
                    <div align="center" class="infoFormBtnDiv">
                       <div id="btnLlamada">
                           <button type="button" name="ClickCallForm" id="c2cButton"><spring:theme code="text.btn.request.call.now" /></button>                           
                           <input type="hidden" id="email_error" value="<spring:theme code='guest.checkout.invalid.email'/>">
                           <input type="hidden" id="cedulaPassport_error" value="<spring:theme code='guest.checkout.invalid.cedulaPassport.cabletica'/>">
                           <input type="hidden" id="phoneNumber_error" value="<spring:theme code='guest.checkout.invalid.phonenumber.cabletica'/>">
                   		   <input type="hidden" id="phoneNumber_regex_error" value="<spring:theme code='guest.checkout.regex.phonenumber.cabletica'/>">
                        </div>
                    </div>
           </form> 

           <div class="formOverlay" id="c2cOverlay" style="display: none">
                <common:loader />
           </div>
        </div>

        <div id="infoSuccessFromContent" class="infoSuccessFrom col-lg-6 col-sm-12">
	    	<div class="successIcon"></div>
	    	<div class="successMsg"><spring:theme code="text.successful.msg" /></div>
	    	<div class="successTxt"><spring:theme code="text.executive.contact.txt" /></div>
	    </div>

    </div>
</div>
