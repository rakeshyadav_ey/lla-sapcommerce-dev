<%@ attribute name="regions" required="true" type="java.util.List"%>
<%@ attribute name="country" required="false" type="java.lang.String"%>
<%@ attribute name="tabindex" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:choose>
	<c:when test="${country == 'US'}">
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="billTo_titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" tabindex="${tabindex + 1}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="billTo_firstName" inputCSS="text" mandatory="true" tabindex="${tabindex + 2}"/>
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="billTo_lastName" inputCSS="text" mandatory="true" tabindex="${tabindex + 3}"/>
		<formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="billTo_street1" inputCSS="text" mandatory="true" tabindex="${tabindex + 4}"/>
		<formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="billTo_street2" inputCSS="text" mandatory="false" tabindex="${tabindex + 5}"/>
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="billTo_city" inputCSS="text" mandatory="true" tabindex="${tabindex + 6}"/>
		<formElement:formSelectBox idKey="address.region" labelKey="address.state" path="billTo_state" mandatory="true" skipBlank="false" skipBlankMessageKey="address.selectState" items="${regions}" itemValue="isocodeShort" tabindex="${tabindex + 7}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.postcode" labelKey="address.postcode" path="billTo_postalCode" inputCSS="text" mandatory="true" tabindex="${tabindex + 8}"/>
		<formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="billTo_phoneNumber" inputCSS="text" mandatory="false" tabindex="${tabindex + 9}"/>
	</c:when>
	<c:when test="${country == 'CA'}">
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="billTo_titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" tabindex="${tabindex + 1}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="billTo_firstName" inputCSS="text" mandatory="true" tabindex="${tabindex + 2}"/>
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="billTo_lastName" inputCSS="text" mandatory="true" tabindex="${tabindex + 3}"/>
		<formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="billTo_street1" inputCSS="text" mandatory="true" tabindex="${tabindex + 4}"/>
		<formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="billTo_street2" inputCSS="text" mandatory="false" tabindex="${tabindex + 5}"/>
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="billTo_city" inputCSS="text" mandatory="true" tabindex="${tabindex + 6}"/>
		<formElement:formSelectBox idKey="address.region" labelKey="address.province" path="billTo_state" mandatory="true" skipBlank="false" skipBlankMessageKey="address.selectProvince" items="${regions}" itemValue="isocodeShort" tabindex="${tabindex + 7}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.postcode" labelKey="address.postcode" path="billTo_postalCode" inputCSS="text" mandatory="true" tabindex="${tabindex + 8}"/>
		<formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="billTo_phoneNumber" inputCSS="text" mandatory="false" tabindex="${tabindex + 9}"/>		
	</c:when>
	<c:when test="${country == 'CN'}">
		<formElement:formInputBox idKey="address.postcode" labelKey="address.postcode" path="billTo_postalCode" inputCSS="text" mandatory="true" tabindex="${tabindex + 2}"/>
		<formElement:formSelectBox idKey="address.region" labelKey="address.province" path="billTo_state" mandatory="true" skipBlank="false" skipBlankMessageKey="address.selectProvince" items="${regions}" itemValue="isocodeShort" tabindex="${tabindex + 3}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="billTo_city" inputCSS="text" mandatory="true" tabindex="${tabindex + 4}"/>
		<formElement:formInputBox idKey="address.line1" labelKey="address.district_and_street" path="billTo_street1" inputCSS="text" mandatory="true" tabindex="${tabindex + 5}"/>
		<formElement:formInputBox idKey="address.line2" labelKey="address.building_and_room" path="billTo_street2" inputCSS="text" mandatory="false" tabindex="${tabindex + 6}"/>
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="billTo_lastName" inputCSS="text" mandatory="true" tabindex="${tabindex + 7}"/>
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="billTo_firstName" inputCSS="text" mandatory="true" tabindex="${tabindex + 8}"/>
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="billTo_titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" tabindex="${tabindex + 1}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="billTo_phoneNumber" inputCSS="text" mandatory="false" tabindex="${tabindex + 9}"/>
	</c:when>
	<c:when test="${country == 'JP'}">
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="billTo_titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" tabindex="${tabindex + 1}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="billTo_lastName" inputCSS="text" mandatory="true" tabindex="${tabindex + 2}"/>
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="billTo_firstName" inputCSS="text" mandatory="true" tabindex="${tabindex + 3}"/>
		<formElement:formInputBox idKey="address.postcode" labelKey="address.postcodeJP" path="billTo_postalCode" inputCSS="text" mandatory="true" tabindex="${tabindex + 4}"/>
		<formElement:formSelectBox idKey="address.region" labelKey="address.prefecture" path="billTo_state" mandatory="true" skipBlank="false" skipBlankMessageKey="address.selectPrefecture" items="${regions}" itemValue="isocodeShort" tabindex="${tabindex + 5}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townJP" path="billTo_city" inputCSS="text" mandatory="true" tabindex="${tabindex + 6}"/>
		<formElement:formInputBox idKey="address.line2" labelKey="address.subarea" path="billTo_street2" inputCSS="text" mandatory="true" tabindex="${tabindex + 7}"/>
		<formElement:formInputBox idKey="address.line1" labelKey="address.furtherSubarea" path="billTo_street1" inputCSS="text" mandatory="true" tabindex="${tabindex + 8}"/>
		<formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="billTo_phoneNumber" inputCSS="text" mandatory="false" tabindex="${tabindex + 9}"/>	
	</c:when>
    <c:otherwise>
        <formElement:formSelectBoxDefaultEnabled idKey="address.province" labelKey="address.province" path="billTo_province" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.none" items="${provinceList}" selectedValue="${sopPaymentDetailsForm.billTo_province}" selectCSSClass="address_province form-control"/>
        <formElement:formSelectBoxDefaultEnabled idKey="address.canton" labelKey="address.canton" path="billTo_building" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.none" items="${building}" selectedValue="${sopPaymentDetailsForm.billTo_building}" selectCSSClass="address_canton form-control"/>
        <formElement:formSelectBoxDefaultEnabled idKey="address.district" labelKey="address.district" path="billTo_district" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.none" items="${district}" selectedValue="${sopPaymentDetailsForm.billTo_district}" selectCSSClass="address_district form-control"/>
        <formElement:formSelectBoxDefaultEnabled idKey="address.neighbourhood" labelKey="address.neighbourhood" path="billTo_neighbourhood" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.none" items="${neighbourhood}" selectedValue="${sopPaymentDetailsForm.billTo_neighbourhood}" selectCSSClass="address_neighbourhood form-control"/>
        <formElement:formInputBox idKey="address.landMark" labelKey="cabletica.address.landMark" placeholder="Al norte de la iglesia San Isidro Casa color papaya a mano izquierda" path="billTo_landMark" inputCSS="text" mandatory="true" tabindex="${tabindex + 4}" maxlength="250"/>
        <input type="hidden" id="address_province_value" value="${sopPaymentDetailsForm.billTo_province}" >
	</c:otherwise>
</c:choose>
<div style="display: none" id="silentOrderPostForm_error">
<input type="hidden" id="province_error" value="<spring:theme code="cabletica.address.province.invalid"/>" >
<input type="hidden" id="canton_error" value="<spring:theme code="cabletica.address.canton.invalid"/>" >
<input type="hidden" id="district_error" value="<spring:theme code="cabletica.address.district.invalid"/>" >
<input type="hidden" id="neighbourhood_error" value="<spring:theme code="cabletica.address.neighbourhood.invalid"/>" >
<input type="hidden" id="landMark_error" value="<spring:theme code="cabletica.address.landMark.invalid"/>" >
<input type="hidden" id="selectDefaultText" value="<spring:theme code="form.select.none"/>" >
<input type="hidden" id="gtmAddressFormError" value="<spring:theme code="gtm.address.form.errors.msg"/>" >
</div>

