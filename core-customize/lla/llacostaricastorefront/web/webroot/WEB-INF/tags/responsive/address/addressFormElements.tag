<%@ attribute name="regions" required="true" type="java.util.List"%>
<%@ attribute name="country" required="false" type="java.lang.String"%>
<%@ attribute name="tabIndex" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<c:choose>
	<c:when test="${country == 'US'}">
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="line1" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="form-control" mandatory="false"/>
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBox idKey="address.region" labelKey="address.state" path="regionIso" mandatory="true" skipBlank="false" skipBlankMessageKey="address.state" items="${regions}" itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}" selectedValue="${addressForm.regionIso}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.postcode" labelKey="address.zipcode" path="postcode" inputCSS="form-control" mandatory="true" />
        <formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control" mandatory="false" />
	</c:when>
	<c:when test="${country == 'CA'}">
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="line1" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="form-control" mandatory="false"/>
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBox idKey="address.region" labelKey="address.province" path="regionIso" mandatory="true" skipBlank="false" skipBlankMessageKey="address.state" items="${regions}" itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}" selectedValue="${addressForm.regionIso}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.postcode" labelKey="address.postalcode" path="postcode" inputCSS="form-control" mandatory="true" />
        <formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control" mandatory="false" />
	</c:when>
	<c:when test="${country == 'CN'}">
		<formElement:formInputBox idKey="address.postcode" labelKey="address.postalcode" path="postcode" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBox idKey="address.region" labelKey="address.province" path="regionIso" mandatory="true" skipBlank="false" skipBlankMessageKey="address.selectProvince" items="${regions}" itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}" selectedValue="${addressForm.regionIso}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line1" labelKey="address.street" path="line1" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line2" labelKey="address.building" path="line2" inputCSS="form-control" mandatory="false"/>
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/>
        <formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control" mandatory="false" />
	</c:when>
	<c:when test="${country == 'JP'}">
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line1" labelKey="address.furtherSubarea" path="line1" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line2" labelKey="address.subarea" path="line2" inputCSS="form-control" mandatory="true"/>
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townJP" path="townCity" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBox idKey="address.region" labelKey="address.prefecture" path="regionIso" mandatory="true" skipBlank="false" skipBlankMessageKey="address.selectPrefecture" items="${regions}" itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}" selectedValue="${addressForm.regionIso}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.postalcode" labelKey="address.postcode" path="postcode" inputCSS="form-control" mandatory="true" />
        <formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control" mandatory="false" />
	</c:when>
	<c:otherwise>
	    <formElement:formSelectBoxDefaultEnabled idKey="address.province" labelKey="address.province" path="province" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.none" items="${provinceList}"  selectCSSClass="address_province form-control"/>
	   <c:choose>
            <c:when test="${not empty buildings}"> 
                <formElement:formSelectBoxDefaultEnabled idKey="address.canton" labelKey="address.canton" path="building" mandatory="true" skipBlank="false"  skipBlankMessageKey="form.select.none" items="${buildings}" selectCSSClass="address_canton form-control"/>
            </c:when>
            <c:otherwise>
	    		<formElement:formSelectBoxDefaultEnabled idKey="address.canton" labelKey="address.canton" path="building" mandatory="true" skipBlank="false"  skipBlankMessageKey="form.select.none" items="${building}" selectCSSClass="address_canton form-control"/>
	    	</c:otherwise>
	   </c:choose>
	   <c:choose>
	     	<c:when test="${not empty districts}"> 
                <formElement:formSelectBoxDefaultEnabled idKey="address.district" labelKey="address.district" path="district" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.none" items="${districts}" selectCSSClass="address_district form-control"/>
            </c:when>
            <c:otherwise>
	    		<formElement:formSelectBoxDefaultEnabled idKey="address.district" labelKey="address.district" path="district" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.none" items="${district}" selectCSSClass="address_district form-control"/>
	    	</c:otherwise>
	    </c:choose>
	    <c:choose>
            <c:when test="${not empty neighbourhoods}">
                <formElement:formSelectBoxDefaultEnabled idKey="address.neighbourhood" labelKey="address.neighbourhood" path="neighbourhood" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.none" items="${neighbourhoods}" selectCSSClass="address_neighbourhood form-control"/>
            </c:when>
            <c:otherwise>
                <formElement:formSelectBoxDefaultEnabled idKey="address.neighbourhood" labelKey="address.neighbourhood" path="neighbourhood" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.none" items="${neighbourhood}" selectCSSClass="address_neighbourhood form-control"/>
            </c:otherwise>
        </c:choose>

<!-- 	    <div id="checkoutPageMap" class="checkoutPageMapStyle"></div> -->
	    <formElement:formInputBox idKey="address.landMark" labelKey="address.landMark" path="landMark" inputCSS="form-control" placeholder="Al norte de la iglesia San Isidro Casa color papaya a mano izquierda" maxlength="250"/>
	    <product:googleMap />
	</c:otherwise>
</c:choose>
<input type="hidden" id="checkoutLatValue" name="lat" value=${setLatitutde} />
<input type="hidden" id="checkoutLongValue" name="long" value=${setLongitude} />

<div style="display: none" id="address_validation_error">
<input type="hidden" id="province_error" value="<spring:theme code="cabletica.address.province.invalid"/>" >
<input type="hidden" id="canton_error" value="<spring:theme code="cabletica.address.canton.invalid"/>" >
<input type="hidden" id="district_error" value="<spring:theme code="cabletica.address.district.invalid"/>" >
<input type="hidden" id="neighbourhood_error" value="<spring:theme code="cabletica.address.neighbourhood.invalid"/>" >
<input type="hidden" id="landMark_error" value="<spring:theme code="cabletica.address.landMark.invalid"/>" >
<input type="hidden" id="selectDefaultText" value="<spring:theme code="form.select.none"/>" >
<input type="hidden" id="gtmAddressFormError" value="<spring:theme code="gtm.address.form.errors.msg"/>" >
</div>

