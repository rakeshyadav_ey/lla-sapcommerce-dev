<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>

<div class="hide">
   <div id="mapServiceabilityCheck">
        <input id="autoCompleteSearchInput" class="controls" type="text" placeholder="<spring:theme code='text.google.enter.location' />" />
        <div class="popUpMapHeading"><p><spring:theme code="text.google.map.tellUsYourLocation" /></p></div>
        <product:addressFormList/>
       <div class="popUpMapButtons">
            <input type="hidden" id="lat" name="lat" value="9.946" />
            <input type="hidden" id="long" name="long" value="-84.012" />

            <button class="btn btn-skip skipServiceBtn" ><spring:theme code="text.google.map.skipStep" /></button>
            <button class="btn btn-confirm confirmServiceBtn"><spring:theme code="text.headertext.conf" /></button>
      </div>
      <div id="mapOverlay" class="formOverlay" style="display: none">
        <common:loader />
      </div>
    </div> 
</div>
