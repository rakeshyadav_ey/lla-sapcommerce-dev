<%@ attribute name="tabIndex" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>

 <div class="">
     
      <div id="openMapServiceability"></div>

      <div class="popUpMapHeading"><spring:theme code="text.google.map.select.where.you.live" /></div>
      <form:form method="get" modelAttribute="addressForm">
      <div class="mapDropdownsRegion">
        <formElement:formSelectBoxDefaultEnabled idKey="address.province"
          labelKey="address.province" path="province" mandatory="true"
          skipBlank="false" skipBlankMessageKey="form.select.none"
          items="${provinceList}" selectCSSClass="address_province form-control" />
        <formElement:formSelectBoxDefaultEnabled idKey="address.canton"
          labelKey="address.canton" path="building" mandatory="true"
          skipBlank="false" skipBlankMessageKey="form.select.none"
          items="${building}" selectCSSClass="address_canton form-control" />
        <formElement:formSelectBoxDefaultEnabled idKey="address.district"
          labelKey="address.district" path="district" mandatory="true"
          skipBlank="false" skipBlankMessageKey="form.select.none"
          items="${district}" selectCSSClass="address_district form-control" />
         <formElement:formSelectBoxDefaultEnabled idKey="address.neighbourhood"
                   labelKey="address.neighbourhood" path="neighbourhood" mandatory="true"
                   skipBlank="false" skipBlankMessageKey="form.select.none"
                   items="${neighbourhood}" selectCSSClass="address_neighbourhood form-control" />
      </div>  
      
      <div class="mapTextBox">
      <formElement:formInputBox idKey="address.landMark"
        labelKey="address.landMark" path="landMark" inputCSS="form-control"
        placeholder="Al norte de la iglesia San Isidro Casa color papaya a mano izquierda"
        maxlength="250" />
      </div>
  
  <div style="display: none" id="address_validation_error">
  <input type="hidden" id="province_error" value="<spring:theme code="cabletica.address.province.invalid"/>" >
  <input type="hidden" id="canton_error" value="<spring:theme code="cabletica.address.canton.invalid"/>" >
  <input type="hidden" id="district_error" value="<spring:theme code="cabletica.address.district.invalid"/>" >
  <input type="hidden" id="neighbourhood_error" value="<spring:theme code="cabletica.address.neighbourhood.invalid"/>" >
  <input type="hidden" id="landMark_error" value="<spring:theme code="cabletica.address.landMark.invalid"/>" >
  <input type="hidden" id="selectDefaultText" value="<spring:theme code="form.select.none"/>" >
  <input type="hidden" id="gtmAddressFormError" value="<spring:theme code="gtm.address.form.errors.msg"/>" >
  </div>
</form:form>

<cart:checkoutResponseForm />
</div>                 