<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="container">
    <ul>
        <c:forEach items="${categoriesList}" var="category">
            <li class="${category eq activeCategoryName ? 'active':''} js-tab-head" data-content-id="${category}">
                <c:choose>
                    <c:when test="${category eq '3PInternetTvTelephone'}">
                        <div class="bundle-name"><spring:theme code="text.plp.tabs.category.tripplePlay"/></div>
                    </c:when>
                    <c:when test="${category eq '2PInternetTv'}">
                        <div class="bundle-name"><spring:theme code="text.plp.tabs.category.doublePlay"/></div>
                    </c:when>
                    <c:when test="${category eq 'internetcabletica'}">
                        <div class="bundle-name"><spring:theme code="text.plp.tabs.category.internet"/></div>
                    </c:when>
                    <c:otherwise>
                        <div class="bundle-name"><spring:theme code="text.plp.tabs.category.television"/></div>
                    </c:otherwise>
                </c:choose>
            </li>
        </c:forEach>
         <li class="js-tab-head redirect" data-content-id='telefonia'>
            <div class="bundle-name"><a href="${telephoneLink}"><spring:theme code="text.plp.tabs.category.telephone"/></a></div>
         </li>
    </ul>
</div>