<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<template:errorpage>
	
	<div class="error-content">
		<div class="error-message text-center">
			<h3>
				<spring:theme code="error.404.page.message" text="Sorry, we can not find the page you are looking for" />
			</h3>
			<h4 class="error-description">
				<spring:theme code="error.404.page.message1" text="You might be interested in below offerings. Please check for it" />
			</h4>
			<ul class="site-category">
				<li>
					<a href="/cableticastorefront/cabletica/es/residential">
						<img class="img-responsive" src = "/cableticastorefront/_ui/responsive/theme-costarica/images/bundle_icon.svg">
						<span><spring:theme code="error.404.page.triplePlay" text="Triple Play" /></span>
					</a>
				</li>
				<li>
					<a href="/cableticastorefront/cabletica/es/residential?selectedCategory=2PInternetTv">
						<img class="img-responsive" src = "/cableticastorefront/_ui/responsive/theme-costarica/images/bundle_icon.svg">
						<span><spring:theme code="error.404.page.doublePlay" text="Double Play" /></span>
					</a>
				</li>
				<li>
					<a href="/cableticastorefront/cabletica/es/residential?selectedCategory=internetcabletica">
						<img class="img-responsive" src = "/cableticastorefront/_ui/responsive/theme-costarica/images/bundle_icon.svg">
						<span><spring:theme code="error.404.page.internet" text="Internet" /></span>
					</a>
				</li>
				<li>
					<a href="/cableticastorefront/cabletica/es/residential?selectedCategory=tvcabletica">
						<img class="img-responsive" src = "/cableticastorefront/_ui/responsive/theme-costarica/images/bundle_icon.svg">
						<span><spring:theme code="error.404.page.television" text="Television" /></span>
					</a>					
				</li>
				<li>
					<a href="https://www.cabletica.com/telefonia/">
						<img class="img-responsive" src = "/cableticastorefront/_ui/responsive/theme-costarica/images/bundle_icon.svg">
						<span><spring:theme code="error.404.page.telefonia" text="Telefonia" /></span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<input type="hidden" id="gtmNotFoundPageError" value="<spring:theme code="gtm.pagenotfound.msg"/>" >
</template:errorpage>
