<%@ page trimDirectiveWhitespaces="true" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
			<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
				<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
					<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
						<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
							<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

								<spring:htmlEscape defaultHtmlEscape="true" />

								<template:page pageTitle="${pageTitle}">

									<c:if test="${empty cartData.rootGroups}">
										<div class="emptycart">
											<div class="cartheader">
												<div class="container">
													<p>
														<spring:theme code="cart.empty" />
													</p>
												</div>
											</div>
											<div class="container">
												<div class="cartbody">
													<div class="cart-left col-sm-6">
														<p>
															<spring:theme code="best.plans.packages" />
														</p>
														<div class="empty-btn-cart">
															<a href="../es/residential">
																<spring:theme code="start.your.purchase" />
															</a>
														</div>
													</div>
													<div class="col-sm-6 cart-item emptybox">
														<div class="contactus">
															<h4>
																<spring:theme code="cart.questions.text" />
															</h4>
															<div class="contactus-info">
																<ul>
																	<li class="call">
																		<p class="emptycart">
																			<spring:theme code="contactus.contactus" />
																			<span>
																				<spring:theme code="contactus.call" htmlEscape="false" />
																			</span>
																		</p>
																	</li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</c:if>

									<%--<div class="cart-top-bar">
										<div class="text-right">
											<spring:theme var="textHelpHtml" code="text.help" />
											<a href="" class="help js-cart-help"
												data-help="${fn:escapeXml(textHelpHtml)}">${textHelpHtml}
												<span class="glyphicon glyphicon-info-sign"></span>
											</a>
											<div class="help-popup-content-holder js-help-popup-content">
												<div class="help-popup-content">
													<strong>${fn:escapeXml(cartData.code)}</strong>
													<spring:theme var="cartHelpContentVar"
														code="basket.page.cartHelpContent" htmlEscape="false" />
													<c:set var="cartHelpContentVarSanitized"
														value="${ycommerce:sanitizeHTML(cartHelpContentVar)}" />
													<div>${cartHelpContentVarSanitized}</div>
												</div>
											</div>
										</div>
										</div>--%>

										<div class="container">

											<cart:cartValidation />
											<cart:cartPickupValidation />
											<c:forEach items="${cartData.entries}" var="entry">
												<c:if
													test="${entry.product.categories[0].code ne 'channelsaddoncabletica'}">
													<c:set var="category" value="${entry.product.categories[0].code}" />
												</c:if>
											</c:forEach>
											<c:if test="${not empty cartData.rootGroups}">
												<div class="process-selection-bar clearfix">
													<ul class="selection-state">
														<li class="previous col-xs-4">
															<p class="level"><a
																	href="./residential?selectedCategory=${category}"><span
																		class="process-state">1</span><span
																		class="process-info">
																		<spring:theme code="selection.level1.text" />
																	</span></a></p>
														</li>
														<li class="active col-xs-4">
															<p class="level"><span class="process-state">2</span><span
																	class="process-info">
																	<spring:theme code="selection.level2.text" />
																</span></p>
														</li>
														<li class="col-xs-4">
															<p class="level"><span class="process-state">3</span><span
																	class="process-info">
																	<spring:theme code="selection.level3.text" />
																</span></p>
														</li>
													</ul>
												</div>
												<div class="cart-header col-xs-12">
													<h1>
														<spring:theme code="text.cart" text="My Shopping Cart" />
													</h1>
													<c:forEach items="${cartData.entries}" var="entry">
														<c:if
															test="${entry.product.categories[0].code eq '3PInternetTvTelephone' || entry.product.categories[0].code eq '2PInternetTv'}">
															<p>
																<spring:theme code="text.cart.message.cabletica.3P2P"
																	text="Products added to you cart" />
															</p>
														</c:if>
														<c:if
															test="${entry.product.categories[0].code eq 'internetcabletica'}">
															<p>
																<spring:theme
																	code="text.cart.message.cabletica.Internet"
																	text="Products added to you cart" />
															</p>
														</c:if>
														<c:if
															test="${entry.product.categories[0].code eq 'tvcabletica'}">
															<p>
																<spring:theme code="text.cart.message.cabletica.TV"
																	text="Products added to you cart" />
															</p>
														</c:if>
													</c:forEach>
												</div>

											</c:if>

											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-6">
													<cms:pageSlot position="TopContent" var="feature">
														<cms:component component="${feature}" element="div"
															class="yComponentWrapper" />
													</cms:pageSlot>
												</div>

												<c:if test="${not empty cartData.rootGroups}">
													<cms:pageSlot position="CenterLeftContentSlot" var="feature">
														<cms:component component="${feature}" element="div"
															class="yComponentWrapper" />
													</cms:pageSlot>
												</c:if>

												<div class="col-xs-12 col-sm-12 col-md-6">
													<c:if test="${not empty cartData.rootGroups}">
														<div class="cart-right">
															<cms:pageSlot position="CenterRightContentSlot"
																var="feature">
																<cms:component component="${feature}" element="div"
																	class="yComponentWrapper" />
															</cms:pageSlot>
															<cms:pageSlot position="BottomContentSlot" var="feature">
																<cms:component component="${feature}" element="div"
																	class="yComponentWrapper" />
															</cms:pageSlot>
														</div>
													</c:if>
												</div>
												<%--<c:if test="${empty cartData.rootGroups}">
													<cms:pageSlot position="EmptyCartMiddleContent" var="feature">
														<cms:component component="${feature}" element="div"
															class="yComponentWrapper content__empty" />
													</cms:pageSlot>
													</c:if>--%>
											</div>
										</div>
								</template:page>
