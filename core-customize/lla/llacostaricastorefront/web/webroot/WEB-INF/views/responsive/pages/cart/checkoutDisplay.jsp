<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:url value="/cart/checkout" var="checkoutUrl" scope="session"/>
<!-- <div class="row">
    <div class="col-xs-12 col-sm-10 col-md-7 col-lg-6 pull-right cart-actions--print">
        <div class="express-checkout">
            <div class="headline"><spring:theme code="text.expresscheckout.header"/></div>
            <strong><spring:theme code="text.expresscheckout.title"/></strong>
            <ul>
                <li><spring:theme code="text.expresscheckout.line1"/></li>
                <li><spring:theme code="text.expresscheckout.line2"/></li>
                <li><spring:theme code="text.expresscheckout.line3"/></li>
            </ul>
            <sec:authorize access="isFullyAuthenticated()">
                <c:if test="${expressCheckoutAllowed}">
                    <div class="checkbox">
                        <label>
                            <c:url value="/checkout/multi/express" var="expressCheckoutUrl" scope="session"/>
                            <input type="checkbox" class="express-checkout-checkbox" data-express-checkout-url="${fn:escapeXml(expressCheckoutUrl)}">
                            <spring:theme text="I would like to Express checkout" code="cart.expresscheckout.checkbox"/>
                        </label>
                     </div>
                </c:if>
           </sec:authorize>
        </div>
    </div>
</div> -->

<div class="cart__actions">
    <p class="headline"><spring:theme code="add.information.to.continue" text="Ingresá tus datos de contacto para seguir:" />
    <input type="hidden" id="c2cTimerOnCart" name="c2cTimerOnCart" value="${c2cTimerOnCart}"/>
<!--     <div class="col-xs-12"> -->
        <div class="clearfix guestCheckoutInfo">            
            <form id="guestCheckout" action="${guestCheckoutUrl}" method="post">
                <div class="guestcheckout-form">
                    <div class="comn-vali-msg"><spring:theme code="checkout.mandatory.message"/></div>
                    <br>
                    <div class="form-group clearfix">
                        <label class="control-label" for="guestCheckoutEmail"><spring:theme code="customer.cabletica.additionalEmail"/></label>
                        <input type="email" class="form-control" id="guestEmail" value="${user.additionalEmail}" name="guestEmail"/>
                    </div>
                    <div class="form-group clearfix">
                        <label class="control-label" for="guestMobile"><spring:theme code="customer.cabletica.mobilePhone"/></label>
                        <input type="mobilePhone" class="form-control" id="mobilePhone" value="${user.mobilePhone}" name="mobilePhone" maxlength="8"/>
                    </div>
                    <div class="form-group clearfix">
                    	<label class="control-label" for="documentType"><spring:theme code="customer.cabletica.documentType"/></label>
	                    <select name="documentType" class="docType form-control" id="documentType">
	                      <c:forEach items="${documentTypes}" var="documentType">
	                       <option value="${documentType.code}">${documentType.name}</option>
	                      </c:forEach>
	                    </select>	               
                  </div>
                  <div class="form-group clearfix">
                        <label class="control-label" for="documentNumber"><spring:theme code="customer.cabletica.documentNumber"/></label>
                        <input type="documentNumber" class="form-control" id="documentNumber" value="${user.mobilePhone}" name="documentNumber"/>
                   		<span class="guest-note"><spring:theme code="cart.guest.note.info" htmlEscape="false"/></span>
                  </div>
                </div>
                <span class="legalInfo">
	                <cms:pageSlot position="BottomParagraphContentSlot" var="feature">
			        	<cms:component component="${feature}" element="div" class="yComponentWrapper"/>
			        </cms:pageSlot>
		        </span>
                <div class="sticky-btn">
                    <div class="form-group clearfix text-center">
                        <button type="button" class="guestCableticaCheckoutButton btn btn-primary btn-block"><spring:theme code="cabletica.checkout.checkout"/></button>
                        <input type="hidden" id="guestEmail_error" value="<spring:theme code="guest.checkout.invalid.email"/>">
                        <input type="hidden" id="guestPhone_error" value="<spring:theme code="guest.checkout.invalid.phonenumber.cabletica"/>">
                        <input type="hidden" id="guestPhone_regex_error" value="<spring:theme code="guest.checkout.regex.phonenumber.cabletica"/>">
                        <input type="hidden" id="documentNumber_national_error" value="<spring:theme code="register.document.cedulanational.id.invalid"/>" >
                        <input type="hidden" id="documentNumber_resident_error" value="<spring:theme code="register.document.cedularesidential.id.invalid"/>" >
                        <input type="hidden" id="documentNumber_passport_error" value="<spring:theme code="register.document.cableticapassport.id.invalid"/>">
                    </div>
                </div>
            </form>
            <div id="checkoutOverlay" class="formOverlay" style="display: none">
                <common:loader />
            </div>
<!--         </div> -->
        
         
        <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
            <c:if test="${not empty siteQuoteEnabled and siteQuoteEnabled eq 'true'}">
                <div class="col-sm-4 col-md-3 col-md-offset-3 pull-right">
                    <button class="btn btn-default btn-block btn--continue-shopping js-continue-shopping-button"    data-continue-shopping-url="${fn:escapeXml(createQuoteUrl)}">
                        <spring:theme code="quote.create"/>
                    </button>
                </div>
            </c:if>
        </sec:authorize>
        <div class="col-sm-4 col-md-3 pull-right hide">
            <button class="btn btn-default btn-block btn--continue-shopping js-continue-shopping-button" data-continue-shopping-url="${fn:escapeXml(continueShoppingUrl)}">
                <spring:theme code="cart.page.continue"/>
            </button>
        </div>
        <cart:checkoutResponseForm />
    </div>
</div>


<!-- <c:if test="${showCheckoutStrategies && not empty cartData.entries}" >
    <div class="cart__actions">
        <div class="row">
            <div class="col-xs-12 col-sm-5 col-md-3 col-lg-2 pull-right">
                <input type="hidden" name="flow" id="flow"/>
                <input type="hidden" name="pci" id="pci"/>
                <select id="selectAltCheckoutFlow" class="doFlowSelectedChange form-control">
                    <option value="select-checkout"><spring:theme code="checkout.checkout.flow.select"/></option>
                    <option value="multistep"><spring:theme code="checkout.checkout.multi"/></option>
                    <option value="multistep-pci"><spring:theme code="checkout.checkout.multi.pci"/></option>
                </select>
                <select id="selectPciOption" class="display-none">
                    <option value=""><spring:theme code="checkout.checkout.multi.pci.select"/></option>
                    <c:if test="${!isOmsEnabled}">
                        <option value="default"><spring:theme code="checkout.checkout.multi.pci-ws"/></option>
                        <option value="hop"><spring:theme code="checkout.checkout.multi.pci-hop"/></option>
                    </c:if>
                    <option value="sop"><spring:theme code="checkout.checkout.multi.pci-sop" text="PCI-SOP" /></option>
                </select>
            </div>
        </div>
    </div>
</c:if> -->
