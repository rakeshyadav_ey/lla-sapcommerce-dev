<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/cart"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="multi-checkout-telco" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/checkout/multi"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<spring:url value="/login/register/termsandconditions" var="getTermsAndConditionsUrl" htmlEscape="false"/>

<div class="checkout-success">
    <div class="container order-confirmation-page-section">
        <div class="order-confirmation row"> 
            <div>
                <p class="order-confirmation-title">
                    <spring:theme code="orderconfirmation.details.of.purchase" />
                </p>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="purchase-request">
                    <div class="col-sm-1 col-md-1 col-lg-1"><span class="infoIcon"></span></div>
                    <div class="order-desc-content col-sm-11 col-sm-11 col-sm-11">
                        <p class="order-desc-title">
                            <spring:theme code="text.account.orderHistory.orderNumber" /> : 
                            <span class="orderNumber">
                            ${fn:escapeXml(orderData.code)}</span>
                        </p>
                        <p class="order-desc-sub-title">
                        <spring:theme code="orderconfirmation.successful.purchase.text" />
                        </p>
                    </div>
                </div>

                <div class="thankyou-text">
                    <p><spring:theme code="orderconfirmation.thankyou.text" /> </p>

                    <p class="thankyou-text-response">
                        <spring:theme code="orderconfirmation.thankyou.message1" htmlEscape="false"/>
                    </p>
                    <p class="thankyou-text-response">
                        <spring:theme code="orderconfirmation.thankyou.message2" htmlEscape="false"/>
                    </p>
                </div>

                <div class="sub-section-content">
                    <p class="sub-section-title">
                        <spring:theme code="orderconfirmation.invoice.title" />
                    </p>
                    <div class="sub-section-body">
                        <p>${ycommerce:encodeHTML(user.firstName)}&nbsp;${ycommerce:encodeHTML(user.lastName)}</p>
                        <p>${fn:escapeXml(email)}</p>
                        <p class="phoneNum"><b>${ycommerce:encodeHTML(user.mobilePhone)}</b></p>
                    </div>
                </div>
                
                <div class="checkout-order-summary ">
                    <multi-checkout-telco:cableticaOrderItems orderData="${orderData}" />
                </div>
            </div> 


            <div class="col-sm-12 col-md-6 col-lg-6">
                 <div class="checkout-order-summary">
                    <cart:cableticaOrderTotals orderData="${orderData}" showTaxEstimate="${taxEstimationEnabled}" />
                </div>

                 <div class="sub-section-content general-condition">
                    <p class="sub-section-title">
                        <spring:theme code="orderconfirmation.general.conditions" />
                    </p>
                    <div class="sub-section-body">
<%--                         <p>&#8226; <spring:theme code="orderconfirmation.general.conditions.text1" /></p> --%>
                        <p>&#8226; <spring:theme code="orderconfirmation.general.conditions.text2" /></p>
                        <p>&#8226; <spring:theme code="orderconfirmation.general.conditions.text3" /></p>
                        <p>&#8226; <spring:theme code="orderconfirmation.general.conditions.text4" />
                        <spring:theme code="orderconfirmation.general.conditions.number" htmlEscape="false"/>
                    </p>
                    </div>
                </div>


            </div>
        </div> 

        
    </div>
	
</div>