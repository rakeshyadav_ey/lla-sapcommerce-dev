<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="category" tagdir="/WEB-INF/tags/responsive/category" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<template:page pageTitle="${pageTitle}">

    <div class="fullWidthDiv">
        <c:forEach items="${categoriesList}" var="category">
            <div class="${category eq activeCategoryName ? 'active':''} js-image-div" data-content-id="${category}">
            <c:choose>
                <c:when test="${category eq '3PInternetTvTelephone'}">
                    <cms:pageSlot position="PlanesBanner" var="feature">
                        <cms:component component="${feature}" element="div" class="no-space yComponentWrapper"/>
                    </cms:pageSlot>
                </c:when>
                <c:when test="${category eq '2PInternetTv'}">
                    <cms:pageSlot position="PlanesBanner2" var="feature">
                        <cms:component component="${feature}" element="div" class="no-space yComponentWrapper"/>
                    </cms:pageSlot>
                </c:when>
                <c:when test="${category eq 'internetcabletica'}">
                    <cms:pageSlot position="PlanesBanner3" var="feature">
                        <cms:component component="${feature}" element="div" class="no-space yComponentWrapper"/>
                    </cms:pageSlot>
                </c:when>
                <c:otherwise>
                    <cms:pageSlot position="PlanesBanner4" var="feature">
                        <cms:component component="${feature}" element="div" class="no-space yComponentWrapper"/>
                    </cms:pageSlot>
                </c:otherwise>
            </c:choose>
             </div>
        </c:forEach>

        <div class="tabs-container">
          <category:categoryList/>
        </div>
    </div>

    <div class="container">
        <div class="process-selection-bar">
            <ul class="selection-state clearfix">
                <li class="active col-xs-4"><p class="level"><span class="process-state">1</span><span class="process-info"><spring:theme code="selection.level1.text"/></span></p></li>
                <li class="col-xs-4"><p class="level"><span class="process-state">2</span><span class="process-info"><spring:theme code="selection.level2.text"/></span></p></li>
                <li class="col-xs-4"><p class="level"><span class="process-state">3</span><span class="process-info"><spring:theme code="selection.level3.text"/></span></p></li>
            </ul>
        </div>
    </div>

    <div class="container">
        <div class="step-infobar display-none col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="info-msg">
                <div class="col-xs-12 col-sm-12 alertInfo">
                    <button class="close closeAccAlert" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
                    <p class="alertMsg"><spring:theme code="basket.page.bundle.message.add.cabletica.errorInfo" htmlEscape="false"/></p>
                </div>
            </div>
        </div>
    </div>

    <div class="allPlpPage">
       <product:productList/>
       <product:googleMap/>
     </div>
</template:page>
