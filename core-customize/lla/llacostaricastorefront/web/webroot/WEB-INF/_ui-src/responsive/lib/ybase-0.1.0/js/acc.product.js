ACC.product = {

    _autoload: [
        "bindToAddToCartForm",
        "enableStorePickupButton",
        "enableVariantSelectors",
        "bindFacets",
        "bindTabLogic",
        "bindSelectBoxLogic"
    ],


    bindFacets: function () {
        $(document).on("click", ".js-show-facets", function (e) {
            e.preventDefault();
            var selectRefinementsTitle = $(this).data("selectRefinementsTitle");
            var colorBoxTitleHtml = ACC.common.encodeHtml(selectRefinementsTitle);
            ACC.colorbox.open(colorBoxTitleHtml, {
                href: ".js-product-facet",
                inline: true,
                width: "480px",
                onComplete: function () {
                    $(document).on("click", ".js-product-facet .js-facet-name", function (e) {
                        e.preventDefault();
                        $(".js-product-facet  .js-facet").removeClass("active");
                        $(this).parents(".js-facet").addClass("active");
                        $.colorbox.resize()
                    })
                },
                onClosed: function () {
                    $(document).off("click", ".js-product-facet .js-facet-name");
                }
            });
        });
        enquire.register("screen and (min-width:" + screenSmMax + ")", function () {
            $("#cboxClose").click();
        });
    },

    bindTabLogic: function () {

        $(document).ready(function () {
            if($('.page-productDetails').length > 0){
                sessionStorage.setItem("page-wrapper", 'true');
            } else if ($('.page-internettvtelephone').length > 0) {
                if ((sessionStorage.getItem("page-wrapper") === 'true') && sessionStorage.getItem("residential-category") !== null) {
                    var type = sessionStorage.getItem("residential-category");
                    sessionStorage.removeItem('page-wrapper');
                    $('.js-tab-head[data-content-id='+ type + ']').trigger( "click" );
                } else {
                    sessionStorage.setItem("residential-category", $('.js-tab-head.active').attr('data-content-id'));
                }
            }
        });

        $(document).on("click", ".js-tab-head", function (e) {
            e.preventDefault();
            var _this = $(this);
            var tabId = _this.attr('data-content-id');
            $('.js-tab-head').each(function () {
                var _loopthis = $(this);
                _loopthis.removeClass('active');
            });
            _this.addClass('active');
            if(_this.hasClass('redirect')){
                var href = _this.find('a').attr('href');
                window.open(href);
                return false;
            }
            $('.tab-content-wrapper').each(function () {
                var _loopthis = $(this);
                _loopthis.addClass('display-none');
            });
            $('#' + tabId).removeClass('display-none');
            sessionStorage.setItem("residential-category", tabId);

            $('.js-image-div').each(function () {
                $(this).removeClass('active');
                if($(this).attr('data-content-id') == tabId){
                    $(this).addClass('active');
                }
            });

            var breadCrumbText = _this.find('.bundle-name').text();
            	breadCrumbText = breadCrumbText.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toLowerCase();
            });
            $(".breadcrumb-section ol li.active").text(breadCrumbText);

        });
        // spliting price to use 2 different font sizes
        $(".residential-plp .livemore-col").each(function(){
            var priceData = $(this).find(".cta-header .headertext").text();
            var dotCount = priceData.match(/\./g).length;
            console.log(dotCount);
            if (dotCount == 2) {
                var floatPrice = priceData.split(".");
                var htmlData = floatPrice[0]+'.'+floatPrice[1]+ "<span>."+floatPrice[2]+"</span>";
                $(this).find(".cta-header .headertext").text("").html(htmlData);
            }
        });
    },

    bindSelectBoxLogic: function () {
        $("#plp-region-selector").change(function(){
            var regionNames = $(this).val();
            console.log(regionNames);
            var newData = {
                  "regionCode":regionNames
            };
            var dataJson = JSON.stringify(newData);
            $.ajax({
                url: ACC.config.encodedContextPath + "/residential/regionNames",
                data: dataJson,
                contentType:'application/json',
                async: false,
                type: 'POST',
                dataType: 'json',
                success: function(data) {
                    $("#tvcabletica .ctabox-addtocart-form .ctabox-addToCart-btn").removeClass('ctaDisabledBtn');
                    $("#tvcabletica .ctaIVA").removeClass('display-none');
                    $("#tvcabletica .ctaPeriod").removeClass('display-none');
                    $.each( data, function( key, value ) {
                        $.each( value, function( key1, value1 ) {
                            var price = value1.toLocaleString();
                            $("#tvcabletica .ctaPrice#"+key1+" .priceData").text(" ");
                            $("#tvcabletica .ctaPrice#"+key1+" .priceData").prepend(price).prepend($("#currencySymbol").val());
                        });
                    });
                },
                failure: function(error){
                    console.error("error: " + error);
                }
            });
        });
    },


    enableAddToCartButton: function () {
        $('.js-enable-btn').each(function () {
            if (!($(this).hasClass('outOfStock') || $(this).hasClass('out-of-stock'))) {
                $(this).prop("disabled", false);
            }
        });
    },

    enableVariantSelectors: function () {
        $('.variant-select').prop("disabled", false);
    },

    bindToAddToCartForm: function () {
        var addToCartForm = $('.add_to_cart_form');
        addToCartForm.ajaxForm({
        	beforeSubmit:ACC.product.showRequest,
        	success: ACC.product.displayAddToCartPopup
         });
        setTimeout(function(){
        	$ajaxCallEvent  = true;
         }, 2000);

        /*if($('.ctabox-form').length) {
           $('.ctabox-addtocart-form').submit(function() {
               var flag = 'true';
               $('.cta-box-container .cta-box').each(function(){
                   if($(this).hasClass("selected")){
                       flag = 'false';
                   }
               });
               if(flag == 'true'){
                   $(this).parent().parent().parent('.cta-box').addClass('selected');
                   return true; // return false to cancel form action
               }else{
                   $(".step-infobar").removeClass("display-none");
                   return false;
               }
           });
        }*/

        $(document).on("click",'.closeAccAlert', function(e){
            e.preventDefault()
            $(".step-infobar").addClass("display-none");
        });

     },
     showRequest: function(arr, $form, options) {  
    	 if($ajaxCallEvent)
    		{
    		 $ajaxCallEvent = false;
    		 return true;
    		}   	
    	 return false;
 
    },

    bindToAddToCartStorePickUpForm: function () {
        var addToCartStorePickUpForm = $('#colorbox #add_to_cart_storepickup_form');
        addToCartStorePickUpForm.ajaxForm({success: ACC.product.displayAddToCartPopup});
    },

    enableStorePickupButton: function () {
        $('.js-pickup-in-store-button').prop("disabled", false);
    },

    displayAddToCartPopup: function (cartResult, statusText, xhr, formElement) {
    	$ajaxCallEvent=true;
        $('#addToCartLayer').remove();
        if (typeof ACC.minicart.updateMiniCartDisplay == 'function') {
            ACC.minicart.updateMiniCartDisplay();
        }
        var titleHeader = $('#addToCartTitle').html();

        ACC.colorbox.open(titleHeader, {
            html: cartResult.addToCartLayer,
            width: "460px"
        });

        var productCode = $('[name=productCodePost]', formElement).val();
        var quantityField = $('[name=qty]', formElement).val();

        var quantity = 1;
        if (quantityField != undefined) {
            quantity = quantityField;
        }

        var cartAnalyticsData = cartResult.cartAnalyticsData;

        var cartData = {
            "cartCode": cartAnalyticsData.cartCode,
            "productCode": productCode, "quantity": quantity,
            "productPrice": cartAnalyticsData.productPostPrice,
            "productName": cartAnalyticsData.productName
        };
        ACC.track.trackAddToCart(productCode, quantity, cartData);
    }
};

$(document).ready(function () {
	$ajaxCallEvent = true;
    ACC.product.enableAddToCartButton();
});