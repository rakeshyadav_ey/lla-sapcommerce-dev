ACC.serviceability = {

    _autoload: [
        "bindGMap"
    ],


    loadGoogleMap: function(){

            var origLattitude, origLongitude;

            //fetching the lat-long values on checkout page
            if($.isNumeric($("#checkoutLatValue").val())){
              origLattitude = $("#checkoutLatValue").val();
              origLongitude = $("#checkoutLongValue").val();
            }
            else{
              //always fetching the values from the hidden variable
              origLattitude = $("#lat").val();
              origLongitude = $("#long").val();
            }  

             var centerPoint = new  google.maps.LatLng(origLattitude, origLongitude); 

              var map = new google.maps.Map(document.getElementById('openMapServiceability'), {
                center: centerPoint,
                mapTypeId: google.maps.MapTypeId.ROADMAP, 
                zoom: 9
              });             

              var infoWindow = new google.maps.InfoWindow();
              infoWindow.close();

               var marker = new google.maps.Marker({  
                draggable: true, 
                position: centerPoint,
                map: map,   
                title: "Map"
              }); 


              const locationButton = document.createElement("button");
              locationButton.textContent = "Hacé click para ir a tu ubicación";
              locationButton.classList.add("custom-map-control-button");
              map.controls[google.maps.ControlPosition.TOP_RIGHT].push(locationButton);

              locationButton.addEventListener("click", function() {
                  infoWindow.close();
                  marker.setIcon(null);
                  marker.setVisible(false);

                  // Try HTML5 geolocation.
                  if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(
                      function (position) {
                        const pos = {
                          lat: position.coords.latitude,
                          lng: position.coords.longitude,
                        };

                        marker.setVisible(true);
                        marker.setPosition(pos);
                        
                        map.setCenter(pos);

                        //set the selected lattitude and longitude value in the DOM
                        //when user clicks on the locate Me button
                        $("#lat").val(position.coords.latitude);
                        $("#long").val(position.coords.longitude);

                      },
                      function(error) {
                        handleLocationError(true, infoWindow, map.getCenter());
                      }
                    );
                  } else {
                    // Browser doesn't support Geolocation
                    handleLocationError(false, infoWindow, map.getCenter());
                  }
             });

              var input = document.getElementById('autoCompleteSearchInput');
              map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
              
              var autocomplete = new google.maps.places.Autocomplete(input);
              autocomplete.bindTo('bounds', map);  

              autocomplete.addListener('place_changed', function() {
                infoWindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                  window.alert("Autocomplete's returned place contains no geometry");
                  return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                  map.setCenter(place.geometry.location);
                  map.setZoom(17);
                }

                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                //set the selected lattitude and longitude value in the DOM 
                //from location selected form autocomplete
                $("#lat").val(place.geometry.location.lat());
                $("#long").val(place.geometry.location.lng());

           });

            //onload set values to hidden variable
            $("#lat").val( origLattitude );
            $("#long").val( origLongitude );

            google.maps.event.addListener(marker, 'dragend', function (event) {
              //set the selected lattitude and longitude value in the DOM when the pin is dragged
              $("#lat").val( event.latLng.lat() );
              $("#long").val( event.latLng.lng() );
            });

            //on click set the marker pin
            google.maps.event.addListener(map, 'click', function(event) {
              if (marker) {
                marker.setPosition(event.latLng)
              } else {
                marker = new google.maps.Marker({
                  map: map,
                  position: event.latLng,
                  draggable: true
                });
              }
              $("#lat").val( event.latLng.lat() );
              $("#long").val( event.latLng.lng() );
          });
    },
    bindGMap: function () {

        $(document).on("click",".ctaBox-clickToCallBtn", function (e) {
            e.preventDefault();

            //resetting the form on load
            $("#addressForm")[0].reset();
            $("#addressForm").find("select").prop("selectedIndex",0);

            //check if any plan is already added to the cart
            var selectedflag = 'true';
            $('.cta-box-container .cta-box').each(function(){
                 if($(this).hasClass("selected")){
                     selectedflag = 'false';
                 }
            });

           if(selectedflag == 'false'){
                $(".step-infobar").removeClass("display-none");
                return false; // return false to cancel form action
           }

            ACC.global.addGoogleMapsApi("ACC.serviceability.loadGoogleMap");
            if (typeof ACC.gtmTagAnalytics.loadedMapPopup === "function") {
                ACC.gtmTagAnalytics.loadedMapPopup();
            }
            //ACC.global.addGoogleMapsApi1();

            var productId = $(this).closest('.divPricenButtons').find('.pageProducts').attr('data-attr-id');
            var initialQuantity = $(this).closest('.divPricenButtons').children('#addonAddtoCart').find('[name=initialQuantity]').attr('value');
            var qty = $(this).closest('.divPricenButtons').children('#addonAddtoCart').find('[name=qty]').attr('value');
            var processType = $(this).closest('.divPricenButtons').children('#addonAddtoCart').find('[name=processType]').attr('value');
            var productTitle = $(this).closest('.divPricenButtons').find('.pageProducts').attr('data-attr-name');
            var productPrice = $(this).closest('.divPricenButtons').children('.priceInfo').find('.ctaPrice').html();
            var cartUrl = "?productCodePost=" + productId + "&qty=" + qty + "&initialQuantity=" + initialQuantity + "&processType=" + processType;
            var cartPageUrl = ACC.config.encodedContextPath + '/addtocart' + cartUrl;
            var pageSource="plp";
            var pageParams = { cartPageUrl: cartPageUrl, productId: productId, productTitle: productTitle, productPrice:productPrice };

            //Selected Item on main page
            var selectedItem = $(this);

            var innerWdt, innerHgt;

            if ($(window).width() <= 767 ) {
                 innerWdt = '96%';
                 innerHgt = '100%';
                 $("#cboxContent").css({"margin": 'auto'});
            }
            else if ($(window).width() <= 1024) {
                 innerWdt = '90%';
                 innerHgt = '50%';
            }
            else{
                 innerWdt = '70%';
                 innerHgt = '50%';
            }

            ACC.colorbox.open("Revisar la cobertura",{
                inline: true,
                href: "#mapServiceabilityCheck",
                scrolling:true,
                width: innerWdt,
                height: innerHgt,
                overlayClose: false,
                onComplete: function ()
                {
                    $("#cboxClose").addClass("mapPopupCloseBtn").removeClass("customInfoPopupClose");
                    $(this).colorbox.resize();

                    $('.confirmServiceBtn').on('click', function(event) {

                        var mapParams = { selectedLat: $('#lat').val(), selectedLng: $('#long').val() }

                        //check if location is serviceable
                        if($("#addressForm").valid()){
                            $("#mapOverlay").css('display','block');
                            $(".formOverlay").fadeIn(300);
                            ACC.serviceability.bindCheckServiceability(pageSource,pageParams,mapParams,selectedItem);
                        }
                    });

                    //if user clicks skip button-redirect to the cart page next step
                    $('.skipServiceBtn').on('click', function(event) {
                        ACC.colorbox.close();
                        //Call dataLayer Push Function gtm
                        if (typeof ACC.gtmTagAnalytics.addToCartCall === "function") {
                            ACC.gtmTagAnalytics.addToCartCall(selectedItem);
                        }
                        //redirect the page to cart step2
                        window.location.replace(pageParams.cartPageUrl);
                    });

                    //if user clicks close icon on the dialog then redirect to the cart page next step
                    $(document).on("click",".mapPopupCloseBtn#cboxClose", function (e) {
                        ACC.colorbox.close();
                        //Call dataLayer Push Function gtm
                        if (typeof ACC.gtmTagAnalytics.addToCartCall === "function") {
                            ACC.gtmTagAnalytics.addToCartCall(selectedItem);
                        }
                        //redirect the page to cart step2
                        window.location.replace(pageParams.cartPageUrl);
                    });
                }
            });
        });

        //when user is on the checkout page and click on step 2 button
        $(document).on("click","#addressSubmit", function (e) {
            e.preventDefault();
            ACC.global.addGoogleMapsApi("ACC.serviceability.loadGoogleMap");
            if (typeof ACC.gtmTagAnalytics.loadedMapPopup === "function") {
                ACC.gtmTagAnalytics.loadedMapPopup();
            }

            $(".mapDropdownsRegion .address_province").removeAttr("disabled");
            $(".mapDropdownsRegion .address_canton").removeAttr("disabled");
            $(".mapDropdownsRegion .address_district").removeAttr("disabled");
            $(".mapDropdownsRegion .address_neighbourhood").removeAttr("disabled");
            $(".mapTextBox [name='landMark']").removeAttr("disabled");

            $('.skipServiceBtn').hide();
            $('#cboxClose').hide();
            $('.popUpMapButtons').css("float","right");

            //get values to hidden variable
            var cLatt = $("#checkoutLatValue").val();
            var cLong = $("#checkoutLongValue").val();

			if(cLatt == null || cLatt == '/'){
				 cLatt = 9.946;
	    		 cLong = -84.012;
			}

            $("#lat").val(cLatt);
            $("#long").val(cLong);
            var pageSource="checkout";
            var pageParams = { checkoutPg:"true" };

            ACC.colorbox.open("Revisar la cobertura",{
                inline: true,
                href: "#mapServiceabilityCheck",
                scrolling:true,
                width:"70%",
                height:"50%",
                overlayClose: false,
                onComplete: function ()
                {
                    $(this).colorbox.resize();
                    $('#cboxClose').hide();
                    $('.confirmServiceBtn').on('click', function(event) {
                        var mapParams = { selectedLat: cLatt, selectedLng: cLong }

                        if ($("#address_validation_error").length) {
                          var province_error = $("#province_error").val();
                          var canton_error = $("#canton_error").val();
                          var district_error = $("#district_error").val();
                          var neighbourhood_error = $("#neighbourhood_error").val();
                          var landMark_error = $("#landMark_error").val();
                        }

                        //validating the serviceability form
                        if(ACC.serviceability.checkoutMapValidation() == false){
                            //will be called if no dropdowns are selected
                            if($('.mapDropdownsRegion label.error').length == 0){
                              $( '<label id="address.province-error" class="error" for="address.province">'+province_error+'</label>' ).insertAfter(".mapDropdownsRegion select.address_province");
                              $( '<label id="address.canton-error" class="error" for="address.canton">'+canton_error+'</label>' ).insertAfter(".mapDropdownsRegion select.address_canton");
                              $( '<label id="address.district-error" class="error" for="address.district">'+district_error+'</label>' ).insertAfter(".mapDropdownsRegion select.address_district");
                              $( '<label id="address.neighbourhood-error" class="error" for="address.neighbourhood">'+neighbourhood_error+'</label>' ).insertAfter(".mapDropdownsRegion select.address_neighbourhood");
                            }
                            if (ACC.serviceability.checkoutMapLandmarkValidation() == false){
                              if($('.mapTextBox label[id="address.landMark-error"]').length == 0){
                                $('<label id="address.landMark-error" class="error" for="address.landMark">'+ landMark_error +'</label>').insertAfter('.mapTextBox input[name="landMark"]');
                              }
                            }
                        }
                        else if (ACC.serviceability.checkoutMapLandmarkValidation() == false){
                            //will be called if all dropdowns are selected and the landmark is empty
                            if($('.mapTextBox label[id="address.landMark-error"]').length == 0){
                              $('<label id="address.landMark-error" class="error" for="address.landMark">'+ landMark_error +'</label>').insertAfter('.mapTextBox input[name="landMark"]');
                            }
                        }
                        else{
                            $("#mapOverlay").css('display','block');
                            $(".formOverlay").fadeIn(300);
                            ACC.serviceability.bindCheckServiceability(pageSource,pageParams,mapParams);
                        }
                    });
                }
            });
        });

     },

     //Function to check if a given location is serviceable or not
     bindCheckServiceability: function (pageSource, pageParams,mapParams,selectedItem = '') {
        //var origLat = 9.946;
        //var origLong = -84.012
        var url, userLat, userLng;

        /*checking the lat long values - if user skips the dialog then adding orig lat long values
          else picking up from the hidden variable*/
        if($.isNumeric($("#mapServiceabilityCheck #lat").val())){
          userLat = $("#mapServiceabilityCheck #lat").val();
          userLng = $("#mapServiceabilityCheck #long").val();
        }
        else{
          //console.log('No lat long value is set');
          userLat = 9.946;
          userLng = -84.012;
          //again setting the lat-long in the map, so map is loaded with the default
          $("#mapServiceabilityCheck #lat").val(userLat);
          $("#mapServiceabilityCheck #long").val(userLng);
        }

       //fetching the province, building, district and landmark form the dialog form
       var map_prov = $("#mapServiceabilityCheck .address_province").val();
       var map_building = $("#mapServiceabilityCheck .address_canton").val();
       var map_district = $("#mapServiceabilityCheck .address_district").val();
       var map_neighbourhood = $("#mapServiceabilityCheck .address_neighbourhood").val();
       var map_land = $("#mapServiceabilityCheck [name='landMark']").val();

       //setting the selected values from dialog in the checkout step 2 form
       $(".address_province").removeAttr("disabled");
       $(".address_canton").removeAttr("disabled");
       $(".address_district").removeAttr("disabled");
       $(".address_neighbourhood").removeAttr("disabled");
       $("[name='landMark']").removeAttr("disabled");

       $(".address_province").val(map_prov);
       $(".address_canton").val(map_building);
       $(".address_district").val(map_district);
       $(".address_neighbourhood").val(map_neighbourhood);
       $("[name='landMark']").val(map_land);

        url  = ACC.config.encodedContextPath + '/serviceabilitycheck?lat='+userLat+'&long='+userLng+'&prov='+map_prov+'&build='+map_building+'&dist='+map_district+'&neigh='+map_neighbourhood+'&land='+map_land+'&originSource='+pageSource;
        //console.log('url: '+ url);
        $.ajax({
            url: url,
            type: 'GET',
            success: function (response) {
                if(response == true)
                {
                    console.log("Serviceability :" + response);
                    ACC.colorbox.close();

                    //checkoutpage check
                    if(pageParams.checkoutPg){
                         $('#checkoutLatValue').remove();
                         $('#checkoutLongValue').remove();

                         $('#addressForm').append('<input type="hidden" name="CSRFToken" value='+ACC.config.CSRFToken+'>');

                         $('#addressForm').submit();
                         $("#mapOverlay").css('display','none');
                         $(".formOverlay").fadeOut(300);
                          
                    }
                    else{
                        //Call dataLayer Push Function gtm
                        if(selectedItem != '' && typeof ACC.gtmTagAnalytics.addToCartCall === "function") {
                                ACC.gtmTagAnalytics.addToCartCall(selectedItem);
                        }
                        //redirecting user to cart page
                        window.location.replace(pageParams.cartPageUrl);
                    }
                }
                else{
                     console.log("Serviceability :" + response);

                     //checkoutpage check
                     if(pageParams.checkoutPg){
                         //open the non serviceability dialog and redirect user to cabletica url
                         ACC.colorbox.open('', {
                            href: "#callResponseFormId",
                            inline:true,
                            scrolling:true,
                            width:"50%",
                            overlayClose: false,
                            onComplete: function(){
                                 $("#cboxClose").addClass("customInfoPopupCloseBtn").removeClass("mapPopupCloseBtn");
                                 var redirectUrl = "https://www.cabletica.com/";
                                 $('.callResponseBtn').on('click', function(event) {
                                    ACC.colorbox.close();
                                    window.location.replace(redirectUrl);
                                });
                            }
                        });
                         var url = ACC.config.encodedContextPath + "/noservicable-c2cRequest";
                                  $.ajax({
                                      type: "POST",
                                      url: url,
                                      success: function(data, xhr) {
                                          console.log("Click to call Triggered ");
                                      },
                                      error: function(result) {
                                          console.log("Error in call");
                                      }
                           });
                     }
                     else{
                        //open the request information form dialog
                        ACC.serviceability.bindRequestInfo(pageParams, selectedItem);
                     }
                }
            },
            error: function (xhr, textStatus, errorThrown) {
              //open the request information form dialog
              ACC.serviceability.bindRequestInfo(pageParams, selectedItem);
            }
        }).done(function() {
            setTimeout(function(){
              $("#mapOverlay").css('display','none');
              $(".formOverlay").fadeOut(300);
            },500);
        });
    },

    //Function to save the information
    bindRequestInfo: function (pageParams, selectedItem = '') {
        var innerWdt, innerHgt;
        //mobile
        if ($(window).width() <= 767 ) {
             innerWdt = '96%';
             innerHgt = '100%';
             $("#cboxContent").css({"float":'inherit', "margin": 'auto'});
        }
        else{
             innerWdt = '800px';
             innerHgt = '450px';
             $("#cboxContent").css("float", 'left');
        }

        //hide the success form
        $("#infoSuccessFromContent").hide();

        ACC.colorbox.open('', {
            href: "#infoModalFormID",
            inline: true,
            innerWidth: innerWdt,
            innerHeight: innerHgt,
            overlayClose: false,
            onComplete: function () {
                //onload the c2c popup,hiding the loader overlay
                $("#c2cOverlay").css('display','none');
                $(".formOverlay").fadeOut(300);

                $("#cboxTitle").hide();
                //Call dataLayer Push Function gtm
                if(selectedItem != '' && typeof ACC.gtmTagAnalytics.c2cModelOpen === "function") {
                        ACC.gtmTagAnalytics.c2cModelOpen(selectedItem);
                }
                //set the product name and price on the dialog
                $('.selectedPlanDsc').html(pageParams.productTitle);
                $('.selectPlanPrice').html(pageParams.productPrice);

                $("#cboxLoadedContent").css({"width":'auto', "margin":'0px', "padding":'0px'});
                $("#cboxContent").css({"width" : innerWdt, "height" : innerHgt});
                $("#cboxClose").addClass("customInfoPopupClose").removeClass("mapPopupCloseBtn");


                    //on submit thie, save the user data
                    $('form#ClickCallForm1').on('click', '#c2cButton ', function(event) {  
                      //event.preventDefault();
                      if($("form#ClickCallForm1").valid()){
                          $("#c2cOverlay").css('display','block');
                          $(".formOverlay").fadeIn(300);
                          var phNum, idNum, prodtId, email;

                          phNum = $('input#phoneNumber').val();
                          idNum = $('input#identityNumber').val();
                          email = $('input#email').val();

                          var url = ACC.config.encodedContextPath + '/click/clickToCallForm';
                              $.ajax({
                              type: "POST",
                              url: url,
                              data: {phoneNumber: phNum, identityNumber: idNum, product: pageParams.productId, email: email},
                              success: function (data) {
                                  $("#infoFromContent").hide();
                                  $("#infoSuccessFromContent").show();
                                  //console.log(data);
                                  if(typeof ACC.gtmTagAnalytics.c2cSuccessCall === "function") {
                                      ACC.gtmTagAnalytics.c2cSuccessCall(selectedItem, data);
                                  }
                              }
                          }).done(function() {
                                setTimeout(function(){
                                  $("#c2cOverlay").css('display','none');
                                  $(".formOverlay").fadeOut(300);
                                },500);
                            });
                      }
                     
                });

                $("#infoFromContent").show();
            },
        });
    },

     handleLocationError : function(browserHasGeolocation,infoWindow,pos){
      infoWindow.setPosition(pos);
      infoWindow.setContent(
        browserHasGeolocation
          ? "Error: The Geolocation service failed."
          : "Error: Your browser doesn't support geolocation."
      );
      infoWindow.open(map);
    },

    checkoutMapValidation: function(){
        var isValid = true;
        $(".mapDropdownsRegion").find("select").each(function() {
            var element = $(this);
            if (element.prop("selectedIndex") == 0 ) {
              $(element).addClass("error");
              isValid = false;
            }
            else{
              $(element).removeClass("error");
              $(element).nextAll('label.error').remove();
              isValid = true;
            }

            $(element).on('change', function() {
               $(element).removeClass("error");
               $(element).nextAll('label.error').remove();
               isValid = true;
            });
         }); 
        return isValid;
     },

     checkoutMapLandmarkValidation: function() {
        var isLandmarkValid = true;
        if ($('.mapTextBox input[name="landMark"]').val() == "") {
            $('.mapTextBox input[name="landMark"]').addClass("error");
            isLandmarkValid = false;
        }
        else{
            isLandmarkValid = true;
        }

        $('.mapTextBox input[name="landMark"]').on('keyup', function() {
           $('.mapTextBox input[name="landMark"]').removeClass("error");
           $('.mapTextBox input[name="landMark"]').nextAll('label.error').remove();
           isLandmarkValid = true;
        });
        return isLandmarkValid;
     }
};

$(document).ready(function () {
    var origLattitude = 9.946;
    var origLongitude = -84.012

    //Enable the cta box buttons on page load
    setTimeout(function(){
        $('.ctaBox-clickToCallBtn .ctabox-addToCart-btn').prop( "disabled", false );
    }, 2000);

    //prevent the form from submitting on Enter key
    $('#addressForm').bind('keypress keydown keyup', function(event){
       if(event.keyCode == 13) {
          event.preventDefault();
       }
    });


    //clickToCall form validation
    if ($("form#ClickCallForm1").length) {
       var email_error = $("input#email_error").val();
       var cedulaPassport_error = $("input#cedulaPassport_error").val();
       var phoneNumber_error = $("input#phoneNumber_error").val();
       var phoneNumber_regex_error = $("input#phoneNumber_regex_error").val();

       $.validator.addMethod("phoneRegx", function (value, element, regexpr) {
            return regexpr.test(value);
       }, phoneNumber_error);

       $.validator.addMethod("regx", function(value, element, regexpr) {
            return regexpr.test(value);
        }, email_error);


       $("form#ClickCallForm1").validate({
            rules: {
               "phoneNumber": {
                    required: true,
                    maxlength: 8,
                    minlength: 8,
                    phoneRegx: /^(2|4|5|6|7|8)([0-9]{7})/
               },
               "identityNumber": {
                  required: true,
               },
               "email": {
                    required: true,
                    regx: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    email: true
               }
            },
            messages: {
                phoneNumber:{
                  required: phoneNumber_error,
                  phoneRegx: phoneNumber_regex_error,
                  minlength:phoneNumber_error,
                  maxlength:phoneNumber_error
                },
                identityNumber: {
                  required: cedulaPassport_error,
                },
                email: {
                  required: email_error,
                  email: email_error
                }
            }
        });
    }
});
