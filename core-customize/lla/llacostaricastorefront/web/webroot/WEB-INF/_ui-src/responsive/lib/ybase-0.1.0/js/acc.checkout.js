ACC.checkout = {

	_autoload: [
		"bindCheckO",
		"bindForms",
		"bindSavedPayments",
		"bindC2CTimeout",
		"bindPlaceOrderSingleClick",
		["bindCheckoutGoogleMap", $("#checkoutPageMap").length > 0]
		],

	bindPlaceOrderSingleClick:function(){
	    $(document).ready(function (){
		$("#placeOrderForm1").submit(function() {
		    // submit more than once return false
		    $(this).submit(function() {
			return false;
		    });
		    // submit once return true
		    return true;
		});
	    });
	},
    bindC2CTimeout:function(){

           if($('.2.step-head.js-checkout-step.active').length > 0 || $('.3.step-head.js-checkout-step.active').length > 0 || $('.4.step-head.js-checkout-step.active').length > 0){
             var timerC=$("#c2cTimerCOnCheckout").val();
               setTimeout(function() {
                   var url = ACC.config.encodedContextPath + "/genesys/clickToCallRequest";
                   $.ajax({
                       type: "POST",
                       url: url,
                       success: function(data, xhr) {
                           console.log(data + "C2C SUCCESS");
                       },
                       error: function(result) {
                           console.log("C2C ERROR");
                       }
                   });
               }, timerC);
           }

         if($('.1.step-head.js-checkout-step.active').length > 0){
            var timerB=$("#c2cTimerBOnCheckout").val();
              setTimeout(function() {
                  var url = ACC.config.encodedContextPath + "/genesys/clickToCallRequest";
                  $.ajax({
                      type: "POST",
                      url: url,
                      success: function(data, xhr) {
                          console.log(data + "C2C SUCCESS");
                      },
                      error: function(result) {
                          console.log("C2C ERROR");
                      }
                  });
              }, timerB);
          }

    },
	bindForms:function(){

		/*$(document).on("click","#addressSubmit",function(e){
			e.preventDefault();
			//ACC.checkout.bindAddressFormSubmit();
			//$('#addressForm').submit();	
		})*/
		
		$(document).on("click","#deliveryMethodSubmit",function(e){
			e.preventDefault();
			$('#selectDeliveryMethodForm').submit();	
		})

	},

	bindSavedPayments:function(){
		$(document).on("click",".js-saved-payments",function(e){
			e.preventDefault();

			var title = $("#savedpaymentstitle").html();

			$.colorbox({
				href: "#savedpaymentsbody",
				inline:true,
				maxWidth:"100%",
				opacity:0.7,
				//width:"320px",
				title: title,
				close:'<span class="glyphicon glyphicon-remove"></span>',
				onComplete: function(){
				}
			});
		})
	},

	bindCheckO: function ()
	{
		var cartEntriesError = false;
		
		// Alternative checkout flows options
		$('.doFlowSelectedChange').change(function ()
		{
			if ('multistep-pci' == $('#selectAltCheckoutFlow').val())
			{
				$('#selectPciOption').show();
			}
			else
			{
				$('#selectPciOption').hide();

			}
		});



		$('.js-continue-shopping-button').click(function ()
		{
			var checkoutUrl = $(this).data("continueShoppingUrl");
			window.location = checkoutUrl;
		});
		
		$('.js-create-quote-button').click(function ()
		{
			$(this).prop("disabled", true);
			var createQuoteUrl = $(this).data("createQuoteUrl");
			window.location = createQuoteUrl;
		});

		
		$('.expressCheckoutButton').click(function()
				{
					document.getElementById("expressCheckoutCheckbox").checked = true;
		});
		
		$(document).on("input",".confirmGuestEmail,.guestEmail",function(){
			  
			  var orginalEmail = $(".guestEmail").val();
			  var confirmationEmail = $(".confirmGuestEmail").val();
			  
			  if(orginalEmail === confirmationEmail){
			    $(".guestCheckoutBtn").removeAttr("disabled");
			  }else{
			     $(".guestCheckoutBtn").attr("disabled","disabled");
			  }
		});
		
		$('.js-continue-checkout-button').click(function ()
		{
			var checkoutUrl = $(this).data("checkoutUrl");
			
			cartEntriesError = ACC.pickupinstore.validatePickupinStoreCartEntires();
			if (!cartEntriesError)
			{
				var expressCheckoutObject = $('.express-checkout-checkbox');
				if(expressCheckoutObject.is(":checked"))
				{
					window.location = expressCheckoutObject.data("expressCheckoutUrl");
				}
				else
				{
					var flow = $('#selectAltCheckoutFlow').val();
					if ( flow == undefined || flow == '' || flow == 'select-checkout')
					{
						// No alternate flow specified, fallback to default behaviour
						window.location = checkoutUrl;
					}
					else
					{
						// Fix multistep-pci flow
						if ('multistep-pci' == flow)
						{
						flow = 'multistep';
						}
						var pci = $('#selectPciOption').val();

						// Build up the redirect URL
						var redirectUrl = checkoutUrl + '/select-flow?flow=' + flow + '&pci=' + pci;
						window.location = redirectUrl;
					}
				}
			}
			return false;
		});

	},

	loadGoogleMapOnCheckout:function() {
      var oLattitude, oLongitude;

      if($.isNumeric($("#checkoutLatValue").val())){
	      oLattitude = $("#checkoutLatValue").val();
	      oLongitude = $("#checkoutLongValue").val();
       }
	   else if($.isNumeric($("#lat").val())){
	      //always fetching the values from the hidden variable
	      oLattitude = $("#lat").val();
	      oLongitude = $("#long").val();
	   } 

	  //onload set values to hidden variable
      $("#checkoutLatValue").val( oLattitude );
      $("#checkoutLongValue").val( oLongitude );

      var centerPoint = new  google.maps.LatLng(oLattitude, oLongitude);

      var mapOpts = {  
          zoom: 10,     
          mapTypeId: google.maps.MapTypeId.ROADMAP,   
          center: centerPoint,
      }   
          
      var cmap = new google.maps.Map(document.getElementById("checkoutPageMap"), mapOpts);    
          
      var marker = new google.maps.Marker({  
          position: centerPoint,
          map: cmap,
          title: "Map"
      }); 

      var infowindow = new google.maps.InfoWindow({
		    content: "<p>Your Location:" + marker.getPosition() + "</p>",
	  });
	  google.maps.event.addListener(marker, "click", () => {
	    infowindow.open(map, marker);
	  });
	},

	bindCheckoutGoogleMap:function() {
	  ACC.global.addGoogleMapsApi("ACC.checkout.loadGoogleMapOnCheckout");	
	}

	/*bindAddressFormSubmit:function() {

	}*/

};

$(document).ready(function () {
		
	if ($("#profile_validation_error").length) {
		var firstName_error = $("#firstName_error").val();
		var lastName_error = $("#lastName_error").val();
	}

	$("#customerProfileForm").validate({ 
		rules: {
			"firstName": {
				required: true,
			},
			"lastName": {
				required: true,
			}
		},
		messages: {
			firstName: firstName_error,
			lastName: lastName_error
		},
        invalidHandler: function() {
        	var errorMsg = $("#gtmPersonalFormError").val();//checkout-1
        	if (typeof googleAnalyticsTagManager.handleGtmErrors === "function") {
                googleAnalyticsTagManager.handleGtmErrors(errorMsg);
            }            
        }
	});

	if ($("#address_validation_error").length) {
		var province_error = $("#province_error").val();
		var canton_error = $("#canton_error").val();
		var district_error = $("#district_error").val();
		var neighbourhood_error = $("#neighbourhood_error").val();
		var landMark_error = $("#landMark_error").val();
	}

	$("#addressForm").validate({ 
		rules: {
			"province": {
				required: true,
			},
			"building": {
				required: true,
			},
			"neighbourhood": {
				required: true,
			},
			"district": {
				required: true,
			},
			"landMark": {
				required: true,
			}
		},
		messages: {
			province: province_error,
			building: canton_error,
			neighbourhood: neighbourhood_error,
			district: district_error,
			landMark: landMark_error
		},
        invalidHandler: function() {
        	var errorMsg = $("#gtmAddressFormError").val();//checkout-2
			if (typeof googleAnalyticsTagManager.handleGtmErrors === "function") {
                googleAnalyticsTagManager.handleGtmErrors(errorMsg);
            }           
        }
	});

	//maxlength characters for address validation
	$("#profileSubmit").on('click', function(event) {
		var l1 = $("input[name='firstName']").val().length;
		var l2 = $("input[name='lastName']").val().length;
		
		var line =  l1+l2;
		var len = 25;

		if(line > len){
			$("label.maxLenError").remove();
				if($("input[name='lastName'] + maxLenError").length == 0) {
			    var addr_len_error = $("#addr_maxlen_error").val();
			    var label = '<label id="register.lastName-error" class="error maxLenError" for="register.lastName">'+addr_len_error+'</label>';
				$("input[name='lastName']").after(label);
			}
		  	return false;
		}
		else{
		    return true;
		}
	});
	
	//guest checkout mobile phone length(8) validation js code for cabletica only
	if ($("#guestCheckout").length) {
        $("#guestCheckout").on('keypress',"input[name='mobilePhone']", function(e){
            const pattern = /^[0-9]$/;
            return pattern.test(e.key )
        });
        var guestEmail_error = $("#guestEmail_error").val();
        var guestPhone_error = $("#guestPhone_error").val();
		var guestPhone_regex_error = $("#guestPhone_regex_error").val(); 

        $("#guestCheckout").validate({
            rules: {
                "guestEmail": {
                    required: true,
                    regx: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
					email: true
                },
                "mobilePhone": {
                    required: true,
					maxlength: 8,
					minlength: 8,
					regx: /^(2|4|5|6|7|8)([0-9]{7})/
                }
            },
            messages: {
                guestEmail: {
					required: guestEmail_error,
					email: guestEmail_error
				},
                mobilePhone:{
					required: guestPhone_error,
					regx: guestPhone_regex_error,
					minlength:guestPhone_error,
					maxlength:guestPhone_error
				}
            },
            invalidHandler: function() {
                if (typeof googleAnalyticsTagManager.loginFormGtmError === "function") {
                    googleAnalyticsTagManager.loginFormGtmError();
                }
            }
        });
    }
    
    $('.page-cartPage').on('click', '.guestCableticaCheckoutButton', function(event) {
        guestLoginValidate();       
    });

    function guestLoginValidate(){
        if($("#guestCheckout").valid()){
        	$("#checkoutOverlay").css('display','block');
        	$(".formOverlay").fadeIn(300);
            
            var guestEmail = $("#guestEmail").val();
            var mobilePhone = $("#mobilePhone").val();
            var documentType = $("#documentType option:selected").val();
            var documentNumber = $("#documentNumber").val();
            var redirectUrl = $('#redirectUrl').val();

            if(mobilePhone==undefined || documentNumber==undefined){
             mobilePhone="";
             documentNumber="";
            }

            var timeoutCall;

            $.ajax({
	                url: ACC.config.encodedContextPath + '/login/checkout/guest',
	                async: true,
	                type: "POST",
	                dataType: "html",
	                data: {
	                    guestEmail: guestEmail,
	                    mobilePhone: mobilePhone,
	                    documentType: documentType,
	                    documentNumber: documentNumber
	                },
	                beforeSend: function(){
					    timeoutCall = setTimeout(function(){
					         console.log("Timeout");
					         //if the api takes more than 60seconds then show the fallout senerio popup
		            	  	 checkoutResponseDialog(redirectUrl);
		            	  	 var url = ACC.config.encodedContextPath + "/noservicable-c2cRequest";
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    success: function(data, xhr) {
                                        console.log("Click to call Triggered ");
                                    },
                                    error: function(result) {
                                        console.log("Error in call");
                                    }
                             });

					    }, 60000);
				   	},
	               	success:function(data) {
	               		  clearTimeout(timeoutCall);
		                  
		            	  //console.log("DATA: "+data);
		            	  if( data.includes("false") ) {
		            	  	  //if response is false displaying the fallout senerio popup
		            	  	  checkoutResponseDialog(redirectUrl);
		            	  }
		            	  else{
		                   	  console.log('saved successfully');
		                   	  //redirecting to checkout page
		                   	  window.location=ACC.config.encodedContextPath + '/checkout';
		               	   }
	               	},
	               	error:function(data,status,xhr) {
		                  console.log('error occured');
		                  clearTimeout(timeoutCall);
		                  //displaying the fallout senerio popup in case of api error or server timeout
		                  checkoutResponseDialog(redirectUrl);

	                }
	            }).done(function() {
				    setTimeout(function(){
				        $("#checkoutOverlay").css('display','none');
				        $(".formOverlay").fadeOut(300);
				    },500);
	    		});
    	}
    }

    //Function to open the fallout senerio popup on cart page
    function checkoutResponseDialog(redirectUrl){
    	ACC.colorbox.open('', {
			href: "#callResponseFormId",
			inline:true,
			scrolling:true,         
            width:"50%",
            overlayClose: false,    
			onComplete: function(){
				 $("#cboxClose").addClass("customInfoPopupCloseBtn");
				 
				 $('.callResponseBtn').on('click', function(event) {
                    ACC.colorbox.close();
                    window.location.replace(redirectUrl);
                });
			}
		});
		//hiding the loader in the background
		$("#checkoutOverlay").css('display','none');
        $(".formOverlay").fadeOut(300);

    }

	$(".docType").on('change', function () {
		var documentNumber_national_error = $("#documentNumber_national_error").val();
		var documentNumber_resident_error = $("#documentNumber_resident_error").val();
		var documentNumber_passport_error = $("#documentNumber_passport_error").val();
		var idType = $('.docType').val();
		$("input[name='documentNumber']").val('');

		if(idType === 'CEDULANATIONAL'){
			$("input[name='documentNumber']").attr('maxlength','10');
			$("input[name='documentNumber']").rules('add', {
                required: true,
				regx:/^(0)([0-9]{9})/,
				minlength:10,
				maxlength:10,
				messages: {
					required: documentNumber_national_error,
					regx: documentNumber_national_error,
					minlength: documentNumber_national_error,
					maxlength: documentNumber_national_error,
				}
            });
			$('#documentNumber-error').text(documentNumber_national_error);
			$('.guest-note').show();
			$("input[name='documentNumber']").val('0');
		} else if(idType === 'CEDULARESIDENTIAL'){
			$("input[name='documentNumber']").attr('maxlength','12');
			$("input[name='documentNumber']").rules('add', {
                required: true,
				regx:/^(1)([0-9]{10,11})/,
				minlength:11,
				maxlength:12,
				messages: {
					required: documentNumber_resident_error,
					regx: documentNumber_resident_error,
					minlength: documentNumber_resident_error,
					maxlength: documentNumber_resident_error,
				}
            });
			$('#documentNumber-error').text(documentNumber_resident_error);
			$('.guest-note').hide();
			$("input[name='documentNumber']").val('1');
		} else{
			$("input[name='documentNumber']").removeAttr('maxlength');
			$("input[name='documentNumber']").rules('add', {
                required: true,
				minlength: 8,
				maxlength: 10,
				regx:/^[a-zA-Z0-9]+$/,
				messages: {
					required: documentNumber_passport_error,
					minlength: documentNumber_passport_error,
					maxlength: documentNumber_passport_error,
					regx: documentNumber_passport_error
				}
            });
			$('#documentNumber-error').text(documentNumber_passport_error);
			$('.guest-note').hide();
		}
	}).trigger("change");

/*	$("#guestCheckout").on('change',"input[name='mobilePhone'], input[name='guestEmail'],input[name='documentNumber']", function(e){
		var mobileNumber=$("#mobilePhone").val();
        var guestEmail=$("#guestEmail").val();
		var documentNumber = $('#documentNumber').val();
		var documentType = $('#documentType').val();
		if((mobileNumber != '') && (guestEmail != '') && (documentNumber != '')) {            
			if($("#guestCheckout").valid()) {
                 var timer=$("#c2cTimerOnCart").val();
                 setTimeout(function() {
                     var url = ACC.config.encodedContextPath + "/genesys/anonymous/clickToCallRequest";
                     $.ajax({
                        type: "POST",
                        data: {mobileNumber: mobileNumber, email:guestEmail, documentType:documentType, documentNumber:documentNumber},
                        url:  url,
                        success: function(data, xhr) {
                            if(data=="Success")
                             console.log( "C2C SUCCESS");
                            if(data=="Skipped")
                             console.log( "C2C Skipped");
                        },
                        error: function(result) {
                            console.log("C2C ERROR");
                        }
                    });
               }, timer);
            }
		}
   });*/

	//On change updating the condition to display number based on id type selection
	$("input[name='documentNumber']").on('keydown mousedown keypress', function(e) {
	
		var idType = $('.docType').val();
		
		if(idType === 'CEDULANATIONAL' || idType === 'CEDULARESIDENTIAL'){
	
		  	if(e.type == 'keydown' && this.selectionStart == 0 && this.selectionEnd == 0) {
		  		this.setSelectionRange(1, 1);
		   	}
			if((e.keyCode == 37 && (this.selectionStart == 1 || e.ctrlKey)) || (e.keyCode == 36) ||
		      (e.keyCode == 8 && this.value.length == 1)|| (e.keyCode == 46 && this.selectionStart == 0)) {
		      e.preventDefault();
		   }
			if(this.selectionStart == 0 &&
		      this.selectionStart != this.selectionEnd) {
		      this.setSelectionRange(1, this.selectionEnd);
		   }
		}
	});
	
});

//On load pushing the number to display based on id type selection
function selectCedulaInfo(){
	var idType = $('.docType').val();
	
	if(idType === 'CEDULANATIONAL'){
		$("input[name='documentNumber']").val('0');
	} else if(idType === 'CEDULARESIDENTIAL'){
		$("input[name='documentNumber']").val('1');
	}else {
		$("input[name='documentNumber']").val('');
	}
}

$(window).on('load', function(){
	if($('.page-multiStepCheckoutSummaryPage').length > 0 && $(".js-checkout-step.active").prop("id") == 'step2') {
		$('#addressSubmit').trigger("click");
	}
	selectCedulaInfo();
});
