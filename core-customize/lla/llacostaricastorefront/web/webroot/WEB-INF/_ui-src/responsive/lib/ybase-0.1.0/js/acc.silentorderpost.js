ACC.silentorderpost = {

	spinner: $("<img>").attr("src", ACC.config.commonResourcePath + "/images/spinner.gif"),

	bindUseDeliveryAddress: function ()
	{
		$('#useDeliveryAddress').on('change', function ()
		{
			if ($('#useDeliveryAddress').is(":checked"))
			{
				var options = {'countryIsoCode': $('#useDeliveryAddressData').data('countryisocode'), 'useDeliveryAddress': true};
				ACC.silentorderpost.enableAddressForm();
				ACC.silentorderpost.displayCreditCardAddressForm(options, ACC.silentorderpost.useDeliveryAddressSelected);
				ACC.silentorderpost.disableAddressForm();
			}
			else
			{
				ACC.silentorderpost.clearAddressForm();
				ACC.silentorderpost.enableAddressForm();
			}
		});

		if ($('#useDeliveryAddress').is(":checked"))
		{
			ACC.silentorderpost.disableAddressForm();
		}
	},

	bindUseDeliveryDataAddress: function ()
	{
		$('.useDeliveryAddress').on('change', function ()
		{
			if ($('.useDeliveryAddress').is(":checked"))
			{
				var options = {'countryIsoCode': $('#useDeliveryAddressData').data('countryisocode'), 'useDeliveryAddress': true};
				ACC.silentorderpost.enableAddressForm();
				ACC.silentorderpost.displayCreditCardAddressForm(options, ACC.silentorderpost.useDeliveryAddressSelected);
				ACC.silentorderpost.disableAddressForm();
				setTimeout(function(){ 
					var mapSelctedValue = $("#address_province_value").val();
					$(".address_province option").each(function(){
					    if($(this).text() == mapSelctedValue){
					        $(this).attr('selected', true);
					    }
					});
				 }, 2000);
			}
			else
			{
				ACC.silentorderpost.clearAddressForm();
				ACC.silentorderpost.enableAddressForm();
				$(".address_province").change(function(){
					var previousNode = $(this).val();
             		var nextNodeVal="Canton";

					$.ajax({
			             type: "GET",
			             data: {prevNode: previousNode, nextNode:nextNodeVal},
			             url: ACC.config.encodedContextPath + "/checkout/multi/delivery-address/getAddressFields",
			             contentType:'json',
			             success: function(data)
			             {
			               $(".address_canton").html("");
			                $(".address_district").html("");
			                  list = '<option value="">'+$("#selectDefaultText").val()+'</option>';
			                  $(".address_canton").append(list);
			               $(data).each(function(i,val)
			                {
			                  list = '<option value="'+val.code+'">'+val.name+'</option>';
			                  $(".address_canton").append(list);
			                 });
			
			                 $(".address_canton").find("option:first-child").prop('disabled', true);
					 $(".address_canton").val($(".address_canton").find('option').first().val());
			                 list = '<option value="">'+$("#selectDefaultText").val()+'</option>';
			                 $(".address_district").append(list);
			                 $(".address_district").find("option:first-child").prop('disabled', true);
			             }
	             	});
				});
				
				$(".address_canton").change(function(){
                   var previousNode = $(this).val();
                   var nextNodeVal="District";

                   $.ajax({
                       type: "GET",
                        data: {prevNode: previousNode, nextNode:nextNodeVal},
                       url: ACC.config.encodedContextPath + "/checkout/multi/delivery-address/getAddressFields",
                       success: function(data)
                       {
                           $(".address_district").html("");
                           list = '<option value="">'+$("#selectDefaultText").val()+'</option>';
                            $(".address_district").append(list);
                            $(data).each(function(i,val)
                             {
                               list = '<option value="'+val.code+'">'+val.name+'</option>';
                               $(".address_district").append(list);
                              });
                             $(".address_district").find("option:first-child").prop('disabled', true);
			     $(".address_district").val($(".address_district").find('option').first().val());
                             list = '<option value="">'+$("#selectDefaultText").val()+'</option>';
                       }
                   });
            	});

				$(".address_district").change(function(){
					var previousNode = $(this).val();
					var nextNodeVal="NeighbourHood";
				
					$.ajax({
						type: "GET",
						 data: {prevNode: previousNode, nextNode:nextNodeVal},
						url: ACC.config.encodedContextPath + "/checkout/multi/delivery-address/getAddressFields",
						success: function(data)
						{
							$(".address_neighbourhood").html("");
							list = '<option value="">'+$("#selectDefaultText").val()+'</option>';
							$(".address_neighbourhood").append(list);
							 $(data).each(function(i,val)
							  {
								list = '<option value="'+val.code+'">'+val.name+'</option>';
								$(".address_neighbourhood").append(list);
							   });
							  $(".address_neighbourhood").find("option:first-child").prop('disabled', true);
							  $(".address_neighbourhood").val($(".address_neighbourhood").find('option').first().val());
						}
					});
				});
			}
		});

		if ($('.useDeliveryAddress').is(":checked"))
		{
			ACC.silentorderpost.disableAddressForm();
		}
	},

	bindSubmitSilentOrderPostForm: function ()
	{
		$('.submit_silentOrderPostForm').click(function ()
		{
			ACC.common.blockFormAndShowProcessingMessage($(this));
			$('.billingAddressForm').filter(":hidden").remove();
			$('#silentOrderPostForm').submit();
		});
	},

	bindCycleFocusEvent: function ()
	{
		$('#lastInTheForm').blur(function ()
		{
			$('#silentOrderPostForm [tabindex$="10"]').focus();
		})
	},

	isEmpty: function (obj)
	{
		if (typeof obj == 'undefined' || obj === null || obj === '') return true;
		return false;
	},

	disableAddressForm: function ()
	{
		$('input[id^="address\\."]').prop('disabled', true);
		$('select[id^="address\\."]').prop('disabled', true);
	},

	enableAddressForm: function ()
	{
		$('input[id^="address\\."]').prop('disabled', false);
		$('select[id^="address\\."]').prop('disabled', false);
	},

	clearAddressForm: function ()
	{
		$('input[id^="address\\."]').val("");
		$('select[id^="address\\."]').val("");
	},

	useDeliveryAddressSelected: function ()
	{
		if ($('#useDeliveryAddress').is(":checked"))
		{
			$('#address\\.country').val($('#useDeliveryAddressData').data('countryisocode'));
			ACC.silentorderpost.disableAddressForm();
		}
		else
		{
			ACC.silentorderpost.clearAddressForm();
			ACC.silentorderpost.enableAddressForm();
		}
	},

	bindCreditCardAddressForm: function ()
	{
		$('#billingCountrySelector :input').on("change", function ()
		{
			var countrySelection = $(this).val();
			var options = {
				'countryIsoCode': countrySelection,
				'useDeliveryAddress': false
			};
			ACC.silentorderpost.displayCreditCardAddressForm(options);
		})
	},

	displayCreditCardAddressForm: function (options, callback)
	{
		$.ajax({ 
			url: ACC.config.encodedContextPath + '/checkout/multi/sop/billingaddressform',
			async: true,
			data: options,
			dataType: "html",
			beforeSend: function ()
			{
				$('#billingAddressForm').html(ACC.silentorderpost.spinner);
			}
		}).done(function (data)
				{
					$("#billingAddressForm").html(data);
					if (typeof callback == 'function')
					{
						callback.call();
					}
				});
	}
}

$(document).ready(function ()
{
	ACC.silentorderpost.bindUseDeliveryAddress();
	ACC.silentorderpost.bindUseDeliveryDataAddress();
	ACC.silentorderpost.bindSubmitSilentOrderPostForm();
	ACC.silentorderpost.bindCreditCardAddressForm();

	// check the checkbox
	$("#useDeliveryAddress").click();
});

$(window).on('load', function(){
	setTimeout(function(){ 
		var mapSelctedValue = $("#address_province_value").val();
		$(".address_province option").each(function(){
		    if($(this).text() == mapSelctedValue){
			$(this).attr('selected', true);
		    }
		});
	}, 1000);
});
