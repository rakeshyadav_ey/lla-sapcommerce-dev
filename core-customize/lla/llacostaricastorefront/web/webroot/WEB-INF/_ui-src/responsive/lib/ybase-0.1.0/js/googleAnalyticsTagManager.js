googleAnalyticsTagManager = {
		productsSelected : [],
		pageLoadGoogleAnalyticsData:function(){
			window.dataLayer = window.dataLayer || [];

            var pageName = $("#analyticsPageInfo").attr('data-attr-pagename');
            if(pageName == '' || pageName == undefined){ pageName = ''; }

            var breadCrumbs = $("#analyticsPageInfo").attr('data-attr-breadcrumbsdata');
            if($('body').hasClass('page-orderConfirmationPage')){
                breadCrumbs = $("#analyticsPageInfo").attr('data-attr-pageName');
            }
            if(breadCrumbs == '' || breadCrumbs == undefined){ breadCrumbs = ''; } else { breadCrumbs = breadCrumbs.split(","); }
            var breadCrumbsdata = [];
            $(".breadcrumb li").each(function() {
            	var data = '';
            	if($(this).find('a').length) {
            		data = $(this).find('a').text();
            		}
            	else{
            		data = $(this).text();
            		}
            	breadCrumbsdata.push({ data });
            });

           var primaryCategory = $("#analyticsPageInfo").attr('data-attr-pagename');
           if(primaryCategory == '' || primaryCategory == undefined){ primaryCategory = ''; }

           var subCategory = "";
           if(primaryCategory == 'PLP') {
        	   primaryCategory = 'Fixed';
               breadCrumbs[breadCrumbs.length - 1] = breadCrumbsdata[breadCrumbsdata.length - 1].data;
               subCategory = $(".js-tab-head.active").attr('data-content-id').replace(/([a-zA-Z])(?=[A-Z])/g, '$1-');
               subCategory = subCategory.includes("internetcabletica") ? '1P-Internet' :  subCategory.includes("tvcabletica") ? "1P-TV" : subCategory.includes("3P-Internet-Tv-Telephone") ? "3P-Internet-TV-Telephone" : subCategory.includes("2P-Internet-Tv") ? "2P-Internet-TV" : subCategory;
           }

           if($('body').hasClass('page-cartPage') || $('body').hasClass('page-multiStepCheckoutSummaryPage') || $('body').hasClass('page-orderConfirmationPage')){
        	   primaryCategory = $("#analyticsPageInfo").attr('data-attr-pagename');
           }

           if(pageName == 'PLP'){
               pageName = window.location.pathname+"/"+breadCrumbsdata[breadCrumbsdata.length - 1].data;
           }else {
               pageName = window.location.pathname;
           }

        var statusLogged = 'Logged';
        var mail = "true";
        var currentCustomer = '';
        var withDebts = '';
        var profileID = $("#analyticsPageInfo").attr('data-attr-userid');
        if(profileID == '' || profileID == undefined){ profileID = ''; statusLogged = 'Not Logged'; mail = "false";}

        var encryptedEmail1 = '';
        var email = $("#analyticsPageInfo").attr('data-attr-email');
        if(email != '' && email != undefined && email != 'anonymous'){ encryptedEmail1 = sha256(email); }

        var pageType = '';
        if($('body').hasClass('pageLabel-residential')){
            pageType = 'PLP';
        }else if($('body').hasClass('page-cartPage')) {
            pageType = 'Cart';
        }else if ($('body').hasClass('page-multiStepCheckoutSummaryPage')){
            pageType = 'Checkout';
        }else if ($('body').hasClass('page-orderConfirmationPage')){
            pageType = 'Purchase';
        }

        var userType = $("#analyticsPageInfo").attr('data-attr-customerType');
        if(userType == '' || userType == undefined){ userType = '';}

        var phone = $("#analyticsPageInfo").attr('data-attr-mobilephone');
        var encryptedPhone = '';
        if(phone != '' && phone != undefined){ encryptedPhone = sha256(phone); }

        var pageInfo = {} , categoryInfo={}, profileInfo={}, socialInfo={};
        pageInfo.pageName= pageName;//'business name of the page', //Dynamic value
        pageInfo.breadCrumbs= breadCrumbs;//['Online Store', 'Mobile Postpaid Plans'], //Dynamic Value
        pageInfo.pageType= pageType;//'type of page from the business', //Dynamic Value
        pageInfo.country= 'Costa Rica';//static value
        pageInfo.channel= 'Ecommerce';//static value
        pageInfo.productPage= '';//dynamic value, but this will be product name in PDP, remaining pages will be NA

        categoryInfo.businessCategory='Residential';
        categoryInfo.primaryCategory= primaryCategory;//'main category from the products', //Dynamic Value
        categoryInfo.subCategory=subCategory;//'bundle category' //Dynamic Value

        profileInfo.profileID=profileID;//'encripted business id for the user', //Dynamic Value
        profileInfo.email=encryptedEmail1;//'encripted email SHA256', //Dynamic Value
        profileInfo.phone=encryptedPhone;//'encripted phone number SHA256' //Dynamic Value
        socialInfo.mail=mail;//'boolean', //Dynamic Value
        socialInfo.facebook='false';//boolean', //Dynamic Value
        socialInfo.other='false';//'boolean' //Dynamic Value

        if($("body").hasClass('page-multiStepCheckoutSummaryPage') || $("body").hasClass('page-orderConfirmationPage')){
            //If user comes to order checkout page or order confirmation page then currentcustomer is false and withDebts is true
            currentCustomer = 'false'; withDebts = 'true';
        }


        if(!($("body").hasClass('page-orderConfirmationPage'))){//for every page load, except order confirmation page
        	 window.dataLayer = window.dataLayer || [];
        	 window.dataLayer.push({
             	'event': 'vpv',
             	'page': pageInfo,
             	'category':categoryInfo,
             	'user':{
                    'statusLogged': statusLogged,//'logged status from the user', //Dynamic Value
                    'userType': userType,//'guest or client', //Dynamic Value
                    'profileInfo': profileInfo,
                    'social': socialInfo,
                    'validation': {
                        'currentCustomer': currentCustomer, //Dynamic Value
                        'withDebts': withDebts //Dynamic Value
                    },
             	}
             });
        }

        if($("body").hasClass('page-multiStepCheckoutSummaryPage')){
            var checkout = [];
            var step = $(".js-checkout-step.active").prop("id");
            var stepInfo = '', option='';
            if(step == 'step1') {
                stepInfo = 1;
                option="Personal Information";
            }else if(step == 'step2') {
                stepInfo = 2;
                option="Address";
            }else if(step == 'step3') {
                stepInfo = 3;
                option="Payment";
            }else if(step == 'step4') {
                stepInfo = 4;
                option="Detail";
            }else{
                stepInfo = '';
            }
            if(stepInfo != ''){
                checkout.step = stepInfo; //dynamic value
                var orderedProducts = [];
                $('.productGtmData').each(function(){
                    var _this=$(this);
                    selectedProducts(_this);
                });

            	window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({
                	'event': 'checkout',
                	  'ecommerce': {
                		  'checkout': {
                			  'actionField': {'step': stepInfo, 'option': option},
                			  'products': googleAnalyticsTagManager.productsSelected
                			  }
                         }
                    });
            }
        }

        if($("body").hasClass('page-orderConfirmationPage')){
            $('.orderedProducts').each(function(){
                var _this=$(this);
                selectedProducts(_this);
            });

            var transaction = {}, address = {} , currency='';
            transaction.id =  $("#addressInfo").attr('data-attr-order-id'); //dynamic value//Order Id
            transaction.affiliation =  "Costa Rica Ecommerce"; //static value
            transaction.revenue=0;
            transaction.tax=0;
            transaction.coupon=0;
            transaction.shipping=0;

            if(typeof $("#cartPagePriceInfo").attr('data-attr-currency') !== 'undefined') {
            	currency = $("#cartPagePriceInfo").attr('data-attr-currency');
            }

            if(typeof $("#cartPagePriceInfo").attr('data-attr-savings') !== 'undefined') {
            	transaction.coupon = Number($("#cartPagePriceInfo").attr('data-attr-savings').replace(/[^0-9\.]+/g, ""));
            }

            var taxRate911 = 0;
            if(typeof $("#cartPagePriceInfo").attr('data-attr-taxRate911') !== 'undefined') {
                taxRate911 = $("#cartPagePriceInfo").attr('data-attr-taxRate911').replace(/[^0-9\.]+/g, "");
            }

            var taxRate13 = 0;
            if(typeof $("#cartPagePriceInfo").attr('data-attr-taxRate13') !== 'undefined') {
                taxRate13 = $("#cartPagePriceInfo").attr('data-attr-taxRate13').replace(/[^0-9\.]+/g, "");
            }
            transaction.tax = Number(taxRate911)+Number(taxRate13); //dynamic value

            if(typeof $("#cartPagePriceInfo").attr('data-attr-cartTotal') !== 'undefined') {
            	transaction.revenue = Number($("#cartPagePriceInfo").attr('data-attr-cartTotal').replace(/[^0-9\.]+/g, ""));
            }

            address.province = $("#addressInfo").attr('data-attr-province'); //dynamic value
            address.district = $("#addressInfo").attr('data-attr-district'); //dynamic value
            address.town = $("#addressInfo").attr('data-attr-town'); //dynamic value
            address.neighbourhood = $("#addressInfo").attr('data-attr-neighbourhood'); //dynamic value
            address.street = $("#addressInfo").attr('data-attr-street'); //dynamic value
            address.country = $("#addressInfo").attr('data-attr-country'); //dynamic value

            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            	'event': 'vpv',
            	'page': pageInfo,
            	'category':categoryInfo,
            	'user':{
            	'statusLogged': statusLogged,//'logged status from the user', //Dynamic Value
            	'userType': userType,//'guest or client', //Dynamic Value
            	'profileInfo': profileInfo,
            	'social': socialInfo
            	      },
                'validation': {
                    'currentCustomer': currentCustomer, //Dynamic Value
                    'withDebts': withDebts //Dynamic Value
                },
            	    'address': address,
            	     'ecommerce': {
            	    	 'currencyCode': currency, //Dynamic Value Ex. USD
            	    	 'purchase': {
            	    		 'actionField': transaction,
            	    		 'products': googleAnalyticsTagManager.productsSelected
            	    		 }
            	          }
            	          });
            }
        /* Page not Found Code Starts*/
        if($("body").hasClass('page-notFound')){
            var errorMsg = $("#gtmNotFoundPageError").val();
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                'event': 'error',
                'description': errorMsg
            });
        }
        /* Page not Found Code Ends*/
        /* On page load global error Starts*/
        var errorGlobalAlert = 0;
        var errorMsg = '', errorMsgAll = '';
        $(".global-alerts .alert-danger").each(function (){
            errorGlobalAlert++;
            errorMsg = $(this).text();
            errorMsg = errorMsg.replace( /[\r\n\t]+/gm, "" ).substring(1);
            errorMsgAll = errorMsgAll+ ' ' +errorMsg;
        });
        if(errorGlobalAlert > 0){
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                'event': 'error',
                'description': errorMsgAll
            });
        }
        /* On page load global error Ends*/
   },
   loginFormGtmError: function (){
       window.dataLayer = window.dataLayer || [];
       window.dataLayer.push({
           'event': 'error',
           'description': 'User entered invalid email address or phone number or identification(cedula national or cedula residential or passport) number on cart page login form'
       });
   },
   handleGtmErrors: function (errorMsg){
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
            'event': 'error',
            'description': errorMsg
        });
   }
}

$(window).on('load', function(){
    let gtmCallInterval = setInterval(function(){
        // when window.google_tag_manager is defined and true, stop the function
        if (!!window.google_tag_manager) {
            clearInterval(gtmCallInterval);
            googleAnalyticsTagManager.pageLoadGoogleAnalyticsData();
        }
    }, 1000);
});

$(document).ready(function(){

	$(document).on("click", ".js-tab-head", function (e) {
        setTimeout(function(){
        	googleAnalyticsTagManager.pageLoadGoogleAnalyticsData();
        }, 1000);
    });

// PLP Add to Cart
    /*$(document).on("click", ".divPricenButtons #addonAddtoCart .ctabox-addToCart-btn", function(e){
        var selectedItem = $(this).parent().parent().parent().find('.pageProducts');
        selectedProducts(selectedItem);
        var currency = selectedItem.attr('data-attr-currency');

        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
        	'event': 'addToCart',
        	'ecommerce': {
            	'currencyCode': currency,
            	'add': { // 'add' actionFieldObjectmeasures.
            		'products': googleAnalyticsTagManager.productsSelected
                       }
                     }
        });
      });*/

    //Remove from cart-eliminar-plp
    $(document).on("click", ".divPricenButtons .ctabox-removefromcart-form .ctabox-removeFromCart-btn", function(e){
        var removedItem = $(this).parent().parent().parent().find('.pageProducts');
        selectedProducts(removedItem);
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
        	'event': 'removeFromCart',
        	'ecommerce': {
            	'remove': { // 'add' actionFieldObjectmeasures.
            		'products': googleAnalyticsTagManager.productsSelected
                       }
                     }
        });
    });

    //Remove from Cart-cartpage
    $(document).on("click", ".deleteForm .submitRemoveBundle", function(e){
        $(".cartProductsdata").each(function(){
            var _this= $(this);
            selectedProducts(_this);
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            	'event': 'removeFromCart',
            	'ecommerce': {
                	'remove': { // 'add' actionFieldObjectmeasures.
                		'products': googleAnalyticsTagManager.productsSelected
                           }
                         }
            });
        });
    });

  //Addons Add
    //Addons add -top
    $(document).on("click", ".item__addCart #addonAddtoCart .ctabox-addToCart-btn", function(e){
    	 var selectedItem = $(this).parent().parent().parent().find('.addonTelephone');
         addAddonInfoData(selectedItem);
    });
    //Checkbox addon
    $(".ctabox-addtocart-form .addOnCheckBox").change(function(){
        var selectedItem = $(this).parent().parent().parent().parent().find('.channelAddonData');
        addAddonInfoData(selectedItem);
    });
    //Qty Addon
    $('.addonqtyplus, .qtyplus').on("click", function (e) {
        var selectedItem = $(this).parent().parent().parent().parent().find('.hdChannelAddonData');
        selectedItem[0].attributes['data-attr-quantity'].value=$(this).parent().find('.item__quantity.form-control')== undefined ? 1 : $(this).parent().find('.item__quantity.form-control').val();
        addAddonInfoData(selectedItem);
    });

   //Remove Addon
    //Remove addon-top
    $(document).on("click", ".deleteForm.addondelForm .js-cartItemDetailBtn", function(e){
    	 var selectedItem = $('.addonTelephone');
         removeAddonInfoData(selectedItem);
    });
    $('.addonqtyminus, .qtyminus').on("click", function (e) {
        var selectedItem = $(this).parent().parent().parent().parent().find('.hdChannelAddonData');
        selectedItem[0].attributes['data-attr-quantity'].value=$(this).parent().find('.item__quantity.form-control')== undefined ? 1 : $(this).parent().find('.item__quantity.form-control').val();
        removeAddonInfoData(selectedItem);
     });

    $(".addOnCheckBoxdata").change(function(){
        var selectedItem = $(this).parent().parent().parent().parent().find('.channelAddonData');
        removeAddonInfoData(selectedItem);
    });

  });

function addAddonInfoData(selectedItem){
    var currency = selectedItem.attr('data-attr-currency');
    selectedProducts(selectedItem);
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
    	'event': 'addToCart',
    	'ecommerce': {
        	'currencyCode': currency,
        	'add': { // 'add' actionFieldObjectmeasures.
        		'products': googleAnalyticsTagManager.productsSelected
                   }
                     }
    });
}
function removeAddonInfoData(selectedItem){
	selectedProducts(selectedItem);
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
    	'event': 'removeFromCart',
    	'ecommerce': {
        	'remove': { // 'add' actionFieldObjectmeasures.
        		'products': googleAnalyticsTagManager.productsSelected
                   }
                     }
    });
}

function selectedProducts(selectedItem){
	    var addonInfo = {};
	    var primaryCategory = selectedItem.attr('data-attr-primarycategory') || selectedItem.attr('data-attr-category_2');
	    if(primaryCategory == '' || primaryCategory == undefined) { primaryCategory = ''; }
	    var subcategory = selectedItem.attr('data-attr-category');
	    if(subcategory == '' || subcategory == undefined) { subcategory = ''; }
	    addonInfo.name = selectedItem.attr('data-attr-name');
	    primaryCategory = primaryCategory == 'Addon' ? 'SVA':'Fixed';
	    //Sub Category
	    switch (true) {//PLP
        case subcategory.includes('3PInternetTvTelephone') || subcategory.includes('Triple'):
        	subcategory = '3P-Internet-TV-Telephone';
            break;
        case subcategory.includes('2PInternetTv') || subcategory.includes('Doble'):
            subcategory = '2P-Internet-TV';
            break;
        case subcategory.includes('internetcabletica') || subcategory.includes('Internet'):
        	subcategory = '1P-Internet';
            break;
        case subcategory.includes('tvcabletica') || subcategory.includes('Television'):
        	subcategory = '1P-TV';
            break;
        case subcategory.includes('Addon') || subcategory.includes('addon'):
        	subcategory = 'AdditionalChannels';
            primaryCategory = 'SVA';
            break;
        default:
        	subcategory = subcategory;
           }
	    addonInfo.id = selectedItem.attr('data-attr-id');
	    addonInfo.price =  Number(selectedItem.attr('data-attr-price').replace(/[^0-9\.]+/g, ""));
	    addonInfo.brand = 'Cabletica';
	    addonInfo.category = 'Residential'+'/'+primaryCategory+'/'+subcategory;
	    addonInfo.variant = '';
	    addonInfo.quantity = Number(selectedItem.attr('data-attr-quantity') == undefined ? 1 : selectedItem.attr('data-attr-quantity')) ;
	    googleAnalyticsTagManager.productsSelected.push(addonInfo);
}

ACC.gtmTagAnalytics = {
    loadedMapPopup: function (){
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
          'event': 'modal',
          'modal': 'serviceability'
        });
    },

    addToCartCall: function(selectedItem) {
        var selectedItemData = selectedItem.parent().parent().parent().find('.pageProducts');
        selectedProducts(selectedItemData);
        var currency = selectedItemData.attr('data-attr-currency');

        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
            'event': 'addToCart',
            'ecommerce': {
                'currencyCode': currency,
                'add': { // 'add' actionFieldObjectmeasures.
                    'products': googleAnalyticsTagManager.productsSelected
                       }
                     }
        });
    },

    c2cModelOpen: function(selectedItem) {
        var selectedItemData = selectedItem.parent().parent().parent().find('.pageProducts');
        selectedProducts(selectedItemData);
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
            'event': 'checkoutLead',
            'ecommerce': {
                'checkout': {
                    'actionField': {'step': 5, 'option': 'Lead Form'},
                    'products': googleAnalyticsTagManager.productsSelected
                }
            }
        });
        googleAnalyticsTagManager.productsSelected = [];
    },

    c2cSuccessCall: function(selectedItem, successInfo) {
        var selectedItemData = selectedItem.parent().parent().parent().find('.pageProducts');
        selectedProducts(selectedItemData);
        var currency = selectedItemData.attr('data-attr-currency');
        successInfo = JSON.parse(successInfo);
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
            'event': 'lead',
            'ecommerce': {
                'currencyCode': currency,
                'purchase': {
                    'actionField': {
                        'id': successInfo.identityNumber, //Dynamic Value from Genesys
                        'affiliation': "Costa Rica Ecommerce", //Dynamic Value Ex. Costa Rica Ecommerce
                        'revenue': googleAnalyticsTagManager.productsSelected[0].price, //Dynamic Value Ex. 10.90
                        'tax':'', //Empty
                        'shipping': '', //Empty
                        'coupon': '' //Empty
                    },
                    'products': googleAnalyticsTagManager.productsSelected
                }
            }
        });
        googleAnalyticsTagManager.productsSelected = [];
    }
};


function sha256(ascii) {
	function rightRotate(value, amount) {
		return (value>>>amount) | (value<<(32 - amount));
	};

	var mathPow = Math.pow;
	var maxWord = mathPow(2, 32);
	var lengthProperty = 'length'
	var i, j; // Used as a counter across the whole file
	var result = ''

	var words = [];
	var asciiBitLength = ascii[lengthProperty]*8;

	//* caching results is optional - remove/add slash from front of this line to toggle
	// Initial hash value: first 32 bits of the fractional parts of the square roots of the first 8 primes
	// (we actually calculate the first 64, but extra values are just ignored)
	var hash = sha256.h = sha256.h || [];
	// Round constants: first 32 bits of the fractional parts of the cube roots of the first 64 primes
	var k = sha256.k = sha256.k || [];
	var primeCounter = k[lengthProperty];

	var isComposite = {};
	for (var candidate = 2; primeCounter < 64; candidate++) {
		if (!isComposite[candidate]) {
			for (i = 0; i < 313; i += candidate) {
				isComposite[i] = candidate;
			}
			hash[primeCounter] = (mathPow(candidate, .5)*maxWord)|0;
			k[primeCounter++] = (mathPow(candidate, 1/3)*maxWord)|0;
		}
	}

	ascii += '\x80' // Append Ƈ' bit (plus zero padding)
	while (ascii[lengthProperty]%64 - 56) ascii += '\x00' // More zero padding
	for (i = 0; i < ascii[lengthProperty]; i++) {
		j = ascii.charCodeAt(i);
		if (j>>8) return; // ASCII check: only accept characters in range 0-255
		words[i>>2] |= j << ((3 - i)%4)*8;
	}
	words[words[lengthProperty]] = ((asciiBitLength/maxWord)|0);
	words[words[lengthProperty]] = (asciiBitLength)

	// process each chunk
	for (j = 0; j < words[lengthProperty];) {
		var w = words.slice(j, j += 16); // The message is expanded into 64 words as part of the iteration
		var oldHash = hash;
		// This is now the undefinedworking hash", often labelled as variables a...g
		// (we have to truncate as well, otherwise extra entries at the end accumulate
		hash = hash.slice(0, 8);

		for (i = 0; i < 64; i++) {
			var i2 = i + j;
			// Expand the message into 64 words
			// Used below if
			var w15 = w[i - 15], w2 = w[i - 2];

			// Iterate
			var a = hash[0], e = hash[4];
			var temp1 = hash[7]
				+ (rightRotate(e, 6) ^ rightRotate(e, 11) ^ rightRotate(e, 25)) // S1
				+ ((e&hash[5])^((~e)&hash[6])) // ch
				+ k[i]
				// Expand the message schedule if needed
				+ (w[i] = (i < 16) ? w[i] : (
						w[i - 16]
						+ (rightRotate(w15, 7) ^ rightRotate(w15, 18) ^ (w15>>>3)) // s0
						+ w[i - 7]
						+ (rightRotate(w2, 17) ^ rightRotate(w2, 19) ^ (w2>>>10)) // s1
					)|0
				);
			// This is only used once, so *could* be moved below, but it only saves 4 bytes and makes things unreadble
			var temp2 = (rightRotate(a, 2) ^ rightRotate(a, 13) ^ rightRotate(a, 22)) // S0
				+ ((a&hash[1])^(a&hash[2])^(hash[1]&hash[2])); // maj

			hash = [(temp1 + temp2)|0].concat(hash); // We don't bother trimming off the extra ones, they're harmless as long as we're truncating when we do the slice()
			hash[4] = (hash[4] + temp1)|0;
		}

		for (i = 0; i < 8; i++) {
			hash[i] = (hash[i] + oldHash[i])|0;
		}
	}

	for (i = 0; i < 8; i++) {
		for (j = 3; j + 1; j--) {
			var b = (hash[i]>>(j*8))&255;
			result += ((b < 16) ? 0 : '') + b.toString(16);
		}
	}
	return result;
};
