ACC.payment = {
		
		activateSavedPaymentButton: function(){

			$(document).on("click",".js-saved-payments",function(e){
				e.preventDefault();
				
				var titleHtml = $("#savedpaymentstitle").html();
				
				$.colorbox({
					href: "#savedpaymentsbody",
					inline:true,
					maxWidth:"100%",
					opacity:0.7,
					width:"320px",
					title: titleHtml,
					close:'<span class="glyphicon glyphicon-remove"></span>',
					onComplete: function(){
					}
				});
			})
		},
		bindPaymentCardTypeSelect: function ()
		{
			ACC.payment.filterCardInformationDisplayed();
			$("#card_cardType").change(function ()
			{
				var cardType = $(this).val();
				if (cardType == '024')
				{
					$('#startDate, #issueNum').show();
				}
				else
				{
					$('#startDate, #issueNum').hide();
				}
			});
		},
		filterCardInformationDisplayed: function ()
		{
			var cardType = $('#card_cardType').val();
			if (cardType == '024')
			{
				$('#startDate, #issueNum').show();
			}
			else
			{
				$('#startDate, #issueNum').hide();
			}
		}
}

$(document).ready(function () {
		ACC.payment.activateSavedPaymentButton();
		ACC.payment.bindPaymentCardTypeSelect();
		//payment method validation
    	$('#card_accountNumber').attr({'minlength':'15','maxlength':'19'});
    	$('#card_accountNumber, #card_cvNumber').on('keyup keypress',function(e){
    		const pattern = /^[0-9]$/;
            return pattern.test(e.key)
    	});

    	$('#card_expiryMonthYear').attr({'minlength':'6','maxlength':'7'});
		
		$('#card_expiryMonthYear').on('keyup keypress',function(e){
			const pattern = /^[0-9/]*$/;
    		return pattern.test(e.key);
		});

    	$('#card_expiryMonthYear').on('keyup keypress',function(e){
          $(this).val($(this).val().replace(/^(\d{2})(\d{4})+$/, "$1\/$2"));
          var card_expiryMonthYear=$(this).val();
          var arr=card_expiryMonthYear.split("/")
          $('#card_expirationMonth').val(arr[0]);
          $('#card_expirationYear').val(arr[1]);
        });

        $('#card_expirationMonth').on('ready',function(e){
	        const pattern = /^(0?[1-9]|1[012])$/;
	        return pattern.test($('#card_expirationMonth').val());
        });

      	if ($("#silentOrderPostForm_error").length) {
			var card_nameOnCard_error = $("#card_nameOnCard_error").val();
    		var card_cardType_error = $("#card_cardType_error").val();
    		var card_accountNumber_error = $("#card_accountNumber_error").val();
			var card_accountNumber_valid_error = $("#card_accountNumber_valid_error").val();
    		var card_cvNumber_error = $("#card_cvNumber_error").val();
    		var card_expirationMonthYear_error = $("#card_expirationMonthYear_error").val();
			var card_expirationMonthYear_valid_error = $("#card_expirationMonthYear_valid_error").val();
			var province_error = $("#province_error").val();
			var canton_error = $("#canton_error").val();
			var district_error = $("#district_error").val();
			var neighbourhood_error = $("#neighbourhood_error").val();
			var landMark_error = $("#landMark_error").val();
    	}
    	var invalidHandlerCount = 2;
    	$("#silentOrderPostForm").validate({
    		rules: {
    			"card_expiryMonthYear": {
                     required: true,
					 minlength:7
                },
				"card_nameOnCard": {
					required: true
				},
               "card_accountNumber": {
    				required: true,
					minlength:18,
					maxlength:19
    			},
    			"card_cvNumber": {
    				required: true
    			},
				"billTo_province": {
					required: true
				},
				"billTo_building": {
					required: true
				},
				"billTo_district": {
					required: true
				},
				"billTo_neighbourhood": {
					required: true
				},
				"billTo_landMark": {
					required: true
				}
    		},
    		messages: {
				card_nameOnCard: card_nameOnCard_error,
    			card_cardType: card_cardType_error,
    			card_accountNumber:{
					required: card_accountNumber_error,
					minlength:card_accountNumber_valid_error,
					maxlength:card_accountNumber_valid_error
				},
    			card_cvNumber: {
					required: card_cvNumber_error
				},
    			card_expiryMonthYear:{
					required: card_expirationMonthYear_error,
					minlength:card_expirationMonthYear_valid_error
				},
				billTo_province: province_error,
				billTo_building: canton_error,
				billTo_district: district_error,
				billTo_neighbourhood: neighbourhood_error,
				billTo_landMark: landMark_error
    		},
    		invalidHandler: function() {
                if((invalidHandlerCount % 2) == 0) {
                     var errorMsg = "User tries to continue checkout flow without filling all the required fields in checkout payment form";
                     if (typeof googleAnalyticsTagManager.handleGtmErrors === "function") {
                         googleAnalyticsTagManager.handleGtmErrors(errorMsg);
                     }
                 }
                 invalidHandlerCount++;
            }
    	});

	//credit card number validation	
	$('#card_accountNumber').on('keypress change blur', function () {
	  $(this).val(function (index, value) {
	    return value.replace(/[^0-9]+/gi, '').replace(/(.{4})/g, '$1 ').trim();
	  });
	  $('#card_cvNumber').val('');
	});
	
	$('#card_accountNumber').on('copy cut paste', function () {
	  setTimeout(function () {
	    $('#card_accountNumber').trigger("change");
	  });
	});

	function getCCLabel(ccNumberMask) {
	
	    var creditCardsRegex = {
	        visa: /^4[0-9]{12}(?:[0-9]{3})?/,
	        master: /^5[1-5][0-9]{4}/,
//	        amex: /^3[47][0-9]{4}/,
	        //discover: /^6(?:011|5[0-9]{2})[0-9]{4}/,
	        //maestro: /^(5018|5081|5044|5020|5038|603845|6304|6759|676[1-3]|6799|6220|504834|504817|504645)[0-9]{8,15}$/
	    },
	        creditCards = [
	            'visa',
	            'master',
//	            'amex',
	            //'discover',
	            //'maestro'   
	        ],
	        regexes = [],
	        ccNumber = ccNumberMask.replace(/\s+|-/g, '');
	
	    creditCards.map((cc) => {
	        var ccRegex = {
	            regex: creditCardsRegex[cc],
	            label: cc
	        };
	        regexes.push(ccRegex);
	    });
	
	    for (var i = 0; i < regexes.length; i++) {
	        if (regexes[i].regex.test(ccNumber)) return regexes[i].label;
	    }
	    return false;
	}
	
	(function ($) {
		var card_cvNumber_error = $("#card_cvNumber_error").val();
	    $.cccheck = function cccheck(settings) {
	        var config = {
	            // Config local
	            'inputCCNumber': '#card_accountNumber',
	            'elementShowLabel': '#show-cc-label'
	        },
	        nodeNumber = $(config.inputCCNumber),
	        nodeLabel = $(config.elementShowLabel);
	
	        if (settings) { 
	            settings = jQuery.extend(config, settings); 
	        }
	
	        nodeNumber.keyup(function (e) {
	            var cardLabel = getCCLabel(nodeNumber.val());
	            nodeLabel.html('<i class="i-icon-payment i-icon-card-' + cardLabel + '"></i>');
				$('#card_cardType').val(cardLabel);
				$('#card_cvNumber').attr({'maxlength':'3'});
				$("input[name='card_cvNumber']").rules('add', {
					minlength:3,
					messages: {
						minlength: card_cvNumber_error
					}
	            });
	        });
	
	        return this;
	    };
	})(jQuery);

	$.cccheck();

	$('#card_accountNumber').on('keyup change', function () {
	  $(this).val(function (index, value) {
	    return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ').trim();
	  });
	});

});

//clear the Payment input values on PageLoad
$(window).on('load', function(){
	$('#card_accountNumber, #card_expiryMonthYear, #card_cvNumber').val('');
});
