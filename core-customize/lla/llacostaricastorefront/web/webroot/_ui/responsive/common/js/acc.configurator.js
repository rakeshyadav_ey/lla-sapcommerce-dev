
// passing value to backend if the addon is selected
if($('.step1ConfigForm').length) {
	var selectedAddon = $("#js-selectedAddon").val();
	if(selectedAddon) {
		$(".ctabox.selected").find('.form-group input[type="checkbox"]').prop('checked');
	}
	$('.step1ConfigForm').submit(function() {
		var checkboxId = $(this).parent().find('.form-group input[type="checkbox"]').attr("id");
		if ($("#"+checkboxId).prop('checked')==true){
			var selectedCheckboxId = $("#"+checkboxId).val();
			$(this).find("input[name='selectedAddon']").val(selectedCheckboxId);
		}
		else {
			$(this).find("input[name='selectedAddon']").val("");
		}
		$(this).parents('.ctabox').addClass('selected');
		return true; // return false to cancel form action
	});	
}

if($('.step2ConfigForm').length) {
	var twoPAddonHD = $("#js-twoPAddonHD").val();
	if(twoPAddonHD == "true") {
		$(".ctabox.selected").find('.form-group input[id^="HD_BOX"]').prop('checked');
	}
	var twoPAddonDVR = $("#js-twoPAddonDVR").val();
	if(twoPAddonDVR == "true") {
		$(".ctabox.selected").find('.form-group input[id^="DVR_BOX"]').prop('checked');
	}
	var twoPHUB = $("#js-twoPHUB").val();
	if(twoPHUB == "true") {
		$(".ctabox.selected").find('.form-group input[id^="HUB"]').prop('checked');
	}
	$('.step2ConfigForm').submit(function() {
		var selectedCheckboxIds = [];
		$(this).find('input[type=checkbox]').each(function (index, element) {
			if(element.checked) {
				selectedCheckboxIds.push($(this).val());
			}
		});
		$(this).find("input[name='selecteddAddons']").val(selectedCheckboxIds);
		$(this).parents('.ctabox').addClass('selected');
		return true; // return false to cancel form action
	});
}

$(document).on("change", '.ctabox .form-group input[type="checkbox"]', function() {
	if($(this).prop('checked')==true) {
		$(this).parents(".ctabox").parent().siblings().find('input[type="checkbox"]').prop('checked', false); 
	}
});

if($('.active-products').length) {
	var activeProducts = [];
	$('.active-products').find('input[type=hidden]').each(function(){
		activeProducts.push($(this).val());
	})
	$('.ctabox').each(function() {
		var currentProduct = $(this).find('input[id="selectedTVProduct"]').val();
		if((activeProducts.indexOf(currentProduct) > -1)) {
			$(this).find('.disabled').remove();
		}
		else {
			$(this).find('.disabled').parent(".ctabox").removeClass("selected");
		}
	})
}

function addOnsSelection() {
	if($('.configurator-body').find('.step-4-cntnr').length > 0 ) {
		var selectedClValue='';
		var currentClValue = [];
		if($('.cl-cntnr__list').length > 0 && $('.cl-cntnr__list').data('cl-selected') != '' && $('.cl-cntnr__list').data('cl-selected') != undefined) {
       		selectedClValue = $('.cl-cntnr__list').data('cl-selected').replace(/[\[\]']+/g,' ').split(',');
   		}
		//var finalSelectedClValue = selectedClValue.replace(/\s+/g, '');
		$('.cl-cntnr__list .cl-cntnr__item ').each(function() {
			var getEachClValue = $(this).data('cl-code');
			currentClValue.push(getEachClValue);	
		});

		var fCl = currentClValue.filter(function(v){return v!=='' && v!== undefined})

		if(selectedClValue != '') {
			var cmnValue = selectedClValue.filter(val => !fCl.includes(val));
			$.map(cmnValue, function(data) {
				$('.cl-cntnr__list .cl-cntnr__item ').each(function() {
					var removeSpace = data.trim();
					var CompareEachVal = $(this).data('cl-code');
					if(removeSpace === CompareEachVal) {
						$(this).addClass('selected-cl');
					}
				});
			});
		}
	}
	//Clickimng on smart protect radio button functionality on 4th step config
	$('.smartp--js input[name="smartProducts"]').click(function() {
        if($(this).attr('previousValue') == 'true'){
			$(this).prop('checked', false)
			$('#selectedSmartProducts').val('');
        } else {
			$('input[name="smartProducts"]').attr('previousValue', false);
			let getSmartPVal = $(this).parents('.smartp--js').data('smart-protect');
			$('#selectedSmartProducts').val(getSmartPVal);
        }
        
        $(this).attr('previousValue', $(this).is(':checked'));
    });
}

$(document).ready(function(){
	addOnsSelection();
});


