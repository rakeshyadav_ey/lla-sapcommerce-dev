adobeAnalytics = {

	pageLoadAdobeAnalyticsData:function(){
	    window.digitalData = window.digitalData || {};

        var referringURL = document.referrer;
        if(referringURL == ''){ referringURL = 'NA'; }

        var primaryCategory = $("#adobeAnalyticsPageInfo").attr('data-attr-pagename');
        if(primaryCategory == '' || primaryCategory == undefined){ primaryCategory = 'NA'; }
        if(primaryCategory == 'SELECT_INTERNET_PLAN' || primaryCategory == 'SELECT_TV_PLAN' || primaryCategory == 'SELECT_PHONE' || primaryCategory == 'SELECT_ADDONS' ) {
            primaryCategory = 'Products';
        }
        var subCategory = "NA";
        if(primaryCategory == 'Products') { subCategory = 'Products'; }

        var breadCrumbs = $("#adobeAnalyticsPageInfo").attr('data-attr-breadcrumbsdata');
        if($('body').hasClass('page-orderConfirmationPage')){
            breadCrumbs = $("#adobeAnalyticsPageInfo").attr('data-attr-pageName');
        }
        if(breadCrumbs == '' || breadCrumbs == undefined){ breadCrumbs = 'N/A'; } else { breadCrumbs = breadCrumbs.split(","); }

        if($('body').hasClass('page-cartPage') || $('body').hasClass('page-multiStepCheckoutSummaryPage') || $('body').hasClass('page-orderConfirmationPage')){
            primaryCategory = $("#adobeAnalyticsPageInfo").attr('data-attr-pagename');
        }

        var profileID = $("#adobeAnalyticsPageInfo").attr('data-attr-userid');
        var email = $("#adobeAnalyticsPageInfo").attr('data-attr-email');
        if($('body').hasClass('page-productDetailsForDevices') || $('body').hasClass('page-cartPage') || $('body').hasClass('page-multiStepCheckoutSummaryPage') || $('body').hasClass('page-orderConfirmationPage')){
            profileID = $("#adobeAnalyticsPageInfo").attr('data-attr-userid');
            email = $("#adobeAnalyticsPageInfo").attr('data-attr-email');
        }
        var statusLogged = 'Logged';
        var mail = "true";
        if(profileID == '' || profileID == undefined){ profileID = 'N/A'; statusLogged = 'Not Logged'; mail = "flase"; }

        var encryptedEmail1 = 'NA';
        if(email != '' && email != undefined && email != 'anonymous'){ encryptedEmail1 = sha256(email); }

        var pageType = 'Subcategory';
        if($("#adobeAnalyticsPageInfo").attr('data-attr-pagename') == 'HOME'){ pageType = "Homepage"; }

        var productPage = "NA";
        if($('body').hasClass('page-productDetails') || $('body').hasClass('page-productDetailsForDevices')){
            productPage = $("#pdpAdobeAnalyticsData").attr('data-attr-name');
        }

        var pageName = $("#adobeAnalyticsPageInfo").attr('data-attr-pagename');
        if(pageName == '' || pageName == undefined){ pageName = 'NA'; }
        pageName = pageName.split("_").join(" ");

        var userType = $("#adobeAnalyticsPageInfo").attr('data-attr-customerType');
        if(userType == '' || userType == undefined){ userType = 'NA';}

        var phone = $("#adobeAnalyticsPageInfo").attr('data-attr-mobilephone');
        var encryptedPhone = 'NA';
        if(phone != '' && phone != undefined){ encryptedPhone = sha256(phone); }

        window.digitalData = window.digitalData || {};
        window.digitalData = {
            page: {
                pageInfo: {
                    hostName: location.hostname,
                    pageName: pageName,
                    currentURL: window.location.href,
                    referringURL: referringURL,
                    breadCrumbs: breadCrumbs,
                },
                category: {
                    primaryCategory: primaryCategory,
                    subCategory: subCategory,
                    pageType: pageType,
                    channel: "ecommerce_puerto_rico", //static value
                },
                attributes: {
                    country: "Puerto Rico", //static value
                }
            },
            user: [{
                profile: [{
                    statusLogged: statusLogged, //"Logged", //dynamic value
                    userType: userType, //"Guest", //dynamic value
                    profileInfo: {
                        profileID: profileID, //"aq123123", //dynamic value
                        email: encryptedEmail1, //"aiadifaoidfasdfsadfs", //dynamic value
                        phone: encryptedPhone, //dynamic value
                    },
                    social: {
                        facebook: "false", //dynamic value
                        mail: mail
                    }
                }]
            }],
            version: "1.0",
        }

        if($("body").hasClass('page-multiStepCheckoutSummaryPage')){
            var step = $(".js-checkout-step.active").prop("id");
            var stepInfo = '';
            if(step == 'step1') {
                stepInfo = 1;
            }else if(step == 'step2') {
                stepInfo = 2;
            }else if(step == 'step3') {
                stepInfo = 3;
            }else if(step == 'step4') {
                stepInfo = 4;
            }else{
                stepInfo = '';
            }
            if(stepInfo != ''){
                _satellite.track("evCheckout",{
                    step: stepInfo
                });
                console.log("User is in Step : "+stepInfo);
            }
            onPageLoadProducts('page-multiStepCheckoutSummaryPage');

            var errorCount = 0;
            $(".help-block span").each(function (){
                errorCount++;
            });
            if(errorCount > 0){
                //console.log("Error pushed");
                _satellite.track("evErrorEvent",{ descriptionError: "User tries to continue checkout flow without filling all the required fields in checkout personal information form" });
            }
        }

        if($('body').hasClass('page-cartPage')){
            onCartPageLoad();
            onPageLoadProducts('page-cartPage');
        }

        if($("body").hasClass('page-orderConfirmationPage')){
            _satellite.track("evOrder",{});

            onCartPageLoad();
            onPageLoadProducts('page-orderConfirmationPage');
            var transaction = [], profile = [], address = [];
            var passport = $("#addressInfo").attr('data-attr-passport');
            if(passport == '' || passport == undefined){ passport = 'NA'; }
            else { passport = sha256(passport); }

            var drivers_licence = $("#addressInfo").attr('data-attr-drivingLicence');
            if(drivers_licence == '' || drivers_licence == undefined){ passport = 'NA'; }
            else { drivers_licence = sha256(drivers_licence); }

            transaction.transactionID =  $("#addressInfo").attr('data-attr-order-id'); //dynamic value
            transaction.affiliation =  "Ecommerce Puerto Rico"; //static value
            transaction.contract = $("#addressInfo").attr('data-attr-contract'); //dynamic value
            transaction.passport = passport; //dynamic value
            transaction.drivers_licence = drivers_licence; //dynamic value
            address.city = $("#addressInfo").attr('data-attr-town'); //dynamic value
            address.postalCode = $("#addressInfo").attr('data-attr-postalCode'); //dynamic value
            address.country = $("#addressInfo").attr('data-attr-country'); //dynamic value

            profile.address = address;
            transaction.profile = profile;
            window.digitalData.transaction = transaction;
        }
        _satellite.track("vpageview",{});
	}
}

function onPageLoadProducts(pageNameData) {
    var productLoop = '.cartProducts';
    if(pageNameData == 'page-multiStepCheckoutSummaryPage' || pageNameData == 'page-orderConfirmationPage' ) { productLoop = '.checkoutProducts'; }

    var products = [];
    $(productLoop).each(function(){
        var productInfo = {}, category = {};
        productInfo.productName = $(this).attr('data-attr-name');
        productInfo.productID = $(this).attr('data-attr-id');
        productInfo.quantity = $(this).attr('data-attr-quantity');

        var primaryCategory = $(this).attr('data-attr-primarycategory');
        if(primaryCategory != '' && primaryCategory != undefined) {
            if(primaryCategory == 'ALL Products') {
                primaryCategory = 'Residential';
            }
        }else {
            primaryCategory = 'NA';
        }
        if($(this).attr('data-attr-category') == 'Telephone') {
            primaryCategory = 'Telephone';
        }

        var subcategory = 'NA';
        if(primaryCategory == 'Residential') {
            subcategory = $(this).attr('data-attr-category');
        } else if(primaryCategory == 'Addon') {
            if($(this).attr('data-attr-id') == 'smart_protect_3' || $(this).attr('data-attr-id') == 'smart_protect_5'){
                subcategory = 'Smartprotect';
            }else {
                subcategory = 'Tv channels';
            }
        } else if(primaryCategory == 'Telephone'){
           subcategory = 'Postpaid';
        }

        category.primaryCategory = primaryCategory;
        category.subcategory = subcategory;
        products.push({
            'productInfo': productInfo,
            'category': category
        });
    });
    window.digitalData.product = products;
}

function onCartPageLoad(){
    var cart = {};
    var price = {};

    var internet = 0;
    if(typeof $("#cartPagePriceInfo").attr('data-attr-internet') !== 'undefined') {
        internet = $("#cartPagePriceInfo").attr('data-attr-internet');
    }
    price["internet-month"] = internet; //dynamic value

    var additionalBox = 0;
    if(typeof $("#cartPagePriceInfo").attr('data-attr-additionalBox') !== 'undefined') {
        additionalBox = $("#cartPagePriceInfo").attr('data-attr-additionalBox');
    }
    price["additionalBox-month"] = additionalBox;//dynamic value

    var additionalChannel = 0;
    if(typeof $("#cartPagePriceInfo").attr('data-attr-additionalChannel') !== 'undefined') {
        additionalChannel = $("#cartPagePriceInfo").attr('data-attr-additionalChannel');
    }
    price["additionalChannel-month"] = additionalChannel;//dynamic value

    var currency = '';
    if(typeof $("#cartPagePriceInfo").attr('data-attr-currency') !== 'undefined') {
        currency = $("#cartPagePriceInfo").attr('data-attr-currency');
    }
    price.currency = currency; //dynamic value

    var smartProtect = 0;
    if(typeof $("#cartPagePriceInfo").attr('data-attr-smartProtect') !== 'undefined') {
        smartProtect = $("#cartPagePriceInfo").attr('data-attr-smartProtect');
    }
    price["SmartProtect-month"] = smartProtect; //dynamic value

    var cartTotal = 0;
    if(typeof $("#cartPagePriceInfo").attr('data-attr-cartTotal') !== 'undefined') {
        cartTotal = $("#cartPagePriceInfo").attr('data-attr-cartTotal');
    }
    price.cartTotal = cartTotal; //dynamic value
    cart.price = price;
    window.digitalData.cart = cart;
}

$(window).on('load', function(){
    let adobeCallInterval = setInterval(function(){
        // when _satellite is defined and true, stop the function
        if(window.hasOwnProperty('_satellite')){
            clearInterval(adobeCallInterval);
            adobeAnalytics.pageLoadAdobeAnalyticsData();
            //console.log(window.digitalData);
        }
    }, 1000);
});

$(document).ready(function(){

    $(document).on("click", ".finish-button .conf-finish--js", function(e){
        var addedProduct = [];
        $(".selectedProducts").each(function(){
           var productInfo = {}, category = {};
           productInfo.productName = $(this).attr('data-attr-name');
           productInfo.productID = $(this).attr('data-attr-id');
           //productInfo.price = $(this).attr('data-attr-price').replace(/[^0-9\.]+/g, "").substring(1);
           productInfo.quantity = 1;

           var primaryCategory = $(this).attr('data-attr-primarycategory');
           if(primaryCategory != '' && primaryCategory != undefined) {
               if(primaryCategory == 'ALL Products') {
                   primaryCategory = 'Residential';
               }
           }else {
               primaryCategory = 'NA';
           }
           if($(this).attr('data-attr-category') == 'Telephone') {
               primaryCategory = 'Telephone';
           }

           var subcategory = 'NA';
           if(primaryCategory == 'Residential') {
               subcategory = $(this).attr('data-attr-category');
           }else if(primaryCategory == 'Addon') {
               subcategory = 'Tv channels';
           }else if(primaryCategory == 'Telephone'){
               subcategory = 'Postpaid';
           }

           category.primaryCategory = primaryCategory;
           category.subcategory = subcategory;
           addedProduct.push({
               'productInfo': productInfo,
               'category': category
           });
        });

        if($('input[name="smartProducts"]').length > 0 && $("input[name='smartProducts']").prop("checked")){
            var smartProducts = $('input[name="smartProducts"]:checked');
            var productInfo = {}, category = {};
            productInfo.productName = $(smartProducts).attr('data-attr-name');
            productInfo.productID = $(smartProducts).attr('data-attr-id');
            productInfo.quantity = 1;
            category.primaryCategory = 'Addon';
            category.subcategory = 'Smartprotect';
            addedProduct.push({
               'productInfo': productInfo,
               'category': category
            });
        }
        //console.log(addedProduct);
        _satellite.track("evAddToCart", { product: addedProduct });
    });

    //Guest checkout
    $('.page-cartPage').on('click', '.guestCheckoutButton ', function(event) {
        _satellite.track("evCheckoutPopUpOptions",{
            action: "GuestGO"
        });
    });

    $('#guestCheckout #guestEmail').on('keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            if($("#guestCheckout").valid()){
                _satellite.track("evCheckoutPopUpOptions",{
                    action: "GuestGO"
                });
            }
        }
    });

    $(document).on("click","#login-popup a",function() {
        _satellite.track("evCheckoutPopUpOptions",{
            action: "Login"
        });
    });

    //Menu item click
    $(document).on("click", ".nav__links .right_edge .sub__navigation ul li a", function(e){
        //e.preventDefault(); console.log("Menu click");
        _satellite.track("evInternalPromotionMenuClick",{ clickText: "Crea tu combinación" });
    });

    $(document).on("click", ".deleteForm .submitRemoveBundle", function(e){
        var removedProducts = [];
        $(".cartProducts").each(function(){
            var productInfo = {}, category = {};
            productInfo.productName = $(this).attr('data-attr-name');
            productInfo.productID = $(this).attr('data-attr-id');
            productInfo.quantity = $(this).attr('data-attr-quantity');

            var primaryCategory = $(this).attr('data-attr-primarycategory');
            if(primaryCategory != '' && primaryCategory != undefined) {
                if(primaryCategory == 'ALL Products') {
                    primaryCategory = 'Residential';
                }
            } else {
                primaryCategory = 'NA';
            }
            if($(this).attr('data-attr-category') == 'Telephone') {
                primaryCategory = 'Telephone';
            }

            var subcategory = 'NA';
            if(primaryCategory == 'Residential') {
                subcategory = $(this).attr('data-attr-category');
            } else if(primaryCategory == 'Addon') {
                if($(this).attr('data-attr-id') == 'smart_protect_3' || $(this).attr('data-attr-id') == 'smart_protect_5'){
                    subcategory = 'Smartprotect';
                }else {
                    subcategory = 'Tv channels';
                }
            } else if(primaryCategory == 'Telephone'){
               subcategory = 'Postpaid';
            }

            category.primaryCategory = primaryCategory;
            category.subcategory = subcategory;
            removedProducts.push({
                'productInfo': productInfo,
                'category': category
            });
        });
        _satellite.track("evRemoveFromCart", {product : removedProducts});
    });

    $(document).on("click", ".addonChannel form .removeAddonBtn", function(e){
        var removedAddon = [];
        var productInfo = {}, category = {};
        productInfo.productName = $(this).parent().parent().parent().find("td:first-child").text();
        productInfo.productID = $(this).parent().find('[name="productCode"]').val();
        productInfo.quantity = "1";
        var subcategory = 'NA';
        if(productInfo.productID == 'smart_protect_3' || productInfo.productID == 'smart_protect_5'){
            subcategory = 'Smartprotect';
        } else {
            subcategory = 'Tv channels';
        }
        category.primaryCategory = "Addon";
        category.subcategory = subcategory;
        removedAddon.push({
            'productInfo': productInfo,
            'category': category
        });
        _satellite.track("evRemoveFromCart", {product : removedAddon});
    });

});

function addressFormError(){
    //console.log("Form error address");
    _satellite.track("evErrorEvent",{ descriptionError: "User tries to continue checkout flow without filling all the required fields in checkout address form" });
}

function closeLoginPopup(){
    _satellite.track("evCheckoutPopUpOptions",{ action: "Cancel" });
}

function sha256(ascii) {
	function rightRotate(value, amount) {
		return (value>>>amount) | (value<<(32 - amount));
	};

	var mathPow = Math.pow;
	var maxWord = mathPow(2, 32);
	var lengthProperty = 'length'
	var i, j; // Used as a counter across the whole file
	var result = ''

	var words = [];
	var asciiBitLength = ascii[lengthProperty]*8;

	//* caching results is optional - remove/add slash from front of this line to toggle
	// Initial hash value: first 32 bits of the fractional parts of the square roots of the first 8 primes
	// (we actually calculate the first 64, but extra values are just ignored)
	var hash = sha256.h = sha256.h || [];
	// Round constants: first 32 bits of the fractional parts of the cube roots of the first 64 primes
	var k = sha256.k = sha256.k || [];
	var primeCounter = k[lengthProperty];

	var isComposite = {};
	for (var candidate = 2; primeCounter < 64; candidate++) {
		if (!isComposite[candidate]) {
			for (i = 0; i < 313; i += candidate) {
				isComposite[i] = candidate;
			}
			hash[primeCounter] = (mathPow(candidate, .5)*maxWord)|0;
			k[primeCounter++] = (mathPow(candidate, 1/3)*maxWord)|0;
		}
	}

	ascii += '\x80' // Append Ƈ' bit (plus zero padding)
	while (ascii[lengthProperty]%64 - 56) ascii += '\x00' // More zero padding
	for (i = 0; i < ascii[lengthProperty]; i++) {
		j = ascii.charCodeAt(i);
		if (j>>8) return; // ASCII check: only accept characters in range 0-255
		words[i>>2] |= j << ((3 - i)%4)*8;
	}
	words[words[lengthProperty]] = ((asciiBitLength/maxWord)|0);
	words[words[lengthProperty]] = (asciiBitLength)

	// process each chunk
	for (j = 0; j < words[lengthProperty];) {
		var w = words.slice(j, j += 16); // The message is expanded into 64 words as part of the iteration
		var oldHash = hash;
		// This is now the undefinedworking hash", often labelled as variables a...g
		// (we have to truncate as well, otherwise extra entries at the end accumulate
		hash = hash.slice(0, 8);

		for (i = 0; i < 64; i++) {
			var i2 = i + j;
			// Expand the message into 64 words
			// Used below if
			var w15 = w[i - 15], w2 = w[i - 2];

			// Iterate
			var a = hash[0], e = hash[4];
			var temp1 = hash[7]
				+ (rightRotate(e, 6) ^ rightRotate(e, 11) ^ rightRotate(e, 25)) // S1
				+ ((e&hash[5])^((~e)&hash[6])) // ch
				+ k[i]
				// Expand the message schedule if needed
				+ (w[i] = (i < 16) ? w[i] : (
						w[i - 16]
						+ (rightRotate(w15, 7) ^ rightRotate(w15, 18) ^ (w15>>>3)) // s0
						+ w[i - 7]
						+ (rightRotate(w2, 17) ^ rightRotate(w2, 19) ^ (w2>>>10)) // s1
					)|0
				);
			// This is only used once, so *could* be moved below, but it only saves 4 bytes and makes things unreadble
			var temp2 = (rightRotate(a, 2) ^ rightRotate(a, 13) ^ rightRotate(a, 22)) // S0
				+ ((a&hash[1])^(a&hash[2])^(hash[1]&hash[2])); // maj

			hash = [(temp1 + temp2)|0].concat(hash); // We don't bother trimming off the extra ones, they're harmless as long as we're truncating when we do the slice()
			hash[4] = (hash[4] + temp1)|0;
		}

		for (i = 0; i < 8; i++) {
			hash[i] = (hash[i] + oldHash[i])|0;
		}
	}

	for (i = 0; i < 8; i++) {
		for (j = 3; j + 1; j--) {
			var b = (hash[i]>>(j*8))&255;
			result += ((b < 16) ? 0 : '') + b.toString(16);
		}
	}
	return result;
};