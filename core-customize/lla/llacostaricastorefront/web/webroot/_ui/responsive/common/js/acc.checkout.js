ACC.checkout = {

	_autoload: [
		"bindCheckO",
		"bindForms",
		"bindSavedPayments",
        "bindC2CTimeout"
        /*"bindCheckoutGoogleMap"*/
    ],

    bindC2CTimeout:function(){

           if($('.2.step-head.js-checkout-step.active').length > 0 || $('.3.step-head.js-checkout-step.active').length > 0){
               setTimeout(function() {
                   var url = ACC.config.encodedContextPath + "/genesys/clickToCallRequest";
                   $.ajax({
                       type: "POST",
                       url: url,
                       success: function(data, xhr) {
                           console.log(data + "C2C SUCCESS");
                       },
                       error: function(result) {
                           console.log("C2C ERROR");
                       }
                   });
               }, 300000);
           }

    },
	bindForms:function(){

		/*$(document).on("click","#addressSubmit",function(e){
			e.preventDefault();
			//ACC.checkout.bindAddressFormSubmit();
			//$('#addressForm').submit();	
		})*/
		
		$(document).on("click","#deliveryMethodSubmit",function(e){
			e.preventDefault();
			$('#selectDeliveryMethodForm').submit();	
		})

	},

	bindSavedPayments:function(){
		$(document).on("click",".js-saved-payments",function(e){
			e.preventDefault();

			var title = $("#savedpaymentstitle").html();

			$.colorbox({
				href: "#savedpaymentsbody",
				inline:true,
				maxWidth:"100%",
				opacity:0.7,
				//width:"320px",
				title: title,
				close:'<span class="glyphicon glyphicon-remove"></span>',
				onComplete: function(){
				}
			});
		})
	},

	bindCheckO: function ()
	{
		var cartEntriesError = false;
		
		// Alternative checkout flows options
		$('.doFlowSelectedChange').change(function ()
		{
			if ('multistep-pci' == $('#selectAltCheckoutFlow').val())
			{
				$('#selectPciOption').show();
			}
			else
			{
				$('#selectPciOption').hide();

			}
		});



		$('.js-continue-shopping-button').click(function ()
		{
			var checkoutUrl = $(this).data("continueShoppingUrl");
			window.location = checkoutUrl;
		});
		
		$('.js-create-quote-button').click(function ()
		{
			$(this).prop("disabled", true);
			var createQuoteUrl = $(this).data("createQuoteUrl");
			window.location = createQuoteUrl;
		});

		
		$('.expressCheckoutButton').click(function()
				{
					document.getElementById("expressCheckoutCheckbox").checked = true;
		});
		
		$(document).on("input",".confirmGuestEmail,.guestEmail",function(){
			  
			  var orginalEmail = $(".guestEmail").val();
			  var confirmationEmail = $(".confirmGuestEmail").val();
			  
			  if(orginalEmail === confirmationEmail){
			    $(".guestCheckoutBtn").removeAttr("disabled");
			  }else{
			     $(".guestCheckoutBtn").attr("disabled","disabled");
			  }
		});
		
		$('.js-continue-checkout-button').click(function ()
		{
			var checkoutUrl = $(this).data("checkoutUrl");
			
			cartEntriesError = ACC.pickupinstore.validatePickupinStoreCartEntires();
			if (!cartEntriesError)
			{
				var expressCheckoutObject = $('.express-checkout-checkbox');
				if(expressCheckoutObject.is(":checked"))
				{
					window.location = expressCheckoutObject.data("expressCheckoutUrl");
				}
				else
				{
					var flow = $('#selectAltCheckoutFlow').val();
					if ( flow == undefined || flow == '' || flow == 'select-checkout')
					{
						// No alternate flow specified, fallback to default behaviour
						window.location = checkoutUrl;
					}
					else
					{
						// Fix multistep-pci flow
						if ('multistep-pci' == flow)
						{
						flow = 'multistep';
						}
						var pci = $('#selectPciOption').val();

						// Build up the redirect URL
						var redirectUrl = checkoutUrl + '/select-flow?flow=' + flow + '&pci=' + pci;
						window.location = redirectUrl;
					}
				}
			}
			return false;
		});

	},

	loadGoogleMapOnCheckout:function() {
     // var centerPts = new google.maps.LatLng(40.713956, 74.006653); 
      var mapOpts = {  
          zoom: 8,     
         // mapTypeId: google.maps.MapTypeId.ROADMAP,   
          center: { lat: -34.397, lng: 150.644 },
      }   
          
      var cmap = new google.maps.Map(document.getElementById("checkoutPageMap"), mapOpts);    
          
      var marker = new google.maps.Marker({  
          position: { lat: -34.397, lng: 150.644 },
          map: cmap,
          title: "Map"
      }); 

      var infowindow = new google.maps.InfoWindow({
		    content: "<p>Your Location:" + marker.getPosition() + "</p>",
	  });
	  google.maps.event.addListener(marker, "click", () => {
	    infowindow.open(map, marker);
	  });
	},

	bindCheckoutGoogleMap:function() {
	  ACC.global.addGoogleMapsApi("ACC.checkout.loadGoogleMapOnCheckout");	
	}

	/*bindAddressFormSubmit:function() {

	}*/

};

$(document).ready(function () {
	if ($("#profile_validation_error").length) {
		var firstName_error = $("#firstName_error").val();
		var lastName_error = $("#lastName_error").val();
	}

	$("#customerProfileForm").validate({ 
		rules: {
			"firstName": {
				required: true,
			},
			"lastName": {
				required: true,
			}
		},
		messages: {
			firstName: firstName_error,
			lastName: lastName_error
		},
        invalidHandler: function() {
        	var errorMsg = $("#gtmPersonalFormError").val();//checkout-1
        	handleGtmErrors(errorMsg);
            addressFormError();
        }
	});

	if ($("#address_validation_error").length) {
		var province_error = $("#province_error").val();
		var canton_error = $("#canton_error").val();
		var district_error = $("#district_error").val();
		var neighbourhood_error = $("#neighbourhood_error").val();
		var landMark_error = $("#landMark_error").val();
	}

	$("#addressForm").validate({ 
		rules: {
			"province": {
				required: true,
			},
			"building": {
				required: true,
			},
			"neighbourhood": {
				required: true,
			},
			"district": {
				required: true,
			},
			"landMark": {
				required: true,
			}
		},
		messages: {
			province: province_error,
			building: canton_error,
			neighbourhood: neighbourhood_error,
			district: district_error,
			landMark: landMark_error
		},
        invalidHandler: function() {
        	var errorMsg = $("#gtmAddressFormError").val();//checkout-2
			handleGtmErrors(errorMsg); 
            addressFormError();
        }
	});

	//GTM error
	var errorCountPersonal = 0, errorGlobalAlert= 0;
    $(".help-block span").each(function (){
    	errorCountPersonal++;
    });
    $(".global-alerts .alert-danger").each(function (){
    	errorGlobalAlert++;
    });
    if(errorCountPersonal > 0){
    	var errorMsg = $("#gtmPersonalFormError").val();//checkout-1
    	handleGtmErrors(errorMsg);    	
    }
    if(errorGlobalAlert > 0){
    	var errorMsg = $("#gtmCheckoutCheckBoxError").val();//checkout-3
    	handleGtmErrors(errorMsg);    	
    }

	//maxlength characters for address validation
	$("#profileSubmit").on('click', function(event) {
		var l1 = $("input[name='firstName']").val().length;
		var l2 = $("input[name='lastName']").val().length;
		
		var line =  l1+l2;
		var len = 25;

		if(line > len){
			$("label.maxLenError").remove();
				if($("input[name='lastName'] + maxLenError").length == 0) {
			    var addr_len_error = $("#addr_maxlen_error").val();
			    var label = '<label id="register.lastName-error" class="error maxLenError" for="register.lastName">'+addr_len_error+'</label>';
				$("input[name='lastName']").after(label);
			}
		  	return false;
		}
		else{
		    return true;
		}
	});
	
	//guest checkout mobile phone length(8) validation js code for cabletica only
	if ($("#guestCheckout").length) {
        $("#guestCheckout").on('keypress',"input[name='mobilePhone']", function(e){
            const pattern = /^[0-9]$/;
            return pattern.test(e.key )
        });
        var guestEmail_error = $("#guestEmail_error").val();
        var guestPhone_error = $("#guestPhone_error").val();
		var guestPhone_regex_error = $("#guestPhone_regex_error").val(); 

        $("#guestCheckout").validate({
            rules: {
                "guestEmail": {
                    required: true,
                    regx: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
					email: true
                },
                "mobilePhone": {
                    required: true,
					maxlength: 8,
					minlength: 8,
					regx: /^(2|4|5|6|7|8)([0-9]{7})/
                }
            },
            messages: {
                guestEmail: {
					required: guestEmail_error,
					email: guestEmail_error
				},
                mobilePhone:{
					required: guestPhone_error,
					regx: guestPhone_regex_error,
					minlength:guestPhone_error,
					maxlength:guestPhone_error
				}
            },       
        });
    }
    
    $('.page-cartPage').on('click', '.guestCableticaCheckoutButton', function(event) {
        guestLoginValidate();
    });
    
    function guestLoginValidate(){         
        if($("#guestCheckout").valid()){
            var guestEmail = $("#guestEmail").val();
            var mobilePhone = $("#mobilePhone").val();
            var documentType = $("#documentType option:selected").val();
            var documentNumber = $("#documentNumber").val();
            if(mobilePhone==undefined || documentNumber==undefined){
             mobilePhone="";
             documentNumber="";
            }
            $.ajax({
                url: ACC.config.encodedContextPath + '/login/checkout/guest',
                async: true,
                type: "POST",
                dataType: "html",
                data: {
                    guestEmail: guestEmail,
                    mobilePhone: mobilePhone,
                    documentType: documentType,
                    documentNumber: documentNumber
                },
                success:function(data) {
                       console.log('saved successfully');
                       window.location=ACC.config.encodedContextPath + '/checkout';
               },
               error:function(data,status,xhr) {
                   console.log('error occured');
               }
            });
        }
    }

	$(".docType").on('change', function () {
		var documentNumber_national_error = $("#documentNumber_national_error").val();
		var documentNumber_resident_error = $("#documentNumber_resident_error").val();
		var documentNumber_passport_error = $("#documentNumber_passport_error").val();
		var idType = $('.docType').val();

		if(idType === 'CEDULANATIONAL'){
			$("input[name='documentNumber']").attr('maxlength','10');
			$("input[name='documentNumber']").rules('add', {
                required: true,
				regx:/^(0)([0-9]{9})/,
				messages: {
					required: documentNumber_national_error,
					regx: documentNumber_national_error
				}
            });
		}else if(idType === 'CEDULARESIDENTIAL'){
			$("input[name='documentNumber']").attr('maxlength','12');
			$("input[name='documentNumber']").rules('add', {
                required: true,
				regx:/^(1)([0-9]{10,11})/,
				messages: {
					required: documentNumber_resident_error,
					regx: documentNumber_resident_error
				}
            });
		}else {
			$("input[name='documentNumber']").removeAttr('maxlength');
			$("input[name='documentNumber']").rules('add', {
                required: true,
				minlength: 8,
				maxlength: 10,
				regx:/^[a-zA-Z0-9]+$/,
				messages: {
					required: documentNumber_passport_error,
					minlength: documentNumber_passport_error,
					maxlength: documentNumber_passport_error,
					regx: documentNumber_passport_error
				}
            });
		}
	}).trigger("change");

/*	$("#guestCheckout").on('blur',"input[name='mobilePhone'], input[name='guestEmail']", function(e){
            if($("#guestCheckout").valid()) {
                 var mobileNumber=$("#mobilePhone").val();
                 var guestEmail=$("#guestEmail").val();
                 var url = ACC.config.encodedContextPath + "/genesys/anonymous/clickToCallRequest";
                 $.ajax({
                    type: "POST",
                    data: {mobileNumber: mobileNumber, email:guestEmail},
                    url:  url,
                    success: function(data, xhr) {
                        console.log(data + "C2C SUCCESS");
                    },
                    error: function(result) {
                        console.log("C2C ERROR");
                    }
                });
            }
        }); */
	
});
