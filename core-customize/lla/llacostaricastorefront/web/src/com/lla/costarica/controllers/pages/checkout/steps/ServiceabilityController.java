/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.costarica.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lla.core.enums.ClickToCallReasonEnum;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import com.lla.mulesoft.integration.service.LLAMulesoftServiceabilityService;

import java.io.IOException;


/**
 * @param <llaMulesoftServiceabilityService>
 */
@Controller
public class ServiceabilityController<llaMulesoftServiceabilityService>
{
	private static final Logger LOG = Logger.getLogger(ServiceabilityController.class);

	@Autowired
	LLAMulesoftServiceabilityService llaMulesoftServiceabilityService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	protected static final String ATTR_ADDRESS_FORM = "addressForm";

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "modelService")
	private ModelService modelService;


	@RequestMapping(value = "/serviceabilitycheck", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Boolean> checkLocationServiceability(@RequestParam(value = "prov", required = false)
	final String province, @RequestParam(value = "build", required = false)
	final String building, @RequestParam(value = "dist", required = false)
	final String district, @RequestParam(value = "neigh", required = false)
	final String neighbourhood, @RequestParam(value = "land", required = false)
	final String landMark,@RequestParam(value = "originSource", required = false)
	final String originSource, @RequestParam(value = "lat", required = true)
	final Double sourceLatitude, @RequestParam(value = "long", required = true)
	final Double sourceLongitude)
	{
		boolean serviceResponse = false;
		if (sourceLatitude != null && sourceLongitude != null && configurationService.getConfiguration().getBoolean("mulesoft.serviciability.call.execute"))
		{
			try
			{
				LOG.info(String.format("Generating serviceability request from Page ::%s for latitude ::: %s and longitude :: %s ", originSource.toUpperCase(),sourceLatitude, sourceLongitude));
				serviceResponse = llaMulesoftServiceabilityService.checkServiceability(sourceLatitude, sourceLongitude);

				final CartModel cart = cartService.getSessionCart();
				if (null != cart)
				{
					cart.setServiceabilityValue(serviceResponse);
					modelService.save(cart);
					modelService.refresh(cart);
				}
				if (serviceResponse)
				{
					final AddressForm addressForm = new AddressForm();
					addressForm.setProvince(province);
					addressForm.setBuilding(building);
					addressForm.setDistrict(district);
					addressForm.setNeighbourhood(neighbourhood);
					addressForm.setLandMark(landMark);
					sessionService.setAttribute(ATTR_ADDRESS_FORM, addressForm);
					sessionService.setAttribute("selectedLattitude", sourceLatitude);
					sessionService.setAttribute("selectedLongitude", sourceLongitude);
					LOG.info(String.format("Location is Serviceable for  request originated from  Page ::: %s for latitude %s and longitude %s ",originSource.toUpperCase(),sourceLatitude,sourceLongitude));
					return ResponseEntity.ok(serviceResponse);
				}

				LOG.info(String.format("Location is not Serviceable for request originated from Page ::: %s for latitude::: %s and longitude ::: %s",originSource.toUpperCase(),sourceLatitude,sourceLongitude));
				return ResponseEntity.ok(serviceResponse);

			}
			catch (final Exception llaApiExp)
			{
				LOG.error(String.format("Error Api Exception for Serviceable Location Test Originated from Page ::%s for latitude ::: %s and longitude::: %s due to %s",originSource.toUpperCase(),sourceLatitude,sourceLongitude,llaApiExp));
				return ResponseEntity.badRequest().body(false);
			}
		}else{
			LOG.info(String.format("Skipping Serviceability Call from Page ::%s for latitude ::: %s and longitude::: %s ",originSource.toUpperCase(),sourceLatitude,sourceLongitude));
            return ResponseEntity.ok(Boolean.TRUE);
        }

	//	return ResponseEntity.badRequest().body(false);
	}
}