/**
 *
 */
package com.lla.costarica.interceptors.beforeview;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;


/**
 * @author US927CX
 *
 */
public class AdobeAnalyticsBeforeViewHandler implements BeforeViewHandler
{
   private static final String CABLETICA_PRODUCTION_SITE= "cabletica.productionsite";
   private static final String CABLETICA_STAGGING_SITE= "cabletica.staggingsite";

   @Resource(name = "configurationService")
   private ConfigurationService configurationService;

   @Override
   public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView) throws Exception
   {
       modelAndView.addObject("isCableticaProductionSite", configurationService.getConfiguration().getString(CABLETICA_PRODUCTION_SITE));
       modelAndView.addObject("isCableticaStaggingSite", configurationService.getConfiguration().getString(CABLETICA_STAGGING_SITE));
   }
}

