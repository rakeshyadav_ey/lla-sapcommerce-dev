/**
 *
*/
package com.lla.costarica.controllers.pages;

import com.lla.core.util.LLACommonUtil;
import com.lla.facades.LLARegionCodeData;
import com.lla.facades.LLARegionPriceData;
import com.lla.facades.address.LLAAddressFacade;
import com.lla.facades.address.ProvinceData;
import com.lla.facades.product.LLATmaProductFacade;
import com.microsoft.sqlserver.jdbc.StringUtils;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.subscriptionservices.model.SubscriptionPricePlanModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;




@Controller
@RequestMapping(value = "/residential")
public class LLACableticaResidentialController extends AbstractPageController
{
	private static final Logger LOG = Logger.getLogger(LLACableticaResidentialController.class);
	private static final String DEFAULT_CATEGORY = "3PInternetTvTelephone";
	private static final String TRIPLEPLAYCATEGORY = "3PInternetTvTelephone";
	private static final String DOBLEPLAYCATEGORY = "2PInternetTv";
	private static final String INTERNETPLANCATEGORY = "internetcabletica";
	private static final String TELEVISIONPLANCATEGORY = "tvcabletica";
	private static final String CABLETICA_RESEDENTIAL_PLP = "residential";
	private static final String CHANNEL_ADDON = "channelsaddoncabletica";
	private static final String REGION = "Costarica";
	private static final Collection<String> ALL_CATEGORIES = Arrays.asList(TRIPLEPLAYCATEGORY, DOBLEPLAYCATEGORY,
			INTERNETPLANCATEGORY, TELEVISIONPLANCATEGORY);
	private static final String TELEFONIA_LINK_URL = "telefonia.link.url";
	private static final String ATTR_ADDRESS_FORM = "addressForm";

	@Resource(name = "llaTmaProductFacade")
	private LLATmaProductFacade llaTmaProductFacade;

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "llaCommonUtil")
	private LLACommonUtil llaCommonUtil;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "commerceCategoryService")
	private CommerceCategoryService commerceCategoryService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Autowired
	private LLAAddressFacade llaAddressFacade;

	@ModelAttribute("provinceList")
	public List<ProvinceData> getProvince()
	{
		return llaCommonUtil.isSiteCabletica() ? llaAddressFacade.getAllProvince() : new ArrayList<ProvinceData>();
	}

	@RequestMapping(method =
	{ RequestMethod.POST, RequestMethod.GET })
	public String getAllProducts(@RequestParam(value = "selectedCategory", required = false, defaultValue = DEFAULT_CATEGORY)
	final String selectedCategory, @RequestParam(value = "cartRuleAlert", required = false)
	final String cartRuleAlert, final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (llaCommonUtil.isSiteCabletica() && null != sessionService.getAttribute(ATTR_ADDRESS_FORM)) {
			sessionService.removeAttribute(ATTR_ADDRESS_FORM);
		}
		final CartModel cart = cartService.getSessionCart();
		String productInCart = StringUtils.EMPTY;
		final CategoryModel category = commerceCategoryService.getCategoryForCode(CHANNEL_ADDON);
		if (null != cart)
		{
			for (final AbstractOrderEntryModel entry : cart.getEntries())
			{
				if (!entry.getProduct().getSupercategories().contains(category))
				{
					productInCart = entry.getProduct().getCode();
				}
			}
		}
		if (!StringUtils.isEmpty(cartRuleAlert) && cartRuleAlert.equals("cartRuleAlert"))
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"basket.page.bundle.message.add.cabletica.error");
			return REDIRECT_PREFIX + "/residential";
		}
		model.addAttribute("productInCart", productInCart);

		model.addAttribute("categoriesList", ALL_CATEGORIES);
		model.addAttribute("activeCategoryName", selectedCategory);
		final Map<String, List<ProductData>> allproductdata = ALL_CATEGORIES.stream().collect(Collectors.toMap(r -> r,
				productData -> llaCommonUtil.sortProductOnSequenceId(llaTmaProductFacade.getProductDataForCategory(productData))));
		model.addAttribute("allProductDatas", allproductdata);
		model.addAttribute("regions", i18NFacade.getRegionsForCountryIso(REGION));
		model.addAttribute("telephoneLink", configurationService.getConfiguration().getString(TELEFONIA_LINK_URL));
		model.addAttribute("pageType", PageType.PLP.name());
		model.addAttribute("addressForm", new AddressForm());
		final ContentPageModel contentPage = getContentPageForLabelOrId(CABLETICA_RESEDENTIAL_PLP);
		setCMSPageData(model, contentPage, selectedCategory);
		return getViewForPage(model);
	}



	@RequestMapping(value = "/regionNames", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<LLARegionPriceData> getRegions(@RequestBody
	final LLARegionCodeData llaRegionCodeData) throws CMSItemNotFoundException
	{
		final LLARegionPriceData llaRegionPriceData = new LLARegionPriceData();
		final String region = llaRegionCodeData.getRegionCode();
		if (StringUtils.isEmpty(region))
		{
			return new ResponseEntity<>(llaRegionPriceData, HttpStatus.BAD_REQUEST);
		}
		final List<ProductModel> productModel = llaTmaProductFacade.getProductForCategory(TELEVISIONPLANCATEGORY);
		final Map<String, Double> llaregionPrices = new HashMap<String, Double>();
		for (final ProductModel productModel2 : productModel)
		{
			final Collection<PriceRowModel> priceRowModelCollection = productModel2.getOwnEurope1Prices();
			priceRowModelCollection.stream().filter(SubscriptionPricePlanModel.class::isInstance)
					.map(SubscriptionPricePlanModel.class::cast)
					.filter(r -> CollectionUtils.isNotEmpty(r.getRegions())
							&& r.getRegions().iterator().next().getIsocode().equals(region))
					.map(r -> r.getRecurringChargeEntries())
					.forEach(r -> llaregionPrices.put(productModel2.getCode(), (double) Math.round(r.iterator().next().getPrice().doubleValue()
							+ r.iterator().next().getTax911().doubleValue() + r.iterator().next().getTax13Percent().doubleValue())));
		}
		llaRegionPriceData.setRegionPrice(llaregionPrices);
		sessionService.setAttribute("llaregion", region);
		sessionService.setAttribute("llaregionPrices", getRegionPriceTv(productModel, region));
		return new ResponseEntity<>(llaRegionPriceData, HttpStatus.OK);
	}

	/**
	 * @param productModel
	 * @return
	 */
	private Map<String, Double> getRegionPriceTv(final List<ProductModel> productModel, final String region)
	{
		final Map<String, Double> llaregionTvPrices = new HashMap<String, Double>();
		for (final ProductModel productModel2 : productModel)
		{
			final Collection<PriceRowModel> priceRowModelCollection = productModel2.getOwnEurope1Prices();
			priceRowModelCollection.stream().filter(SubscriptionPricePlanModel.class::isInstance)
					.map(SubscriptionPricePlanModel.class::cast)
					.filter(r -> CollectionUtils.isNotEmpty(r.getRegions())
							&& r.getRegions().iterator().next().getIsocode().equals(region))
					.map(r -> r.getRecurringChargeEntries())
					.forEach(r -> llaregionTvPrices.put(productModel2.getCode(), r.iterator().next().getPrice().doubleValue()));
		}
		return llaregionTvPrices;
	}



	/**
	 * @param model
	 * @param contentPage
	 */
	private void setCMSPageData(final Model model, final ContentPageModel contentPage, final String selectedCategory)
	{
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		updatePageTitle(model, contentPage);
		if (selectedCategory.equalsIgnoreCase(TRIPLEPLAYCATEGORY) || selectedCategory.equalsIgnoreCase(DOBLEPLAYCATEGORY))
		{
			final List<Breadcrumb> breadcrumbs = resourceBreadcrumbBuilder.getBreadcrumbs(null);
			breadcrumbs.add(new Breadcrumb("#",
					getMessageSource().getMessage("cabletica.create.breadcrumb.paquetes", null, getI18nService().getCurrentLocale()),
					null));
			breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("cabletica.create.breadcrumb" + selectedCategory, null,
					getI18nService().getCurrentLocale()), null));
			model.addAttribute("breadcrumbs", breadcrumbs);
		}
		else
		{
			model.addAttribute(WebConstants.BREADCRUMBS_KEY,
					resourceBreadcrumbBuilder.getBreadcrumbs("cabletica.create.breadcrumb" + selectedCategory));
		}
	}

	protected void updatePageTitle(final Model model, final AbstractPageModel cmsPage)
	{
		storeContentPageTitleInModel(model, getPageTitleResolver().resolveHomePageTitle(cmsPage.getTitle()));
	}

}

