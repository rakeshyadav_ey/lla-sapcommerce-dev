package com.lla.costarica.controllers.misc;

import com.lla.core.enums.ClickToCallReasonEnum;
import com.lla.core.enums.OriginSource;
import com.lla.core.service.order.LLACommerceCheckoutService;
import com.lla.core.util.LLACommonUtil;
import com.lla.mulesoft.integration.dto.ClickCallServicablity;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.acceleratorcms.model.components.ClickToCallModel;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.falloutprocess.model.FalloutProcessModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class ClickToCallController extends AbstractController {
    private static final Logger LOG = Logger.getLogger(ClickToCallController.class);
    @Autowired
    private CartService cartService;
    @Autowired
    private LLACommonUtil llaCommonUtil;
    @Autowired
    private ConfigurationService configurationService;
    @Autowired
    private LLACommerceCheckoutService llaCommerceCheckoutService;
    @Autowired
    private BusinessProcessService businessProcessService;
    @Autowired
    private ModelService modelService;
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;
    @RequestMapping(value = "/genesys/clickToCallRequest", method = {RequestMethod.POST})
    @ResponseBody
    public ResponseEntity getCallBack(final Model model, final HttpStatus httpStatus, final HttpServletRequest request,
                                      final HttpServletResponse response)
    {
        boolean isCallRequestPlaced = false;
        ClickToCallModel c2cModel =null;
        final CartModel cartModel = cartService.getSessionCart();
        if (null != cartModel)
        {
                CustomerModel user=(CustomerModel)cartModel.getUser();
                LOG.info(String.format("Making Genesys Click to Call Request for Guest User ::%s having mobile number ::%s for Cart ::%s",user.getAdditionalEmail(),user.getMobilePhone(),cartModel.getCode()));
                llaCommonUtil.launchFalloutProcess(cartModel, ClickToCallReasonEnum.LEAD,OriginSource.CHECKOUT,null,StringUtils.EMPTY, StringUtils.EMPTY);
               // c2cModel = llaCommerceCheckoutService.executeCustomerCallBackRequest(((AbstractOrderModel) cartModel), null);
        }
        return new ResponseEntity(httpStatus.OK);
    }

    @RequestMapping(value = "/genesys/anonymous/clickToCallRequest", method = {RequestMethod.POST})
    @ResponseBody
    public String getCallBack(final Model model, @RequestParam(value="mobileNumber",required = true) final String mobileNumber,@RequestParam(value="email",required = true) final String email,
                              @RequestParam(value="documentType",required = true) final String documentType,
                              @RequestParam(value="documentNumber",required = true) final String documentNumber,
                              final HttpStatus httpStatus, final HttpServletRequest request,final HttpServletResponse response)
    {
        if(configurationService.getConfiguration().getBoolean("anonymous.click.to.call.required")){
            boolean isCallRequestPlaced = false;
            ClickToCallModel c2cModel =null;
            final CartModel cartModel = cartService.getSessionCart();
            if (null != cartModel)
            {
                try {
                    LOG.info(String.format("Making Genesys Click to Call Request for Anonymous User ::%s having mobile number ::%s",email,mobileNumber));
                    c2cModel = llaCommerceCheckoutService.executeAnonymousCallBackRequest(((AbstractOrderModel) cartModel),createC2CDataObject(mobileNumber,email,documentType, documentNumber));
                } catch (LLAApiException exception) {
                    LOG.error(String.format("Exception in Making Call %s ",exception));
                }
            }
            if(!c2cModel.getGenesysNotificationSent()) {
                return "Failure";
            }
            return "Success";
        }else{
            return "Skipped";
        }

    }
    private ClickCallServicablity createC2CDataObject(final String mobileNumber,final String email,final String documentType, final String documentNumber){
        ClickCallServicablity clickCallServicablity = new ClickCallServicablity();
        clickCallServicablity.setEmail(email);
        clickCallServicablity.setIdentityNumber(documentNumber);
        clickCallServicablity.setPhoneNumber(mobileNumber);
        clickCallServicablity.setDocumentType(documentType);
        clickCallServicablity.setProduct(llaMulesoftIntegrationUtil.getProductForC2C(cartService.getSessionCart()).getCode());
        return  clickCallServicablity;
    }

    @RequestMapping(value = "/noservicable-c2cRequest", method = {RequestMethod.POST})
    @ResponseBody
    public ResponseEntity callMeBack(final Model model, final HttpStatus httpStatus, final HttpServletRequest request,
                                     final HttpServletResponse response)
    {
        boolean isCallRequestPlaced = false;
        ClickToCallModel c2cModel =null;
        final CartModel cartModel = cartService.getSessionCart();
        if (null != cartModel)
        {
            CustomerModel user=(CustomerModel)cartModel.getUser();
            LOG.info(String.format("Making Genesys Click to Call Request for Guest User ::%s having mobile number ::%s for Cart ::%s",user.getAdditionalEmail(),user.getMobilePhone(),cartModel.getCode()));
            llaCommonUtil.launchFalloutProcess(cartModel, ClickToCallReasonEnum.NOT_SERVICEABLE,OriginSource.CHECKOUT,null,StringUtils.EMPTY, StringUtils.EMPTY);
        }
        return new ResponseEntity(httpStatus.OK);
    }

}
