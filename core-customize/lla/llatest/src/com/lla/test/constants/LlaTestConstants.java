/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.test.constants;

/**
 * 
 */
public class LlaTestConstants extends GeneratedLlaTestConstants
{

	public static final String EXTENSIONNAME = "llatest";

}
