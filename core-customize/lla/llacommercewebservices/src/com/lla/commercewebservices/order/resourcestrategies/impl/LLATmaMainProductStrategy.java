/**
 *
 */
package com.lla.commercewebservices.order.resourcestrategies.impl;

import de.hybris.platform.b2ctelcoservices.order.data.TmaCartValidationResult;
import de.hybris.platform.b2ctelcoservices.order.resourcestrategies.TmaAbstractOrderResourceStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;

import com.lla.core.util.LLACommonUtil;

/**
 * @author PR678TG
 *
 */
public class LLATmaMainProductStrategy implements TmaAbstractOrderResourceStrategy
{
	private static final String CATEGORY_BUNDLE = "bundle";
	private static final String CATEGORY_DEVICES = "devices";
	private static final String CATEGORY_PLANS = "plans";

	private LLACommonUtil llaCommonUtil;

	public LLACommonUtil getLlaCommonUtil()
	{
		return llaCommonUtil;
	}

	public void setLlaCommonUtil(final LLACommonUtil llaCommonUtil)
	{
		this.llaCommonUtil = llaCommonUtil;
	}

	@Override
	public TmaCartValidationResult validateResource(final CommerceCartParameter parameter)
	{
		final TmaCartValidationResult result = new TmaCartValidationResult();

		if (getLlaCommonUtil().isSiteVTR() && checkMainProduct(parameter.getProduct()))
		{
			final boolean mainProductPresent = parameter.getCart().getEntries().stream()
					.filter(entries -> checkMainProduct(entries.getProduct())).findFirst().isPresent();

			if (mainProductPresent)
			{
				result.setValid(false);
				result.setMessage("Product already in the cart will be replaced with " + parameter.getProduct().getCode());
				return result;
			}
		}

		result.setValid(true);
		result.setCommerceCartParameter(parameter);
		return result;
	}

	private boolean checkMainProduct(final ProductModel product)
	{
		if (getLlaCommonUtil().containsFirstLevelCategoryWithCode(product, CATEGORY_DEVICES)
				|| getLlaCommonUtil().containsSecondLevelCategoryWithCode(product, CATEGORY_BUNDLE))
		{
			return true;
		}
		return false;
	}

	@Override
	public void updateResource(final CommerceCartParameter commerceCartParameter,
			final CommerceCartModification commerceCartModification) throws CommerceCartModificationException
	{
		// XXX Auto-generated method stub
	}

}
