package com.lla.commercewebservices.order.checklist;

import com.lla.core.util.LLACommonUtil;
import de.hybris.platform.b2ctelcoservices.checklist.context.TmaChecklistContext;
import de.hybris.platform.b2ctelcoservices.checklist.impl.DefaultTmaChecklistService;
import de.hybris.platform.b2ctelcoservices.compatibility.TmaPolicyEngine;
import de.hybris.platform.b2ctelcoservices.compatibility.data.RuleEvaluationResult;
import de.hybris.platform.b2ctelcoservices.compatibility.data.TmaPolicyContext;
import de.hybris.platform.b2ctelcoservices.enums.TmaCompatibilityPolicyActionType;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class LLATmaChecklistService extends DefaultTmaChecklistService
{
    private TmaPolicyEngine policyEngine;

    @Autowired
    private LLACommonUtil llaCommonUtil;

    @Override
    public boolean areActionsFulfilled(final AbstractOrderModel order)
    {
        cleanUpCartEntriesValidation(order);
        if (!llaCommonUtil.isSiteVTR())
        {
            final List<TmaPolicyContext> contexts = createContexts(order.getEntries());

            final Set<RuleEvaluationResult> results = getPolicyEngine().findPolicies(contexts,
                    Collections.singleton(TmaCompatibilityPolicyActionType.CHECKLIST));
            getPolicyEngine().applyActions(results);
        }
        return order.getEntries().stream()
                .allMatch(entry -> CollectionUtils.isEmpty(((CartEntryModel) entry).getCartEntryValidations()));
    }


    @Override
    protected void cleanUpCartEntriesValidation(final AbstractOrderModel order)
    {
        // XXX Auto-generated method stub
        super.cleanUpCartEntriesValidation(order);
    }

    @Override
    public Set<RuleEvaluationResult> findActions(final TmaChecklistContext checklistContext)
    {
        // XXX Auto-generated method stub
        return super.findActions(checklistContext);
    }

    /**
     * @return the policyEngine
     */
    @Override
    public TmaPolicyEngine getPolicyEngine()
    {
        return policyEngine;
    }


    /**
     * @param policyEngine
     *           the policyEngine to set
     */
    @Override
    public void setPolicyEngine(final TmaPolicyEngine policyEngine)
    {
        this.policyEngine = policyEngine;
    }


}
