package com.lla.commercewebservices.cart.hooks;

import com.lla.core.util.LLACommonUtil;
import de.hybris.platform.b2ctelcoservices.compatibility.TmaCompatibilityPolicyEngine;
import de.hybris.platform.b2ctelcoservices.hook.impl.TmaPoUpdateCartEntryHook;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.hook.CommerceUpdateCartEntryHook;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.enums.GroupType;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.order.EntryGroup;

public class LLATmaPoUpdateCartEntryHook extends TmaPoUpdateCartEntryHook implements CommerceUpdateCartEntryHook
{
    private TmaCompatibilityPolicyEngine compatibilityPoliciesEngine;

    private LLACommonUtil llaCommonUtil;

    @Override
    public void beforeUpdateCartEntry(CommerceCartParameter parameter) {
        super.beforeUpdateCartEntry(parameter);
    }

    @Override
    public void afterUpdateCartEntry(CommerceCartParameter parameter, CommerceCartModification result)
    {
        final CartModel cart = parameter.getCart();

        if (parameter.getProduct() == null)
        {
            return;
        }

        final AbstractOrderModel cartModel = parameter.getCart();
        final EntryGroup entryGroup = getEntryGroupService()
                .getGroupOfType(cartModel, result.getEntry().getEntryGroupNumbers(), GroupType.B2CTELCO_BPO);

        if (entryGroup == null && !getLlaCommonUtil().isSiteVTR()) // NOSONAR
        {
            getCompatibilityPoliciesEngine().verifyCompatibilityPoliciesForStandaloneProducts(cartModel);
            return;
        }
        getTmaCartHookHelper().invalidateBpoEntries(cart, entryGroup);
        if(!getLlaCommonUtil().isSiteVTR())
        {
            getCompatibilityPoliciesEngine().verifyCompatibilityPolicies(cartModel, entryGroup);
        }
    }


    public LLACommonUtil getLlaCommonUtil()
    {
        return llaCommonUtil;
    }

    public void setLlaCommonUtil(LLACommonUtil llaCommonUtil) {
        this.llaCommonUtil = llaCommonUtil;
    }

    @Override
    public TmaCompatibilityPolicyEngine getCompatibilityPoliciesEngine() {
        return compatibilityPoliciesEngine;
    }

    @Override
    public void setCompatibilityPoliciesEngine(TmaCompatibilityPolicyEngine compatibilityPoliciesEngine) {
        this.compatibilityPoliciesEngine = compatibilityPoliciesEngine;
    }
}
