package com.lla.commercewebservices.cart.hooks;

import com.lla.core.util.LLACommonUtil;
import de.hybris.platform.b2ctelcoservices.compatibility.TmaCompatibilityPolicyEngine;
import de.hybris.platform.b2ctelcoservices.hook.impl.TmaPoAddToCartMethodHook;
import de.hybris.platform.b2ctelcoservices.model.TmaBundledProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.model.TmaCartSubscriptionInfoModel;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.hook.CommerceAddToCartMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.enums.GroupType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.order.EntryGroup;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LLATmaPoAddToCartMethodHook extends TmaPoAddToCartMethodHook implements CommerceAddToCartMethodHook
{

    private TmaCompatibilityPolicyEngine compatibilityPolicyEngine;

    private LLACommonUtil llaCommonUtil;

    @Override
    public void afterAddToCart(CommerceCartParameter parameter, CommerceCartModification result)
    {
        if (!(parameter.getProduct() instanceof TmaProductOfferingModel))
        {
            return;
        }
        if (result.getQuantityAdded() <= 0 || result.getEntry() == null)
        {
            return;
        }
        final AbstractOrderEntryModel orderEntry = result.getEntry();
        final AbstractOrderModel order = orderEntry.getOrder();

        if (orderEntry.getBpo() == null)
        {
            getModelService().save(orderEntry);
            getModelService().refresh(orderEntry);
            if(!getLlaCommonUtil().isSiteVTR())
            {
                compatibilityPolicyEngine.verifyCompatibilityPoliciesForStandaloneProducts(order);
            }
            return;
        }
        final EntryGroup rootEntryGroup = getEntryGroup(parameter, order);
        assignGroupNumberToEntry(orderEntry, rootEntryGroup.getGroupNumber());
        result.setEntryGroupNumbers(orderEntry.getEntryGroupNumbers());

        updateGroupEntriesSubscriptionDetails(order, rootEntryGroup, orderEntry.getSubscriptionInfo());
        getTmaCartHookHelper().invalidateBpoEntries((CartModel) order, rootEntryGroup);

        getModelService().save(orderEntry);
        getModelService().refresh(orderEntry);
        getEntryGroupService().forceOrderSaving(order);

        getCompatibilityPolicyEngine().verifyCompatibilityPolicies(order, rootEntryGroup);
    }

    private EntryGroup getEntryGroup(final CommerceCartParameter parameter, final AbstractOrderModel order)
    {
        final TmaBundledProductOfferingModel rootBpo = (TmaBundledProductOfferingModel) getTmaPoService()
                .getPoForCode(parameter.getBpoCode());

        final EntryGroup rootEntryGroup = createEntryGroupStructure(rootBpo);
        rootEntryGroup.setProcessType(parameter.getProcessType());
        if (!parameter.getEntryGroupNumbers().isEmpty())
        {
            final EntryGroup correspondingCartGroup = getCorrespondingGroup(rootEntryGroup, order, parameter.getEntryGroupNumbers());

            if (correspondingCartGroup != null)
            {
                invalidateBpoEntries(parameter.getCart(), correspondingCartGroup.getGroupNumber());
                return correspondingCartGroup;
            }
        }
        assignGroupNumbers(rootEntryGroup, parameter.getCart());

        final List<EntryGroup> orderGroups = new ArrayList<>(order.getEntryGroups());
        orderGroups.add(rootEntryGroup);
        order.setEntryGroups(orderGroups);
        getEntryGroupService().forceOrderSaving(order);
        return rootEntryGroup;
    }

    private void assignGroupNumbers(final EntryGroup group, final CartModel cart)
    {
        group.setGroupNumber(getEntryGroupService().findMaxGroupNumber(cart.getEntryGroups()) + 1);
    }

    private void assignGroupNumberToEntry(final AbstractOrderEntryModel entry, final int entryGroupNumber)
    {
        final Set<Integer> numbers = new HashSet<>();
        if (entry.getEntryGroupNumbers() != null)
        {
            numbers.addAll(entry.getEntryGroupNumbers());
        }
        numbers.add(entryGroupNumber);
        entry.setEntryGroupNumbers(numbers);
    }

    private void updateGroupEntriesSubscriptionDetails(final AbstractOrderModel order, final EntryGroup entryGroup,
                                                       final TmaCartSubscriptionInfoModel subscriptionInfoModel)
    {
        if (subscriptionInfoModel == null || subscriptionInfoModel.getSubscriptionTerm() == null)
        {
            return;
        }
        for (final AbstractOrderEntryModel cartEntry : order.getEntries())
        {
            if (cartEntry.getEntryGroupNumbers() != null && cartEntry.getEntryGroupNumbers().contains(entryGroup.getGroupNumber())
                    && cartEntry.getSubscriptionInfo() == null)
            {
                cartEntry.setSubscriptionInfo(subscriptionInfoModel);
                getModelService().save(cartEntry);
                getModelService().refresh(cartEntry);

                final CommerceCartParameter cartEntryParam = new CommerceCartParameter();
                order.setCalculated(false);
                cartEntryParam.setCart((CartModel) order);
                cartEntryParam.setCreateNewEntry(false);
                cartEntryParam.setProduct(cartEntry.getProduct());
                cartEntryParam.setQuantity(cartEntry.getQuantity());
                cartEntryParam.setUnit(cartEntry.getUnit());
                cartEntryParam.setEnableHooks(false);
                cartEntryParam.setProcessType(cartEntry.getProcessType().getCode());
                cartEntryParam.setBpoCode(cartEntry.getBpo().getCode());
                cartEntryParam.setSubscriptionInfo(cartEntry.getSubscriptionInfo());

                getCommerceCartCalculationStrategy().calculateCart(cartEntryParam);
            }
        }
    }

    @Override
    public void beforeAddToCart(CommerceCartParameter parameters) throws CommerceCartModificationException {
        super.beforeAddToCart(parameters);
    }

    private EntryGroup createEntryGroupStructure(final TmaBundledProductOfferingModel bpo)
    {
        final EntryGroup entryGroup = new EntryGroup();
        entryGroup.setErroneous(Boolean.FALSE);
        entryGroup.setExternalReferenceId(bpo.getCode());
        entryGroup.setGroupType(GroupType.B2CTELCO_BPO);
        entryGroup.setLabel(bpo.getName());
        entryGroup.setChildren(new ArrayList<>());
        return entryGroup;
    }

    private EntryGroup getCorrespondingGroup(final EntryGroup entryGroup, final AbstractOrderModel orderModel,
                                             final Set<Integer> groupNumbers)
    {
        final EntryGroup group = getEntryGroupService().getGroupOfType(orderModel, groupNumbers, GroupType.B2CTELCO_BPO);
        if (StringUtils.equals(group.getExternalReferenceId(), entryGroup.getExternalReferenceId())
                && group.getProcessType().equals(entryGroup.getProcessType()))
        {
            return group;
        }
        return null;
    }

    @Override
    public TmaCompatibilityPolicyEngine getCompatibilityPolicyEngine() {
        return compatibilityPolicyEngine;
    }

    @Override
    public void setCompatibilityPolicyEngine(TmaCompatibilityPolicyEngine compatibilityPolicyEngine) {
        this.compatibilityPolicyEngine = compatibilityPolicyEngine;
    }

    public LLACommonUtil getLlaCommonUtil() {
        return llaCommonUtil;
    }

    public void setLlaCommonUtil(LLACommonUtil llaCommonUtil) {
        this.llaCommonUtil = llaCommonUtil;
    }
}
