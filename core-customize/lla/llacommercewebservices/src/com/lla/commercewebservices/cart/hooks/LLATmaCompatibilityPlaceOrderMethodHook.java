/**
 *
 */
package com.lla.commercewebservices.cart.hooks;

import de.hybris.platform.b2ctelcoservices.compatibility.TmaCompatibilityPolicyEngine;
import de.hybris.platform.b2ctelcoservices.order.hook.TmaCompatibilityPlaceOrderMethodHook;
import de.hybris.platform.commerceservices.order.hook.CommercePlaceOrderMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.InvalidCartException;

import org.springframework.beans.factory.annotation.Autowired;

import com.lla.core.util.LLACommonUtil;

public class LLATmaCompatibilityPlaceOrderMethodHook extends TmaCompatibilityPlaceOrderMethodHook
        implements CommercePlaceOrderMethodHook
{
    private TmaCompatibilityPolicyEngine tmaCompatibilityPolicyEngine;
    @Autowired
    private LLACommonUtil llaCommonUtil;

    /**
     * @param tmaCompatibilityPolicyEngine
     */
    public LLATmaCompatibilityPlaceOrderMethodHook(final TmaCompatibilityPolicyEngine tmaCompatibilityPolicyEngine)
    {
        super(tmaCompatibilityPolicyEngine);
        // XXX Auto-generated constructor stub
    }


    @Override
    public void beforePlaceOrder(final CommerceCheckoutParameter parameter) throws InvalidCartException
    {
        final CartModel cartModel = parameter.getCart();
        if (!llaCommonUtil.isSiteVTR())
        {
            triggerCompatibility(cartModel);
        }
        if (hasValidationErrors(cartModel))
        {
            throw new InvalidCartException(
                    "Cannot place order because cart '" + parameter.getCart().getCode() + "' has compatibility errors.");
        }
    }


    @Override
    protected boolean hasValidationErrors(final CartModel cartModel)
    {
        // XXX Auto-generated method stub
        return super.hasValidationErrors(cartModel);
    }

}
