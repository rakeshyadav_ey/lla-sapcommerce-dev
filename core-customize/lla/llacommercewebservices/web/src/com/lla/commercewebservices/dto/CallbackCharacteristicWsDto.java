package com.lla.commercewebservices.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import java.io.Serializable;


@ApiModel
@Validated
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "valueType",
        "value"
})
public class CallbackCharacteristicWsDto implements Serializable
{
    private final static long serialVersionUID = 1L;
    @JsonProperty("name")
    private String name=null;
    @JsonProperty("valueType")
    private String valueType=null;
    @JsonProperty("value")
    private String value=null;


    public CallbackCharacteristicWsDto name(String name)
    {
        this.name = name;
        return this;
    }

    @ApiModelProperty(value = "name  of the characterstic")
    public String getName()
    {
        return name;
    }


    public void setName(String name)
    {
        this.name = name;
    }


    public CallbackCharacteristicWsDto valueType(String valueType)
    {
        this.valueType = valueType;
        return this;
    }


    @ApiModelProperty(value = "valuetype  of the characterstic")
    public String getValueType()
    {
        return valueType;
    }


    public void setValueType(String valueType)
    {
        this.valueType = valueType;
    }

    public CallbackCharacteristicWsDto value(String value)
    {
        this.value = value;
        return this;
    }

    @ApiModelProperty(value = "value  of the characterstic")
    public String getValue()
    {
        return value;
    }


    public void setValue(String value)
    {
        this.value = value;
    }



}
