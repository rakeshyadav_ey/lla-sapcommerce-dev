package com.lla.commercewebservices.v2.controller;

import com.lla.core.service.c2c.ClickToCallService;

import com.lla.core.service.product.LLATmaPoVariantService;
import com.lla.mulesoft.integration.exception.LLAApiException;
import de.hybris.platform.acceleratorcms.model.components.ClickToCallModel;
import de.hybris.platform.b2ctelcoservices.model.TmaPoVariantModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.RequestParameterException;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.client.Client;
import java.util.List;

@Controller
@Api(tags = "Click to Call Notification for Agent and Customer")
@RequestMapping(value = "/{baseSiteId}/{lang}/c2c")
public class LLAC2CController extends BaseController{

    @Autowired
    private ClickToCallService clickToCallService;
    @Autowired
    private LLATmaPoVariantService llaTmaPoVariantService;


    @RequestMapping(value = "/callMeBack", method = RequestMethod.POST)
    @CacheControl(directive = CacheControlDirective.PRIVATE, maxAge = 120)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @ApiBaseSiteIdParam
    @ApiOperation(value = "Trigger Click to Call for Agent and Customer", notes = "Trigger Click to Call for Agent and Customer")
    public void triggerC2CNotification(
            @ApiParam(value = "Language identifier")  @PathVariable("lang") final String lang,
            @ApiParam(value = "Plan Code")  @RequestParam(required=true) final String planCode,
            @ApiParam(value = "Number of Lines")  @RequestParam(required=true) final Integer numberOfLines,
            @ApiParam(value = "Device Code")  @RequestParam(required=false) final String deviceCode,
            @ApiParam(value = "Customer Name")  @RequestParam(required=true) final String customerName,
            @ApiParam(value = "Customer Last Name")  @RequestParam(required=true) final String customerLastName,
            @ApiParam(value = "Phone Number")  @RequestParam(required=true) final String phoneNumber,
            @ApiParam(value = "Email")  @RequestParam(required=true) final String emailAddress,
            @PathVariable final String baseSiteId){

        try {
             String colorCode=StringUtils.EMPTY;
             String capacityCode=StringUtils.EMPTY;
             if(null != numberOfLines && (numberOfLines > 10 || numberOfLines < 1)){
                 throw new RequestParameterException("Please provide valid value for numberOfLines");
             }
            if(StringUtils.isNotEmpty(deviceCode)){
               TmaPoVariantModel poVariant= llaTmaPoVariantService.getTmaPoVariant(deviceCode);
               final List<CategoryModel> categoryList= (List<CategoryModel>) poVariant.getSupercategories();
               for(CategoryModel category:categoryList){
                  if(category.getSupercategories().get(0).getCode().equals("color")){
                      colorCode=category.getName();
                  }
                  if(category.getSupercategories().get(0).getCode().equals("capacity")){
                      capacityCode=category.getName();
                  }
               }
            }
            ClickToCallModel model=clickToCallService.triggerC2CEmailToAgent_Customer(null,null,lang,baseSiteId,planCode,numberOfLines,deviceCode,customerLastName,customerName,phoneNumber,emailAddress,colorCode,capacityCode);
        } catch (LLAApiException exception) {
            exception.printStackTrace();
        }

    }
}
