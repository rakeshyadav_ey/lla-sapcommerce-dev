package com.lla.commercewebservices.dto;

import java.io.Serializable;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel
@Validated
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
{ "id", "type" })
public class CallbackAccountWsDto implements Serializable
{
	private final static long serialVersionUID = 1L;
	@JsonProperty("id")
	private String id = null;
	@JsonProperty("type")
	private String type = null;


	public CallbackAccountWsDto id(final String id)
	{
		this.id = id;
		return this;
	}

	@ApiModelProperty(value = "id  of the account")
	public String getId()
	{
		return id;
	}


	public void setId(final String id)
	{
		this.id = id;
	}

	public CallbackAccountWsDto type(final String type)
	{
		this.type = type;
		return this;
	}

	@ApiModelProperty(value = "type  of the account")
	public String getType()
	{
		return type;
	}


	public void setType(final String type)
	{
		this.type = type;
	}

}
