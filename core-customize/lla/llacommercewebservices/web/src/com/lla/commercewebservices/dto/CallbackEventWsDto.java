package com.lla.commercewebservices.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import java.io.Serializable;


@ApiModel
@Validated
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "customer"
})
public class CallbackEventWsDto implements Serializable
{
    private final static long serialVersionUID = 1L;
    @JsonProperty("customer")
    private CallbackCustomerWsDto customer = null;


    public CallbackEventWsDto customer(CallbackCustomerWsDto customer)
    {
        this.customer = customer;
        return this;
    }

    @ApiModelProperty(value = "customer  of the event")
    public CallbackCustomerWsDto getCustomer()
    {
        return customer;
    }

    public void setCustomer(CallbackCustomerWsDto customer)
    {
        this.customer = customer;
    }



}
