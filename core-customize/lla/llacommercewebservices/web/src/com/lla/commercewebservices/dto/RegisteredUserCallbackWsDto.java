package com.lla.commercewebservices.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "correlationId",
        "description",
        "domain",
        "eventId",
        "eventTime",
        "eventType",
        "event"
})
public class RegisteredUserCallbackWsDto implements Serializable
{
    private final static long serialVersionUID = 1L;

    @JsonProperty("correlationId")
    private String correlationId = null;
    @JsonProperty("description")
    private String description = null;
    @JsonProperty("domain")
    private String domain = null;
    @JsonProperty("eventId")
    private String eventId = null;
    @JsonProperty("eventTime")
    private String eventTime = null;
    @JsonProperty("eventType")
    private String eventType = null;
    @JsonProperty("event")
    private CallbackEventWsDto event = null;

    public RegisteredUserCallbackWsDto correlationId(String correlationId)
    {
        this.correlationId = correlationId;
        return this;
    }

    @ApiModelProperty(value = "correlation of the customer")
    public String getCorrelationId()
    {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }


    public RegisteredUserCallbackWsDto description(String description)
    {
        this.description = description;
        return this;
    }

    @ApiModelProperty(value = "description of the customer")
    public String getDescription()
    {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }

    public RegisteredUserCallbackWsDto domain(String domain)
    {
        this.domain = domain;
        return this;
    }

    @ApiModelProperty(value = "domain of the customer")
    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public RegisteredUserCallbackWsDto eventId(String eventId)
    {
        this.eventId = eventId;
        return this;
    }

    @ApiModelProperty(value = "eventId of the customer")
    public String getEventId()
    {
        return eventId;
    }


    public void setEventId(String eventId)
    {
        this.eventId = eventId;
    }

    public RegisteredUserCallbackWsDto eventTime(String eventTime)
    {
        this.eventTime = eventTime;
        return this;
    }

    @ApiModelProperty(value = "event time of the customer")
    public String getEventTime()
    {
        return eventTime;
    }

    public void setEventTime(String eventTime)
    {
        this.eventTime = eventTime;
    }

    public RegisteredUserCallbackWsDto eventType(String eventType)
    {
        this.eventType = eventType;
        return this;
    }


    @ApiModelProperty(value = "event type of the customer")
    public String getEventType()
    {
        return eventType;
    }


    public void setEventType(String eventType)
    {
        this.eventType = eventType;
    }


    public RegisteredUserCallbackWsDto event(CallbackEventWsDto event)
    {
        this.event = event;
        return this;
    }
    @ApiModelProperty(value = "event  of the customer")
    public CallbackEventWsDto getEvent()
    {
        return event;
    }

    @JsonProperty("event")
    public void setEvent(CallbackEventWsDto event)
    {
        this.event = event;
    }

}
