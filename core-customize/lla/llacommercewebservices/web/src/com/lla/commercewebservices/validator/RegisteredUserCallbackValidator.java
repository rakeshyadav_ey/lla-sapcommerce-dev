/**
 *
 */
package com.lla.commercewebservices.validator;

import org.apache.commons.lang.StringUtils;

import com.lla.commercewebservices.dto.RegisteredUserCallbackWsDto;


/**
 * @author LU836YG
 *
 */
public class RegisteredUserCallbackValidator
{
	private static final String CUSTOMER_STATUS = "Ok";

	public StringBuffer validate(final RegisteredUserCallbackWsDto user)
	{
		final StringBuffer errorMessage = new StringBuffer("Please send valid ");

		if (StringUtils.isEmpty(user.getCorrelationId()))
		{
			return errorMessage.append("correlationID ");
		}
		else if (user.getEvent().equals(null))
		{
			return errorMessage.append("event details");

		}
		else if (user.getEvent().getCustomer().equals(null))
		{
			return errorMessage.append("customer details");

		}
		else if (StringUtils.isEmpty(user.getEvent().getCustomer().getId()))
		{
			return errorMessage.append("customer id");

		}
		else if (StringUtils.isEmpty(user.getEvent().getCustomer().getStatus()))
		{
			return errorMessage.append("customer status");

		}
		else if (!(user.getEvent().getCustomer().getStatus().equalsIgnoreCase(CUSTOMER_STATUS)))
		{
			return errorMessage.append("customer status as Ok");
		}
		return null;

	}

}
