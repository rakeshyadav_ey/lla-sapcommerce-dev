package com.lla.commercewebservices.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import java.io.Serializable;
import java.util.List;


@ApiModel
@Validated
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "status",
        "statusReason",
        "account",
        "characteristic"
})
public class CallbackCustomerWsDto implements Serializable
{
    private final static long serialVersionUID = 1L;

    @JsonProperty("id")
    private String id = null;
    @JsonProperty("name")
    private String name = null;
    @JsonProperty("status")
    private String status = null;
    @JsonProperty("statusReason")
    private String statusReason = null;
    @JsonProperty("account")
    private List<CallbackAccountWsDto> account = null;
    @JsonProperty("characteristic")
    private List<CallbackCharacteristicWsDto> characteristic = null;


    public CallbackCustomerWsDto id(String id)
    {
        this.id = id;
        return this;
    }

    @ApiModelProperty(value = "id  of the customer")
    public String getId()
    {
        return id;
    }


    public void setId(String id)
    {
        this.id = id;
    }


    public CallbackCustomerWsDto name(String name)
    {
        this.name = name;
        return this;
    }

    @ApiModelProperty(value = "name  of the customer")
    public String getName()
    {
        return name;
    }


    public void setName(String name)
    {
        this.name = name;
    }

    public CallbackCustomerWsDto status(String status)
    {
        this.status = status;
        return this;
    }


    @ApiModelProperty(value = "status  of the customer")
    public String getStatus()
    {
        return status;
    }


    public void setStatus(String status)
    {
        this.status = status;
    }

    public CallbackCustomerWsDto statusReason(String statusReason)
    {
        this.statusReason = statusReason;
        return this;
    }

    @ApiModelProperty(value = "reason  of the customer")
    public String getStatusReason()
    {
        return statusReason;
    }


    public void setStatusReason(String statusReason)
    {
        this.statusReason = statusReason;
    }



    public CallbackCustomerWsDto account(List<CallbackAccountWsDto> account)
    {
        this.account = account;
        return this;
    }

    @ApiModelProperty(value = "account  of the customer")
    public List<CallbackAccountWsDto> getAccount()
    {
        return account;
    }


    public void setAccount(List<CallbackAccountWsDto> account)
    {
        this.account = account;
    }


    public CallbackCustomerWsDto characteristic(List<CallbackCharacteristicWsDto> characteristic)
    {
        this.characteristic = characteristic;
        return this;
    }

    @ApiModelProperty(value = "characterstic  of the customer")
    public List<CallbackCharacteristicWsDto> getCharacteristic()
    {
        return characteristic;
    }



    public void setCharacteristic(List<CallbackCharacteristicWsDto> characteristic)
    {
        this.characteristic = characteristic;
    }


}
