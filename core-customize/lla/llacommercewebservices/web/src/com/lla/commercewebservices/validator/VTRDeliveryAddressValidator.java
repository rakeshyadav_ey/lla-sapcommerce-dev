package com.lla.commercewebservices.validator;

import de.hybris.platform.commercewebservicescommons.dto.user.AddressWsDTO;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class VTRDeliveryAddressValidator implements Validator
{
    private static final String FIELD_REQUIRED_AND_NOT_TOO_LONG_MESSAGE_ID = "vtr.address.validation";
    private static final String DATE_OF_BIRTH="dateOfBirth";
    private static final String CUSTOMER_ID="customerIDCard";
    private static final String ADDRESS_ID="addressID";
    private static final String FIRST_NAME="firstName";
    private static final String LAST_NAME="lastName";
    private static final String EMAIL="email";

    @Override
    public boolean supports(Class<?> aClass) {
        return AddressWsDTO.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors)
    {
        final AddressWsDTO address = (AddressWsDTO) o;
        Assert.notNull(errors, "Errors object must not be null");
        if(StringUtils.isEmpty(address.getAddressID()) || StringUtils.length(address.getAddressID()) <= 2)
        {
           errors.rejectValue(ADDRESS_ID,FIELD_REQUIRED_AND_NOT_TOO_LONG_MESSAGE_ID);
        }

        if(StringUtils.isEmpty(address.getFirstName()))
        {
            errors.rejectValue(FIRST_NAME,FIELD_REQUIRED_AND_NOT_TOO_LONG_MESSAGE_ID);
        }
        if(StringUtils.isEmpty(address.getLastName()))
        {
            errors.rejectValue(LAST_NAME,FIELD_REQUIRED_AND_NOT_TOO_LONG_MESSAGE_ID);
        }
        if(StringUtils.isEmpty(address.getEmail()))
        {
            errors.rejectValue(EMAIL,FIELD_REQUIRED_AND_NOT_TOO_LONG_MESSAGE_ID);
        }
        if(StringUtils.isEmpty(address.getDateOfBirth()))
        {
            errors.rejectValue(DATE_OF_BIRTH,FIELD_REQUIRED_AND_NOT_TOO_LONG_MESSAGE_ID);
        }
        if(StringUtils.isEmpty(address.getCustomerIDCard()))
        {
            errors.rejectValue(CUSTOMER_ID,FIELD_REQUIRED_AND_NOT_TOO_LONG_MESSAGE_ID);
        }
    }
}
