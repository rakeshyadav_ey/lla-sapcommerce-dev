package com.lla.commercewebservices.v2.controller;

import com.lla.core.model.LLABundleProductMappingModel;
import com.lla.facades.populators.LLABundleMappingPopulator;
import com.lla.facades.product.LLATmaProductFacade;
import com.lla.facades.productconfigurator.data.LLABundleProductMappingData;
import com.lla.facades.productconfigurator.data.LLAPuertoRicoProductConfigDTO;
import com.lla.facades.util.LLAConfiguratorUtil;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.commercefacades.product.data.ProductConfiguratorResponseData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.request.mapping.annotation.ApiVersion;
import de.hybris.platform.commercewebservicescommons.dto.order.CartWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.product.ProductConfigWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.product.ProductConfiguratorResponseWSDTO;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.CartException;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@ApiVersion("v2")
@Api(tags = "Product Configurator")
@RequestMapping(value = "/{baseSiteId}/{lang}")
    public class ProductConfiguratorController extends BaseController{
    public static final String STRING = "string";
    private static final String INTERNETCATEGORY = "internet";
    private static final String TVCATEGORY = "tv";
    private static final String PHONECATEGORY = "phone";
    private static final String ADDONPAGELABEL = "configureProduct/selectAddons";
    private static final String SMARTPRODUCTCATEGORY = "smartproduct";
    public static final String ONEP = "internet_500";
    public static final String TWOP = "tv_ultimate";
    public static final String PRODUCT_CODES = "product.code.add";
    public static final String ONEP_90 = "internet_090";
    @Resource(name = "llaTmaProductFacade")
    private LLATmaProductFacade llaTmaProductFacade;
    @Resource(name = "productConverter")
    private Converter<ProductModel, ProductData> productConverter;
    @Resource(name = "configurationService")
    private ConfigurationService configurationService;
    @Resource(name = "llaCartFacade")
    private com.lla.facades.cart.LLACartFacade llaCartFacade;
    @Autowired
    private LLABundleMappingPopulator llaBundleMappingPopulator;
    @Autowired
    private LLAConfiguratorUtil llaConfiguratorUtill;
    @Autowired
    private CommonI18NService commonI18NService;

    @RequestMapping(value = "/getCurrentStepDetails", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE })
    @CacheControl(directive = CacheControlDirective.PRIVATE, maxAge = 120)
    @Cacheable(value = "productCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(true,true,true,true,true,#productCode,#processType,#userId,#fields,#region)")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @ApiBaseSiteIdParam
    @ApiOperation(value = "Get Plan details for Current Configurator Step", notes = "Returns details of a plans available for configurator current step.")
    public ProductConfiguratorResponseWSDTO getPlanDetails(
            @ApiParam(value = "Current Step") @RequestParam(required = false)  final int currentStep,
            @ApiParam(value = "ProductConfigWSDTO object.") @RequestBody final ProductConfigWsDTO productConfigWSDTO,
            @ApiParam(value = "Language identifier")  @RequestParam(required = false) final String lang,
            @ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL") @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields)
    {
        return getProductConfiguratorResponseWSDTO(currentStep, productConfigWSDTO, fields, purgeProductConfigDto(getDataMapper().map(productConfigWSDTO, LLAPuertoRicoProductConfigDTO.class, fields)));
    }

    @RequestMapping(value = "/savePlanDetails", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE })
    @CacheControl(directive = CacheControlDirective.PRIVATE, maxAge = 120)
    @Cacheable(value = "productCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(true,true,true,true,true,#productCode,#processType,#userId,#fields,#region)")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @ApiBaseSiteIdParam
    @ApiOperation(value = "Update Plan details after modification in existing Configured Plans", notes = "Update configured plans details.")
    public ProductConfiguratorResponseWSDTO savePlanDetails(
            @ApiParam(value = "Selected Product identifier") @RequestParam(required = false) final String selectedProductCode,
            @ApiParam(value = "Quantity") @RequestParam(required = false, defaultValue = "1") int quantity,
            @ApiParam(value = "Current Step") @RequestParam(required = false) int currentStep,
            @ApiParam(value = "Selected Addon identifier") @RequestParam(required = false) final String selectedAddonProductCode,
            @ApiParam(value = "Selected Channel identifier") @RequestParam(required = false) final String selectedChannel,
            @ApiParam(value = "Selected Smart Protect identifier") @RequestParam(required = false) final String selectedSmartProtect,
            @ApiParam(value = "Language identifier")  @RequestParam(required = false) final String lang,
            @ApiParam(value = "ProductConfigWSDTO object.") @RequestBody ProductConfigWsDTO productConfigWSDTO,
            @ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL") @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields) throws CalculationException {
        LLAPuertoRicoProductConfigDTO productConfig=purgeProductConfigDto(getDataMapper().map(productConfigWSDTO, LLAPuertoRicoProductConfigDTO.class, fields));
        switch (currentStep){
            case 1:
                productConfig.setOneP(selectedProductCode);
                break;
            case 2:
                productConfig.setTwoP(selectedProductCode);
                if (StringUtils.isNotEmpty(productConfig.getOneP()) && !productConfig.getOneP().equals(STRING) && StringUtils.isNotEmpty(productConfig.getTwoP()) && !productConfig.getTwoP().equals(STRING))
                {
                    productConfig.setThreeP(llaTmaProductFacade.getProductForCategory(PHONECATEGORY).get(0).getCode());
                }
                break;
            case 3:
                if(StringUtils.isBlank(productConfig.getThreeP()))
                    productConfig.setThreeP(selectedProductCode);
                break;
            case 4:
                if(null != selectedAddonProductCode){
                    llaConfiguratorUtill.saveAddon(selectedAddonProductCode, quantity, productConfig);
                }
                saveTVChannelAddons(selectedChannel, selectedSmartProtect, productConfig);
                currentStep--;
                break;
        }
        currentStep++;
        return getProductConfiguratorResponseWSDTO(currentStep, getDataMapper().map(productConfig, ProductConfigWsDTO.class, fields), fields, productConfig);
    }

    @RequestMapping(value = "/users/{userId}/carts/{cartId}/addProductConfiguratorToCart", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE })
    @CacheControl(directive = CacheControlDirective.PRIVATE, maxAge = 120)
    @Cacheable(value = "productCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(true,true,true,true,true,#productCode,#processType,#userId,#fields,#region)")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @ApiBaseSiteIdParam
    @ApiOperation(value = "Add configured bundle package to cart", notes = "Add configured bundle package to cart.")
    public CartWsDTO addProductConfiguratorToCart(
            @ApiParam(value = "Language identifier")  @PathVariable("lang") final String lang,
            @ApiParam(value = "ProductConfigWSDTO object.") @RequestBody ProductConfigWsDTO productConfigWSDTO,
            @ApiParam(value = "Replace products in cart.") @RequestParam(required = false) boolean updateCartFlag,
            @ApiParam(value = "Cart identifier: cart code for logged in user, cart guid for anonymous user, 'current' for the last modified cart", required = true) @PathVariable final String cartId,
            @ApiParam(value = "Identifier of the Customer", required = true) @PathVariable("userId") final String userId,
            @ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL") @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields) throws CalculationException {
        LLAPuertoRicoProductConfigDTO productConfig=purgeProductConfigDto(getDataMapper().map(productConfigWSDTO, LLAPuertoRicoProductConfigDTO.class, fields));
        if(llaCartFacade.hasSessionCart() && !updateCartFlag && llaCartFacade.getSessionCart().getEntries().size() > 0){
            //pass this as a message n exception
            throw new CartException("You need to delete product from cart");
        }else if(updateCartFlag){
            llaCartFacade.removeSessionCart();
        }
        llaCartFacade.addConfiguratorProductsToCart(productConfig, 1, "ACQUISITION", null, -1,
                null, null, null);
        return getDataMapper().map(llaCartFacade.getSessionCart(), CartWsDTO.class, fields);
    }



    @RequestMapping(value = "/reconfigurePlanDetails", method = RequestMethod.DELETE)
    @CacheControl(directive = CacheControlDirective.PRIVATE, maxAge = 120)
    @Cacheable(value = "productCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(true,true,true,true,true,#productCode,#processType,#userId,#fields,#region)")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @ApiBaseSiteIdParam
    @ApiOperation(value = "Reconfigure Plan details after modification in existing Configured Plans", notes = "Re-configure plans details.")
    public ProductConfiguratorResponseWSDTO reconfigurePlanDetails(
            @ApiParam(value = "Removed Product from Configurator") @RequestParam(required = false) final String deletedProductCode,
            @ApiParam(value = "Current Step") @RequestParam(required = false) final int currentStep,
            @ApiParam(value = "addonProducts") @RequestParam(required = false) final List<String> addonProducts,
            @ApiParam(value="selectedChannel") @RequestParam(required = false) final String deleteChannel,
            @ApiParam(value = "ProductConfigWSDTO object.") @RequestBody final ProductConfigWsDTO productConfigWSDTO,
            @ApiParam(value = "Language identifier")  @RequestParam(required = false) final String lang,
            @ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL") @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields)
    {
        LLAPuertoRicoProductConfigDTO productConfig=getDataMapper().map(productConfigWSDTO, LLAPuertoRicoProductConfigDTO.class, fields);
        switch (currentStep) {
            case 1:
                productConfig = new LLAPuertoRicoProductConfigDTO();
                break;
            case 2:
                if(StringUtils.isNotEmpty(deletedProductCode)) {
                    productConfig.setTwoP(StringUtils.EMPTY);
                    productConfig.setThreeP(StringUtils.EMPTY);
                }else if(CollectionUtils.isNotEmpty(addonProducts)){
                    llaConfiguratorUtill.removeAddons(addonProducts,productConfig);
                }
                break;
            case 4 :
                if(StringUtils.isNotEmpty(deleteChannel)){
                    List<String> channelList= productConfig.getChannelList();
                    channelList.remove(deleteChannel);
                    productConfig.setChannelList(channelList);
                }
        }
        return getProductConfiguratorResponseWSDTO(currentStep, getDataMapper().map(productConfig, ProductConfigWsDTO.class, fields), fields, productConfig);
    }

    private List<ProductModel> filterTvPlan(final List<ProductModel> productForCategory, final LLAPuertoRicoProductConfigDTO productConfig) {
        return new ArrayList<>(llaTmaProductFacade.getTvProductsForInternet(productConfig.getOneP()));
    }

    private List<ProductData> sortproduct(List<ProductData> productDataForCategory) {
        return productDataForCategory.stream().sorted(Comparator.comparing(ProductData::getCode)).collect(Collectors.toList());
    }

    /**
     * @param productConfig
     * @return
     */
    private List<ProductData> getProductDatas(LLAPuertoRicoProductConfigDTO productConfig)
    {
        List<String> productCodes = new ArrayList<String>(Arrays.asList(productConfig.getOneP(),productConfig.getTwoP(),productConfig.getTwoPHUB(),productConfig.getTwoPAddonHD(),productConfig.getTwoPAddonDVR(),productConfig.getThreeP()));
        productCodes.addAll(null!=productConfig.getChannelList()?productConfig.getChannelList() : CollectionUtils.EMPTY_COLLECTION);
        List<TmaProductOfferingModel> productDatas = productCodes.stream().filter(StringUtils::isNotEmpty).map(llaTmaProductFacade::getProductForCode).collect(Collectors.toList());
        return llaTmaProductFacade.getProductData(productDatas);
    }

    private void saveTVChannelAddons(@RequestParam(required = false) @ApiParam("Selected Channel identifier") String selectedChannel, @RequestParam(required = false) @ApiParam("Selected Smart Protect identifier") String selectedSmartProtect, LLAPuertoRicoProductConfigDTO productConfig) throws CalculationException {
        if(StringUtils.isNotEmpty(selectedChannel)){
            List<String> channelList= new ArrayList<>();
            if(CollectionUtils.isNotEmpty(productConfig.getChannelList()))
                channelList.addAll(productConfig.getChannelList());
            channelList.add(selectedChannel);
            productConfig.setChannelList(channelList);
        }else if(StringUtils.isNotEmpty(selectedSmartProtect)){
            List<String> smartProtect= new ArrayList<>();
            if(CollectionUtils.isNotEmpty(productConfig.getSmartProductList()))
                smartProtect.addAll(productConfig.getSmartProductList());
            smartProtect.add(selectedSmartProtect);
            productConfig.setSmartProductList(smartProtect);
        }
    }

    private ProductConfiguratorResponseWSDTO getProductConfiguratorResponseWSDTO(int currentStep,  ProductConfigWsDTO productConfigWSDTO,
                                                                                 String fields, LLAPuertoRicoProductConfigDTO productConfig) {
        ProductConfiguratorResponseData responseData = new ProductConfiguratorResponseData();
        ProductConfiguratorResponseWSDTO responseWSDTO;
        List<LLABundleProductMappingModel> productMapping;
        switch (currentStep) {
            case 1:
                responseData.setBlueBoxMessages(configurationService.getConfiguration().getString("peacock.product.configurator.default.message."+commonI18NService.getCurrentLanguage().getIsocode()));
                return getProductConfiguratorResponseWSDTO(currentStep, productConfigWSDTO, fields, productConfig, responseData, INTERNETCATEGORY);
            case 2:
                responseData.setActiveProductList(productConverter.convertAll(filterTvPlan(llaTmaProductFacade.getProductForCategory(TVCATEGORY), productConfig)));
                responseData.setBlueBoxMessages(llaTmaProductFacade.getBundleMessage(productConfig, currentStep));
                return getProductConfiguratorResponseWSDTO(currentStep, productConfigWSDTO, fields, productConfig, responseData, TVCATEGORY);
            case 3:
                responseData.setBlueBoxMessages(llaTmaProductFacade.getBundleMessage(productConfig, currentStep));
                return getProductConfiguratorResponseWSDTO(currentStep, productConfigWSDTO, fields, productConfig, responseData, PHONECATEGORY);
            case 4:
                responseData.setSmartProductsList(sortproduct(llaTmaProductFacade.getProductDataForCategory(SMARTPRODUCTCATEGORY)));
                responseData.setCurrentStep(String.valueOf(currentStep));
                responseData.setBlueBoxMessages(llaTmaProductFacade.getBundleMessage(productConfig, currentStep));
                productMapping = llaCartFacade.getBundleProductMappingForCart(productConfig);
                if (!productMapping.isEmpty()) {
                    LLABundleProductMappingData target = new LLABundleProductMappingData();
                    llaBundleMappingPopulator.populate(productMapping.get(0), target);
                    responseData.setBundleProductMapping(target);
                }

                if (StringUtils.isNotEmpty(productConfig.getOneP()) || StringUtils.isNotEmpty(productConfig.getThreeP())) {
                    responseData.setSelectedProductsList(getProductDatas(productConfig));
                }
                if (StringUtils.isNotEmpty(productConfig.getTwoP())) {
                    final Collection<TmaProductOfferingModel> channelProductList = llaTmaProductFacade.getProductForCode(productConfig.getTwoP()).getChannelProductList();
                    final List<TmaProductOfferingModel> finalProducts = productConfig.getTwoP().contains(TWOP) ? Arrays.asList(StringUtils.split(configurationService.getConfiguration().getString(PRODUCT_CODES), ",")).stream().map(llaTmaProductFacade::getProductForCode).collect(Collectors.toList()).stream().filter(fp -> channelProductList.contains(fp)).collect(Collectors.toList()) : Lists.newArrayList();
                    responseData.setChannelList(llaConfiguratorUtill.getProductDataForList(StringUtils.isNotEmpty(productConfig.getOneP()) && productConfig.getOneP().contains(ONEP) && (!CollectionUtils.sizeIsEmpty(finalProducts)) ? finalProducts : channelProductList));
                    responseData.setSelectedProductsList(getProductDatas(productConfig));
                }
                if ((!(ONEP_90.equalsIgnoreCase(productConfig.getOneP()))) && StringUtils.isNotEmpty(productConfig.getTwoP())) {

                    responseData.setAddonProductList(llaConfiguratorUtill.getProductDataForList(llaConfiguratorUtill.getAddons(productConfig)));
                    responseData.setAddonProductMaxQuantity(llaConfiguratorUtill.getMaxQuantityForAddons(productConfig));
                    responseData.setSelectedProductsList(getProductDatas(productConfig));
                }
                responseWSDTO = getDataMapper().map(responseData, ProductConfiguratorResponseWSDTO.class, fields);
                responseWSDTO.setProductConfigWSDTO(productConfigWSDTO);

                return responseWSDTO;
            default:
                throw new IllegalStateException("Unexpected value: " + currentStep);
        }
    }

    private LLAPuertoRicoProductConfigDTO purgeProductConfigDto(LLAPuertoRicoProductConfigDTO productConfig) {
        if(StringUtils.isNotBlank(productConfig.getOneP()) && productConfig.getOneP().equals(STRING)){
            productConfig.setOneP(null);
        }
        if(StringUtils.isNotBlank(productConfig.getOnePAddon()) && productConfig.getOnePAddon().equals(STRING)){
            productConfig.setOnePAddon(null);
        }
        if(StringUtils.isNotBlank(productConfig.getTwoP()) && productConfig.getTwoP().equals(STRING)){
            productConfig.setTwoP(null);
        }
        if(StringUtils.isNotBlank(productConfig.getTwoPAddonDVR()) && productConfig.getTwoPAddonDVR().equals(STRING)){
            productConfig.setTwoPAddonDVR(null);
        }
        if(StringUtils.isNotBlank(productConfig.getTwoPAddonHD()) && productConfig.getTwoPAddonHD().equals(STRING)){
            productConfig.setTwoPAddonHD(null);
        }
        if(StringUtils.isNotBlank(productConfig.getTwoPHUB()) && productConfig.getTwoPHUB().equals(STRING)){
            productConfig.setTwoPHUB(null);
        }
        if(StringUtils.isNotBlank(productConfig.getThreeP()) && productConfig.getThreeP().equals(STRING)){
            productConfig.setThreeP(null);
        }
        if(CollectionUtils.isNotEmpty(productConfig.getChannelList())){
            List<String> channelList= new ArrayList<>();
            productConfig.getChannelList().stream().forEach(channel -> {
               if(!channel.equals(STRING))
                   channelList.add(channel);
            });
            productConfig.setChannelList(channelList);
        }
        if(CollectionUtils.isNotEmpty(productConfig.getSmartProductList())){
            List<String> smartProductList= new ArrayList<>();
            productConfig.getSmartProductList().stream().forEach(smartProduct -> {
                if(!smartProduct.equals(STRING))
                    smartProductList.add(smartProduct);
            });
            productConfig.setSmartProductList(smartProductList);
        }
        if(null !=productConfig.getAddonProductsQuantity() && productConfig.getAddonProductsQuantity().keySet().contains("additionalProp1")){
            productConfig.setAddonProductsQuantity(new HashMap<String, Integer>());
        }
        return productConfig;
    }

    private ProductConfiguratorResponseWSDTO getProductConfiguratorResponseWSDTO(int currentStep, ProductConfigWsDTO productConfigWSDTO, String fields, LLAPuertoRicoProductConfigDTO productConfig, ProductConfiguratorResponseData responseData, String phonecategory) {
        List<LLABundleProductMappingModel> productMapping;
        ProductConfiguratorResponseWSDTO responseWSDTO;
        responseData.setProductList(sortproduct(llaTmaProductFacade.getProductDataForCategory(phonecategory)));
        responseData.setCurrentStep(String.valueOf(currentStep));
        productMapping = llaCartFacade.getBundleProductMappingForCart(productConfig);
        if (!productMapping.isEmpty()) {
            LLABundleProductMappingData target = new LLABundleProductMappingData();
            llaBundleMappingPopulator.populate(productMapping.get(0), target);
            responseData.setBundleProductMapping(target);
        }
        responseWSDTO = getDataMapper().map(responseData, ProductConfiguratorResponseWSDTO.class, fields);
        responseWSDTO.setProductConfigWSDTO(productConfigWSDTO);
        return responseWSDTO;
    }


}