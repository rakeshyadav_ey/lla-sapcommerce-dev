/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.constants;

/**
 * Global class for all Llaamazoncloud constants. You can add global constants for your extension into this class.
 */
public final class LlaamazoncloudConstants extends GeneratedLlaamazoncloudConstants
{
	public static final String EXTENSIONNAME = "llaamazoncloud";

	private LlaamazoncloudConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "llaamazoncloudPlatformLogo";
}
