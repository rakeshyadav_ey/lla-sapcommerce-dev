/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.setup;

import static com.lla.constants.LlaamazoncloudConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.lla.constants.LlaamazoncloudConstants;
import com.lla.service.LlaamazoncloudService;


@SystemSetup(extension = LlaamazoncloudConstants.EXTENSIONNAME)
public class LlaamazoncloudSystemSetup
{
	private final LlaamazoncloudService llaamazoncloudService;

	public LlaamazoncloudSystemSetup(final LlaamazoncloudService llaamazoncloudService)
	{
		this.llaamazoncloudService = llaamazoncloudService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		llaamazoncloudService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return LlaamazoncloudSystemSetup.class.getResourceAsStream("/llaamazoncloud/sap-hybris-platform.png");
	}
}
