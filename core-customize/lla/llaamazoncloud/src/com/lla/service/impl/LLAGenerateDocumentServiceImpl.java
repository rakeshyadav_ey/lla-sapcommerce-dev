/*
 *   Copyright (c) 2019 SAP SE or an SAP affiliate company.  All rights reserved.
 */
package com.lla.service.impl;

import au.com.bytecode.opencsv.CSVWriter;
import com.lla.service.LLAGenerateDocumentService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.util.Config;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * The type Lla generate document service.
 */
public class LLAGenerateDocumentServiceImpl implements LLAGenerateDocumentService {

    private static final Logger LOG = LoggerFactory.getLogger(LLAGenerateDocumentServiceImpl.class);

    private static final String CSV_DIRECTORY = Config.getParameter("lla.s3.csv.file.directory");
    private static final String SITE = Config.getParameter("lla.csv.header.site");
    private static final String CART_ID = Config.getParameter("lla.csv.header.code");
    private static final String CREATION_TIME = Config.getParameter("lla.csv.header.creationTime");
    private static final String SESSION_ID = Config.getParameter("lla.csv.header.sessionId");
    private static final String UID = Config.getParameter("lla.csv.header.user");
    private static final String EMAIL = Config.getParameter("lla.csv.header.customer.email");
    private static final String USER_NAME = Config.getParameter("lla.csv.header.user.name");
    private static final String FIXED_PHONE = Config.getParameter("lla.csv.header.customer.phone");
    private static final String MOBILE = Config.getParameter("lla.csv.header.customer.mobile");
    private static final String PHONE1 = Config.getParameter("lla.csv.header.address.phone1");
    private static final String PHONE2 = Config.getParameter("lla.csv.header.address.phone2");
    private static final String CELL_PHONE = Config.getParameter("lla.csv.header.address.cellphone");
    private static final String DOCUMENT_NO = Config.getParameter("lla.csv.header.customer.documentNo");
    private static final String TRN_NO = Config.getParameter("lla.csv.header.customer.trnNo");
    private static final String INSTALL_PHONE1 = Config.getParameter("lla.csv.header.installation.address.phone1");
    private static final String INSERT_UPDATE = Config.getParameter("lla.csv.header.insert.update");
    private static final String CREATION_TIME_FORMAT="dd/MM/yyyy";
    private static final String DATE_FORMAT="yyyy-dd-M_HH-mm-ss";
    private static final String CSV=".csv";
    private static final String SUFFIX="/";
    private static final String UNDERSCORE="_";

    /**
     * @param abandonedCart
     * @param fileName
     * @throws IOException
     */
    @Override
    public void generateDocument(List<CartModel> abandonedCart, String fileName) throws IOException {

        LOG.info("Inside Generate Document Method:fileName is [" + fileName + "]");
        List<String[]> csvDataList =new ArrayList<>();
        String[] headers = new String[]{ INSERT_UPDATE,SITE, CART_ID, CREATION_TIME, SESSION_ID, UID, EMAIL,USER_NAME, FIXED_PHONE,MOBILE,PHONE1,PHONE2,CELL_PHONE,DOCUMENT_NO,TRN_NO,INSTALL_PHONE1};
        csvDataList.add(headers);
        StringBuffer csvFileName = new StringBuffer(fileName);
        final StringBuffer formattedFilename = csvFileName.append(UNDERSCORE).append(DateTime.now().toString(DATE_FORMAT)).append(CSV);
        final StringBuffer csvFilePath = new StringBuffer(CSV_DIRECTORY).append(SUFFIX).append(formattedFilename);
        File csvFile = new File(String.valueOf(csvFilePath));
        csvFile.createNewFile();
        CSVWriter writer = new CSVWriter(new FileWriter(String.valueOf(csvFilePath)));
        if (CollectionUtils.isNotEmpty(abandonedCart)) {
            for (CartModel cart : abandonedCart) {
                List<String> data=createCsvData(cart);
                csvDataList.add(data.toArray(new String[data.size()]));
            }
            try {
                if (CollectionUtils.isNotEmpty(csvDataList)) {
                    writer.writeAll(csvDataList);
                    writer.close();
                }
            } catch (Exception e) {
                LOG.error("Error occured while generating document with message [" + e.getMessage() + "]");
            }
        }
    }

    private List <String> createCsvData(CartModel cart) {

        LOG.info("Inside Create CSV Data Method:- ");
        List<String> valueList = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat(CREATION_TIME_FORMAT);
        final CustomerModel customer = (CustomerModel) cart.getUser();
        valueList.add(StringUtils.EMPTY);
        valueList.add(cart.getSite().getUid());
        valueList.add(cart.getCode());
        valueList.add(dateFormat.format(cart.getCreationtime()));
        valueList.add(StringUtils.isNotEmpty(cart.getSessionId()) ? cart.getSessionId() : StringUtils.EMPTY);
        valueList.add(StringUtils.isNotEmpty(customer.getUid()) ? customer.getUid() : StringUtils.EMPTY);
        valueList.add(StringUtils.isNotEmpty(customer.getAdditionalEmail()) ? customer.getAdditionalEmail() : StringUtils.EMPTY);
        valueList.add(StringUtils.isNotEmpty(customer.getName()) ? customer.getName() : StringUtils.EMPTY);
        valueList.add(StringUtils.isNotEmpty(customer.getFixedPhone()) ? customer.getFixedPhone() : StringUtils.EMPTY);
        valueList.add(StringUtils.isNotEmpty(customer.getMobilePhone()) ? customer.getMobilePhone() : StringUtils.EMPTY);

        if (null != cart.getDeliveryAddress()) {
            AddressModel address = cart.getDeliveryAddress();
            valueList.add(StringUtils.isNotEmpty(address.getPhone1()) ? address.getPhone1() : StringUtils.EMPTY);
            valueList.add(StringUtils.isNotEmpty(address.getPhone2()) ? address.getPhone2() : StringUtils.EMPTY);
            valueList.add(StringUtils.isNotEmpty(address.getCellphone()) ? address.getCellphone() : StringUtils.EMPTY);
        } else {
            valueList.add(StringUtils.EMPTY);
            valueList.add(StringUtils.EMPTY);
            valueList.add(StringUtils.EMPTY);
        }
        valueList.add(StringUtils.isNotEmpty(customer.getDocumentNumber()) ? customer.getDocumentNumber() : StringUtils.EMPTY);
        valueList.add(StringUtils.isNotEmpty(customer.getTrnNumber()) ? customer.getTrnNumber() : StringUtils.EMPTY);
        if (null != cart.getInstallationAddress()) {
            valueList.add(StringUtils.isNotEmpty(cart.getInstallationAddress().getPhone1()) ? cart.getInstallationAddress().getPhone1() : StringUtils.EMPTY);
        } else {
            valueList.add(StringUtils.EMPTY);
        }
        return valueList;
    }
}