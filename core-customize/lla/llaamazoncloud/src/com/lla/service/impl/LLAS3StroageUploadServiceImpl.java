/*
 *   Copyright (c) 2019 SAP SE or an SAP affiliate company.  All rights reserved.
 */
package com.lla.service.impl;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.lla.service.LLAS3StroageUploadService;
import de.hybris.platform.util.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Files;
import java.util.Objects;

/**
 * The type Lla s3 stroage upload service.
 */
public class LLAS3StroageUploadServiceImpl implements LLAS3StroageUploadService {

    private static final Logger LOG = LoggerFactory.getLogger(LLAS3StroageUploadServiceImpl.class);
    private static final String ACCESS_KEY = Config.getParameter("lla.s3.storage.accessKeyId");
    private static final String END_POINT = Config.getParameter("lla.s3.storage.endpoint");
    private static final String SECRET_KEY = Config.getParameter("lla.s3.storage.secretAccessKey");
    private static final String BUCKET_NAME = Config.getParameter("lla.s3.storage.bucketName");
    private static final String BUCKET_FOLDER = Config.getParameter("lla.s3.storage.bucket.folder");
    private static final String UPLOAD_DIR = Config.getParameter("lla.s3.csv.file.directory");
    private static final String SUFFIX="/";

    /**
     * Method to upload csv file to aws s3 location.
     */
    @Override
    public void uploadFile() {

        LOG.info("Inside Upload CSV File To S3 Location:-");
        Objects.requireNonNull(UPLOAD_DIR, "Upload dir path cannot be null");
        Objects.requireNonNull(BUCKET_NAME, "Upload dir path cannot be null");
        Objects.requireNonNull(BUCKET_FOLDER, "Upload S3 dir path cannot be null");
        final StringBuffer bucketName=new StringBuffer(BUCKET_NAME);
        AmazonS3 connect = setupConnection(ACCESS_KEY, SECRET_KEY, END_POINT);

        if (null != connect) {
            final File folder = new File(UPLOAD_DIR);
            for (final File file : folder.listFiles()) {
                try {
                    connect.putObject(new PutObjectRequest(String.valueOf(bucketName.append(SUFFIX).append(BUCKET_FOLDER)), file.getName(), file));
                    Files.delete(file.toPath());
                } catch (final Exception error) {
                    LOG.error("Error occured while uploading file with message [" + error.getMessage() + "]");
                }
            }
        }
    }

    private AmazonS3 setupConnection(String accessKey, String secretKey, String endPoint) {

        LOG.info("Inside Amazon S3 Setup Connection:accessKey:["+accessKey+"] secretKey:["+secretKey+"] endPoint:["+endPoint+"]");
        Objects.requireNonNull(accessKey, "Access key cannot be null");
        Objects.requireNonNull(secretKey, "Secret key cannot be null");
        Objects.requireNonNull(endPoint, "End point cannot be null");

        final AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        final ClientConfiguration clientConfig = new ClientConfiguration();
        clientConfig.setProtocol(Protocol.HTTPS);
        final AmazonS3 connect = new AmazonS3Client(credentials, clientConfig);
        connect.setEndpoint(endPoint);
        connect.setRegion(Region.getRegion(Regions.US_EAST_1));
        return connect;
    }
}
