package com.lla.service;

import de.hybris.platform.core.model.order.CartModel;

import java.io.IOException;
import java.util.List;

public interface LLAGenerateDocumentService {

    void generateDocument(List<CartModel> abandonedCart,String fileName) throws IOException;
}
