/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.service;

public interface LlaamazoncloudService
{
	String getHybrisLogoUrl(String logoCode);

	void createLogo(String logoCode);
}
