/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.controller;

import static com.lla.constants.LlaamazoncloudConstants.PLATFORM_LOGO_CODE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.lla.service.LlaamazoncloudService;


@Controller
public class LlaamazoncloudHelloController
{
	@Autowired
	private LlaamazoncloudService llaamazoncloudService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String printWelcome(final ModelMap model)
	{
		model.addAttribute("logoUrl", llaamazoncloudService.getHybrisLogoUrl(PLATFORM_LOGO_CODE));
		return "welcome";
	}
}
