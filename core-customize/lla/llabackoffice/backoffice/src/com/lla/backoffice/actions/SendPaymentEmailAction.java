/**
 *
 */
package com.lla.backoffice.actions;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import javax.annotation.Resource;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import com.hybris.backoffice.widgets.notificationarea.NotificationService;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author LU836YG
 *
 */
public class SendPaymentEmailAction implements CockpitAction<AbstractOrderModel, Object>
{
	private static final String PAYMENT_MESSAGE = "hmc.action.send.payment.email.message";
	private static final String PAYMENT_NOTIFICATION_MESSAGE = "llabackoffice.lla.order.payment.notification.event";
	@Autowired
	private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

	@Resource(name = "llaMulesoftNotificationService")
	private LLAMulesoftNotificationService llaMulesoftNotificationService;

	@Resource(name = "notificationService")
	private NotificationService notificationService;

	@Override
	public boolean canPerform(final ActionContext<AbstractOrderModel> ctx)
	{
		if (llaMulesoftIntegrationUtil.isVtrOrder(ctx.getData().getSite()))
		{
		   return ctx != null && ctx.getData() instanceof AbstractOrderModel;
		}
		return ctx.getIconDisabledUri().isEmpty();
	}
	@Override
	public String getConfirmationMessage(final ActionContext<AbstractOrderModel> ctx)
	{
		return ctx.getLabel(PAYMENT_MESSAGE);
	}

	@Override
	public boolean needsConfirmation(final ActionContext<AbstractOrderModel> arg0)
	{
		return true;
	}


	@Override
	public ActionResult<Object> perform(final ActionContext<AbstractOrderModel> ctx)
	{
		final Object data = ctx.getData();

		if (data != null && data instanceof AbstractOrderModel)
		{
			try
			{
			final AbstractOrderModel order = (AbstractOrderModel) data;
			llaMulesoftNotificationService.generatePaymentNotification(order);
			notificationService.notifyUser(notificationService.getWidgetNotificationSource(ctx), PAYMENT_NOTIFICATION_MESSAGE,
					NotificationEvent.Level.SUCCESS);
			return new ActionResult<Object>(ActionResult.SUCCESS, order);
			}
			catch (final LLAApiException llaApiException)
			{
				notificationService.notifyUser(notificationService.getWidgetNotificationSource(ctx), PAYMENT_NOTIFICATION_MESSAGE,
						NotificationEvent.Level.FAILURE);
				return new ActionResult(ActionResult.ERROR);

			}
		}
		else
		{
			return new ActionResult(ActionResult.ERROR);
		}
	}
}

