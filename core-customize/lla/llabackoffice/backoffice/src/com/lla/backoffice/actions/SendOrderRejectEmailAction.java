/**
 *
 */
package com.lla.backoffice.actions;

import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;

import javax.annotation.Resource;

import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.log4j.Logger;
import com.hybris.backoffice.widgets.notificationarea.NotificationService;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.service.LLAMulesoftNotificationService;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author HP737SW
 *
 */
public class SendOrderRejectEmailAction implements CockpitAction<AbstractOrderModel, Object>
{
	public static final Logger LOG = Logger.getLogger(SendOrderRejectEmailAction.class);
	private static final String REJECT_MESSAGE = "hmc.action.send.order.reject.message";
	private static final String ORDER_REJECT_NOTIFICATION_MESSAGE = "llabackoffice.lla.order.reject.notification.event";

	@Resource(name = "llaMulesoftNotificationService")
	private LLAMulesoftNotificationService llaMulesoftNotificationService;

	@Autowired
	private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

	@Autowired
	private ModelService modelService;

	@Resource(name = "notificationService")
	private NotificationService notificationService;

	@Override
	public boolean canPerform(final ActionContext<AbstractOrderModel> ctx)
	{
		return ctx != null && ctx.getData() instanceof AbstractOrderModel;
	}

	@Override
	public String getConfirmationMessage(final ActionContext<AbstractOrderModel> ctx)
	{
		return ctx.getLabel(REJECT_MESSAGE);
	}

	@Override
	public boolean needsConfirmation(final ActionContext<AbstractOrderModel> arg0)
	{
		return true;
	}

	@Override
	public ActionResult<Object> perform(final ActionContext<AbstractOrderModel> ctx)
	{
		final Object data = ctx.getData();

		if (data != null && data instanceof AbstractOrderModel)
		{
			try
			{
				final OrderModel order = (OrderModel) data;
				llaMulesoftNotificationService.generateOrderCancelNotification(order);
				notificationService.notifyUser(notificationService.getWidgetNotificationSource(ctx), ORDER_REJECT_NOTIFICATION_MESSAGE,
						NotificationEvent.Level.SUCCESS);
				if(llaMulesoftIntegrationUtil.isCableticaOrder(order.getSite())){
					order.setStatus(OrderStatus.CSA_REJECTED);
					modelService.save(order);

				}
				return new ActionResult<Object>(ActionResult.SUCCESS, order);
			}
			catch (final LLAApiException llaApiException)
			{
				notificationService.notifyUser(notificationService.getWidgetNotificationSource(ctx),
						ORDER_REJECT_NOTIFICATION_MESSAGE, NotificationEvent.Level.FAILURE);
				return new ActionResult(ActionResult.ERROR);

			}
		}
		else
		{
			return new ActionResult(ActionResult.ERROR);
		}
	}
}
