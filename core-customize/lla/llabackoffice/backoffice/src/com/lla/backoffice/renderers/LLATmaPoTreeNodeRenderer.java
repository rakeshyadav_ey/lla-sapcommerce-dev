//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.lla.backoffice.renderers;

import com.hybris.backoffice.widgets.notificationarea.NotificationService;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent.Level;
import com.hybris.cockpitng.components.Action;
import com.hybris.cockpitng.dataaccess.facades.permissions.PermissionFacade;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.engine.impl.DefaultWidgetInstanceManager;
import com.hybris.cockpitng.labels.LabelService;
import de.hybris.platform.b2ctelcobackoffice.tree.TmaBpoTreeModel;
import de.hybris.platform.b2ctelcobackoffice.tree.TmaPoTreeNode;
import de.hybris.platform.b2ctelcoservices.model.TmaBundledProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.services.TmaPoService;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Div;
import org.zkoss.zul.Html;
import org.zkoss.zul.Label;
import org.zkoss.zul.Toolbarbutton;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.TreeitemRenderer;
import org.zkoss.zul.Treerow;

public class LLATmaPoTreeNodeRenderer implements TreeitemRenderer<TmaPoTreeNode> {
    private static final Logger LOG = Logger.getLogger(LLATmaPoTreeNodeRenderer.class.getName());
    private static final String REMOVE_CHILD_PRODUCT_OFFERING_NODE_TOOLTIP = "backoffice.productOfferingTreeNode.action.removeChildNode.tooltip";
    private static final String ADD_CHILD_PRODUCT_OFFERING_ACTION_ID = "de.hybris.platform.b2ctelcobackoffice.action.addchildproductoffering";
    private static final String REMOVE_CHILD_PRODUCT_OFFERING_BTN_IMAGE = "cng/images/icon_remove_treeitem.png";
    private static final String NOTIFICATION_AREA = "b2ctelcobackoffice-bpoStructure";
    private TmaPoService productOfferingService;
    private PermissionFacade permissionFacade;
    private WidgetInstanceManager widgetInstanceManager;
    private LabelService labelService;
    private NotificationService notificationService;

    public LLATmaPoTreeNodeRenderer() {
    }

    public void render(Treeitem treeitem, TmaPoTreeNode treeNode, int index) throws Exception {
        TmaProductOfferingModel productOfferingModel = treeNode.getData();
        TmaProductOfferingModel parentProductOffering = null;
        if (treeNode.getParent() != null) {
            parentProductOffering = (TmaProductOfferingModel)treeNode.getParent().getData();
        }

        treeitem.setValue(treeNode);
        treeitem.setDraggable("false");
        treeitem.setDroppable("false");
        this.selectRootNode(treeitem);
        Treerow row = treeitem.getTreerow();
        if (row == null) {
            row = new Treerow();
            row.setParent(treeitem);
        }

        Treecell firstCell = new Treecell();
        firstCell.setParent(row);
        firstCell.setSclass("bpo-tree-first-column");
        firstCell.appendChild(new Label(this.getLabelService().getObjectLabel(productOfferingModel)));
        this.addTreeItemActions(treeitem, row, productOfferingModel, parentProductOffering);
        /*if (parentProductOffering != null) {
            this.addExistingParentsDetails(row, productOfferingModel, parentProductOffering);
        }*/

    }

    private void selectRootNode(Treeitem treeitem) {
        if (treeitem.getParentItem() == null) {
            treeitem.setSelected(true);
        }

    }

    private void addTreeItemActions(Treeitem treeItem, Treerow treerow, TmaProductOfferingModel productOfferingModel, TmaProductOfferingModel parentProductOfferingModel) {
        if (this.getPermissionFacade().canChangePropertyPermission("TmaBundledProductOffering", "children") && this.getPermissionFacade().canChangePropertyPermission("TmaProductOffering", "parents")) {
            Treecell actionsCell = new Treecell();
            actionsCell.setParent(treerow);
            actionsCell.setSclass("bpo-tree-second-column");
            actionsCell.appendChild(this.addEligibleActions(treeItem, productOfferingModel, parentProductOfferingModel));
        } else {
            LOG.debug("User is not allowed to modify TmaProductOffering hierarchy.");
        }
    }

    private void addExistingParentsDetails(Treerow row, TmaProductOfferingModel productOfferingModel, TmaProductOfferingModel parentProductOffering) {
        if (productOfferingModel instanceof TmaBundledProductOfferingModel) {
            if (parentProductOffering.getParents().size() != 1) {
                List<String> parentNamesList = new ArrayList();
                Iterator var6 = productOfferingModel.getParents().iterator();

                while(var6.hasNext()) {
                    TmaProductOfferingModel parent = (TmaProductOfferingModel)var6.next();
                    if (!StringUtils.equals(parent.getCode(), parentProductOffering.getCode())) {
                        parentNamesList.add(this.getLabelService().getObjectLabel(parent));
                    }
                }

                if (!CollectionUtils.isEmpty(parentNamesList)) {
                    Treecell actionsCell = new Treecell();
                    actionsCell.appendChild(new Label("Also contained in " + parentNamesList));
                    actionsCell.setParent(row);
                    actionsCell.setStyle("text-align:left;");
                }
            }
        }
    }

    private Component addEligibleActions(Treeitem treeitem, TmaProductOfferingModel productOfferingModel, TmaProductOfferingModel parentProductOfferingModel) {
        Div actionsComponent = new Div();
        boolean isAddButtonPresent = false;
        if (productOfferingModel instanceof TmaBundledProductOfferingModel) {
            actionsComponent.appendChild(this.createAddChildProductOfferingAction(treeitem));
            isAddButtonPresent = true;
        }

        if (treeitem.getParentItem() == null) {
            return actionsComponent;
        } else {
            Toolbarbutton removeButton = this.createRemoveChildProductOfferingAction(productOfferingModel, parentProductOfferingModel);
            removeButton.setStyle("clear:both;");
            if (!isAddButtonPresent) {
                Div emptyDiv = new Div();
                emptyDiv.setStyle("margin-left:34px;float:left;");
                emptyDiv.appendChild(new Html("&nbsp;"));
                actionsComponent.appendChild(emptyDiv);
            }

            actionsComponent.appendChild(removeButton);
            return actionsComponent;
        }
    }

    private Component createAddChildProductOfferingAction(Treeitem treeItem) {
        Action action = new Action();
        action.setActionId("de.hybris.platform.b2ctelcobackoffice.action.addchildproductoffering");
        action.setWidgetInstanceManager(this.getWidgetInstanceManager());
        action.setComponentDefinitionService(((DefaultWidgetInstanceManager)this.getWidgetInstanceManager()).getComponentDefinitionService());
        action.afterCompose();
        action.getActionContext().setData(treeItem);
        action.setStyle("text-align:center;width:38px;float:left;");
        return action;
    }

    private Toolbarbutton createRemoveChildProductOfferingAction(TmaProductOfferingModel offeringModel, TmaProductOfferingModel parentOfferingModel) {
        Toolbarbutton toolbarbutton = new Toolbarbutton("", "cng/images/icon_remove_treeitem.png");
        toolbarbutton.setTooltiptext(Labels.getLabel("backoffice.productOfferingTreeNode.action.removeChildNode.tooltip"));
        toolbarbutton.addEventListener("onClick", this.getRemoveChildProductOfferingEventListener(offeringModel, parentOfferingModel));
        toolbarbutton.setStyle("vertical-align:inherit;");
        return toolbarbutton;
    }

    private EventListener getRemoveChildProductOfferingEventListener(TmaProductOfferingModel childOfferingModel, TmaProductOfferingModel parentOfferingModel) {
        return (event) -> {
            if (parentOfferingModel != null && childOfferingModel != null) {
                this.getProductOfferingService().removePoChildFromParent((TmaBundledProductOfferingModel)parentOfferingModel, childOfferingModel);
                Treecell actionsCell = (Treecell)event.getTarget().getParent().getParent();
                Treerow treerow = (Treerow)actionsCell.getParent();
                Treeitem treeItem = (Treeitem)treerow.getParent();
                TmaPoTreeNode selectedTreeNode = (TmaPoTreeNode)treeItem.getValue();
                TmaBpoTreeModel tree = (TmaBpoTreeModel)treeItem.getTree().getModel();
                tree.remove(selectedTreeNode);
                this.getNotificationService().notifyUser("b2ctelcobackoffice-bpoStructure", "Remove", Level.SUCCESS, new Object[]{this.getLabelService().getObjectLabel(childOfferingModel), this.getLabelService().getObjectLabel(parentOfferingModel)});
            } else {
                this.getNotificationService().notifyUser("b2ctelcobackoffice-bpoStructure", "Remove", Level.FAILURE, new Object[]{childOfferingModel, parentOfferingModel});
            }
        };
    }

    private TmaPoService getProductOfferingService() {
        return this.productOfferingService;
    }

    public void setProductOfferingService(TmaPoService productOfferingService) {
        this.productOfferingService = productOfferingService;
    }

    private PermissionFacade getPermissionFacade() {
        return this.permissionFacade;
    }

    public void setPermissionFacade(PermissionFacade permissionFacade) {
        this.permissionFacade = permissionFacade;
    }

    private WidgetInstanceManager getWidgetInstanceManager() {
        return this.widgetInstanceManager;
    }

    public void setWidgetInstanceManager(WidgetInstanceManager widgetInstanceManager) {
        this.widgetInstanceManager = widgetInstanceManager;
    }

    private LabelService getLabelService() {
        return this.labelService;
    }

    public void setLabelService(LabelService labelService) {
        this.labelService = labelService;
    }

    protected NotificationService getNotificationService() {
        return this.notificationService;
    }

    public void setNotificationService(NotificationService notificationService) {
        this.notificationService = notificationService;
    }
}
