package com.lla.backoffice.renderers;


import com.hybris.cockpitng.core.config.impl.jaxb.editorarea.AbstractSection;
import com.hybris.cockpitng.dataaccess.facades.type.DataType;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.widgets.editorarea.renderer.EditorAreaRendererUtils;
import com.hybris.cockpitng.widgets.editorarea.renderer.impl.DefaultEditorAreaSectionRenderer;
import de.hybris.platform.b2ctelcobackoffice.tree.TmaBpoTreeModel;
import de.hybris.platform.b2ctelcobackoffice.tree.TmaPoTreeNode;
import de.hybris.platform.b2ctelcoservices.model.TmaBundledProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import java.text.MessageFormat;
import java.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Vlayout;

public class LLATmaBpoStructureEditorAreaRenderer extends DefaultEditorAreaSectionRenderer {
    private static final Logger LOG = LoggerFactory.getLogger(de.hybris.platform.b2ctelcobackoffice.renderers.TmaBpoStructureEditorAreaRenderer.class);
    private static final String BACKOFFICE_SECTION_PRODUCT_OFFERING_STRUCTURE = "backoffice.section.productOffering.structure";
    private static final String REFRESH_BUTTON_LISTENER = "REFRESH_BUTTON_LISTENER_ID";
    private static final String SOCKET_OUTPUT_CURRENT_OBJECT = "currentObject";
    private LLATmaPoTreeNodeRenderer nodeRenderer;

    public LLATmaBpoStructureEditorAreaRenderer() {
    }

    public void render(Component component, AbstractSection abstractSection, Object object, DataType dataType, WidgetInstanceManager widgetInstanceManager) {
        Vlayout vlayout = new Vlayout();
        vlayout.appendChild(new Label(Labels.getLabel("backoffice.section.productOffering.structure")));
        component.appendChild(vlayout);
        EditorAreaRendererUtils.setAfterCancelListener(widgetInstanceManager.getModel(), "REFRESH_BUTTON_LISTENER_ID", (event) -> {
            widgetInstanceManager.getWidgetslot().updateView();
        }, false);
        TmaProductOfferingModel productOffering = (TmaProductOfferingModel)widgetInstanceManager.getModel().getValue("currentObject", TmaProductOfferingModel.class);
        if (!(productOffering instanceof TmaBundledProductOfferingModel)) {
            LOG.debug(MessageFormat.format("The section cannot be rendered due to the wrong type for {0}.", "currentObject"));
        }

        TmaBpoTreeModel tmaBundledProductOfferingTreeModel = new TmaBpoTreeModel(new TmaPoTreeNode(productOffering, Collections.singletonList(new TmaPoTreeNode(productOffering))));
        Tree treeStructure = new Tree();
        treeStructure.setModel(tmaBundledProductOfferingTreeModel);
        treeStructure.setItemRenderer(this.getTreeNodeRenderer(widgetInstanceManager));
        component.appendChild(treeStructure);
    }

    private LLATmaPoTreeNodeRenderer getTreeNodeRenderer(WidgetInstanceManager wim) {
        this.nodeRenderer.setWidgetInstanceManager(wim);
        return this.nodeRenderer;
    }

    public void setNodeRenderer(LLATmaPoTreeNodeRenderer nodeRenderer) {
        this.nodeRenderer = nodeRenderer;
    }
}

