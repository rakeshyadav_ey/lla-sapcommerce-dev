package com.lla.backoffice.actions;

import com.hybris.backoffice.widgets.notificationarea.NotificationService;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.lla.mulesoft.integration.exception.LLAApiException;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.core.model.order.OrderModel;

import de.hybris.platform.payment.enums.PaymentTransactionType;
import org.apache.log4j.Logger;
import org.llapayment.service.LLAPaymentCaptureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.concurrent.atomic.AtomicBoolean;

public class CapturePaymentAction implements CockpitAction<OrderModel, Object> {
    private static final String CONFIRMATION_MESSAGE = "hmc.action.capturePayment.confirmation.message";
    private static final String ERROR_MESSAGE = "llabackoffice.capturePayment.error.event";
    private static final String SUCCESS_MESSAGE = "llabackoffice.capturePayment.success.event";
    private static final Logger LOG = Logger.getLogger(CapturePaymentAction.class);
    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

    @Autowired
    private LLAPaymentCaptureService llaPaymentCaptureService;

    @Resource(name = "notificationService")
    private NotificationService notificationService;

    @Override
    public ActionResult<Object> perform(ActionContext<OrderModel> actionContext) {
        final Object data = actionContext.getData();

        try {
            if ((data != null) && (data instanceof OrderModel) && llaPaymentCaptureService.capturePayment((OrderModel) data)) {
                notificationService.notifyUser(notificationService.getWidgetNotificationSource(actionContext), SUCCESS_MESSAGE,
                        NotificationEvent.Level.SUCCESS);
                   return new ActionResult<>(ActionResult.SUCCESS, data);
            }else{
                return getObjectActionResult(actionContext);
            }
        } catch (LLAApiException e) {
            LOG.error("Error in Capturing Payment for Order ::: ", e);
            return new ActionResult(ActionResult.ERROR);
        }
    }

    private ActionResult<Object> getObjectActionResult(ActionContext<OrderModel> actionContext) {
        notificationService.notifyUser(notificationService.getWidgetNotificationSource(actionContext), ERROR_MESSAGE,
                NotificationEvent.Level.FAILURE);
        return new ActionResult(ActionResult.ERROR);
    }

    @Override
    public boolean canPerform(ActionContext<OrderModel> ctx) {
        return ctx != null && ctx.getData() instanceof OrderModel && llaMulesoftIntegrationUtil.isCableticaOrder(ctx.getData().getSite())
                && !CollectionUtils.isEmpty(ctx.getData().getPaymentTransactions()) && !isPaymentCaptured(ctx.getData());
    }

    private boolean isPaymentCaptured(OrderModel data) {
        AtomicBoolean flag = new AtomicBoolean(false);
         data.getPaymentTransactions().stream().forEach(paymentTransactionModel -> {
            paymentTransactionModel.getEntries().stream().forEach(paymentTransactionEntryModel -> {
                if (paymentTransactionEntryModel.getType().equals(PaymentTransactionType.CAPTURE) && paymentTransactionEntryModel.getTransactionStatus().equalsIgnoreCase("ACCEPTED"))
                    flag.set(true);
            });
            });
         return flag.get();
    }

    @Override
    public boolean needsConfirmation(ActionContext<OrderModel> ctx) {
        return true;
    }

    @Override
    public String getConfirmationMessage(ActionContext<OrderModel> ctx) {
        return ctx.getLabel(CONFIRMATION_MESSAGE);
    }
}
