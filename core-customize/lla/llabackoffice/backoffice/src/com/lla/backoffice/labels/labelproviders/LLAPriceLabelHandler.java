package com.lla.backoffice.labels.labelproviders;

import com.hybris.backoffice.labels.LabelHandler;
import com.hybris.backoffice.labels.impl.PriceLabelHandler;
import com.hybris.cockpitng.core.util.Validate;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Currency;

public class LLAPriceLabelHandler extends PriceLabelHandler implements LabelHandler<Double, CurrencyModel>
{
    private static final Logger LOG = LoggerFactory.getLogger(LLAPriceLabelHandler.class);

  @Override
    protected NumberFormat getNumberFormatter(Currency javaCurrency, CurrencyModel currencyModel)
    {
        NumberFormat nf = super.getNumberFormatter(javaCurrency, currencyModel);
         nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(2);
        return nf;
    }

    @Override
    public String getLabel(Double price, CurrencyModel currencyModel)
    {
        if("CLP".equalsIgnoreCase(currencyModel.getIsocode()))
        {
            Validate.notNull("Price may not be null", new Object[]{price});
            Validate.notNull("Currency may not be null", new Object[]{currencyModel});
            String currencyIsoCode = currencyModel.getIsocode();
            Currency javaCurrency = null;

            try {
                javaCurrency = this.getI18NService().getBestMatchingJavaCurrency(currencyIsoCode);
            } catch (IllegalArgumentException var7) {
                if (LOG.isDebugEnabled()) {
                    LOG.warn(String.format("Could not find Java currency for iso code: '%s'.", currencyIsoCode), var7);
                } else {
                    LOG.warn("Could not find Java currency for iso code: '{}'.", currencyIsoCode);
                }
            }

            try {

                String formattedPrice = javaCurrency == null ? this.getNumberFormatForNonJavaCurrency(currencyModel).format(price) : this.getNumberFormatter(javaCurrency, currencyModel).format(price);
                return formattedPrice.split("[.]")[0].replace(",",".");

            } catch (RuntimeException var6) {
                LOG.warn(String.format("Could not format the given price '%d' in the given currency '%s'", price, currencyModel.getSymbol()), var6);
                return "";
            }
        }
        else
             return super.getLabel(price, currencyModel);
    }
}
