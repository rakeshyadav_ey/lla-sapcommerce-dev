package com.lla.puertoricostore.controllers.misc;

import com.lla.core.enums.ClickToCallReasonEnum;
import com.lla.core.enums.FalloutReasonEnum;
import com.lla.core.enums.OriginSource;
import com.lla.core.util.LLACommonUtil;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Objects;

/**
 * The type Click to call controller.
 */
@Controller
@RequestMapping(value = "/click")
public class ClickToCallController extends AbstractController {
    private static final Logger LOG = Logger.getLogger(ClickToCallController.class);
    private static final String CALLBACK_URL="/callBack";
    private static final String FUZZY_SEARCH="FuzzySearch";
    private static final String SERVICEABLITY="Serviceability";
    private static final String CREDIT_CHECK="CreditCheck";

    @Autowired
    private CartService cartService;
    @Autowired
    private LLACommonUtil llaCommonUtil;
    @Autowired
    private ModelService modelService;

    /**
     * Gets call back.
     *
     * @param callBackType  the call back type
     * @param houseNo       the house no
     * @param model         the model
     * @param redirectModel the redirect model
     * @return the call back
     */
    @RequestMapping(value = "/callBackRequest", method = RequestMethod.POST)
    @ResponseBody
    public String getCallBack(@RequestParam(value="callBackType",required = true) final String callBackType,@RequestParam(value="houseNo",required = false) final String houseNo,final Model model,final RedirectAttributes redirectModel) {
         LOG.info(String.format("Inside get call back method for %s call back type ",callBackType));

         CartModel cart = cartService.getSessionCart();
        if (Objects.nonNull(cart)&& StringUtils.isNotEmpty(callBackType))
        {
            CustomerModel customer=(CustomerModel) cart.getUser();
            cart.setFalloutReasonValue(FalloutReasonEnum.CONNECTION_TIMEOUT);
            modelService.save(cart);
            modelService.refresh(cart);

            switch (callBackType) {

                case FUZZY_SEARCH:
                    LOG.info(String.format(
                            "Launching fallout process for cart %s and redirecting to callBack Page as %s at %s as its connection timeout for %s api.",
                            cartService.getSessionCart().getCode(), ClickToCallReasonEnum.UNAVAILABLE_NORMALIZED_ADDRESS, OriginSource.CHECKOUT,
                            callBackType));
                    llaCommonUtil.launchFalloutProcess(cart, ClickToCallReasonEnum.UNAVAILABLE_NORMALIZED_ADDRESS, OriginSource.CHECKOUT, null,
                                    Strings.EMPTY, StringUtils.EMPTY);
                    return CALLBACK_URL;

                case SERVICEABLITY:
                    LOG.info(String.format(
                            "Launching fallout process for cart %s having house No %s and redirecting to callBack Page as %s at %s as its connection timeout for %s api.",
                            cartService.getSessionCart().getCode(), houseNo,ClickToCallReasonEnum.NOT_SERVICEABLE, OriginSource.CHECKOUT,
                            callBackType));
                    llaCommonUtil.launchFalloutProcess(cart, ClickToCallReasonEnum.NOT_SERVICEABLE, OriginSource.CHECKOUT, null, houseNo,
                            StringUtils.EMPTY);
                    return CALLBACK_URL;

                case CREDIT_CHECK:

                    LOG.info(String.format(
                            "Launching fallout process for cart %s and redirecting to callBack Page as %s at %s as its connection timeout for %s api.",
                            cartService.getSessionCart().getCode(), ClickToCallReasonEnum.CREDIT, OriginSource.CHECKOUT, callBackType));
                    llaCommonUtil.launchFalloutProcess(cart, ClickToCallReasonEnum.CREDIT, OriginSource.CHECKOUT, null, (Objects.nonNull(cart.getSelectedAddress()) && StringUtils.isNotEmpty(cart.getSelectedAddress().getHouseNo()))?cart.getSelectedAddress().getHouseNo():Strings.EMPTY ,
                            (Objects.nonNull(customer) && StringUtils.isNotEmpty(customer.getCustomerSSN()))? customer.getCustomerSSN():StringUtils.EMPTY);
                    return CALLBACK_URL;
            }
        }
        return CALLBACK_URL;
    }
}

