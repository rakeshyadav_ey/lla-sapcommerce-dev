/**
 *
 */
package com.lla.puertoricostore.forms;

/**
 * @author LU836YG
 *
 */
public class CreateCombinationForm
{
	private Boolean internet;
	private Boolean tv;
	private Boolean phone;

	/**
	 * @return the internet
	 */
	public Boolean getInternet()
	{
		return internet;
	}

	/**
	 * @param internet
	 *           the internet to set
	 */
	public void setInternet(final Boolean internet)
	{
		this.internet = internet;
	}

	/**
	 * @return the tv
	 */
	public Boolean getTv()
	{
		return tv;
	}

	/**
	 * @param tv
	 *           the tv to set
	 */
	public void setTv(final Boolean tv)
	{
		this.tv = tv;
	}

	/**
	 * @return the phone
	 */
	public Boolean getPhone()
	{
		return phone;
	}

	/**
	 * @param phone
	 *           the phone to set
	 */
	public void setPhone(final Boolean phone)
	{
		this.phone = phone;
	}



}
