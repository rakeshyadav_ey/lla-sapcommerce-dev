/**
 *
 */
package com.lla.puertoricostore.controllers.pages;

import com.lla.common.addon.idp.strategy.LLALoginStrategy;
import com.lla.core.model.LLABundleProductMappingModel;
import com.lla.facades.constants.LlaFacadesConstants;
import com.lla.facades.util.LLAConfiguratorUtil;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;

import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.subscriptionservices.model.RecurringChargeEntryModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionPricePlanModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.commercefacades.order.data.CartData;
import java.util.*;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
//import org.assertj.core.util.Lists;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.lla.facades.cart.LLACartFacade;
import com.lla.facades.product.LLATmaProductFacade;
import com.lla.facades.productconfigurator.data.LLAPuertoRicoProductConfigDTO;
import com.lla.puertoricostore.controllers.ControllerConstants;
import com.lla.puertoricostore.forms.CreateCombinationForm;

import org.springframework.web.bind.annotation.ModelAttribute;
import de.hybris.platform.commercefacades.order.CartFacade;

/**
 * @author GF986ZE
 *
 */
@Controller
@RequestMapping(value = "/configureProduct")
public class ProductConfiguratorController extends AbstractPageController
{
	private static final Logger LOG = Logger.getLogger(ProductConfiguratorController.class);

	private static final String INTERNETCMSPAGELABEL = "configureProduct/selectInternetPlan";
	private static final String TVCMSPAGELABEL = "configureProduct/selectTvPlan";
	private static final String PHONECMSPAGELABEL = "configureProduct/selectphonePlan";
	private static final String CREATECOMBOPAGELABEL = "configureProduct/createCombination";
	private static final String REDIRECT_CARTPAGE_URL = "/cart";
	private static final String INTERNETCATEGORY = "internet";
	private static final String TVCATEGORY = "tv";
	private static final String PHONECATEGORY = "phone";
	private static final String ADDONPAGELABEL = "configureProduct/selectAddons";
	private static final String SMARTPRODUCTCATEGORY = "smartproduct";
	private static final String REDIRECT_INTERNET_URL = REDIRECT_PREFIX + "/configureProduct/selectInternetPlan";
	private static final String REDIRECT_TV_URL = REDIRECT_PREFIX + "/configureProduct/selectTvPlan";
	private static final String REDIRECT_PHONE_URL = REDIRECT_PREFIX + "/configureProduct/selectPhone";
	private static final String REDIRECT_ADDON_URL = REDIRECT_PREFIX + "/configureProduct/selectAddons";
	private static final String REDIRECT_CREATE_COMBINATION_URL = REDIRECT_PREFIX + "/configureProduct";
	private static final String SUCCESS = "success";
	public static final String PRODUCT_LIST = "productList";
	public static final String CURRENT_STEP_ID = "currentStepID";
	public static final String BUNDLE_MESSAGE = "bundleMessage";
	public static final String IMAGE = "image";
	public static final String ONEP = "internet_500";
	public static final String TWOP = "tv_ultimate";
	public static final String PRODUCT_CODES = "product.code.add";
	public static final String ONEP_90 = "internet_090";
	public static final String INTERNET_TV_SELECTED = "internetTvSelected";
	public static final String INTERNET_PHONE_SELECTED = "internetPhoneSelected";
	public static final String TV_PHONE_SELECTED = "tvPhoneSelected";
	public static final String INTERNET_SELECTED = "internetSelected";
	public static final String TV_SELECTED = "tvSelected";
	public static final String PHONE_SELECTED = "phoneSelected";

	@Autowired
	private SessionService sessionService;

	@Autowired
	private ConfigurationService configurationService;

	@Resource(name = "llaTmaProductFacade")
	private LLATmaProductFacade llaTmaProductFacade;

	@Autowired
	private LLAConfiguratorUtil llaConfiguratorUtill;

	@Resource(name = "llaCartFacade")
	private LLACartFacade llaCartFacade;
	@Resource(name = "llaLoginStrategy")
	private LLALoginStrategy llaLoginStrategy;
	@Resource(name = "cartService")
	private CartService cartService;
	@Autowired
	ProductService productService;
	
	@Resource(name = "cartFacade")
        private CartFacade cartFacade;

	@ModelAttribute("authUrl")
	public String getAuthorizationUrl(final HttpServletRequest request)
	{
		request.getSession().setAttribute("llaRefererUrl",request.getRequestURL());
		return llaLoginStrategy.authUrlConstructor(request, "auth");
	}
	@ModelAttribute("cartData")
        public CartData getSessionCartData(final HttpServletRequest request)
        {
          return cartFacade.getSessionCart();
        }
	
	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;
	@RequestMapping(value = "/selectInternetPlan", method = RequestMethod.GET)
	public String getInternetPlanDetails(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		final LLAPuertoRicoProductConfigDTO productConfig = llaConfiguratorUtill.getLlaPuertoRicoProductConfigDTO();
		model.addAttribute(PRODUCT_LIST, llaConfiguratorUtill.sortproduct(llaTmaProductFacade.getProductForCategory(INTERNETCATEGORY)));
		model.addAttribute("tvaddonVisibility", getConfigurationService().getConfiguration().getString("addon.tv.visibility.flag", "false"));
		model.addAttribute("addonProduct", llaTmaProductFacade.getProductForCode("wifi_box"));
		model.addAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO, productConfig);
		model.addAttribute(CURRENT_STEP_ID, "1");
		model.addAttribute("pageType", PageType.SELECT_INTERNET_PLAN.name());
		model.addAttribute("planList", sessionService.getAttribute(ControllerConstants.SELECTED_PLAN_LIST));
		final ContentPageModel contentPage = getContentPageForLabelOrId(INTERNETCMSPAGELABEL);
		setCMSPageData(model, contentPage);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/saveInternetPlan", method = RequestMethod.POST)
	public String saveInternetPlan(@RequestParam("selectedInternet")
								   final String selectedInternet, final Model model, @RequestParam(value = "selectedAddon", required = false)
								   final String selectedAddon, final RedirectAttributes redirectModel) {
//		if(!llaConfiguratorUtill.isCartEmpty()){
//			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.error.multiple.bundle");
//			return REDIRECT_INTERNET_URL;
//		}
		final LLAPuertoRicoProductConfigDTO productConfig = llaConfiguratorUtill.updateProductConfigDTO(1);
		sessionService.setAttribute(ControllerConstants.SELECTED_PLAN_PRICE,
				fetchRecurringPriceForPlan(productService.getProductForCode(selectedInternet)));
		productConfig.setOneP(selectedInternet);
		if (StringUtils.isNotEmpty(selectedAddon))
		{
			productConfig.setOnePAddon(selectedAddon);
		}
		sessionService.setAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO, productConfig);
		try
		{
			if (INTERNET_SELECTED.equals(sessionService.getAttribute(ControllerConstants.PLAN_SELECTED)))
			{
				llaCartFacade.addConfiguratorProductsToCart(productConfig, 1, "ACQUISITION", null, -1, null, null, null);
				return REDIRECT_PREFIX + "/cart";
			}
			else if (INTERNET_PHONE_SELECTED.equals(sessionService.getAttribute(ControllerConstants.PLAN_SELECTED)))
			{
				return REDIRECT_PHONE_URL;
			}
		}
		catch (final CalculationException exception)
		{
			LOG.error("Error in Cart Calculation ", exception);
		}
		return REDIRECT_TV_URL;
	}

	@RequestMapping(value = "/selectTvPlan", method = RequestMethod.GET)
	public String getTvPlanDetails(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException {
		final LLAPuertoRicoProductConfigDTO productConfig = llaConfiguratorUtill.getLlaPuertoRicoProductConfigDTO();
		model.addAttribute(PRODUCT_LIST, llaConfiguratorUtill.sortproduct(llaTmaProductFacade.getProductForCategory(TVCATEGORY)));
		model.addAttribute("activeProductList", filterTvPlan(llaTmaProductFacade.getProductForCategory(TVCATEGORY), productConfig));
		model.addAttribute(BUNDLE_MESSAGE, llaTmaProductFacade.getBundleMessage(productConfig, 2));
		//model.addAttribute("addonProducts", llaConfiguratorUtill.getAddons(productConfig));
		model.addAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO, productConfig);
		model.addAttribute(CURRENT_STEP_ID, "2");
		final List<LLABundleProductMappingModel> productMapping = llaCartFacade.getBundleProductMappingForCart(productConfig);
//		if(!productMapping.isEmpty()) {
//			model.addAttribute("productMapping", productMapping.get(0));
//		}
		if(StringUtils.isNotEmpty(productConfig.getOneP()) && 
				MapUtils.isNotEmpty(llaCartFacade.getProductPriceMap(llaConfiguratorUtill.sortproduct(llaTmaProductFacade.getProductForCategory(TVCATEGORY)),productMapping))) {
			model.addAttribute("productPriceMap", llaCartFacade.getProductPriceMap(llaConfiguratorUtill.sortproduct(llaTmaProductFacade.getProductForCategory(TVCATEGORY)),productMapping));
		} else {
			model.addAttribute("productPriceMap", llaConfiguratorUtill.getProductPriceMap(llaTmaProductFacade.getProductForCategory(TVCATEGORY)));
		}
		model.addAttribute("pageType", PageType.SELECT_TV_PLAN.name());
		model.addAttribute(ControllerConstants.SELECTED_PLAN_PRICE,
				null != sessionService.getAttribute(ControllerConstants.SELECTED_PLAN_PRICE)
						? sessionService.getAttribute(ControllerConstants.SELECTED_PLAN_PRICE)
						: Long.valueOf((long) 0.0));
		model.addAttribute("planList", sessionService.getAttribute(ControllerConstants.SELECTED_PLAN_LIST));
		final ContentPageModel contentPage = getContentPageForLabelOrId(TVCMSPAGELABEL);
		setCMSPageData(model, contentPage);

		return getViewForPage(model);
	}

	@RequestMapping(value = "/saveTvPlan", method = RequestMethod.POST)
	public String saveTVPlan(@RequestParam("selectedTVProduct")
							 final String tvProduct, final Model model, final RedirectAttributes redirectModel) {
//		if(!llaConfiguratorUtill.isCartEmpty()){
//			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.error.multiple.bundle");
//			return REDIRECT_TV_URL;
//		}
		final LLAPuertoRicoProductConfigDTO productConfig = llaConfiguratorUtill.updateProductConfigDTO(2);
		productConfig.setTwoP(tvProduct);
		if (StringUtils.isNotEmpty(productConfig.getOneP()) && StringUtils.isNotEmpty(productConfig.getTwoP()))
		{
			productConfig.setThreeP(llaTmaProductFacade.getProductForCategory(PHONECATEGORY).get(0).getCode());
		}
		/*llaConfiguratorUtill.saveAddons(addonProducts, productConfig);*/
		sessionService.setAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO, productConfig);
		sessionService.setAttribute(ControllerConstants.SELECTED_PLAN_PRICE,
				fetchRecurringPriceForPlan(productService.getProductForCode(tvProduct)));
		//return REDIRECT_PHONE_URL;
		try
		{
			if (TV_PHONE_SELECTED.equals(sessionService.getAttribute(ControllerConstants.PLAN_SELECTED)))
			{
				return REDIRECT_PHONE_URL;
			}
			else if (INTERNET_TV_SELECTED.equals(sessionService.getAttribute(ControllerConstants.PLAN_SELECTED)))
			{
				llaCartFacade.addConfiguratorProductsToCart(productConfig, 1, "ACQUISITION", null, -1, null, null, null);
				return REDIRECT_PREFIX + "/cart";
			}
			else
			{
				llaCartFacade.addConfiguratorProductsToCart(productConfig, 1, "ACQUISITION", null, -1, null, null, null);
				return REDIRECT_PREFIX + "/cart";
			}
		}
		catch (final CalculationException exception)
		{
			LOG.error("Error in Cart Calculation ", exception);
		}
		return null;
	}

	@RequestMapping(value = "/selectPhone", method = RequestMethod.GET)
	public String getPhoneDetails(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException {
		final LLAPuertoRicoProductConfigDTO productConfig = llaConfiguratorUtill.getLlaPuertoRicoProductConfigDTO();
		model.addAttribute(PRODUCT_LIST, llaConfiguratorUtill.fileterPhone(llaTmaProductFacade.getProductForCategory(PHONECATEGORY), productConfig));
		model.addAttribute(BUNDLE_MESSAGE, llaTmaProductFacade.getBundleMessage(productConfig, 3));
		model.addAttribute(CURRENT_STEP_ID, "3");
		model.addAttribute("pageType", PageType.SELECT_PHONE.name());
		model.addAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO, productConfig);
		final List<LLABundleProductMappingModel> productMapping = llaCartFacade.getBundleProductMappingForCart(productConfig);
		if(!productMapping.isEmpty()) {
			model.addAttribute("productMapping", productMapping.get(0));
		}
		model.addAttribute(ControllerConstants.SELECTED_PLAN_PRICE,
				null != sessionService.getAttribute(ControllerConstants.SELECTED_PLAN_PRICE)
						? sessionService.getAttribute(ControllerConstants.SELECTED_PLAN_PRICE)
						: Long.valueOf((long) 0.0));
		model.addAttribute("planList", sessionService.getAttribute(ControllerConstants.SELECTED_PLAN_LIST));
		final ContentPageModel contentPage = getContentPageForLabelOrId(PHONECMSPAGELABEL);
		setCMSPageData(model, contentPage);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/savePhone", method = RequestMethod.POST)
	public String savePhonePlan(@RequestParam("selectedPhone") final String selectedPhone,
								final RedirectAttributes redirectModel) {
//		if(!llaConfiguratorUtill.isCartEmpty()){
//			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.error.multiple.bundle");
//			return REDIRECT_PHONE_URL;
//		}
		final LLAPuertoRicoProductConfigDTO productConfig = llaConfiguratorUtill.updateProductConfigDTO(3);
		sessionService.setAttribute(ControllerConstants.SELECTED_PLAN_PRICE,
				fetchRecurringPriceForPlan(productService.getProductForCode(selectedPhone)));
		productConfig.setThreeP(selectedPhone);
		sessionService.setAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO, productConfig);
		//return REDIRECT_ADDON_URL;
		try
		{
			llaCartFacade.addConfiguratorProductsToCart(productConfig, 1, "ACQUISITION", null, -1, null, null, null);
		}
		catch (final CalculationException exception)
		{
			LOG.error("Error in Cart Calculation ", exception);
		}
		return REDIRECT_PREFIX + "/cart";
	}

	@RequestMapping(value = "/selectAddons", method = RequestMethod.GET)
	public String getAddons(final Model model, final HttpServletRequest request,
							final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException {

		final LLAPuertoRicoProductConfigDTO productConfig = llaConfiguratorUtill.getLlaPuertoRicoProductConfigDTO();
		if(StringUtils.isNotEmpty(productConfig.getOneP()) || StringUtils.isNotEmpty(productConfig.getThreeP()))
		{
			model.addAttribute("llaTmaProductDatas", getProductDatas(productConfig));
		}
		if(StringUtils.isNotEmpty(productConfig.getTwoP()))
		{
			final Collection<TmaProductOfferingModel> channelProductList = llaTmaProductFacade.getProductForCode(productConfig.getTwoP()).getChannelProductList();
			final List<TmaProductOfferingModel> finalProducts = productConfig.getTwoP().contains(TWOP) ? Arrays.asList(StringUtils.split(getConfigurationService().getConfiguration().getString(PRODUCT_CODES), ",")).stream().map(llaTmaProductFacade::getProductForCode).collect(Collectors.toList()).stream().filter(fp->channelProductList.contains(fp)).collect(Collectors.toList()): Lists.newArrayList();
			model.addAttribute("channelList",StringUtils.isNotEmpty(productConfig.getOneP()) && productConfig.getOneP().contains(ONEP) && (!CollectionUtils.sizeIsEmpty(finalProducts)) ? finalProducts : channelProductList);
			model.addAttribute("llaTmaProductDatas", getProductDatas(productConfig));
		}
		if((!(ONEP_90.equalsIgnoreCase(productConfig.getOneP()))) && StringUtils.isNotEmpty(productConfig.getTwoP()))
		{

			model.addAttribute("addonProducts",llaConfiguratorUtill.getAddons(productConfig));
			model.addAttribute("addonProductList", getAllTVAddons(productConfig));
		}
		model.addAttribute("smartProducts", llaTmaProductFacade.getProductForCategory(SMARTPRODUCTCATEGORY));
		model.addAttribute(BUNDLE_MESSAGE, llaTmaProductFacade.getBundleMessage(productConfig, 4));
		model.addAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO, productConfig);
		model.addAttribute(IMAGE, llaConfiguratorUtill.getMediaForCode("prAddonPageBanner"));
		model.addAttribute(CURRENT_STEP_ID, "4");
		model.addAttribute("pageType", PageType.SELECT_ADDONS.name());
		final ContentPageModel contentPage = getContentPageForLabelOrId(ADDONPAGELABEL);
		setCMSPageData(model, contentPage);

		return getViewForPage(model);
	}

	/**
	 * @param productConfig
	 * @return
	 */
	private List<ProductData> getProductDatas(LLAPuertoRicoProductConfigDTO productConfig)
	{
		List<String> productCodes = new ArrayList<String>(Arrays.asList(productConfig.getOneP(),productConfig.getTwoP(),productConfig.getTwoPHUB(),productConfig.getTwoPAddonHD(),productConfig.getTwoPAddonDVR(),productConfig.getThreeP()));
		productCodes.addAll(null!=productConfig.getChannelList()?productConfig.getChannelList() : CollectionUtils.EMPTY_COLLECTION);
		List<TmaProductOfferingModel> productDatas = productCodes.stream().filter(StringUtils::isNotEmpty).map(llaTmaProductFacade::getProductForCode).collect(Collectors.toList());
		return llaTmaProductFacade.getProductData(productDatas);
	}

	@RequestMapping(value = "/saveTVChannel", method = RequestMethod.POST)
	@ResponseBody
	public String saveTVChannel(@RequestParam(value="selectedChannel", required = false)
								final String selectedChannel, final Model model){
		final LLAPuertoRicoProductConfigDTO productConfig = sessionService.getAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO);
		List<String> channelList= new ArrayList<>();
		if(null != productConfig && CollectionUtils.isNotEmpty(productConfig.getChannelList())) {
			channelList.addAll(productConfig.getChannelList());
		}
		channelList.add(selectedChannel);
		productConfig.setChannelList(channelList);
		sessionService.setAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO, productConfig);
		//return SUCCESS;
		return REDIRECT_PREFIX + "/cart";
	}

	@RequestMapping(value = "/removeTVChannel", method = RequestMethod.POST)
	@ResponseBody
	public String removeTVChannel(@RequestParam(value="selectedChannel", required = false)
								  final String selectedChannel, final Model model){
		final LLAPuertoRicoProductConfigDTO productConfig = sessionService.getAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO);
		final List<String> channelList = (null != productConfig) ? productConfig.getChannelList() : null;
		if (null != channelList)
		{
			channelList.remove(selectedChannel);
		}
		productConfig.setChannelList(channelList);
		sessionService.setAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO, productConfig);
		//return SUCCESS;
		return REDIRECT_PREFIX + "/cart";
	}

	@RequestMapping(value = "/saveTVHubBox", method = RequestMethod.POST)
	@ResponseBody
	public String saveTVHubBox(@RequestParam(value = "addonProducts", required = false)final List<String> addonProducts, final Model model)
	{
		final LLAPuertoRicoProductConfigDTO productConfig = sessionService.getAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO);
		llaConfiguratorUtill.saveAddons(addonProducts, (null != productConfig) ? productConfig : null);
		sessionService.setAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO, productConfig);
		//return SUCCESS;
		return REDIRECT_PREFIX + "/cart";
	}

	@RequestMapping(value = "/removeTVHubBox", method = RequestMethod.POST)
	@ResponseBody
	public String removeTVHubBox(@RequestParam(value = "addonProducts", required = false)final List<String> addonProducts, final Model model)
	{
		final LLAPuertoRicoProductConfigDTO productConfig = sessionService.getAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO);
		llaConfiguratorUtill.removeAddons(addonProducts, (null != productConfig) ? productConfig : null);
		sessionService.setAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO, productConfig);
		//return SUCCESS;
		return REDIRECT_PREFIX + "/cart";
	}

	@RequestMapping(value = "/saveAddons", method = RequestMethod.POST)
	public String saveTVPlan(@RequestParam(value="selectedChannels", required = false)
							 final List<String> selectedChannels, final Model model, @RequestParam(value = "selectedSmartProducts", required = false)
							 final List<String> selectedSmartProducts, @RequestParam(value = "processType", required = false)
							 final String processType, @RequestParam(value = "plan", required = false)
							 final String plan, @RequestParam(value = "quantity", required = false, defaultValue = "1")
							 final long quantity, @RequestParam(value = "rootBpoCode", required = false)
							 final String rootBpoCode, @RequestParam(value = "cartGroupNo", required = false, defaultValue = "-1")
							 final int cartGroupNo, @RequestParam(value = "subscriptionTermId", required = false)
							 final String subscriptionTermId, @RequestParam(value = "subscriberId", required = false)
							 final String subscriberId, @RequestParam(value = "subscriberBillingId", required = false)
							 final String subscriberBillingId, final RedirectAttributes redirectModel) throws CalculationException {

		final LLAPuertoRicoProductConfigDTO productConfig = sessionService.getAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO);
		if(null == productConfig){
			return REDIRECT_PREFIX + "/cart";
		}
		if(CollectionUtils.isNotEmpty(selectedChannels))
		{
			productConfig.setChannelList(selectedChannels);
		}
		if(CollectionUtils.isNotEmpty(selectedSmartProducts))
		{
			productConfig.setSmartProductList(selectedSmartProducts);
		}
		sessionService.setAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO, productConfig);
//		if(llaCartFacade.hasSessionCart() && llaCartFacade.getSessionCart().getEntries().size() > 0){
//			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.error.multiple.bundle");
//			return REDIRECT_ADDON_URL;
//		}

		llaCartFacade.addConfiguratorProductsToCart(productConfig, quantity, processType, rootBpoCode, cartGroupNo,
				subscriptionTermId, subscriberId, subscriberBillingId);
		//sessionService.removeAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO);
		return REDIRECT_PREFIX + "/cart";
	}

	@RequestMapping(value = "/removeProduct", method = RequestMethod.POST)
	public String removeProductBack(@RequestParam(value = "deleteProduct") final String deleteProduct,
									@RequestParam(value="stepId") final int stepId,final RedirectAttributes redirectModel) throws CalculationException
	{
		LLAPuertoRicoProductConfigDTO productConfig = llaConfiguratorUtill.getLlaPuertoRicoProductConfigDTO();
		if(stepId == 1){
			productConfig = productConfigDto();
			return REDIRECT_INTERNET_URL;
		}else if(stepId == 2){
			productConfig = productConfigDto();
			return REDIRECT_TV_URL;
		}
		sessionService.setAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO, productConfig);
		llaCartFacade.addConfiguratorProductsToCart(productConfig, 1, "ACQUISITION", null, -1, null, null, null);
		return REDIRECT_TV_URL;
	}

	/**
	 * @return productConfig
	 */
	private LLAPuertoRicoProductConfigDTO productConfigDto()
	{
		LLAPuertoRicoProductConfigDTO productConfig;
		productConfig = new LLAPuertoRicoProductConfigDTO();
		sessionService.setAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO, productConfig);
		return productConfig;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String getCreateCombinationForm(@RequestParam(value = "categoryCode", required = false) final String categoryCode, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		if (null != sessionService.getAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO))
		{
			sessionService.removeAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO);
		}
		model.addAttribute("categoryCode", categoryCode);
		model.addAttribute("pageType", PageType.CREATE_COMBINATION.name());
		model.addAttribute(ControllerConstants.CREATE_COMBINATION_FORM, new CreateCombinationForm());
		final ContentPageModel contentPage = getContentPageForLabelOrId(CREATECOMBOPAGELABEL);
		setCMSPageData(model, contentPage);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/saveCombination", method = RequestMethod.POST)
	public String createCombination(@Valid
									final CreateCombinationForm form, final Model model, final BindingResult bindingResult, final HttpServletRequest request,
									final RedirectAttributes redirectModel)
	{
		final List<String> planList = new ArrayList<String>();
		if (form.getInternet() && form.getTv())
		{
			planList.add(ControllerConstants.INTERNET_SELECTED);
			planList.add(ControllerConstants.TV_SELECTED);
			sessionService.setAttribute(ControllerConstants.SELECTED_PLAN_LIST, planList);
			sessionService.setAttribute(ControllerConstants.PLAN_SELECTED, INTERNET_TV_SELECTED);
			return REDIRECT_INTERNET_URL;
		}
		else if (form.getInternet() && form.getPhone())
		{
			planList.add(ControllerConstants.INTERNET_SELECTED);
			planList.add(ControllerConstants.PHONE_SELECTED);
			sessionService.setAttribute(ControllerConstants.SELECTED_PLAN_LIST, planList);
			sessionService.setAttribute(ControllerConstants.PLAN_SELECTED, INTERNET_PHONE_SELECTED);
			return REDIRECT_INTERNET_URL;
		}
		else if (form.getTv() && form.getPhone())
		{
			planList.add(ControllerConstants.TV_SELECTED);
			planList.add(ControllerConstants.PHONE_SELECTED);
			sessionService.setAttribute(ControllerConstants.SELECTED_PLAN_LIST, planList);
			sessionService.setAttribute(ControllerConstants.PLAN_SELECTED, TV_PHONE_SELECTED);
			return REDIRECT_TV_URL;
		}
		else if (form.getInternet())
		{
			planList.add(ControllerConstants.INTERNET_SELECTED);
			sessionService.setAttribute(ControllerConstants.SELECTED_PLAN_LIST, planList);
			sessionService.setAttribute(ControllerConstants.PLAN_SELECTED, INTERNET_SELECTED);
			return REDIRECT_INTERNET_URL;
		}
		else if (form.getTv())
		{
			planList.add(ControllerConstants.TV_SELECTED);
			sessionService.setAttribute(ControllerConstants.SELECTED_PLAN_LIST, planList);
			sessionService.setAttribute(ControllerConstants.PLAN_SELECTED, TV_SELECTED);
			return REDIRECT_TV_URL;
		}
		else if (form.getPhone())
		{
			planList.add(ControllerConstants.PHONE_SELECTED);
			sessionService.setAttribute(ControllerConstants.SELECTED_PLAN_LIST, planList);
			sessionService.setAttribute(ControllerConstants.PLAN_SELECTED, PHONE_SELECTED);
			return REDIRECT_PHONE_URL;
		}
		return REDIRECT_INTERNET_URL;
	}

	@RequestMapping(value = "/removeCombination", method = RequestMethod.GET)
	public String removeCombination(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		cartService.removeSessionCart();
		sessionService.removeAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO);
		sessionService.removeAttribute(ControllerConstants.SELECTED_PLAN_PRICE);
		return REDIRECT_CREATE_COMBINATION_URL;

	}

	private List<ProductModel> filterTvPlan(final List<ProductModel> productForCategory, final LLAPuertoRicoProductConfigDTO productConfig) {
		return new ArrayList<>(llaTmaProductFacade.getTvProductsForInternet(productConfig.getOneP()));
	}


	protected void updatePageTitle(final Model model, final AbstractPageModel cmsPage)
	{
		storeContentPageTitleInModel(model, getPageTitleResolver().resolveHomePageTitle(cmsPage.getTitle()));
	}

	/**
	 * @param model
	 * @param contentPage
	 */
	private void setCMSPageData(final Model model, final ContentPageModel contentPage)
	{
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		updatePageTitle(model, contentPage);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("product.configurator.create.breadcrumb"));
	}

	private List<String> getAllTVAddons(LLAPuertoRicoProductConfigDTO productConfig)
	{
		List <String> allAddons = new ArrayList<>();
		if(StringUtils.isNotEmpty(productConfig.getTwoPAddonDVR()))
		{
			allAddons.add(LLAConfiguratorUtil.DVR_BOX);
		}
		if(StringUtils.isNotEmpty(productConfig.getTwoPAddonHD()))
		{
			allAddons.add(LLAConfiguratorUtil.HD_BOX);
		}
		if(StringUtils.isNotEmpty(productConfig.getTwoPHUB()))
		{
			allAddons.add(LLAConfiguratorUtil.HUB_TV_BOX);
		}
		return allAddons;
	}

	/**
	 * @param product
	 * @return
	 */
	private Long fetchRecurringPriceForPlan(final ProductModel product)
	{
		final Collection<PriceRowModel> priceRowModelCollection = product.getEurope1Prices();
		for (final PriceRowModel priceRow : priceRowModelCollection)
		{
			if (priceRow instanceof SubscriptionPricePlanModel)
			{
				final SubscriptionPricePlanModel pricePlan = (SubscriptionPricePlanModel) priceRow;
				final Collection<RecurringChargeEntryModel> recurringChargeEntries = pricePlan.getRecurringChargeEntries();
				final RecurringChargeEntryModel model = recurringChargeEntries.iterator().next();
				final Double price = model.getPrice();
				return Long.valueOf((long) (null != price ? price.doubleValue() : 0.0));
			}
		}
		return Long.valueOf((long) 0.0);
	}

}
