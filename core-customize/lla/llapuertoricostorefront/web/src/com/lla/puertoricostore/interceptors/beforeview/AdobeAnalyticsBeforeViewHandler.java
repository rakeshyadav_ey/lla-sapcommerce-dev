package com.lla.puertoricostore.interceptors.beforeview;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AdobeAnalyticsBeforeViewHandler implements BeforeViewHandler
{
    private static final String PUERTORICO_PRODUCTION_SITE= "puertorico.productionsite";
    private static final String PUERTORICO_STAGGING_SITE= "puertorico.staggingsite";

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Override
    public void beforeView(HttpServletRequest request, HttpServletResponse response, ModelAndView modelAndView) throws Exception
    {
        modelAndView.addObject("isPuertoricoProductionSite", configurationService.getConfiguration().getString(PUERTORICO_PRODUCTION_SITE));
        modelAndView.addObject("isPuertoricoStaggingSite", configurationService.getConfiguration().getString(PUERTORICO_STAGGING_SITE));
    }
}
