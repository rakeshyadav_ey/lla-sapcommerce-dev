/**
 *
 */
package com.lla.puertoricostore.controllers.cms;

import de.hybris.platform.acceleratorcms.model.components.PuertoricoMiddleComponentModel;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lla.puertoricostore.controllers.ControllerConstants;


/**
 * @author HP737SW
 *
 */
@Controller("PuertoricoMiddleComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.PuertoricoMiddleComponent)
public class PuertoricoMiddleComponentController extends AbstractAcceleratorCMSComponentController<PuertoricoMiddleComponentModel>
{

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final PuertoricoMiddleComponentModel component)
	{
		model.addAttribute("puertoricoMiddleLinksList", component.getPuertoricoMiddleLinksList());
		model.addAttribute("puertoricoMiddleHeadlineText", component.getPuertoricoMiddleHeadlineText());

	}

}
