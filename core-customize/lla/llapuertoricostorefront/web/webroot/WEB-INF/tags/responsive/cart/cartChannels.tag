<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>

<c:if test="${tvaddonProducts.size() > 0}">
  <h3 class="addonChannel-head"><spring:theme code="basket.page.cart.tvaddon.head" text="Additional Boxes"/></h3>
      <c:if test="${cmsSite.uid eq 'puertorico'}">
          <table class="addonChannel">
              <cart:cartAddonEntry cartData="${cartData}" entry="${entry}" addonType="tvaddon"/>
          </table>
      </c:if>
</c:if>

<c:if test="${channelAddonProducts.size() > 0}">
    <h3 class="addonChannel-head"><spring:theme code="basket.page.cart.channel.head" text="Also you can choose"/></h3>
    <c:if test="${cmsSite.uid eq 'puertorico'}">
        <table class="addonChannel">
            <cart:cartAddonEntry cartData="${cartData}" entry="${entry}" addonType="channeladdon"/>
        </table>
    </c:if>
</c:if>
<c:if test="${smartProduct.code ne null}">
    <h3 class="addonChannel-head"><spring:theme code="basket.page.cart.smartChannel.head" text="SmartProtect"/></h3>
    <c:if test="${cmsSite.uid eq 'puertorico'}">
        <table class="addonChannel">
            <cart:cartAddonEntry cartData="${cartData}" entry="${entry}" addonType="smartproduct"/>
        </table>
    </c:if>
</c:if>