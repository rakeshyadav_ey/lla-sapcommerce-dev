<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="entry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ attribute name="addonType" required="false" type="java.lang.String" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<tbody>
    <c:forEach items="${cartData.rootGroups}" var="entryGroup" varStatus="count" >
        <c:forEach items="${entryGroup.orderEntries}" var="entry" varStatus="count" >
            <c:if test="${entry.product.categories[0].code eq addonType}">
                <tr>
                    <td>${entry.product.name}</td>
                    <td class="channel-premium">${entry.product.remarks}</td>
                    <td><span class="channel-price">
                        <c:set var="itemPrice" value="0"/>
                        <c:forEach items="${entry.orderEntryPrices}" var="orderEntryPrice">
                        <fmt:setLocale value="en_US" scope="session" />
                            ${currentCurrency.symbol}<fmt:formatNumber value="${orderEntryPrice.monthlyPrice.value}" pattern="#,##0.00" />
                            <c:set var="itemPrice" value='${orderEntryPrice.monthlyPrice.value}' />
                        </c:forEach>
                    </span></td>
                    <td>
                        <c:url value="/cart/update" var="cartUpdateFormAction" />
                        <c:set var="entryNumberHtml" value="${fn:escapeXml(entry.entryNumber)}"/>
                        <c:set var="productCodeHtml" value="${fn:escapeXml(entry.product.code)}"/>
                        <form:form id="updateCartForm${entry.entryNumber}" action="${cartUpdateFormAction}" method="post" modelAttribute="updateQuantityForm${entry.entryNumber}"
                                   class="js-qty-form${entry.entryNumber}"
                                    data-cart="${fn:escapeXml(cartDataJson)}">
                            <input type="hidden" name="entryNumber" value="${entryNumberHtml}"/>
                            <input type="hidden" name="productCode" value="${productCodeHtml}"/>
                            <input type="hidden" path="quantity" name="quantity" value="0"/>
                            <input type="hidden" data-attr-category="" name="quantity" value="0"/>
                            <button type="submit" class="btn removeAddonBtn" title="Delete from cart"
                                <c:if test="${entry.product.name ne null}"> data-attr-name="${entry.product.name}" </c:if>
                                <c:if test="${entry.product.code ne null}"> data-attr-id="${entry.product.code}" </c:if>
                                <c:if test="${entry.quantity ne null}"> data-attr-quantity="${entry.quantity}" </c:if>
                                <c:if test="${itemPrice ne null}"> data-attr-price="${itemPrice}" </c:if>
                                <c:if test="${entry.product.categories[0].name ne null}"> data-attr-category="${entry.product.categories[0].name}" </c:if>
                                <c:if test="${entry.product.categories[0].parentCategoryName ne null}"> data-attr-primarycategory="${entry.product.categories[0].parentCategoryName}" </c:if>
                            ><span class="glyphicon glyphicon-trash remove"></span></button>
                        </form:form>
                    </td>
                </tr>
             </c:if>
         </c:forEach>
    </c:forEach>
</tbody>