<%@ attribute name="tabIndex" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:message code="checkout.step.addressform.palceholder.walkup" var="walkupName"/>
<spring:message code="checkout.step.addressform.palceholder.walkupNumber" var="walkupNumber"/>
<spring:message code="checkout.step.addressform.palceholder.street" var="street"/>
<spring:message code="checkout.step.addressform.palceholder.apartment" var="apartment"/>
<spring:message code="checkout.step.addressform.palceholder.city" var="city"/>
<spring:message code="checkout.step.addressform.palceholder.pincode" var="pincode"/>

<formElement:formInputBox placeholder="${walkupName}" tabindex="1" idKey="walkupAddressStreetname" labelKey="residence.address.streetname" path="streetname" inputCSS="form-control" mandatory="true" />
<formElement:formInputBox placeholder="${walkupNumber}" tabindex="2" idKey="walkupAddressStreetNumber" labelKey="residence.address.streetnumber" path="streetNumber" inputCSS="form-control" mandatory="false" />
<formElement:formInputBox placeholder="${street}" tabindex="3" idKey="walkupAddressLine1" labelKey="residence.address.line1" path="line1" inputCSS="form-control" mandatory="false" />
<formElement:formInputBox placeholder="${apartment}" tabindex="4" idKey="walkupAddressAppartment" labelKey="residence.address.appartment" path="appartment" inputCSS="form-control" mandatory="true"/>
<formElement:formInputBox placeholder="${city}" tabindex="5" idKey="walkupAddressTownCity" labelKey="residence.address.townCity" path="townCity" inputCSS="form-control" mandatory="true" />
<formElement:formInputBox placeholder="${pincode}" tabindex="6" idKey="walkupAddressPostcode" labelKey="residence.address.postcode" path="postcode" inputCSS="form-control" mandatory="true" />