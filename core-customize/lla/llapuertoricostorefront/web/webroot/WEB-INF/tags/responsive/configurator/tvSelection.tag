<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:url value="/configureProduct/saveTvPlan" var="nextStep"/>
<c:url value="/configureProduct/selectPhone" var="nextStepWithoutSelection"/>
<c:url value="/configureProduct/removeProduct" var="deleteItem"/>
<c:set var="priceMap" value="${productPriceMap}"/>

<div class="configurator-body">
	<%--<c:choose>
		<c:when test="${not empty bundleMessage}">
			<div class="container">
				<div class="step2-infobar">
					<div class="info-msg">
						<div class="hidden-xs">
							<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path fill-rule="evenodd" clip-rule="evenodd" d="M18 33.75C22.1772 33.75 26.1832 32.0906 29.1369 29.1369C32.0906 26.1832 33.75 22.1772 33.75 18C33.75 13.8228 32.0906 9.81677 29.1369 6.86307C26.1832 3.90937 22.1772 2.25 18 2.25C13.8228 2.25 9.81677 3.90937 6.86307 6.86307C3.90937 9.81677 2.25 13.8228 2.25 18C2.25 22.1772 3.90937 26.1832 6.86307 29.1369C9.81677 32.0906 13.8228 33.75 18 33.75ZM18 36C22.7739 36 27.3523 34.1036 30.7279 30.7279C34.1036 27.3523 36 22.7739 36 18C36 13.2261 34.1036 8.64773 30.7279 5.27208C27.3523 1.89642 22.7739 0 18 0C13.2261 0 8.64773 1.89642 5.27208 5.27208C1.89642 8.64773 0 13.2261 0 18C0 22.7739 1.89642 27.3523 5.27208 30.7279C8.64773 34.1036 13.2261 36 18 36Z" fill="#2E71A7"/>
							<path fill-rule="evenodd" clip-rule="evenodd" d="M24.682 11.1824C24.9969 10.8703 25.422 10.6946 25.8653 10.6934C26.3087 10.6921 26.7348 10.8655 27.0514 11.1759C27.368 11.4863 27.5497 11.9088 27.5573 12.3521C27.5648 12.7954 27.3976 13.2239 27.0918 13.5449L18.1097 24.7724C17.9554 24.9387 17.7691 25.0721 17.5619 25.1647C17.3548 25.2573 17.1312 25.3072 16.9043 25.3114C16.6775 25.3156 16.4521 25.2741 16.2417 25.1892C16.0313 25.1043 15.8402 24.9778 15.6798 24.8174L9.7285 18.8639C9.5627 18.7094 9.42972 18.5231 9.33749 18.3161C9.24526 18.1091 9.19567 17.8857 9.19167 17.6591C9.18767 17.4325 9.22935 17.2074 9.31422 16.9973C9.3991 16.7872 9.52542 16.5963 9.68566 16.4361C9.84591 16.2758 10.0368 16.1495 10.2469 16.0646C10.457 15.9798 10.6821 15.9381 10.9087 15.9421C11.1353 15.9461 11.3587 15.9957 11.5657 16.0879C11.7727 16.1801 11.959 16.3131 12.1135 16.4789L16.825 21.1881L24.6393 11.2319C24.6532 11.2145 24.6682 11.1979 24.6842 11.1824H24.682Z" fill="#2E71A7"/>
							</svg>
						</div>
						<div class="col-xs-12 col-sm-10">
							${bundleMessage}
						</div>
						<div class="next-button">
							<form:form action="${nextStepWithoutSelection}" method="get">
								<button type="submit" class="btn btn-next">
									<spring:theme code="configurator.next.button.text" />
								</button>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div class="step1-infobar">
				<div class="container">
					<div class="info-msg">
						<div class="col-xs-12 col-sm-10">
							<spring:theme code="configurator.step2.infobar.text" htmlEscape="false" />
						</div>
						<div class="next-button">
							<form:form action="${nextStepWithoutSelection}" method="get">
								<button type="submit" class="btn btn-next">
									<spring:theme code="configurator.next.button.text" />
								</button>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</c:otherwise>
	</c:choose> --%>

	<div class="container">
		<div class="ctabox-container row">
			<div class="active-products">
				<c:forEach var="activeitem" items="${activeProductList}" varStatus="productIndex">
					<input type="hidden" value="${activeitem.code}">
				</c:forEach>
			</div>
			<c:set var="tvunit" value="0"/>
			<c:forEach var="entry" items="${priceMap}" varStatus="productIndex">
			<c:set var="item" value="${entry.key}"></c:set> 
				<div class="col-sm-4">
					<div class="ctabox <c:if test="${productConfigDTO.twoP == item.code}">selected</c:if>">
						<div class="disabled"></div>
						<div class="ctabox__selected--blur"></div>
						<div class="productName">
                            <p class="pName"> ${item.name}</p>
                        </div>
						<div>
							<%--<div class="img-block">
							    <img src="${item.thumbnail.url}">
								<div class="img-overlay"></div>
							</div>--%>
							<div class="content">
								<%--<div class="step2-tv">
									<c:if test="${not empty productMapping.tvPriceList[tvunit] && productMapping.tvPriceList[tvunit] ne '0'}">
										<c:if test="${productConfigDTO.oneP eq 'internet_250' || productConfigDTO.oneP eq 'internet_500'}">
											<p class="addItemText"><spring:theme code="text.price.puertorico.tvadd" /></p>
										</c:if>
									</c:if>

									<c:choose>
										<c:when test="${item.code eq 'tv_ultimate' || item.code eq 'tv_upick'}">
											<p class="TV-plan Upickultimate">${item.name}</p>
										</c:when>
										<c:otherwise>
											<p class="TV-plan">${item.name}</p>
										</c:otherwise>
									</c:choose>
								</div>--%>
								<div class="internet-specs">
									<div class="internet-spec TV-plan-desc <c:if test="${item.code eq 'tv_ultimate'}">ultimate</c:if> <c:if test="${item.code eq 'tv_upick'}">tv-blue-step2</c:if>">${item.description}<br>
									<c:choose>
										<c:when test="${productConfigDTO.oneP eq 'internet_600' && item.code eq 'tv_ultimate'}">
                                            <spring:theme code="configurator.step2.3hubtv" htmlEscape="false" />
                                        </c:when>
                                        <c:when test="${productConfigDTO.oneP eq 'internet_600' && item.code eq 'tv_upick' && item.code eq 'tv_Primera'}">
                                            <spring:theme code="configurator.step2.1hubtv" htmlEscape="false" />
                                        </c:when>
										<c:when test="${addonProducts[0].code eq 'HUB_TV_BOX' && item.code eq 'tv_upick'}">
											<spring:theme code="configurator.step2.1hubtv" htmlEscape="false" />
										</c:when>
										<c:when test="${productConfigDTO.oneP eq null}">
										<spring:theme code="configurator.step2.1hd.onep" htmlEscape="false" />
										</c:when>
										<c:otherwise>
											<spring:theme code="configurator.step2.1hd" htmlEscape="false" />
										</c:otherwise>
									</c:choose>
									</div>
								</div>
							</div>
							<c:set var="itemPrice" value="0"/>
                            <c:choose>
                                <c:when test="${empty productConfigDTO.oneP}">
                                    <c:forEach var="priceOnetime" items="${item.europe1Prices}">
                                        <c:forEach var="priceData" items="${priceOnetime.recurringChargeEntries}">
                                            <fmt:setLocale value="en_US" scope="session" />
                                                <p class="internet-price">${currentCurrency.symbol} <fmt:formatNumber value="${priceData.price}" pattern="#,##0.00" /><span class="period"><spring:theme code="text.price.puertorico.month" /></span></p>
                                            <c:set var="itemPrice" value='${priceData.price}' />
                                        </c:forEach>
                                    </c:forEach>
                                </c:when>
                                <c:otherwise>
                                    <c:if test="${not empty entry.value && entry.value ne '0'}">
                                        <p class="internet-price"><span class="period"><spring:theme code="text.price.puertorico.tvdiscount" /></span>&nbsp;+${currentCurrency.symbol} <fmt:formatNumber value="${entry.value}" pattern="" /> <span class="period"><spring:theme code="text.price.puertorico.month" /></span></p>
                                        <c:set var="itemPrice" value='${entry.value}' />
                                    </c:if>
                                </c:otherwise>
                            </c:choose>
						</div>
						<!--I want it button here-->
						<form:form action="${nextStep}" method="post" class="step2ConfigForm ctabox-want-form">
                            <%--<div class="checkbox">
                                <c:forEach var="addonProduct" items="${addonProducts}">
                                    <div class="form-group">
                                        <input type="checkbox" value="${addonProduct.code}" id="${addonProduct.code}_${productIndex.count}">
                                        <label for="${addonProduct.code}_${productIndex.count}">${addonProduct.description}</label>
                                    </div>
                                </c:forEach>
                            </div>--%>
							<input type="hidden" name="selectedTVProduct" id="selectedTVProduct" value="${item.code}"/>
							<input type="hidden" name="selecteddAddons" id="selecteddAddons" value=""/>
							<input type="hidden" value="${productConfigDTO.twoPAddonHD}" id="js-twoPAddonHD">
							<input type="hidden" value="${productConfigDTO.twoPAddonDVR}" id="js-twoPAddonHD">
							<input type="hidden" value="${productConfigDTO.twoPHUB}" id="js-twoPHUB">
							<div class="ctabox-btn ctabox-wantbtn--js">
								<button type="submit" class="btn btn-primary"
								    <c:if test="${item.name ne null}"> data-attr-name="${item.name}" </c:if>
                                    <c:if test="${item.code ne null}"> data-attr-id="${item.code}" </c:if>
                                    <c:if test="${itemPrice ne null}"> data-attr-price="${itemPrice}" </c:if>
                                    <c:if test="${item.supercategories[0].name ne null}"> data-attr-category="${item.supercategories[0].name}" </c:if>
                                    <c:if test="${item.supercategories[0].supercategories[0].name ne null}"> data-attr-primarycategory="${item.supercategories[0].supercategories[0].name}" </c:if>
                                    <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
								>
									<spring:theme code="configurator.Iwantit.button.text" />
								</button>
							</div>
						</form:form>

						<!--Delete button here-->
						<form:form action="${deleteItem}" method="post" class="step2ConfigForm ctabox-dlt-form dsp-none">

							<input type="hidden" name="deleteProduct" id="deleteProduct" value="${item.code}"/>
							<input type="hidden" name="stepId" id="stepId" value="2"/>

							<div class="ctabox-btn ctabox-dltbtn--js">
								<button type="submit" class="btn btn-primary text-uppercase">
									<spring:theme code="text.iconCartRemove" />
								</button>
							</div>
						</form:form>
					</div>
				</div>
				<c:set var="tvunit" value="${tvunit+1}"/>
			</c:forEach>
		</div>
		<div class="regulatory-info">
			<p><spring:theme code="basket.page.cart.terms.text" text="regulatoryInfo" /></p>
		</div>
		<div class="sticky-footer clearfix">
			<div class="view-content">
				<p class="view-less"><spring:theme code="text.sticky.footer.view.less" /></p>
				<p class="view-more hide"><spring:theme code="text.sticky.footer.view.more" /></p>
			</div>
			<div class="category-content">
				<c:choose>
					<c:when test="${not empty bundleMessage}">
						${bundleMessage} 
					</c:when>
					<c:otherwise>
						<div>
							<c:forEach items="${planList}" var="selectedPlan" varStatus="productIndex">
								<c:if test="${selectedPlan eq 'tvSelected'}">
									<p class="<c:if test="${selectedPlan ne 'internetSelected'}">active</c:if>"><spring:theme code="text.sticky.footer.select.television" /></p>
								</c:if>
								<c:if test="${selectedPlan eq 'phoneSelected'}">
									<p><spring:theme code="text.sticky.footer.select.telephone" /></p>
								</c:if>
							</c:forEach>
						</div>
						<div>
							<p class="price">
								<span class="price-text">${currentCurrency.symbol}0.00</span><spring:theme code="text.price.puertorico.month" />
							</p>
						</div>
					</c:otherwise>
				</c:choose>
			</div>
    	</div>
		<div class="bundleinfo visible-xs">
			<p class="data"><spring:theme code="configurator.bundle.info" /></p>
		</div>
	</div>
</div>

