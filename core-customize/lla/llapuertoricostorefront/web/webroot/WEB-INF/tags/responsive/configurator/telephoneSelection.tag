<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:url value="/configureProduct/savePhone" var="nextStep"/>
<c:url value="/configureProduct/selectAddons" var="nextStepWithoutSelection"/>

<div class="configurator-body">
    <c:if test="${not empty productList}">
        <c:forEach var="item" items="${productList}" varStatus="productIndex">
            <div class="container">
                <div class="ctabox-container row">
                    <c:forEach var="item" items="${productList}" varStatus="productIndex">
                        <div class="col-sm-4">
                            <div class="ctabox <c:if test="${productConfigDTO.oneP == item.code}">selected</c:if>">
                                <div class="ctabox__selected--blur"></div>
                                <div class="productName">
                                    <p class="pName"> ${item.name}</p>
                                </div>
                                <div>
                                    <div class="content">
                                        <div class="internet-specs">
                                            <c:forEach var="specCharValue" items="${item.productSpecCharacteristicValues}" varStatus="index">
                                                <div class="internet-spec ${specCharValue.productSpecCharacteristic.id}">
                                                    ${specCharValue.ctaSpecDescription}
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                	<c:set var="itemPrice" value="0"/>
                                    <c:choose>
                                        <c:when test="${empty productConfigDTO.oneP && empty productConfigDTO.twoP}">
                                            <c:forEach var="priceOnetime" items="${item.europe1Prices}">
                                            <c:forEach var="priceData" items="${priceOnetime.recurringChargeEntries}">
                                            <fmt:setLocale value="en_US" scope="session" />
                                                <p class="internet-price">${currentCurrency.symbol} <fmt:formatNumber value="${priceData.price}" pattern="" /><span class="period"><spring:theme code="text.price.puertorico.month" /></span></p>
                                                <c:set var="itemPrice" value='${priceData.price}' />
                                            </c:forEach>
                                            </c:forEach>

                                        </c:when>
                                        <c:otherwise>
                                            <c:if test="${not empty productMapping.phonePrice && productMapping.phonePrice ne '0'}">
                                                <p class="internet-price"><span class="period"> <spring:theme code="text.price.puertorico.phonediscount" /> </span>&nbsp;+${currentCurrency.symbol} <fmt:formatNumber value="${productMapping.phonePrice}" pattern="" /><span class="period"><spring:theme code="text.price.puertorico.month" /></span></p>
                                            </c:if>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <!--I want it button in below form-->
                                <form:form action="${nextStep}" method="post" class="step2ConfigForm ctabox-want-form">
                                    <input type="hidden" name="selectedPhone" id="selectedPhone" value="${item.code}"/>

                                    <div class="ctabox-btn ctabox-wantbtn--js">
                                        <button type="submit" class="btn btn-primary"
                                            <c:if test="${item.name ne null}"> data-attr-name="${item.name}" </c:if>
                                            <c:if test="${item.code ne null}"> data-attr-id="${item.code}" </c:if>
                                            <c:if test="${itemPrice ne null}"> data-attr-price="${itemPrice}" </c:if>
                                            <c:if test="${item.supercategories[0].name ne null}"> data-attr-category="${item.supercategories[0].name}" </c:if>
                                            <c:if test="${item.supercategories[0].supercategories[0].name ne null}"> data-attr-primarycategory="${item.supercategories[0].supercategories[0].name}" </c:if>
                                            <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
                                        >
                                             <spring:theme code="configurator.Iwantit.button.text" />
                                        </button>
                                    </div>
                                </form:form>
                                <!--Delete button in below form-->
                                <form:form action="${deleteItem}" method="post" class="step1ConfigForm ctabox-dlt-form dsp-none">

                                    <input type="hidden" name="deleteProduct" id="deleteProduct" value="${item.code}"/>
                                    <input type="hidden" name="stepId" id="stepId" value="1"/>
                                    <div class="ctabox-btn ctabox-dltbtn--js">
                                        <button type="submit" class="btn btn-primary text-uppercase">
                                            <spring:theme code="text.iconCartRemove" />
                                        </button>
                                    </div>
                                </form:form>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <div class="row">
                    <div class="col-sm-12 terms">
                        <div class="regulatory-info text-center">
                           <p><spring:theme code="basket.page.cart.terms.text" text="regulatoryInfo" /></p>
                        </div>
                    </div>
                </div>

                <div class="sticky-footer clearfix">
                    <div class="view-content">
                        <p class="view-less"><spring:theme code="text.sticky.footer.view.less" /></p>
                        <p class="view-more hide"><spring:theme code="text.sticky.footer.view.more" /></p>
                    </div>
                    <div class="category-content">
                        <c:choose>
                            <c:when test="${not empty bundleMessage}">
                                ${bundleMessage} 
                            </c:when>
                            <c:otherwise>
                                <div>
                                    <c:forEach items="${planList}" var="selectedPlan">
                                        <c:if test="${selectedPlan eq 'phoneSelected'}">
                                            <p><spring:theme code="text.sticky.footer.select.telephone" /></p>
                                        </c:if>
                                    </c:forEach>
                                </div>
                                <div>
                                    <p class="price">
                                        <span class="price-text">${currentCurrency.symbol}0.00</span><spring:theme code="text.price.puertorico.month" />
                                    </p>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>

                <%--<div class="row">
                    <div class="col-xs-12">
                        <div class="config-bnr config-step-3 relative">
                            <img src="${item.thumbnail.url}" alt="Config Banner" class="img-responsive img-fit"/>
                            <div class="config-bnr__text">
                                <div class="phone">
                                    <svg width="57" height="56" viewBox="0 0 57 56" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M54.2656 2.73438L43.3281 0.109375C41.6875 -0.21875 40.0469 0.546875 39.3906 2.07812L34.25 14C33.7031 15.4219 34.0312 17.0625 35.2344 17.9375L41.1406 22.75C37.4219 30.4062 31.2969 36.5312 23.6406 40.25L18.8281 34.3438C17.9531 33.1406 16.3125 32.8125 14.8906 33.3594L2.96875 38.5C1.54688 39.1562 0.671875 40.7969 1 42.3281L3.625 53.375C3.95312 54.9062 5.26562 56 6.90625 56C34.4688 56 57 33.6875 57 6.01562C57 4.375 55.9062 3.0625 54.2656 2.73438ZM7.01562 52.5L4.5 41.6719L16.2031 36.6406L22.7656 44.625C34.0312 39.2656 40.1562 33.1406 45.5156 21.875L37.5312 15.3125L42.5625 3.60938L53.3906 6.125C53.3906 31.7188 32.6094 52.5 7.01562 52.5Z" fill="#2D9CDB"/>
                                    </svg>

                                </div>
                                <p class="config-bnr__text-para">
                                    ${item.description}
                                </p>
				
				<c:set var="itemPrice" value="0"/>
                                <c:choose>
									<c:when test="${empty productConfigDTO.oneP && empty productConfigDTO.twoP}">
										<c:forEach var="priceOnetime" items="${item.europe1Prices}">
										<c:forEach var="priceData" items="${priceOnetime.recurringChargeEntries}">
										<fmt:setLocale value="en_US" scope="session" />
											<p class="telephone-price">${currentCurrency.symbol} <fmt:formatNumber value="${priceData.price}" pattern="" /><span class="period"><spring:theme code="text.price.puertorico.month" /></span></p>
										        <c:set var="itemPrice" value='${priceData.price}' />
										</c:forEach>
										</c:forEach>

									</c:when>
									<c:otherwise>
										<c:if test="${not empty productMapping.phonePrice && productMapping.phonePrice ne '0'}">
											<p class="telephone-price"><span class="optionFor"> <spring:theme code="text.price.puertorico.phonediscount" /> </span>&nbsp;+${currentCurrency.symbol} <fmt:formatNumber value="${productMapping.phonePrice}" pattern="" /><span class="period"><spring:theme code="text.price.puertorico.month" /></span></p>
										        <c:set var="itemPrice" value='${productMapping.phonePrice}' />
										</c:if>
									</c:otherwise>
   							</c:choose>
			 <div class="ctabox-container">

                                <div class="config-bnr__text-btn">
                                    <form:form action="${nextStep}" method="post" class="step2ConfigForm ctabox-want-form">
                                        <input type="hidden" name="selectedPhone" id="selectedPhone" value="${item.code}"/>

                                        <div class="ctabox-btn">
                                            <button type="submit" class="btn btn-primary"
					       <c:if test="${item.name ne null}"> data-attr-name="${item.name}" </c:if>
                                               <c:if test="${item.code ne null}"> data-attr-id="${item.code}" </c:if>
                                               <c:if test="${itemPrice ne null}"> data-attr-price="${itemPrice}" </c:if>
                                               <c:if test="${item.supercategories[0].name ne null}"> data-attr-category="${item.supercategories[0].name}" </c:if>
                                               <c:if test="${item.supercategories[0].supercategories[0].name ne null}"> data-attr-primarycategory="${item.supercategories[0].supercategories[0].name}" </c:if>
                                               <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
					    >
                                                <spring:theme code="configurator.Iwantit.button.text" />
                                            </button>
                                        </div>
                                    </form:form>
                                </div>
			     </div>	
                            </div>
                        </div>
                    </div>
                 </div>--%>

            </div>
        </c:forEach>
    </c:if>
<%--    Telephone plan price   "${selected_plan_price }"  --%>
</div>