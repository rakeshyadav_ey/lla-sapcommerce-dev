<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true"%>
<%@ attribute name="checkoutSteps" required="true" type="java.util.List" %>
<%@ attribute name="progressBarId" required="true" type="java.lang.String" %>
<%@ attribute name="cssClass" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<ycommerce:testId code="checkoutSteps">
    <c:if test="${siteId eq 'puertorico'}">
        <input type="hidden" id="c2cTimerForLcprApi" name="c2cTimerForLcprApi" value="${c2cTimerForLcprApi}"/>
        <input type="hidden" id="c2cTimerForCallBack" name="c2cTimerForCallBack" value="${c2cTimerForCallBack}"/>
    </c:if>
    <div class="checkout-steps ${fn:escapeXml(cssClass)}">
        <c:forEach items="${checkoutSteps}" var="checkoutStep" varStatus="status">
            <c:url value="${checkoutStep.url}" var="stepUrl"/>
            <c:choose>
                <c:when test="${progressBarId eq checkoutStep.progressBarId and checkoutStep.progressBarId ne 'confirmOrder'}">
                    <c:set scope="page"  var="activeCheckoutStepNumber"  value="${checkoutStep.stepNumber}"/>
                    <a href="${fn:escapeXml(stepUrl)}" class="step-head js-checkout-step active" id="step${checkoutStep.stepNumber}">
                    <c:if test="${cmsSite.uid eq 'puertorico'}">
                     <div class="title"><spring:theme code="checkout.multi.puertorico.${checkoutStep.progressBarId}"/></div>
                     </c:if> 
                    <c:if test="${cmsSite.uid eq 'jamaica' || cmsSite.uid eq 'panama'}">
                    <div class="title"><spring:theme code="checkout.multi.${checkoutStep.progressBarId}"/></div>
                    </c:if>
                    </a>
                    <div class="step-body"><jsp:doBody/></div>
                </c:when>
                <c:when test="${checkoutStep.stepNumber > activeCheckoutStepNumber and checkoutStep.progressBarId ne 'confirmOrder'}">
                    <a href="${fn:escapeXml(stepUrl)}" class="step-head js-checkout-step inactive">
                     <c:if test="${cmsSite.uid eq 'puertorico'}">
                        <div class="title">
                            <spring:theme code="checkout.multi.puertorico.${checkoutStep.progressBarId}"/>
                        </div>
                      </c:if>
                     <c:if test="${cmsSite.uid eq 'jamaica' || cmsSite.uid eq 'panama'}">
                    <div class="title"><spring:theme code="checkout.multi.${checkoutStep.progressBarId}"/></div>
                    </c:if>
                    </a>
                </c:when>
                <c:otherwise>
                    <c:set scope="page"  var="creditSelectedFromUser"  value="yes"/>
                    <c:if test="${checkoutStep.stepNumber eq 4 && (hideCreditCheck ne null && hideCreditCheck eq 'true')}">
                        <c:set scope="page"  var="creditSelectedFromUser"  value="no"/>
                    </c:if>
	                <c:if test="${checkoutStep.progressBarId ne 'confirmOrder' && creditSelectedFromUser eq 'yes'}">
	                    <a href="${fn:escapeXml(stepUrl)}" class="step-head js-checkout-step ">
	                     <c:if test="${cmsSite.uid eq 'puertorico'}">
                        <div class="title">
                            <spring:theme code="checkout.multi.puertorico.${checkoutStep.progressBarId}"/>
                            <span class="completeIcon"></span>
                        </div>
                      </c:if>
                       <c:if test="${cmsSite.uid eq 'jamaica' || cmsSite.uid eq 'panama'}">
	                        <div class="title"><spring:theme code="checkout.multi.${checkoutStep.progressBarId}"/></div>
	                    </c:if>
	                        <%--<div class="edit">
	                            <span class="glyphicon glyphicon-pencil"></span>
	                        </div>--%>
	                    </a>
	                </c:if>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </div>
</ycommerce:testId>