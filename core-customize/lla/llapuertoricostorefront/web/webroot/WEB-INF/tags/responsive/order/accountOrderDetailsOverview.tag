<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="order-detail-overview">
<h3 class="summary-head"><spring:theme code="checkout.multi.order.summary" text="Order Summary"/></h3>
    <div class="row orderData-right">
        <div class="col-sm-12">
            <div class="item-group">
                <ycommerce:testId code="orderDetail_overviewOrderID_label">
                    <span class="item-label"><spring:theme code="text.account.orderHistory.orderNumber"/></span>
                    <span class="item-value">&#x23;${fn:escapeXml(orderData.code)}</span>
                </ycommerce:testId>
            </div>
        </div>
        <c:if test="${not empty orderData.statusDisplay}">
            <div class="col-sm-12">
                <div class="item-group">
                    <ycommerce:testId code="orderDetail_overviewOrderStatus_label">
                        <span class="item-label"><spring:theme code="text.account.orderHistory.orderStatus"/></span>
                        <span class="item-value"><spring:theme code="text.account.order.status.display.${orderData.statusDisplay}"/></span>
                    </ycommerce:testId>
                </div>
            </div>
        </c:if>
        <div class="col-sm-12">
            <div class="item-group">
                <ycommerce:testId code="orderDetail_overviewStatusDate_label">
                    <span class="item-label"><spring:theme code="text.account.orderHistory.dateRequest"/></span>
                    <span class="item-value"><fmt:formatDate value="${order.created}" pattern = "dd-MM-yyyy"/></span>
                </ycommerce:testId>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="item-group">
                <ycommerce:testId code="orderDetail_overviewOrderTotal_label">
                    <span class="item-label"><spring:theme code="text.account.order.total"/></span>
                    <c:if test="${cmsSite.uid ne 'puertorico'}">
                        <span class="item-value"><format:price priceData="${order.totalPriceWithTax}"/></span>
                    </c:if>
                    <c:if test="${cmsSite.uid eq 'puertorico'}">
                        <span class="item-value">${currentCurrency.symbol}${fn:escapeXml(orderData.totalPrice.value)}</span>
                     </c:if>
                </ycommerce:testId>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="item-group">
                <ycommerce:testId code="orderDetail_overviewOrderTotal_label">
                    <span class="item-label"><spring:theme code="text.account.order.address"/></span>
                    <span class="item-value">
                    <c:set var="deliveryAddress" value="${order.deliveryAddress}"/>
                       ${ycommerce:encodeHTML(deliveryAddress.line1)} &nbsp; ${ycommerce:encodeHTML(deliveryAddress.line2)} &nbsp; ${ycommerce:encodeHTML(deliveryAddress.town)} &nbsp; ${ycommerce:encodeHTML(deliveryAddress.postalCode)} &nbsp; ${ycommerce:encodeHTML(deliveryAddress.country.isocode)} &nbsp;
                    </span>
                </ycommerce:testId>
            </div>
        </div>
        <c:if test="${orderData.quoteCode ne null}">
			  <div class="col-sm-12">
			  	  <div class="item-group">
					  <spring:url htmlEscape="false" value="/my-account/my-quotes/{/quoteCode}" var="quoteDetailUrl">
					  <spring:param name="quoteCode"  value="${orderData.quoteCode}"/>
					  </spring:url>
		              <ycommerce:testId code="orderDetail_overviewQuoteId_label">
							  <span class="item-label"><spring:theme code="text.account.quote.code"/></span>
							  <span class="item-value">
								  <a href="${fn:escapeXml(quoteDetailUrl)}" >
								  	  ${fn:escapeXml(orderData.quoteCode)}
								  </a>
							  </span>
		              </ycommerce:testId>
					</div>
				</div>
			</c:if>
        </div>
    </div>
</div>

<input type="hidden" id="addressInfo"
    <c:if test="${orderData.code ne null}"> data-attr-order-id="${fn:escapeXml(orderData.code)}" </c:if>
    <c:if test="${user.passport ne null}"> data-attr-passport="${user.passport}" </c:if>
    <c:if test="${user.driversLicence ne null}"> data-attr-drivingLicence="${user.driversLicence}" </c:if>
    <c:if test="${order.deliveryAddress.postalCode ne null}"> data-attr-postalCode="${order.deliveryAddress.postalCode}" </c:if>
    <c:if test="${order.deliveryAddress.town ne null}"> data-attr-town="${orderData.deliveryAddress.town}" </c:if>
    <c:if test="${order.deliveryAddress.country.name ne null}"> data-attr-country="${orderData.deliveryAddress.country.name}" </c:if>
    <c:if test="${orderData.contractType.name ne null}"> data-attr-contract="${orderData.contractType.name}" </c:if>
/>