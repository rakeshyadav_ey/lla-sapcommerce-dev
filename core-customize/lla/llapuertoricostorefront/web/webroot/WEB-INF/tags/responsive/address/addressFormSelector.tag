<%@ attribute name="supportedCountries" required="true" type="java.util.List"%>
<%@ attribute name="regions" required="true" type="java.util.List"%>
<%@ attribute name="country" required="false" type="java.lang.String"%>
<%@ attribute name="cancelUrl" required="false" type="java.lang.String"%>
<%@ attribute name="addressBook" required="false" type="java.lang.String"%>
<%@ attribute name="urbanResidenceTypes" required="false" type="java.util.List"%>
<%@ attribute name="ruralResidenceTypes" required="false" type="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<!--<c:if test="${not empty deliveryAddresses}">
	<button type="button" class="btn btn-default btn-block js-address-book">
		<spring:theme code="checkout.checkout.multi.deliveryAddress.viewAddressBook" />
	</button>
	<br>
</c:if>-->

<c:if test="${empty addressFormEnabled or addressFormEnabled}">
	<!--<div id="locationField">
      <input id="autocomplete" placeholder="Enter your address" onFocus="geolocate()" type="text" class="form-control"/>
    </div>-->
	<form:form method="post"  id="addressForm" modelAttribute="addressForm">
		<form:hidden path="addressId" class="add_edit_delivery_address_id"
			status="${not empty suggestedAddresses ? 'hasSuggestedAddresses' : ''}" />
		<input type="hidden" name="bill_state" id="address.billstate" />

		<c:choose>
		   <c:when test="${cmsSite.uid eq 'puertorico'}">
		         <div class="controls">
		            <p class="addressTypeText"><spring:theme code="checkout.multi.addressTypes.text"/></p>
                     <div class="addressTypes">
                         <div class="addressTypeRadio">
                             <label class="addressRadioLabel" for="walkUp"><spring:theme code="checkout.multi.type.residence.walkUp" />
                             <form:radiobutton path="typeOfResidence" class="addressRadioButton" id="walkUp" value="WALKUP"/>
                             <span class="checkmark"></span>
                             </label>
                         </div>

                         <div class="addressTypeRadio">
                             <label class="addressRadioLabel" for="urban"><spring:theme code="checkout.multi.type.residence.urban" />
                             <form:radiobutton path="typeOfResidence" class="addressRadioButton" id="urban" value="URBAN"/>
                             <span class="checkmark"></span>
                             </label>
                         </div>

                         <div class="addressTypeRadio">
                             <label class="addressRadioLabel" for="rural"><spring:theme code="checkout.multi.type.residence.rural" />
                             <form:radiobutton path="typeOfResidence" class="addressRadioButton" id="rural" value="RURAL"/>
                             <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                 </div>
                 <div class="addressTypesFields">
                     <div id="walkUpFormElements">
                         <address:walkupAddressFormElements/>
                     </div>
                     <div  id="urbanFormElements">
                         <address:urbanAddressFormElements urbanResidenceTypes="${urbanResidenceTypes}"/>
                     </div>
                     <div id="ruralFormElements">
                         <address:ruralAddressFormElements ruralResidenceTypes="${ruralResidenceTypes}"/>
                     </div>
                 </div>

                 <div id="addressFormErrors">
                     <input type="hidden" id="errorMsgStreetname" value="<spring:theme code="addess.walkup.streetname.invalid"/>" >
                     <input type="hidden" id="errorMsgApartment" value="<spring:theme code="addess.walkup.appartment.invalid"/>" >
                     <input type="hidden" id="errorMsgTowncity" value="<spring:theme code="addess.walkup.townCity.invalid"/>" >
                     <input type="hidden" id="errorMsgPostcode" value="<spring:theme code="addess.walkup.postcode.invalid"/>" >
                     <input type="hidden" id="errorMsgPostcodeLength" value="<spring:theme code="addess.walkup.postcodelength.invalid"/>" >
                     <input type="hidden" id="errorMsgBuilding" value="<spring:theme code="addess.urban.building.invalid"/>" >
                     <input type="hidden" id="errorMsgKilometer" value="<spring:theme code="addess.rural.kilometer.invalid"/>" >
					 <input type="hidden" id="errorMsgHighway" value="<spring:theme code="addess.rural.line1.invalidHighway"/>" >
					 <input type="hidden" id="errorMsgStreet" value="<spring:theme code="addess.rural.line1.invalidStreet"/>" >
                 </div>

		   </c:when>
		   <c:otherwise>
		 <div id="countrySelector" data-address-code="${fn:escapeXml(addressData.id)}"
			data-country-iso-code="${fn:escapeXml(addressData.country.isocode)}"
			class="form-group">
			<formElement:formSelectBox idKey="address.country"
                            labelKey="address.country" path="countryIso" mandatory="true"
                            skipBlank="false" skipBlankMessageKey="address.country"
                            items="${supportedCountries}" itemValue="isocode"
                            selectedValue="${supportedCountries[0].isocode}"
                            selectCSSClass="form-control hide" />
		</div>
		<div id="i18nAddressForm" class="i18nAddressForm">
			<c:if test="${not empty country}">
				<address:addressFormElements regions="${regions}"
					country="${country}" />
			</c:if>
		</div>
		</c:otherwise>
       </c:choose>
		<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
			<div class="checkbox">
				<c:choose>
					<c:when test="${showSaveToAddressBook}">
						<formElement:formCheckbox idKey="saveAddressInMyAddressBook"
							labelKey="checkout.summary.deliveryAddress.saveAddressInMyAddressBook"
							path="saveInAddressBook" inputCSS="add-address-left-input"
							labelCSS="add-address-left-label" mandatory="true" />
					</c:when>
					<c:when test="${not addressBookEmpty && not isDefaultAddress}">
						<ycommerce:testId code="editAddress_defaultAddress_box">
							<formElement:formCheckbox idKey="defaultAddress"
								labelKey="address.default" path="defaultAddress"
								inputCSS="add-address-left-input"
								labelCSS="add-address-left-label" mandatory="true" />
						</ycommerce:testId>
					</c:when>
				</c:choose>
			</div>
		</sec:authorize>
		<div id="addressform_button_panel" class="form-actions">
			<c:choose>
				<c:when test="${edit eq true && not addressBook}">
					<ycommerce:testId code="multicheckout_saveAddress_button">
						<button
							class="positive right change_address_button show_processing_message"
							type="submit">
							<spring:theme code="checkout.multi.saveAddress" />
						</button>
					</ycommerce:testId>
				</c:when>
				<c:when test="${addressBook eq true}">
					<div class="accountActions">
						<div class="row">
							<div class="col-sm-6 col-sm-push-6 accountButtons">
								<ycommerce:testId code="editAddress_saveAddress_button">
									<button class="btn btn-primary btn-block change_address_button show_processing_message"
											type="submit">
										<spring:theme code="text.button.save" />
									</button>
								</ycommerce:testId>
							</div>
							<div class="col-sm-6 col-sm-pull-6 accountButtons">
								<ycommerce:testId code="editAddress_cancelAddress_button">
									<c:url value="${cancelUrl}" var="cancel"/>
									<a class="btn btn-block btn-default" href="${fn:escapeXml(cancel)}">
										<spring:theme code="text.button.cancel" />
									</a>
								</ycommerce:testId>
							</div>
						</div>
					</div>
				</c:when>
			</c:choose>
		</div>
	</form:form>
</c:if>