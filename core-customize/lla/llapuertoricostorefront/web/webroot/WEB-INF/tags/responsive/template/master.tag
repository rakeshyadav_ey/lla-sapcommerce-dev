<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true" %>
<%@ attribute name="metaDescription" required="false" %>
<%@ attribute name="metaKeywords" required="false" %>
<%@ attribute name="pageCss" required="false" fragment="true" %>
<%@ attribute name="pageScripts" required="false" fragment="true" %>

<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="analytics" tagdir="/WEB-INF/tags/shared/analytics" %>
<%@ taglib prefix="addonScripts" tagdir="/WEB-INF/tags/responsive/common/header" %>
<%@ taglib prefix="generatedVariables" tagdir="/WEB-INF/tags/shared/variables" %>
<%@ taglib prefix="debug" tagdir="/WEB-INF/tags/shared/debug" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="htmlmeta" uri="http://hybris.com/tld/htmlmeta"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<!DOCTYPE html>
<html lang="${fn:escapeXml(currentLanguage.isocode)}">
<head>
	<title>
		${not empty pageTitle ? pageTitle : not empty cmsPage.title ? fn:escapeXml(cmsPage.title) : 'Accelerator Title'}
	</title>

	<%-- Meta Content --%>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8">
    <meta name="referrer" content="no-referrer-when-downgrade" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

	<%-- Additional meta tags --%>
	<htmlmeta:meta items="${metatags}"/>

	<%-- Favourite Icon --%>
	<spring:theme code="img.favIcon" text="/" var="favIconPath"/>
	
	<c:choose>
		<%-- if empty webroot, skip originalContextPath, simply use favIconPath --%>
		<c:when test="${fn:length(originalContextPath) eq 1}" >
			<link rel="shortcut icon" type="image/x-icon" media="all" href="${favIconPath}" />
		</c:when>
		<c:otherwise>
			<link rel="shortcut icon" type="image/x-icon" media="all" href="${originalContextPath}${favIconPath}" />
		</c:otherwise>
	</c:choose>

	<%-- CSS Files Are Loaded First as they can be downloaded in parallel --%>
	<template:styleSheets/>

	<%-- Inject any additional CSS required by the page --%>
	<jsp:invoke fragment="pageCss"/>
	<analytics:analytics/>
	<generatedVariables:generatedVariables/>	
</head>

<body class="${pageBodyCssClasses} ${cmsPageRequestContextData.liveEdit ? ' yCmsLiveEdit' : ''} language-${fn:escapeXml(currentLanguage.isocode)}">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5L799W3" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

	<%-- Inject the page body here --%>
	<jsp:doBody/>


	<form name="accessiblityForm">
		<input type="hidden" id="accesibility_refreshScreenReaderBufferField" name="accesibility_refreshScreenReaderBufferField" value=""/>
	</form>
	<div id="ariaStatusMsg" class="skip" role="status" aria-relevant="text" aria-live="polite"></div>

	<%-- Load JavaScript required by the site --%>
	<template:javaScript/>
	
	<%-- Inject any additional JavaScript required by the page --%>
	<jsp:invoke fragment="pageScripts"/>

	<%-- Inject CMS Components from addons using the placeholder slot--%>
	<addonScripts:addonScripts/>
    <c:choose>
        <c:when test="${not empty breadcrumbs}">
            <c:set var="breadcrumbinfo"><spring:theme code="breadcrumb.home.puertorico" />,</c:set>
            <c:forEach items="${breadcrumbs}" var="breadcrumb" varStatus="status">
                <c:set var="breadcrumbinfo">${breadcrumbinfo}${breadcrumb.name}<c:if test="${not status.last}">, </c:if></c:set>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <c:set var="breadcrumbinfo" value="N/A"/>
        </c:otherwise>
    </c:choose>

    <input type="hidden" id="adobeAnalyticsPageInfo"
        <c:if test="${pageType ne null}"> data-attr-pageName="${pageType}" </c:if>
        <c:if test="${breadcrumbinfo ne null}"> data-attr-breadcrumbsdata="${breadcrumbinfo}" </c:if>
        <c:if test="${user.customerId ne null}"> data-attr-userid="${user.customerId}" </c:if>
        <c:if test="${user.additionalEmail ne null}"> data-attr-email="${user.additionalEmail}" </c:if>
        <c:if test="${user.customerType ne null && user.customerType ne '' }"> data-attr-customerType="${user.customerType}" </c:if>
        <c:if test="${user.fixedPhone ne null && user.fixedPhone ne '' }"> data-attr-mobilePhone="${user.fixedPhone}" </c:if>
    />

	<c:url value="/configureProduct/removeCombination" var="continueCart"/>
	<c:if test="${not empty cartData.rootGroups}"> 
		<input type="hidden" id="cartCheck" value="true">
	</c:if>
	<a href="#cartItemRemoval-popup" class="cartItemRemoval-popup-link hidden">
		remove cart
	</a>
	<div class="hidden">
		<div id="cartItemRemoval-popup">
			<p class="cartItemRemoval-msg"><spring:theme code="text.cart.remove.product.message" htmlEscape="false"/></p>
			<div>
				<a class="btn btn-primary btn-block cartRemoveProducts" href="${continueCart}"><spring:theme code="text.cart.remove.product.button"/></a>
			</div>
			<div>
				<button class="btn btn-default btn-block cartItemRemoval-popup-close"><spring:theme code="text.cart.popup.close"/></button>
			</div>
		</div>
	</div>

</body>

<debug:debugFooter/>

</html>
