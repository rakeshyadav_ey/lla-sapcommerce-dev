<%@ attribute name="tabIndex" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ attribute name="ruralResidenceTypes" required="true" type="java.util.List"%>

<spring:message code="checkout.step.addressform.palceholder.sector" var="sector"/>
<spring:message code="checkout.step.addressform.palceholder.plotNumber" var="plotNumber"/>
<spring:message code="checkout.step.addressform.palceholder.kilometer" var="kilometer"/>
<spring:message code="checkout.step.addressform.palceholder.ruralHighway" var="extra"/>
<spring:message code="checkout.step.addressform.palceholder.city" var="city"/>
<spring:message code="checkout.step.addressform.palceholder.pincode" var="pincode"/>

<input type="hidden" id="placeHolderRuralHighway" value="<spring:theme code="checkout.step.addressform.palceholder.ruralHighway"/>" >
<input type="hidden" id="placeHolderRuralStreet" value="<spring:theme code="checkout.step.addressform.palceholder.ruralStreet"/>" >

<formElement:formInputBox placeholder="${sector}" tabindex="1" idKey="ruralAddressArea" labelKey="residence.address.area" path="area" inputCSS="form-control" mandatory="false" />
<formElement:formInputBox placeholder="${plotNumber}" tabindex="2" idKey="ruralAddressPlotNo" labelKey="residence.address.plotNo" path="plotNo" inputCSS="form-control" mandatory="false" />
<formElement:formSelectBoxDefaultEnabled skipBlank="true" tabindex="3" skipBlankMessageKey="form.select.none" idKey="ruralAddressLine2"  labelKey="residence.rural.address.line2"  path="line2" mandatory="true"
                                         items="${ruralResidenceTypes}"  selectCSSClass="form-control"/>
<formElement:formInputBox placeholder="${extra}" tabindex="4" idKey="ruralAddressLine1" labelKey=" " path="line1" inputCSS="form-control" mandatory="true" />
<formElement:formInputBox placeholder="${kilometer}" tabindex="5" idKey="ruralAddressKilometer" labelKey="residence.address.kilometer" path="kilometer" inputCSS="form-control" mandatory="true" />
<formElement:formInputBox placeholder="${city}" tabindex="6" idKey="ruralAddressTownCity" labelKey="residence.address.townCity" path="townCity" inputCSS="form-control" mandatory="true" />
<formElement:formInputBox placeholder="${pincode}" tabindex="7" idKey="ruralAddressPostcode" labelKey="residence.address.postcode" path="postcode" inputCSS="form-control" mandatory="true" />