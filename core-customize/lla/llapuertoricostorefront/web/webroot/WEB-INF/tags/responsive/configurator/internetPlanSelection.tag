<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:url value="/configureProduct/saveInternetPlan" var="nextStep"/>
<c:url value="/configureProduct/selectTvPlan" var="nextStepWithoutSelection"/>
<c:url value="/configureProduct/removeProduct" var="deleteItem"/>

<div class="configurator-body">
	<%--<div class="step1-infobar">
		<div class="container">
			<div class="info-msg">
				<div class="col-xs-12 col-sm-10">
					<spring:theme code="configurator.step1.infobar.text" htmlEscape="false" />
				</div>
				<div class="next-button">
					<form:form action="${nextStepWithoutSelection}" method="get">
						<button type="submit" class="btn btn-next">
							<spring:theme code="configurator.next.button.text" />
						</button>
					</form:form>
				</div>
			</div>
		</div>
	</div>--%>
	<div class="container">
		<div class="ctabox-container row">
			<c:forEach var="item" items="${productList}" varStatus="productIndex">
				<div class="col-sm-4">
					<div class="ctabox <c:if test="${productConfigDTO.oneP == item.code}">selected</c:if>">
						<div class="ctabox__selected--blur"></div>
						<div class="productName">
                            <p class="pName"> ${item.name}</p>
                        </div>
						<div>
							<%--<div class="img-block">
								<img src="${item.thumbnail.url}" />
								<div class="img-overlay"></div>
							</div>--%>
							<div class="content">
							    <div class="internet-specs">
                                    <c:forEach var="specCharValue" items="${item.productSpecCharacteristicValues}" varStatus="index">
                                        <div class="internet-spec ${specCharValue.productSpecCharacteristic.id}">
                                            ${specCharValue.ctaSpecDescription}
                                        </div>
                                    </c:forEach>
                                </div>
                                <div>
                                <c:set var="itemPrice" value="0"/>
                                <c:forEach var="priceOnetime" items="${item.europe1Prices}">
                                    <c:forEach var="priceData" items="${priceOnetime.recurringChargeEntries}">
                                    <fmt:setLocale value="en_US" scope="session" />
                                        <p class="internet-price">${currentCurrency.symbol} <fmt:formatNumber value="${priceData.price}" pattern="#,##0.00" /><span class="period"><spring:theme code="text.price.puertorico.month" /></span></p>
                                        <c:set var="itemPrice" value='${priceData.price}' />
                                    </c:forEach>
                                </c:forEach>

                                </div>
							</div>
						</div>
						<!--I want it button in below form-->
						<form:form action="${nextStep}" method="post" class="step1ConfigForm ctabox-want-form">
						    <c:if test="${tvaddonVisibility eq 'true'}">
                                <div class="checkbox">
                                    <div class="form-group">
                                        <input type="checkbox" value="${addonProduct.code}" id="wifi_${productIndex.count}">
                                        <label for="wifi_${productIndex.count}">Add <span class="primary-color">+1 </span>Wi-Fi Extender</label>
                                        <input type="hidden" value="${productConfigDTO.onePAddon}" id="js-selectedAddon">
                                    </div>
                                </div>
                                <input type="hidden" name="selectedAddon" id="selectedAddon" value=""/>
							</c:if>
							<input type="hidden" name="selectedInternet" id="selectedInternet" value="${item.code}"/>

							<div class="ctabox-btn ctabox-wantbtn--js">
								<button type="submit" class="btn btn-primary"
								    <c:if test="${item.name ne null}"> data-attr-name="${item.name}" </c:if>
								    <c:if test="${item.code ne null}"> data-attr-id="${item.code}" </c:if>
                                    <c:if test="${itemPrice ne null}"> data-attr-price="${itemPrice}" </c:if>
                                    <c:if test="${item.supercategories[0].name ne null}"> data-attr-category="${item.supercategories[0].name}" </c:if>
                                    <c:if test="${item.supercategories[0].supercategories[0].name ne null}"> data-attr-primarycategory="${item.supercategories[0].supercategories[0].name}" </c:if>
								    <c:if test="${currentCurrency.isocode ne null}"> data-attr-currency="${currentCurrency.isocode}" </c:if>
								>
									<spring:theme code="configurator.Iwantit.button.text" />
								</button>
							</div>
						</form:form>
						<!--Delete button in below form-->
						<form:form action="${deleteItem}" method="post" class="step1ConfigForm ctabox-dlt-form dsp-none">

							<input type="hidden" name="deleteProduct" id="deleteProduct" value="${item.code}"/>
							<input type="hidden" name="stepId" id="stepId" value="1"/>
							<div class="ctabox-btn ctabox-dltbtn--js">
								<button type="submit" class="btn btn-primary text-uppercase">
									<spring:theme code="text.iconCartRemove" />
								</button>
							</div>
						</form:form>
					</div>
				</div>
			</c:forEach>
		</div>
		<div class="regulatory-info">
            <p><spring:theme code="basket.page.cart.terms.text" text="regulatoryInfo" /></p>
        </div>
		<div class="sticky-footer clearfix">
			<div class="view-content">
				<p class="view-less"><spring:theme code="text.sticky.footer.view.less" /></p>
				<p class="view-more hide"><spring:theme code="text.sticky.footer.view.more" /></p>
			</div>
			<div class="category-content">
				<div>
					<c:forEach items="${planList}" var="selectedPlan" varStatus="productIndex">
						<c:if test="${selectedPlan eq 'internetSelected'}">
							<p class="active"><spring:theme code="text.sticky.footer.select.internet" /></p>
						</c:if>
						<c:if test="${selectedPlan eq 'tvSelected'}">
							<p><spring:theme code="text.sticky.footer.select.television" /></p>
						</c:if>
						<c:if test="${selectedPlan eq 'phoneSelected' && productIndex.count != 3}">
							<p><spring:theme code="text.sticky.footer.select.telephone" /></p>
						</c:if>
					</c:forEach>
				</div>
				<div>
					<p class="price">
						<span class="price-text">${currentCurrency.symbol}0.00</span><spring:theme code="text.price.puertorico.month" />
					</p>
				</div>
			</div>
    	</div>
		<div class="bundleinfo visible-xs">
			<p class="data"><spring:theme code="configurator.bundle.info" /></p>
		</div>
		<%-- Inernet Plan Price  "${selected_plan_price }"  --%>
	</div>
</div>
