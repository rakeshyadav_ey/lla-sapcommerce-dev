<%@ attribute name="tabIndex" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ attribute name="urbanResidenceTypes" required="true" type="java.util.List"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<spring:message code="checkout.step.addressform.palceholder.urbanization" var="urbanization"/>
<spring:message code="checkout.step.addressform.palceholder.block" var="block"/>
<spring:message code="checkout.step.addressform.palceholder.house" var="house"/>
<spring:message code="checkout.step.addressform.palceholder.street" var="street"/>
<spring:message code="checkout.step.addressform.palceholder.urbanApartment" var="urbanApartment"/>
<spring:message code="checkout.step.addressform.palceholder.city" var="city"/>
<spring:message code="checkout.step.addressform.palceholder.pincode" var="pincode"/>

<formElement:formInputBox placeholder="${urbanization}" tabindex="1" idKey="urbanAddressStreetname" labelKey="residence.urban.address.streetname" path="streetname"  inputCSS="form-control" mandatory="false" />
<formElement:formInputBox placeholder="${block}" tabindex="2" idKey="urbanAddressBlock" labelKey="residence.address.block" path="block" inputCSS="form-control" mandatory="false" />
<formElement:formInputBox placeholder="${house}" tabindex="3" idKey="urbanAddressBuilding" labelKey="residence.address.building" path="building" inputCSS="form-control" mandatory="true" />
<formElement:formInputBox placeholder="${street}" tabindex="4" idKey="urbanAddressLine1" labelKey="residence.address.line1" path="line1" inputCSS="form-control" mandatory="false" />

<formElement:formSelectBoxDefaultEnabled skipBlank="true" skipBlankMessageKey="form.select.none" tabindex="5" idKey="residenceAddressLine2"  labelKey="residence.address.line2"   path="line2" mandatory="true"
                                         items="${urbanResidenceTypes}"  selectedValue="${addressForm.line2}" selectCSSClass="form-control"/>

<formElement:formInputBox placeholder="${urbanApartment}" tabindex="6" idKey="urbanAddressAppartment" labelKey="residence.address.appartment" path="appartment" inputCSS="form-control" mandatory="false"/>
<formElement:formInputBox placeholder="${city}" tabindex="7" idKey="urbanAddressTownCity" labelKey="residence.address.townCity" path="townCity" inputCSS="form-control" mandatory="true" />
<formElement:formInputBox placeholder="${pincode}" tabindex="8" idKey="urbanAddressPostcode" labelKey="residence.address.postcode" path="postcode" inputCSS="form-control" mandatory="true" />