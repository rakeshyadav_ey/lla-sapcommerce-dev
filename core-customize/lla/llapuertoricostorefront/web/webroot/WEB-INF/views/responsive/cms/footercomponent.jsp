<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/responsive/common/footer"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

    <div class="footer__top">
        <div class="container footer-container">
            <div class="footer__left col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <div class="site-logo">
                            <cms:pageSlot position="FooterSiteLogo" var="logo" limit="1">
                                <cms:component component="${logo}" element="div" class="yComponentWrapper"/>
                            </cms:pageSlot>
                        </div>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 footer__nav">
                    <c:forEach items="${navigationNodes}" var="node">
                        <c:if test="${node.visible}">
                            <c:forEach items="${node.links}" step="${component.wrapAfter}"
                                       varStatus="i">

                                <div class="footer__nav--container">
                                    <c:if test="${component.wrapAfter > i.index}">
                                       <c:choose>
                                            <c:when test="${node.uid eq 'Footer2OfflinestorePuertoricoNavNode'}">
                                            <div class="title"><a href="#">${fn:escapeXml(node.title)}</a></div>
                                            </c:when>
                                            <c:when test="${node.uid eq 'Footer2HomePuertoricoNavNode'}">
                                            <div class="title"><a href="#">${fn:escapeXml(node.title)}</a></div>
                                            </c:when>
                                            <c:otherwise>
                                              <div class="title">${fn:escapeXml(node.title)}</div>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                    <ul class="footer__nav--links">
                                        <c:forEach items="${node.links}" var="childlink"
                                                   begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
                                            <cms:component component="${childlink}"
                                                           evaluateRestriction="true" element="li" class="footer__link"/>
                                        </c:forEach>
                                    </ul>
                                </div>
                            </c:forEach>
                        </c:if>
                    </c:forEach>
                    </div>
                </div>
            </div>
            <%-- <div class="footer__right col-xs-12 col-md-3">
                <c:if test="${showLanguageCurrency}">
                    <div class="row">
                        <div class="col-xs-6 col-md-6 footer__dropdown">
                            <footer:languageSelector languages="${languages}"
                                                     currentLanguage="${currentLanguage}" />
                        </div>
                        <div class="col-xs-6 col-md-6 footer__dropdown">
                            <footer:currencySelector currencies="${currencies}"
                                                     currentCurrency="${currentCurrency}" />
                        </div>
                    </div>
                </c:if>
            </div> --%>
        </div>
    </div>


<div class="footer__bottom clearfix">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                <div class="footer__copyright">
<%--                 ${fn:escapeXml(notice)} --%>
					 ${(notice)}
                </div>
            </div>
            <div class="service-info">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
                    <div class="details">
                        <div class="icons">
                            <span>
                                <svg width="21" height="14" viewBox="0 0 21 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M11 9C9.59375 9 8.5 10.125 8.5 11.5C8.5 12.9062 9.59375 14 11 14C12.375 14 13.5 12.9062 13.5 11.5C13.5 10.125 12.375 9 11 9ZM11 13C10.1562 13 9.5 12.3438 9.5 11.5C9.5 10.6875 10.1562 10 11 10C11.8125 10 12.5 10.6875 12.5 11.5C12.5 12.3438 11.8125 13 11 13ZM20.875 3.9375C15.3438 -1.28125 6.625 -1.3125 1.09375 3.9375C0.9375 4.09375 0.9375 4.34375 1.09375 4.5L1.28125 4.65625C1.40625 4.8125 1.65625 4.8125 1.8125 4.6875C6.96875 -0.21875 15.0312 -0.1875 20.1562 4.6875C20.3125 4.8125 20.5625 4.8125 20.6875 4.65625L20.875 4.5C21.0312 4.34375 21.0312 4.09375 20.875 3.9375ZM17.4375 7.46875C13.75 4.1875 8.21875 4.1875 4.53125 7.46875C4.375 7.59375 4.375 7.84375 4.53125 8L4.6875 8.1875C4.84375 8.3125 5.0625 8.34375 5.21875 8.1875C8.5 5.28125 13.4688 5.28125 16.75 8.1875C16.9062 8.34375 17.125 8.3125 17.2812 8.1875L17.4375 8C17.5938 7.84375 17.5938 7.59375 17.4375 7.46875Z" fill="white"/>
                                </svg>
                            </span>
                            <span>
                                <svg width="21" height="16" viewBox="0 0 21 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M18.58 0H1.58C0.73625 0 0.0799999 0.6875 0.0799999 1.5V11.5C0.0799999 12.3438 0.73625 13 1.58 13H9.58V15H4.58C4.29875 15 4.08 15.25 4.08 15.5C4.08 15.7812 4.29875 16 4.58 16H15.58C15.83 16 16.08 15.7812 16.08 15.5C16.08 15.25 15.83 15 15.58 15H10.58V13H18.58C19.3925 13 20.08 12.3438 20.08 11.5V1.5C20.08 0.6875 19.3925 0 18.58 0ZM19.08 11.5C19.08 11.7812 18.83 12 18.58 12H1.58C1.29875 12 1.08 11.7812 1.08 11.5V1.5C1.08 1.25 1.29875 1 1.58 1H18.58C18.83 1 19.08 1.25 19.08 1.5V11.5Z" fill="white"/>
                                </svg>                            
                            </span>
                            <span>
                                <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M16.2188 0.78125L13.0938 0.03125C12.625 -0.0625 12.1562 0.15625 11.9688 0.59375L10.5 4C10.3438 4.40625 10.4375 4.875 10.7812 5.125L12.4688 6.5C11.4062 8.6875 9.65625 10.4375 7.46875 11.5L6.09375 9.8125C5.84375 9.46875 5.375 9.375 4.96875 9.53125L1.5625 11C1.15625 11.1875 0.90625 11.6562 1 12.0938L1.75 15.25C1.84375 15.6875 2.21875 16 2.6875 16C10.5625 16 17 9.625 17 1.71875C17 1.25 16.6875 0.875 16.2188 0.78125ZM2.71875 15L2 11.9062L5.34375 10.4688L7.21875 12.75C10.4375 11.2188 12.1875 9.46875 13.7188 6.25L11.4375 4.375L12.875 1.03125L15.9688 1.75C15.9688 9.0625 10.0312 15 2.71875 15Z" fill="white"/>
                                </svg>                            
                            </span>
                        </div>
                        <spring:theme code="footer.text.customer.service" htmlEscape="false" />
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
                    <div class="details">
                        <div class="icons clearfix">
                            <span>
                                <svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 12.221V2.19476C1.02114 1.87643 1.16157 1.57742 1.39373 1.35639C1.62589 1.13535 1.93303 1.00824 2.2549 1H8.1705C8.49353 1.00855 8.80191 1.1354 9.03606 1.35606C9.27021 1.57672 9.41352 1.87552 9.43831 2.19476V15.8027C9.41357 16.1222 9.27035 16.4213 9.03626 16.6424C8.80216 16.8634 8.49376 16.9908 8.1705 17H2.2549C1.93262 16.9917 1.62513 16.8642 1.3929 16.6427C1.16068 16.4211 1.02052 16.1215 1 15.8027V12.221Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M1.07471 14.2065H9.35293" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </span>
                        </div>
                        <spring:theme code="footer.text.service.sales" htmlEscape="false" /> 
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
                    <div class="details">
                        <div class="icons clearfix">
                            <span>
                                <svg width="21" height="14" viewBox="0 0 21 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                     <path d="M11 9C9.59375 9 8.5 10.125 8.5 11.5C8.5 12.9062 9.59375 14 11 14C12.375 14 13.5 12.9062 13.5 11.5C13.5 10.125 12.375 9 11 9ZM11 13C10.1562 13 9.5 12.3438 9.5 11.5C9.5 10.6875 10.1562 10 11 10C11.8125 10 12.5 10.6875 12.5 11.5C12.5 12.3438 11.8125 13 11 13ZM20.875 3.9375C15.3438 -1.28125 6.625 -1.3125 1.09375 3.9375C0.9375 4.09375 0.9375 4.34375 1.09375 4.5L1.28125 4.65625C1.40625 4.8125 1.65625 4.8125 1.8125 4.6875C6.96875 -0.21875 15.0312 -0.1875 20.1562 4.6875C20.3125 4.8125 20.5625 4.8125 20.6875 4.65625L20.875 4.5C21.0312 4.34375 21.0312 4.09375 20.875 3.9375ZM17.4375 7.46875C13.75 4.1875 8.21875 4.1875 4.53125 7.46875C4.375 7.59375 4.375 7.84375 4.53125 8L4.6875 8.1875C4.84375 8.3125 5.0625 8.34375 5.21875 8.1875C8.5 5.28125 13.4688 5.28125 16.75 8.1875C16.9062 8.34375 17.125 8.3125 17.2812 8.1875L17.4375 8C17.5938 7.84375 17.5938 7.59375 17.4375 7.46875Z" fill="white"/>
                                </svg>
                            </span>
                            <span>
                                <svg width="21" height="16" viewBox="0 0 21 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                     <path d="M18.58 0H1.58C0.73625 0 0.0799999 0.6875 0.0799999 1.5V11.5C0.0799999 12.3438 0.73625 13 1.58 13H9.58V15H4.58C4.29875 15 4.08 15.25 4.08 15.5C4.08 15.7812 4.29875 16 4.58 16H15.58C15.83 16 16.08 15.7812 16.08 15.5C16.08 15.25 15.83 15 15.58 15H10.58V13H18.58C19.3925 13 20.08 12.3438 20.08 11.5V1.5C20.08 0.6875 19.3925 0 18.58 0ZM19.08 11.5C19.08 11.7812 18.83 12 18.58 12H1.58C1.29875 12 1.08 11.7812 1.08 11.5V1.5C1.08 1.25 1.29875 1 1.58 1H18.58C18.83 1 19.08 1.25 19.08 1.5V11.5Z" fill="white"/>
                                </svg>                           
                            </span>
                            <span>
                                <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                     <path d="M16.2188 0.78125L13.0938 0.03125C12.625 -0.0625 12.1562 0.15625 11.9688 0.59375L10.5 4C10.3438 4.40625 10.4375 4.875 10.7812 5.125L12.4688 6.5C11.4062 8.6875 9.65625 10.4375 7.46875 11.5L6.09375 9.8125C5.84375 9.46875 5.375 9.375 4.96875 9.53125L1.5625 11C1.15625 11.1875 0.90625 11.6562 1 12.0938L1.75 15.25C1.84375 15.6875 2.21875 16 2.6875 16C10.5625 16 17 9.625 17 1.71875C17 1.25 16.6875 0.875 16.2188 0.78125ZM2.71875 15L2 11.9062L5.34375 10.4688L7.21875 12.75C10.4375 11.2188 12.1875 9.46875 13.7188 6.25L11.4375 4.375L12.875 1.03125L15.9688 1.75C15.9688 9.0625 10.0312 15 2.71875 15Z" fill="white"/>
                                </svg>                            
                            </span>
                        </div>
                        <spring:theme code="footer.text.sales" htmlEscape="false" /> 
                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-xs-12">
                <div class="social-links">
                    <spring:theme code="footer.text.follow.us" htmlEscape="false" /> 
                    <div class="links">
                        <a href="https://www.facebook.com/LibertyPR/" target="_blank">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                               <path d="M24 12.073C24 5.40365 18.629 0 12 0C5.37097 0 0 5.40365 0 12.073C0 18.1095 4.35484 23.1237 10.1129 24V15.5781H7.06452V12.073H10.1129V9.44422C10.1129 6.42596 11.9032 4.72211 14.6129 4.72211C15.9677 4.72211 17.3226 4.96552 17.3226 4.96552V7.93509H15.8226C14.3226 7.93509 13.8387 8.86004 13.8387 9.83367V12.073H17.1774L16.6452 15.5781H13.8387V24C19.5968 23.1237 24 18.1095 24 12.073Z" fill="white"/>
                            </svg>                                                                                                   
                        </a>
                        <a href="https://twitter.com/LibertyPR" target="_blank">
                           <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                               <path d="M21.5577 7.83976C22.4971 7.13494 23.3425 6.28916 24 5.30241C23.1546 5.67831 22.1683 5.96024 21.182 6.05422C22.2153 5.44337 22.9667 4.50361 23.3425 3.32892C22.4031 3.89277 21.3229 4.31566 20.2427 4.5506C19.3033 3.56386 18.0352 3 16.6262 3C13.9022 3 11.6947 5.20843 11.6947 7.93374C11.6947 8.30964 11.7417 8.68554 11.8356 9.06145C7.74951 8.82651 4.08611 6.85301 1.64384 3.89277C1.22114 4.59759 0.986301 5.44337 0.986301 6.38313C0.986301 8.0747 1.8317 9.57831 3.19374 10.4711C2.3953 10.4241 1.59687 10.2361 0.939335 9.86024V9.90723C0.939335 12.3036 2.63014 14.2771 4.88454 14.747C4.50881 14.841 4.03914 14.9349 3.61644 14.9349C3.28767 14.9349 3.00587 14.888 2.6771 14.841C3.28767 16.8145 5.11937 18.2241 7.27984 18.2711C5.58904 19.5867 3.47554 20.3855 1.17417 20.3855C0.751468 20.3855 0.375734 20.3386 0 20.2916C2.16047 21.7012 4.74364 22.5 7.56164 22.5C16.6262 22.5 21.5577 15.0289 21.5577 8.49759C21.5577 8.26265 21.5577 8.0747 21.5577 7.83976Z" fill="white"/>
                           </svg>                                                                                                   
                        </a>
                        <a href="https://www.youtube.com/user/LibertyIncreible/feed" target="_blank">
                           <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                               <path d="M23.4735 5.62109C23.2102 4.58984 22.3766 3.77344 21.3675 3.51562C19.4808 3 12.0219 3 12.0219 3C12.0219 3 4.5192 3 2.63254 3.51562C1.6234 3.77344 0.789762 4.58984 0.526508 5.62109C0 7.42578 0 11.293 0 11.293C0 11.293 0 15.1172 0.526508 16.9648C0.789762 17.9961 1.6234 18.7695 2.63254 19.0273C4.5192 19.5 12.0219 19.5 12.0219 19.5C12.0219 19.5 19.4808 19.5 21.3675 19.0273C22.3766 18.7695 23.2102 17.9961 23.4735 16.9648C24 15.1172 24 11.293 24 11.293C24 11.293 24 7.42578 23.4735 5.62109ZM9.5649 14.7734V7.8125L15.7952 11.293L9.5649 14.7734Z" fill="white"/>
                           </svg>
                           
                        </a>
                        <a href="https://www.instagram.com/libertypuertorico/" target="_blank">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                               <path d="M17.762 6.76202H18.8096M5.7144 1.52393H18.2858C20.6002 1.52393 22.4763 3.40007 22.4763 5.7144V18.2858C22.4763 20.6002 20.6002 22.4763 18.2858 22.4763H5.7144C3.40007 22.4763 1.52393 20.6002 1.52393 18.2858V5.7144C1.52393 3.40007 3.40007 1.52393 5.7144 1.52393ZM12.0001 16.1906C9.68578 16.1906 7.80964 14.3145 7.80964 12.0001C7.80964 9.68578 9.68578 7.80964 12.0001 7.80964C14.3145 7.80964 16.1906 9.68578 16.1906 12.0001C16.1906 14.3145 14.3145 16.1906 12.0001 16.1906Z" stroke="white" stroke-width="1.57143"/>
                            </svg>                         
                        </a>
                        <a href="https://www.linkedin.com/company/liberty-cablevision-of-puerto-rico" target="_blank">
                           <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0)"><path d="M22.0513 0.515625H1.42634C0.515625 0.515625 -0.234375 1.3192 -0.234375 2.28348V22.8013C-0.234375 23.7656 0.515625 24.5156 1.42634 24.5156H22.0513C22.9621 24.5156 23.7656 23.7656 23.7656 22.8013V2.28348C23.7656 1.3192 22.9621 0.515625 22.0513 0.515625ZM6.99777 21.0871H3.46205V9.67634H6.99777V21.0871ZM5.22991 8.0692C4.05134 8.0692 3.14063 7.15848 3.14063 6.03348C3.14063 4.90848 4.05134 3.9442 5.22991 3.9442C6.35491 3.9442 7.26563 4.90848 7.26563 6.03348C7.26563 7.15848 6.35491 8.0692 5.22991 8.0692ZM20.3371 21.0871H16.7478V15.5156C16.7478 14.2299 16.7478 12.5156 14.9263 12.5156C13.0513 12.5156 12.7835 13.9621 12.7835 15.4621V21.0871H9.24777V9.67634H12.6228V11.2299H12.6763C13.1585 10.3192 14.3371 9.35491 16.0513 9.35491C19.6406 9.35491 20.3371 11.7656 20.3371 14.8192V21.0871Z" fill="white"/></g><defs><clipPath id="clip0"><rect width="24" height="24" fill="white"/></clipPath></defs>
                           </svg>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 visible-sm visible-xs">
                <div class="footer__copyright">
					 ${(notice)}
                </div>
            </div>
        </div>
    </div>
</div>
