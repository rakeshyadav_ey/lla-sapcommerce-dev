<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<template:page pageTitle="${pageTitle}">

<c:url value="${currentstep}" var="currentCheckoutStepUrl" />
	<cms:pageSlot position="Section1" var="feature">
		<cms:component component="${feature}" element="div" class=""/>
	</cms:pageSlot>
	<div class="container__full">
		<div class="row">
			<cms:pageSlot position="Section2A" var="feature" element="div" class="col-md-3">
				<cms:component component="${feature}"/>
			</cms:pageSlot>

			<cms:pageSlot position="Section2B" var="feature" element="div" class="col-md-9">
				<cms:component component="${feature}"/>
			</cms:pageSlot>
		</div>
	</div>
	<cms:pageSlot position="Section3" var="feature" element="div" >
		<cms:component component="${feature}"/>
	</cms:pageSlot>
	<div class="container">
    		<div class="col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 col-xs-12 callBack-content">
    			<div class="row">
    				<cms:pageSlot position="CallBack" var="feature" element="div" >
    					<cms:component component="${feature}"/>
    				</cms:pageSlot>
    			<c:forEach items="${cartData.rootGroups}" var="entryGroup" varStatus="count" >
    			<input type="hidden" class="gtm_callback_cartProductsdata"
                            <c:if test="${entryGroup.orderEntries[0].product.name ne null}"> data-attr-name="${entryGroup.orderEntries[0].product.name}" </c:if>
                            <c:if test="${entryGroup.orderEntries[0].product.code ne null}"> data-attr-id="${entryGroup.orderEntries[0].product.code}" </c:if>
                            <c:if test="${entryGroup.orderEntries[0].quantity ne null}"> data-attr-quantity="${entryGroup.orderEntries[0].quantity}" </c:if>
                            <c:if test="${entryGroup.orderEntries[0].orderEntryPrices[0].basePrice.value  ne null}"> data-attr-price="${entryGroup.orderEntries[0].orderEntryPrices[0].basePrice.value}" </c:if>
                            <c:if test="${entryGroup.orderEntries[0].product.categories[0].name ne null}"> data-attr-category="${entryGroup.orderEntries[0].product.categories[0].name}" </c:if>
                            <c:if test="${entryGroup.orderEntries[0].product.categories[0].parentCategoryName ne null}"> data-attr-primarycategory="${entryGroup.orderEntries[0].product.categories[0].parentCategoryName}" </c:if>
                        />
				</c:forEach>    			
				    <input type="hidden" value="${cartData.falloutReason}" name="gtm_callback_falloutReason" class="gtm_callback_fail"/>
				    <input type="hidden" value="${cartData.debtCheckFlag}" name="gtm_debtCheckFlag"/>				    
    				<input type="hidden" value="${address.line1}" name="gtm_callback_address_line1"/>
    				<input type="hidden" value="${address.province}" name="gtm_callback_address_province"/>
    				<input type="hidden" value="${address.district}" name="gtm_callback_address_district"/>
    				<input type="hidden" value="${address.town}" name="gtm_callback_address_town"/>
    				<input type="hidden" value="${address.neighbourhood}" name="gtm_callback_address_neighbourhood"/>
    				<input type="hidden" value="${address.streetname}" name="gtm_callback_address_streetname"/>
    				<input type="hidden" value="${address.country.name}" name="gtm_callback_address_country"/>
    				<input type="hidden" value="${cartData.code}" name="gtm_cartCode"/>
    				<input type="hidden" value="${cartData.totalPriceWithTax.value}" name="gtm_cartTotalPriceWithTax"/>
    				<input type="hidden" value="${cartData.totalTax.value}" name="gtm_cartTotalTax"/>
    				 <input type="hidden" class="Gtm_creditScore" 
	         <c:if test="${cartData.creditScoreLevel.minScoreRange ne null}"> data-attr-minScoreRange="${cartData.creditScoreLevel.minScoreRange}" </c:if>
	         <c:if test="${cartData.creditScoreLevel.maxScoreRange ne null}"> data-attr-maxScoreRange="${cartData.creditScoreLevel.maxScoreRange}" </c:if>
	         <c:if test="${cartData.creditScoreLevel.letterMapping ne null}"> data-attr-letterMapping="${cartData.creditScoreLevel.letterMapping}" </c:if>	      
	         <c:if test="${cartData.code ne null}"> data-attr-cartCode="${cartData.code}" </c:if>
	         <c:if test="${cartData.debtCheckFlag ne null}"> data-attr-debtCheckFlag="${cartData.debtCheckFlag}" </c:if>
	                                                data-attr-acceptsCreditCheck="" data-attr-isCustomer=""/>
                                <!--<div><a href="${currentstep}" class="btn btn-primary btn-home"><spring:theme code="text.api.callback.button"/></a></div> -->
    			</div>
    		</div>
    		<div class="text-center col-xs-12 errorPage-content">
    			<div class="row">
    				<cms:pageSlot position="Error" var="feature" element="div" >
    					<cms:component component="${feature}"/>
    				</cms:pageSlot>
    					
    		        <div><a href="${currentCheckoutStepUrl}" class="btn btn-primary btn-home"><spring:theme code="text.api.error.button"/></a></div>
    				
    			</div>
    		</div>
    	</div>
</template:page>
<script type="text/javascript">
window.onload = function() {
    $("#cartIdHolder").text(${cartData.code});
}
</script>