<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/llatelcoaddon/responsive/cart"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<!-- <div class="well well-tertiary well-lg"> -->
<div class="order-confirmation-details">
	 <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	   <div class="row">
	     <div class=" col-sm-12">
		    <ycommerce:testId code="orderDetail_overview_section">
		        <order:accountOrderDetailsOverview order="${orderData}"/>
		    </ycommerce:testId>
		    </div>
		    <div class="row">
		    <div class=" col-sm-12">
	             <div class="additional-info-box visible-lg visible-md">
		             <p><spring:theme code="text.account.order.info.text1" text="orderInfo" htmlEscape="false"/></p>
		             <p><spring:theme code="text.account.order.info.text2" text="orderInfo" htmlEscape="false"/></p>
		             <p><spring:theme code="text.account.order.info.text3" text="orderInfo" htmlEscape="false"/> <span>&nbsp;&#x23;${fn:escapeXml(orderData.code)}</span></p>
	            	 <p><spring:theme code="text.account.order.info.text4" text="orderInfo" htmlEscape="false"/>
	            </div>
            </div>
          </div>
	    </div>
	    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	    	<div class="order-totals-overview">
	    		<cart:cartTotals cartData="${cartData}" />
	    	</div>
	    	<div class="additional-info-box visible-sm visible-xs">
				<p><spring:theme code="text.account.order.info.text1" text="orderInfo" htmlEscape="false"/></p>
				<p><spring:theme code="text.account.order.info.text2" text="orderInfo" htmlEscape="false"/></p>
				<p><spring:theme code="text.account.order.info.text3" text="orderInfo" htmlEscape="false"/> <span>&nbsp;&#x23;${fn:escapeXml(orderData.code)}</span></p>
			   	<p><spring:theme code="text.account.order.info.text4" text="orderInfo" htmlEscape="false"/>
		   </div>
	    </div>
    </div>
</div>
<!-- </div> -->