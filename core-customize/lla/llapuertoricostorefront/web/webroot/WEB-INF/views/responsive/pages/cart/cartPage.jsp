<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<template:page pageTitle="${pageTitle}">

<div class="container">
	<cart:cartValidation/>
	<cart:cartPickupValidation/>

	<%--<div class="cart-top-bar">
        <div class="text-right">
            <spring:theme var="textHelpHtml" code="text.help" />
            <a href="" class="help js-cart-help" data-help="${fn:escapeXml(textHelpHtml)}">${textHelpHtml}
                <span class="glyphicon glyphicon-info-sign"></span>
            </a>
            <div class="help-popup-content-holder js-help-popup-content">
                <div class="help-popup-content">
                    <strong>${fn:escapeXml(cartData.code)}</strong>
                    <spring:theme var="cartHelpContentVar" code="basket.page.cartHelpContent" htmlEscape="false" />
                    <c:set var="cartHelpContentVarSanitized" value="${ycommerce:sanitizeHTML(cartHelpContentVar)}" />
                    <div>${cartHelpContentVarSanitized}</div>
                </div>
            </div>
		</div>
	</div>--%>
	

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-6">		
            <cms:pageSlot position="TopContent" var="feature">                              
                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>                
            </cms:pageSlot>
   		         
		     <cms:pageSlot position="BottomContentSlot" var="feature">
		         <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
		     </cms:pageSlot>		            	        
		</div>

	   <c:if test="${not empty cartData.rootGroups}">
           <cms:pageSlot position="CenterLeftContentSlot" var="feature">
                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
           </cms:pageSlot>
        </c:if>
		
		<div class="col-xs-12 col-sm-12 col-md-6">
			 <c:if test="${not empty cartData.rootGroups}">
			    <c:if test="${channelAddonProducts.size() > 0 || smartProduct.code ne null || tvaddonProducts.size() > 0}">
			 	<div class="cart-right">
				 	<div class="cart-channels">
				 		<cart:cartChannels/>
				 	</div>
				 </div>
				 </c:if>
			 	<div class="cart-right">
		            <cms:pageSlot position="CenterRightContentSlot" var="feature">
		                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
		            </cms:pageSlot>			           	            	        
	            </div>	
	           
	         <div class="cart-rightCustom">		 
	           <form id="guestCartCheckout" action="" method="post">
		           <div class="checkout-button">
			           <p class="guestHeadline"><spring:theme code="add.information.to.continue" text="Ingresa los siguientes datos para continuar" /></p>
			           <div class="form-group clearfix formGuestLogin" tabindex="1">
				            <label class="control-label" for="name"><spring:theme code="guest.name"/></label>
				            <input type="name" class="form-control" id="guestName" value="${user.name}" name="guestName" />
				       </div>
			           <div class="form-group clearfix formGuestLogin" tabindex="2">
			                <label class="control-label" for="guestCheckoutEmail"><spring:theme code="guest.email"/></label>
			                <input type="email" class="form-control" id="guestEmail" value="${user.additionalEmail}" name="guestEmail" />
		              </div>  
		               <div class="form-group clearfix formGuestLogin" tabindex="3">
			                <label class="control-label" for="guestMobile"><spring:theme code="customer.lcpr.telephone"/></label>
			                <input type="mobilePhone" class="form-control" id="mobilePhone" value="${user.mobilePhone}" name="mobilePhone" />
		               </div>
		               <div class="form-group clearfix formGuestLogin check">
                            <input type="checkbox" class="customerCheckSelect" id="existingCustomer" name="existingCustomer" tabindex="4"/>
                            <label for="existingCustomer"><span><spring:theme code="text.puertorico.existing.customer.check"/></span></label>
                       </div>
		                <div class="guestLoginInfoBox"><p class="guestLoginInfoText"><spring:theme code="text.cart.login.info"/></p></div>
		                <div class="form-group clearfix text-center" tabindex="5">
		                	<button type="button" class="guestCartCheckoutButton btn btn-primary" ><spring:theme code="checkout.checkout"/></button>
		                	<input type="hidden" id="guestName_error" value="<spring:theme code="guest.checkout.name.empty.msg"/>">
		                	<input type="hidden" id="guestNameLength_error" value="<spring:theme code="guest.checkout.name.length.invalid"/>">
			                <input type="hidden" id="guestEmail_error" value="<spring:theme code="guest.checkout.lcpr.email.invalid"/>">
						    <input type="hidden" id="guestEmailempty_error" value="<spring:theme code="guest.checkout.email.empty.msg"/>">
			                <input type="hidden" id="guestPhone_error" value="<spring:theme code="register.fixedPhone.lcpr.invalid"/>">
						    <input type="hidden" id="guestPhoneempty_error" value="<spring:theme code="guest.checkout.guestPhone.empty.msg"/>">
		               </div>
	              </div>
              </form>
            </div>
              
	             
	             <div class="contactus">
		               <h4><spring:theme code="contactus.Questions" htmlEscape="false"/></h4>
					   <p><spring:theme code="contactus.getincontact" htmlEscape="false"/></p>
		               <ul>
		               <li class="call"><spring:theme code="contactus.call" htmlEscape="false" /></li>
		               </ul>
		           </div>            
			</c:if>
		</div>
	</div>
	
	<c:if test="${empty cartData.rootGroups}">
           <cms:pageSlot position="EmptyCartMiddleContent" var="feature">
               <cms:component component="${feature}" element="div" class="yComponentWrapper content__empty"/>
           </cms:pageSlot>
	</c:if>
	
</div>




</template:page>
