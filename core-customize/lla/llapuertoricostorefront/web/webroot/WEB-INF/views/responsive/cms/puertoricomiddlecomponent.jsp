<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="whatDoUneed-section">
	<div class="container">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 firstSection">
			<p class="subHeading"><spring:theme code="text.home.sectionwhatdouneed.text1" /></p>
            <h2 class="mainHeading"><spring:theme code="text.home.sectionwhatdouneed.text2" /></h2>
		</div>
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 secondSection">
		    <c:forEach items="${puertoricoMiddleLinksList}" var="items">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
	                <a href="${items.url}">
	                    <img class="img-responsive"  src="${items.media.url}" alt="">
	                    <h3 class="sectionHead">${items.text}</h3>
	                    <p class="sectionDesc">${items.description}</p>
	                </a>
                </div>
			</c:forEach>
		</div>
	</div>
</div>