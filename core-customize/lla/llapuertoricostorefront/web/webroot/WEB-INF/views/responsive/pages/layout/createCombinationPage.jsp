<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var = "comboMessage_en" value = "${configurator_msg_en}"/>
<c:set var = "comboMessage_es" value = "${configurator_msg_es}"/>

<template:page pageTitle="${pageTitle}">

<c:if test="${cmsSite.uid eq 'puertorico'}">   
    <c:forEach items="${cartData.entries}" var="entryGroup" varStatus="count" >
       <input type="hidden" class="cartProducts"
           <c:if test="${entryGroup.product.name ne null}"> data-attr-name="${entryGroup.product.name}" </c:if>
           <c:if test="${entryGroup.product.code ne null}"> data-attr-id="${entryGroup.product.code}" </c:if>
           <c:if test="${entryGroup.orderEntryPrices[0].basePrice.value ne null}"> data-attr-price="${entryGroup.orderEntryPrices[0].basePrice.value}" </c:if>
           <c:if test="${entryGroup.quantity ne null}"> data-attr-quantity="${entryGroup.quantity}" </c:if>
           <c:if test="${entryGroup.product.categories[0].name ne null}"> data-attr-category="${entryGroup.product.categories[0].name}" </c:if>
           <c:if test="${entryGroup.product.categories[0].parentCategoryName ne null}"> data-attr-primarycategory="${entryGroup.product.categories[0].parentCategoryName}" </c:if>
       />
    </c:forEach>
</c:if>
	<div class="recomended-plans container">
		<div class="tab-headline" id="plans-headline"><spring:theme code="text.recommended.plans.headline" /></div>
		<div class="tab-headline" id="combination-headline" style="display: none;"><spring:theme code="text.create.combination.headline" /></div>
		<div class="tabs-container">
			<div class="container">
				<ul>
					<li class="js-tab-head ${empty param.categoryCode ? 'active' : ''}" data-content-id="recommendedPlans">
						<span class="recmdplans-icon"></span>
						<div class="bundle-name"><spring:theme code="text.recommended.plans.tab" /></div>
					</li>
					<li class="js-tab-head ${empty param.categoryCode ? '' : 'active'}" data-content-id="createCombination">
						<span class="wifi-icon"></span>
						<span class="tv-icon"></span>
						<span class="landphone-icon"></span>
						<div class="bundle-name"><spring:theme code="text.create.combination.tab" /></div>
					</li>
				</ul>
			</div>
		</div>

		<div class="cta-wrapper">
			<div class="tab-content-wrapper ${empty param.categoryCode ? '' : 'display-none'}" id="recommendedPlans">
				<div class="cta-container">
					<cms:pageSlot position="Section2C" var="feature" element="div" >
						<cms:component component="${feature}" element="div" class="yComponentWrapper col-xs-12 col-sm-12"/>
					</cms:pageSlot>
				</div>
				<div class="container createComboBanner">
					<div class="plans-create">
						<div class="headline"><spring:theme code="text.plans.create.combination" /></div>
						<div class="createButton">
							<button type="button" class="btn plans-create-button" onclick="$('.js-tab-head[data-content-id=createCombination]').click();"><spring:theme code="text.plans.create.combination.button" /></button>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-content-wrapper ${empty param.categoryCode ? 'display-none' : ''}" id="createCombination">
				<div class="container">
					<div class="create-combination">
						<h3 class="form-headline"><spring:theme code="text.create.combination.form.headline" /></h3>
						<form:form method="post" modelAttribute="createCombinatioForm" action="configureProduct/saveCombination">
							<div class="form-checkbox">
								<div class="checkbox">
									<label class="custom-label">
										<spring:theme code="text.create.combination.category.internet" />
										<c:set var="internetCheckbox" value=""/>
										<c:if test="${param.categoryCode eq 'internet' || param.categoryCode eq 'all' || param.categoryCode eq null}"> <c:set var="internetCheckbox" value="true"/> </c:if>
										<form:checkbox id="internet" path="internet" checked="${internetCheckbox}" />
										<span class="checkmark"></span>
									</label>
									<label class="custom-label">
										<spring:theme code="text.create.combination.category.tv" />
										<c:set var="tvCheckbox" value=""/>
										<c:if test="${param.categoryCode eq 'tv' || param.categoryCode eq 'all' || param.categoryCode eq null}"> <c:set var="tvCheckbox" value="true"/> </c:if>
										<form:checkbox id="tv" path="tv" checked="${tvCheckbox}" />
										<span class="checkmark"></span>
									</label>
									<label class="custom-label">
										<div class="phone-selection-info hide">
											<spring:theme code="text.create.combination.category.telephone" />
											<c:set var="phoneCheckbox" value=""/>
											<c:if test="${param.categoryCode eq 'phone' || param.categoryCode eq 'all' || param.categoryCode eq null}"> <c:set var="phoneCheckbox" value="true"/> </c:if>
											<form:checkbox id="phone" path="phone" checked="${phoneCheckbox}" />
											<span class="checkmark"></span>
										</div>
										<div class="phone-text-info">
											<spring:theme code="text.create.combination.category.fixed.telephone" />
											<span class="checkInfo"></span>
										</div>
									</label>
								</div>
							</div>

							<div class="alert alert-info configuratorAlert hide">
								<%-- <spring:theme code="text.plans.info" htmlEscape="false" /> --%>
								<div class="clearfix hide" id="internet_TV">
									<cms:pageSlot position="Section1" var="feature" element="div">
										<cms:component component="${feature}" element="div"
											class="yComponentWrapper col-xs-12 col-sm-12" />
									</cms:pageSlot>
								</div>
								<div class="clearfix hide" id="TV_telephone">
									<cms:pageSlot position="Section2" var="feature" element="div">
										<cms:component component="${feature}" element="div"
											class="yComponentWrapper col-xs-12 col-sm-12" />
									</cms:pageSlot>
								</div>
								<div class="clearfix hide" id="internet_telephone">
									<cms:pageSlot position="Section3" var="feature" element="div">
										<cms:component component="${feature}" element="div"
											class="yComponentWrapper col-xs-12 col-sm-12" />
									</cms:pageSlot>
								</div>
								<div class="clearfix hide" id="onlyOne_selected">
									<cms:pageSlot position="Section4" var="feature" element="div">
										<cms:component component="${feature}" element="div"
											class="yComponentWrapper col-xs-12 col-sm-12" />
									</cms:pageSlot>
								</div>
								<div class="clearfix hide" id="nothing_selected">
									<cms:pageSlot position="Section5" var="feature" element="div">
										<cms:component component="${feature}" element="div"
											class="yComponentWrapper col-xs-12 col-sm-12" />
									</cms:pageSlot>
								</div>
								<div class="clearfix hide" id="internet_TV_telephone">
									<cms:pageSlot position="Section6" var="feature" element="div">
										<cms:component component="${feature}" element="div"
											class="yComponentWrapper col-xs-12 col-sm-12" />
									</cms:pageSlot>
								</div>
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-primary js-create-comb-button" disabled="">
									<spring:theme code="text.create.combination.continue.button" />
								</button>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>

</template:page>