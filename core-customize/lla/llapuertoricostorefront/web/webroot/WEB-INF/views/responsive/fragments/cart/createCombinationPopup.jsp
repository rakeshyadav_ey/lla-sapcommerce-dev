<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- Create two buttons one for to continue and other for close -->

<spring:url value="/configureProduct/createCombination" var="continue" htmlEscape="false"/>
<spring:url value="/configureProduct/removeCombination" var="close" htmlEscape="false"/>

<div> There is an existing product in cart, either click on cart or delete it and create/add new product/combination  </div>