<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<template:errorpage>
	
	<div class="error-content">
		<div class="error-message text-center">
			<h3>
				<spring:theme code="error.404.page.message" text="Sorry, we can not find the page you are looking for" />
			</h3>
			<h4 class="error-description">
				<spring:theme code="error.404.page.message1" text="You might be interested in below offerings. Please check for it" />
			</h4>
			<ul class="site-category">
				<%--<li>
					<a href="plp/prepaid">
						<svg xmlns="http://www.w3.org/2000/svg" width="21" height="37" viewBox="0 0 21 37" fill="none">
							<path d="M1.49707 21.2025V3.60884C1.54304 2.91675 1.84835 2.26664 2.35311 1.78607C2.85787 1.30551 3.52565 1.02915 4.22545 1.01123H17.087C17.7893 1.02981 18.4598 1.30562 18.9689 1.78537C19.478 2.26512 19.7895 2.91477 19.8434 3.60884V33.1949C19.7897 33.8895 19.4783 34.5398 18.9693 35.0205C18.4604 35.5011 17.7898 35.7781 17.087 35.7981H4.22545C3.52475 35.78 2.85621 35.5029 2.35131 35.0212C1.84642 34.5394 1.54169 33.888 1.49707 33.1949V25.4076" stroke="#FF4713" stroke-width="1.50616" stroke-linecap="round" stroke-linejoin="round"/>
							<path d="M1.65918 29.7246H19.6575" stroke="#FF4713" stroke-width="1.50616" stroke-linecap="round" stroke-linejoin="round"/>
						</svg>
						<span><spring:theme code="error.404.page.prepaid" text="prepaid" /></span>
					</a>
				</li>
				<li>
					<a href="plp/postpaid">
						<svg xmlns="http://www.w3.org/2000/svg" width="21" height="37" viewBox="0 0 21 37" fill="none">
							<path d="M1.49707 21.2025V3.60884C1.54304 2.91675 1.84835 2.26664 2.35311 1.78607C2.85787 1.30551 3.52565 1.02915 4.22545 1.01123H17.087C17.7893 1.02981 18.4598 1.30562 18.9689 1.78537C19.478 2.26512 19.7895 2.91477 19.8434 3.60884V33.1949C19.7897 33.8895 19.4783 34.5398 18.9693 35.0205C18.4604 35.5011 17.7898 35.7781 17.087 35.7981H4.22545C3.52475 35.78 2.85621 35.5029 2.35131 35.0212C1.84642 34.5394 1.54169 33.888 1.49707 33.1949V25.4076" stroke="#FF4713" stroke-width="1.50616" stroke-linecap="round" stroke-linejoin="round"/>
							<path d="M1.65918 29.7246H19.6575" stroke="#FF4713" stroke-width="1.50616" stroke-linecap="round" stroke-linejoin="round"/>
						</svg>
						<span><spring:theme code="error.404.page.postpaid" text="postpaid" /></span>
					</a>
				</li>
				<li>
					<a href="plp/internet_tv_phone">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 216 216" style="enable-background:new 0 0 216 216;" xml:space="preserve">
							<style type="text/css">
								.st0{fill:#636569;}
							</style>
							<g>
								<path class="st0" d="M80.7,101.1h60.7c2.7,0,5.1-2.2,5.1-4.7V35.6c0-2.8-2.3-5.1-5.1-5.1H80.7c-2.8,0-5.1,2.3-5.1,5.1V96   C75.6,98.8,77.9,101.1,80.7,101.1z M85.4,40.3h51v51h-51V40.3z"/>
								<path class="st0" d="M32.7,186.9h60.7c2.7,0,5.1-2.2,5.1-4.7v-60.7c0-2.8-2.3-5.1-5.1-5.1H32.7c-2.8,0-5.1,2.3-5.1,5.1v60.4   C27.7,184.7,29.9,186.9,32.7,186.9z M37.4,126.2h51v51h-51V126.2z"/>
								<path class="st0" d="M185.3,116.2h-4.8c-2.7,0-4.9,2.2-4.9,4.9v0.3c0,2.7,2.2,4.9,4.9,4.9h0.1v51h-51v-51h24.2   c2.7,0,4.9-2.2,4.9-4.9v-0.3c0-2.7-2.2-4.9-4.9-4.9h-29.3c-2.8,0-5.1,2.3-5.1,5.1V182c0,2.8,2.3,5.1,5.1,5.1h60.7   c2.8,0,5.1-2.3,5.1-5.1v-60.7C190.3,118.5,188.1,116.2,185.3,116.2z"/>
							</g>
						</svg>
						<span><spring:theme code="error.404.page.residential" text="Residential" /></span>
					</a>					
				</li>
				<li>
					<a href="Open-Catalogue/bundles/Bundles/Complete-Package-300/p/FMC_300">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 216 216" style="enable-background:new 0 0 216 216;" xml:space="preserve">
							<style type="text/css">
								.st0{fill:#636569;}
							</style>
							<g>
								<path class="st0" d="M80.7,101.1h60.7c2.7,0,5.1-2.2,5.1-4.7V35.6c0-2.8-2.3-5.1-5.1-5.1H80.7c-2.8,0-5.1,2.3-5.1,5.1V96   C75.6,98.8,77.9,101.1,80.7,101.1z M85.4,40.3h51v51h-51V40.3z"/>
								<path class="st0" d="M32.7,186.9h60.7c2.7,0,5.1-2.2,5.1-4.7v-60.7c0-2.8-2.3-5.1-5.1-5.1H32.7c-2.8,0-5.1,2.3-5.1,5.1v60.4   C27.7,184.7,29.9,186.9,32.7,186.9z M37.4,126.2h51v51h-51V126.2z"/>
								<path class="st0" d="M185.3,116.2h-4.8c-2.7,0-4.9,2.2-4.9,4.9v0.3c0,2.7,2.2,4.9,4.9,4.9h0.1v51h-51v-51h24.2   c2.7,0,4.9-2.2,4.9-4.9v-0.3c0-2.7-2.2-4.9-4.9-4.9h-29.3c-2.8,0-5.1,2.3-5.1,5.1V182c0,2.8,2.3,5.1,5.1,5.1h60.7   c2.8,0,5.1-2.3,5.1-5.1v-60.7C190.3,118.5,188.1,116.2,185.3,116.2z"/>
							</g>
						</svg>
						<span><spring:theme code="error.404.page.FMC.300" text="FMC 300" /></span>
					</a>
				</li>--%>
				<li>
					<a href="configureProduct/selectInternetPlan">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 216 216" style="enable-background:new 0 0 216 216;" xml:space="preserve">
							<style type="text/css">
								.st0{fill:#636569;}
							</style>
							<g>
								<path class="st0" d="M80.7,101.1h60.7c2.7,0,5.1-2.2,5.1-4.7V35.6c0-2.8-2.3-5.1-5.1-5.1H80.7c-2.8,0-5.1,2.3-5.1,5.1V96   C75.6,98.8,77.9,101.1,80.7,101.1z M85.4,40.3h51v51h-51V40.3z"/>
								<path class="st0" d="M32.7,186.9h60.7c2.7,0,5.1-2.2,5.1-4.7v-60.7c0-2.8-2.3-5.1-5.1-5.1H32.7c-2.8,0-5.1,2.3-5.1,5.1v60.4   C27.7,184.7,29.9,186.9,32.7,186.9z M37.4,126.2h51v51h-51V126.2z"/>
								<path class="st0" d="M185.3,116.2h-4.8c-2.7,0-4.9,2.2-4.9,4.9v0.3c0,2.7,2.2,4.9,4.9,4.9h0.1v51h-51v-51h24.2   c2.7,0,4.9-2.2,4.9-4.9v-0.3c0-2.7-2.2-4.9-4.9-4.9h-29.3c-2.8,0-5.1,2.3-5.1,5.1V182c0,2.8,2.3,5.1,5.1,5.1h60.7   c2.8,0,5.1-2.3,5.1-5.1v-60.7C190.3,118.5,188.1,116.2,185.3,116.2z"/>
							</g>
						</svg>
						<span><spring:theme code="product.configurator.create.breadcrumb" text="Create Your Combination" /></span>
					</a>
				</li>
			</ul>
		</div>
	</div>

</template:errorpage>