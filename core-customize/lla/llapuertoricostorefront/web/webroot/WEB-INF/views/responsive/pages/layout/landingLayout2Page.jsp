<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">
    	
	<div class="row">
		<cms:pageSlot position="Section2C" var="feature" element="div" >
		<cms:component component="${feature}" element="div" class="yComponentWrapper col-xs-12 col-sm-12"/>
		</cms:pageSlot>
	</div>
     
    <div class="home-banner">
		<div class="container">
			<cms:pageSlot position="Section1" var="feature">
			<cms:component component="${feature}" element="div" class="no-space yComponentWrapper home-crt-combination"/>
			</cms:pageSlot>
		</div>
	</div>
   <%--  <cms:pageSlot position="Section3" var="feature" element="div" class="row no-margin" >
        <cms:component component="${feature}" element="div" class="no-space yComponentWrapper"/>
    </cms:pageSlot> --%>

<!--   <div class="container experience-section row"> -->
	<div class="home-banner2">
		<cms:pageSlot position="Section5" var="feature" element="div">
			<cms:component component="${feature}" element="div" class="experience-col yComponentWrapper"/>
		</cms:pageSlot>
	</div>
<!--    </div> -->

</template:page>
