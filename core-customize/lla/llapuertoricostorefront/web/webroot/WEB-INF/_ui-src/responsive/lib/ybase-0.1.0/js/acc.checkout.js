ACC.checkout = {

	_autoload: [
		"bindCheckO",
		"bindForms",
		"bindSavedPayments",
		"bindContractMethodMessage",
		"bindCreditCheckValue",
		"bindCheckSsn",
	  	"bindPlaceOrderSingleClick",
	  	"bindShowResidenceTypes",
	  	"bindDeliveryAddress",
	  	"bindAddressForServiceability",
	  	"bindSelectedAddress",
	  	"bindEditForm",
	  	"bindNoAddressFound",
	  	"bindDebtCheckSsnForm",
		"bindCreditCheckAddress"
    ],

    bindPlaceOrderSingleClick:function(){
		$(document).ready(function (){
			if ($('.page-error').length > 0 && $('.btn-home').length > 0 && sessionStorage.errorBackURL && $('a.btn.btn-primary.btn-home').attr('href') === ''){
				$("a.btn.btn-primary.btn-home").prop('href', sessionStorage.errorBackURL)
				}
			$("#placeOrderForm1").submit(function() {
				// submit more than once return false
			    $(this).submit(function() {
					return false;
				});
				// submit once return true
				return true;
			});
		});
	},

    bindEditForm:function(){
        $(document).on("click",'#selectedAddressEdit',function(){
             window.location=ACC.config.encodedContextPath + '/checkout/multi/delivery-address/add'
        });
    },

    bindNoAddressFound:function()
    {
            $(document).on("click",'#noAddressFound',function(e){
             e.preventDefault();
               var url  = ACC.config.encodedContextPath + '/checkout/multi/delivery-address/no-address-found';
                         $.ajax({
                               type:"POST",
                               url:url,
                               success:function(data){
                            	   if(data=='/error'){
                                       sessionStorage.errorBackURL = window.location.href;
                                       }
                                  var avoid = "redirect:";
                                  var avoided = data.replace(avoid,'');
                                  if(avoided=='/error'){
                                      sessionStorage.errorBackURL = window.location.href;
                                      }
                                 var urlDomain=window.location.href;
                                  var lcprUrl= urlDomain.split("/puertorico/");
                                   urlDomain=lcprUrl[0];
                                   if(lcprUrl[1].toString().includes("es"))
                                   {
                                   window.location= urlDomain+"/puertorico/es"+avoided;
                                   }
                                   if(lcprUrl[1].toString().includes("en"))
                                  {
                                  window.location= urlDomain+"/puertorico/en"+avoided;
                                  }
                                }
                         });

            });

    },

	bindDeliveryAddress:function() {
    	$(document).on("click","#addressSubmit",function(e){
            e.preventDefault();

            //scroll to top
            ACC.checkout.bindScrollToTop();

            if($("#addressForm").valid()){
                $(".checkout-shipping").hide();
                $(".addressValidationLoader").show();
                var timeout=$("#c2cTimerForLcprApi").val();
                 var callBackTimeOut=$("#c2cTimerForCallBack").val();
                 var callBackType="FuzzySearch";
                var url  = ACC.config.encodedContextPath + '/checkout/multi/delivery-address/addAddress';
                $.ajax({
                    type: "POST",
                    url:url,
                    data: $("#addressForm").serialize(),
                     timeout: timeout,
                     async:true,
                    success: function (data) {
                     console.log("Moving to next step-clearing Interval for timeout function:- fuzzy search");
                        if(data=='/callBack')
                        {
                          var urlDomain=window.location.href;
                          var lcprUrl= urlDomain.split("/puertorico/");
                           urlDomain=lcprUrl[0];
                           if(lcprUrl[1].toString().includes("es"))
                           {
                           window.location= urlDomain+"/puertorico/es"+data;
                           }
                           if(lcprUrl[1].toString().includes("en"))
                          {
                          window.location= urlDomain+"/puertorico/en"+data;
                          }
                        }else if(data=='/error')
                        {
                           sessionStorage.errorBackURL = window.location.href;
                           var urlDomain=window.location.href;
                           var lcprUrl= urlDomain.split("/puertorico/");
                            urlDomain=lcprUrl[0];
                            if(lcprUrl[1].toString().includes("es"))
                            {
                            window.location= urlDomain+"/puertorico/es"+data;
                            }
                            if(lcprUrl[1].toString().includes("en"))
                           {
                           window.location= urlDomain+"/puertorico/en"+data;
                           }
                        }
			else if(data.toString().includes("redirect:")){
                            var avoid = "redirect:";
                            var profileForm = data.replace(avoid,'');
                            window.location=ACC.config.encodedContextPath + profileForm;
                        } else {
                            $(".addressValidationLoader").hide();
                            $("#callBackUrl").show();
                            var list=data.split("|");
                            $.each( list, function( key, value ) {
                                var addressdata = value.split(",");
                                addressdata.pop();
                                var printData = "<p class='addressData'>"+addressdata[0]+", "+addressdata[1]+"<br> <span class='subDetails'>"+addressdata[2]+" "+addressdata[3]+", "+addressdata[4]+"</span></p>";
                                var $input = $('<input>', {
                                    type : "radio",
                                    name : "address",
                                    class : "selectedAddressRadio",
                                    value : value
                                });
                                var checkmark = '<span class="checkDesign"></span>';
                                $('.addressList .listData').append(
                                    $("<label />", {
                                        insertAfter: "addressList",
                                        class : "selectedAddressLabel",
                                        append: [printData, $input, checkmark]
                                    })
                                );
                            });
                            $('.addressList').show();
                        }
                },
                error:function (xhr, textStatus, thrownError)
                {
                     var url  = ACC.config.encodedContextPath +'/click/callBackRequest';
                              e.preventDefault();
                              $.ajax({
                                    type:"POST",
                                    url:url,
				    async:"true",
                                    timeout: callBackTimeOut,
                                    data:{callBackType:callBackType,houseNo:''},
                                    async:true,
                                    success:function(data){
                                          console.log(" Moving to next step-clearing Interval for timeout function:- call back request");
                                          if(data=='/error'){
                                              sessionStorage.errorBackURL = window.location.href;
                                              }
                                       var avoid = "redirect:";
                                       var avoided = data.replace(avoid,'');
                                       if(avoided=='/error'){
                                           sessionStorage.errorBackURL = window.location.href;
                                           }
                                         var urlDomain=window.location.href;
                                         var lcprUrl= urlDomain.split("/puertorico/");
                                          urlDomain=lcprUrl[0];
                                          if(lcprUrl[1].toString().includes("es"))
                                          {
                                          window.location= urlDomain+"/puertorico/es"+avoided;
                                          }
                                          if(lcprUrl[1].toString().includes("en"))
                                         {
                                         window.location= urlDomain+"/puertorico/en"+avoided;
                                         }
                                     },
                                    error:function (xhr, textStatus, thrownError)
                                    {
                                       var urlDomain=window.location.href;
                                       var lcprUrl= urlDomain.split("/puertorico/");
                                        urlDomain=lcprUrl[0];
                                        if(lcprUrl[1].toString().includes("es"))
                                        {
                                        window.location= urlDomain+"/puertorico/es/callBack";
                                        }
                                        if(lcprUrl[1].toString().includes("en"))
                                       {
                                       window.location= urlDomain+"/puertorico/en/callBack";
                                       }
                                    }
                              });

               	 if (typeof googleAnalyticsTagManager.handleGtmErrors === "function") {
	                    googleAnalyticsTagManager.handleGtmErrors(thrownError);
	             }
               }
                });
            }
        });
    },

	bindDebtCheckSsnForm: function() {
        var ssnErrorMsgEmpty = $("#ssnErrorMsgEmpty").val();
	    var ssnErrorMsgInvalid = $("#ssnErrorMsgInvalid").val();
        $('#customerSSNForm').validate({
            rules: {
                "ssn": {
                    required: true,
                    minlength: 11,
                    maxlength: 11
                },
            },
            messages: {
                ssn: {
                    required: ssnErrorMsgEmpty,
                    minlength: ssnErrorMsgInvalid,
                    maxlength: ssnErrorMsgInvalid
                },
            },
            invalidHandler: function() {
                console.log("SSN error msg");
            }
        });

		$(document).on("submit", "#customerSSNForm", function(e) {
			e.preventDefault();
			$(".addressValidationLoader").show();
			$("#selectCreditCheck").hide();
			if($("#customerSSNForm").valid()){
                var ssn = $("#ssn-val").val();
                ssn = format(ssn, [3, 2], "-");
                $('input[name="ssn"]').val(ssn);
                var timeout = $("#c2cTimerForLcprApi").val();
                var callBackTimeOut = $("#c2cTimerForCallBack").val();
                var callBackType = "DebtCheck";
                var url = ACC.config.encodedContextPath + '/checkout/multi/credit-check/updateCustomerSSN';
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#customerSSNForm").serialize(),
                    timeout: timeout,
                    success: function(data) {
                        console.log("Moving to next step-clearing Interval for timeout function:- Debt Check");
                        var avoid = "redirect:";
                        var avoided = data.replace(avoid, '');
                        if (avoided == '/callBack') {
                            var urlDomain = window.location.href;
                            var lcprUrl = urlDomain.split("/puertorico/");
                            urlDomain = lcprUrl[0];
                            if (lcprUrl[1].toString().includes("es")) {
                                window.location = urlDomain + "/puertorico/es" + avoided;
                            }
                            if (lcprUrl[1].toString().includes("en")) {
                                window.location = urlDomain + "/puertorico/en" + avoided;
                            }
                        } else if (avoided == '/error') {
                            sessionStorage.errorBackURL = window.location.href;
                            var urlDomain = window.location.href;
                            var lcprUrl = urlDomain.split("/puertorico/");
                            urlDomain = lcprUrl[0];
                            if (lcprUrl[1].toString().includes("es")) {
                                window.location = urlDomain + "/puertorico/es" + avoided;
                            }
                            if (lcprUrl[1].toString().includes("en")) {
                                window.location = urlDomain + "/puertorico/en" + avoided;
                            }
                        } else if (data.toString().includes("success")) {
                            $("#selectCreditCheck").hide();
                            $("#selectCreditCheckAddr, .creditCheckWithSavedAddress").show();
                            $('#newCreditCheckAddr, .addressValidationLoader').hide();
                            sessionStorage.setItem('gtm_acceptCreditCheck', true);
                        }
                    },
                    error: function(xhr, textStatus, thrownError) {
                        var url = ACC.config.encodedContextPath + '/click/callBackRequest';
                        e.preventDefault();
                        $.ajax({
                            type: "POST",
                            url: url,
                            timeout: callBackTimeOut,
                            data: { callBackType: callBackType, houseNo: '' },
                            async: true,
                            success: function(data) {
                                console.log(" Moving to next step-clearing Interval for timeout function:- call back request");
                                if (data == '/error') {
                                    sessionStorage.errorBackURL = window.location.href;
                                }
                                var avoid = "redirect:";
                                var avoided = data.replace(avoid, '');
                                if (avoided == '/error') {
                                    sessionStorage.errorBackURL = window.location.href;
                                }
                                var urlDomain = window.location.href;
                                var lcprUrl = urlDomain.split("/puertorico/");
                                urlDomain = lcprUrl[0];
                                if (lcprUrl[1].toString().includes("es")) {
                                    window.location = urlDomain + "/puertorico/es" + avoided;
                                }
                                if (lcprUrl[1].toString().includes("en")) {
                                    window.location = urlDomain + "/puertorico/en" + avoided;
                                }
                            },
                            error: function(xhr, textStatus, thrownError) {
                                var urlDomain = window.location.href;
                                var lcprUrl = urlDomain.split("/puertorico/");
                                urlDomain = lcprUrl[0];
                                if (lcprUrl[1].toString().includes("es")) {
                                    window.location = urlDomain + "/puertorico/es/callBack";
                                }
                                if (lcprUrl[1].toString().includes("en")) {
                                    window.location = urlDomain + "/puertorico/en/callBack";
                                }
                            }
                        });

                        if (typeof googleAnalyticsTagManager.handleGtmErrors === "function") {
                            googleAnalyticsTagManager.handleGtmErrors(thrownError);
                        }
                    }
                });
            }else{
                $(".addressValidationLoader").hide();
                $("#selectCreditCheck").show();
            }
		});
	},


bindCreditCheckAddress:function() {
       $(document).on("submit","#creditCheckForm",function(e){
            e.preventDefault();
            if($("#creditCheckForm").valid()){
                //$("#creditCheckStep").hide();
                $(".addressValidationLoader").show();
                var timeout=$("#c2cTimerForLcprApi").val();
                 var callBackTimeOut=$("#c2cTimerForCallBack").val();
                 var callBackType="CreditCheck";
                var url  = ACC.config.encodedContextPath + '/checkout/multi/credit-check/updateCreditCheckAddress';
                $.ajax({
                    type: "POST",
                    url:url,
                    data: $("#creditCheckForm").serialize(),
                     timeout: timeout,
                    success: function (data) {
                     console.log("Moving to next step-clearing Interval for timeout function:- Credit Check");
                        if(data=='/callBack')
                        {
                           var urlDomain=window.location.href;
                           var lcprUrl= urlDomain.split("/puertorico/");
                           urlDomain=lcprUrl[0];
                           if(lcprUrl[1].toString().includes("es"))
                           {
                           window.location= urlDomain+"/puertorico/es"+data;
                           }
                           if(lcprUrl[1].toString().includes("en"))
                          {
                          window.location= urlDomain+"/puertorico/en"+data;
                          }
                        }else if(data=='/error'){
                           sessionStorage.errorBackURL = window.location.href;
                            var urlDomain=window.location.href;
                           var lcprUrl= urlDomain.split("/puertorico/");
                            urlDomain=lcprUrl[0];
                            if(lcprUrl[1].toString().includes("es"))
                            {
                            window.location= urlDomain+"/puertorico/es"+data;
                            }
                            if(lcprUrl[1].toString().includes("en"))
                           {
                           window.location= urlDomain+"/puertorico/en"+data;
                           }
                        } else if(data.toString().includes("redirect:")){
                            var avoid = "redirect:";
                            var profileForm = data.replace(avoid,'');
                            window.location=ACC.config.encodedContextPath + profileForm;
                        }
                },
                error:function (xhr, textStatus, thrownError)
                {
                       var url  = ACC.config.encodedContextPath +'/click/callBackRequest';
                       e.preventDefault();
                       $.ajax({
                             type:"POST",
                             url:url,
                             timeout: callBackTimeOut,
                             data:{callBackType:callBackType,houseNo:''},
                             async:true,
                             success:function(data){
                                   console.log(" Moving to next step-clearing Interval for timeout function:- call back request");
                                   if(data=='/error'){
                                       sessionStorage.errorBackURL = window.location.href;
                                       }
                                var avoid = "redirect:";
                                var avoided = data.replace(avoid,'');
                                if(avoided=='/error'){
                                    sessionStorage.errorBackURL = window.location.href;
                                    }
                                  var urlDomain=window.location.href;
                                  var lcprUrl= urlDomain.split("/puertorico/");
                                   urlDomain=lcprUrl[0];
                                   if(lcprUrl[1].toString().includes("es"))
                                   {
                                   window.location= urlDomain+"/puertorico/es"+avoided;
                                   }
                                   if(lcprUrl[1].toString().includes("en"))
                                  {
                                  window.location= urlDomain+"/puertorico/en"+avoided;
                                  }
                              },
                             error:function (xhr, textStatus, thrownError)
                             {
                                var urlDomain=window.location.href;
                                var lcprUrl= urlDomain.split("/puertorico/");
                                 urlDomain=lcprUrl[0];
                                 if(lcprUrl[1].toString().includes("es"))
                                 {
                                 window.location= urlDomain+"/puertorico/es/callBack";
                                 }
                                 if(lcprUrl[1].toString().includes("en"))
                                {
                                window.location= urlDomain+"/puertorico/en/callBack";
                                }
                             }
                       });
                       if (typeof googleAnalyticsTagManager.handleGtmErrors === "function") {
                           googleAnalyticsTagManager.handleGtmErrors(thrownError);
                       }
                }
                });
            }
        });
    },
    bindSelectedAddress:function(){
        $(document).on('change', 'input[type=radio][name=address]', function() {
            $('input[name="address"]').parent().removeClass('selected');
            $(this).parent().addClass('selected');
            var radioVal = $(this).val();
            $(".addressList").hide();
            $(".selectedAddress").show();
            var addressdata = radioVal.split(",");
            addressdata.pop(); //.toString();
            //console.log(addressdata);
            var printData = "<p class='addressData'>"+addressdata[0]+", "+addressdata[1]+"<br> <span class='subDetails'>"+addressdata[2]+" "+addressdata[3]+", "+addressdata[4]+"</span></p>";
            $("#selectedAddr").html(printData);
            $("#addressSelected").val(radioVal);
            $("#callBackUrl").hide();
        });
    },

    bindAddressForServiceability:function(){
        $(document).on("click",'#selectedAddressSubmit',function(e){
            e.preventDefault();

            //scroll to top
            ACC.checkout.bindScrollToTop();

            $(".selectedAddress").hide();
            $(".checkServiceabilityLoader").show();
            var t = $("input[name='address']:checked").val();
            var timeout=$("#c2cTimerForLcprApi").val();
             var callBackTimeOut=$("#c2cTimerForCallBack").val();
            var callBackType="Serviceability";
            var arrayT = t.split(",");
            var houseNo= arrayT[5];
            var newData = {
                  "streetname":arrayT[0],
                  "line1":arrayT[1],
                  "city":arrayT[2],
                  "province":arrayT[3],
                  "postalCode":arrayT[4],
                  "houseNo":arrayT[5]
            };
            var stringdata = JSON.stringify(newData);
            var url  = ACC.config.encodedContextPath + '/checkout/multi/delivery-address/check-serviceability';
            $.ajax({
                  type:"POST",
                  url:url,
                  contentType:'application/json',
                  dataType:'json',
                  data:stringdata,
                  timeout:timeout,
                  async:true,
                  success:function(data){
                     console.log("Moving to next step-clearing Interval for timeout function:- check serviceability");
                     var avoid = "redirect:";
                     var avoided = data.replace(avoid,'');
                     if(avoided=='/error'){
                         sessionStorage.errorBackURL = window.location.href;
                      }
                     if(data=='/error'){
                     sessionStorage.errorBackURL = window.location.href;
                     }
                         var urlDomain=window.location.href;
                            var lcprUrl= urlDomain.split("/puertorico/");
                             urlDomain=lcprUrl[0];
                             if(lcprUrl[1].toString().includes("es"))
                             {
                             window.location= urlDomain+"/puertorico/es"+avoided;
                             }
                             if(lcprUrl[1].toString().includes("en"))
                            {
                            window.location= urlDomain+"/puertorico/en"+avoided;
                            }
                  },
                  error:function (xhr, textStatus, thrownError)
                  {
                    e.preventDefault();
                             var url  = ACC.config.encodedContextPath +'/click/callBackRequest';
                              $.ajax({
                                    type:"POST",
                                    url:url,
				       async:"true",
                                    timeout: callBackTimeOut,
                                    data:{callBackType:callBackType,houseNo:houseNo},
                                    async:true,
                                    success:function(data){
                                    	  console.log("Moving to next step-clearing Interval for timeout function:- call back request");
                                    	  if(data=='/error'){
                                              sessionStorage.errorBackURL = window.location.href;
                                              }
                                       var avoid = "redirect:";
                                       var avoided = data.replace(avoid,'');
                                       if(avoided=='/error'){
                                           sessionStorage.errorBackURL = window.location.href;
                                           }
                                       var urlDomain=window.location.href;
                                        var lcprUrl= urlDomain.split("/puertorico/");
                                         urlDomain=lcprUrl[0];
                                         if(lcprUrl[1].toString().includes("es"))
                                         {
                                         window.location= urlDomain+"/puertorico/es"+avoided;
                                         }
                                         if(lcprUrl[1].toString().includes("en"))
                                        {
                                        window.location= urlDomain+"/puertorico/en"+avoided;
                                        }
                                     },
                                      error:function (xhr, textStatus, thrownError)
                                     {
                                         var urlDomain=window.location.href;
                                          var lcprUrl= urlDomain.split("/puertorico/");
                                           urlDomain=lcprUrl[0];
                                           if(lcprUrl[1].toString().includes("es"))
                                           {
                                           window.location= urlDomain+"/puertorico/es/callBack";
                                           }
                                           if(lcprUrl[1].toString().includes("en"))
                                          {
                                          window.location= urlDomain+"/puertorico/en/callBack";
                                          }
                                     }
                              });

                 	 if (typeof googleAnalyticsTagManager.handleGtmErrors === "function") {
  	                    googleAnalyticsTagManager.handleGtmErrors(thrownError);
  	                }
                 }
            });
        });
    },

    bindShowResidenceTypes:function() {

        var selectedAddressradioButton = '';
        $(".addressRadioButton").each(function(){
            if($(this).is(':checked')) { selectedAddressradioButton = $(this).attr('id'); }
        });
        if(selectedAddressradioButton == '' || selectedAddressradioButton == 'walkUp'){
            $("#walkUp.addressRadioButton").prop("checked", true).parent().parent().addClass('selected');
            $("#walkUpFormElements :input").removeAttr("disabled");
            $("#urbanFormElements :input").attr("disabled", "true");
            $("#ruralFormElements :input").attr("disabled", "true");
            $("#walkUpFormElements").show();
            $("#urbanFormElements, #ruralFormElements").hide();
        }else if(selectedAddressradioButton == 'urban') {
            $("#urban.addressRadioButton").parent().parent().addClass('selected');

            $("#urbanFormElements :input").removeAttr("disabled");
            $("#walkUpFormElements :input").attr("disabled", "true");
            $("#ruralFormElements :input").attr("disabled", "true");

            $("#urbanFormElements").show();
            $("#walkUpFormElements, #ruralFormElements").hide();
        }else if(selectedAddressradioButton == 'rural') {
            $("#rural.addressRadioButton").parent().parent().addClass('selected');
            $("#ruralFormElements :input").removeAttr("disabled");
            $("#urbanFormElements :input").attr("disabled", "true");
            $("#walkUpFormElements :input").attr("disabled", "true");

            if($('#ruralAddressLine2').val() == 'Carretera'){
                $("input[id='ruralAddressLine1']").attr('placeholder',$("#placeHolderRuralHighway").val()).show();
                $("#ruralAddressKilometer").show();
                $("#addressForm, #creditCheckForm").off('keypress', "input[id='ruralAddressLine1']").on('keypress',"input[id='ruralAddressLine1']", function(e){
                    var regex =  /^[0-9]$/;
                    return regex.test(e.key);
                }).off('paste', "input[id='ruralAddressLine1']").on('paste',"input[id='ruralAddressLine1']", function(e){
                    var stopPaste8 = function(){
                        this.value = this.value.replace(/[^0-9 ]/gi,'');
                    };
                    setTimeout(stopPaste8.bind(this), 1);
                });
            }else if( $('#ruralAddressLine2').val() == 'Calle' ){
                $("input[id='ruralAddressLine1']").attr('placeholder',$("#placeHolderRuralStreet").val()).show();
                $("#addressForm, #creditCheckForm").off('keypress', "input[id='ruralAddressLine1']").on('keypress',"input[id='ruralAddressLine1']", function(e){
                    var regex =  /^[a-z A-Z 0-9]+$/;
                    return regex.test(e.key);
                }).off('paste', "input[id='ruralAddressLine1']").on('paste',"input[id='ruralAddressLine1']", function(e){
                    var stopPaste9 = function(){
                        this.value = this.value.replace(/[^a-z A-Z 0-9 ]/gi,'');
                    };
                    setTimeout(stopPaste9.bind(this), 1);
                });
                $("#ruralAddressKilometer, #ruralAddressKilometer-error").hide();
            } else {
                $("#addressForm input[id='ruralAddressLine1'], #ruralAddressLine1-error, #ruralAddressKilometer").hide();
            }


            $("#ruralFormElements").show();
            $("#walkUpFormElements, #urbanFormElements").hide();
        }else{
            $("#walkUpFormElements").show();
            $("#urbanFormElements").hide();
            $("#ruralFormElements").hide();
            $("#urbanFormElements :input").attr("disabled", "true");
            $("#ruralFormElements :input").attr("disabled", "true");
        }

        $('input[name="typeOfResidence"]').change(function(){
            $("#addressForm").resetForm();
            $("label.error").hide();
            $(this).prop('checked', true);
            $('input[name="typeOfResidence"]').parent().parent().removeClass('selected');
            $(this).parent().parent().addClass('selected');
            var radioVal = $(this).val();
            if('URBAN' === radioVal){
                $("#urbanFormElements :input").removeAttr("disabled");
                $("#walkUpFormElements :input").attr("disabled", "true");
                $("#ruralFormElements :input").attr("disabled", "true");

                $("#urbanFormElements").show();
                $("#ruralFormElements").hide();
                $("#walkUpFormElements").hide();
            } else if('RURAL' === radioVal){
                $("#ruralFormElements :input").removeAttr("disabled");
                $("#urbanFormElements :input").attr("disabled", "true");
                $("#walkUpFormElements :input").attr("disabled", "true");

                $("#ruralFormElements").show();
                $("#urbanFormElements").hide();
                $("#walkUpFormElements").hide();
            }else if('WALKUP' === radioVal){
                 $("#walkUpFormElements :input").removeAttr("disabled");
                 $("#urbanFormElements :input").attr("disabled", "true");
                 $("#ruralFormElements :input").attr("disabled", "true");

                 $("#walkUpFormElements").show();
                 $("#urbanFormElements").hide();
                 $("#ruralFormElements").hide();
            }
         });
	},

	bindForms:function(){
		$(document).on("click","#deliveryMethodSubmit",function(e){
			e.preventDefault();
			$('#selectDeliveryMethodForm').submit();
		})
	},

	bindSavedPayments:function(){
		$(document).on("click",".js-saved-payments",function(e){
			e.preventDefault();
			var title = $("#savedpaymentstitle").html();
			$.colorbox({
				href: "#savedpaymentsbody",
				inline:true,
				maxWidth:"100%",
				opacity:0.7,
				//width:"320px",
				title: title,
				close:'<span class="glyphicon glyphicon-remove"></span>',
				onComplete: function(){
				}
			});
		})
	},

	bindContractMethodMessage: function() {
		$(".WITHCONTRACT").show();
        $(".WITHOUTCONTRACT").hide();
		function showContractMessage() {
			var selectedContract = $('input[name=contractType]:checked', '#contractTypeForm').val();
			if(selectedContract == "WITHCONTRACT") {
				$(".WITHCONTRACT").show();
				$(".WITHCONTRACT").siblings(".contract-block-text").show();
				$(".WITHOUTCONTRACT").hide();
				$(".WITHOUTCONTRACT").siblings(".contract-block-text").hide();
			}
			else {
				$(".WITHOUTCONTRACT").show();
				$(".WITHOUTCONTRACT").siblings(".contract-block-text").show();
				$(".WITHCONTRACT").hide();
				$(".WITHCONTRACT").siblings(".contract-block-text").hide();
			}
		}
		$('#contractTypeForm input').on('change', function() {
			showContractMessage();
		});
        $(".WITHCONTRACT .readMore, .WITHOUTCONTRACT .readMore").on("click", function () {
            $(this).hide();
            $(this).parent().find(".initial").hide();
            $(this).parent().find(".readmoreText").slideToggle();
        }).on("click", ".termsInfo.readmoreText", function (event) {
            event.stopPropagation();
        });
        $(".WITHCONTRACT .readLess, .WITHOUTCONTRACT .readLess").on("click", function () {
            $(this).parent().parent().find(".readmoreText").slideToggle();
            $(this).parent().parent().find(".initial, .readMore").show();
        });
	},

	bindCreditCheckValue: function() {
		$("#selectCreditCheck, .creditCheckYes").show();
        $("#selectCreditCheckAddr, .creditCheckWithSavedAddress, .getAccAlert.newAddress, .creditCheckNo").hide();
		$('#newCreditCheckAddr').hide();

        $('input[name="creditCheck"]').change(function() {
            if ($(this).val() == "creditCheckYes") {
                $(".termsInfo.creditCheckYes").show();
                $(".termsInfo.creditCheckNo").hide();
            }else if($(this).val() == "creditCheckNo") {
                $(".termsInfo.creditCheckYes").hide();
                $(".termsInfo.creditCheckNo").show();
            }
        });

        $('.submitCreditCheckSelect').click(function(event) {
            var creditCheckValue = $('input[name="creditCheck"]:checked').val();
            if (creditCheckValue == "creditCheckYes") {
                $("#selectCreditCheck").hide();
                $("#selectCreditCheckAddr, .creditCheckWithSavedAddress").show();
                $('#newCreditCheckAddr').hide();
                sessionStorage.setItem('gtm_acceptCreditCheck', true);
            }else if( creditCheckValue == 'creditCheckNo'){
            	 sessionStorage.setItem('gtm_acceptCreditCheck', false);
            	  event.preventDefault();
                 var url  = ACC.config.encodedContextPath + '/checkout/multi/credit-check/no-credit-check';
                   $.ajax({
                              type:"POST",
                              url:url,
                              success:function(data){
                            	  if(data=='/error'){
                                      sessionStorage.errorBackURL = window.location.href;
                                      }
                                 var avoid = "redirect:";
                                 var avoided = data.replace(avoid,'');
                                 if(avoided=='/error'){
                                     sessionStorage.errorBackURL = window.location.href;
                                     }
                                 var urlDomain=window.location.href;
                                      var lcprUrl= urlDomain.split("/puertorico/");
                                       urlDomain=lcprUrl[0];
                                       if(lcprUrl[1].toString().includes("es"))
                                       {
                                       window.location= urlDomain+"/puertorico/es"+avoided;
                                       }
                                       if(lcprUrl[1].toString().includes("en"))
                                      {
                                      window.location= urlDomain+"/puertorico/en"+avoided;
                                      }
                               }
                          });
            }
        });
        $('.creditCheckWithSavedAddress').click(function(event) {
            if ($('input[name="selectedAddr"]').prop('checked') == true) {
                $("#creditCheckStep").hide();
                $(".addressValidationLoader").show();
                 var timeout=$("#c2cTimerForLcprApi").val();
                  var callBackTimeOut=$("#c2cTimerForCallBack").val();
                 var callBackType="CreditCheck";
                  event.preventDefault();
                $.ajax({
                    url: ACC.config.encodedContextPath + '/checkout/multi/credit-check/update',
                    async: true,
                    type: "POST",
                    timeout:timeout,
                    async:true,
                    success: function(data) {
                         console.log("Moving to next step-clearing Interval for timeout function:- credit-check-update");
                        var avoid = "redirect:";
						var avoided = data.replace(avoid,'');
						if(avoided=='/error'){
                            sessionStorage.errorBackURL = window.location.href;
                            }
						var urlDomain=window.location.href;
                          var lcprUrl= urlDomain.split("/puertorico/");
                           urlDomain=lcprUrl[0];
                           if(lcprUrl[1].toString().includes("es"))
                           {
                           window.location= urlDomain+"/puertorico/es"+avoided;
                           }
                           if(lcprUrl[1].toString().includes("en"))
                          {
                          window.location= urlDomain+"/puertorico/en"+avoided;
                          }
                    },
                    error:function (xhr, textStatus, thrownError){
                                event.preventDefault();
                                 var url  = ACC.config.encodedContextPath +'/click/callBackRequest';
                                          $.ajax({
                                                type:"POST",
                                                url:url,
						   async:"true",
                                                timeout: callBackTimeOut,
                                                data:{callBackType:callBackType,houseNo:''},
                                                async:true,
                                        success:function(data)
                                        {
                                               console.log("Moving to next step-clearing Interval for timeout function:- call back request");
                                                if(data=='/error'){
                                                          sessionStorage.errorBackURL = window.location.href;
                                                   }
                                                   var avoid = "redirect:";
                                                   var avoided = data.replace(avoid,'');
                                                   if(avoided=='/error'){
                                                       sessionStorage.errorBackURL = window.location.href;
                                                     }
                                             var urlDomain=window.location.href;
                                             var lcprUrl= urlDomain.split("/puertorico/");
                                              urlDomain=lcprUrl[0];
                                              if(lcprUrl[1].toString().includes("es"))
                                              {
                                              window.location= urlDomain+"/puertorico/es"+avoided;
                                              }
                                              if(lcprUrl[1].toString().includes("en"))
                                             {
                                             window.location= urlDomain+"/puertorico/en"+avoided;
                                             }
                                         },
                                        error:function (xhr, textStatus, thrownError)
                                               {

                                                 var urlDomain=window.location.href;
                                                   var lcprUrl= urlDomain.split("/puertorico/");
                                                    urlDomain=lcprUrl[0];
                                                    if(lcprUrl[1].toString().includes("es"))
                                                    {
                                                    window.location= urlDomain+"/puertorico/es/callBack";
                                                    }
                                                    if(lcprUrl[1].toString().includes("en"))
                                                   {
                                                   window.location= urlDomain+"/puertorico/en/callBack";
                                                   }
                                               }
                                          });

                   	 if (typeof googleAnalyticsTagManager.handleGtmErrors === "function") {
    	                    googleAnalyticsTagManager.handleGtmErrors(thrownError);
    	                }
                    }
                });
            }

        });

        $(".creditCheckSelect").change(function() {
            if ($(this).attr('id') == 'selectedAddr' && $(this).prop('checked') == true) {
                $('input[name="creditCheckdAddr"]').prop('checked', false);
                $("#selcteCreditCheck, .creditCheckWithSavedAddress, .savedAddress, .creditCheckSameAddrText").show();
                $('#newCreditCheckAddr, .getAccAlert.newAddress').hide();
            }else if ($(this).attr('id') == 'creditCheckdAddr' && $(this).prop('checked') == true){
                $('input[name="selectedAddr"]').prop('checked', false);
                $("#selcteCreditCheck, .creditCheckWithSavedAddress, .savedAddress, .creditCheckSameAddrText").hide();
                $('#newCreditCheckAddr, .getAccAlert.newAddress').show();
            }else {
                $(this).prop('checked', true);
            }
        });

        $(document).on("click","#creditCheckSubmit",function(e){
            e.preventDefault();
            if($("#creditCheckForm").valid()){
                $("#creditCheckStep").hide();
                $(".addressValidationLoader").show();
                $("#creditCheckForm").submit();
            }
        });

        $(".creditCheckYes .readMore, .creditCheckNo .readMore").on("click", function () {
            $(this).hide();
            $(this).parent().find(".initial").hide();
            $(this).parent().find(".readmoreText").slideToggle();
        }).on("click", ".termsInfo.readmoreText", function (event) {
            event.stopPropagation();
        });
        $(".creditCheckYes .readLess, .creditCheckNo .readLess").on("click", function () {
            $(this).parent().parent().find(".readmoreText").slideToggle();
            $(this).parent().parent().find(".initial, .readMore").show();
        });

	},

	bindCheckO: function () {
		var cartEntriesError = false;

		// Alternative checkout flows options
		$('.doFlowSelectedChange').change(function ()
		{
			if ('multistep-pci' == $('#selectAltCheckoutFlow').val())
			{
				$('#selectPciOption').show();
			}
			else
			{
				$('#selectPciOption').hide();

			}
		});

		$('.js-continue-shopping-button').click(function ()	{
			var checkoutUrl = $(this).data("continueShoppingUrl");
			window.location = checkoutUrl;
		});

		$('.js-create-quote-button').click(function () {
			$(this).prop("disabled", true);
			var createQuoteUrl = $(this).data("createQuoteUrl");
			window.location = createQuoteUrl;
		});


		$('.expressCheckoutButton').click(function() {
             document.getElementById("expressCheckoutCheckbox").checked = true;
		});

		$(document).on("input",".confirmGuestEmail,.guestEmail",function(){
			  var orginalEmail = $(".guestEmail").val();
			  var confirmationEmail = $(".confirmGuestEmail").val();
			  if(orginalEmail === confirmationEmail){
			    $(".guestCheckoutBtn").removeAttr("disabled");
			  }else{
			     $(".guestCheckoutBtn").attr("disabled","disabled");
			  }
		});

		$('.js-continue-checkout-button').click(function () {
			var checkoutUrl = $(this).data("checkoutUrl");
			cartEntriesError = ACC.pickupinstore.validatePickupinStoreCartEntires();
			if (!cartEntriesError) {
				var expressCheckoutObject = $('.express-checkout-checkbox');
				if(expressCheckoutObject.is(":checked")) {
					window.location = expressCheckoutObject.data("expressCheckoutUrl");
				} else {
					var flow = $('#selectAltCheckoutFlow').val();
					if ( flow == undefined || flow == '' || flow == 'select-checkout') {
						// No alternate flow specified, fallback to default behaviour
						window.location = checkoutUrl;
					} else {
						// Fix multistep-pci flow
						if ('multistep-pci' == flow) {
						    flow = 'multistep';
						}
						var pci = $('#selectPciOption').val();
						// Build up the redirect URL
						var redirectUrl = checkoutUrl + '/select-flow?flow=' + flow + '&pci=' + pci;
						window.location = redirectUrl;
					}
				}
			}
			return false;
		});
	},
    bindCheckSsn :function () {
        window.addEventListener( "pageshow", function ( event ) {
            if($('input[name="ssn"]').val() != '' && $('input[name="ssn"]').val() != undefined){
              var _this = $('input[name="ssn"]');
              var $input = _this;
              var val = _this.val();
              console.log(val);
              $("#ssn-val").val(val.replace(/-/g, ""));
              var displayVal = ssntransformDisplay(val);
              $input.val(displayVal);
            }
        });

        $('input[name="ssn"]').on("cut copy paste",function(e) {
            e.preventDefault();
        });

        var value;
        var $ssn = $('input[name="ssn"]');
        var $ssnVal = $('#ssn-val');

        String.prototype.replaceAt=function(index, character) {
            return this.substr(0, index) + character + this.substr(index+character.length);
        }

        var ssn = {
            init: function () {
                this.bindSSNfunc();
            },

            bindSSNfunc: function () {
                $ssn.on('focus', this.syncInput)
                    // .on('input', this.syncInput)
                    .on('change', this.syncInput)
                    //.on('blur', this.syncInput)
                    .on('keydown', this.syncInput)
                    .on('keyup', this.syncInput)
                    .on('click', this.syncInput);
            },

            transformDisplay: function (val) {
                // Strip all non numbers
                var displayVal = val.replace(/[^0-9|\\*]/g, '');
                displayVal = displayVal.substr(0, 9);

                // Inject dashes
                if (displayVal.length >= 4) {
                    displayVal = displayVal.slice(0, 3) + '-' + displayVal.slice(3);
                }

                if (displayVal.length >= 7) {
                    displayVal = displayVal.slice(0, 6) + '-' + displayVal.slice(6);
                }

                // Replace all numbers with astericks
                if (displayVal.length < 7) {
                    displayVal = displayVal.replace(/[0-9]/g, '*');
                }

                return displayVal;
            },

            transformValue: function (val) {
                if (typeof value !== 'string') {
                    value = "";
                }

                if (!val) {
                    value = null;
                    return;
                }

                var cleanVal = val.replace(/[^0-9|\\*]/g, '');
                cleanVal = cleanVal.substr(0, 9);

                for (i = 0, l = cleanVal.length; i < l; i++) {
                    if (/[0-9]/g.exec(cleanVal[i])) {
                        value = value.replaceAt(i, cleanVal[i]);
                    }
                }

                value = value.substr(0, cleanVal.length);
            },

            syncInput: function () {
                var $input = $(this);
                var val = $(this).val();
                this.setSelectionRange(val.length, val.length);
                if($(window).width() < 767){
                    setTimeout(function(){
                        var displayVal = ssn.transformDisplay(val);
                        ssn.transformValue(val);
                        $input.val(displayVal);
                        $ssnVal.val(value);
                        var actualValLength = $ssnVal.val().length;
                        if (actualValLength > 3 && actualValLength < 5) {
                            actualValLength = 1;
                        } else if (actualValLength >= 5) {
                            actualValLength = 2;
                        } else {
                            actualValLength = 0;
                        }
                        if (displayVal.length > ($ssnVal.val().length) + actualValLength) {
                            var lastChar = displayVal.slice(-1);
                            if (lastChar === '*') {
                                var valFinal = $ssn.val().substring(0, $ssn.val().length - 1);
                                $ssn.val(valFinal);
                            }
                        }
                    }, 100);
                }else{
                    var displayVal = ssn.transformDisplay(val);
                    ssn.transformValue(val);
                    $input.val(displayVal);
                    $ssnVal.val(value);
                    var actualValLength = $ssnVal.val().length;
                    if (actualValLength > 3 && actualValLength < 5) {
                        actualValLength = 1;
                    } else if (actualValLength >= 5) {
                        actualValLength = 2;
                    } else {
                        actualValLength = 0;
                    }
                    if (displayVal.length > ($ssnVal.val().length) + actualValLength) {
                        var lastChar = displayVal.slice(-1);
                        if (lastChar === '*') {
                            var valFinal = $ssn.val().substring(0, $ssn.val().length - 1);
                            $ssn.val(valFinal);
                        }
                    }
                }
            },

            syncValue: function () {

            },
        };

        ssn.init();

        $(document).on("click",".personalDetails_SubmitForm", function (e) {
            e.preventDefault();
            //console.log('in check function...');
            var firstName = $('input[name="firstName"]').val();
            var lastName = $('input[name="lastName"]').val();
            var dob = $('input[name="dob"]').val();
            var documentType = $("select[name='documentType']").children("option:selected").val();
            var fixedPhone = $('input[name="fixedPhone"]').val();
            var additionalEmail = $('input[name="additionalEmail"]').val();
            var docNumber = $('input[name="documentNumber"]').val();

            if(documentType == 'OTHER'){
                docNumber = '';
            }
            var data = {firstName:firstName,lastName:lastName,dob:dob,documentType:documentType, documentNumber :docNumber,fixedPhone:fixedPhone,additionalEmail:additionalEmail}
            var url  = ACC.config.encodedContextPath + '/checkout/multi/profile-form/verifyDebt';
            $.ajax({
                type: "POST",
                data: data,
                dataType: "html",
                url: url,
                 success:function(data) {
                    //console.log('in success'+data);
                	  if(data=='/error'){
                          sessionStorage.errorBackURL = window.location.href;
                          }
                    if(data != 'true') {
                        window.location = ACC.config.encodedContextPath + data
                    }
                 },
                 error:function (xhr, ajaxOptions, thrownError){
                	 if (typeof googleAnalyticsTagManager.handleGtmErrors === "function") {
 	                    googleAnalyticsTagManager.handleGtmErrors(thrownError);
 	                }
                }
            });
        });
	},
  bindScrollToTop : function(){
      //scroll to top
      $('html, body').animate({
        scrollTop: $(".main__inner-wrapper").offset().top
      }, 1000);
  }
};

//maxlength characters for address validation
$(document).ready(function(){
	//allowing only numeric values in DOB and phone number fields
	$("#customerProfileForm").on('keypress',"input[name='dob'], input[name='fixedPhone']", function(e){
		const pattern = /^[0-9]$/;
		return pattern.test(e.key )
	});
	//Mobile number format display in personal details
	$("input[name='fixedPhone']").on('keyup touchend', function() {
		formatPhone(this);
	}).attr("maxlength", "13");
	function formatPhone(obj) {
		var numbers = obj.value.replace(/\D/g, ''),char = {0:'(',3:')',6:'-'};
		obj.value = '';
		for (var i = 0; i < numbers.length; i++) {
			obj.value += (char[i] || '') + numbers[i];
		}
	}

	//DOB format display in personal details
	$("input[name='dob']").on('keyup', function() {
        var key = event.keyCode || event.charCode;

        if( key == 8){
            return false;
        }
        var val = $(this).val();
        var length = val.length;

        if(length == 3 && val.substring(2, 3) !== "-") {
            val = val.substring(0,2) + '-' + val.substring(2,3);
        }
        if(length == 6 && val.substring(5, 6) !== "-") {
            val = val.substring(0,5) + '-' + val.substring(5,6);
        }
        $(this).val(val);
        if ($(this).val().length == 2 || $(this).val().length == 5) {
            $(this).val($(this).val() + "-");
        }
    }).attr("maxlength", "10");

	//Document Type Other selection
	$('select[name ="documentType"]').on('change', function() {
    if(this.value == 'OTHER'){
        $('input[name ="documentNumber"]').parent().hide();
		$('select[name ="driverLicenceState"]').parent().parent().hide();

    }else{
        $('input[name ="documentNumber"]').parent().show();
		$('select[name ="driverLicenceState"]').parent().parent().show();
	}
	}).trigger("change");

    // SSN check
        $("#customerProfileForm").on("change", function() {
            if($('#hasSSN').is(':checked')) {
	            $(".ssn-fields").removeClass("hide");
	            $("input[name='checkSSN']").val("true");
	            $("#customercheckSSN").val("true");
            }
            else{
                $(".ssn-fields").addClass("hide");
                $("input[name='checkSSN']").val("false");
                $("#customercheckSSN").val("false");
            }
        }).trigger("change");

	$(window).on('load', function(){
		$("input[name='fixedPhone']").each(function() {
			$(this).val($(this).val().replace(/(\d{3})(\d{3})(\d{4})/, "($1)$2-$3"));
		});
	});

	$('.customerProfileForm .btn').on('click', function(e){
        e.preventDefault();
        $("#customerProfileForm").submit();
	});

	//cart
	//Mobile number format display in Cart login
	$("#guestCartCheckout").on('keypress',"input[name='mobilePhone']", function(e){
		const pattern = /^[0-9]$/;
		return pattern.test(e.key )
	});
	$("input[name='mobilePhone']").on('keyup touchend', function() {
		formatPhone(this);
	}).attr("maxlength", "13");
	function formatPhone(obj) {
		var numbers = obj.value.replace(/\D/g, ''),char = {0:'(',3:')',6:'-'};
		obj.value = '';
		for (var i = 0; i < numbers.length; i++) {
			obj.value += (char[i] || '') + numbers[i];
		}
	}
	 //Guest checkout for button GO press functionality
    $('.page-cartPage').on('click', '.guestCartCheckoutButton', function(event) {
        guestCartLoginValidate();
    });
    function guestCartLoginValidate(){
        if($('#guestCartCheckout').valid()){
			var guestName = $("#guestName").val();
            var guestEmail = $("#guestEmail").val();
            var mobilePhone = $("#mobilePhone").val();
            var existingCustomer = $("#existingCustomer").is(":checked");

            if(mobilePhone==undefined){
             mobilePhone="";
            }
            $.ajax({
                url: ACC.config.encodedContextPath + '/login/checkout/guest',
                async: true,
                type: "POST",
                dataType: "html",
                data: {
					guestName: guestName,
                    guestEmail: guestEmail,
                    mobilePhone:mobilePhone,
                    existingCustomer:existingCustomer
                },
                success:function(data) {
                	  if(data=='/error'){
                          sessionStorage.errorBackURL = window.location.href;
                          }
                if(data=='/callBack')
                 {
                   window.location=ACC.config.encodedContextPath + '/callBack';
                 }
                 else{
                   window.location=ACC.config.encodedContextPath + '/checkout';
                 }
               },
               error:function(data,status,xhr) {
                   console.log('error occured');
               }
            });
        }
    }

    $('#guestCartCheckout #mobilePhone').on('blur', function() {
        $("#guestCartCheckout #mobilePhone").valid();
    }).on('keypress', function() {
        $("#guestCartCheckout #mobilePhone").valid();
    });

	$('#guestCartCheckout #guestName').on('blur', function() {
        $("#guestCartCheckout #guestName").valid();
    }).on('keypress', function() {
        $("#guestCartCheckout #guestName").valid();
    });

	 if ($("#guestCartCheckout").length) {
        $('#guestCartCheckout').on('keypress',"input[name='mobilePhone']", function(e){
            const pattern = /^[0-9]$/;
            return pattern.test(e.key )
        });
		var guestName_error = $("#guestName_error").val();
		var guestNameLength_error = $("#guestNameLength_error").val();
        var guestEmail_error = $("#guestEmail_error").val();
        var guestEmailempty_error = $("#guestEmailempty_error").val();
        var guestPhone_error = $("#guestPhone_error").val();
        var guestPhoneempty_error = $("#guestPhoneempty_error").val();
        $.validator.addMethod("regx", function(value, element, regexpr) {
            if( regexpr.test(value)){
              return true;
            }
            else{
              return false;
            }
        }, guestEmail_error);
        $('#guestCartCheckout').validate({
            rules: {
                "guestEmail": {
                    required: true,
                    regx: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    email: true
                },
                "mobilePhone": {
                    required: true,
                    minlength: 13,
                    maxlength: 13
                },
				"guestName": {
					required: true,
					regx: /^[a-zA-Z][\sa-zA-Z]+$/,
//					maxlength: 25
				},
            },
            messages: {
				guestName:{
					required: guestName_error,
					regx: guestName_error,
//					maxlength: guestNameLength_error
				},
                guestEmail: {
                    required: guestEmailempty_error,
                },
                mobilePhone: {
                    required: guestPhoneempty_error,
                    minlength: guestPhone_error,
                    maxlength: guestPhone_error
                },
            },
            invalidHandler: function() {
                if (typeof googleAnalyticsTagManager.loginFormGtmError === "function") {
                    googleAnalyticsTagManager.loginFormGtmError();
                }
            }
        });

         jQuery.extend(jQuery.validator.messages, {
             email: guestEmail_error,
         });
    }

    /* Address Form Validtion */
    if ($("#addressForm, #creditCheckForm").length) {
        $("#addressForm, #creditCheckForm").on('keypress change paste',"input[id='walkupAddressStreetname'], input[id='walkupAddressLine1'], input[id='urbanAddressStreetname']", function(e){
            const pattern = /^[a-zA-Z0-9- ]+$/;
            return pattern.test(e.key);
        }).on('paste',"input[id='walkupAddressStreetname'], input[id='walkupAddressLine1'], input[id='urbanAddressStreetname']", function(e){
            var stopPaste = function(){
                this.value = this.value.replace(/[^a-zA-Z0-9- ]/gi,'');
            };
            setTimeout(stopPaste.bind(this), 1);
        });

        $("#addressForm, #creditCheckForm").on('keypress change paste',"input[id='walkupAddressAppartment'], input[id='urbanAddressBuilding'], input[id='urbanAddressBlock']", function(e){
            var regex =  /^[a-z A-Z 0-9 . -]+$/;
            return regex.test(e.key);
        }).on('paste',"input[id='walkupAddressAppartment'], input[id='urbanAddressBuilding'], input[id='urbanAddressBlock']", function(e){
            var stopPaste1 = function(){
                this.value = this.value.replace(/[^a-z A-Z 0-9 . - ]/gi,'');
            };
            setTimeout(stopPaste1.bind(this), 1);
        });

        $("#addressForm, #creditCheckForm").on('keypress change paste',"input[id='walkupAddressTownCity'], input[id='urbanAddressTownCity'], input[id='ruralAddressTownCity']", function(e){
            var regex = /^[a-zA-Z- ]+$/;
            return regex.test(e.key);
        }).on('paste',"input[id='walkupAddressTownCity'], input[id='urbanAddressTownCity'], input[id='ruralAddressTownCity']", function(e){
            var stopPaste2 = function(){
                this.value = this.value.replace(/[^a-z A-Z - ]/gi,'');
            };
            setTimeout(stopPaste2.bind(this), 1);
        });

        $("#addressForm, #creditCheckForm").on('keypress change paste',"input[id='urbanAddressLine1'], input[id='urbanAddressAppartment'], input[id='ruralAddressArea']", function(e){
            var regex = /^[a-zA-Z0-9 ]+$/;
            return regex.test(e.key);
        }).on('paste',"input[id='urbanAddressLine1'], input[id='urbanAddressAppartment'], input[id='ruralAddressArea']", function(e){
            var stopPaste3 = function(){
                this.value = this.value.replace(/[^a-z A-Z 0-9 ]/gi,'');
            };
            setTimeout(stopPaste3.bind(this), 1);
        });

        $("#addressForm, #creditCheckForm").on('keypress change paste',"input[name='streetNumber'], input[id='walkupAddressPostcode'], input[id='urbanAddressPostcode'], input[id='ruralAddressPostcode'], input[id='ruralAddressPlotNo']", function(e){
            var regex = /^[0-9 ]$/;
            return regex.test(e.key);
        }).on('paste',"input[name='streetNumber'], input[id='walkupAddressPostcode'], input[id='urbanAddressPostcode'], input[id='ruralAddressPostcode'], input[id='ruralAddressPlotNo']", function(e){
            var stopPaste4 = function(){
                this.value = this.value.replace(/[^0-9 ]/gi,'');
            };
            setTimeout(stopPaste4.bind(this), 1);
        });

        $("#addressForm, #creditCheckForm").on('keypress change paste',"input[name='kilometer']", function(e){
            var regex = /^[0-9. ]$/;
            return regex.test(e.key);
        }).on('paste',"input[name='kilometer']", function(e){
            var stopPaste4 = function(){
                this.value = this.value.replace(/[^0-9. ]/gi,'');
            };
            setTimeout(stopPaste4.bind(this), 1);
        });

        var streetnameErrorMsg = $("#errorMsgStreetname").val();
        var appartmentErrorMsg = $("#errorMsgApartment").val();
        var townCityErrorMsg = $("#errorMsgTowncity").val();
        var postcodeErrorMsg = $("#errorMsgPostcode").val();
        var postcodeErrorMaxlen = $("#errorMsgPostcodeLength").val();
        var buildingErrorMsg = $("#errorMsgBuilding").val();
        var kilometerErrorMsg = $("#errorMsgKilometer").val();
        var line1ErrorMsg = $("#errorMsgHighway").val();
        var line2ErrorMsg = $("#errorMsgStreet").val();

        $("#addressForm input[id='ruralAddressLine1'],#addressForm #ruralAddressKilometer, #creditCheckForm input[id='ruralAddressLine1'], #creditCheckForm #ruralAddressKilometer").hide();
        $('#ruralAddressLine2').on('change', function (e) {
            $("#addressForm #ruralAddressLine1-error").hide();
            if( $(this).val() == 'Carretera'){
                $("input[id='ruralAddressLine1']").attr('placeholder',$("#placeHolderRuralHighway").val()).val('').show();
                $("#ruralAddressKilometer").val('').show();
                $("#addressForm, #creditCheckForm").off('keypress', "input[id='ruralAddressLine1']").on('keypress',"input[id='ruralAddressLine1']", function(e){
                    var regex =  /^[0-9]$/;
                    return regex.test(e.key);
                }).off('paste', "input[id='ruralAddressLine1']").on('paste',"input[id='ruralAddressLine1']", function(e){
                    var stopPaste8 = function(){
                        this.value = this.value.replace(/[^0-9 ]/gi,'');
                    };
                    setTimeout(stopPaste8.bind(this), 1);
                });
            }else if( $(this).val() == 'Calle' ){
                $("input[id='ruralAddressLine1']").attr('placeholder',$("#placeHolderRuralStreet").val()).val('').show();
                $("#addressForm, #creditCheckForm").off('keypress', "input[id='ruralAddressLine1']").on('keypress',"input[id='ruralAddressLine1']", function(e){
                    var regex =  /^[a-z A-Z 0-9]+$/;
                    return regex.test(e.key);
                }).off('paste', "input[id='ruralAddressLine1']").on('paste',"input[id='ruralAddressLine1']", function(e){
                    var stopPaste9 = function(){
                        this.value = this.value.replace(/[^a-z A-Z 0-9 ]/gi,'');
                    };
                    setTimeout(stopPaste9.bind(this), 1);
                });
                $("#ruralAddressKilometer, #ruralAddressKilometer-error").hide();
            } else {
                $("#addressForm input[id='ruralAddressLine1'], #ruralAddressLine1-error, #ruralAddressKilometer").hide();
            }
        });
    }

    $("#addressForm, #creditCheckForm").validate({
        rules: {
            "streetname" : {
                required: function() {
                    return $('#walkUp').is(':checked');
                }
            },
            "appartment" : {
                required: function() {
                    return ($('#walkUp').is(':checked'));
                }
            },
            "townCity" : {
                required: function() {
                    return ($('#walkUp').is(':checked') || $('#urban').is(':checked') || $('#rural').is(':checked'));
                }
            },
            "postcode" : {
                required: function() {
                    return ($('#walkUp').is(':checked') || $('#urban').is(':checked') || $('#rural').is(':checked'));
                },
                maxlength: 5,
                minlength: 5
            },
            "building" : {
                required: function() {
                    return $('#urban').is(':checked');
                }
            },
            "kilometer" : {
                required: function() {
                    return ($('#rural').is(':checked') && $('#ruralAddressLine2').val() == 'Carretera');
                }
            },
            "line1" : {
                required: function() {
                    return ($('#rural').is(':checked') && ($('#ruralAddressLine2').val() == 'Carretera' || $('#ruralAddressLine2').val() == 'Calle'));
                }
            }
        },
        messages: {
            streetname: streetnameErrorMsg,
            appartment: appartmentErrorMsg,
            townCity: townCityErrorMsg,
            postcode: {
                required: postcodeErrorMsg,
                minlength: postcodeErrorMaxlen,
                maxlength: postcodeErrorMaxlen
            },
            building: buildingErrorMsg,
            kilometer: kilometerErrorMsg,
            line1: {
                required: function(element) {
                    var err;
                    var idType = $('#ruralAddressLine2').val();
                    if(idType === "Carretera") {
                        err = line1ErrorMsg;
                    }
                    if(idType === "Calle") {
                        err = line2ErrorMsg;
                    }
                    return err;
                }
            }
        },
        invalidHandler: function() {
            if (typeof googleAnalyticsTagManager.addressFormGTMError === "function") {
                  googleAnalyticsTagManager.addressFormGTMError();
            }
        }
    });

    if($(".place-order-form .btn-place-order").length > 0){
        $(".page-multiStepCheckoutSummaryPage").addClass('final-step-checkout');
    }

    $('#customerProfileForm input[name="ssn"]').val('');

    //load datepicker in selected language
    var selectedlang = $("html").attr("lang");
    if(selectedlang == "es"){
        $.datepicker.regional['es'] = {
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa']
        }
    }
    if(selectedlang == "en"){
        $.datepicker.regional['en'] = {
            monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"]
        }
    }
    var options = $.extend(
        {},                                         
        $.datepicker.regional[selectedlang],        
        { 
          changeMonth: true,
          changeYear: true,
          dateFormat: "dd-mm-yy",
          defaultDate: "-26y",
          showAnim: "fold",
          yearRange: "-99:-18",
          maxDate: "-18Y",
          minDate: "-99Y"
        }
    );
    $("#dob").datepicker(options).attr('readonly','readonly');

    //disable copy paste in datepicker
    $('#dob').on("cut copy paste",function(e) {
      e.preventDefault();
    });

    //function to hide or show datepicker on click
    $('#dob').click(function(e) {
      if ($(this).data('count')) { // toggle
          if ($('#ui-datepicker-div').is(':visible')){
              $('#dob').datepicker('hide');
          } else {
                $('#dob').datepicker('show');
          }
      } 
      else { // first click
          $(this).data('count', 1);
      }
    });
    $('#dob').blur(function() {
        $(this).data('count', '')
    });

    $('.ui-datepicker').addClass('notranslate');
});

//Hack for reverting year order
$(document.body).delegate('select.ui-datepicker-year', 'mousedown', function() {
  (function(sel) {
    var el = $(sel);
    var ops = $(el).children().get();
    if ( ops.length > 0 && $(ops).first().val() < $(ops).last().val() ) {
      $(el).empty();
      $(el).html(ops.reverse());
    }
  })(this);
});

function format(input, format, sep) {
    var output = "";
    var idx = 0;
    for (var i = 0; i < format.length && idx < input.length; i++) {
        output += input.substr(idx, format[i]);
        if (idx + format[i] < input.length) output += sep;
        idx += format[i];
    }
    output += input.substr(idx);
    return output;
}
function ssntransformDisplay (val) {
    // Strip all non numbers
    var displayVal = val;
    // Inject dashes
    if (displayVal.length >= 4) {
        displayVal = displayVal.slice(0, 3) + '-' + displayVal.slice(3);
    }

    if (displayVal.length >= 7) {
        displayVal = displayVal.slice(0, 6) + '-' + displayVal.slice(6);
    }
    var a = '';
    for(var i = 0; i < val.length; i++){
        a += val.charAt(i);
        // Replace all numbers with astericks
        if (i < 7) {
            a = a.replace(/[0-9]/g, '*');
            console.log(a);
        }
        displayVal = a;
    }
    return displayVal;
}