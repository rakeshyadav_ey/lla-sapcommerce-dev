// passing value to backend if the addon is selected
if($('.step1ConfigForm').length) {
	var selectedAddon = $("#js-selectedAddon").val();
	if(selectedAddon) {
		$(".ctabox.selected").find('.form-group input[type="checkbox"]').prop('checked');
	}
	$('.step1ConfigForm').submit(function() {
		var checkboxId = $(this).parent().find('.form-group input[type="checkbox"]').attr("id");
		if ($("#"+checkboxId).prop('checked')==true){
			var selectedCheckboxId = $("#"+checkboxId).val();
			$(this).find("input[name='selectedAddon']").val(selectedCheckboxId);
		}
		else {
			$(this).find("input[name='selectedAddon']").val("");
		}
		$(this).parents('.ctabox').addClass('selected');
		return true; // return false to cancel form action
	});	
}

if($('.step2ConfigForm').length) {
	var twoPAddonHD = $("#js-twoPAddonHD").val();
	if(twoPAddonHD == "true") {
		$(".ctabox.selected").find('.form-group input[id^="HD_BOX"]').prop('checked');
	}
	var twoPAddonDVR = $("#js-twoPAddonDVR").val();
	if(twoPAddonDVR == "true") {
		$(".ctabox.selected").find('.form-group input[id^="DVR_BOX"]').prop('checked');
	}
	var twoPHUB = $("#js-twoPHUB").val();
	if(twoPHUB == "true") {
		$(".ctabox.selected").find('.form-group input[id^="HUB"]').prop('checked');
	}
	$('.step2ConfigForm').submit(function() {
		var selectedCheckboxIds = [];
		$(this).find('input[type=checkbox]').each(function (index, element) {
			if(element.checked) {
				selectedCheckboxIds.push($(this).val());
			}
		});
		$(this).find("input[name='selecteddAddons']").val(selectedCheckboxIds);
		$(this).parents('.ctabox').addClass('selected');
		return true; // return false to cancel form action
	});
}

$(document).on("change", '.ctabox .form-group input[type="checkbox"]', function() {
	if($(this).prop('checked')==true) {
		$(this).parents(".ctabox").parent().siblings().find('input[type="checkbox"]').prop('checked', false); 
	}
});

if($('.active-products').length) {
	var activeProducts = [];
	$('.active-products').find('input[type=hidden]').each(function(){
		activeProducts.push($(this).val());
	})
	$('.ctabox').each(function() {
		var currentProduct = $(this).find('input[id="selectedTVProduct"]').val();
		if((activeProducts.indexOf(currentProduct) > -1)) {
			$(this).find('.disabled').remove();
		}
		else {
			$(this).find('.disabled').parent(".ctabox").removeClass("selected");
		}
	})
}

function addOnsSelection() {
	if($('.configurator-body').find('.step-4-cntnr').length > 0 ) {
		$('.cl-cntnr__list').each(function() {
            var selectedClValue = '';
            var currentClValue = [];

            if($(this).length > 0 && $(this).data('cl-selected') != '' && $(this).data('cl-selected') != undefined) {
                selectedClValue = $(this).data('cl-selected').replace(/[\[\]']+/g,' ').split(',');
            }
            //var finalSelectedClValue = selectedClValue.replace(/\s+/g, '');
            $(this).find('.cl-cntnr__item').each(function() {
                var getEachClValue = $(this).data('cl-code');
                currentClValue.push(getEachClValue);
            });

            var fCl = currentClValue.filter(function(v){return v!=='' && v!== undefined})
            if(selectedClValue != '') {
                var cmnValue = selectedClValue.filter(val => !fCl.includes(val));
                $.map(cmnValue, function(data) {
                    $('.cl-cntnr__list .cl-cntnr__item').each(function() {
                        var removeSpace = data.trim();
                        var CompareEachVal = $(this).data('cl-code');
                        if(removeSpace === CompareEachVal) {
                            $(this).find('span').find('form').find("input[type='checkbox']").prop('checked', true);
                        }
                    });
                });
            }
                     
        });
	}
	
	//Clicking on smart protect radio or hub tv radio button functionality on cartpage addon section
	$('.smartp--js input[name="smartProducts"], .addOnRadioInput input[name="hubTvaddon"]').click(function() {
		let getVal = '';
        if($(this).attr('previousValue') == 'true'){
			$(this).prop('checked', false);			
			if ($(this).parent().parent().hasClass('hubtv--js')){
        		$('input[name="hubTvaddon"]').attr('previousValue', false);
        		getVal = $(this).parents('.hubtv--js').data('hubtv-addon');
        	}
			else{
        		$('input[name="smartProducts"]').attr('previousValue', false);
        		getVal = $(this).parents('.smartp--js').data('smart-protect');
        	}
			$('#productCodePost_cart_remove').val(getVal);
            $("#addonRemoveSubmission").submit();
			
        } else {        	
        	if ($(this).parent().parent().hasClass('hubtv--js')){
        		$('input[name="hubTvaddon"]').attr('previousValue', false);
        		getVal = $(this).parents('.hubtv--js').data('hubtv-addon');
        	}
        	else{
        		$('input[name="smartProducts"]').attr('previousValue', false);
        		getVal = $(this).parents('.smartp--js').data('smart-protect');
        	}        				     	        			
			$('#productCodePost_cart').val(getVal);
            $("#addonSubmission").submit();
        }               
        $(this).attr('previousValue', $(this).is(':checked'));   
   
    });
}
$(document).ready(function(){
	addOnsSelection();
	//CartPage- On addon selection/deselection checkboxes
	$(".addAddonToPlanData").change(function(){               
        if(this.checked) {         
            $('#productCodePost_cart').val($(this).parent().parent().parent().attr("data-cl-code"));
            $("#addonSubmission").submit();
        }else{            
            $('#productCodePost_cart_remove').val($(this).parent().parent().parent().attr("data-cl-code"));
            $("#addonRemoveSubmission").submit();
        }        
    });

	//configurator sticky footer - show more and show less js
	$(".sticky-footer .view-less").on("click", function() {
		$(".category-content").slideUp();
		$(".view-less").addClass("hide");
		$(".view-more").removeClass("hide");
	});
	$(".sticky-footer .view-more").on("click", function() {
		$(".category-content").slideDown();
		$(".view-more").addClass("hide");
		$(".view-less").removeClass("hide");
	});
});
