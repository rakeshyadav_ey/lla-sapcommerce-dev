ACC.common = {
	currentCurrency: $("main").data('currencyIsoCode') || "USD",
	processingMessage: $("<img>").attr("src", ACC.config.commonResourcePath + "/images/spinner.gif"),


	blockFormAndShowProcessingMessage: function (submitButton)
	{
		var form = submitButton.parents('form:first');
		form.block({ message: ACC.common.processingMessage });
	},

	refreshScreenReaderBuffer: function ()
	{
		// changes a value in a hidden form field in order
		// to trigger a buffer update in a screen reader
		$('#accesibility_refreshScreenReaderBufferField').attr('value', new Date().getTime());
	},

	checkAuthenticationStatusBeforeAction: function (actionCallback)
	{
		$.ajax({
			url: ACC.config.authenticationStatusUrl,
			statusCode: {
				401: function () {
					location.href = ACC.config.loginUrl;
				}
			},
			dataType: 'json',
			success: function (data) {
				if (data == "authenticated") {
					actionCallback();
				}
			}
		});
	},

	encodeHtml: function (rawText)
	{
		return rawText.toString()
				.replace(/&/g, '&amp;')
				.replace(/</g, '&lt;')
				.replace(/>/g, '&gt;')
				.replace(/"/g, '&quot;')
				.replace(/'/g, '&#39;')
				.replace(/\//g, '&#47;');
	}
};





/* Extend jquery with a postJSON method */
jQuery.extend({
	postJSON: function (url, data, callback)
	{
		return jQuery.post(url, data, callback, "json");
	}
});

// add a CSRF request token to POST ajax request if its not available
$.ajaxPrefilter(function (options, originalOptions, jqXHR)
{
	// Modify options, control originalOptions, store jqXHR, etc
	if (options.type === "post" || options.type === "POST")
	{
		var noData = (typeof options.data === "undefined");
		if (noData)
		{
			options.data = "CSRFToken=" + ACC.config.CSRFToken;
		}
		else
		{
			var patt1 = /application\/json/i;
			if (options.data instanceof window.FormData)
			{
				options.data.append("CSRFToken", ACC.config.CSRFToken);
			}
			// if its a json post, then append CSRF to the header. 
			else if (patt1.test(options.contentType))
			{
				jqXHR.setRequestHeader('CSRFToken', ACC.config.CSRFToken);
			}
			else if (options.data.indexOf("CSRFToken") === -1)
			{
				options.data = options.data + "&" + "CSRFToken=" + ACC.config.CSRFToken;
			}
		}
		
	}
});

// login popup
$(document).on("click",".login-link",function(e) {
	e.preventDefault();
	var popupHeadLine = $(this.hash).prev(".headline").html();
	$.colorbox({
		inline:true,
		scrolling:true,
		maxWidth:"100%",
		width:"543px",
		href: $(this).attr("href"),
		close:'<span class="close-popup"></span>',
		title: popupHeadLine,
		onComplete: function() {
			$("#cboxTitle").css({
				"padding": "0"
			});
			$("#cboxLoadedContent").css({
				"margin-top": "50px"
			});
			ACC.common.refreshScreenReaderBuffer();
		},
		onClosed: function() {
			ACC.common.refreshScreenReaderBuffer();
		}
	});
	$(".login-popup-close").on("click", function() {
		$.colorbox.close();
	})
});

$(document).ready(function() {
    // Homepage Cta Box Price Design
    if($('body').hasClass('page-homepage')){
        $(".livemore-col").each(function(){
            var priceData = $(this).find(".cta-header .price").text();
            var floatPrice = priceData.split(".");
            var htmlData = floatPrice[0]+"<span>."+floatPrice[1]+"</span>";
            $(this).find(".cta-header .price").text("").html(htmlData);
        });
        $(".promotion-cta").parent().parent().parent().removeClass('container');
    }

	// footer js in mobile view
	$('.footer__link').each(function() {
		var currentElement = $(this);
		var footerLink = currentElement.find("a").text();
		if(footerLink == '') {
			currentElement.parent(".footer__nav--links").remove();
		}
		else {
			currentElement.parent(".footer__nav--links").siblings(".title").addClass("has-links upArrow")
		}
	});

	$(".footer__nav--container .title").on("click", function() {
	    if ($(window).width() < 767) {
		 event.preventDefault();		
		if ($(this).hasClass("upArrow")){
			$(this).removeClass("upArrow");
			$(this).addClass("downArrow");
		}
		else {
			$(this).removeClass("downArrow");
			$(this).addClass("upArrow");
		}	
		$(this).parent(".footer__nav--container").find(".footer__nav--links").slideToggle();
	    }
	});
});
