ACC.langcurrency = {

	_autoload: [
		"bindLangCurrencySelector"
	],

	bindLangCurrencySelector: function (){

		$(".js-languageSelector-desktop").on("change",$('#lang-selector') ,function(){
			$(".js-languageSelector-desktop").find('#lang-form').submit();
		});

		$(".branding-mobile").on("change",$('#lang-selector') ,function(){
			$(".branding-mobile").find('#lang-form').submit();
		});

        if ($(window).width() < 800) {
            $('#lang-selector option[value="en"]').text('en');
            $('#lang-selector option[value="es"]').text('es');
        }

		$('#currency-selector').change(function(){
			$('#currency-form').submit();
		});
	}
};