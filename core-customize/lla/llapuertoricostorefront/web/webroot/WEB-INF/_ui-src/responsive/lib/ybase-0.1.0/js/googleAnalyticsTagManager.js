googleAnalyticsTagManager = {		
    pageLoadGoogleAnalyticsData:function(){
        window.dataLayer = window.dataLayer || [];        
        var primaryCategory = $("#adobeAnalyticsPageInfo").attr('data-attr-pagename');
        if(primaryCategory == '' || primaryCategory == undefined){ primaryCategory = ''; }
        if(primaryCategory == 'SELECT_INTERNET_PLAN' || primaryCategory == 'SELECT_TV_PLAN' || primaryCategory == 'SELECT_PHONE' || primaryCategory == 'SELECT_ADDONS' ) {
            primaryCategory = 'Products';
        }
        var subCategory = "";
        if(primaryCategory == 'Products') { subCategory = 'Products'; }

        var breadCrumbs = $("#adobeAnalyticsPageInfo").attr('data-attr-breadcrumbsdata');
        if($('body').hasClass('page-orderConfirmationPage')){
            breadCrumbs = $("#adobeAnalyticsPageInfo").attr('data-attr-pageName');
        }
        if(breadCrumbs == '' || breadCrumbs == undefined || breadCrumbs == 'N/A'){ breadCrumbs = ''; } else { breadCrumbs = breadCrumbs.split(","); }

        if($('body').hasClass('page-cartPage') || $('body').hasClass('page-multiStepCheckoutSummaryPage') || $('body').hasClass('page-orderConfirmationPage')){
            primaryCategory = $("#adobeAnalyticsPageInfo").attr('data-attr-pagename');
        }

        var profileID = $("#adobeAnalyticsPageInfo").attr('data-attr-userid');
        var email = $("#adobeAnalyticsPageInfo").attr('data-attr-email');
        if($('body').hasClass('page-productDetailsForDevices') || $('body').hasClass('page-cartPage') || $('body').hasClass('page-multiStepCheckoutSummaryPage') || $('body').hasClass('page-orderConfirmationPage')){
            profileID = $("#adobeAnalyticsPageInfo").attr('data-attr-userid');
            email = $("#adobeAnalyticsPageInfo").attr('data-attr-email');
        }
        var statusLogged = 'Logged';
        var mail = "true";
        if(profileID == '' || profileID == undefined){ profileID = ''; statusLogged = 'Not Logged'; mail = "false"; }

        var encryptedEmail1 = '';
        if(email != '' && email != undefined && email != 'anonymous'){ encryptedEmail1 = sha256(email); }

        var pageType = '';
        if($("#adobeAnalyticsPageInfo").attr('data-attr-pagename') == 'HOME'){
            pageType = "Homepage";
            primaryCategory = 'Fixed';
            subCategory = 'Internet-TV-Telephone-PlanOnly';
        }
        if($('body').hasClass('page-internetplanspage')){
            pageType = 'PLP';
            primaryCategory = 'Fixed';
            subCategory = '1P-Internet';
        }else if($('body').hasClass('page-tvplanspage')) {
            pageType = 'PLP';
            primaryCategory = 'Fixed';
            subCategory = '1P-TV';
        }else if($('body').hasClass('page-telephoneplanspage')) {
            pageType = 'PLP';
            primaryCategory = 'Fixed';
            subCategory = '1P-Telephone';
        }else if($('body').hasClass('page-addonplanspage')) {
             pageType = 'PLP';
             primaryCategory = 'SVA';
             subCategory = '0P-AdditionalChannels';
         }else if($('body').hasClass('page-cartPage')) {
            pageType = 'Cart';
        }else if ($('body').hasClass('page-multiStepCheckoutSummaryPage')){
            pageType = 'Checkout';
        }else if ($('body').hasClass('page-orderConfirmationPage')){
            pageType = 'OrderConfirmation';
        }else if($('body').hasClass('page-storefinderPage')){
            pageType = 'Find Store';
        }
        else if($('body').hasClass('page-createcombinationpage')){
            pageType = 'Create Combination';
        }

        var productPage = "";
        if($('body').hasClass('page-productDetails') || $('body').hasClass('page-productDetailsForDevices')){
            productPage = $("#pdpAdobeAnalyticsData").attr('data-attr-name');
        }

        var pageName = window.location.pathname;
        if(pageName == '' || pageName == undefined){ pageName = ''; }

        var userType = $("#adobeAnalyticsPageInfo").attr('data-attr-customerType');
        if(userType == '' || userType == undefined){ userType = '';}

        var phone = $("#adobeAnalyticsPageInfo").attr('data-attr-mobilephone');
        var encryptedPhone = '';
        if(phone != '' && phone != undefined){ encryptedPhone = sha256(phone); }


        if(!$('body').hasClass('page-orderConfirmationPage')){
            let serviceabilityCheck = $('.Gtm_creditScore').attr("data-attr-serviceabilityCheck");
            if(serviceabilityCheck == '' || serviceabilityCheck == undefined) { serviceabilityCheck = ''; }
            if($('body').hasClass('page-multiStepCheckoutSummaryPage') && ($(".js-checkout-step.active").prop("id") == 'step2')){
                window.dataLayer.push({
                    'event': 'vpv',
                    'page': {
                        'pageName': pageName,
                        'breadCrumbs': breadCrumbs,
                        'pageType': pageType,
                        'country': 'Puerto Rico',
                        'channel': 'Ecommerce',
                        'productPage': productPage
                    },
                    'category':{
                        'businessCategory': 'Residential',
                        'primaryCategory': primaryCategory,
                        'subCategory': subCategory
                    },
                    'user':{
                        'statusLogged': statusLogged,//'logged status from the user', //Dynamic Value
                        'userType': userType,//'guest or client', //Dynamic Value
                        'profileInfo': {
                            'profileID': profileID,
                            'email': encryptedEmail1,
                            'phone': encryptedPhone
                        },
                        'social': {
                            'mail': mail,
                            'facebook': 'false',
                            'other': 'false'
                        },
                        'customerCheck': {
                            'serviceabilityCheck': serviceabilityCheck
                        }
                    },
                });
            }else if($('body').hasClass('page-multiStepCheckoutSummaryPage') && ($(".js-checkout-step.active").prop("id") == 'step3') || $('body').hasClass('page-multiStepCheckoutSummaryPage') && ($(".js-checkout-step.active").prop("id") == 'step4')  || $('body').hasClass('page-multiStepCheckoutSummaryPage') && ($(".place-order-form #placeOrder").length > 0) || $('body').hasClass('page-callBack') ||$('body').hasClass('page-error')){
            	    if($('body').hasClass('page-multiStepCheckoutSummaryPage') && ($(".place-order-form #placeOrder").length > 0)){
            	    	primaryCategory = pageType;
            	      }
            	let creditCheck = '';
            	 if($(".place-order-form #placeOrder").length > 0 ){
            		 creditCheck = sessionStorage.getItem("gtm_acceptCreditCheck");                    
            	 }                                 
            	 let isCustomer =  sessionStorage.getItem("gtm_isCustomer");
            	 let creditscore = '';
            	 let gtmData = $('.Gtm_creditScore'); 
            	 if($(".js-checkout-step.active").prop("id") == 'step3' || $(".js-checkout-step.active").prop("id") == 'step4'){
            		 sessionStorage.removeItem("gtm_acceptCreditCheck");                     
            	 }            	 
            	 if($('body').hasClass('page-error')){
            		pageType = 'Error';
            		primaryCategory = 'ERROR';              		
            	}   
            	 if($('body').hasClass('page-callBack')){
        		   if ($("#adobeAnalyticsPageInfo").attr('data-attr-pagename') == undefined || $("#adobeAnalyticsPageInfo").attr('data-attr-pagename') == null || $("#adobeAnalyticsPageInfo").attr('data-attr-pagename') == '' ){
        			   pageType = primaryCategory = breadCrumbs [1];
        		   }
             	}
            	 gtmData[0].attributes['data-attr-acceptsCreditCheck'].value = creditCheck;
            	 gtmData[0].attributes['data-attr-isCustomer'].value = isCustomer;
                 creditscore = gtmData.attr("data-attr-minScoreRange") | gtmData.attr("data-attr-minScoreRange") | gtmData.attr("data-attr-letterMapping");
                 creditscore = creditscore == undefined || creditscore == null ? '' : creditscore;
            	
            	window.dataLayer.push({
                    'event': 'vpv',
                    'page': {
                        'pageName': pageName,
                        'breadCrumbs': breadCrumbs,
                        'pageType': pageType,
                        'country': 'Puerto Rico',
                        'channel': 'Ecommerce',
                        'productPage': productPage
                    },
                    'category':{
                        'businessCategory': 'Residential',
                        'primaryCategory': primaryCategory,
                        'subCategory': subCategory
                    },
                    'user':{
                        'statusLogged': statusLogged,//'logged status from the user', //Dynamic Value
                        'userType': userType,//'guest or client', //Dynamic Value
                        'profileInfo': {
                            'profileID': profileID,
                            'email': encryptedEmail1,
                            'phone': encryptedPhone
                        },
                        'social': {
                            'mail': mail,
                            'facebook': 'false',
                            'other': 'false'
                        },
                        'customerCheck': {
                            'isCustomer': isCustomer == "true" ? true : isCustomer == "false" ? false : '',
                            'debtCheck': gtmData.attr("data-attr-debtCheckFlag") == "true" ? true : gtmData.attr("data-attr-debtCheckFlag") == "false" ? false : '',
                            'acceptsCreditCheck': creditCheck == "true" ? true : creditCheck == "false" ? false : '',
                            'creditScore':creditscore,
                            'serviceabilityCheck': serviceabilityCheck
                        }
                    },
                 });
            }
            else{   
            	 if($(".js-checkout-step.active").prop("id") == 'step1' || $(".js-checkout-step.active").prop("id") == 'step2'){
         	    	sessionStorage.removeItem("gtm_acceptCreditCheck");
         		    sessionStorage.removeItem("gtm_isCustomer");
         	      }
            	window.dataLayer.push({
                    'event': 'vpv',
                    'page': {
                        'pageName': pageName,
                        'breadCrumbs': breadCrumbs,
                        'pageType': pageType,
                        'country': 'Puerto Rico',
                        'channel': 'Ecommerce',
                        'productPage': productPage
                    },
                    'category':{
                        'businessCategory': 'Residential',
                        'primaryCategory': primaryCategory,
                        'subCategory': subCategory
                    },
                    'user':{
                        'statusLogged': statusLogged,//'logged status from the user', //Dynamic Value
                        'userType': userType,//'guest or client', //Dynamic Value
                        'profileInfo': {
                            'profileID': profileID,
                            'email': encryptedEmail1,
                            'phone': encryptedPhone
                        },
                        'social': {
                            'mail': mail,
                            'facebook': 'false',
                            'other': 'false'
                        }
                    },
                 });
            }
         }

        /* Order Confirmation Page Starts */
        if($('body').hasClass('page-orderConfirmationPage')){
            var productsOrderConfirmationPage = [];
            let creditCheck = sessionStorage.getItem("gtm_acceptCreditCheck");
       	    let isCustomer =  sessionStorage.getItem("gtm_isCustomer");
      		sessionStorage.removeItem("gtm_acceptCreditCheck");
            sessionStorage.removeItem("gtm_isCustomer"); 
            let creditscore = '';
            let gtmData = $('.Gtm_creditScore');            	             	           
            if($('body').hasClass('page-error')){
       		pageType = 'Error';
       		primaryCategory = 'ERROR';  
       		}             
            gtmData[0].attributes['data-attr-acceptsCreditCheck'].value = creditCheck;
       	    gtmData[0].attributes['data-attr-isCustomer'].value = isCustomer; 
            creditscore = gtmData.attr("data-attr-minScoreRange") | gtmData.attr("data-attr-minScoreRange") | gtmData.attr("data-attr-letterMapping");
            creditscore = creditscore == undefined || creditscore == null ? '' : creditscore; 
            $(".checkoutProducts").each(function(){
                var primaryCategory = $(this).attr('data-attr-primarycategory');
                if(primaryCategory != '' && primaryCategory != undefined && primaryCategory == 'ALL Products') {
                        primaryCategory = 'Fixed';
                }else if(primaryCategory != '' && primaryCategory != undefined && primaryCategory == 'Addon') {
                     primaryCategory = 'SVA';
                }else {
                    primaryCategory = '';
                }
                var subcategory = "1P-"+$(this).attr('data-attr-category');
                if($(this).attr('data-attr-id') == 'smart_protect_3' || $(this).attr('data-attr-id') == 'smart_protect_5'){
                    subcategory = '1P-Smartprotect';
                }
                var subCat = $(this).attr('data-attr-category');
                if(subCat == 'Internet'|| subCat == 'TV' || subCat == 'Telephone'){
                    primaryCategory = 'Fixed';
                }
                productsOrderConfirmationPage.push({
                    'name' : $(this).attr('data-attr-name'),
                    'id' : $(this).attr('data-attr-id'),
                    'price' : $(this).attr('data-attr-price').replace(/,/g, ""),
                    'brand' : 'Liberty PR',
                    'category' : "Residential/"+primaryCategory+"/"+subcategory,
                    'variant': '',
                    'quantity' : $(this).attr('data-attr-quantity'),
                });
            });

            var actionField =  {
                'id': $("#addressInfo").attr('data-attr-order-id'), // Transaction ID.
                 'affiliation': 'Puerto Rico Ecommerce',
                 'revenue': $("#cartPagePriceInfo").attr('data-attr-carttotal'), // Total transaction value
                 'tax': "", //$("#orderConfirmationPagePriceInfo").attr('data-attr-taxrate').replace(/,/g, ""), //Dynamic Value Ex. 4.90
                 'shipping': '', //Dynamic Value Ex. 1.90
                 'coupon': '' //Dynamic Value Ex. 0.50
           };
           var address = [];
           address.province = ''; //dynamic value
           address.district = ""; //dynamic value
           address.town = $("#addressInfo").attr('data-attr-town'); //dynamic value
           address.neighbourhood = "" //dynamic value
           address.street = ''; //dynamic value
           address.postalCode = $("#addressInfo").attr('data-attr-postalCode'); //dynamic value
           address.country = $("#addressInfo").attr('data-attr-country'); //dynamic value

           let serviceabilityCheck = $('.Gtm_creditScore').attr("data-attr-serviceabilityCheck");
           if(serviceabilityCheck == '' || serviceabilityCheck == undefined) { serviceabilityCheck = ''; }

            window.dataLayer.push ({
                'event': 'vpv',
                'page': {
                    'pageName': pageName,
                    'breadCrumbs': breadCrumbs,
                    'pageType': pageType,
                    'country': 'Puerto Rico',
                    'channel': 'Ecommerce',
                    'productPage': productPage
                },
                'category':{
                    'businessCategory': 'Residential',
                    'primaryCategory': primaryCategory,
                    'subCategory': subCategory
                },
                'user':{
                    'statusLogged': statusLogged,//'logged status from the user', //Dynamic Value
                    'userType': userType,//'guest or client', //Dynamic Value
                    'profileInfo': {
                        'profileID': profileID,
                        'email': encryptedEmail1,
                        'phone': encryptedPhone
                    },
                    'social': {
                        'mail': mail,
                        'facebook': 'false',
                        'other': 'false'
                    },
                    'customerCheck': {
                        'isCustomer': isCustomer == "true" ? true : isCustomer == "false" ? false : '',
                        'debtCheck': gtmData.attr("data-attr-debtCheckFlag") == "true" ? true : gtmData.attr("data-attr-debtCheckFlag") == "false" ? false : '',
                        'acceptsCreditCheck': creditCheck == "true" ? true : creditCheck == "false" ? false : '',
                        'creditScore':creditscore,
                        'serviceabilityCheck': serviceabilityCheck
                    }
                },
                'address': address,
                'ecommerce': {
                    'currencyCode': $("main").attr('data-currency-iso-code'),
                    'purchase': {
                        'actionField': actionField,
                        'products': productsOrderConfirmationPage
                    }
                }
            });
        }
        /* Order Confirmation Page Ends */

        /* Chekcout Page Starts */
        if($('body').hasClass('page-multiStepCheckoutSummaryPage')){
            var actionField = {};
            var stepInfo = '', option='';
            var step = $(".js-checkout-step.active").prop("id");
            if(step == 'step1') {
                stepInfo = 1;
                option="Address";
            }else if(step == 'step2') {
                stepInfo = 2;
                option=" Personal Information";
            }else if(step == 'step3') {
                stepInfo = 3;
                option="Contract";
            }else if(step == 'step4') {
                stepInfo = 4;
                option="Credit Check";
            }else if(($(".place-order-form #placeOrder").length > 0)) {
                stepInfo = 5;
                option="Detail";
            }
            var products = [];
            $(".checkoutProducts").each(function(){
                var primaryCategory = $(this).attr('data-attr-primarycategory');
                if(primaryCategory != '' && primaryCategory != undefined && primaryCategory == 'ALL Products') {
                        primaryCategory = 'Fixed';
                }else if(primaryCategory != '' && primaryCategory != undefined && primaryCategory == 'Addon') {
                     primaryCategory = 'SVA';
                }else {
                    primaryCategory = '';
                }
                var subcategory = "1P-"+$(this).attr('data-attr-category');
                var subCat = $(this).attr('data-attr-category');	
                if(subCat == 'Internet'|| subCat == 'TV' || subCat == 'Telephone'){	
                    primaryCategory = 'Fixed';	
                }
                if($(this).attr('data-attr-id') == 'smart_protect_3' || $(this).attr('data-attr-id') == 'smart_protect_5'){
                    subcategory = '1P-Smartprotect';
                }
                products.push({
                    'name' : $(this).attr('data-attr-name'),
                    'id' : $(this).attr('data-attr-id'),
                    'price' : $(this).attr('data-attr-price').replace(/,/g, ""),
                    'brand' : 'Liberty PR',
                    'category' : "Residential/"+primaryCategory+"/"+subcategory,
                    'variant': '',
                    'quantity' : $(this).attr('data-attr-quantity')
                });
            });
            window.dataLayer.push({
                'event': 'checkout',
                'ecommerce': {
                    'checkout': {
                        'actionField': {'step': stepInfo, 'option': option},
                        'products': products
                    }
                }
            });
        }
        /* Chekcout Page Ends */


        /* Page not Found Code Starts*/
        if($('body').hasClass('page-notFound')){
            var errorMsg = $("#gtmNotFoundPageError").val();
            window.dataLayer.push({
                'event': 'error',
                'description': errorMsg
            });
        }
        /* Page not Found Code Ends*/
        $(".global-alerts .alert-danger").each(function (){
            var errorMsg = $(this).text();
            errorMsg = errorMsg.replace( /[\r\n\t]+/gm, "" ).substring(1);
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                'event': 'error',
                'description': errorMsg
            });
        });
        
        /*Call Back*/
        if($('body').hasClass('page-callBack') || $('body').hasClass('page-error')){ 
        	
         if($('body').hasClass('page-callBack')){
        	 var productsCallBack = [];
             $(".gtm_callback_cartProductsdata").each(function(){
                 var primaryCategory = $(this).attr('data-attr-primarycategory');
                 if(primaryCategory != '' && primaryCategory != undefined && primaryCategory == 'ALL Products') {
                         primaryCategory = 'Fixed';
                 }else if(primaryCategory != '' && primaryCategory != undefined && primaryCategory == 'Addon') {
                      primaryCategory = 'SVA';
                 }else {
                     primaryCategory = '';
                 }
                 var subcategory = "1P-"+$(this).attr('data-attr-category');
                 if($(this).attr('data-attr-id') == 'smart_protect_3' || $(this).attr('data-attr-id') == 'smart_protect_5'){
                     subcategory = '1P-Smartprotect';
                 }
                 var subCat = $(this).attr('data-attr-category');
                 if(subCat == 'Internet'|| subCat == 'TV' || subCat == 'Telephone'){
                     primaryCategory = 'Fixed';
                 }
                 productsCallBack.push({
                     'name' : $(this).attr('data-attr-name'),
                     'id' : $(this).attr('data-attr-id'),
                     'price' : $(this).attr('data-attr-price').replace(/,/g, ""),
                     'brand' : 'Liberty PR',
                     'category' : "Residential/"+primaryCategory+"/"+subcategory,
                     'variant': '',
                     'quantity' : $(this).attr('data-attr-quantity'),
                 });
             });

             var actionField =  {
                  'id': $("#addressInfo").attr('data-attr-order-id') || $('input[name="gtm_cartCode"]').val(), // Transaction ID.
                  'affiliation': 'Puerto Rico Ecommerce',
                  'revenue': $("#cartPagePriceInfo").attr('data-attr-carttotal')||$('input[name="gtm_cartTotalPriceWithTax"]').val(), // Total transaction value
                  'tax': $('input[name="gtm_cartTotalTax"]').val(), //$("#orderConfirmationPagePriceInfo").attr('data-attr-taxrate').replace(/,/g, ""), //Dynamic Value Ex. 4.90
                  'shipping': '', //Dynamic Value Ex. 1.90
                  'coupon': '' //Dynamic Value Ex. 0.50
            };
            var address = [];
            address.province = $('input[name="gtm_callback_address_line1"]').val(); //dynamic value
            address.district = $('input[name="gtm_callback_address_district"]').val(); //dynamic value
            address.town = $("#addressInfo").attr('data-attr-town')||$('input[name="gtm_callback_address_town"]').val(); //dynamic value
            address.neighbourhood = $('input[name="gtm_callback_address_neighbourhood"]').val(); //dynamic value
            address.street = $('input[name="gtm_callback_address_streetname"]').val(); //dynamic value
            address.country = $("#addressInfo").attr('data-attr-country')||$('input[name="gtm_callback_address_country"]').val(); //dynamic value

           
            window.dataLayer.push ({
                 'event': 'leadEcommerce',
                 'address': address,
                 'ecommerce': {
                     'currencyCode': $("main").attr('data-currency-iso-code'),
                     'purchase': {
                         'actionField': actionField,
                         'products': productsCallBack
                     }
                 }
             });   
            } 
         
         if($('input[name="gtm_callback_falloutReason"]').val()!=null && $('input[name="gtm_callback_falloutReason"]').val() != ""){            	 
                 var errorMsg = $('input[name="gtm_callback_falloutReason"]').val();
                 window.dataLayer.push({
                     'event': 'error',
                     'description': errorMsg
                 });
             }
             
        }
    },   
    
    loginFormGtmError: function (){
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
           'event': 'error',
           'description': 'User entered invalid email address or phone number on cart page login popup'
        });
    },
    addressFormGTMError: function(){	
        window.dataLayer = window.dataLayer || [];	
        window.dataLayer.push({	
          'event': 'error',	
          'description': 'User is trying to move to the next step without filling installation address form correctly'	
        });
    },
    handleGtmErrors:function(error){
    	window.dataLayer = window.dataLayer || [];	
        window.dataLayer.push({	
          'event': 'error',	
          'description': error
        });
    }    
}

$(window).on('load', function(){
    let gtmCallInterval = setInterval(function(){
        // when window.google_tag_manager is defined and true, stop the function
        if (!!window.google_tag_manager) {
            clearInterval(gtmCallInterval);
            googleAnalyticsTagManager.pageLoadGoogleAnalyticsData();
        }
    }, 1000);
});

$(document).ready(function(){
    //Add to cart, Homepage - Best Selling Plans
    $(document).on("click", ".preConf-plans", function(e) {
        var fetchData = $(this).find(".cta-btn-block").find('.btn-block');
        var products = {};
        var primaryCategory = 'Fixed';
        var subcategory = fetchData.attr('data-attr-name') =='Internet'?'1P-Internet':'3P-Internet-TV-Telephone';
        products = ({
            'name' : fetchData.attr('data-attr-name'),
            'id' : fetchData.attr('data-attr-id'),
            'price' : fetchData.attr('data-attr-price').replace(/[,$]/g, ""),
            'brand': 'Liberty PR',
            'category': "Residential/"+primaryCategory+"/"+subcategory,
            'variant': '',
            'quantity': 1
        });
        window.dataLayer.push({
            'event': 'addToCart',
            'ecommerce': {
                'currencyCode': fetchData.attr('data-attr-currency'),
                'add': {
                    'products': [products]
                }
            }
        });
    });

    //PLP - Add to cart
    $(document).on("click",".ctabox-want-form .ctabox-btn .btn",function(e){
    	//to remove the selected products-if any
    	$('.ctabox-container .ctabox.selected').each(function (){
    		let removePrd = $(this).find('.ctabox-want-form .ctabox-btn .btn');
            removeProductFromPLP(removePrd); 
    	});
    	
        var products = {};
        var primaryCategory = 'Fixed';
       
        var subcategory = $(this).attr('data-attr-category');
        if(subcategory == '' && subcategory == undefined) { subcategory = ""; }
        products = ({
            'name' : $(this).attr('data-attr-name'),
            'id' : $(this).attr('data-attr-id'),
            'price' : $(this).attr('data-attr-price').replace(/,/g, ""),
            'brand': 'Liberty PR',
            'category': "Residential/"+primaryCategory+"/1P-"+subcategory,
            'variant': '',
            'quantity': 1
        });
        window.dataLayer.push({
            'event': 'addToCart',
            'ecommerce': {
                'currencyCode': $(this).attr('data-attr-currency'),
                'add': {
                    'products': [products]
                }
            }
        });
    });
    //Remove from cart, PLP
    $(document).on("click",".ctabox-dlt-form .ctabox-btn .btn",function(e){     
        var fetchData = $(this).parent().parent().parent().find('.ctabox-want-form .ctabox-btn .btn');
        removeProductFromPLP(fetchData);       
    });

   //cart addons- add or removal- channels check boxes
    $(".addAddonToPlanData").change(function(){       	
    	addRemoveCartAddons($(this));
    });
    //cart addons- add or removal- radio 
   $('.addOnRadioInput input[name="hubTvaddon"]').click(function() {    	
    	addRemoveCartAddons($(this));
    });
    
  //Remove or change - products or plan -  from cart page  
   $(".deleteForm .submitRemoveBundle, #cartItemRemoval-popup .cartRemoveProducts").click( function(e){   
        var removedProducts = [];      
        $(".cartProducts").each(function(){
            var primaryCategory = $(this).attr('data-attr-primarycategory');
            if(primaryCategory != '' && primaryCategory != undefined && primaryCategory == 'ALL Products') {
                    primaryCategory = 'Fixed';
            }else if(primaryCategory != '' && primaryCategory != undefined && primaryCategory == 'Addon') {
                 primaryCategory = 'SVA';
            }else {
                primaryCategory = '';
            }
            var subcategory = $(this).attr('data-attr-category');
            if($(this).attr('data-attr-id') == 'smart_protect_3' || $(this).attr('data-attr-id') == 'smart_protect_5'){
                subcategory = 'Smartprotect';
            }
            if(subcategory == '' && subcategory == undefined) { subcategory = ""; }
            removedProducts.push({
                'name' : $(this).attr('data-attr-name'),
                'id' : $(this).attr('data-attr-id'),
                'price' : $(this).attr('data-attr-price').replace(/,/g, ""),
                'brand': 'Liberty PR',
                'category': "Residential/"+primaryCategory+"/1P-"+subcategory,
                'variant': '',
                'quantity': 1
            });
        });
        window.dataLayer.push({
            'event': 'removeFromCart',
            'ecommerce': {
                'currencyCode': $("main").attr('data-currency-iso-code'),
                'remove': {
                    'products': removedProducts
                }
            }
        });
    });
    
   //Remove addon from cart page
    $(document).on("click", ".addonChannel form .removeAddonBtn", function(e){
        var products = {};
       var subcategory = $(this).attr('data-attr-category');
       if($(this).attr('data-attr-id') == 'smart_protect_3' || $(this).attr('data-attr-id') == 'smart_protect_5'){
           subcategory = 'Smartprotect';
       }
        if(subcategory == '' && subcategory == undefined) { subcategory = ""; }
        products = ({
            'name' : $(this).attr('data-attr-name'),
            'id' : $(this).attr('data-attr-id'),
            'price' : $(this).attr('data-attr-price').replace(/,/g, ""),
            'brand': 'Liberty PR',
            'category': "Residential/SVA/1P-"+subcategory,
            'variant': '',
            'quantity': $(this).attr('data-attr-quantity'),
        });
        window.dataLayer.push({
            'event': 'removeFromCart',
            'ecommerce': {
                'currencyCode': $("main").attr('data-currency-iso-code'),
                'remove': {
                    'products': [products]
                }
            }
        });
    });     

   //Cart - Smart Protect Addon Add or Remove 
    $('.smartp--js input[name="smartProducts"]').click(function() {       
    	var prevSelected = '';
        var currentSelected = $(this).attr('data-attr-code');//id
        $('.cartProducts').each(function () {
        	let cartId = $(this).attr('data-attr-id');
        	 $('.smartp--js').each(function() {
        		 if ($(this).attr('data-smart-protect') == cartId){
        			 prevSelected = $(this).attr('data-smart-protect');
        		 }
        	 });
        });
        if( prevSelected == undefined || prevSelected == '') {
            addToCartAddon(currentSelected);
        }else if( prevSelected == currentSelected) {           
            removeFromCartAddon(currentSelected);
        }else{
            removeFromCartAddon(prevSelected);
            addToCartAddon(currentSelected);
        }
    });    
});

//Cart - Smart Protect Addon Add
function addToCartAddon(id){
    var products = {};
    $(".addSmartAddonToPlanGTM").each(function(){
        if(id == $(this).attr('data-attr-id')){
             var fetchData = $(this);
             var primaryCategory = fetchData.attr('data-attr-primarycategory');
             if(primaryCategory != '' && primaryCategory != undefined && primaryCategory == 'Addon') {
                 primaryCategory = 'SVA';
             }else {
                 primaryCategory = '';
             }
             var subcategory = fetchData.attr('data-attr-category');
             if(subcategory == '' && subcategory == undefined) { subcategory = "NA"; }
             products = ({
                 'name' : fetchData.attr('data-attr-name'),
                 'id' : fetchData.attr('data-attr-id'),
                 'price' : fetchData.attr('data-attr-price').replace(/,/g, ""),
                 'brand': 'Liberty PR',
                 'category': "Residential/"+primaryCategory+"/1P-Addon",
                 'variant': '',
                 'quantity': 1
             });
         }
     });
    window.dataLayer.push({
        'event': 'addToCart',
        'ecommerce': {
            'currencyCode': $("main").attr('data-currency-iso-code'),
            'add': {
                'products': [products]
            }
        }
    });
}

// Cart - Smart Protect Addon Remove
function removeFromCartAddon(id){
    var products = {};
    $(".addSmartAddonToPlanGTM").each(function(){
        if(id == $(this).attr('data-attr-id')){
             var fetchData = $(this);
             var primaryCategory = fetchData.attr('data-attr-primarycategory');
             if(primaryCategory != '' && primaryCategory != undefined && primaryCategory == 'Addon') {
                 primaryCategory = 'SVA';
             }else {
                 primaryCategory = '';
             }
             var subcategory = fetchData.attr('data-attr-category');
             if(subcategory == '' && subcategory == undefined) { subcategory = ""; }
             products = ({
                 'name' : fetchData.attr('data-attr-name'),
                 'id' : fetchData.attr('data-attr-id'),
                 'price' : fetchData.attr('data-attr-price').replace(/,/g, ""),
                 'brand': 'Liberty PR',
                 'category': "Residential/"+primaryCategory+"/1P-Addon",
                 'variant': '',
                 'quantity': 1
             });
         }
     });
    window.dataLayer.push({
        'event': 'removeFromCart',
        'ecommerce': {
            'currencyCode': $("main").attr('data-currency-iso-code'),
            'remove': {
                'products': [products]
            }
        }
    });
}

//Cart - Addons add or remove -Channels Check boxes and Radios
function addRemoveCartAddons(thisaddon){
    var fetchData = thisaddon.parent().parent().parent().find('.addAddonToPlanGTM');
    var products = {};
    var primaryCategory = fetchData.attr('data-attr-primarycategory');
    if(primaryCategory != '' && primaryCategory != undefined && primaryCategory == 'Addon') {
        primaryCategory = 'SVA';
    }else {
        primaryCategory = '';
    }
    var subcategory = fetchData.attr('data-attr-category');
    if(subcategory == '' && subcategory == undefined) { subcategory = ""; }
    products = ({
        'name' : fetchData.attr('data-attr-name'),
        'id' : fetchData.attr('data-attr-id'),
        'price' : fetchData.attr('data-attr-price').replace(/,/g, ""),
        'brand': 'Liberty PR',
        'category': "Residential/"+primaryCategory+"/1P-"+subcategory,
        'variant': '',
        'quantity': 1
    });

    if(thisaddon[0].checked) {
        window.dataLayer.push({
            'event': 'addToCart',
            'ecommerce': {
                'currencyCode': fetchData.attr('data-attr-currency'),
                'add': {
                    'products': [products]
                }
            }
        });
    }else{
        window.dataLayer.push({
            'event': 'removeFromCart',
            'ecommerce': {
                'currencyCode': fetchData.attr('data-attr-currency'),
                'remove': {
                    'products': [products]
                }
            }
        });
    }
}

//PLP-Remove Product
function removeProductFromPLP(removePLPProd){	
        var products = {};
        var primaryCategory = 'Fixed';
        
        var subcategory = removePLPProd[0].attributes['data-attr-category'].value;
        if(subcategory == '' && subcategory == undefined) { subcategory = ""; }
        products = ({
            'name' : removePLPProd[0].attributes['data-attr-name'].value,
            'id' : removePLPProd[0].attributes['data-attr-id'].value,
            'price' : removePLPProd[0].attributes['data-attr-price'].value.replace(/,/g, ""),
            'brand': 'Liberty PR',
            'category': "Residential/"+primaryCategory+"/1P-"+subcategory,
            'variant': '',
            'quantity': 1
        });
        window.dataLayer.push({
            'event': 'removeFromCart',
            'ecommerce': {
                'currencyCode': removePLPProd[0].attributes['data-attr-currency'].value,
                'remove': {
                    'products': [products]
                }
            }
        });   
}

function sha256(ascii) {
	function rightRotate(value, amount) {
		return (value>>>amount) | (value<<(32 - amount));
	};

	var mathPow = Math.pow;
	var maxWord = mathPow(2, 32);
	var lengthProperty = 'length'
	var i, j; // Used as a counter across the whole file
	var result = ''

	var words = [];
	var asciiBitLength = ascii[lengthProperty]*8;

	//* caching results is optional - remove/add slash from front of this line to toggle
	// Initial hash value: first 32 bits of the fractional parts of the square roots of the first 8 primes
	// (we actually calculate the first 64, but extra values are just ignored)
	var hash = sha256.h = sha256.h || [];
	// Round constants: first 32 bits of the fractional parts of the cube roots of the first 64 primes
	var k = sha256.k = sha256.k || [];
	var primeCounter = k[lengthProperty];

	var isComposite = {};
	for (var candidate = 2; primeCounter < 64; candidate++) {
		if (!isComposite[candidate]) {
			for (i = 0; i < 313; i += candidate) {
				isComposite[i] = candidate;
			}
			hash[primeCounter] = (mathPow(candidate, .5)*maxWord)|0;
			k[primeCounter++] = (mathPow(candidate, 1/3)*maxWord)|0;
		}
	}

	ascii += '\x80' // Append Ƈ' bit (plus zero padding)
	while (ascii[lengthProperty]%64 - 56) ascii += '\x00' // More zero padding
	for (i = 0; i < ascii[lengthProperty]; i++) {
		j = ascii.charCodeAt(i);
		if (j>>8) return; // ASCII check: only accept characters in range 0-255
		words[i>>2] |= j << ((3 - i)%4)*8;
	}
	words[words[lengthProperty]] = ((asciiBitLength/maxWord)|0);
	words[words[lengthProperty]] = (asciiBitLength)

	// process each chunk
	for (j = 0; j < words[lengthProperty];) {
		var w = words.slice(j, j += 16); // The message is expanded into 64 words as part of the iteration
		var oldHash = hash;
		// This is now the undefinedworking hash", often labelled as variables a...g
		// (we have to truncate as well, otherwise extra entries at the end accumulate
		hash = hash.slice(0, 8);

		for (i = 0; i < 64; i++) {
			var i2 = i + j;
			// Expand the message into 64 words
			// Used below if
			var w15 = w[i - 15], w2 = w[i - 2];

			// Iterate
			var a = hash[0], e = hash[4];
			var temp1 = hash[7]
				+ (rightRotate(e, 6) ^ rightRotate(e, 11) ^ rightRotate(e, 25)) // S1
				+ ((e&hash[5])^((~e)&hash[6])) // ch
				+ k[i]
				// Expand the message schedule if needed
				+ (w[i] = (i < 16) ? w[i] : (
						w[i - 16]
						+ (rightRotate(w15, 7) ^ rightRotate(w15, 18) ^ (w15>>>3)) // s0
						+ w[i - 7]
						+ (rightRotate(w2, 17) ^ rightRotate(w2, 19) ^ (w2>>>10)) // s1
					)|0
				);
			// This is only used once, so *could* be moved below, but it only saves 4 bytes and makes things unreadble
			var temp2 = (rightRotate(a, 2) ^ rightRotate(a, 13) ^ rightRotate(a, 22)) // S0
				+ ((a&hash[1])^(a&hash[2])^(hash[1]&hash[2])); // maj

			hash = [(temp1 + temp2)|0].concat(hash); // We don't bother trimming off the extra ones, they're harmless as long as we're truncating when we do the slice()
			hash[4] = (hash[4] + temp1)|0;
		}

		for (i = 0; i < 8; i++) {
			hash[i] = (hash[i] + oldHash[i])|0;
		}
	}

	for (i = 0; i < 8; i++) {
		for (j = 3; j + 1; j--) {
			var b = (hash[i]>>(j*8))&255;
			result += ((b < 16) ? 0 : '') + b.toString(16);
		}
	}
	return result;
}; 