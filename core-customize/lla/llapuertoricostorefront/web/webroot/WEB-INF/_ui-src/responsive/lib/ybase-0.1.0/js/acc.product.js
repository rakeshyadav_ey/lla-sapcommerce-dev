ACC.product = {

    _autoload: [
        "bindToAddToCartForm",
        "enableStorePickupButton",
        "enableVariantSelectors",
        "bindFacets",
        "bindTabLogic"
    ],

    bindTabLogic: function () {
        $(document).on("click", ".js-tab-head", function (e) {
            e.preventDefault();
            var _this = $(this);
            var tabId = _this.attr('data-content-id');
            $('.js-tab-head').each(function () {
                var _loopthis = $(this);
                _loopthis.removeClass('active');
            });
            _this.addClass('active');
            if($('.js-tab-head[data-content-id="recommendedPlans"]').hasClass("active")) {
               $("#plans-headline").show();
               $("#combination-headline").hide();
            }
            if($('.js-tab-head[data-content-id="createCombination"]').hasClass("active")) {
                $("#plans-headline").hide();
               $("#combination-headline").show();
            }
            $('.tab-content-wrapper').each(function () {
                var _loopthis = $(this);
                _loopthis.addClass('display-none');
            });
            $('#' + tabId).removeClass('display-none');
            sessionStorage.setItem("residential-category", tabId);
        });
        // Enable button only if atleast one category is selected
        $("#createCombinatioForm").on("change", ".custom-label", function() {
            $('.configuratorAlert').addClass('hide');
            $('.configuratorAlert > div').addClass('hide');
            if($('#internet').is(':checked') && $("#tv").is(':checked')) {
                $('#phone').prop('checked', true);
            }
            if($('#createCombinatioForm input:checked').length > 0) {
                $(".js-create-comb-button").removeAttr("disabled");
            }
            else {
                $(".js-create-comb-button").attr("disabled", "true");
            }
            if($('#internet').is(':checked') && $("#phone").is(':checked') && $("#tv").is(':checked')) {
                $('.configuratorAlert').removeClass('hide');
                $("#internet_TV_telephone").removeClass('hide');
				$(".phone-text-info").show();
				$(".phone-selection-info").addClass('hide');
            }
            if($('#internet').is(':checked') && $("#phone").is(':checked') && !$("#tv").is(':checked')) {
                $('.configuratorAlert').removeClass('hide');
                $("#internet_telephone").removeClass('hide');
				$(".phone-text-info").hide();
				$(".phone-selection-info").removeClass('hide');
            }
            if($('#internet').is(':checked') && !$("#phone").is(':checked') && $("#tv").is(':checked')) {
                $('.configuratorAlert').removeClass('hide');
                $("#internet_TV").removeClass('hide');
				$(".phone-text-info").show();
				$(".phone-selection-info").addClass('hide');
            }
            if(!$('#internet').is(':checked') && $("#phone").is(':checked') && $("#tv").is(':checked')) {
                $('.configuratorAlert').removeClass('hide');
                $("#TV_telephone").removeClass('hide');
				$(".phone-text-info").hide();
				$(".phone-selection-info").removeClass('hide');
            }
            if($('#createCombinatioForm input:checked').length == 1) {
                $('.configuratorAlert').removeClass('hide');
                $("#onlyOne_selected").removeClass('hide');
            }
            if($('#createCombinatioForm input:checked').length == 0) {
                $('.configuratorAlert').removeClass('hide');
                $("#nothing_selected").removeClass('hide');
            }
        });
        
        if($('#createCombinatioForm input:checked').length > 0) {
            $(".js-create-comb-button").removeAttr("disabled");
        }
        
    },

    bindFacets: function () {
        $(document).on("click", ".js-show-facets", function (e) {
            e.preventDefault();
            var selectRefinementsTitle = $(this).data("selectRefinementsTitle");
            var colorBoxTitleHtml = ACC.common.encodeHtml(selectRefinementsTitle);
            ACC.colorbox.open(colorBoxTitleHtml, {
                href: ".js-product-facet",
                inline: true,
                width: "480px",
                onComplete: function () {
                    $(document).on("click", ".js-product-facet .js-facet-name", function (e) {
                        e.preventDefault();
                        $(".js-product-facet  .js-facet").removeClass("active");
                        $(this).parents(".js-facet").addClass("active");
                        $.colorbox.resize()
                    })
                },
                onClosed: function () {
                    $(document).off("click", ".js-product-facet .js-facet-name");
                }
            });
        });
        enquire.register("screen and (min-width:" + screenSmMax + ")", function () {
            $("#cboxClose").click();
        });
    },


    enableAddToCartButton: function () {
        $('.js-enable-btn').each(function () {
            if (!($(this).hasClass('outOfStock') || $(this).hasClass('out-of-stock'))) {
                $(this).prop("disabled", false);
            }
        });
    },

    enableVariantSelectors: function () {
        $('.variant-select').prop("disabled", false);
    },

    bindToAddToCartForm: function () {
        var addToCartForm = $('.add_to_cart_form');
        addToCartForm.ajaxForm({
        	beforeSubmit:ACC.product.showRequest,
        	success: ACC.product.displayAddToCartPopup
         });    
        setTimeout(function(){
        	$ajaxCallEvent  = true;
         }, 2000);
     },
     showRequest: function(arr, $form, options) {  
    	 if($ajaxCallEvent)
    		{
    		 $ajaxCallEvent = false;
    		 return true;
    		}   	
    	 return false;
 
    },

    bindToAddToCartStorePickUpForm: function () {
        var addToCartStorePickUpForm = $('#colorbox #add_to_cart_storepickup_form');
        addToCartStorePickUpForm.ajaxForm({success: ACC.product.displayAddToCartPopup});
    },

    enableStorePickupButton: function () {
        $('.js-pickup-in-store-button').prop("disabled", false);
    },

    displayAddToCartPopup: function (cartResult, statusText, xhr, formElement) {
    	$ajaxCallEvent=true;
        $('#addToCartLayer').remove();
        if (typeof ACC.minicart.updateMiniCartDisplay == 'function') {
            ACC.minicart.updateMiniCartDisplay();
        }
        var titleHeader = $('#addToCartTitle').html();

        ACC.colorbox.open(titleHeader, {
            html: cartResult.addToCartLayer,
            width: "460px"
        });

        var productCode = $('[name=productCodePost]', formElement).val();
        var quantityField = $('[name=qty]', formElement).val();

        var quantity = 1;
        if (quantityField != undefined) {
            quantity = quantityField;
        }

        var cartAnalyticsData = cartResult.cartAnalyticsData;

        var cartData = {
            "cartCode": cartAnalyticsData.cartCode,
            "productCode": productCode, "quantity": quantity,
            "productPrice": cartAnalyticsData.productPostPrice,
            "productName": cartAnalyticsData.productName
        };
        ACC.track.trackAddToCart(productCode, quantity, cartData);
    }
};

$(document).ready(function () {
	$ajaxCallEvent = true;
    ACC.product.enableAddToCartButton();
    $('.js-tab-head[data-content-id="recommendedPlans"]').on("click", function() {
        if (window.location.href.indexOf('?') > -1) {
            history.pushState('', document.title, window.location.pathname);
        }
    });
});
$(window).on('load', function(){
    $(".page-createcombinationpage .recomended-plans").show();
    if (window.location.href.indexOf('?') == -1) {
        var currentTab = sessionStorage.getItem("residential-category");
        if(currentTab == "createCombination") {
            $(".page-createcombinationpage .recomended-plans").show();
            $('.js-tab-head[data-content-id="createCombination"]').trigger("click");
        }
    }
});
/*For clickable-banner*/	
$(document).on("click", ".home-crt-combination", function(e) {        	
     var bannerHref= $(this).find(".home-banner-cta").find("a").attr('href');	
	 location.href = bannerHref;	
});
/*For Home page - on click of preconfigured plans*/	
/*$(document).on("click", ".preConf-plans", function(e) {
	 $(this).find(".cta-btn-block").find("form").submit();		
});*/
