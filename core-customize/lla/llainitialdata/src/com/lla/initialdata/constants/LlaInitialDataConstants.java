/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.initialdata.constants;

/**
 * Global class for all LlaInitialData constants.
 */
public final class LlaInitialDataConstants extends GeneratedLlaInitialDataConstants
{
	public static final String EXTENSIONNAME = "llainitialdata";

	private LlaInitialDataConstants()
	{
		//empty
	}
}
