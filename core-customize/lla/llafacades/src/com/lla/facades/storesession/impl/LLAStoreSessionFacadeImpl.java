package com.lla.facades.storesession.impl;

import com.lla.core.util.LLACommonUtil;
import com.lla.facades.storesession.LLAStoreSessionFacade;
import de.hybris.platform.commercefacades.storesession.data.LanguageData;
import de.hybris.platform.commercefacades.storesession.impl.DefaultStoreSessionFacade;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

public class LLAStoreSessionFacadeImpl extends DefaultStoreSessionFacade implements LLAStoreSessionFacade {

    @Autowired
    private LLACommonUtil llaCommonUtil;
    @Override
    public void initializeSession(final List<Locale> preferredLocales)
    {
        initializeSessionTaxGroup();
        initializeSessionLanguage(preferredLocales);
        initializeSessionCurrency();
    }



    protected void initializeSessionLanguage(final List<Locale> preferredLocales)
    {
        // Try to use the default language for the site
        final LanguageData defaultLanguage = getDefaultLanguage();
        if (defaultLanguage != null)
        {
            setCurrentLanguage(defaultLanguage.getIsocode());
        }
    }}
