package com.lla.facades.price.converters;

import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.pricing.services.TmaCommercePriceService;
import de.hybris.platform.commercefacades.product.converters.populator.ProductPricePopulator;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.lla.core.enums.DevicePriceType;
import com.lla.core.util.LLACommonUtil;

public class LLADevicePricePopulator<SOURCE extends ProductModel, TARGET extends ProductData>
		extends ProductPricePopulator<SOURCE, TARGET>
{
	private CommonI18NService i18NService;
	@Autowired
	private LLACommonUtil llaCommonUtil;

	@Override
	public void populate(final SOURCE source, final TARGET target)
	{
		if (!(source instanceof TmaProductOfferingModel))
		{
			return;
		}
		final TmaProductOfferingModel sourceProductOffering = (TmaProductOfferingModel) source;
		final List<PriceData> devicePriceList = new ArrayList();
		if (source instanceof TmaSimpleProductOfferingModel && isProductOfDeviceCategory(sourceProductOffering))
		{
			final Collection<PriceRowModel> devicePriceRowCollection = sourceProductOffering.getEurope1Prices();
			for (final PriceRowModel priceRow : devicePriceRowCollection)
			{
				final PriceData priceData = getPriceDataFactory().create(PriceDataType.BUY,
						BigDecimal.valueOf(priceRow.getPrice().doubleValue()), priceRow.getCurrency().getIsocode());
                     priceData.setPlanCode(priceRow.getPlanCode());
				if (null != priceRow.getPriceType())
				{
					priceData.setPlanType(priceRow.getPriceType().getCode());
				}
				else
				{
					priceData.setPlanType(DevicePriceType.valueOf("NOOFFER").getCode());
				}
				if (llaCommonUtil.isSiteVTR())
				{
					priceData.setPlanCode(priceRow.getPlanCode());
					priceData.setMobilePriceType(priceRow.getMobilePriceType());
				}
				devicePriceList.add(priceData);
			}
			target.setDevicePriceList(devicePriceList);
		}
	}

    private boolean isProductOfDeviceCategory(TmaProductOfferingModel sourceProductOffering) {
        return (llaCommonUtil.isSiteJamaica() || llaCommonUtil.isSitePanama()) || llaCommonUtil.isSiteVTR() && llaCommonUtil.containsFirstLevelCategoryWithCode(sourceProductOffering, "devices");
    }

	@Override
	protected TmaCommercePriceService getCommercePriceService()
	{
		return (TmaCommercePriceService) super.getCommercePriceService();
	}

	protected CommonI18NService getI18NService()
	{
		return i18NService;
	}

	@Required
	public void setI18NService(final CommonI18NService i18NService)
	{
		this.i18NService = i18NService;
	}
}
