package com.lla.facades.price;

import com.lla.core.util.LLACommonUtil;
import de.hybris.platform.b2ctelcofacades.price.impl.DefaultTmaPriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.subscriptionfacades.data.SubscriptionPricePlanData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class LLADefaultTmaPriceDataFactory extends DefaultTmaPriceDataFactory
{
    protected static final String VTR_SITE_ID = "website.vtr";

    @Autowired
    LLACommonUtil llaCommonUtil;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private BaseStoreService baseStoreService;

    private final ConcurrentMap<String, NumberFormat> currencyFormats = new ConcurrentHashMap<>();

    @Override
    protected NumberFormat createCurrencyFormat(Locale locale, CurrencyModel currency)
    {
        if(llaCommonUtil.isSiteVTR())
        {
            final String key = locale.getISO3Country() + "_" + currency.getIsocode();

            NumberFormat numberFormat = currencyFormats.get(key);
            if (numberFormat == null)
            {
                final NumberFormat currencyFormat = createNumberFormat(locale, currency);
                numberFormat = currencyFormats.putIfAbsent(key, currencyFormat);
                if (numberFormat == null)
                {
                    numberFormat = currencyFormat;
                }
            }
     /*  if(numberFormat instanceof DecimalFormat && Objects.nonNull(((DecimalFormat) numberFormat).getDecimalFormatSymbols()))
            {
                ((DecimalFormat) numberFormat).getDecimalFormatSymbols().setCurrencySymbol("$");
            }*/

            numberFormat.setMaximumFractionDigits(3);
            numberFormat.setMinimumFractionDigits(3);

            // don't allow multiple references
            return (NumberFormat) numberFormat.clone();
        }
        else
           return super.createCurrencyFormat(locale, currency);
    }

    @Override
    public SubscriptionPricePlanData create(final PriceDataType priceType, final BigDecimal value, final CurrencyModel currency)
    {
        if(llaCommonUtil.isSiteVTR())
        {
            Assert.notNull(priceType, "Parameter priceType cannot be null.");
            Assert.notNull(value, "Parameter value cannot be null.");
            Assert.notNull(currency, "Parameter currency cannot be null.");

            final PriceData priceData = createPriceData();

            priceData.setPriceType(priceType);
            priceData.setValue(value);
            priceData.setCurrencyIso(currency.getIsocode());
            String formattedValue = formatPrice(value, currency);
            String price=formattedValue.split("[,]")[0];
            BaseStoreModel baseStore = baseStoreService.getCurrentBaseStore();
            if(Objects.nonNull(baseStore) && configurationService.getConfiguration().getString(VTR_SITE_ID).equalsIgnoreCase(baseStore.getUid()) &&Objects.nonNull(baseStore.getDefaultCurrency()))
            {
                String currencySymbol = baseStore.getDefaultCurrency().getSymbol();
                if(!StringUtils.isEmpty(currencySymbol) && !price.contains(currencySymbol))
                {
                        price=currencySymbol.concat(price);
                }
            }

            priceData.setFormattedValue(price);

            return (SubscriptionPricePlanData) priceData;
        }
        else
           return super.create(priceType,value,currency);
    }

}
