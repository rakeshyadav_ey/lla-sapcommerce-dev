/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.facades.constants;

/**
 * Global class for all LlaFacades constants.
 */
public class LlaFacadesConstants extends GeneratedLlaFacadesConstants
{
	public static final String EXTENSIONNAME = "llafacades";
	public static final String PRODUCT_CONFIG_DTO = "productConfigDTO";

	private LlaFacadesConstants()
	{
		//empty
	}

	public static final String SUBSCRIPTION_CATEGORY_MASTER_1 = "subscription.category.master1";
	public static final String SUBSCRIPTION_CATEGORY_CHILD_1 = "subscription.category.child1";
	public static final String SUBSCRIPTION_CATEGORY_MASTER_2 = "subscription.category.master2";
	public static final String SUBSCRIPTION_CATEGORY_CHILD_2 = "subscription.category.child2";

	public static final String RUT_NUMBER = "rutNumber";
	public static final String PHONE_NUMBER = "phoneNumber";
	public static final String INACTIVITY_PAGE = "inactivityPage";
	public static final String FIXED_OR_MOBILE = "fixedOrMobile";
	public static final String CHILE_TIME_ZONE = "America/Santiago";
	public static final String CHILE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";


	public static final String PROFILE_CHECKOUT_STEP = "profile-form";
	public static final String DELIVERY_CHECKOUT_STEP = "delivery-address";
	public static final String INSTALLATION_CHECKOUT_STEP = "installation-address";
	public static final String PAYMENT_CHECKOUT_STEP = "payment-method";
	public static final String SUMMARY_CHECKOUT_STEP = "summary";
	public static final String CONTRACT_CHECKOUT_STEP = "contract-detail";
	public static final String CREDIT_CHECK_STEP = "credit-check";



}
