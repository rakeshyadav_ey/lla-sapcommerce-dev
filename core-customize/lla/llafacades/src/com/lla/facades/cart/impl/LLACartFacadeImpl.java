package com.lla.facades.cart.impl;

import com.lla.core.enums.CustomerLastStepEnum;

import com.lla.core.enums.FalloutReasonEnum;

import com.lla.core.enums.TVBoxTypeEnum;
import com.lla.core.model.LLABundleProductMappingModel;
import com.lla.core.service.product.LLAProductService;
import com.lla.core.util.LLACommonUtil;
import com.lla.facades.cart.LLACartFacade;
import com.lla.facades.constants.LlaFacadesConstants;
import com.lla.facades.productconfigurator.data.LLAPuertoRicoProductConfigDTO;
import de.hybris.platform.b2ctelcofacades.data.CartActionInput;
import de.hybris.platform.b2ctelcofacades.data.TmaCartStrategyType;
import de.hybris.platform.b2ctelcofacades.order.TmaCartFacade;
import de.hybris.platform.b2ctelcofacades.order.impl.DefaultTmaCartFacade;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.order.TmaCartStrategy;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.order.CommerceSaveCartTextGenerationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Resource;

import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import com.lla.core.enums.TVBoxTypeEnum;
import com.lla.core.model.LLABundleProductMappingModel;
import com.lla.core.service.product.LLAProductService;
import com.lla.core.util.LLACommonUtil;
import com.lla.facades.cart.LLACartFacade;
import com.lla.facades.constants.LlaFacadesConstants;
import com.lla.facades.productconfigurator.data.LLAPuertoRicoProductConfigDTO;

import static com.lla.core.constants.LlaCoreConstants.*;

public class LLACartFacadeImpl extends DefaultTmaCartFacade implements LLACartFacade {

	private static final Logger LOG = Logger.getLogger(LLACartFacadeImpl.class);
	private static final String TVADDON_CATEGORY = "tvaddon";
	private static final String RUT_NUMBER="rutNumber";
	private static final String PHONE_NUMBER="phoneNumber";

    @Autowired
    private LLACommonUtil llaCommonUtil;

    @Autowired
    private CartService cartService;

    @Autowired
    private ConfigurationService configurationService;

 	@Resource(name = "cartFacade")
 	private TmaCartFacade tmaCartFacade;

    @Autowired
    private LLAProductService productService;

    @Autowired
	private CalculationService calculationService;

    @Autowired
	private CommerceCartService commerceCartService;

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	@Autowired
	private CommerceCartCalculationStrategy commerceCartCalculationStrategy;

	@Autowired
	private UserService userService;
	
	@Autowired
	private CommonI18NService commonI18NService;


	public LLACartFacadeImpl(final Map<TmaCartStrategyType, TmaCartStrategy> cartStrategyMap, final CommerceSaveCartTextGenerationStrategy saveCartTextGenerationStrategy, final ConfigurationService configurationService) {

        super(cartStrategyMap, saveCartTextGenerationStrategy, configurationService);
    }

    @Override
    public CartModificationData addProductOfferingToCartForMobiles(final String spoCode, final String planType,final long quantity,final String processType,
                                                         final String rootBpoCode, final int cartGroupNo, final String subscriptionTermId, final String subscriberId,
                                                         final String subscriberBillingId) throws CommerceCartModificationException
    {
        final CartActionInput cartActionInput = new CartActionInput();
        cartActionInput.setCartId(getCartService().getSessionCart().getCode());
        cartActionInput.setUserGuid(getCustomerFacade().getCurrentCustomerUid());
        cartActionInput.setProductCode(spoCode);
        cartActionInput.setQuantity(quantity);
        cartActionInput.setProcessType(processType);
        cartActionInput.setRootBpoCode(rootBpoCode);
        cartActionInput.setCartGroupNo(cartGroupNo);
        cartActionInput.setSubscriptionTermId(subscriptionTermId);
        cartActionInput.setSubscriberId(subscriberId);
        cartActionInput.setSubscriberBillingId(subscriberBillingId);
        if(null != planType)
        {
            cartActionInput.setPlanType(planType);
        }

        return processCartActionForDevice(Collections.singletonList(cartActionInput)).get(0);
    }

    @Override
    public List<CartModificationData> processCartActionForDevice(final List<CartActionInput> cartActionInputList) throws CommerceCartModificationException

    {
        final List<CartModificationData> cartModificationDataList = new ArrayList<>();
        for (final CartActionInput cartActionInput : cartActionInputList)
        {
            final List<CommerceCartParameter> commerceCartParameters = new ArrayList<>();

            final TmaCartStrategy strategy = findCartStrategy(cartActionInput);
            if (CollectionUtils.isNotEmpty(cartActionInput.getChildren()))
            {
                for (final CartActionInput childCartActionInput : cartActionInput.getChildren())
                {
                    validateCartActionInput(childCartActionInput);
                    commerceCartParameters.add(getTmaCommerceCartParameterConverter().convert(childCartActionInput));
                }
            }
            else
            {
                validateCartActionInput(cartActionInput);
                commerceCartParameters.add(getTmaCommerceCartParameterConverter().convert(cartActionInput));
            }

            cartModificationDataList
                    .addAll(getCartModificationConverter().convertAll(strategy.processCartAction(commerceCartParameters)));
        }

        return cartModificationDataList;
    }

    @Override
    public String checkCartForCompatibilityIssues()
    {
        boolean isNotCompatible = false;
        final Map<String,String> categoryMap = new HashMap<>();
        categoryMap.put(configurationService.getConfiguration().getString(LlaFacadesConstants.SUBSCRIPTION_CATEGORY_MASTER_1),configurationService.getConfiguration().getString(LlaFacadesConstants.SUBSCRIPTION_CATEGORY_CHILD_1));
        categoryMap.put(configurationService.getConfiguration().getString(LlaFacadesConstants.SUBSCRIPTION_CATEGORY_MASTER_2),configurationService.getConfiguration().getString(LlaFacadesConstants.SUBSCRIPTION_CATEGORY_CHILD_2));
        final CartModel cartModel = cartService.getSessionCart();
        final List<ProductModel> productList = llaCommonUtil.retrieveCustomerSubscriptionForCompatibility(cartModel);

        for(final String category:categoryMap.keySet())
        {
            final List<String> listOfCategories = Stream.of(categoryMap.get(category).split(",", -1))
                    .collect(Collectors.toList());
            for(final AbstractOrderEntryModel cartEntry : cartModel.getEntries())
            {
                boolean isCompatible = false;
                if(llaCommonUtil.containsFirstLevelCategoryWithCode(cartEntry.getProduct(),category) || llaCommonUtil.containsSecondLevelCategoryWithCode(cartEntry.getProduct(),category))
                {
                    for(final AbstractOrderEntryModel childCartEntry : cartModel.getEntries())
                    {
                        for(final String childCategory : listOfCategories)
                        {
                            if(llaCommonUtil.containsFirstLevelCategoryWithCode(childCartEntry.getProduct(),childCategory)|| llaCommonUtil.containsSecondLevelCategoryWithCode(childCartEntry.getProduct(),childCategory))
                            {
                                isCompatible = true;
                            }
                        }
                    }
                    for(final ProductModel product : productList)
                    {
                        for(final String childCategory : listOfCategories)
                        {
                            if(llaCommonUtil.containsFirstLevelCategoryWithCode(product,childCategory)|| llaCommonUtil.containsSecondLevelCategoryWithCode(product,childCategory))
                            {
                                isCompatible = true;
                            }
                        }
                    }
                    if(!isCompatible)
                    {
                        isNotCompatible = true;
                    }
                }
            }
            if(isNotCompatible)
            {
                return category;
            }
        }
        return null;
    }

    @Override
	public void setAccountNumberForCustomer(String accountNumber)
	{
		CartModel cart=getCartService().getSessionCart();
		if(null != cart) {
		 cart.setCustomerAccountNumber(accountNumber);
		 getModelService().save(cart);
		 getModelService().refresh(cart);
		}
		
	}

	@Override
	public void updateCartIfExistingCustomer(String isCustomerExisting)
	{
		CartModel cart=getCartService().getSessionCart();
		if(null != cart) {
		 cart.setIsCustomerExisting(Boolean.valueOf(isCustomerExisting));
		 for(AbstractOrderEntryModel entry :cart.getEntries()) {
			 
				if (llaCommonUtil.containsFirstLevelCategoryWithCode(entry.getProduct(), "3Pbundles")
						|| llaCommonUtil.containsFirstLevelCategoryWithCode(entry.getProduct(), "postpaid"))
				{
					entry.setCalculated(Boolean.FALSE);
				}
				 
		 }
		 cart.setCalculated(Boolean.FALSE);
		 getModelService().saveAll();
		 getModelService().refresh(cart);
		 recalculateCartEntryWithNewPrice();
		}
		
	}

	@Override
	public void updateRUTNumber(Map<String, String> userDetails)
	{

		CartModel cart = cartService.getSessionCart();
		if(Objects.nonNull(cart))
		{
			cart.setRutNumber(userDetails.get(RUT_NUMBER));
			cart.setPhoneNumber(userDetails.get(PHONE_NUMBER));
			getModelService().save(cart);
		}


	}

	@Override
	public void updateCartUTMfields(Map<String, String> utmDetails)
	{
		CartModel cart = cartService.getSessionCart();
		if(Objects.nonNull(cart))
		{
			cart.setUtmzField(utmDetails.get(UTM_FIELD));
			getModelService().save(cart);
		}
	}

	/**
	 */
	private void recalculateCartEntryWithNewPrice()
	{
		commerceCartCalculationStrategy.calculateCart(getCartService().getSessionCart());
		
	}


	@Override
	public void addConfiguratorProductsToCart(final LLAPuertoRicoProductConfigDTO productConfigDTO, final long quantity,
			final String processType, final String rootBpoCode, final int cartGroupNo, final String subscriptionTermId,
			final String subscriberId, final String subscriberBillingId) throws CalculationException {
		if (null != productConfigDTO)
		{

			final List<String> productCodes = getSPOList(productConfigDTO);
			for (final String spoCode : productCodes)
			{
				try
				{
					if(null != productConfigDTO.getAddonProductsQuantity() && productConfigDTO.getAddonProductsQuantity().keySet().contains(spoCode)) {
						tmaCartFacade.addProductOfferingToCart(spoCode, productConfigDTO.getAddonProductsQuantity().get(spoCode), processType, rootBpoCode, cartGroupNo,
								subscriptionTermId, subscriberId, subscriberBillingId);
					}else{
						tmaCartFacade.addProductOfferingToCart(spoCode, quantity, processType, rootBpoCode, cartGroupNo,
								subscriptionTermId, subscriberId, subscriberBillingId);
					}
				}
				catch (final CommerceCartModificationException ex)
				{
					LOG.warn("Couldn't add product of code " + spoCode + " to cart.", ex);
				}
			}
		}
        final CartModel cart=cartService.getSessionCart();
        cart.setBundleProductMapping(getBundleProductMappingForCart(productConfigDTO));
		cart.setIsTelephoneFree(llaCommonUtil.isTelephoneFree(cart));
		calculationService.recalculate(cart);
        getModelService().save(cart);

	}

    public List<LLABundleProductMappingModel> getBundleProductMappingForCart(final LLAPuertoRicoProductConfigDTO productConfigDTO) {
        final List<LLABundleProductMappingModel> bundles=productService.getAllBundleMapping().stream().filter(bundle -> filterBundle(bundle, productConfigDTO)).collect(Collectors.toList());
        return bundles;
    }
    
    @Override
	 public Map<ProductModel, Double> getProductPriceMap(final List<ProductModel> productList,
			 final List<LLABundleProductMappingModel> productMappingList)
	 {

		 Map<ProductModel, Double> sortProductbyPrice = new HashMap<ProductModel, Double>();
		 int i = 0, j = 0;
		 if ((null != productList  && CollectionUtils.isNotEmpty(productList)) && 
			  (null != productMappingList && CollectionUtils.isNotEmpty(productMappingList)) &&
			  (null != productMappingList.get(0).getTvPriceList() && CollectionUtils.isNotEmpty(productMappingList.get(0).getTvPriceList())))
		 {
			 for (ProductModel product : productList)
				 sortProductbyPrice.put(productList.get(i++), productMappingList.get(0).getTvPriceList().get(j++));
		 }
		 Map<ProductModel, Double> sortedMapInAscendingOrder = sortProductbyPrice.entrySet().stream()
				 .sorted(Map.Entry.comparingByValue())
				 .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

		 return sortedMapInAscendingOrder;
	 }

	@Override
	public void createParentChildEntry(String cartCode, OrderEntryData entry, OrderEntryData childEntry) {
		CartModel cartModel = commerceCartService.getCartForCodeAndUser(cartCode, userService.getCurrentUser());
		AbstractOrderEntryModel parentEntryModel = cartModel.getEntries().get(entry.getEntryNumber());
		AbstractOrderEntryModel childEntryModel = cartModel.getEntries().get(childEntry.getEntryNumber());
		Collection<AbstractOrderEntryModel> list = new ArrayList<>();
		list.add(childEntryModel);
		if(CollectionUtils.isNotEmpty(parentEntryModel.getChildEntries())){
			list.addAll(parentEntryModel.getChildEntries());
		}
		parentEntryModel.setChildEntries(list);
		getModelService().save(childEntryModel);
	}

	@Override
	public void setLineNumberForEntries(CartModificationData parentData) {
		CartModel cartModel=commerceCartService.getCartForCodeAndUser(parentData.getCartCode(), userService.getCurrentUser());
		if(null == cartModel.getNumberOfLines()){
			cartModel.setNumberOfLines(1);
		}else {
			cartModel.setNumberOfLines(cartModel.getNumberOfLines() + 1);
		}
		getModelService().save(cartModel);
	}

        public LinkedHashSet<String> getSelectedPlanInOrder() {
		final CartData data = getSessionCart();
		LinkedHashSet<String> selectedPlanSet = new LinkedHashSet<String>();
		if (null != data && CollectionUtils.isNotEmpty(data.getEntries())) {
			for (OrderEntryData entry : data.getEntries()) {
				selectedPlanSet.add(entry.getProduct().getName());
			}
		}
		return selectedPlanSet;
	}

	private boolean filterBundle(final LLABundleProductMappingModel bundle, final LLAPuertoRicoProductConfigDTO productConfigDTO) {
        if (null != productConfigDTO && (isOneP(bundle.getInternetPlan(), productConfigDTO.getOneP())) && isTwoP(bundle.getTv(), productConfigDTO.getTwoP()) &&
                isThreeP(bundle.getTelephonePlan(), productConfigDTO.getThreeP()) && isTvAddon(bundle.getTvBoxType(), productConfigDTO.getTwoPAddonDVR())) {

            return true;
        }
        return false;
    }


   private boolean isTvAddon(final TVBoxTypeEnum tvBoxType, final String twoPAddonDVR)
   {
        if(StringUtils.isNotEmpty(twoPAddonDVR) && TVBoxTypeEnum.DVR == tvBoxType)
		{
			return true;
		}else  if(StringUtils.isEmpty(twoPAddonDVR) && TVBoxTypeEnum.DVR != tvBoxType)
		{
			return true;
		}
		else
		{
			return false;
		}
    }
	
    private boolean isThreeP(final TmaProductOfferingModel telephonePlan, final String threeP) {

        if (StringUtils.isEmpty(threeP) && null == telephonePlan)
		{
			return true;
		}
		else if (telephonePlan !=null && StringUtils.isNotEmpty(telephonePlan.getCode()) && telephonePlan.getCode().equalsIgnoreCase(threeP))
		{
			return true;
		}
		else
		{
			return false;
		}
    }

    private boolean isTwoP(final TmaProductOfferingModel tv, final String twoP) {
        if (StringUtils.isEmpty(twoP) && null == tv)
		{
			return true;
		}
		else if (null != tv && tv.getCode().equalsIgnoreCase(twoP))
		{
			return true;
		}
		else
		{
			return false;
		}
    }

    private boolean isOneP(final TmaProductOfferingModel internetPlan, final String oneP) {
        if (StringUtils.isEmpty(oneP) && null == internetPlan)
		{
			return true;
		}
		else if (null != internetPlan && internetPlan.getCode().equalsIgnoreCase(oneP))
		{
			return true;
		}
		else
		{
			return false;
		}
    }
    
    /**
  	 * @param cartBundles
  	 */
 	@Override
 	public void filterCartBundles(final List<LLABundleProductMappingModel>  cartBundles) {
  		List<LLABundleProductMappingModel>  filteredBundles = new ArrayList<LLABundleProductMappingModel>();
  		final CartModel cart = cartService.getSessionCart();
 		try
 		{

 			for (final LLABundleProductMappingModel cartBundle : cartBundles)
 			{
 				for (final LLABundleProductMappingModel bundle : productService.getAllBundleMapping())
 				{
 					if (((null != bundle.getTelephonePlan() && null != cartBundle.getTelephonePlan())|| (null == bundle.getTelephonePlan() && null == cartBundle.getTelephonePlan()))
 	                  && (null != bundle.getTvBoxType() && bundle.getTvBoxType().equals(TVBoxTypeEnum.DVR))
 	                  && ((null != bundle.getTv() && null != cartBundle.getTv()) && bundle.getTv().equals(cartBundle.getTv())))
					{
					   filteredBundles.add(bundle);
					   break;
					}
 				}
 			}
 			if (null != filteredBundles && CollectionUtils.isNotEmpty(filteredBundles))
 			{
 				cart.setBundleProductMapping(filteredBundles);
 			}
 			calculationService.recalculate(cart);
 			getModelService().save(cart);
 			getModelService().refresh(cart);
 		}
 		catch (final CalculationException exception)
 		{
 			LOG.error("Error in Cart Calculation ", exception);
 		}
    }
     
     /**
   	 * @param cartBundles
   	 */
 	@Override
 	 public void removeDVRBundleFromCart(final List<LLABundleProductMappingModel> cartBundles)
 	 {
 		 List<LLABundleProductMappingModel> filteredBundles = new ArrayList<LLABundleProductMappingModel>();
 		 final CartModel cart = cartService.getSessionCart();
 		 try
 		 {

 			 for (final LLABundleProductMappingModel cartBundle : cartBundles)
 			 {
 				 for (final LLABundleProductMappingModel bundle : productService.getAllBundleMapping())
 				 {
 					if (((null != bundle.getTelephonePlan() && null != cartBundle.getTelephonePlan())|| (null == bundle.getTelephonePlan() && null == cartBundle.getTelephonePlan()))
 	                   && ((null != bundle.getTv() && null != cartBundle.getTv()) && bundle.getTv().equals(cartBundle.getTv()))
 	                   && (bundle.getTvBoxType() == TVBoxTypeEnum.HD))
					 {
						filteredBundles.add(bundle);
						break;
					 }
 				 }
 			 }

 			 if (null != filteredBundles && CollectionUtils.isNotEmpty(filteredBundles))
 			 {
 				 cart.setBundleProductMapping(filteredBundles);
 			 }
 			 calculationService.recalculate(cart);
 			 getModelService().save(cart);
 			 getModelService().refresh(cart);
 		 }
 		 catch (final CalculationException exception)
 		 {
 			 LOG.error("Error in Cart Calculation ", exception);
 		 }

 	 }


    /**
	 * @param productConfigDTO
	 * @return
	 */
	private List<String> getSPOList(final LLAPuertoRicoProductConfigDTO productConfigDTO)
	{
		final List<String> productCodeList = new ArrayList<String>();
		if (StringUtils.isNotEmpty(productConfigDTO.getOneP()))
		{
			productCodeList.add(productConfigDTO.getOneP());

		}
		if (StringUtils.isNotEmpty(productConfigDTO.getTwoP()))
		{
			productCodeList.add(productConfigDTO.getTwoP());
		}
		if (StringUtils.isNotEmpty(productConfigDTO.getThreeP()))
		{
			productCodeList.add(productConfigDTO.getThreeP());
		}
		setAddonsProducts(productCodeList,productConfigDTO);
		return productCodeList;
	}

	/**
	 * @param productCodeList
	 * @param productConfigDTO
	 */
	private void setAddonsProducts(final List<String> productCodeList, final LLAPuertoRicoProductConfigDTO productConfigDTO)
	{

		if(StringUtils.isNotEmpty(productConfigDTO.getOneP())
				&& StringUtils.isNotEmpty(productConfigDTO.getOnePAddon())){
			productCodeList.add(productConfigDTO.getOnePAddon());
		}
		setTVAddonsCodes(productCodeList, productConfigDTO);
		if (CollectionUtils.isNotEmpty(productConfigDTO.getChannelList()))
		{
			productConfigDTO.getChannelList().stream().forEach(channel ->{
				if(StringUtils.isNotBlank(channel)){
					productCodeList.add(channel);
				}
			});
		}
		if (CollectionUtils.isNotEmpty(productConfigDTO.getSmartProductList()))
		{
			productConfigDTO.getSmartProductList().stream().forEach(smartProtect ->{
				if(StringUtils.isNotBlank(smartProtect)){
					productCodeList.add(smartProtect);
				}
			});
		}
	}

	/**
	 * @param productCodeList
	 * @param productConfigDTO
	 */
	private void setTVAddonsCodes(final List<String> productCodeList, final LLAPuertoRicoProductConfigDTO productConfigDTO)
	{
		if(StringUtils.isNotEmpty(productConfigDTO.getTwoP())){
			if(StringUtils.isNotEmpty(productConfigDTO.getTwoPAddonDVR()))
			{
				productCodeList.add(productConfigDTO.getTwoPAddonDVR());
			}
			if(StringUtils.isNotEmpty(productConfigDTO.getTwoPAddonHD()))
			{
				productCodeList.add(productConfigDTO.getTwoPAddonHD());
			}
			if(StringUtils.isNotEmpty(productConfigDTO.getTwoPHUB()))
			{
				productCodeList.add(productConfigDTO.getTwoPHUB());
			}
		}

	}

	@Override
	public List<ProductData> getPrimaryProductsList()
	{
		final CartModel cart = cartService.getSessionCart();
		if (null != cart)
		{

			final List<ProductModel> products = new ArrayList<ProductModel>();
			final String promaryProductsCategories = configurationService.getConfiguration()
					.getString("puertorico.primary.products.category.codes");
			final List<String> CategoryList = Stream.of(promaryProductsCategories.split(",", -1)).collect(Collectors.toList());
			for (final AbstractOrderEntryModel entry : cart.getEntries())
			{

				for (final String categoryCode : CategoryList)
				{
					if (llaCommonUtil.containsFirstLevelCategoryWithCode(entry.getProduct(), categoryCode))
					{
						products.add(entry.getProduct());
					}
				}
			}
			return productConverter.convertAll(products);

		}
		return null;
	}

	@Override
	public List<ProductData> getChannelAddonProducts()
	{
		final CartModel cart = cartService.getSessionCart();
		if (null != cart)
		{

			final List<ProductModel> products = new ArrayList<ProductModel>();
			for (final AbstractOrderEntryModel entry : cart.getEntries())
			{
				if (entry.getProduct().getSupercategories().iterator().next().getCode().equalsIgnoreCase("channeladdon"))
				{
					products.add(entry.getProduct());
				}
			}
			return productConverter.convertAll(products);
		}
		return null;
	}

	@Override
	public ProductData getSmartProductIfPresent()
	{
		final CartModel cart = cartService.getSessionCart();
		if (null != cart)
		{
			for (final AbstractOrderEntryModel entry : cart.getEntries())
			{
				if (entry.getProduct().getSupercategories().iterator().next().getCode().equalsIgnoreCase("smartproduct"))
				{
					return  productConverter.convert(entry.getProduct());
				}
			}

		}
		return null;
	}

	@Override

	public void calculateAutoPayDetails(Boolean autoPay,Integer numberOfLines) throws CalculationException {
		final CartModel cart = cartService.getSessionCart();
		if (null != cart) {
			cart.setAutoPayActive(autoPay);
			cart.setCalculated(Boolean.FALSE);
			cart.setNumberOfLines(numberOfLines);
			getModelService().save(cart);
			getModelService().refresh(cart);
			calculationService.recalculate(cart);
		}
	}
	

	/**
	 * Customer Last visited Checkout Step will be Identified 
	 * @param currentStep
	 */
	@Override
	public void lastVisitedStep(final String currentStep) {
		final CartModel cart = cartService.getSessionCart();
		if(Objects.nonNull(cart)) {
		switch (currentStep)
		{
			case LlaFacadesConstants.PROFILE_CHECKOUT_STEP:
				saveCustomerLastCheckoutStep(cart,CustomerLastStepEnum.PERSONAL_DETAILS_CHECKOUTSTEP);
				break;
			case LlaFacadesConstants.DELIVERY_CHECKOUT_STEP:
			saveCustomerLastCheckoutStep(cart,CustomerLastStepEnum.ADDRESS_CHECKOUTSTEP);
			break;
			case LlaFacadesConstants.PAYMENT_CHECKOUT_STEP:
				saveCustomerLastCheckoutStep(cart,CustomerLastStepEnum.PAYMENT_CHECKOUTSTEP);
				break;
			case LlaFacadesConstants.INSTALLATION_CHECKOUT_STEP:
				saveCustomerLastCheckoutStep(cart,CustomerLastStepEnum.ADDRESS_CHECKOUTSTEP);
				break;	
			case LlaFacadesConstants.SUMMARY_CHECKOUT_STEP:
				saveCustomerLastCheckoutStep(cart,CustomerLastStepEnum.SUMMARY_CHECKOUTSTEP);
			break;
			case LlaFacadesConstants.CONTRACT_CHECKOUT_STEP:
				saveCustomerLastCheckoutStep(cart,CustomerLastStepEnum.CONTRACT_CHECKOUTSTEP);
				break;
			case LlaFacadesConstants.CREDIT_CHECK_STEP:
				saveCustomerLastCheckoutStep(cart,CustomerLastStepEnum.CREDIT_CHECK_CHECKOUTSTEP);
				break;
			default:
				LOG.info("Current Step checkout Begins.");
		}		
		}
	}

	/**
	 * @param cart
	 * @param customerLastStepEnum
	 */
	private void saveCustomerLastCheckoutStep(final CartModel cart, CustomerLastStepEnum customerLastStepEnum)
	{
		cart.setLastStepValue(customerLastStepEnum);
		getModelService().save(cart);
		getModelService().refresh(cart);
	}

	
	public List<ProductData> getTVAddonIfPresent()
	{
		final CartModel cart = cartService.getSessionCart();
		final List<ProductModel> products = new ArrayList<ProductModel>();
		if (null != cart)
		{
			for (final AbstractOrderEntryModel entry : cart.getEntries())
			{
				if (entry.getProduct().getSupercategories().iterator().next().getCode().equalsIgnoreCase(TVADDON_CATEGORY))
				{
					products.add(entry.getProduct());
				}
			}
			return productConverter.convertAll(products);
		}
		return null;
	}
}

