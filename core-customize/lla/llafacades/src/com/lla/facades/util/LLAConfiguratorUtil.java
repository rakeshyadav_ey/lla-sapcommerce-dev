package com.lla.facades.util;

import com.lla.core.util.LLACommonUtil;
import com.lla.facades.constants.LlaFacadesConstants;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.subscriptionservices.model.RecurringChargeEntryModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionPricePlanModel;

import java.util.*;
import java.util.stream.Collectors;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.lla.facades.product.LLATmaProductFacade;
import com.lla.facades.productconfigurator.data.LLAPuertoRicoProductConfigDTO;

public class LLAConfiguratorUtil {

    public static final String DVR_BOX = "DVR_BOX";
    public static final String INTERNET_90 = "internet_090";
    public static final String INTERNET_500 = "internet_500";
    public static final String TV_ULTIMATE = "tv_ultimate";
    public static final String HD_BOX = "HD_BOX";
    public static final String HUB_TV_BOX = "HUB_TV_BOX";
    public static final String ADDONS_TV_LIST_WITHOUT_INTERNET = "addons.tv.list.without.internet";
    public static final String ADDONS_TV_LIST_WITH_INTERNET = "addons.tv.list.with.internet";
    public static final String DEFAULT_VALUE_SPACE = "";
    public static final String COMMA = ",";
    private static final String PUERTO_RICO_CATALOG_ID = "puertoricoContentCatalog";
 	 private static final String TVCATEGORY = "tv";

    @Autowired
    private SiteConfigService siteConfigService;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private MediaService mediaService;

    @Resource(name = "catalogVersionService")
    private CatalogVersionService catalogVersionService;
    
    @Resource(name = "cartService")
 	 private CartService cartService;
    
    @Autowired
 	 private LLACommonUtil llaCommonUtil;

    @Autowired
    private LLATmaProductFacade llaTmaProductFacade;
    public List<TmaProductOfferingModel> getAddons(final LLAPuertoRicoProductConfigDTO productConfig) {
        List<String> addons;
        if (null != productConfig && null != productConfig.getOneP()) {
            addons = Arrays.asList(StringUtils.split(siteConfigService.getString(ADDONS_TV_LIST_WITH_INTERNET, DEFAULT_VALUE_SPACE), COMMA));
        } else{
            addons = Arrays.asList(StringUtils.split(siteConfigService.getString(ADDONS_TV_LIST_WITHOUT_INTERNET, DEFAULT_VALUE_SPACE), COMMA));
        }
        final List<TmaProductOfferingModel> list = new ArrayList<>();
        for(final String code: addons) {
            list.add(llaTmaProductFacade.getProductForCode(code));
        }
        return list;
    }

    public void saveAddons(final List<String> addonProducts, final LLAPuertoRicoProductConfigDTO productConfig) {
        if(addonProducts.contains(DVR_BOX))
        {
            productConfig.setTwoPAddonDVR(DVR_BOX);
        }
        if(addonProducts.contains(HD_BOX))
        {
            productConfig.setTwoPAddonHD(HD_BOX);
        }
        if(addonProducts.contains(HUB_TV_BOX))
        {
            productConfig.setTwoPHUB(HUB_TV_BOX);
        }
    }

    public void removeAddons(final List<String> addonProducts, final LLAPuertoRicoProductConfigDTO productConfig) {
        if (addonProducts.contains(DVR_BOX)) {
            productConfig.setTwoPAddonDVR(StringUtils.EMPTY);
        }
        if (addonProducts.contains(HD_BOX)) {
            productConfig.setTwoPAddonHD(StringUtils.EMPTY);
        }
        if (addonProducts.contains(HUB_TV_BOX)) {
            productConfig.setTwoPHUB(StringUtils.EMPTY);
        }
    }

	 public List<ProductModel> sortproduct(final List<ProductModel> productForCategory)
	 {
		 return productForCategory.stream().sorted(Comparator.comparing(ProductModel::getCode)).collect(Collectors.toList());
	 }
	 
	 public Map<ProductModel, Double> getProductPriceMap(final List<ProductModel> productForCategory)
	 {
		 Map<ProductModel, Double> sortProductbyPrice = new HashMap<ProductModel, Double>();
		 if (null != productForCategory && CollectionUtils.isNotEmpty(productForCategory))
		 {
			 for (ProductModel product : productForCategory)
			 {
				 Collection<PriceRowModel> productPricesRows = product.getEurope1Prices();
				 if (null != productPricesRows && CollectionUtils.isNotEmpty(productPricesRows))
				 {
					 SubscriptionPricePlanModel entry = (SubscriptionPricePlanModel) productPricesRows.iterator().next();
					 if (null != entry && CollectionUtils.isNotEmpty(entry.getRecurringChargeEntries()))
					 {
						 RecurringChargeEntryModel chargeEntry = entry.getRecurringChargeEntries().iterator().next();
						 sortProductbyPrice.put(product, (null != chargeEntry) ? chargeEntry.getPrice() : 0.0);
					 }
				 }

			 }
		 }
		 // Sort Map In Ascending Order
		 Map<ProductModel, Double> sortedMapInAscendingOrder = sortProductbyPrice.entrySet().stream()
				 .sorted(Map.Entry.comparingByValue())
             .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		 
		 return sortedMapInAscendingOrder;
		 
	 }

    public LLAPuertoRicoProductConfigDTO getLlaPuertoRicoProductConfigDTO() {
        LLAPuertoRicoProductConfigDTO productConfig = new LLAPuertoRicoProductConfigDTO();
        if (null != sessionService.getAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO)) {
            productConfig = sessionService.getAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO);
        } else {
            sessionService.setAttribute(LlaFacadesConstants.PRODUCT_CONFIG_DTO, productConfig);
        }
        return productConfig;
    }


    /**
     * @param stepId
     */
    public LLAPuertoRicoProductConfigDTO updateProductConfigDTO(final int stepId)
    {
        LLAPuertoRicoProductConfigDTO productConfig = getLlaPuertoRicoProductConfigDTO();
        switch (stepId)
        {
            case 1:
                if (StringUtils.isNotEmpty(productConfig.getOneP()))
                {
                    return new LLAPuertoRicoProductConfigDTO();
                }
                break;

            case 2:
                if (StringUtils.isNotEmpty(productConfig.getTwoP()))
                {
                    setTVAndAddonsToEmpty(productConfig);
                    productConfig.setThreeP(StringUtils.EMPTY);
                    productConfig.setChannelList(new ArrayList<String>());
                    productConfig.setSmartProductList(new ArrayList<String>());
                    return productConfig;
                }
                break;
            case 3:
                if (StringUtils.isNotEmpty(productConfig.getThreeP()))
                {
                    productConfig.setThreeP(StringUtils.EMPTY);
                    return productConfig;
                }
                break;

        }
        return productConfig;
    }

    public List<ProductModel> fileterPhone(final List<ProductModel> productForCategory,
                                           final LLAPuertoRicoProductConfigDTO productConfig)
    {
        if(null != productConfig && StringUtils.isNotEmpty(productConfig.getOneP()) && StringUtils.isNotEmpty(productConfig.getTwoP()))
        {
            return null;
        }
        return productForCategory;
    }

    private void setTVAndAddonsToEmpty(LLAPuertoRicoProductConfigDTO productConfig)
    {
        productConfig.setTwoP(StringUtils.EMPTY);
        productConfig.setTwoPAddonDVR(StringUtils.EMPTY);
        productConfig.setTwoPAddonHD(StringUtils.EMPTY);
        productConfig.setTwoPHUB(StringUtils.EMPTY);
    }

    public MediaModel getMediaForCode(final String code)
    {
        try
        {
            return mediaService.getMedia(catalogVersionService.getCatalogVersion(PUERTO_RICO_CATALOG_ID, "Online"), code);
        }
        catch (final UnknownIdentifierException ignore)
        {
            // Ignore this exception
        }
        return null;
    }

    public List<ProductData> getProductDataForList(Collection<TmaProductOfferingModel> addons) {
        List<ProductData> data=new ArrayList<>();
        if(null != addons)
            addons.stream().forEach(model -> {
                        data.add(llaTmaProductFacade.getProductForCodeAndOptions(model.getCode(),Arrays.asList(ProductOption.BASIC, ProductOption.GALLERY, ProductOption.CATEGORIES, ProductOption.IMAGES, ProductOption.PRODUCT_SPEC_CHAR_VALUE_USE, ProductOption.PRICE, ProductOption.PRODUCT_OFFERING_PRICES,ProductOption.PRODUCT_SPECIFICATION)));
                    }
            );
        return data;
    }

    public Map<String, Integer> getMaxQuantityForAddons(LLAPuertoRicoProductConfigDTO productConfig) {
        List<String> addons;
        if (null != productConfig && null != productConfig.getOneP()) {
            addons = Arrays.asList(StringUtils.split(siteConfigService.getString("addons.tv.list.with.internet.quantity", DEFAULT_VALUE_SPACE), COMMA));
        } else{
            addons = Arrays.asList(StringUtils.split(siteConfigService.getString("addons.tv.list.without.internet.quantity", DEFAULT_VALUE_SPACE), COMMA));
        }
        Map<String, Integer> map = new HashMap<>();
        for(String str:addons){
            List<String> sub=Arrays.asList(StringUtils.split(str,":"));
            map.put(sub.get(0), Integer.valueOf(sub.get(1)));
        }
        return map;
    }

    public void saveAddon(String selectedAddonProductCode, int quantity, LLAPuertoRicoProductConfigDTO productConfig) {
        if(selectedAddonProductCode.equals(DVR_BOX))
        {
            productConfig.setTwoPAddonDVR(DVR_BOX);
        }
        if(selectedAddonProductCode.contains(HD_BOX))
        {
            productConfig.setTwoPAddonHD(HD_BOX);
        }
        if(selectedAddonProductCode.contains(HUB_TV_BOX))
        {
            productConfig.setTwoPHUB(HUB_TV_BOX);
        }
        productConfig.getAddonProductsQuantity();
        Map<String, Integer> map = new HashMap<>();
        if(null != productConfig.getAddonProductsQuantity()) {
            map = productConfig.getAddonProductsQuantity();
        }
        map.put(selectedAddonProductCode, quantity);
        productConfig.setAddonProductsQuantity(map);
    }
    
   // 	To check cart has entries
 	public boolean isCartEmpty()
 	{
 		if (cartService.hasSessionCart() && cartService.getSessionCart().getEntries().size() > 0)
 		{
 			return false;
 		}
 		else
 		{
 			return true;
 		}

 	}
}
