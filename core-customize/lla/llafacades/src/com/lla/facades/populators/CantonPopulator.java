package com.lla.facades.populators;

import com.lla.core.model.CantonModel;
import com.lla.core.model.ProvinceModel;
import com.lla.facades.address.CantonData;
import com.lla.facades.address.ProvinceData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class CantonPopulator implements Populator<CantonModel, CantonData> {
    /**
     * Populate the target instance with values from the source instance.
     *
     * @param source the source object
     * @param target  the target to fill
     * @throws ConversionException if an error occurs
     */
    @Override
    public void populate(CantonModel source, CantonData target) throws ConversionException {
        target.setName(source.getLabel());
        target.setCode(source.getUid());
    }
}
