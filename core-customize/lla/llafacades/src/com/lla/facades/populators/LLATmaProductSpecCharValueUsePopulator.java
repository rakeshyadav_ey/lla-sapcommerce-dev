/**
 * 
 */
package com.lla.facades.populators;

import de.hybris.platform.b2ctelcofacades.converters.populator.TmaProductSpecCharValueUsePopulator;
import de.hybris.platform.b2ctelcofacades.data.TmaProductSpecCharacteristicValueUseData;
import de.hybris.platform.b2ctelcoservices.model.TmaProductSpecCharacteristicValueModel;
import de.hybris.platform.converters.Populator;

/**
 * @author HP737SW
 *
 */
public class LLATmaProductSpecCharValueUsePopulator extends TmaProductSpecCharValueUsePopulator implements Populator<TmaProductSpecCharacteristicValueModel, TmaProductSpecCharacteristicValueUseData>
{
	@Override
	public void populate(TmaProductSpecCharacteristicValueModel source, TmaProductSpecCharacteristicValueUseData target)
	{
		super.populate(source, target);
		target.setId(source.getId());
		if(null!=source.getName()) {
		target.setName(source.getName());
		}
		target.setSequenceId( source.getSequenceId());
		if(null != source.getSpecificationType())
			target.setSpecificationType(source.getSpecificationType().toString());
	}
}