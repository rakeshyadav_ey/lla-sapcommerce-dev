/**
 *
 */
package com.lla.facades.populators;

import com.lla.core.enums.FalloutReasonEnum;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.subscriptionfacades.data.OrderPriceData;
import de.hybris.platform.subscriptionservices.model.RecurringChargeEntryModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionPricePlanModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.lla.core.util.LLACommonUtil;
import com.lla.core.enums.PlanTypes;
import com.lla.facades.product.data.PlanTypeData;
import com.lla.core.enums.DevicePaymentOption;
import com.lla.facades.product.data.DevicePaymentOptionData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
/**
 * @author HP737SW
 *
 */
public class LLACommonPopulatorUtils
{
	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	@Autowired
	private LLACommonUtil llaCommonUtil;

	private Converter<PlanTypes, PlanTypeData> planTypeConverter;
	private Converter<DevicePaymentOption, DevicePaymentOptionData> devicePaymentOptionConverter;

	/**
	 * @return the priceDataFactory
	 */
	public PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	/**
	 * @param priceDataFactory
	 *           the priceDataFactory to set
	 */
	public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}

	public void populate(final AbstractOrderModel source, final AbstractOrderData target) throws ConversionException
	{
		
		if(llaCommonUtil.isSitePuertorico()) {
		final PriceData priceData = new PriceData();
		final BigDecimal bd = new BigDecimal("0.0");
		final BigDecimal actualDecimal = new BigDecimal("0.00");
		priceData.setValue(actualDecimal.setScale(2));
		final PriceData actualSmartPrice = createPrice(source, source.getSmartProtectPrice());
		final PriceData actualAddonTotalPrice = createPrice(source, source.getAddonProductsTotalPrice());
		final PriceData actualChannelsTotalPrice = createPrice(source, source.getChannelsAddonTotalPrice());
		final BigDecimal smartValue = actualSmartPrice.getValue();
		final BigDecimal actualAddonValue = actualAddonTotalPrice.getValue();
		final BigDecimal actualChannelsValue = actualChannelsTotalPrice.getValue();
		target.setSmartProtectPrice(!smartValue.equals(bd) ? actualSmartPrice : priceData);
		target.setAddonProductsTotalPrice(!actualAddonValue.equals(bd) ? actualAddonTotalPrice : priceData);
		target.setChannelsAddonTotalPrice(!actualChannelsValue.equals(bd) ? actualChannelsTotalPrice : priceData);
		target.setBundlePrice(createPrice(source, source.getBundlePrice()));
		target.setTotalPrice(createPrice(source, source.getTotalPrice()));
		target.setDebtCheckFlag(source.getDebtCheckFlag());
		target.setIsTelephoneFree(source.getIsTelephoneFree());
		target.setIsCustomerExisting(source.getIsCustomerExisting());
		target.setServiceabilityCheck(!source.getIsRequiredServiceActive());
		target.setCreditCheckAccpetFlag(null != source.getFalloutReasonValue() && source.getFalloutReasonValue().equals(FalloutReasonEnum.CREDIT_CHECK_REFUSED) ? false : true);
		}
		if(llaCommonUtil.isSitePanama()) {
			if(null != source.getPlanTypes()) {
				target.setPlanTypes(planTypeConverter.convert(source.getPlanTypes()));
			}
			if(null != source.getDevicePaymentOption()) {
				target.setDevicePaymentOption(devicePaymentOptionConverter.convert(source.getDevicePaymentOption()));
			}
			buildMonthlyPrices(source,target);
		}
		}



	/**
	 * @param source
	 * @param target
	 */
	private void buildMonthlyPrices(AbstractOrderModel source, AbstractOrderData target)
	{
		Double monthlyPrice=Double.valueOf(0.0);
       for(final AbstractOrderEntryModel entry:source.getEntries()){
           final Collection<PriceRowModel> priceRowModelCollection=entry.getProduct().getOwnEurope1Prices();
           for(final PriceRowModel pricePlanModel:priceRowModelCollection){
               if(pricePlanModel instanceof SubscriptionPricePlanModel){
                   final Collection<RecurringChargeEntryModel> recurringChargeEntries= ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
                   for(final RecurringChargeEntryModel recurringChargeEntry:recurringChargeEntries){
                 	  TmaProductOfferingModel product = (TmaProductOfferingModel) recurringChargeEntry.getSubscriptionPricePlanRecurring().getProduct();
                 	  if(CollectionUtils.isNotEmpty(product.getEurope1Discounts()) && BooleanUtils.isTrue(product.getPromoFlag())) {
                 		  for (DiscountRowModel discount : entry.getProduct().getEurope1Discounts()) {
                 			  if (discount.getAbsolute()) {
                 					monthlyPrice+= discount.getValue()*entry.getQuantity();	
                 			  }
                 			  else {
                 				  monthlyPrice+= recurringChargeEntry.getPrice().doubleValue()*entry.getQuantity() * (discount.getValue() / 100);	
                 			  }
                 		  }
                 	  }
                 	  else {
                      monthlyPrice+=recurringChargeEntry.getPrice().doubleValue()*entry.getQuantity();
                 	  }
                   }
               }
           }
       }
     final List<OrderPriceData> orderPriceDataList=null!=target.getOrderPrices()?target.getOrderPrices():new ArrayList<>();
     final OrderPriceData monthlyPriceData=new OrderPriceData();
     monthlyPriceData.setMonthlyPrice(createPrice(source,monthlyPrice));
     orderPriceDataList.add(monthlyPriceData);
     target.setOrderPrices(orderPriceDataList);
   }
		

	private PriceData createPrice(final AbstractOrderModel source, final Double val)
	{
		if (source == null)
		{
			throw new IllegalArgumentException("source order must not be null");
		}

		final CurrencyModel currency = source.getCurrency();
		if (currency == null)
		{
			throw new IllegalArgumentException("source order currency must not be null");
		}

		// Get double value, handle null as zero
		final double priceValue = val != null ? val.doubleValue() : 0d;

	
		return getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(priceValue), currency);
	}

	/**
	 * @return the planTypeConverter
	 */
	public Converter<PlanTypes, PlanTypeData> getPlanTypeConverter()
	{
		return planTypeConverter;
	}

	/**
	 * @param planTypeConverter the planTypeConverter to set
	 */
	public void setPlanTypeConverter(Converter<PlanTypes, PlanTypeData> planTypeConverter)
	{
		this.planTypeConverter = planTypeConverter;
	}

	/**
	 * @return the devicePaymentOptionConverter
	 */
	public Converter<DevicePaymentOption, DevicePaymentOptionData> getDevicePaymentOptionConverter()
	{
		return devicePaymentOptionConverter;
	}

	/**
	 * @param devicePaymentOptionConverter the devicePaymentOptionConverter to set
	 */
	public void setDevicePaymentOptionConverter(Converter<DevicePaymentOption, DevicePaymentOptionData> devicePaymentOptionConverter)
	{
		this.devicePaymentOptionConverter = devicePaymentOptionConverter;
	}
}
