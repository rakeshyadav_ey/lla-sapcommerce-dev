package com.lla.facades.populators;

import com.lla.core.enums.ContractType;
import com.lla.core.model.CreditScoreModel;
import com.lla.core.util.LLACommonUtil;
import com.lla.facades.order.data.ContractTypeData;
import de.hybris.platform.commercefacades.creditscore.data.CreditScoreData;
import de.hybris.platform.commercefacades.order.converters.populator.OrderPopulator;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.springframework.beans.factory.annotation.Required;
import javax.annotation.Resource;
import de.hybris.platform.core.model.user.CustomerModel;
import com.lla.core.enums.PlanTypes;
import com.lla.facades.product.data.PlanTypeData;
import com.lla.core.enums.DevicePaymentOption;
import com.lla.facades.product.data.DevicePaymentOptionData;
import com.lla.core.enums.ContractType;
import com.lla.facades.order.data.ContractTypeData;

public class LLAOrderPopulator extends OrderPopulator {

	private Converter<ContractType, ContractTypeData> contractTypeConverter;

    private Converter<CreditScoreModel, CreditScoreData> llaCreditScoreConverter;

    @Resource(name="llaCommonUtil")
    private LLACommonUtil llaCommonUtil;

    private Converter<PlanTypes, PlanTypeData> planTypeConverter;
    private Converter<DevicePaymentOption, DevicePaymentOptionData> devicePaymentOptionConverter;

    @Override
    public void populate(final OrderModel source, final OrderData target)
    {
        super.populate(source, target);
        addInstallationAddress(source, target);
        CustomerModel customer=(CustomerModel) source.getUser();
        if(customer.getType().equals(CustomerType.GUEST)){
             target.setPlacedBy(customer.getAdditionalEmail());
        }else{
            target.setPlacedBy(customer.getUid());
        }

        if (llaCommonUtil.isSitePuertorico()) {
            addCreditScoreLevel(source, target);
            addCreditScorePaymentInfo(source, target);
        }
        if (llaCommonUtil.isSitePanama()) {
            addPlanTypeDeviceTypeOption(source, target);
        }
    }
    protected void addCreditScorePaymentInfo(final OrderModel source, final OrderData target)
 	{
 		if (null != source.getAmountDueFirstBill())
 		{
 			target.setAmountDueFirstBill(source.getAmountDueFirstBill());
 		}
 		if (null != source.getAmountDueToday())
 		{
 			target.setAmountDueToday(source.getAmountDueToday());
 		}
 	}
    protected void addInstallationAddress(OrderModel source, OrderData target) {
        if(null != source.getInstallationAddress()) {
            target.setInstallationAddress(getAddressConverter().convert(source.getInstallationAddress()));
        }
        if(null !=source.getContractType()) {
        target.setContractType(getContractTypeConverter().convert(source.getContractType()));
        }
    }

    protected void addCreditScoreLevel(OrderModel source, OrderData target) {
        if (null != source.getCreditScoreLevel()) {
            target.setCreditScoreLevel(getLlaCreditScoreConverter().convert(source.getCreditScoreLevel()));
        }
    }
    protected void addPlanTypeDeviceTypeOption(OrderModel source, OrderData target) {
        if(null !=source.getPlanTypes()) {
            target.setPlanTypes(planTypeConverter.convert(source.getPlanTypes()));
        }
        if(null !=source.getDevicePaymentOption()) {
            target.setDevicePaymentOption(devicePaymentOptionConverter.convert(source.getDevicePaymentOption()));
        }
    }

    /**
	 * @return the contractTypeConverter
	 */
	public Converter<ContractType, ContractTypeData> getContractTypeConverter()
	{
		return contractTypeConverter;
	}

	/**
	 * @param contractTypeConverter the contractTypeConverter to set
	 */
	public void setContractTypeConverter(Converter<ContractType, ContractTypeData> contractTypeConverter)
	{
		this.contractTypeConverter = contractTypeConverter;
	}


    public Converter<CreditScoreModel, CreditScoreData> getLlaCreditScoreConverter() {
        return llaCreditScoreConverter;
    }
  @Required
    public void setLlaCreditScoreConverter(Converter<CreditScoreModel, CreditScoreData> llaCreditScoreConverter){
                this.llaCreditScoreConverter = llaCreditScoreConverter;
	}
    /**
     * @return the planTypeConverter
     */
    public Converter<PlanTypes, PlanTypeData> getPlanTypeConverter(){

        return planTypeConverter;
    }

    /**
     * @param planTypeConverter the planTypeConverter to set
     */
    public void setPlanTypeConverter(Converter<PlanTypes, PlanTypeData> planTypeConverter)
    {
        this.planTypeConverter = planTypeConverter;
    }

    /**
     * @return the devicePaymentOptionConverter
     */
    public Converter<DevicePaymentOption, DevicePaymentOptionData> getDevicePaymentOptionConverter()
    {
        return devicePaymentOptionConverter;
    }

    /**
     * @param devicePaymentOptionConverter the devicePaymentOptionConverter to set
     */
    public void setDevicePaymentOptionConverter(Converter<DevicePaymentOption, DevicePaymentOptionData> devicePaymentOptionConverter)
    {
        this.devicePaymentOptionConverter = devicePaymentOptionConverter;
    }
}
