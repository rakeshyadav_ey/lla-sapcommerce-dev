/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.facades.populators;

import de.hybris.platform.b2ctelcofacades.data.TmaProductSpecCharacteristicValueUseData;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.model.TmaProductSpecCharacteristicValueModel;
import de.hybris.platform.b2ctelcoservices.services.TmaRegionService;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.subscriptionfacades.converters.SubscriptionXStreamAliasConverter;
import de.hybris.platform.subscriptionfacades.data.BillingTimeData;
import de.hybris.platform.subscriptionfacades.data.OrderEntryPriceData;
import de.hybris.platform.subscriptionfacades.order.converters.populator.SubscriptionOrderEntryPopulator;
import de.hybris.platform.subscriptionfacades.order.impl.BillingTimeComparator;
import de.hybris.platform.subscriptionservices.model.BillingFrequencyModel;
import de.hybris.platform.subscriptionservices.model.BillingTimeModel;
import de.hybris.platform.subscriptionservices.model.RecurringChargeEntryModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionPricePlanModel;
import de.hybris.platform.subscriptionservices.subscription.SubscriptionProductService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.lla.core.util.LLACommonUtil;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;


/**
 * Converter for converting order / cart entries. It sets the total and base prices for entries that have multiple
 * billing frequencies (as they are subscription products)
 */
public class LLASubscriptionOrderEntryPopulator extends SubscriptionOrderEntryPopulator
{
    protected static final String SUBSCRIPTION_CATEGORIES = "subscription.categories";
    private static final String TELEVISIONPLANCATEGORY = "tvcabletica";
    private Converter<BillingTimeModel, BillingTimeData> billingTimeConverter;
    private SubscriptionXStreamAliasConverter subscriptionXStreamAliasConverter;
    private Converter<TmaProductSpecCharacteristicValueModel, TmaProductSpecCharacteristicValueUseData> pscvuConverter;
    private SubscriptionProductService subscriptionProductService;
    private Comparator billingTimeComparator;
    @Autowired
    private LLACommonUtil llaCommonUtil;

    @Autowired
    private LLAMulesoftIntegrationUtil llaMulesoftIntegrationUtil;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private CategoryService categoryService;

 	@Autowired
 	private CommerceCategoryService commerceCategoryService;

 	@Autowired
 	private SessionService sessionService;

 	@Autowired
 	private TmaRegionService tmaRegionService;

    @Override
    public void populate(@Nonnull final AbstractOrderEntryModel source, @Nonnull final OrderEntryData target)
    {
        super.populate(source,target);
         if(llaCommonUtil.isSiteVTR())
         {
             if(Objects.nonNull(source.getBasePrice()))
             {
                 target.setBasePrice(createPrice(source,source.getBasePrice()));
             }
             if(Objects.nonNull(source.getMonthlyPrice()))
             {
                 target.setMonthlyPrice(createPrice(source,source.getMonthlyPrice()));
                 target.setTotalPrice(createPrice(source,source.getMonthlyPrice()));
             }
             if(Objects.nonNull(source.getDiscountedValue()))
             {
                 PriceData discountPrice = createPrice(source, source.getDiscountedValue());
                 discountPrice.setDiscountPercentage(source.getDiscountPercentage());
                 discountPrice.setDiscountPeriod(source.getDiscountPeriod());
                 target.setDiscountedPrice(discountPrice);
             }

         }
        boolean containsSubscriptionCategory = false;
        if(!llaCommonUtil.isSitePuertorico() && !llaCommonUtil.isSiteCabletica() && !llaMulesoftIntegrationUtil.isCableticaOrder(source.getOrder().getSite())) {
            final String configuredFixedLineProductsCategories = configurationService.getConfiguration()
                    .getString(SUBSCRIPTION_CATEGORIES, "");
            final List<String> listOfCategories = Stream.of(configuredFixedLineProductsCategories.split(",", -1))
                    .collect(Collectors.toList());
            if (source.getOrder() != null && source.getQuantity() != null) {
                for (final String category : listOfCategories) {
                    if (llaCommonUtil.containsFirstLevelCategoryWithCode(source.getProduct(), category)) {
                        final List<OrderEntryPriceData> orderEntryPrices = new ArrayList<>();
                        orderEntryPrices.add(createOrderEntryPriceDataForEntry(source));
                        target.setOrderEntryPrices(orderEntryPrices);
                        containsSubscriptionCategory = true;
                    }
                }
            }
        }
        if(llaCommonUtil.isSitePanama() || llaCommonUtil.isSiteCabletica()) {
            target.getProduct().setSummary(source.getProduct().getSummary());
            target.getProduct().setDescription(source.getProduct().getDescription());
        }
        if(llaCommonUtil.isSiteJamaica()) {
      	  createProductSpecifications(source, target);
			  if (Objects.nonNull(source.getActualPrices()))
			  {
				  target.setActualPrice(createPrice(source, source.getActualPrices()));
			  }
			  if (Objects.nonNull(source.getDiscountFlag()))
			  {
				  target.setDiscountFlag(source.getDiscountFlag());
			  }
        }
        if (source.getOrder() != null && source.getOrder().getBillingTime() != null && source.getQuantity() != null && !containsSubscriptionCategory)
        {
            target.setOrderEntryPrices(getOrderEntryPricesForEntry(source));
        }
    }

    @Override
	@Nonnull
    protected List<OrderEntryPriceData> getOrderEntryPricesForEntry(@Nonnull final AbstractOrderEntryModel source)
    {
		if (llaCommonUtil.isSitePanama() || llaCommonUtil.isSitePuertorico())
		{

			final List<OrderEntryPriceData> orderEntryPrices = new ArrayList<>();
			final OrderEntryPriceData orderEntryPrice = createOrderEntryPriceDataForEntry(source);
			orderEntryPrices.add(orderEntryPrice);
			return orderEntryPrices;
		}
		else
		{
			final Map<String, OrderEntryPriceData> orderEntryPriceContainer = new HashMap<>();

			final Collection<AbstractOrderEntryModel> abstractOrdersEntries = new ArrayList<>();
			abstractOrdersEntries.add(source);
			if (source.getChildEntries() != null)
			{
				abstractOrdersEntries.addAll(source.getChildEntries());
			}

			for (final AbstractOrderEntryModel abstractOrderEntry : abstractOrdersEntries)
			{
				final OrderEntryPriceData orderEntryPrice = createOrderEntryPriceDataForEntry(abstractOrderEntry);

				if (getSubscriptionProductService().isSubscription(abstractOrderEntry.getProduct()))
				{
					final ProductModel subscriptionProduct = source.getProduct();
					final BillingFrequencyModel productBillingFrequency = subscriptionProduct.getSubscriptionTerm().getBillingPlan()
							.getBillingFrequency();

					if (abstractOrderEntry.getOrder().getBillingTime().equals(productBillingFrequency))
					{
						orderEntryPrice.setDefaultPrice(true);
					}
				}

				orderEntryPriceContainer.put(abstractOrderEntry.getOrder().getBillingTime().getCode(), orderEntryPrice);
			}

			// extract the totals in the same order as the billingtimes are sorted in
			final List<OrderEntryPriceData> orderEntryPrices = new ArrayList<>();
			for (final BillingTimeData billingTime : buildBillingTimes(source))
			{
				final OrderEntryPriceData orderEntryPrice = orderEntryPriceContainer.get(billingTime.getCode());
				orderEntryPrices.add(orderEntryPrice == null ? new OrderEntryPriceData() : orderEntryPrice);
			}
			return orderEntryPrices;
		}

    }

    @Override
	@Nonnull
    protected List<BillingTimeData> buildBillingTimes(@Nonnull final AbstractOrderEntryModel source)
    {
        final List<AbstractOrderModel> children = new ArrayList<>(source.getOrder().getChildren());
        final Stream<BillingTimeModel> sortedBillingTimes = children.stream().map(c -> c.getBillingTime())
                .sorted(getBillingTimeComparator());

        return Stream.concat(Stream.of(source.getOrder().getBillingTime()), sortedBillingTimes)
                .map(billingTime -> getBillingTimeConverter().convert(billingTime)).collect(Collectors.toList());
    }

    @Override
	@Nonnull
    protected OrderEntryPriceData createOrderEntryPriceDataForEntry(@Nonnull final AbstractOrderEntryModel entry)
    {
        final OrderEntryPriceData orderEntryPrice = new OrderEntryPriceData();
        orderEntryPrice.setTotalPrice(createPrice(entry, entry.getTotalPrice()));
        orderEntryPrice.setBasePrice(createPrice(entry, entry.getBasePrice()));

         Collection<PriceRowModel> priceRowModelCollection=entry.getProduct().getOwnEurope1Prices();
            for(PriceRowModel pricePlanModel:priceRowModelCollection){
                if(pricePlanModel instanceof SubscriptionPricePlanModel){
               	 Collection<RecurringChargeEntryModel> recurringChargeEntries = new ArrayList<RecurringChargeEntryModel>();
             		 if(llaCommonUtil.isSiteCabletica() && entry.getProduct().getSupercategories().contains(commerceCategoryService.getCategoryForCode(TELEVISIONPLANCATEGORY))) {
             			final String llaregion = sessionService.getAttribute("llaregion");
             			final RegionModel regionModel = tmaRegionService.findRegionByIsocode(llaregion);
             			if(null != regionModel && ((SubscriptionPricePlanModel) pricePlanModel).getRegions().contains(regionModel)) {
                			recurringChargeEntries =  ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
             			}
             		 }
             		 else {
               	 recurringChargeEntries= ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
             		 }
                   if(llaCommonUtil.isSiteJamaica() && null != entry.getMonthlyPrice() && entry.getMonthlyPrice() != 0.0) {
                  	 orderEntryPrice.setMonthlyPrice(createPrice(entry, entry.getMonthlyPrice()));
                   }
                   if(llaCommonUtil.isSitePanama() && null != entry.getMonthlyPrice() && entry.getMonthlyPrice() != 0.0) {
                  	 orderEntryPrice.setMonthlyPrice(createPrice(entry, entry.getMonthlyPrice()));
                   }
                   else {
                  	 for(RecurringChargeEntryModel recurringChargeEntry:recurringChargeEntries) {
                  		 if(llaCommonUtil.isSiteCabletica()) {
                  			 double tax13Price = 0.0;
                       	    double tax911Price = 0.0;

                       	   if(null != recurringChargeEntry.getTax911()){
           						tax911Price = recurringChargeEntry.getTax911().doubleValue();
                       	   orderEntryPrice.setTax911Price(createPrice(entry,tax911Price));
                  		   }
           					   if(null != recurringChargeEntry.getTax13Percent()) {
           						tax13Price = recurringChargeEntry.getTax13Percent().doubleValue();
           						orderEntryPrice.setTax13Price(createPrice(entry,tax13Price));
           					   }

                   		   orderEntryPrice.setMonthlyPrice(createPrice(entry,entry.getMonthlyPrice().doubleValue()*entry.getQuantity()));

                   		 }

                   		 if(llaCommonUtil.isSiteCabletica() && entry.getProduct().getSupercategories().contains(commerceCategoryService.getCategoryForCode(TELEVISIONPLANCATEGORY))) {
                   			 final Map<String, Double> llaregionPrices = (Map<String, Double>) sessionService.getAttribute("llaregionPrices");
                   			 if(!llaregionPrices.isEmpty()) {
                   			 for (Map.Entry<String, Double> set : llaregionPrices.entrySet()) {
                   				 if(entry.getProduct().getCode().equals(set.getKey())){
                         			 orderEntryPrice.setMonthlyPrice(createPrice(entry,set.getValue()*entry.getQuantity()));
                   				 }
                   			  }
                   			}
                   		 }else {
                   			 if(!llaCommonUtil.isSiteCabletica() && !llaCommonUtil.isSitePanama() && !llaCommonUtil.isSiteJamaica()) {
                                 orderEntryPrice.setMonthlyPrice(createPrice(entry, recurringChargeEntry.getPrice().doubleValue() * entry.getQuantity()));
                                 orderEntryPrice.setBasePrice(createPrice(entry,recurringChargeEntry.getPrice().doubleValue()));
                             }
                   		 }
                  	 }
                   }
                }
            }


        final BillingTimeData billingTime = getBillingTimeConverter().convert(entry.getOrder().getBillingTime());
        orderEntryPrice.setBillingTime(billingTime);
        return orderEntryPrice;
    }

    @Nonnull
    protected void createProductSpecifications(@Nonnull final AbstractOrderEntryModel entry, final OrderEntryData data) {
  		final TmaProductOfferingModel product = (TmaProductOfferingModel) entry.getProduct();
  		Set<TmaProductSpecCharacteristicValueModel> spec = product.getProductSpecCharacteristicValues();
  		List<TmaProductSpecCharacteristicValueUseData> pscvList = new ArrayList<>();
  		if(!spec.isEmpty()) {
  			pscvList.addAll(getPscvuConverter().convertAll(spec));
 			data.getProduct().setProductSpecCharValueUses(pscvList);
  		}
  	}

    @Override
	protected Converter<BillingTimeModel, BillingTimeData> getBillingTimeConverter()
    {
        return billingTimeConverter;
    }

    @Override
	@Required
    public void setBillingTimeConverter(final Converter<BillingTimeModel, BillingTimeData> billingFrequencyConverter)
    {
        this.billingTimeConverter = billingFrequencyConverter;
    }

    @Override
	protected SubscriptionXStreamAliasConverter getSubscriptionXStreamAliasConverter()
    {
        return subscriptionXStreamAliasConverter;
    }

    @Override
	@Required
    public void setSubscriptionXStreamAliasConverter(final SubscriptionXStreamAliasConverter subscriptionXStreamAliasConverter)
    {
        this.subscriptionXStreamAliasConverter = subscriptionXStreamAliasConverter;
    }

    /**
     * @return subscription product service.
     */
    @Override
	protected SubscriptionProductService getSubscriptionProductService()
    {
        return subscriptionProductService;
    }

    @Override
	@Required
    public void setSubscriptionProductService(final SubscriptionProductService subscriptionProductService)
    {
        this.subscriptionProductService = subscriptionProductService;
    }

    @Override
	protected Comparator getBillingTimeComparator()
    {
        return billingTimeComparator;
    }

    @Override
	@Required
    public void setBillingTimeComparator(final BillingTimeComparator billingTimeComparator)
    {
        this.billingTimeComparator = billingTimeComparator;
    }

    public Converter<TmaProductSpecCharacteristicValueModel, TmaProductSpecCharacteristicValueUseData> getPscvuConverter()
 	 {
 		  return pscvuConverter;
 	 }

 	 @Required
 	 public void setPscvuConverter(
 			Converter<TmaProductSpecCharacteristicValueModel, TmaProductSpecCharacteristicValueUseData> pscvuConverter)
 	 {
 		  this.pscvuConverter = pscvuConverter;
 	 }

}
