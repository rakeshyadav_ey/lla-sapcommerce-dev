package com.lla.facades.populators;

import com.lla.core.model.ProvinceModel;
import com.lla.facades.address.ProvinceData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class ProvincePopulator implements Populator<ProvinceModel, ProvinceData> {
    /**
     * Populate the target instance with values from the source instance.
     *
     * @param provinceModel the source object
     * @param provinceData  the target to fill
     * @throws ConversionException if an error occurs
     */
    @Override
    public void populate(ProvinceModel provinceModel, ProvinceData provinceData) throws ConversionException {
        provinceData.setName(provinceModel.getLabel());
        provinceData.setCode(provinceModel.getUid());
    }
}
