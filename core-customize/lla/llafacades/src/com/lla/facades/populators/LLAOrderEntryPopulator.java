package com.lla.facades.populators;

import com.lla.core.util.LLACommonUtil;
import com.lla.mulesoft.integration.util.LLAMulesoftIntegrationUtil;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

public class LLAOrderEntryPopulator<SOURCE extends AbstractOrderEntryModel, TARGET extends OrderEntryData>
        implements Populator<SOURCE, TARGET> {


    @Autowired
    private LLACommonUtil llaCommonUtil;
    @Autowired
    private PriceDataFactory priceDataFactory;
    @Override
    public void populate(SOURCE source, TARGET target) throws ConversionException {
       if(source.getProduct() instanceof TmaProductOfferingModel && StringUtils.isNotEmpty(((TmaProductOfferingModel) source.getProduct()).getFmcPromotionCode())){
            String message=((TmaProductOfferingModel)source.getProduct()).getFmcPromotionMessage();
            target.setFmcPromotionMessage(message);
            target.setFmcRuleApplied(Boolean.TRUE);
        }

       if(llaCommonUtil.isSitePuertorico() && null != source.getEntryRecurringPrice()){

           target.setEntryRecurringPrice(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(source.getEntryRecurringPrice()), source.getOrder().getCurrency()));
        }

    }
}