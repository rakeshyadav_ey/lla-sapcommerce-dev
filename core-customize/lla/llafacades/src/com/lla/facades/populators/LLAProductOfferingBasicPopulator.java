package com.lla.facades.populators;

import com.google.common.collect.Lists;
import de.hybris.platform.b2ctelcofacades.converters.populator.TmaProductOfferingBasicPopulator;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.b2ctelcofacades.data.TmaProductSpecCharacteristicValueUseData;
import de.hybris.platform.b2ctelcofacades.data.TmaProductSpecificationData;
import de.hybris.platform.b2ctelcoservices.model.TmaProductSpecCharacteristicValueModel;
import de.hybris.platform.b2ctelcoservices.model.TmaProductSpecificationModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.site.BaseSiteService;


import java.lang.reflect.Array;
import java.util.*;


import javax.annotation.Resource;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import javax.annotation.Resource;
import de.hybris.platform.subscriptionservices.model.RecurringChargeEntryModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionPricePlanModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.lla.common.addon.model.TvChannelModel;
import com.lla.core.util.LLACommonUtil;
import com.lla.facades.channel.data.ChannelData;

import javax.annotation.Resource;


public class LLAProductOfferingBasicPopulator<SOURCE extends ProductModel, TARGET extends ProductData> extends TmaProductOfferingBasicPopulator<SOURCE, TARGET> {

	private Converter<TmaProductSpecCharacteristicValueModel, TmaProductSpecCharacteristicValueUseData> pscvuConverter;
	private Converter<TmaProductSpecificationModel, TmaProductSpecificationData> productSpecificationConverter;
	private static final String CATEGORY_BUNDLE = "bundle";
    @Resource
    private CartService cartService;

    @Resource(name = "productService")
    private ProductService productService;

    @Autowired
    private LLACommonUtil llaCommonUtil;

    @Autowired
    private PriceDataFactory priceDataFactory;

    @Autowired
    private Converter<ProductModel, ProductData> productConverter;
    
    @Autowired
    private BaseSiteService baseSiteService;
    
    @Autowired
    private CommonI18NService commonI18NService;

    @Override
    public void populate(final SOURCE productModel, final TARGET productData) {
        super.populate(productModel, productData);
        productData.setIsHomepageProduct(BooleanUtils.isTrue(productModel.getIsHomepageProduct()));
        productData.setIsPLPProduct(BooleanUtils.isTrue(productModel.getIsPLPProduct()));
        if ((llaCommonUtil.isSitePanama() || llaCommonUtil.isSiteJamaica() || llaCommonUtil.isSiteCabletica()) && productModel instanceof TmaProductOfferingModel) {
            productData.setServicesIncluded(((TmaProductOfferingModel) productModel).getServicesIncluded());
            productData.setChannelList(getChannelList(productModel.getChannelList()));
            productData.setProductDisclaimer(((TmaProductOfferingModel) productModel).getProductDisclaimer());
            productData.setWhatYouGet(((TmaProductOfferingModel) productModel).getWhatYouGet());
	     productData.setIsVisibleOnPLP(((TmaProductOfferingModel) productModel).getIsVisibleOnPLP() != null ? ((TmaProductOfferingModel) productModel).getIsVisibleOnPLP() : Boolean.TRUE);	
            if(!llaCommonUtil.isSiteCabletica()) {
            productData.setBundlePresentInCart(bundlePresentInCart());
            }
			// START CODE for TSE-1527
			if(llaCommonUtil.isSitePanama()) {
				productData.setSubTitle1(((TmaProductOfferingModel) productModel).getSubTitle1());
				productData.setSubTitle2(((TmaProductOfferingModel) productModel).getSubTitle2());
				if(((TmaProductOfferingModel) productModel).getPromoFlag() != null && ((TmaProductOfferingModel) productModel).getPromoFlag())
				{
					productData.setPromotionMessage(((TmaProductOfferingModel) productModel).getPromotionMessage());
				}

			}
			// END CODE for TSE-1527
            productData.setMonthlyPrice(setMonthlyPrice((TmaProductOfferingModel) productModel));
            final Set<TmaProductSpecCharacteristicValueModel> pscvuModels = ((TmaProductOfferingModel) productModel).getProductSpecCharacteristicValues();
            productData.setProductSpecCharValueUses(getPscvuDataListFromModels(((TmaProductOfferingModel) productModel).getProductSpecification(), pscvuModels));

            if(null!= ((TmaProductOfferingModel) productModel).getPopularFlag())
				{
					productData.setPopularFlag(((TmaProductOfferingModel) productModel).getPopularFlag());
				}
            if(null != ((TmaProductOfferingModel) productModel).getFmcSequenceId())
				{
					productData.setSequenceId(((TmaProductOfferingModel) productModel).getFmcSequenceId());
				}
				if (null != ((TmaProductOfferingModel) productModel).getPromoFlag())
				{
					productData.setPromoFlag(((TmaProductOfferingModel) productModel).getPromoFlag());
				}

        }
        if(llaCommonUtil.isSitePuertorico()){
            if(null != productModel && StringUtils.isNotEmpty(productModel.getRemarks())){
                productData.setRemarks(productModel.getRemarks());
            }

            if(null != productModel && StringUtils.isNotEmpty(productModel.getCode())) {
            	productData.setCode(productModel.getCode());
            }
        }
        if(llaCommonUtil.isSiteCabletica()) {
      	  if(null != productModel && StringUtils.isNotEmpty(productModel.getSummary())) {
      		  productData.setSummary(((TmaProductOfferingModel) productModel).getSummary());
      	  }
        }
		if (llaCommonUtil.isSiteJamaica()){
			final TmaProductOfferingModel productFeatureModel = ((TmaProductOfferingModel) productModel);
			productData.setProductFeatureList(llaCommonUtil.setProductFeatureList(productFeatureModel));
		}

		if ((llaCommonUtil.isSiteVTR() || llaCommonUtil.isSitePuertorico()) && productModel instanceof TmaProductOfferingModel)
		{
			final TmaProductOfferingModel product = ((TmaProductOfferingModel) productModel);
			productData.setDescription(productModel.getDescription());
			if(null != productModel.getIsHomepageProduct())
			productData.setIsHomepageProduct(productModel.getIsHomepageProduct());
			if(StringUtils.isNotEmpty(productModel.getHomepageProductSequence()))
			productData.setHomepageProductSequence(productModel.getHomepageProductSequence());
			if(null != product.getPopularFlag())
			productData.setPopularFlag(product.getPopularFlag());
			if(StringUtils.isNotEmpty(product.getExtensorTag()))
			productData.setExtensorTag(product.getExtensorTag());
			if(StringUtils.isNotEmpty(product.getMostSoldTag()))
			productData.setMostSoldTag(product.getMostSoldTag());
			if(!CollectionUtils.isEmpty(product.getAssociatedProductCode()))
			productData.setAssociatedProductCode(product.getAssociatedProductCode());
			productData.setProductFeatureList(llaCommonUtil.setProductFeatureList(product));
			if (!CollectionUtils.isEmpty(product.getChannelList()))
			{
				productData.setChannelList(getChannelList(product.getChannelList()));
			}
			if(product.getNewFlag()!=null)
			{
				productData.setNewFlag(product.getNewFlag());
			}
			if(StringUtils.isNotEmpty(product.getWebPortabilityTag()))
			productData.setWebPortabilityTag(product.getWebPortabilityTag());
			if(!CollectionUtils.isEmpty(product.getFfthAddonProductCode()))
			productData.setFfthAddonProductCode(product.getFfthAddonProductCode());
			if(!CollectionUtils.isEmpty(product.getHfcAddonProductCode()))
			productData.setHfcAddonProductCode(product.getHfcAddonProductCode());
			if(StringUtils.isNotEmpty(product.getIncludeDBox()))
			productData.setIncludeDBox(product.getIncludeDBox());
			if(StringUtils.isNotEmpty(product.getIncludeExtensor()))
			productData.setIncludeExtensor(product.getIncludeExtensor());
			if(Objects.nonNull(product.getMaxOrderQuantity()))
			{
				productData.setMaxOrderQuantity(product.getMaxOrderQuantity().longValue());
			}
			if(null != product.getFmcSequenceId())
			{
				productData.setSequenceId(product.getFmcSequenceId());
			}
			if(null !=product.getColorCode()) {
				productData.setColorCode(product.getColorCode());
			}
			if(null !=product.getMetaDescription(getCurrentLocale())) {
				productData.setMetaDescription(product.getMetaDescription(getCurrentLocale()));
			}
			if(null !=product.getPageTitle(getCurrentLocale())) {
				productData.setPageTitle(product.getPageTitle(getCurrentLocale()));
			}
			if(null !=product.getDeviceSlug(getCurrentLocale())) {
				productData.setDeviceSlug(product.getDeviceSlug(getCurrentLocale()));
			}
		}
		if(llaCommonUtil.isSiteJamaica() && productModel instanceof TmaProductOfferingModel){
			final TmaProductOfferingModel product = ((TmaProductOfferingModel) productModel);
			if (!CollectionUtils.isEmpty(product.getChannelList())) {
				productData.setCategorySpecificChannels(getCategorySpecificChannels(product.getChannelList()));
			}
			//TSE-127 : setting discount period and number.
			if(BooleanUtils.isTrue(product.getPromoFlag()))
			{
				if(Objects.nonNull(product.getDiscountNumber())) {
					productData.setDiscountNumber(product.getDiscountNumber());
				}
				if(Objects.nonNull(product.getDiscountPeriod()))
				{
					productData.setDiscountPeriod(product.getDiscountPeriod());
				}
			}
		}
    }

	/**
	 * @param productSpecificationModel
	 * @param pscvuModels
	 * @return
	 */
	private List<TmaProductSpecCharacteristicValueUseData> getPscvuDataListFromModels(
			final TmaProductSpecificationModel productSpecificationModel, final Set<TmaProductSpecCharacteristicValueModel> pscvuModels)
	{
		final List<TmaProductSpecCharacteristicValueUseData> pscvuDataList = new ArrayList<>();
		if (CollectionUtils.isEmpty(pscvuModels))
		{
			return pscvuDataList;
		}
		pscvuModels.forEach(pscvuModel -> {
		final TmaProductSpecCharacteristicValueUseData pscvuData = getPscvuConverter().convert(pscvuModel);
		if (productSpecificationModel != null)
		{
			pscvuData.setProductSpecification(getProductSpecificationConverter().convert(productSpecificationModel));
		}
		pscvuDataList.add(pscvuData);
		});
	return pscvuDataList;
	}

	/**
	 * @param productModel
	 * @return
	 */
	private PriceData setMonthlyPrice(final TmaProductOfferingModel productModel)
	{
		Double monthlyPrice = Double.valueOf(0.0);
		 double tax13Price = 0.0;
  	   double tax911Price = 0.0;
		final Collection<PriceRowModel> priceRowModelCollection = productModel.getOwnEurope1Prices();
		for (final PriceRowModel pricePlanModel : priceRowModelCollection)
		{
			if (pricePlanModel instanceof SubscriptionPricePlanModel)
			{
				final Collection<RecurringChargeEntryModel> recurringChargeEntries = ((SubscriptionPricePlanModel) pricePlanModel)
						.getRecurringChargeEntries();
				for (final RecurringChargeEntryModel recurringChargeEntry : recurringChargeEntries)
				{
					if(null != recurringChargeEntry.getTax911())
						tax911Price = recurringChargeEntry.getTax911().doubleValue();
					if(null != recurringChargeEntry.getTax13Percent())
						tax13Price = recurringChargeEntry.getTax13Percent().doubleValue();
					
						monthlyPrice += recurringChargeEntry.getPrice().doubleValue() + tax911Price + tax13Price;
		
				
				}
			}
			final CurrencyModel currency = pricePlanModel.getCurrency();
			final double priceValue = monthlyPrice != null ? monthlyPrice.doubleValue() : 0d;
			return priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(priceValue), currency);
		}
		return null;
	}

	private List<ChannelData> getChannelList(final Set<TvChannelModel> channelList)
	{
		final List<ChannelData> list = new ArrayList<>();
		channelList.stream().forEach(channel -> {
			final ChannelData data = new ChannelData();
			data.setChannelName(channel.getChannelName());
			data.setChannelNumber(channel.getChannelNumber().toString());
			if (llaCommonUtil.isSiteVTR())
			{
				data.setChannelCategory(channel.getChannelCategory());
				data.setChannelType(channel.getChannelType().getCode());
				if (Objects.nonNull(channel.getChannelImage()))
				{
					data.setChannelImageURL(channel.getChannelImage().getURL());
					data.setImageAltTxt(channel.getChannelImage().getAltText());
				}

			}
			if(llaCommonUtil.isSiteJamaica()){
				data.setChannelCategory(channel.getChannelCategory());
				if(Objects.nonNull(channel.getChannelType())){
					data.setChannelType(channel.getChannelType().getCode());
				}
			}
			list.add(data);
		});
		list.sort(Comparator.comparingInt((final ChannelData d) -> Integer.parseInt(d.getChannelNumber())));
		return list;
	}

    /**
     * @param channelList
     * @return a map of key-value pairs of <{@link String} and {@link ChannelData}>.
     */
	private Map<String, List<ChannelData>> getCategorySpecificChannels(Set<TvChannelModel> channelList)
	{
		Map<String,List<ChannelData>>channelDataMap=new HashMap<String, List<ChannelData>>();
		List<ChannelData> channelDataList=getChannelList(channelList);
		channelDataList.parallelStream().forEach(channel -> {
			 if(StringUtils.isNotEmpty(channel.getChannelCategory()))
			 {
				 List<ChannelData> newChannelList=Lists.newArrayList(channelDataList.parallelStream().filter(category->category.getChannelCategory().equals(channel.getChannelCategory())).collect(Collectors.toSet()));
				 newChannelList.sort(Comparator.comparingInt((final ChannelData d) -> Integer.parseInt(d.getChannelNumber())));
				 channelDataMap.put(channel.getChannelCategory(), newChannelList);
			 }
		});
		return MapUtils.isNotEmpty(channelDataMap) ? channelDataMap :MapUtils.EMPTY_SORTED_MAP;
	}

	private boolean bundlePresentInCart()
	{
		final CartModel cartModel = cartService.getSessionCart();
		for (final AbstractOrderEntryModel entry : cartModel.getEntries())
		{
			final ProductModel product = productService.getProductForCode(entry.getProduct().getCode());
			if (llaCommonUtil.containsSecondLevelCategoryWithCode(product, CATEGORY_BUNDLE))
			{
				return true;
			}
		}
		return false;
	}
	
	private Locale getCurrentLocale(){
      BaseSiteModel baseSite =baseSiteService.getCurrentBaseSite();
       Locale locale = commonI18NService.getLocaleForLanguage(baseSite.getDefaultLanguage());
	    return locale;
  }

	/**
	 * @return the productSpecificationConverter
	 */
	public Converter<TmaProductSpecificationModel, TmaProductSpecificationData> getProductSpecificationConverter()
	{
		return productSpecificationConverter;
	}

	/**
	 * @param productSpecificationConverter
	 *           the productSpecificationConverter to set
	 */
	public void setProductSpecificationConverter(
			final Converter<TmaProductSpecificationModel, TmaProductSpecificationData> productSpecificationConverter)
	{
		this.productSpecificationConverter = productSpecificationConverter;
	}

	/**
	 * @return the pscvuConverter
	 */
	public Converter<TmaProductSpecCharacteristicValueModel, TmaProductSpecCharacteristicValueUseData> getPscvuConverter()
	{
		return pscvuConverter;
	}

	/**
	 * @param pscvuConverter
	 *           the pscvuConverter to set
	 */
	public void setPscvuConverter(
			final Converter<TmaProductSpecCharacteristicValueModel, TmaProductSpecCharacteristicValueUseData> pscvuConverter)
	{
		this.pscvuConverter = pscvuConverter;
	}
}
