package com.lla.facades.populators;

import com.lla.core.model.DistrictModel;
import com.lla.core.model.NeighbourhoodModel;
import com.lla.facades.address.DistrictData;
import com.lla.facades.address.NeighbourHoodData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class NeighbourHoodPopulator implements Populator<NeighbourhoodModel, NeighbourHoodData> {
    /**
     * Populate the target instance with values from the source instance.
     *
     * @param source the source object
     * @param target  the target to fill
     * @throws ConversionException if an error occurs
     */
    @Override
    public void populate(NeighbourhoodModel source, NeighbourHoodData target) throws ConversionException {
        target.setName(source.getLabel());
        target.setCode(source.getUid());
    }
}
