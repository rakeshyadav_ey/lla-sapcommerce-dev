package com.lla.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.AbstractOrderPopulator;
import de.hybris.platform.commercefacades.order.converters.populator.ExtendedCartPopulator;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;
import java.math.BigDecimal;

public class LLAExtendedCartPopulator extends ExtendedCartPopulator {
    @Resource(name = "priceDataFactory")
    private PriceDataFactory priceDataFactory;

    @Override
    public void populate(final CartModel source, final CartData target) {
        super.populate(source,target);
        target.setTotalRecurringPrice(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(source.getTotalRecurringPrice().doubleValue()), source.getCurrency()));
    }
}

