package com.lla.facades.populators;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.lang.StringUtils;

public class LLACustomerPopulator implements Populator<CustomerModel, CustomerData>
{
    private static final String DRIVER_LICENSE = "DRIVER_LICENSE";
    private static final String PASSPORT = "PASSPORT";
    @Override
    public void populate(CustomerModel customerModel, CustomerData customerData) throws ConversionException {
        customerData.setFirstName(customerModel.getFirstName());
        setUid(customerModel,customerData);
        customerData.setLastName(customerModel.getLastName());
        customerData.setTrnNumber(customerModel.getTrnNumber());
        customerData.setDob(customerModel.getDateOfBirth());
        customerData.setEmailAddress(customerModel.getUid());
        customerData.setGender(null!=customerModel.getGender()?customerModel.getGender().getCode(): StringUtils.EMPTY);
        customerData.setTitle(null!=customerModel.getTitle()?customerModel.getTitle().getCode(): StringUtils.EMPTY);
        customerData.setCustomerType(null!=customerModel.getType()?customerModel.getType().toString(): StringUtils.EMPTY);
        customerData.setFixedPhone(null!=customerModel.getFixedPhone()?customerModel.getFixedPhone():StringUtils.EMPTY);
        customerData.setCustomerId(customerModel.getCustomerID());
        customerData.setAdditionalEmail(customerModel.getAdditionalEmail());
        customerData.setDocumentType(customerModel.getDocumentType());
        customerData.setDocumentNumber(customerModel.getDocumentNumber());
        if(DRIVER_LICENSE.equals(customerModel.getDocumentType()))
        {
            customerData.setDriversLicence(customerModel.getDocumentNumber());
        }
        if(PASSPORT.equals(customerModel.getDocumentType()))
        {
            customerData.setPassport(customerModel.getDocumentNumber());
        }
        customerData.setCustomerType(null!=customerModel.getType()?customerModel.getType().toString(): StringUtils.EMPTY);
        customerData.setMobilePhone(null!=customerModel.getMobilePhone()?customerModel.getMobilePhone(): StringUtils.EMPTY);
        customerData.setMobilePhoneSecondary(null!=customerModel.getMobilePhoneSecondary()?customerModel.getMobilePhoneSecondary(): StringUtils.EMPTY);
        customerData.setFixedPhone(null!=customerModel.getFixedPhone()?customerModel.getFixedPhone():StringUtils.EMPTY);
        customerData.setRutNumber(null!=customerModel.getRutNumber()?customerModel.getRutNumber():StringUtils.EMPTY);
        customerData.setMobilePhone(null!=customerModel.getMobilePhone()?customerModel.getMobilePhone():StringUtils.EMPTY);
    }
    protected void setUid(final UserModel source, final CustomerData target)
    {
        target.setUid(source.getUid());
        if (source instanceof CustomerModel)
        {
            final CustomerModel customer = (CustomerModel) source;
            if (isOriginalUidAvailable(customer))
            {
                target.setDisplayUid(customer.getOriginalUid());
            }
        }
    }

    protected boolean isOriginalUidAvailable(final CustomerModel source)
    {
        return source.getOriginalUid() != null;
    }
}