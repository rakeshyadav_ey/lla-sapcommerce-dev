package com.lla.facades.populators;

import com.lla.core.enums.DriverLicenceState;
import com.lla.core.enums.ProductRegion;
import com.lla.facades.customer.data.DriverLicenceStateData;
import com.lla.facades.product.data.ProductRegionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.type.TypeService;
import org.springframework.beans.factory.annotation.Required;

public class DriverLicenceStateDataPopulator implements Populator<DriverLicenceState, DriverLicenceStateData> {
    private TypeService typeService;

    protected TypeService getTypeService()
    {
        return typeService;
    }

    @Required
    public void setTypeService(final TypeService typeService)
    {
        this.typeService = typeService;
    }

    /**
     * Populate the target instance with values from the source instance.
     *
     * @param source     the source object
     * @param target the target to fill
     * @throws ConversionException if an error occurs
     */
    @Override
    public void populate(DriverLicenceState source, DriverLicenceStateData target) throws ConversionException {
        target.setCode(source.getCode());
        target.setName(getTypeService().getEnumerationValue(source).getName());
    }
}