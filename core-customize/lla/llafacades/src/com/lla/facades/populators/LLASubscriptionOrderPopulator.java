/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.facades.populators;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.subscriptionfacades.data.OrderEntryPriceData;
import de.hybris.platform.subscriptionfacades.data.OrderPriceData;
import de.hybris.platform.subscriptionfacades.order.converters.populator.SubscriptionOrderPopulator;
import de.hybris.platform.subscriptionservices.model.RecurringChargeEntryModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionPricePlanModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;

import com.lla.core.util.LLACommonUtil;
import org.apache.commons.lang3.StringUtils;

/**
 * Converter for converting order / cart entries. It sets the total and base prices for entries that have multiple
 * billing frequencies (as they are subscription products)
 */
public class LLASubscriptionOrderPopulator extends SubscriptionOrderPopulator
{
    @Autowired
    private  LLACommonUtil llaCommonUtil;

    @Autowired
    private LLACommonPopulatorUtils llaCommonPopulatorUtils;
    @Override
    public void populate(@Nonnull final OrderModel source,
                         @Nonnull final OrderData target) throws ConversionException{
        super.populate(source,target);
        double regularMonthlyPrice = 0.0;
        if(llaCommonUtil.isSitePanama()){
      	  llaCommonPopulatorUtils.populate(source, target);
        }if(llaCommonUtil.isSitePuertorico()){
      	  llaCommonPopulatorUtils.populate(source, target);
            llaCommonUtil.setBundleProductPriceLable(source, target);
        }
        if (llaCommonUtil.isSiteJamaica()) {
          for (final AbstractOrderEntryModel entry : source.getEntries()) {
              regularMonthlyPrice+= Objects.nonNull(entry.getActualPrices())? entry.getActualPrices() :Double.valueOf(0.0D) ;
          }
           target.setRegularMonthlyPrice(createPrice(source,regularMonthlyPrice));

          if(Objects.nonNull(source.getOrderMonthlyPrice()))
            {
                target.setMonthlyTotalPrice(createPrice(source,source.getOrderMonthlyPrice()));
            }
            if(Objects.nonNull(source.getTotalDiscounts()))
            {
                target.setTotalDiscounts(createPrice(source,source.getTotalDiscounts()));
            }
            if(Objects.nonNull(source.getDiscountPeriod()))
            {
                target.setDiscountPeriod(source.getDiscountPeriod());
            }
            if(Objects.nonNull(source.getDiscountNumber()))
            {
                target.setDiscountNumber(source.getDiscountNumber());
            }
          }

          final CustomerModel customer=(CustomerModel) source.getUser();
          final String uid = org.apache.commons.lang.StringUtils.substringAfter(customer.getUid(),"|");
        if(!StringUtils.isEmpty(uid)) {
            target.setPlacedBy(uid);
        }
        else {
             target.setPlacedBy(customer.getAdditionalEmail());
        }

        if (llaCommonUtil.isSiteCabletica()) {
           double monthlyCableticaTotalPrice = 0.0;
           double totalPriceWithTax = 0.0;
           for (final OrderEntryData data : target.getEntries()) {
              for (final OrderEntryPriceData price : data.getOrderEntryPrices()) {
					  monthlyCableticaTotalPrice += price.getMonthlyPrice().getValue().doubleValue();
              final PriceData tax911 = price.getTax911Price();
              final PriceData tax13 = price.getTax13Price();

              	for (final CategoryData categoryData : data.getProduct().getCategories())
					{
						if(!categoryData.getCode().equals("channelsaddoncabletica")) {
							if (null != tax911 && null != tax13)
							{
								totalPriceWithTax += price.getMonthlyPrice().getValue().doubleValue()+tax911.getValue().doubleValue()+tax13.getValue().doubleValue();
							}
						}
					}
              }
            }
			target.setTax911Amount(createPrice(source, source.getTax911Price()));
			target.setTax13Amount(createPrice(source, source.getTax13Price()));
			target.setMonthlyTotalPrice(createPrice(source, source.getOrderMonthlyPrice()));
			target.setTotalPriceWithTax(createPrice(source, totalPriceWithTax));
       }

    }
}
