package com.lla.facades.populators;

import com.lla.core.util.LLACommonUtil;
import de.hybris.platform.commercefacades.order.converters.populator.OrderHistoryPopulator;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.subscriptionfacades.data.OrderPriceData;
import de.hybris.platform.subscriptionservices.model.RecurringChargeEntryModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionPricePlanModel;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class LLAOrderHistoryPopulator extends OrderHistoryPopulator {
    @Autowired
    private LLACommonUtil llaCommonUtil;
    @Override
    public void populate(final OrderModel source, final OrderHistoryData target)
    {
        super.populate(source, target);
        if(llaCommonUtil.isSitePanama()){
            buildMonthlyPrices(source,target);
        }
        if(llaCommonUtil.isSitePuertorico()){
            target.setTotal(createPrice(source, source.getTotalPrice()));
        }
    }

    private void buildMonthlyPrices(OrderModel source, OrderHistoryData target) {
        Double monthlyPrice=Double.valueOf(0.0);
        for(AbstractOrderEntryModel entry:source.getEntries()){
            Collection<PriceRowModel> priceRowModelCollection=entry.getProduct().getOwnEurope1Prices();
            for(PriceRowModel pricePlanModel:priceRowModelCollection){
                if(pricePlanModel instanceof SubscriptionPricePlanModel){
                    final Collection<RecurringChargeEntryModel> recurringChargeEntries= ((SubscriptionPricePlanModel) pricePlanModel).getRecurringChargeEntries();
                    for(RecurringChargeEntryModel recurringChargeEntry:recurringChargeEntries){
                        monthlyPrice+=recurringChargeEntry.getPrice().doubleValue()*entry.getQuantity();
                    }
                }
            }
        }
        List<OrderPriceData> orderPriceDataList=null!=target.getOrderHistoryPrices()?target.getOrderHistoryPrices():new ArrayList<>();
        OrderPriceData monthlyPriceData=new OrderPriceData();
        monthlyPriceData.setMonthlyPrice(createPrice(source,monthlyPrice));
        orderPriceDataList.add(monthlyPriceData);
        target.setOrderHistoryPrices(orderPriceDataList);
    }

    protected PriceData createPrice(final AbstractOrderModel orderModel, final Double val)
    {
        return getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(val.doubleValue()),
                orderModel.getCurrency());
    }

}
