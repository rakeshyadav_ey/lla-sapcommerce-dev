package com.lla.facades.populators;
import com.lla.core.enums.AddressAreaTypeEnum;
import com.lla.core.enums.LLATechnologyEnum;
import com.lla.core.enums.RuralResidenceEnum;
import com.lla.core.enums.UrbanResidenceEnum;
import de.hybris.platform.b2ctelcofacades.converters.populator.TmaAddressReversePopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;

import com.lla.core.util.LLACommonUtil;
import org.apache.commons.lang.StringUtils;

import java.util.Objects;


public class LLAAddressReversePopulator extends TmaAddressReversePopulator
{


	@Resource(name = "llaCommonUtil")
	private LLACommonUtil llaCommonUtil;

    /**
     *  Populating Address.area attribute
     * @param addressData
     * @param addressModel
     * @throws ConversionException
     */
    @Override
    public void populate(final AddressData addressData, final AddressModel addressModel) throws ConversionException{
		super.populate(addressData, addressModel);
		if(StringUtils.isNotEmpty(addressData.getArea())) {
			addressModel.setArea(addressData.getArea());
		}
		addressModel.setEmail(addressData.getEmail());
		if (llaCommonUtil.isSitePanama() || llaCommonUtil.isSiteCabletica())
		{
			addressModel.setProvince(addressData.getProvince());
			addressModel.setDistrict(addressData.getDistrict());
			addressModel.setNeighbourhood(addressData.getNeighbourhood());
			addressModel.setBuilding(addressData.getBuilding());
			addressModel.setAppartment(addressData.getAppartment());
			addressModel.setStreetname(addressData.getStreetname());
			addressModel.setServiceable(addressData.getServiceable());
			addressModel.setLandMark(addressData.getLandMark());
    }
		if(llaCommonUtil.isSiteJamaica()) {
			addressModel.setCellphone(addressData.getCellphone());
		}

		if(llaCommonUtil.isSiteVTR())
		{
			addressModel.setEmail(addressData.getEmail());
			addressModel.setDistrict(addressData.getDistrict());
			addressModel.setAppartment(addressData.getAppartment());
			addressModel.setStreetname(addressData.getStreetname());
			addressModel.setHouseNo(addressData.getHouseNo());
			addressModel.setRegionName(addressData.getRegionName());
			addressModel.setLocality(addressData.getLocality());
			CountryModel country = getCommonI18NService().getCountry("CL");
			if(Objects.nonNull(country))
			{
				addressModel.setCountry(country);
			}
			addressModel.setStreetnumber(addressData.getStreetNo());
			addressModel.setStreetExtension(addressData.getStreetExtension());
			addressModel.setCity(addressData.getCity());
			if(StringUtils.isNotEmpty(addressData.getTechnologyServiced()))
			{
				addressModel.setTechnologyServiced(LLATechnologyEnum.valueOf(addressData.getTechnologyServiced()));
			}

		}
	    if (llaCommonUtil.isSitePuertorico()) {
		    setPuertoricoAddressFields(addressData, addressModel);
	    }
    }

    private void setPuertoricoAddressFields(AddressData addressData, AddressModel addressModel)
    {
	    if (StringUtils.isNotEmpty(addressData.getStreetname())) {
		    addressModel.setStreetname(addressData.getStreetname());
	    }
	    if (StringUtils.isNotEmpty(addressData.getAppartment())) {
		    addressModel.setAppartment(addressData.getAppartment());
	    }
	    if (StringUtils.isNotEmpty(addressData.getBlock())) {
		    addressModel.setBlock(addressData.getBlock());
	    }
	    if (StringUtils.isNotEmpty(addressData.getBuilding())) {
		    addressModel.setBuilding(addressData.getBuilding());
	    }
	    if (StringUtils.isNotEmpty(addressData.getStreetNo())) {
		    addressModel.setStreetnumber(addressData.getStreetNo());
	    }
	    if (StringUtils.isNotEmpty(addressData.getPlotNo())) {
		    addressModel.setPlotNo(addressData.getPlotNo());
	    }
	    if (StringUtils.isNotEmpty(addressData.getKilometer())) {
		    addressModel.setKilometer(addressData.getKilometer());
	    }
	    if(StringUtils.isNotEmpty(addressData.getTypeOfResidence())){
		    addressModel.setTypeOfResidence(AddressAreaTypeEnum.valueOf(addressData.getTypeOfResidence()));
	    }
	    if(StringUtils.isNotEmpty(addressData.getLine2())){
		    if (addressData.getTypeOfResidence().equalsIgnoreCase(String.valueOf(AddressAreaTypeEnum.URBAN))) {
			    addressModel.setLine2(String.valueOf(UrbanResidenceEnum.valueOf(addressData.getLine2())));
		    } else {
			    addressModel.setLine2(String.valueOf(RuralResidenceEnum.valueOf(addressData.getLine2())));
		    }
	    }
	    if(Objects.nonNull(addressData.getServiceable())){
		    addressModel.setServiceable(addressData.getServiceable());
	    }
    }
}

