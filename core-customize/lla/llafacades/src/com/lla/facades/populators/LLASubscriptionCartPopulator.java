/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lla.facades.populators;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.subscriptionfacades.data.OrderEntryPriceData;
import de.hybris.platform.subscriptionfacades.data.OrderPriceData;
import de.hybris.platform.subscriptionfacades.order.converters.populator.SubscriptionCartPopulator;
import de.hybris.platform.subscriptionservices.model.RecurringChargeEntryModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionPricePlanModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;

import com.lla.core.util.LLACommonUtil;

/**
 * Converter for converting order / cart entries. It sets the total and base prices for entries that have multiple
 * billing frequencies (as they are subscription products)
 */
public class LLASubscriptionCartPopulator extends SubscriptionCartPopulator
{
    public static final String PHONE = "Phone";
    public static final String TV = "TV";
    public static final String INTERNET = "Internet";
    public static final String PHONE_CATEGORY_CODE = "phone";
    public static final String TV_CATEGORY_CODE = "tv";
    public static final String INTERNET_CATEGORY_CODE = "internet";
    public static final String PLUS = " + ";
    public static final String MONTH = " /Month";
    @Autowired
    private  LLACommonUtil llaCommonUtil;
    @Autowired
    private LLACommonPopulatorUtils llaCommonPopulatorUtils;

    @Autowired
    private ProductService productService;
    @Override
    public void populate(@Nonnull final CartModel source,
                         @Nonnull final CartData target) throws ConversionException{
        super.populate(source,target);
        if(llaCommonUtil.isSitePanama()){
      	  llaCommonPopulatorUtils.populate(source, target);
        }
        if(llaCommonUtil.isSitePuertorico()){
      	  llaCommonPopulatorUtils.populate(source, target);
            llaCommonUtil.setBundleProductPriceLable(source, target);
            if(Objects.nonNull(source.getFalloutReasonValue()))
               target.setFalloutReason(source.getFalloutReasonValue().toString());

        }
        if (llaCommonUtil.isSiteJamaica()) {
            if(Objects.nonNull(source.getOrderMonthlyPrice()))
            {
                target.setMonthlyTotalPrice(createPrice(source,source.getOrderMonthlyPrice()));
            }
            //TSE-127: setting discount period and number.
            if(Objects.nonNull(source.getDiscountPeriod()))
            {
                target.setDiscountPeriod(source.getDiscountPeriod());
            }
            if(Objects.nonNull(source.getDiscountNumber()))
            {
                target.setDiscountNumber(source.getDiscountNumber());
            }
        }
        if (llaCommonUtil.isSiteCabletica()) {
           double monthlyCableticaTotalPrice = 0.0;
           double totalPriceWithTax = 0.0;
           for (final OrderEntryData data : target.getEntries()) {
               for (final OrderEntryPriceData price : data.getOrderEntryPrices()) {
						monthlyCableticaTotalPrice += price.getMonthlyPrice().getValue().doubleValue();
               	final PriceData tax911 = price.getTax911Price();
                 	final PriceData tax13 = price.getTax13Price();
               	for (final CategoryData categoryData : data.getProduct().getCategories())
   					{
   						if(!categoryData.getCode().equals("channelsaddoncabletica")) {
   							  if(null != tax911 && null != tax13)
								{
									totalPriceWithTax += price.getMonthlyPrice().getValue().doubleValue()+tax911.getValue().doubleValue()+tax13.getValue().doubleValue();
								}
   						}
   					}
                 }
               }
              target.setTax911Amount(createPrice(source, source.getTax911Price()));
              target.setTax13Amount(createPrice(source, source.getTax13Price()));	
              target.setMonthlyTotalPrice(createPrice(source, source.getOrderMonthlyPrice()));
              target.setTotalPriceWithTax(createPrice(source, totalPriceWithTax));
       }
    }
}
