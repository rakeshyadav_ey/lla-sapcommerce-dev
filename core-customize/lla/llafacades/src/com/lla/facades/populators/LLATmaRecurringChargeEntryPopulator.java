package com.lla.facades.populators;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import de.hybris.platform.b2ctelcofacades.converters.populator.TmaRecurringChargeEntryPopulator;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.site.BaseSiteService;
import com.lla.core.util.LLACommonUtil;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.subscriptionservices.model.RecurringChargeEntryModel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import de.hybris.platform.subscriptionfacades.data.RecurringChargeEntryData;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class LLATmaRecurringChargeEntryPopulator extends TmaRecurringChargeEntryPopulator {
    @Autowired
    private BaseSiteService baseSiteService;

    @Autowired
    private LLACommonUtil llaCommonUtil;
    @Override
    public void populate(final RecurringChargeEntryModel source, final RecurringChargeEntryData target)
    {
        validateParameterNotNullStandardMessage("source", source);
        validateParameterNotNullStandardMessage("target", target);
        super.populate(source, target);
        String isoCode=getCommonI18NService().getCurrentCurrency().getIsocode();
        if(baseSiteService.getCurrentBaseSite().getUid().equalsIgnoreCase("puertorico")) {
            if(source.getAppreciationPrice()!=null){
                target.setAppreciationPrice(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(source.getAppreciationPrice()), isoCode));
            }else {
                target.setAppreciationPrice(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(0), isoCode));
            }

            if (source.getSignatureWithoutAutoPayPrice() != null) {
                target.setSignatureWithoutAutoPayPrice(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(source.getSignatureWithoutAutoPayPrice()), isoCode));
            } else {
                target.setSignatureWithoutAutoPayPrice(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(0), isoCode));
            }

            if (source.getSignatureWithAutoPayPrice() != null) {
                target.setSignatureWithAutoPayPrice(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(source.getSignatureWithAutoPayPrice()), isoCode));
            } else {
                target.setSignatureWithAutoPayPrice(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(0), isoCode));
            }
           if(source.getWithoutAutoPayPrice()!=null){
              target.setWithoutAutoPayPrice(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(source.getWithoutAutoPayPrice()), isoCode));
           }else{
               target.setWithoutAutoPayPrice(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(0.0), isoCode));
           }
           if(source.getQty()!=null) {
         	  target.setRecurringLines(source.getQty());
         	  if(llaCommonUtil.containsSecondLevelCategoryWithCode(source.getSubscriptionPricePlanRecurring().getProduct(), "prepaidPlans"))
               {
                   AtomicReference<Double> totalPricePlan = new AtomicReference<>((double) 0);
                   source.getSubscriptionPricePlanRecurring().getRecurringChargeEntries().stream().forEach(rec -> {
                       if (rec.getQty() <= source.getQty()) {
                           totalPricePlan.updateAndGet(v -> v + rec.getPrice());
                       }
                   });
                   target.setTotalPriceForLines(getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(totalPricePlan.get()), isoCode));
               }
           }



        }
        if(llaCommonUtil.isSiteJamaica() || llaCommonUtil.isSiteVTR() || llaCommonUtil.isSitePanama())
        {
            TmaProductOfferingModel product = (TmaProductOfferingModel) source.getSubscriptionPricePlanRecurring().getProduct();
            final PriceData totalPrice = target.getPrice();
            if (null != product.getEurope1Discounts()) {
                List<PriceData> priceData = new ArrayList<>();
                for (DiscountRowModel discount : product.getEurope1Discounts()) {
                    PriceData price;
                    if (discount.getAbsolute())
                        price = getPriceData(source, discount.getValue());
                    else
                        {
                            if(llaCommonUtil.isSiteVTR())
                            {
                                double discountAmount = ((totalPrice.getValue().doubleValue()) - (totalPrice.getValue().doubleValue() * (discount.getValue() / 100)));
                                price = getPriceData(source,discountAmount);
                                price.setDiscountPercentage(discount.getDiscountString());
                            }
                            else
                               price = getPriceData(source, totalPrice.getValue().doubleValue() * (discount.getValue() / 100));
                    }
                    if(StringUtils.isNotEmpty(discount.getDiscountPeriod()))
                    {
                        price.setDiscountPeriod(discount.getDiscountPeriod());
                    }
                    if(Objects.nonNull(discount.getPlanType()))
                    {
                        price.setPlanType(discount.getPlanType().getCode());
                    }
                    priceData.add(price);
                    if(llaCommonUtil.isSiteJamaica() || llaCommonUtil.isSitePanama()) {
                    target.setDiscountedPrice(price);
                    }
                    else
                  	  target.setVtrdiscountedPrice(priceData);
                }
            }
        }
    }

    private PriceData getPriceData(final RecurringChargeEntryModel source, final Double discount) {
        double price = 0d;
        if (discount != null)
        {
            price = discount;
        }

        CurrencyModel currency = source.getCurrency();
        if (currency == null)
        {
            currency = getCommonI18NService().getCurrentCurrency();
        }

        return getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(price), currency);
    }
}
