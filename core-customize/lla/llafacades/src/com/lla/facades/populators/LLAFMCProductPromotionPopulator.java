package com.lla.facades.populators;

import de.hybris.platform.b2ctelcoservices.jalo.TmaSimpleProductOffering;
import de.hybris.platform.b2ctelcoservices.model.TmaSimpleProductOfferingModel;
import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class LLAFMCProductPromotionPopulator<SOURCE extends ProductModel, TARGET extends ProductData> extends
        AbstractProductPopulator<SOURCE, TARGET> {
    @Override
    public void populate(SOURCE source, TARGET target) throws ConversionException {
        if(source instanceof TmaSimpleProductOfferingModel){
            TmaSimpleProductOfferingModel spoProduct=(TmaSimpleProductOfferingModel)source;
            target.setFmcPromotionCode(spoProduct.getFmcPromotionCode());
            target.setFmcPromotionMessage(spoProduct.getFmcPromotionMessage());
        }

    }
}
