package com.lla.facades.populators;

import de.hybris.platform.b2ctelcofacades.converters.populator.TmaAddressPopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;

import com.lla.core.util.LLACommonUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.util.Strings;

import java.util.Objects;

public class LLAAddressPopulator extends TmaAddressPopulator {



	@Resource(name = "llaCommonUtil")
	private LLACommonUtil llaCommonUtil;


    /**
     *  Populating Address.area attribute
     * @param source
     * @param source
     * @throws ConversionException
     */
    @Override
    public void populate(final AddressModel source, final AddressData target)
    {
		super.populate(source, target);
		target.setArea(source.getArea());
		if (llaCommonUtil.isSitePanama() || llaCommonUtil.isSiteCabletica())
		{
			target.setProvince(source.getProvince());
			target.setDistrict(source.getDistrict());
			target.setNeighbourhood(source.getNeighbourhood());
			target.setBuilding(source.getBuilding());
			target.setAppartment(source.getAppartment());
			target.setStreetname(source.getStreetname());
			target.setServiceable(source.getServiceable());
			target.setLandMark(source.getLandMark());
		}
		if (llaCommonUtil.isSiteJamaica()) {
			target.setCellphone(source.getCellphone());
		}

		if (llaCommonUtil.isSiteVTR()) {
			target.setLocality(source.getLocality());
    }
		if(llaCommonUtil.isSitePuertorico()){

			target.setTypeOfResidence(Objects.nonNull(source.getTypeOfResidence()) ? source.getTypeOfResidence().getCode() : Strings.EMPTY);
			target.setStreetname(source.getStreetname());
			target.setStreetNo(source.getStreetnumber());
			target.setKilometer(source.getKilometer());
			target.setPlotNo(source.getPlotNo());
			target.setBlock(source.getBlock());
			target.setAppartment(source.getAppartment());
			target.setBuilding(source.getBuilding());

    }

}
}
