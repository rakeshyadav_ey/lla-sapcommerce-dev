package com.lla.facades.populators;

import com.lla.core.model.CreditScoreModel;
import de.hybris.platform.commercefacades.creditscore.data.CreditScoreData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class LLACreditScorePopulator implements Populator<CreditScoreModel, CreditScoreData> {

    /**
     * Populate the target instance with values from the source instance.
     *
     * @param creditScoreModel the source object
     * @param creditScoreData  the target to fill
     * @throws ConversionException if an error occurs
     */
    @Override
    public void populate(CreditScoreModel creditScoreModel, CreditScoreData creditScoreData) throws ConversionException {

        if (null !=creditScoreModel) {

            if (null != creditScoreModel.getContractStatus()) {
                creditScoreData.setContractStatus(creditScoreModel.getContractStatus().toString());
            }
            if (null != creditScoreModel.getLetterMapping()) {
                creditScoreData.setLetterMapping(creditScoreModel.getLetterMapping());
            }
            if (null != creditScoreModel.getInstallationFee()) {
                creditScoreData.setInstallationFee(creditScoreModel.getInstallationFee().toString());
            }
            if (null != creditScoreModel.getPack()) {
                creditScoreData.setPack(creditScoreModel.getPack());
            }
            if (null != creditScoreModel.getUpfrontPayment()) {
                creditScoreData.setUpfrontPayment(creditScoreModel.getUpfrontPayment());
            }
            if (null != creditScoreModel.getOnetimeCharges()) {
                creditScoreData.setOnetimeCharges(creditScoreModel.getOnetimeCharges().toString());
            }
            if (null != creditScoreModel.getMinScoreRange()) {
                creditScoreData.setMinScoreRange(creditScoreModel.getMinScoreRange());
            }
            if (null != creditScoreModel.getMaxScoreRange()) {
                creditScoreData.setMaxScoreRange(creditScoreModel.getMaxScoreRange());
            }
            if (null != creditScoreModel.getPaperBilling()) {
                creditScoreData.setPaperBilling(creditScoreModel.getPaperBilling().toString());
            }
            if (null != creditScoreModel.getDvrDeposit()) {
            	creditScoreData.setDvrDeposit((null != creditScoreModel.getDvrDeposit()) ? creditScoreModel.getDvrDeposit() : 0.0);
            }
        }
    }
}
