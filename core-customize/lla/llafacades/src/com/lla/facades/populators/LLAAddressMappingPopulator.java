/**
 *
 */
package com.lla.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.lla.core.model.LLAPanamaAddressMappingModel;
import com.lla.facades.addressmapping.data.LLAPanamaAddressMappingData;


/**
 * @author GF986ZE
 *
 */
public class LLAAddressMappingPopulator implements Populator<LLAPanamaAddressMappingModel, LLAPanamaAddressMappingData>
{

	@Override
	public void populate(final LLAPanamaAddressMappingModel source, final LLAPanamaAddressMappingData target)
			throws ConversionException
	{
		target.setProject(source.getProject());
		target.setNeighbourhood(source.getNeighbourhood());
		target.setTownship(source.getTownship());
		target.setDistrict(source.getDistrict());
		target.setProvince(source.getProvince());

	}

}
