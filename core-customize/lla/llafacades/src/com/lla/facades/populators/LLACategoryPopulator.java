package com.lla.facades.populators;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.converters.populator.CategoryPopulator;
import de.hybris.platform.commercefacades.product.data.CategoryData;

public class LLACategoryPopulator extends CategoryPopulator {

    @Override
    public void populate(final CategoryModel source, final CategoryData target)
    {
        super.populate(source, target);
        if(!source.getSupercategories().isEmpty())
        {
            target.setParentCategoryName(source.getSupercategories().get(0).getName());
        }
    }
}