package com.lla.facades.populators;

import de.hybris.platform.b2ctelcofacades.data.CartActionInput;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

public class LLACommerceCartPopulator implements Populator<CartActionInput, CommerceCartParameter> {

    @Override
    public void populate(final CartActionInput cartActionInput, final CommerceCartParameter commerceCartParameter)
            throws ConversionException {
        validateParameterNotNullStandardMessage("source", cartActionInput);
        validateParameterNotNullStandardMessage("target", commerceCartParameter);
        if(null != cartActionInput.getPlanType())
        {
            commerceCartParameter.setPlanType(cartActionInput.getPlanType());
        }

    }
}
