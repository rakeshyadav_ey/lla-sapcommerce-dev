package com.lla.facades.populators;

import com.lla.core.util.LLACommonUtil;
import de.hybris.platform.b2ctelcofacades.converters.populator.TmaProductSpecCharacteristicValuePopulator;
import de.hybris.platform.b2ctelcofacades.data.TmaProductSpecCharacteristicValueData;
import de.hybris.platform.b2ctelcoservices.model.TmaProductSpecCharacteristicValueModel;
import de.hybris.platform.converters.Populator;
import org.springframework.beans.factory.annotation.Autowired;

public class LLAProductSpecCharacteristicValuePopulator extends TmaProductSpecCharacteristicValuePopulator implements Populator<TmaProductSpecCharacteristicValueModel, TmaProductSpecCharacteristicValueData>
{
    @Autowired
    private LLACommonUtil llaCommonUtil;

    @Override
    public void populate(final TmaProductSpecCharacteristicValueModel source, final TmaProductSpecCharacteristicValueData target)
    {
        target.setId(source.getId());
        target.setValue(source.getValue());
        target.setDescription(source.getDescription());
        target.setUnitOfMeasurment(source.getUnitOfMeasure() != null ? source.getUnitOfMeasure().getName() : null);
        target.setValueType(source.getValueType() != null ? source.getValueType().getCode() : null);
        target.setCtaSpecDescription(source.getCtaSpecDescription());
        if(llaCommonUtil.isSitePanama()) {
            target.setGratisImageURL(source.getGratisImageURL());
            target.setChannelImageURL(source.getChannelImageURL());
            if(source.getGratisFlag() != null)
            {
                target.setGratisFlag(source.getGratisFlag());
            }
            if(source.getChannelFlag() !=null)
            {
                target.setChannelFlag(source.getChannelFlag());
            }
        }
        if(null != source.getProductSpecCharacteristic())
        {
            target.setProductSpecCharacteristic(source.getProductSpecCharacteristic().getId());
        }
    }
}