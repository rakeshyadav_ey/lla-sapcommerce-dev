package com.lla.facades.populators;

import de.hybris.platform.b2ctelcofacades.search.solrfacetsearch.populators.TmaSearchResultPoPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;

public class LLASearchResultPoPopulator extends TmaSearchResultPoPopulator {

    @Override
    public void populate(final SearchResultValueData source, final ProductData target)
    {
        super.populate(source, target);
        target.setAllCategories(this.getValue(source, "allCategories"));

        if(null != getValue(source, "brand, 10015"))
        {
            target.setBrand(this.getValue(source, "brand, 10015"));
        }
        target.setProductDisclaimer(this.<String> getValue(source, "productDisclaimer"));
    }
}
