package com.lla.facades.populators;

import de.hybris.platform.b2ctelcofacades.order.converters.populator.TmaCartModificationPopulator;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;

public class LLACartModificationPopulator extends TmaCartModificationPopulator {

    @Override
    public void populate(final CommerceCartModification source, final CartModificationData target)
    {
        super.populate(source, target);
        target.setDeleteMasterEntry(source.isDeleteMasterEntry());
    }
}
