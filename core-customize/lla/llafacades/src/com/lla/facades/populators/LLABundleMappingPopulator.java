package com.lla.facades.populators;

import com.lla.core.enums.DocumentType;
import com.lla.core.model.LLABundleProductMappingModel;
import com.lla.facades.customer.data.DocumentTypeData;
import com.lla.facades.productconfigurator.data.LLABundleProductMappingData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class LLABundleMappingPopulator implements Populator<LLABundleProductMappingModel, LLABundleProductMappingData> {
    @Autowired
    private PriceDataFactory priceDataFactory;

    @Autowired
    private BaseStoreService baseStoreService;
    @Autowired
    private Converter<ProductModel, ProductData> productConverter;
    /**
     * Populate the target instance with values from the source instance.
     *
     * @param source the source object
     * @param target  the target to fill
     * @throws ConversionException if an error occurs
     */
    @Override
    public void populate(LLABundleProductMappingModel source, LLABundleProductMappingData target) throws ConversionException {
        if(null != source.getBundlePrice())
            target.setBundlePrice(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(source.getBundlePrice()), baseStoreService.getCurrentBaseStore().getDefaultCurrency()));
        target.setBundleType(source.getBundleType().toString());
        if(null != source.getInternetPlan())
            target.setInternetPlan(productConverter.convert(source.getInternetPlan()));
        if(null != source.getPackageId())
            target.setPackageId(source.getPackageId());
        if(null !=source.getPhonePrice())
            target.setPhonePrice(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(source.getPhonePrice()), baseStoreService.getCurrentBaseStore().getDefaultCurrency()));
        target.setTechnology(source.getTechnology().toString());
        if(null != source.getTv())
            target.setTv(productConverter.convert(source.getTv()));
        if(null != source.getTvBoxType())
            target.setTvBoxType(source.getTvBoxType().toString());
        if(CollectionUtils.isNotEmpty(source.getTvPriceList()))
            target.setTvPriceList(getPriceList(source.getTvPriceList()));
    }

    private List<PriceData> getPriceList(List<Double> tvPriceList) {
        List<PriceData> priceData = new ArrayList<>();
        tvPriceList.stream().forEach(price ->{
            priceData.add(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(price), baseStoreService.getCurrentBaseStore().getDefaultCurrency()));
        });
        return  priceData;
    }
}
