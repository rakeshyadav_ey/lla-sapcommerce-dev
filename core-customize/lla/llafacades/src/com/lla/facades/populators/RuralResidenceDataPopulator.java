package com.lla.facades.populators;

import com.lla.core.enums.RuralResidenceEnum;
import com.lla.facades.address.LLAEnumTypeData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Required;

public class RuralResidenceDataPopulator implements Populator<RuralResidenceEnum, LLAEnumTypeData> {

    private EnumerationService enumerationService;

    public EnumerationService getEnumerationService() {
        return enumerationService;
    }

    @Required public void setEnumerationService(EnumerationService enumerationService) {
        this.enumerationService = enumerationService;
    }

    /**
     * Populate the target instance with values from the source instance.
     *
     * @param ruralResidenceEnum     the source object
     * @param ruralResidenceEnumData the target to fill
     * @throws ConversionException if an error occurs
     */
    @Override public void populate(RuralResidenceEnum ruralResidenceEnum, LLAEnumTypeData ruralResidenceEnumData)
            throws ConversionException {
        ruralResidenceEnumData.setCode(ruralResidenceEnum.getCode());
        ruralResidenceEnumData.setName(getEnumerationService().getEnumerationName(ruralResidenceEnum));
    }
}
