package com.lla.facades.populators;

import de.hybris.platform.b2ctelcofacades.order.converters.populator.TmaCartPopulator;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.order.data.ZoneDeliveryModeData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CashOnDeliveryPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.OnlinePaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import org.apache.commons.lang.StringUtils;
import java.util.Objects;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

public class LLATmaCartPopulator extends TmaCartPopulator {
		
    public LLATmaCartPopulator(Converter<PaymentInfoModel, CCPaymentInfoData> creditCardPaymentInfoConverter, Converter<AddressModel, AddressData> addressConverter, Converter<DeliveryModeModel, DeliveryModeData> deliveryModeConverter, Converter<ZoneDeliveryModeModel, ZoneDeliveryModeData> zoneDeliveryModeConverter) {
        super(creditCardPaymentInfoConverter, addressConverter, deliveryModeConverter, zoneDeliveryModeConverter);
    }

    @Override
    public void populate(final CartModel cartModel, final CartData cartData)
    {
        validateParameterNotNullStandardMessage("source", cartModel);
        validateParameterNotNullStandardMessage("target", cartData);
        cartData.setUser(getPrincipalConverter().convert(cartModel.getUser()));

        if (cartModel.getPaymentInfo() != null && cartModel.getPaymentInfo() instanceof CreditCardPaymentInfoModel)
        {
            cartData.setPaymentInfo(getCreditCardPaymentInfoConverter().convert(cartModel.getPaymentInfo()));
        }
       if (cartModel.getDeliveryAddress() != null)
        {
            cartData.setDeliveryAddress(getAddressConverter().convert(cartModel.getDeliveryAddress()));
        }

        if (cartModel.getPaymentAddress() != null)
        {
            cartData.setBillingAddress(getAddressConverter().convert(cartModel.getPaymentAddress()));
        }

        final DeliveryModeModel deliveryModeModel = cartModel.getDeliveryMode();
        if (deliveryModeModel instanceof ZoneDeliveryModeModel)
        {
            cartData.setDeliveryMode(getZoneDeliveryModeConverter().convert((ZoneDeliveryModeModel) deliveryModeModel));
        }
        if (deliveryModeModel != null && !(deliveryModeModel instanceof ZoneDeliveryModeModel))
        {
            cartData.setDeliveryMode(getDeliveryModeConverter().convert(deliveryModeModel));
        }
		 
        setPromotionOnEntries(cartData);
    }
}
