/**
 *
 */
package com.lla.facades.populators;

import de.hybris.platform.commercefacades.storelocator.converters.populator.PointOfServicePopulator;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import javax.annotation.Resource;

import com.lla.core.util.LLACommonUtil;


/**
 * @author HP737SW
 *
 */
public class LLAPointOfServicePopulator extends PointOfServicePopulator
{
	@Resource(name = "llaCommonUtil")
	private LLACommonUtil llaCommonUtil;

	@Override
	public void populate(final PointOfServiceModel source, final PointOfServiceData target)
	{
		super.populate(source, target);
		if (llaCommonUtil.isSitePuertorico())
		{
			if (null != source.getProducts())
			{
				target.setProductsList(source.getProducts());
			}
		}

	}

}
