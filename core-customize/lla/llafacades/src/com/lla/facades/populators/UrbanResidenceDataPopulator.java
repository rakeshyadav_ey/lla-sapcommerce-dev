package com.lla.facades.populators;

import com.lla.core.enums.UrbanResidenceEnum;
import com.lla.facades.address.LLAEnumTypeData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Required;

public class UrbanResidenceDataPopulator implements Populator<UrbanResidenceEnum, LLAEnumTypeData> {

    private EnumerationService enumerationService;

    public EnumerationService getEnumerationService() {
        return enumerationService;
    }
   @Required
    public void setEnumerationService(EnumerationService enumerationService) {
        this.enumerationService = enumerationService;
    }

    /**
     * Populate the target instance with values from the source instance.
     *
     * @param urbanResidenceEnum     the source object
     * @param urbanResidenceEnumData the target to fill
     * @throws ConversionException if an error occurs
     */
    @Override
    public void populate(UrbanResidenceEnum urbanResidenceEnum, LLAEnumTypeData urbanResidenceEnumData)
            throws ConversionException {
        urbanResidenceEnumData.setCode(urbanResidenceEnum.getCode());
        urbanResidenceEnumData.setName(getEnumerationService().getEnumerationName(urbanResidenceEnum));
    }
}
