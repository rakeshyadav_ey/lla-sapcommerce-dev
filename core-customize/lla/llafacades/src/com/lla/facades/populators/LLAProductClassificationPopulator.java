/**
 *
 */
package com.lla.facades.populators;

import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.commercefacades.product.converters.populator.ProductClassificationPopulator;
import de.hybris.platform.commercefacades.product.data.ClassificationData;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.lla.core.util.LLACommonUtil;


/**
 * @author PR678TG
 *
 */
public class LLAProductClassificationPopulator<SOURCE extends ProductModel, TARGET extends ProductData>
		extends ProductClassificationPopulator<SOURCE, TARGET>
{
	@Autowired
	private FlexibleSearchService flexibleSearchService;

	@Autowired
	private CommonI18NService commonI18nService;

	@Autowired
	private LLACommonUtil llaCommonUtil;

	private Converter<MediaModel, ImageData> imageConverter;

	protected Converter<MediaModel, ImageData> getImageConverter()
	{
		return imageConverter;
	}

	@Required
	public void setImageConverter(final Converter<MediaModel, ImageData> imageConverter)
	{
		this.imageConverter = imageConverter;
	}


	@Override
	public void populate(final SOURCE productModel, final TARGET productData) throws ConversionException
	{
		super.populate(productModel, productData);

		if ((llaCommonUtil.isSiteVTR() && llaCommonUtil.containsFirstLevelCategoryWithCode(productModel, "devices")) || (llaCommonUtil.isSitePuertorico() && llaCommonUtil.containsSecondLevelCategoryWithCode(productModel, "devices")))
		{
			final List<ProductFeatureModel> list = productModel.getFeatures();
			if (productData.getClassifications() != null && list != null)
			{
				for (final ClassificationData classificationData : productData.getClassifications())
				{
					for (final FeatureData featureData : classificationData.getFeatures())
					{
						final ProductFeatureModel productFeatureModel = list.stream()
								.filter(featureModel -> featureModel.getQualifier().equals(featureData.getCode())).findFirst().get();

						if (StringUtils.isNotEmpty(productFeatureModel.getIconName()))
						{
							featureData.setIconName(productFeatureModel.getIconName());
						}

						if (Objects.nonNull(productFeatureModel.getMedia()))
						{
							final ImageData imageData = getImageConverter().convert(productFeatureModel.getMedia());
							featureData.setMedia(imageData);
						}
					}
				}
			}
		}
	}

}
