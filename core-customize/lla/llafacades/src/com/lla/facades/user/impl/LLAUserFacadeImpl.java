package com.lla.facades.user.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;

import com.lla.core.model.C2CObjectModel;
import com.lla.core.service.LLACustomerAccountService;
import com.lla.facades.constants.LlaFacadesConstants;
import com.lla.facades.user.LLAUserFacade;

import de.hybris.platform.b2ctelcofacades.user.impl.DefaultTmaUserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;

public class LLAUserFacadeImpl  extends DefaultTmaUserFacade implements LLAUserFacade {

    @Autowired
    LLACustomerAccountService llaCustomerAccountService;
    @Autowired
    private ModelService modelService;
    @Override
    public void setDefaultAddress(final AddressData addressData)
    {
        validateParameterNotNullStandardMessage("addressData", addressData);
        final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
        final AddressModel addressModel = getCustomerAccountService().getAddressForCode(currentCustomer, addressData.getId());
        if (addressModel != null)
        {
            llaCustomerAccountService.setDefaultAddressEntry(currentCustomer, addressModel);
        }
    }
	@Override
	public void updateC2C(final Map<String,String> c2cObject) {
		C2CObjectModel c2c = modelService.create(C2CObjectModel.class);
		try {
			
			if(c2cObject.containsKey(LlaFacadesConstants.RUT_NUMBER)) {
				c2c.setRutNumber(c2cObject.get(LlaFacadesConstants.RUT_NUMBER));
			}
			if(c2cObject.containsKey(LlaFacadesConstants.PHONE_NUMBER)) {
				c2c.setPhoneNumber(c2cObject.get(LlaFacadesConstants.PHONE_NUMBER));
			}
			if(c2cObject.containsKey(LlaFacadesConstants.INACTIVITY_PAGE)) {
				c2c.setInactivityPage(c2cObject.get(LlaFacadesConstants.INACTIVITY_PAGE));
			}
			if(c2cObject.containsKey(LlaFacadesConstants.FIXED_OR_MOBILE)) {
				c2c.setFixedOrMobile(c2cObject.get(LlaFacadesConstants.FIXED_OR_MOBILE));
			}
			c2c.setGuid(UUID.randomUUID().toString());
			c2c.setChileTime(getChileTime());
		}
		catch(Exception e) {
			throw new IllegalArgumentException("Unable to update the values");
		}
		finally {
			modelService.save(c2c);
		}
	}
	
	protected String getChileTime() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(LlaFacadesConstants.CHILE_TIME_FORMAT);
        ZoneId chileZoneId = ZoneId.of(LlaFacadesConstants.CHILE_TIME_ZONE);
        LocalDateTime chileZonedDateTime = LocalDateTime.now(chileZoneId);
		return chileZonedDateTime.format(formatter).toString();
	}
	
}
