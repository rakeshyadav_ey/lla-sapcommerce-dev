package com.lla.facades.user;

import java.util.Map;

public interface LLAUserFacade {
    
	void updateC2C(final Map<String,String> c2cObject);
}
