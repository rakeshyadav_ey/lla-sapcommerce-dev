package com.lla.facades.cart;

import de.hybris.platform.b2ctelcofacades.data.CartActionInput;
import de.hybris.platform.b2ctelcofacades.order.TmaCartFacade;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.exceptions.CalculationException;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import com.lla.core.model.LLABundleProductMappingModel;
import com.lla.facades.productconfigurator.data.LLAPuertoRicoProductConfigDTO;


public interface LLACartFacade extends TmaCartFacade{

    CartModificationData addProductOfferingToCartForMobiles(final String spoCode, final String planType, final long quantity, final String processType,
                                                            final String rootBpoCode, final int cartGroupNo, final String subscriptionTermId, final String subscriberId,
                                                            final String subscriberBillingId) throws CommerceCartModificationException;


    List<CartModificationData> processCartActionForDevice(final List<CartActionInput> cartActionInputList) throws CommerceCartModificationException;

    String checkCartForCompatibilityIssues();

    void setAccountNumberForCustomer(String accountNumber);


void updateCartIfExistingCustomer(String isCustomerExisting);

	void addConfiguratorProductsToCart(final LLAPuertoRicoProductConfigDTO productConfigDTO, final long quantity,
			final String processType, final String rootBpoCode, final int cartGroupNo,
			final String subscriptionTermId, final String subscriberId, final String subscriberBillingId) throws CalculationException;

	List<ProductData> getPrimaryProductsList();

	List<ProductData> getChannelAddonProducts();

	ProductData getSmartProductIfPresent();

	/**
	 * Persist AutoPay Details
	 * @param autoPay
	 * @param numberOfLines
	 */
	void calculateAutoPayDetails(final Boolean autoPay, final Integer numberOfLines) throws CalculationException;

	/**
	 * Validate TV Addon Present or Not
	 * @return
	 */
	List<ProductData> getTVAddonIfPresent();

	/**
	 * Retreive BUndle Product mapping for Cart
	 * @param productConfigDTO
	 * @return
	 */
	List<LLABundleProductMappingModel> getBundleProductMappingForCart(final LLAPuertoRicoProductConfigDTO productConfigDTO);

    void createParentChildEntry(String cartCode, OrderEntryData entry, OrderEntryData childEntry);
	
    LinkedHashSet<String> getSelectedPlanInOrder();

	void setLineNumberForEntries(CartModificationData parentData);


	void updateRUTNumber(Map<String, String> userDetails);

	void updateCartUTMfields(Map<String, String> utmDetails);

	/**
	 * @param cartBundles
	 */
	public void filterCartBundles(final List<LLABundleProductMappingModel> cartBundles);

	/**
	 * @param bundles
	 */
	public void removeDVRBundleFromCart(final List<LLABundleProductMappingModel> cartBundles);
	
	/**
	 * Retreive Product price map in ascending order
	 * @param productList
	 * @param productMappingList
	 * @return
	 */
	public Map<ProductModel, Double> getProductPriceMap(final List<ProductModel> productList,
			 final List<LLABundleProductMappingModel> productMappingList);
	
	public void lastVisitedStep(final String currentStep);

}
