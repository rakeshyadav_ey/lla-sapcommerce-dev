/**
 *
 */
package com.lla.facades.address.impl;

import com.lla.core.model.*;
import com.lla.facades.address.*;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;

import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.collections.CollectionUtils;

import com.lla.core.service.address.LLAAddressService;
import com.lla.facades.addressmapping.data.LLAPanamaAddressMappingData;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author GF986ZE
 *
 */
public class LLAAddressFacadeImpl implements LLAAddressFacade
{
	public static final String DASH ="-";
	@Resource
	private LLAAddressService llaAddressService;

	@Resource
	private Converter<LLAPanamaAddressMappingModel, LLAPanamaAddressMappingData> llaAddressMappingConverter;

	@Autowired
	private Converter<ProvinceModel, ProvinceData> provinceConverter;

	@Autowired
	private Converter<CantonModel, CantonData> cantonConverter;

	@Autowired
	private Converter<DistrictModel, DistrictData> districtConverter;

	@Autowired
	private CartService cartService;
	@Autowired
	private ModelService modelService;
	@Autowired
	private Converter<NeighbourhoodModel, NeighbourHoodData> neighbourHoodConverter;

	public List<LLAPanamaAddressMappingData> getAddressMappingForGivenText(final String text)
	{
		final List<LLAPanamaAddressMappingData> llaPanamaAddressMappingDataList = new ArrayList<>();
		final List<LLAPanamaAddressMappingModel> addresMappings = getLlaAddressService().getAddressMappingForGivenText(text);
		if (null != addresMappings && CollectionUtils.isNotEmpty(addresMappings))
		{
		llaPanamaAddressMappingDataList.addAll(llaAddressMappingConverter.convertAll(addresMappings));
		}
		return llaPanamaAddressMappingDataList;

	}

	@Override
	public List<ProvinceData> getAllProvince() {
		final List<ProvinceData> provinceDataList = new ArrayList<>();

		final List<ProvinceModel> provinceMapping = getLlaAddressService().getAllProvince();
		if (null != provinceMapping && CollectionUtils.isNotEmpty(provinceMapping))
		{
			provinceDataList.addAll(provinceConverter.convertAll(provinceMapping));
		}
		return provinceDataList;
	}

	@Override
	public <T> List<T> getAddressValues(String prevNode, String nextNode) {
		final List<T> addressResult=llaAddressService.getAddressValues( prevNode,  nextNode);
		if(CollectionUtils.isNotEmpty(addressResult))
		{
			List<T> dataList=new ArrayList<>();
			for(T item:addressResult){
				if(item instanceof  CantonModel){
					dataList.add((T) cantonConverter.convert((CantonModel) item));
				}
				if(item instanceof DistrictModel){
					dataList.add((T) districtConverter.convert((DistrictModel) item));
				}
				if(item instanceof NeighbourhoodModel){
					dataList.add((T) neighbourHoodConverter.convert((NeighbourhoodModel) item));
				}
			}
			return dataList;
		}
		return null;
	}

	@Override
	public void setSelectedAddress(AddressData addressData) {
		if (cartService.hasSessionCart())
		{
			CartModel cart = cartService.getSessionCart();
			if (Objects.nonNull(addressData)) {
				AddressModel addressModel = modelService.create(AddressModel.class);
				addressModel.setStreetname(addressData.getStreetname());
				addressModel.setLine1(addressData.getLine1());
				addressModel.setTown(addressData.getCity());
				addressModel.setProvince(addressData.getProvince());
				addressModel.setPostalcode(StringUtils.substringBeforeLast(addressData.getPostalCode(),DASH));
				addressModel.setHouseNo(addressData.getHouseNo());
				addressModel.setOwner(cart.getUser());
				modelService.save(addressModel);
				cart.setSelectedAddress(addressModel);
				modelService.save(cart);
			}
		}
	}

	@Override
	public AddressData getSelectedAddress(CartModel cart)
	{
		AddressModel serviceableAddress = cart.getSelectedAddress();
		AddressData addressData = new AddressData();
		if(Objects.nonNull(serviceableAddress))
		{
			addressData.setStreetname(serviceableAddress.getStreetname());
			addressData.setLine1(serviceableAddress.getLine1());
			addressData.setCity(serviceableAddress.getCity());
			addressData.setProvince(serviceableAddress.getProvince());
			addressData.setHouseNo(serviceableAddress.getHouseNo());
			addressData.setPostalCode(serviceableAddress.getPostalcode());
		}
		return addressData;
	}


	public void setLlaAddressService(final LLAAddressService llaAddressService)
	{
		this.llaAddressService = llaAddressService;
	}


	public LLAAddressService getLlaAddressService()
	{
		return llaAddressService;
	}


	public Converter<LLAPanamaAddressMappingModel, LLAPanamaAddressMappingData> getLlaAddressMappingConverter()
	{
		return llaAddressMappingConverter;
	}


	public void setLlaAddressMappingConverter(
			final Converter<LLAPanamaAddressMappingModel, LLAPanamaAddressMappingData> llaAddressMappingConverter)
	{
		this.llaAddressMappingConverter = llaAddressMappingConverter;
	}


}
