/**
 *
 */
package com.lla.facades.address;

import java.util.List;

import com.lla.facades.addressmapping.data.LLAPanamaAddressMappingData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.order.CartModel;

/**
 * @author GF986ZE
 *
 */
public interface LLAAddressFacade
{

	List<LLAPanamaAddressMappingData> getAddressMappingForGivenText(final String text);

	List<ProvinceData> getAllProvince();

	<T> List<T> getAddressValues(final String prevNode, final String nextNode);

	void setSelectedAddress(final AddressData addressData);

	AddressData getSelectedAddress(final CartModel cart);

}
