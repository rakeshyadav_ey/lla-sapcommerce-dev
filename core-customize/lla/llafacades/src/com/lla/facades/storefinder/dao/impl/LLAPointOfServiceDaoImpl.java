package com.lla.facades.storefinder.dao.impl;

import com.lla.facades.storefinder.dao.LLAPointOfServiceDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.GPS;
import de.hybris.platform.storelocator.exception.GeoLocatorException;
import de.hybris.platform.storelocator.exception.PointOfServiceDaoException;
import de.hybris.platform.storelocator.impl.DefaultPointOfServiceDao;
import de.hybris.platform.storelocator.impl.GeometryUtils;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Collection;
import java.util.List;

public class LLAPointOfServiceDaoImpl extends DefaultPointOfServiceDao implements LLAPointOfServiceDao {

    protected FlexibleSearchQuery buildQuery(String storeName, GPS center, double radius, BaseStoreModel baseStore) {
        try {
            List<GPS> corners = GeometryUtils.getSquareOfTolerance(center, radius);
            if (CollectionUtils.isNotEmpty(corners) && corners.size() == 2) {
                Double latMax = ((GPS)corners.get(1)).getDecimalLatitude();
                Double lonMax = ((GPS)corners.get(1)).getDecimalLongitude();
                Double latMin = ((GPS)corners.get(0)).getDecimalLatitude();
                Double lonMin = ((GPS)corners.get(0)).getDecimalLongitude();
                StringBuilder query = new StringBuilder();
                query.append("SELECT {PK} FROM {").append("PointOfService").append("} WHERE {").append("latitude").append("} is not null AND {").append("longitude").append("} is not null AND {").append("latitude").append("} >= ?latMin AND {").append("latitude").append("} <= ?latMax AND {").append("longitude").append("} >= ?lonMin AND {").append("longitude").append("} <= ?lonMax");
                if (baseStore != null) {
                    query.append(" AND {").append("baseStore").append("} = ?baseStore");
                }
                if(StringUtils.isNotEmpty(storeName)){
                    query.append(" OR {").append("displayName").append("} LIKE ?storeName");
                }

                FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query.toString());
                fQuery.addQueryParameter("latMax", latMax);
                fQuery.addQueryParameter("latMin", latMin);
                fQuery.addQueryParameter("lonMax", lonMax);
                fQuery.addQueryParameter("lonMin", lonMin);
                if (baseStore != null) {
                    fQuery.addQueryParameter("baseStore", baseStore);
                }
                if(StringUtils.isNotEmpty(storeName)) {
                    fQuery.addQueryParameter("storeName", "%"+storeName+"%");
                }

                return fQuery;
            } else {
                throw new PointOfServiceDaoException("Could not fetch locations from database. Unexpected neighborhood");
            }
        } catch (GeoLocatorException var12) {
            throw new PointOfServiceDaoException("Could not fetch locations from database, due to :" + var12.getMessage(), var12);
        }
    }

    @Override
    public Collection<PointOfServiceModel> getGeocodedPOSByName(String locationText, GPS referenceGps, double radius, BaseStoreModel baseStore) {
        FlexibleSearchQuery fQuery = this.buildQuery(locationText, referenceGps, radius, baseStore);
        SearchResult<PointOfServiceModel> result = this.search(fQuery);
        return result.getResult();
    }
}
