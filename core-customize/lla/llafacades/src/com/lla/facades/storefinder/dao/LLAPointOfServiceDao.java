package com.lla.facades.storefinder.dao;

import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.GPS;
import de.hybris.platform.storelocator.PointOfServiceDao;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Collection;

public interface LLAPointOfServiceDao extends PointOfServiceDao {

    Collection<PointOfServiceModel> getGeocodedPOSByName(String locationText, GPS referenceGps, double doubleValue, BaseStoreModel baseStore);
}
