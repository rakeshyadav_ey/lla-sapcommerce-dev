package com.lla.facades.storefinder.services;

import com.lla.facades.storefinder.dao.LLAPointOfServiceDao;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.commerceservices.storefinder.impl.DefaultStoreFinderService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.GPS;
import de.hybris.platform.storelocator.exception.GeoServiceWrapperException;
import de.hybris.platform.storelocator.impl.DefaultGPS;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.*;

public class LLAStoreFinderService<ITEM extends PointOfServiceDistanceData> extends DefaultStoreFinderService {

    private static Logger LOG = Logger.getLogger(LLAStoreFinderService.class);
    private LLAPointOfServiceDao llaPointOfServiceDao;

    @Override
    public StoreFinderSearchPageData<ITEM> locationSearch(final BaseStoreModel baseStore, final String locationText,
                                                          final PageableData pageableData)
    {
        final GeoPoint geoPoint = new GeoPoint();
        if (locationText != null && !locationText.isEmpty())
        {
            try
            {
                // Resolve the address to a point
                final GPS resolvedPoint = getGeoWebServiceWrapper()
                        .geocodeAddress(generateGeoAddressForSearchQuery(baseStore, locationText));

                geoPoint.setLatitude(resolvedPoint.getDecimalLatitude());
                geoPoint.setLongitude(resolvedPoint.getDecimalLongitude());

                return doSearch(baseStore, locationText, geoPoint, pageableData, baseStore.getMaxRadiusForPoSSearch());
            }
            catch (final GeoServiceWrapperException ex)
            {
                LOG.info("Failed to resolve location for [" + locationText + "]", ex);
            }
        }

        // Return no results
        return createSearchResult(locationText, geoPoint, Collections.<ITEM>emptyList(), createPaginationData());
    }

    protected StoreFinderSearchPageData<ITEM> doSearch(final BaseStoreModel baseStore, final String locationText,
                                                       final GeoPoint centerPoint, final PageableData pageableData, final Double maxRadiusKm)
    {
        final Collection<PointOfServiceModel> posResults;

        final int resultRangeStart = pageableData.getCurrentPage() * pageableData.getPageSize();
        final int resultRangeEnd = (pageableData.getCurrentPage() + 1) * pageableData.getPageSize();

        if (maxRadiusKm != null)
        {
            final GPS referenceGps = new DefaultGPS(centerPoint.getLatitude(), centerPoint.getLongitude());
            posResults = getLlaPointOfServiceDao().getGeocodedPOSByName(locationText, referenceGps, maxRadiusKm.doubleValue(), baseStore);
        }
        else
        {
            final Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("baseStore", baseStore);
            paramMap.put("type", PointOfServiceTypeEnum.STORE);
            if(StringUtils.isNotEmpty(locationText)){
                paramMap.put("displayName", locationText);
            }
            posResults = getPointOfServiceGenericDao().find(paramMap);
        }

        if (posResults != null)
        {
            // Sort all the POS
            final List<ITEM> orderedResults = calculateDistances(centerPoint, posResults);
            final PaginationData paginationData = createPagination(pageableData, posResults.size());
            // Slice the required range window out of the results
            final List<ITEM> orderedResultsWindow = orderedResults
                    .subList(Math.min(orderedResults.size(), resultRangeStart), Math.min(orderedResults.size(), resultRangeEnd));

            return createSearchResult(locationText, centerPoint, orderedResultsWindow, paginationData);
        }

        // Return no results
        return createSearchResult(locationText, centerPoint, Collections.<ITEM>emptyList(), createPagination(pageableData, 0));
    }

    public LLAPointOfServiceDao getLlaPointOfServiceDao() {
        return llaPointOfServiceDao;
    }

    @Required
    public void setLlaPointOfServiceDao(LLAPointOfServiceDao llaPointOfServiceDao) {
        this.llaPointOfServiceDao = llaPointOfServiceDao;
    }
}
