/**
 *
 */
package com.lla.facades.checkout.impl;

import com.lla.core.model.CreditScoreModel;
import com.lla.core.service.LLAInstallationAddressService;
import com.lla.core.service.order.LLACommerceCheckoutService;
import com.lla.core.service.order.impl.LLACommerceCartModificationStatus;
import com.lla.core.util.LLACommonUtil;
import com.lla.facades.checkout.LLACheckoutFacade;
import com.lla.facades.product.LLATmaProductFacade;
import de.hybris.platform.b2ctelcofacades.bundle.impl.DefaultTmaCheckoutFacade;
import de.hybris.platform.commercefacades.creditscore.data.CreditScoreData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

/**
 * @author GZ132VA
 *
 */
public class LLACheckoutFacadeImpl extends DefaultTmaCheckoutFacade implements LLACheckoutFacade
{
	private static final String CATEGORY_DEVICES = "devices";
	private static final String CATEGORY_POSTPAID = "postpaid";
	private static final String CATEGORY_4PBUNDLE = "4Pbundles";

	@Resource
	private CartService cartService;

	@Resource(name = "productService")
	private ProductService productService;
	@Autowired
	private Converter<CartModel, CartData> cartConverter;

	@Autowired
	private LLACommonUtil llaCommonUtil;

	@Resource(name = "llaInstallationAddressService")
	private LLAInstallationAddressService llaInstallationAddressService;
	@Autowired
	private LLACommerceCheckoutService llaCommerceCheckoutService;

	@Autowired
	LLATmaProductFacade llaTmaProductFacade;

	private Converter<CreditScoreModel, CreditScoreData> llaCreditScoreConverter;


	@Override
	public List<AddressData> getSupportedInstallationAddresses(final boolean visibleAddressesOnly)
	{

		final CartModel cartModel = getCart();
		return cartModel == null ? Collections.emptyList()
				: getAddressConverter()
						.convertAll(
								llaInstallationAddressService.getSupportedInstallationAddressesForOrder(cartModel, visibleAddressesOnly));
	}

	@Override
	public AddressData getInstallationAddressForCode(final String code)
	{
		Assert.notNull(code, "Parameter code cannot be null.");
		for (final AddressData address : getSupportedInstallationAddresses(false))
		{
			if (code.equals(address.getId()))
			{
				return address;
			}
		}
		return null;
	}

	@Override
	public boolean setInstallationAddress(final AddressData addressData)
	{
		final CartModel cartModel = getCart();
		if (cartModel != null)
		{
			AddressModel addressModel = null;
			if (addressData != null)
			{
				addressModel = addressData.getId() == null ? creteInstallationAddressModel(addressData, cartModel)
						: getInstallationAddressModelForCode(addressData.getId());
			}

			final CommerceCheckoutParameter parameter = createCommerceCheckoutParameter(cartModel, true);
			parameter.setAddress(addressModel);
			parameter.setIsInstallationAddress(true);
			return llaCommerceCheckoutService.setInstallationAddress(parameter);
		}
		return false;
	}

	@Override
	public CartData getCheckoutCart()
	{
		final CartModel cart = getCartService().getSessionCart();
		final CartData cartData = getCartConverter().convert(cart);
	//	final CartData cartData = getCartFacade().getSessionCart();
		if (cartData != null)
		{
			if(llaCommonUtil.isSiteJamaica()){
				cartData.setInstallationAddress(getInstallationAddress());
			}
			List entryGroups = cartData.getRootGroups();
			List entries = cartData.getEntries();
			if(entryGroups != null) {
				if((llaCommonUtil.isSitePanama() && llaTmaProductFacade.checkIfAddonProductListRequired()) || (llaCommonUtil.isSiteCabletica())){
					setEnrtiesData(cartData, entryGroups, entries);
				}
			}
			//credit score value set
			if (cart.getCreditScoreLevel() != null) {
				cartData.setCreditScoreLevel(getCreditScore(cart.getCreditScoreLevel()));
			}
			if(null!= cart.getAmountDueFirstBill())
			{
				cartData.setAmountDueFirstBill(cart.getAmountDueFirstBill());
			}
			if(null!= cart.getAmountDueToday())
			{
				cartData.setAmountDueToday(cart.getAmountDueToday());
			}
			cartData.setDeliveryAddress(getDeliveryAddress());
			cartData.setDeliveryMode(getDeliveryMode());
			cartData.setPaymentInfo(getPaymentDetails());
		}
		return cartData;
	}

/**
* @param cartData
* @param entryGroups
* @param entries
*/	
	private void setEnrtiesData(final CartData cartData, List entryGroups, List entries)
	{
		Collections.reverse(entryGroups);
		Collections.reverse(entries);
		cartData.setRootGroups(entryGroups);
		cartData.setEntries(entries);
	}
	
	protected AddressModel getInstallationAddressModelForCode(final String code)
	{
		Assert.notNull(code, "Parameter code cannot be null.");
		final CartModel cartModel = getCart();
		if (cartModel != null)
		{
			for (final AddressModel address : llaInstallationAddressService.getSupportedInstallationAddressesForOrder(cartModel, false))
			{
				if (code.equals(address.getPk().toString()))
				{
					return address;
				}
			}
		}
		return null;
	}

	protected AddressModel creteInstallationAddressModel(final AddressData addressData, final CartModel cartModel)
	{
		final AddressModel addressModel = getModelService().create(AddressModel.class);
		getAddressReversePopulator().populate(addressData, addressModel);
		addressModel.setOwner(cartModel);
		return addressModel;
	}

	protected AddressData getInstallationAddress()
	{
		final CartModel cart = getCart();
		if (cart != null)
		{
			final AddressModel installationAddress = cart.getInstallationAddress();
			if (installationAddress != null)
			{
				// Ensure that the delivery address is in the set of supported addresses
				final AddressModel supportedAddress = getInstallationAddressModelForCode(installationAddress.getPk().toString());
				if (supportedAddress != null)
				{
					return getAddressConverter().convert(supportedAddress);
				}
			}
		}
		return null;
	}

	private CreditScoreData getCreditScore(CreditScoreModel creditScoreModel){

		CreditScoreData creditScoreData =getLlaCreditScoreConverter().convert(creditScoreModel);
		return creditScoreData;
	}

	@Override
	public boolean setInstallationAddressIfAvailable()
	{
		final CartModel cartModel = getCart();
		//TODO to set default installation address

        final UserModel currentUser = getCurrentUserForCheckout();
		if (cartModel.getUser().equals(currentUser))
		{
			if(currentUser.getDefaultInstallationAddress() !=null){
				return setDefaultOrFirstInstallationAddress(cartModel, currentUser.getDefaultInstallationAddress());
			}else{
				final List<AddressModel> supportedInstallationAddresses = llaInstallationAddressService.getSupportedInstallationAddressesForOrder(cartModel,
						true);
				if (supportedInstallationAddresses != null && !supportedInstallationAddresses.isEmpty())
				{
					return setDefaultOrFirstInstallationAddress(cartModel, supportedInstallationAddresses.get(0));
				}
			}
		}
		return false;
	}
	
	@Override
	public AddressModel setCreditCheckAddress(final AddressData addressData, final CartModel cartModel)
	{
		final AddressModel addressModel = new AddressModel();
		getAddressReversePopulator().populate(addressData, addressModel);
		addressModel.setOwner(cartModel.getUser());
		getModelService().save(addressModel);
		cartModel.setCreditCheckAddress(addressModel);
		getModelService().save(cartModel);
		return addressModel;
	}

	/**
	 *
	 * @param cartModel
	 * @param addressModel
	 * @return
	 */
	private boolean setDefaultOrFirstInstallationAddress(CartModel cartModel, AddressModel addressModel) {
		if(null!=addressModel) {
			final CommerceCheckoutParameter parameter = createCommerceCheckoutParameter(cartModel, true);
			parameter.setAddress(addressModel);
			parameter.setIsDeliveryAddress(false);
			parameter.setIsInstallationAddress(Boolean.TRUE);
			return llaCommerceCheckoutService.setInstallationAddress(parameter);
		}else{
			return false;
		}
	}

	@Override
	public List<CartModificationData> validateCartForDevice() throws CommerceCartModificationException
	{
		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		parameter.setCart(getCartService().getSessionCart());
		return Converters.convertAll(validateCartForDevice(parameter), getCartModificationConverter());
	}

	private List<CommerceCartModification> validateCartForDevice(final CommerceCartParameter parameter)
			throws CommerceCartModificationException
	{
		final List<CommerceCartModification> modifications = new ArrayList<CommerceCartModification>();

		if (!validateCart(parameter))
		{
			final CommerceCartModification modification = new CommerceCartModification();
			modification.setStatusCode(LLACommerceCartModificationStatus.DEVICE_COMPATIBILITY_ERROR.getCode());
			modifications.add(modification);
		}
		return modifications;
	}

	private boolean validateCart(CommerceCartParameter parameter)
	{
		boolean containsDevice = false;
		int deviceQuantity = 0;
		int planQuantity = 0;
		CartModel cartModel = parameter.getCart();
		for (final AbstractOrderEntryModel entry : cartModel.getEntries())
		{
			final ProductModel product = productService.getProductForCode(entry.getProduct().getCode());
			if (llaCommonUtil.containsFirstLevelCategoryWithCode(product, CATEGORY_DEVICES))
			{
				deviceQuantity = entry.getQuantity().intValue() + deviceQuantity;
			}
			else if (llaCommonUtil.containsFirstLevelCategoryWithCode(product, CATEGORY_POSTPAID)
					|| llaCommonUtil.containsFirstLevelCategoryWithCode(product, CATEGORY_4PBUNDLE))
			{
				planQuantity = entry.getQuantity().intValue() + planQuantity;
			}
		}

		if (deviceQuantity > planQuantity)
		{
			return false;
		}

		return true;
	}

	public Converter<CreditScoreModel, CreditScoreData> getLlaCreditScoreConverter() {
		return llaCreditScoreConverter;
	}
     @Required
	public void setLlaCreditScoreConverter(Converter<CreditScoreModel, CreditScoreData> llaCreditScoreConverter) {
		this.llaCreditScoreConverter = llaCreditScoreConverter;
	}

	@Override
	public OrderData placeOrder() throws InvalidCartException
	{
		final CartModel cartModel = getCart();
		if (cartModel != null)
		{
			if (cartModel.getUser().equals(getCurrentUserForCheckout()) || getCheckoutCustomerStrategy().isAnonymousCheckout())
			{
				beforePlaceOrder(cartModel);
				final OrderModel orderModel = placeOrder(cartModel);
				afterPlaceOrder(cartModel, orderModel);
				if (orderModel != null)
				{
					return getOrderConverter().convert(orderModel);
				}
			}
		}
		return null;
	}
	@Override
	protected void afterPlaceOrder(@SuppressWarnings("unused") final CartModel cartModel, final OrderModel orderModel) //NOSONAR
	{
		if (orderModel != null)
		{
			if(llaCommonUtil.isSitePuertorico()){
				if(null!=cartModel.getSelectedAddress()){
					orderModel.setServiceabilityCheck(Boolean.TRUE);
					orderModel.setServiceQualResultRequest(cartModel.getServiceQualResultRequest());
				}
				if(null!=cartModel.getCreditScoreLevel()){
					orderModel.setCreditCheck(Boolean.TRUE);
				}
				getModelService().save(orderModel);
			}
			getCartService().removeSessionCart();
			getModelService().refresh(orderModel);
		}
	}


}