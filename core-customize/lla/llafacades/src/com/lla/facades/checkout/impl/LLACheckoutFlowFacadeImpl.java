/**
 *
 */
package com.lla.facades.checkout.impl;

import com.lla.facades.checkout.LLACheckoutFacade;
import de.hybris.platform.acceleratorfacades.flow.impl.DefaultCheckoutFlowFacade;
import de.hybris.platform.commercefacades.order.data.CartData;

import com.lla.facades.checkout.LLACheckoutFlowFacade;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CashOnDeliveryPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.OnlinePaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PayInStorePaymentInfoModel;
import de.hybris.platform.jalo.order.payment.CashOnDeliveryPaymentInfo;
import de.hybris.platform.jalo.order.payment.OnlinePaymentInfo;

import javax.annotation.Resource;

/**
 * @author GZ132VA
 *
 */
public class LLACheckoutFlowFacadeImpl extends DefaultCheckoutFlowFacade implements LLACheckoutFlowFacade
{
	@Resource(name = "checkoutFacade")
	private LLACheckoutFacade llaCheckoutFacade;
	@Override
	public Boolean hasNoInstallationAddress()
	{
		final CartData cartData = llaCheckoutFacade.getCheckoutCart();
		return cartData == null || cartData.getInstallationAddress() == null;
	}

	@Override
	public Boolean hasNoAdditionalPaymentInfo() {
		final CartModel cartModel = getCartService().getSessionCart();
		return !(cartModel.getPaymentInfo() instanceof OnlinePaymentInfoModel || cartModel.getPaymentInfo() instanceof CashOnDeliveryPaymentInfoModel ||
				cartModel.getPaymentInfo() instanceof PayInStorePaymentInfoModel);

	}


}