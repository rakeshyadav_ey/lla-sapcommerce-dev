/**
 *
 */
package com.lla.facades.checkout;

import de.hybris.platform.b2ctelcofacades.bundle.TmaCheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import java.util.List;


/**
 * @author GZ132VA
 *
 */
public interface LLACheckoutFacade extends TmaCheckoutFacade
{
	List<AddressData> getSupportedInstallationAddresses(final boolean flag);

	AddressData getInstallationAddressForCode(final String code);

	boolean setInstallationAddress(final AddressData addressData);
	boolean setInstallationAddressIfAvailable();
	AddressModel setCreditCheckAddress(final AddressData addressData, final CartModel cartModel);
	List<CartModificationData> validateCartForDevice() throws CommerceCartModificationException;
}
