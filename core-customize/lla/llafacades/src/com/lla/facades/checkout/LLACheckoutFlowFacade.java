/**
 *
 */
package com.lla.facades.checkout;

import de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade;


/**
 * @author GZ132VA
 *
 */
public interface LLACheckoutFlowFacade extends CheckoutFlowFacade
{
	Boolean hasNoInstallationAddress();

	Boolean hasNoAdditionalPaymentInfo();
}
