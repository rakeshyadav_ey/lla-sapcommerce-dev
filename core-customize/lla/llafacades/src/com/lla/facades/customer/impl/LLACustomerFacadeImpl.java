/**
 *
 */
package com.lla.facades.customer.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lla.core.service.LLACustomerAccountService;
import com.lla.core.service.customer.LLACustomerService;
import com.lla.core.util.LLACommonUtil;
import com.lla.facades.customer.LLACustomerFacade;
import com.lla.mulesoft.integration.dto.ViewPlanResponse;
import com.lla.mulesoft.integration.service.LLAMulesoftViewPlanService;
import de.hybris.platform.b2ctelcofacades.user.impl.TmaDefaultCustomerFacade;
import de.hybris.platform.b2ctelcoservices.services.TmaIdentificationService;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.lla.core.constants.LlaCoreConstants.USER_CALLBACK_EVENT_NAME;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

/**
 * The type Lla customer facade.
 *
 * @author GZ132VA
 */
public class LLACustomerFacadeImpl extends TmaDefaultCustomerFacade implements LLACustomerFacade
{
	private static final Logger LOG = Logger.getLogger(LLACustomerFacadeImpl.class);

	/**
	 * The constant CUSTOMER_BILLING_ACCOUNT.
	 */
	public static final String CUSTOMER_BILLING_ACCOUNT = "BillingAccount";
	/**
	 * The constant CUSTOMER_SERVICE_ACCOUNT.
	 */
	public static final String CUSTOMER_SERVICE_ACCOUNT = "ServiceAccount";
	/**
	 * The constant DEFAULT_SYSTEM_ID.
	 */
	public static final String DEFAULT_SYSTEM_ID = "Siebel";
	private static final String PROCESS_MSG = "Process: ";
	@Resource(name = "customerAccountService")
	private LLACustomerAccountService llaCustomerAccountService;
	@Autowired
	private LLAMulesoftViewPlanService llaMulesoftViewPlanService;

	@Autowired
	private LLACustomerService llaCustomerService;

	@Autowired
	private LLACommonUtil llaCommonUtil;

	/**
	 * The Business process service.
	 */
	@Autowired
	BusinessProcessService businessProcessService;

	/**
	 * Instantiates a new Lla customer facade.
	 *
	 * @param enumerationService       the enumeration service
	 * @param tmaIdentificationService the tma identification service
	 */
	public LLACustomerFacadeImpl(final EnumerationService enumerationService,
			final TmaIdentificationService tmaIdentificationService)
	{
		super(enumerationService, tmaIdentificationService);
		// XXX Auto-generated constructor stub
	}

	@Override
	public void register(final RegisterData registerData) throws DuplicateUidException
	{
		validateParameterNotNullStandardMessage("registerData", registerData);
		Assert.hasText(registerData.getFirstName(), "The field [FirstName] cannot be empty");
		Assert.hasText(registerData.getLogin(), "The field [Login] cannot be empty");

		final CustomerModel newCustomer = getModelService().create(CustomerModel.class);
		newCustomer.setFlowId(registerData.getFlowId());
		newCustomer.setFirstName(registerData.getFirstName());
		newCustomer.setLastName(registerData.getLastName());
		setCommonPropertiesForRegister(registerData, newCustomer);
		getCustomerAccountService().register(newCustomer, registerData.getPassword());
	}

	@Override
	public void createCustomerSubscriptionPlans(final CustomerModel customerModel)
	{

		final List<String> mergedAccounts = new ArrayList<>();
		mergedAccounts.addAll(customerModel.getLiberateAccounts());
		mergedAccounts.addAll(customerModel.getCerillionAccounts());
		final String customerId = customerModel.getUid();
		final String serviceResponse = llaMulesoftViewPlanService.getCustomerPlanDetails(mergedAccounts, customerModel.getUid());

		final ObjectMapper mapper = new ObjectMapper();
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		if (StringUtils.isNotEmpty(serviceResponse))
		{
			getSessionService().setAttribute("viewPlanResponse", serviceResponse);
			try
			{
				final List<ViewPlanResponse> viewPlanResponseList = Arrays
						.asList(mapper.readValue(serviceResponse, ViewPlanResponse[].class));
				LOG.info("View Plan Response Parsed " + viewPlanResponseList);
				llaCustomerService.createCustomerSubscriptionPlans(viewPlanResponseList, customerId);

			}
			catch (final JsonProcessingException e)
			{
				LOG.error("Error Parsing View Plan Response", e);
			}

		}
		else
		{
			LOG.error(String.format("Received invalid response from Service :: %s", serviceResponse));
		}

	}

	@Override
	public void updateCartWithGuestForAnonymousCheckout(final CustomerData guestCustomerData)
	{
		// First thing to do is to try to change the user on the session cart
		if (getCartService().hasSessionCart())
		{
			getCartService().changeCurrentCartUser(getUserService().getUserForUID(guestCustomerData.getUid()));
		}

		// Update the session currency (which might change the cart currency)
		if (!updateSessionCurrency(guestCustomerData.getCurrency(), getStoreSessionFacade().getDefaultCurrency()))
		{
			// Update the user
			getUserFacade().syncSessionCurrency();
		}

		/*
		 * if (!updateSessionLanguage(guestCustomerData.getLanguage(), getStoreSessionFacade().getDefaultLanguage())) { //
		 * Update the user getUserFacade().syncSessionLanguage(); }
		 */

		// Calculate the cart after setting everything up
		if (getCartService().hasSessionCart())
		{
			final CartModel sessionCart = getCartService().getSessionCart();

			// Clear the delivery address, delivery mode, payment info before starting the guest checkout.
			sessionCart.setDeliveryAddress(null);
			sessionCart.setDeliveryMode(null);
			sessionCart.setPaymentInfo(null);
			getCartService().saveOrder(sessionCart);
			getUserService().setCurrentUser(sessionCart.getUser());
			final Authentication oldAuthentication = SecurityContextHolder.getContext().getAuthentication();
			final UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(
					sessionCart.getUser().getUid(), null, oldAuthentication.getAuthorities());
			newAuthentication.setDetails(oldAuthentication.getDetails());
			SecurityContextHolder.getContext().setAuthentication(newAuthentication);
			try
			{
				final CommerceCartParameter parameter = new CommerceCartParameter();
				parameter.setEnableHooks(true);
				parameter.setCart(sessionCart);
				getCommerceCartService().recalculateCart(parameter);
			}
			catch (final CalculationException ex)
			{
				LOG.error("Failed to recalculate order [" + sessionCart.getCode() + "]", ex);
			}
		}
	}

	@Override
	public String updateCustomer(final String correlationId, final String customerId, final String accountId,
			final String accountType, final String systemId)
	{

		final CustomerModel customer = llaCustomerService.getCutsomerForCorrelationID(correlationId);
		if (customer != null && StringUtils.isNotEmpty(customerId))
		{
			customer.setCustomerID(customerId);

		}
		else
		{
			return "Customer does not exist for correlationId : " + correlationId;
		}
		if (StringUtils.isNotEmpty(accountId) && StringUtils.isNotEmpty(accountType)
				&& accountType.equals(CUSTOMER_BILLING_ACCOUNT))
		{
			return llaCustomerService.updateCustomerBillingAccount(customer, accountId,
					systemId.equals(null) ? DEFAULT_SYSTEM_ID : systemId);
		}
		else if (StringUtils.isNotEmpty(accountId) && StringUtils.isNotEmpty(accountType)
				&& accountType.equals(CUSTOMER_SERVICE_ACCOUNT))
		{
			return llaCustomerService.updateCustomerServiceAccount(customer, accountId,
					systemId.equals(null) ? DEFAULT_SYSTEM_ID : systemId);
		}
		else
		{
			return "Please send valid accountId and accountType";

		}
	}

	@Override
	public void updateCustomerDetails(AddressData addressData) throws ParseException
	{
		CartModel cart = getCartService().getSessionCart();
		if(Objects.nonNull(cart) && llaCommonUtil.isSiteVTR())
		{
			CustomerModel user = (CustomerModel) cart.getUser();
			if(Objects.nonNull(user))
			{
				user.setFirstName(addressData.getFirstName());
				user.setLastName(addressData.getLastName());
				Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(addressData.getDateOfBirth());
				user.setDateOfBirth(date1);
				user.setCsgCustomerNo(addressData.getCustomerIDCard());
				user.setAddressId(addressData.getAddressID());
				user.setAdditionalEmail(addressData.getEmail());
			}
			getModelService().save(user);
			getModelService().save(cart);
		}
	}

	@Override
	public void updateOrderProcess(String correlationId)
	{
		final CustomerModel customer = llaCustomerService.getCutsomerForCorrelationID(correlationId);
		Collection<OrderModel> orders = customer.getOrders();
		if(CollectionUtils.isNotEmpty(orders) && null!=customer.getCustomerID() && CollectionUtils.isNotEmpty(customer.getBillingAccounts()))
		{
			orders.forEach(order -> {
				Collection<OrderProcessModel> orderProcesses = order.getOrderProcess();
				 if(CollectionUtils.isNotEmpty(orderProcesses)) {
					 orderProcesses.forEach(orderProcess -> {
						LOG.info(String.format("Triggering user callback event for order %s", order.getCode()));
						businessProcessService.triggerEvent(
								orderProcess.getCode() + "_"
										+ USER_CALLBACK_EVENT_NAME);
						LOG.info(String.format("Process: %s fired event  %s", orderProcess.getCode(), USER_CALLBACK_EVENT_NAME));
				});
			}
		});
	  }
	}
	/**
	 * Sets existing customer details.
	 *
	 * @param mobilePhone      the mobile phone
	 * @param guestEmail       the guest email
	 * @param guestName        the guest name
	 * @param existingCustomer the existing customer
	 */
	@Override
	public void setExistingCustomerDetails(String mobilePhone, String guestEmail, String guestName,Boolean existingCustomer)
	{
		CartModel cart = getCartService().getSessionCart();
		if(Objects.nonNull(cart) && llaCommonUtil.isSitePuertorico())
		{
			if(StringUtils.isNotEmpty(mobilePhone)){
				cart.setPhoneNumber(mobilePhone);
			}
			if(StringUtils.isNotEmpty(guestEmail)){
				cart.setCustomerEmail(guestEmail);
			}
			if(StringUtils.isNotEmpty(guestName)){
				cart.setName(guestName);
			}
			if(Objects.nonNull(existingCustomer))
			{
				cart.setIsCustomerExisting(existingCustomer);
			}
			getModelService().save(cart);
			getModelService().refresh(cart);
		}
	}

	@Override
	public void createGuestUserForAnonymousCheckout(String email, String name) throws DuplicateUidException
	{
		validateParameterNotNullStandardMessage("email", email);
		final CustomerModel guestCustomer = getModelService().create(CustomerModel.class);
		final String guid = generateGUID();

		//takes care of localizing the name based on the site language
		guestCustomer.setUid(guid + "|" + email);
		guestCustomer.setName(name);
		guestCustomer.setType(CustomerType.valueOf(CustomerType.GUEST.getCode()));
		guestCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		guestCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		CartModel cart = getCartService().getSessionCart();
		if(Objects.nonNull(cart) && StringUtils.isNotEmpty(cart.getRutNumber()) && StringUtils.isNotEmpty(cart.getPhoneNumber()))
		{
			guestCustomer.setMobilePhone(cart.getPhoneNumber());
			guestCustomer.setRutNumber(cart.getRutNumber());
		}

		getCustomerAccountService().registerGuestForAnonymousCheckout(guestCustomer, guid);
		updateCartWithGuestForAnonymousCheckout(getCustomerConverter().convert(guestCustomer));
	}

}
