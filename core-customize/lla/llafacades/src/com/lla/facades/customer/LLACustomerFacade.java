/**
 *
 */
package com.lla.facades.customer;

import de.hybris.platform.b2ctelcofacades.user.TmaCustomerFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.CustomerModel;

import java.text.ParseException;

/**
 * The interface Lla customer facade.
 *
 * @author GZ132VA
 */
public interface LLACustomerFacade extends TmaCustomerFacade
{
	/**
	 * Create customer subscription plans.
	 *
	 * @param customerModel the customer model
	 */
	void createCustomerSubscriptionPlans(final CustomerModel customerModel);

	/**
	 * Update customer string.
	 *
	 * @param correlationId the correlation id
	 * @param customerId    the customer id
	 * @param accountId     the account id
	 * @param accountType   the account type
	 * @param systemId      the system id
	 * @return string
	 */
	String updateCustomer(String correlationId, String customerId, String accountId, String accountType, String systemId);

	/**
	 * Update customer details.
	 *
	 * @param addressData the address data
	 * @throws ParseException the parse exception
	 */
	void updateCustomerDetails(AddressData addressData) throws ParseException;

	/**
	 * Update order process.
	 *
	 * @param correlationId the correlation id
	 */
	void updateOrderProcess(String correlationId);

	/**
	 * Sets existing customer details.
	 *
	 * @param mobilePhone      the mobile phone
	 * @param guestEmail       the guest email
	 * @param guestName        the guest name
	 * @param existingCustomer the existing customer
	 */
	void setExistingCustomerDetails(String mobilePhone,String guestEmail,String guestName,Boolean existingCustomer);

	// void mapBillingAccountToCustomer(final CustomerModel customerModel, final Set<TmaBillingAccountModel> billingAccountModelList);
}
