package com.lla.facades.product.mapping;

import org.springframework.beans.factory.annotation.Required;

import java.util.Map;

public class LLACategoryRedirectUrlMapping {
    private Map<String, String> mapping;

    public Map<String, String> getMapping()
    {
        return mapping;
    }

    @Required
    public void setMapping(final Map<String, String> mapping)
    {
        this.mapping = mapping;
    }

}
