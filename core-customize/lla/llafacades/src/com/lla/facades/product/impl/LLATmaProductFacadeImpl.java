package com.lla.facades.product.impl;

import com.lla.facades.channel.data.ChannelData;
import de.hybris.platform.b2ctelcofacades.product.TmaProductOfferFacade;
import com.lla.facades.product.LLATmaProductFacade;
import de.hybris.platform.b2ctelcofacades.product.impl.DefaultTmaProductFacade;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.b2ctelcoservices.services.TmaPoService;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.helper.ProductAndCategoryHelper;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Resource;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import  org.apache.commons.collections.CollectionUtils;

import com.lla.core.model.LLABundleProductMappingModel;
import com.lla.core.model.LLAConfiguratorMessageModel;
import com.lla.core.service.product.LLAProductService;
import com.lla.core.util.LLACommonUtil;
import com.lla.facades.product.LLATmaProductFacade;
import com.lla.facades.productconfigurator.data.LLAPuertoRicoProductConfigDTO;

public class LLATmaProductFacadeImpl extends DefaultTmaProductFacade implements LLATmaProductFacade {


	private static final String PANAMA_ADDON_PRODUCTS_CODES = "addon.product.codes.panama";
	private static final String PANAMA_ADDON_HDBOX = "2p.internet.telephone";
	private static final String PANAMA_CATEGORIES_CODES_TO_SHOW_ADDON_LIST = "categories.codes.to.show.addon.products";
	private static final String CHANNEL_ADDON = "channelsaddoncabletica";

	@Resource(name = "commerceCategoryService")
	private CommerceCategoryService commerceCategoryService;
    @Resource(name = "categoryConverter")
    private Converter<CategoryModel, CategoryData> categoryConverter;

    @Resource(name = "productAndCategoryHelper")
    private ProductAndCategoryHelper productAndCategoryHelper;

    @Resource(name = "llaProductConverter")
 	 private Converter<ProductModel, ProductData> llaProductConverter;

 	 @Autowired
 	 private LLAProductService productService;

 	@Autowired
	 private CategoryService categoryService;

	@Resource(name = "tmaProductOfferFacade")
	private TmaProductOfferFacade productOfferFacade;

	@Autowired
	private TmaPoService tmaPoService;

	@Autowired
	private ConfigurationService configurationService;

   @Autowired
   private CartService cartService;

	@Autowired
	private LLACommonUtil llaCommonUtil;

	final List<ProductOption> options = Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.SUMMARY,
			ProductOption.PRICE_RANGE, ProductOption.DESCRIPTION, ProductOption.SOLD_INDIVIDUALLY, ProductOption.GALLERY,
			ProductOption.CATEGORIES, ProductOption.REVIEW, ProductOption.PROMOTIONS, ProductOption.CLASSIFICATION,
			ProductOption.VARIANT_MATRIX, ProductOption.VARIANT_MATRIX_ALL_OPTIONS, ProductOption.STOCK, ProductOption.VOLUME_PRICES,
			ProductOption.DELIVERY_MODE_AVAILABILITY, ProductOption.SPO_BUNDLE_TABS, ProductOption.PRODUCT_SPEC_CHAR_VALUE_USE,
			ProductOption.PRODUCT_OFFERING_PRICES, ProductOption.VARIANT_FULL, ProductOption.REFERENCES,
			ProductOption.VARIANT_FIRST_VARIANT, ProductOption.VARIANT_MATRIX_BASE, ProductOption.VARIANT_MATRIX_MEDIA,
			ProductOption.VARIANT_MATRIX_URL, ProductOption.VARIANT_MATRIX_STOCK, ProductOption.URL, ProductOption.KEYWORDS,
			ProductOption.IMAGES, ProductOption.PRODUCT_SPECIFICATION,
			ProductOption.PRODUCT_BPO_CHILDREN, ProductOption.PARENT_BPOS,
			ProductOption.PRODUCT_OFFERING_GROUPS, ProductOption.VARIANT_MATRIX_PRICE);
	final Collection<ProductOption> PRODUCT_OPTIONS = Arrays.stream(ProductOption.values()).collect(Collectors.toList());

	@Override
	public List<CategoryData> getProductCategories(final TmaProductOfferingModel productModel) {
		final List<CategoryModel> categoryModels = new ArrayList<>();
		Collection<CategoryModel> supercategories=productModel.getSupercategories();

		while (!supercategories.isEmpty())
		{
			CategoryModel toDisplay = null;
			toDisplay = processCategoryModels(supercategories, toDisplay);
			supercategories= new ArrayList<>();
			if (toDisplay != null)
			{
				categoryModels.add(toDisplay);
				supercategories.addAll(toDisplay.getSupercategories());
			}
		}
		return categoryConverter.convertAll(categoryModels);
	}

	@Override
	public List<ProductData> getProductDataForCategory(final String categoryCode) {
		final List<ProductData> dataList=new ArrayList<>();
		for(final ProductModel product:getProductService().getProductsForCategory(commerceCategoryService.getCategoryForCode(categoryCode))){
			dataList.add(productOfferFacade.getPoForCode(product.getCode(), Arrays.asList(ProductOption.BASIC, ProductOption.GALLERY, ProductOption.CATEGORIES, ProductOption.IMAGES, ProductOption.PRODUCT_SPEC_CHAR_VALUE_USE, ProductOption.PRICE, ProductOption.PRODUCT_OFFERING_PRICES,ProductOption.PRODUCT_SPECIFICATION)));
		}
		return dataList;
	}

	/**
	 * Return Product Data List for Product Model List
	 * @param tmaProductOfferingModelsList
	 * @return
	 */
	@Override
	public List<ProductData> getProductDataListForModels(final List<TmaProductOfferingModel> tmaProductOfferingModelsList) {
		final List<ProductData> dataList=new ArrayList<>();
		for(TmaProductOfferingModel item:tmaProductOfferingModelsList){
			dataList.add(productOfferFacade.getPoForCode(item.getCode(), Arrays.asList(ProductOption.BASIC, ProductOption.GALLERY, ProductOption.CATEGORIES, ProductOption.IMAGES, ProductOption.PRODUCT_SPEC_CHAR_VALUE_USE, ProductOption.PRICE, ProductOption.PRODUCT_OFFERING_PRICES)));
		}

		return dataList;
	}
    /**
     * Return Map
     * @param productCode
     * @return a map of key-value pairs of <{@link String} and {@link ChannelData}>.
     */
	@Override
	public Map<String, List<ChannelData>> fetchChannelData(String productCode) {
		ProductModel productModel=productService.getProductForCode(productCode);
		ProductData productData=llaProductConverter.convert(productModel);
		return MapUtils.isNotEmpty(productData.getCategorySpecificChannels()) ?productData.getCategorySpecificChannels() : Collections.EMPTY_MAP;
	}

	private CategoryModel processCategoryModels(final Collection<CategoryModel> categoryModels, final CategoryModel toDisplay)
	{
		CategoryModel categoryToDisplay = toDisplay;
		for (final CategoryModel categoryModel : categoryModels)
		{
			if (productAndCategoryHelper.isValidProductCategory(categoryModel))
			{
				if (categoryToDisplay == null)
				{
					categoryToDisplay = categoryModel;
				}
			}
		}
		return categoryToDisplay;
	}

	@Override
	public List<ProductModel> getProductForCategory(final String categoryCode)
	{
		final CategoryModel categoryModel = categoryService.getCategoryForCode(categoryCode);
		final List<ProductModel> productList = productService.getProductsForCategory(categoryModel);
		final List<ProductModel> finalProducts = productList.stream().sorted(Comparator.comparing(ProductModel::getCode).reversed())
				.collect(Collectors.toList());
		return finalProducts;

	}


	@Override
	public TmaProductOfferingModel getProductForCode(final String productCode)
	{
		return tmaPoService.getPoForCode(productCode);
	}

	@Override
	public Set<TmaProductOfferingModel> getTvProductsForInternet(final String oneP) {
		final List<LLABundleProductMappingModel> bundles=productService.getAllBundleMapping();
		final Set<TmaProductOfferingModel> products2P= new HashSet<>();
		if(StringUtils.isNotEmpty(oneP)) {
			final List<LLABundleProductMappingModel> possibleBundles = bundles.stream()
					.filter(bundle -> null != bundle.getInternetPlan() && bundle.getInternetPlan().getCode().equalsIgnoreCase(oneP))
					.collect(Collectors.toList());
			for(final LLABundleProductMappingModel bundle: possibleBundles){
				if(null != bundle.getTv())
				{
					products2P.add(bundle.getTv());
				}
			}
		}else{
			for(final LLABundleProductMappingModel bundle: bundles){
				if(null!= bundle.getTv())
				{
					products2P.add(bundle.getTv());
				}
			}
		}
		return products2P;
	}

	@Override
	public String getBundleMessage(final LLAPuertoRicoProductConfigDTO productConfig, final int currentStepId)
	{
		final List<LLABundleProductMappingModel> bundles = productService.getAllBundleMapping();
		if (currentStepId == 2)
		{
			return getMessageForStepTwo(bundles, productConfig.getOneP(), currentStepId);
		}
		if (currentStepId == 3)
		{
			return getMessageForStepThree(bundles, productConfig.getOneP(), productConfig.getTwoP(), currentStepId);
		}
		if (currentStepId == 4)
		{
			return getMessageForStepFour(bundles, productConfig.getOneP(), productConfig.getTwoP(), productConfig.getThreeP(),
					currentStepId);
		}

		return null;
	}

	/**
	 * @param bundles
	 * @param oneP
	 * @param twoP
	 * @param threeP
	 * @param currentStepId
	 * @return
	 */
	private String getMessageForStepFour(final List<LLABundleProductMappingModel> bundles, final String oneP, final String twoP,
										 final String threeP, final int currentStepId)
	{
		final List<LLABundleProductMappingModel> possibleBundles = new ArrayList<LLABundleProductMappingModel>();

		for (final LLABundleProductMappingModel bundle : bundles)
		{
			if (StringUtils.isNotEmpty(oneP) && StringUtils.isNotEmpty(twoP) && StringUtils.isNotEmpty(threeP))
			{
				if (null != bundle.getInternetPlan() && bundle.getInternetPlan().getCode().equalsIgnoreCase(oneP)
						&& null != bundle.getTv() && bundle.getTv().getCode().equalsIgnoreCase(twoP)
						&& null != bundle.getTelephonePlan() && bundle.getTelephonePlan().getCode().equalsIgnoreCase(threeP))
				{
					possibleBundles.add(bundle);
				}

			}
			if (StringUtils.isEmpty(oneP))
			{
				getBundlesIfNoInternetPlan(bundle, possibleBundles, twoP, threeP);
			}
			if (StringUtils.isEmpty(twoP))
			{
				getBundlesIfNoTVtPlan(bundle, possibleBundles, oneP, threeP);
			}

		}
		return getMessageFromBundle(possibleBundles, currentStepId);
	}
	/**
	 * @param bundle
	 * @param possibleBundles
	 * @param oneP
	 * @param threeP
	 */
	private void getBundlesIfNoTVtPlan(final LLABundleProductMappingModel bundle,
									   final List<LLABundleProductMappingModel> possibleBundles, final String oneP, final String threeP)
	{
		if (StringUtils.isNotEmpty(oneP) && StringUtils.isNotEmpty(threeP))
		{
			if (null == bundle.getTv() && null != bundle.getInternetPlan()
					&& bundle.getInternetPlan().getCode().equalsIgnoreCase(oneP) && null != bundle.getTelephonePlan()
					&& bundle.getTelephonePlan().getCode().equalsIgnoreCase(threeP))
			{
				possibleBundles.add(bundle);
			}
		}
		else if (StringUtils.isNotEmpty(oneP) && StringUtils.isEmpty(threeP))
		{
			if (null == bundle.getTv() && null == bundle.getTelephonePlan() && null != bundle.getInternetPlan()
					&& bundle.getInternetPlan().getCode().equalsIgnoreCase(oneP))

			{
				possibleBundles.add(bundle);
			}
		}
	}

	/**
	 * @param bundle
	 * @param possibleBundles
	 * @param twoP
	 * @param threeP
	 */
	private void getBundlesIfNoInternetPlan(final LLABundleProductMappingModel bundle,
											final List<LLABundleProductMappingModel> possibleBundles, final String twoP, final String threeP)
	{
		if (StringUtils.isNotEmpty(twoP) && StringUtils.isNotEmpty(threeP))
		{
			if (null == bundle.getInternetPlan() && null != bundle.getTv() && bundle.getTv().getCode().equalsIgnoreCase(twoP)
					&& null != bundle.getTelephonePlan() && bundle.getTelephonePlan().getCode().equalsIgnoreCase(threeP))
			{
				possibleBundles.add(bundle);
			}

		}
		else if (StringUtils.isEmpty(twoP) && StringUtils.isNotEmpty(threeP))
		{
			if (null == bundle.getTv() && null == bundle.getInternetPlan() && null != bundle.getTelephonePlan()
					&& bundle.getTelephonePlan().getCode().equalsIgnoreCase(threeP))
			{
				possibleBundles.add(bundle);
			}

		}
		else if (StringUtils.isNotEmpty(twoP) && StringUtils.isEmpty(threeP))
		{
			if (bundle.getTelephonePlan() == null && bundle.getInternetPlan() == null && null != bundle.getTv()
					&& bundle.getTv().getCode().equalsIgnoreCase(twoP))
			{
				possibleBundles.add(bundle);
			}

		}
	}

	/**
	 * @param bundles
	 * @param oneP
	 * @return
	 */
	private String getMessageForStepThree(final List<LLABundleProductMappingModel> bundles, final String oneP, final String twoP,
										  final int currentStepId)
	{
		final List<LLABundleProductMappingModel> possibleBundles = new ArrayList<LLABundleProductMappingModel>();

		for (final LLABundleProductMappingModel bundle : bundles)
		{
			if (StringUtils.isNotEmpty(oneP) && StringUtils.isNotEmpty(twoP))
			{
				if (null != bundle.getInternetPlan() && bundle.getInternetPlan().getCode().equalsIgnoreCase(oneP)
						&& null != bundle.getTv() && bundle.getTv().getCode().equalsIgnoreCase(twoP))
				{
					possibleBundles.add(bundle);
				}
			}
			else if (StringUtils.isEmpty(oneP) && StringUtils.isNotEmpty(twoP))
			{
				if (bundle.getInternetPlan() == null && null != bundle.getTv() && bundle.getTv().getCode().equalsIgnoreCase(twoP))
				{
					possibleBundles.add(bundle);
				}
			}
			else if (StringUtils.isNotEmpty(oneP) && StringUtils.isEmpty(twoP))
			{
				if (bundle.getTv() == null && null != bundle.getInternetPlan()
						&& bundle.getInternetPlan().getCode().equalsIgnoreCase(oneP))
				{
					possibleBundles.add(bundle);
				}
			}
			else if (StringUtils.isEmpty(oneP) && StringUtils.isEmpty(twoP))
			{
				if (bundle.getTv() == null && bundle.getInternetPlan() == null)
				{
					possibleBundles.add(bundle);
				}
			}
		}
		return getMessageFromBundle(possibleBundles, currentStepId);
	}


	private String getMessageForStepTwo(final List<LLABundleProductMappingModel> bundles, final String oneP, final int stepId)
	{
		final List<LLABundleProductMappingModel> possibleBundles = new ArrayList<LLABundleProductMappingModel>();
		if (StringUtils.isNotEmpty(oneP))
		{
			for (final LLABundleProductMappingModel bundle : bundles)
			{
				if (null != bundle.getInternetPlan() && bundle.getInternetPlan().getCode().equalsIgnoreCase(oneP))
				{
					possibleBundles.add(bundle);
				}
			}
		}
		return getMessageFromBundle(possibleBundles, stepId);

	}


	private String getMessageFromBundle(final List<LLABundleProductMappingModel> possibleBundles, final int stepId)
	{

		if (!CollectionUtils.isEmpty(possibleBundles))
		{
			final LLAConfiguratorMessageModel message = productService.getBundleMessageForPackage(possibleBundles.get(0));
			if (null != message)
			{
				switch (stepId)
				{
					case 2:
						return message.getTvPageMessage();
					case 3:
						return message.getPhonePageMessage();
					case 4:
						return message.getAddonPageMessage();
				}
			}
		}
		return null;
	}

	@Override
	public List<ProductData> getAllProductDataForCategory(String categoryCode)
	{
		final Collection<ProductOption> productOptions = Arrays.stream(ProductOption.values()).collect(Collectors.toList());
		List<ProductData> dataList=new ArrayList<>();
		for(ProductModel product:getProductService().getProductsForCategory(commerceCategoryService.getCategoryForCode(categoryCode))){
			dataList.add(productOfferFacade.getPoForCode(product.getCode(), productOptions));
		}
		return dataList;
	}

	@Override
	public List<ProductData> getHomePageProducts(List<ProductData> allProducts)
	{
		return allProducts.stream().filter(p -> p.isIsHomepageProduct()).collect(Collectors.toList());
	}
	@Override
	public List<ProductData> getProductData(final List<TmaProductOfferingModel> tmaProductOfferingModels)
	{
		return Converters.convertAll(tmaProductOfferingModels, llaProductConverter);
	}

	@Override
	public List<ProductData> getPLPProducts(List<ProductData> allProducts)
	{
		return allProducts.stream().filter(p -> p.isIsPLPProduct()).collect(Collectors.toList());
	}

	@Override
	public List<ProductData> getAssociatedProducts(final List<ProductData> allProducts)
	{
		List<ProductData> associatedProducts;
		final List<ProductData> products = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(allProducts)) {
			allProducts.forEach(p -> {
				if(null != p.getAssociatedProductCode())
				p.getAssociatedProductCode().forEach(associateProduct -> {
					products.add(productOfferFacade.getPoForCode(associateProduct, PRODUCT_OPTIONS));

				});
			});
		}
		associatedProducts = products.stream()
				.filter(llaCommonUtil.distinctByKey(p-> p.getCode()))
				.collect(Collectors.toList());
		return associatedProducts;
	}
	@Override
	public List<ProductData> getAddonProductsForPanama()
	{

		if (checkIfAddonProductListRequired())
		{
			String productCodes = null;
			if (checkIfAddon2pProductListRequired())
			{

				productCodes = configurationService.getConfiguration().getString(PANAMA_ADDON_HDBOX);

			}

			else
			{
				productCodes = configurationService.getConfiguration().getString(PANAMA_ADDON_PRODUCTS_CODES);
			}

			final List<String> productCodeList = Stream.of(productCodes.split(",", -1)).collect(Collectors.toList());
			removeAddonCodeIfProductPresentInCart(productCodeList);
			final List<TmaProductOfferingModel> products = new ArrayList<TmaProductOfferingModel>();
			for (final String productCode : productCodeList)
			{
				products.add(getProductForCode(productCode));
			}
			if (!CollectionUtils.isEmpty(products))
			{
				return getProductDataWithOptions(products);
			}
		}
		return Collections.EMPTY_LIST;

	}

	@Override
	public boolean checkIfAddonProductListRequired()
	{

		final String categoryCodes = configurationService.getConfiguration().getString(PANAMA_CATEGORIES_CODES_TO_SHOW_ADDON_LIST);
		final List<String> categoryCodesList = Stream.of(categoryCodes.split(",", -1)).collect(Collectors.toList());

		final CartModel cart = cartService.getSessionCart();
		if (null != cart && !CollectionUtils.isEmpty(cart.getEntries()))
		{
			for (final AbstractOrderEntryModel entry : cart.getEntries())
			{
				for (final String categoryCode : categoryCodesList)
				{
					if (llaCommonUtil.containsFirstLevelCategoryWithCode(entry.getProduct(), categoryCode)
							|| llaCommonUtil.containsSecondLevelCategoryWithCode(entry.getProduct(), categoryCode))
					{
						return true;

					}
				}
			}
		}
		return false;



	}


	public boolean checkIfAddon2pProductListRequired()
	{



		final CartModel cart = cartService.getSessionCart();
		if (null != cart && !CollectionUtils.isEmpty(cart.getEntries()))
		{
			for (final AbstractOrderEntryModel entry : cart.getEntries())
			{

				if (llaCommonUtil.containsFirstLevelCategoryWithCode(entry.getProduct(), "2PInternetTelephone")
						|| llaCommonUtil.containsSecondLevelCategoryWithCode(entry.getProduct(), "2PInternetTelephone"))
				{
					return true;

				}

			}
		}
		return false;



	}



	/**
	 * @param productCodeList
	 */
	private void removeAddonCodeIfProductPresentInCart(final List<String> productCodeList)
	{
		final CartModel cart = cartService.getSessionCart();
		if (null != cart && !CollectionUtils.isEmpty(cart.getEntries()))
		{
			for (final AbstractOrderEntryModel entry : cart.getEntries())
			{
				final Iterator itr = productCodeList.iterator();
				while (itr.hasNext())
				{
					final String code = (String) itr.next();
					if (code.equalsIgnoreCase(entry.getProduct().getCode()))
					{
						itr.remove();
					}
				}
			}
		}

	}


	private List<ProductData> getProductDataWithOptions(final List<TmaProductOfferingModel> products)
	{

		final List<ProductOption> options = Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.SUMMARY,
				ProductOption.DESCRIPTION, ProductOption.SOLD_INDIVIDUALLY, ProductOption.GALLERY, ProductOption.CATEGORIES,
				ProductOption.REVIEW, ProductOption.PROMOTIONS, ProductOption.CLASSIFICATION, ProductOption.VARIANT_MATRIX,
				ProductOption.VARIANT_MATRIX_ALL_OPTIONS, ProductOption.STOCK, ProductOption.VOLUME_PRICES,
				ProductOption.DELIVERY_MODE_AVAILABILITY, ProductOption.SPO_BUNDLE_TABS, ProductOption.PRODUCT_SPEC_CHAR_VALUE_USE);
		final List<ProductData> productDataList = new ArrayList<ProductData>();
		for (final ProductModel product : products)
		{
			productDataList.add(productOfferFacade.getPoForCode(product.getCode(), options));
		}
		return productDataList;
	}

	@Override
	public List<ProductData> getProductDatas(final List<ProductModel> productModel)
	{
		return Converters.convertAll(productModel, llaProductConverter);

	}
	@Override
	public List<ProductData> getAddons(List<ProductData> allProducts)
	{
		List<ProductData> allAddonProduct = new ArrayList<>();
		List<ProductData> hfcAddons = new ArrayList<>();
		List<ProductData> ffthAddons = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(allProducts))
		{
			allProducts.forEach(p->{
				if(null != p.getHfcAddonProductCode())
				p.getHfcAddonProductCode().forEach(hfcaddon ->{
					hfcAddons.add(productOfferFacade.getPoForCode(hfcaddon,PRODUCT_OPTIONS));
				});
			});
			allAddonProduct.addAll(hfcAddons);

			allProducts.forEach(p->{
				if(null != p.getHfcAddonProductCode())
				p.getFfthAddonProductCode().forEach(ffthaddon ->{
					ffthAddons.add(productOfferFacade.getPoForCode(ffthaddon,PRODUCT_OPTIONS));
				});
			});
			allAddonProduct.addAll(ffthAddons);
		}

		return allAddonProduct.stream()
				.filter(llaCommonUtil.distinctByKey(p->p.getCode()))
				.collect(Collectors.toList());
	}

	@Override
	public List<ProductData> getAddonProductsForCabletica()
	{
		final CartModel cart = cartService.getSessionCart();
		Collection<TmaProductOfferingModel> products = new ArrayList<TmaProductOfferingModel>();
		final CategoryModel category = commerceCategoryService.getCategoryForCode(CHANNEL_ADDON);
		if (null != cart && !CollectionUtils.isEmpty(cart.getEntries()))
		{
			for (final AbstractOrderEntryModel entry : cart.getEntries())
			{
				if (!entry.getProduct().getSupercategories().contains(category))
				{
					products = ((TmaProductOfferingModel) entry.getProduct()).getChannelProductList();
				}
			}
		}
		if (!CollectionUtils.isEmpty(products))
		{
			return getProductDataWithOptions(new ArrayList<>(products));
		}
		return Collections.EMPTY_LIST;
	}

}
