package com.lla.facades.product;

import com.lla.facades.channel.data.ChannelData;
import de.hybris.platform.b2ctelcofacades.product.TmaProductFacade;
import de.hybris.platform.b2ctelcoservices.model.TmaProductOfferingModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.lla.facades.productconfigurator.data.LLAPuertoRicoProductConfigDTO;

public interface LLATmaProductFacade extends TmaProductFacade {

    List<CategoryData> getProductCategories(TmaProductOfferingModel productModel);


    List<ProductData> getProductDataForCategory(String categoryCode);

    List<ProductModel> getProductForCategory(String categoryCode);

	TmaProductOfferingModel getProductForCode(final String productCode);

    Set<TmaProductOfferingModel> getTvProductsForInternet(String oneP);

    String getBundleMessage(LLAPuertoRicoProductConfigDTO productConfig, int currentStepId);

    List<ProductData> getProductData(List<TmaProductOfferingModel> tmaProductOfferingModels);


    List<ProductData> getAllProductDataForCategory(String categoryCode);

    List<ProductData> getHomePageProducts(List<ProductData> allProducts);

    List<ProductData> getPLPProducts(List<ProductData> allProducts);

    List<ProductData> getAddonProductsForPanama();
    
    List<ProductData> getProductDatas(List<ProductModel> productModel);
	
    boolean checkIfAddonProductListRequired();

    List<ProductData> getAssociatedProducts(List<ProductData> allProducts);

    List<ProductData> getAddons(List<ProductData> allProducts);

    List<ProductData> getAddonProductsForCabletica();

    List<ProductData> getProductDataListForModels(final List<TmaProductOfferingModel> tmaProductOfferingModelsList);

    Map<String,List<ChannelData>>fetchChannelData(String productCode);



}
