import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {ConfigModule} from '@spartacus/core';
import {AppComponent} from './app.component';
import {translationChunksConfig, translations} from '@spartacus/assets';
import {TmaAuthModule, TmaB2cStorefrontModule, TmaProductSummaryModule, tmaTranslations, TmfModule} from '@spartacus/tua-spa';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    TmaAuthModule,
    TmfModule.forRoot(),
    TmaB2cStorefrontModule.withConfig({
      backend: {
        tmf: {
          baseUrl: 'https://api.cdoj51jxo6-lilacserv1-d2-public.model-t.cc.commerce.ondemand.com',
          prefix: '/llatelcotmfwebservices/v2/',
        },
        occ: {
          baseUrl: 'https://api.cdoj51jxo6-lilacserv1-d2-public.model-t.cc.commerce.ondemand.com',
          prefix: '/llacommercewebservices/v2/',
          endpoints: {
            product_scopes: {
              details:
                'products/${productCode}?fields=averageRating,stock(DEFAULT),description,availableForPickup,code,url,price(DEFAULT),numberOfReviews,manufacturer,categories(FULL),priceRange,multidimensional,configuratorType,configurable,tags,images(FULL),productOfferingPrice(FULL),productSpecification,validFor',
            },
            productSearch:
              'products/search?fields=products(code,name,summary,price(FULL),images(DEFAULT),stock(FULL),averageRating,variantOptions,productSpecification),facets,breadcrumbs,pagination(DEFAULT),sorts(DEFAULT),freeTextSearch',
          },
        }
      },
      routing: {
        routes: {
          product: {
            paths: ['product/:productCode/:name', 'product/:productCode'],
          }
        }
      },
      context: {
        urlParameters: ['baseSite', 'language', 'currency'],
        baseSite: ['vtr']
      },
      i18n: {
        resources: translations,
        chunks: translationChunksConfig,
        fallbackLang: 'en'
      },
      features: {
        level: '1.4'
      }
    }),
    ConfigModule.withConfig({
      i18n: {
        resources: tmaTranslations
      }
    }),
    TmaProductSummaryModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}