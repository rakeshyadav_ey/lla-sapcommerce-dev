/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.acceleratorstorefrontcommons.forms.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import com.lla.core.util.LLACommonUtil;



import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Validator for address forms. Enforces the order of validation
 */
@Component("addressValidator")
public class AddressValidator implements Validator
{
	private static final Logger LOG = Logger.getLogger(AddressValidator.class);
	private static final int MAX_FIELD_LENGTH = 255;
	private static final int MAX_FIELD_LENGTH_PANAMA = 50;
	private static final int MAX_POSTCODE_LENGTH = 10;
	
	@Autowired
	private LLACommonUtil llaCommonUtil;

	@Autowired
	private SiteConfigService siteConfigService;
	@Override
	public boolean supports(final Class<?> aClass)
	{
		return AddressForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final AddressForm addressForm = (AddressForm) object;
		validateCountrySpecificFields(addressForm, errors);
	}


	protected void validateCountrySpecificFields(final AddressForm addressForm, final Errors errors)
	{
		final String isoCode = addressForm.getCountryIso();
		if (isoCode != null)
		{
			switch (CountryCode.lookup(isoCode))
			{

				   case JAMAICA:

					/*validateStringField(addressForm.getCountryIso(), AddressField.COUNTRY, MAX_FIELD_LENGTH, errors);
					validateStringField(addressForm.getFirstName(), AddressField.FIRSTNAME, MAX_FIELD_LENGTH, errors);
					validateStringField(addressForm.getLastName(), AddressField.LASTNAME, MAX_FIELD_LENGTH, errors);*/
					validateStringField(addressForm.getLine1(), AddressField.LINE1, MAX_FIELD_LENGTH, errors);
					validateStringField(addressForm.getLine2(), AddressField.LINE2, MAX_FIELD_LENGTH, errors);
					validateStringField(addressForm.getTownCity(), AddressField.TOWN, MAX_FIELD_LENGTH, errors);
					validateStringField(addressForm.getArea(), AddressField.AREA, MAX_FIELD_LENGTH, errors);
					//validateStringField(addressForm.getPostcode(), AddressField.POSTCODE, MAX_POSTCODE_LENGTH, errors);
	            /*if (StringUtils.isEmpty(addressForm.getTrnNumber()) || StringUtils.length(addressForm.getTrnNumber()) > 11
	                    || !validateTrnPattern(addressForm.getTrnNumber()))
	            {
	                errors.rejectValue("trnNumber", "register.trnNumber.invalid");
	            }

	            if(null==addressForm.getDob() || StringUtils.isEmpty(addressForm.getDob().trim()) || StringUtils.length(addressForm.getDob().trim()) > 10|| !validateDOBFormat(addressForm.getDob())){
	              errors.rejectValue("dob", "register.dob.invalid");
	            }
	            if(!validateDOBIsAfterTodaysDate(addressForm.getDob())){
	                errors.rejectValue("dob", "register.dob.after.currentdate");
<<<<<<< HEAD
	            }
=======
	            }*/
					break;

               case PANAMA:

					validateStringField(addressForm.getCountryIso(), AddressField.COUNTRY, MAX_FIELD_LENGTH_PANAMA, errors);
					validateStringFieldLength(addressForm.getAppartment(), AddressField.APPARTMENT, MAX_FIELD_LENGTH_PANAMA, errors);
					validateStringFieldLength(addressForm.getBuilding(), AddressField.BUILDING, MAX_FIELD_LENGTH_PANAMA, errors);
					validateStringFieldLength(addressForm.getDistrict(), AddressField.DISTRICT, MAX_FIELD_LENGTH_PANAMA, errors);
					//validateStringField(addressForm.getStreetname(), AddressField.STREETNAME, MAX_FIELD_LENGTH_PANAMA, errors);
					validateStringField(addressForm.getProvince(), AddressField.PROVINCE, MAX_FIELD_LENGTH_PANAMA, errors);
					validateStringFieldLength(addressForm.getTownCity(), AddressField.TOWN, MAX_FIELD_LENGTH_PANAMA, errors);
					validateStringField(addressForm.getNeighbourhood(), AddressField.NEIGHBOURHOOD, MAX_FIELD_LENGTH_PANAMA, errors);
					if(llaCommonUtil.isPrepaidCart())
						validateStringFieldPrepaidDistrict(addressForm.getDistrict(), AddressField.DISTRICT, siteConfigService.getProperty("address.prepaid.default.district.name"), errors);
					break;
				case CABLETICA:
					validateStringFieldLength(addressForm.getBuilding(), AddressField.BUILDING, MAX_FIELD_LENGTH, errors);
					validateStringFieldLength(addressForm.getDistrict(), AddressField.DISTRICT, MAX_FIELD_LENGTH, errors);
					validateStringField(addressForm.getProvince(), AddressField.PROVINCE, MAX_FIELD_LENGTH, errors);
					validateStringField(addressForm.getNeighbourhood(), AddressField.NEIGHBOURHOOD, MAX_FIELD_LENGTH, errors);
				   default:
					validateStringFieldLength(addressForm.getTitleCode(), AddressField.TITLE, MAX_FIELD_LENGTH, errors);
					break;
			}
		}
	}

	protected static void validateStringFieldPrepaidDistrict(final String addressField, final AddressField fieldType,
											  final String expectedValue, final Errors errors){
		if (addressField != null && !addressField.equalsIgnoreCase(expectedValue))
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
	}

	protected static void validateStringField(final String addressField, final AddressField fieldType,
											  final int maxFieldLength, final Errors errors)
	{
		if (addressField == null || StringUtils.isEmpty(addressField) || (StringUtils.length(addressField) > maxFieldLength))
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected static void validateStringFieldLength(final String field, final AddressField fieldType, final int maxFieldLength,
			final Errors errors)
	{
		if (StringUtils.isNotEmpty(field) && StringUtils.length(field) > maxFieldLength)
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected static void validateFieldNotNull(final String addressField, final AddressField fieldType,
											   final Errors errors)
	{
		if (addressField == null)
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected enum CountryCode
	{
		JAMAICA("JM"), PANAMA("PA"),CABLETICA("CR"),DEFAULT("");

		private final String isoCode;

		private static Map<String, CountryCode> lookupMap = new HashMap<String, CountryCode>();
		static
		{
			for (final CountryCode code : CountryCode.values())
			{
				lookupMap.put(code.getIsoCode(), code);
			}
		}

		private CountryCode(final String isoCodeStr)
		{
			this.isoCode = isoCodeStr;
		}

		public static CountryCode lookup(final String isoCodeStr)
		{
			CountryCode code = lookupMap.get(isoCodeStr);
			if (code == null)
			{
				code = DEFAULT;
			}
			return code;
		}

		public String getIsoCode()
		{
			return isoCode;
		}
	}

	 /**
    * Validate Date Of Birth
    * @param dob
    * @return
    */
   private boolean validateDOBIsAfterTodaysDate(String dob) {
       Date currentDate = new Date();
       try
       {
           SimpleDateFormat sdfrmt = new SimpleDateFormat("dd-MM-yyyy");
           Date dobDate = sdfrmt.parse(dob);
           if(dobDate.after(currentDate)){
               LOG.error("Date of Birth should not be greater then Today's date");
               return false;
           }
           LOG.error(dob+" is valid date format");
           return true;
      }
       /* Date format is invalid */
       catch (ParseException exception)
       {
           LOG.error(dob+" is Invalid Date format");
       }
       return false;
   }

   /**
    * Validate DOB Format
    * @param dob
    * @return
    */
   private boolean validateDOBFormat(String dob) {
       SimpleDateFormat sdfrmt = new SimpleDateFormat("dd-MM-yyyy");
       try
       {
           Date dobDate = sdfrmt.parse(dob);
           Date currentDate =new Date();
           if(dobDate.after(currentDate)){
               LOG.error("Date of Birth should not be greater then Today's date");
               return false;
           }
          LOG.error(dob+" is valid date format");
          return true;
       }
       /* Date format is invalid */
       catch (ParseException exception)
       {
           LOG.error(dob+" is Invalid Date format");
           return false;
       }
   }

   /**
    * Validate TRN Pattern
    * @param patternToTest
    * @return
    */
   private boolean validateTrnPattern(String patternToTest) {
       String regex="[0-9]{3}-[0-9]{3}-[0-9]{3}";
       Pattern r = Pattern.compile(regex);
       Matcher m = r.matcher(patternToTest);
       if (m.find( )) {
           LOG.info("TRN Validated");
          return true;
       }
       LOG.info("TRN Not Validated");
       return false;
   }


	protected enum AddressField
	{
		TITLE("titleCode", "address.title.invalid"), FIRSTNAME("firstName", "address.firstName.invalid"),
		LASTNAME("lastName", "address.lastName.invalid"), LINE1("line1", "address.line1.invalid"),
		LINE2("line2", "address.line2.invalid"), TOWN("townCity", "address.townCity.invalid"),
		POSTCODE("postcode", "address.postcode.invalid"), REGION("regionIso", "address.regionIso.invalid"),
		COUNTRY("countryIso", "address.country.invalid"), BUILDING("building", "address.building.invalid"),
		STREETNAME("streetname", "address.streetname.invalid"),PROVINCE("province","address.province.invalid"),
		DISTRICT("district", "address.district.invalid"), NEIGHBOURHOOD("neighbourhood", "address.neighbourhood.invalid"),
		APPARTMENT("appartment", "address.appartment.invalid"), AREA("area", "address.area.invalid");

		private final String fieldKey;
		private final String errorKey;

		private AddressField(final String fieldKey, final String errorKey)
		{
			this.fieldKey = fieldKey;
			this.errorKey = errorKey;
		}

		public String getFieldKey()
		{
			return fieldKey;
		}

		public String getErrorKey()
		{
			return errorKey;
		}
	}
}
