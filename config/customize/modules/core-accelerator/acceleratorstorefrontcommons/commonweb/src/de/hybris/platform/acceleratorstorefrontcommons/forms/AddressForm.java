/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.acceleratorstorefrontcommons.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 */
public class AddressForm
{
	private String addressId;
	private String titleCode;
	private String firstName;
	private String lastName;
	private String line1;
	private String line2;
	private String townCity;
	private String regionIso;
	private String postcode;
	private String countryIso;
	private Boolean saveInAddressBook;
	private Boolean defaultAddress;
	private Boolean shippingAddress;
	private Boolean billingAddress;
	private Boolean editAddress;
	private String phone;
	private String area;
	private String appartment;
	private String building;
	private String streetname;
	private String province;
	private String district;
	private String neighbourhood;
	private Boolean isServiceable;
	private String dob;
	private String trnNumber;
	private String phone2;
	private String landMark;
	private String streetNumber;
	private String block;
	private String typeOfResidence;
	private String kilometer;
	private String houseNo;
	private String plotNo;



	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}
	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getKilometer() {
		return kilometer;
	}

	public void setKilometer(String kilometer) {
		this.kilometer = kilometer;
	}

	public String getPlotNo() {
		return plotNo;
	}

	public void setPlotNo(String plotNo) {
		this.plotNo = plotNo;
	}

	public String getTypeOfResidence() {
		return typeOfResidence;
	}

	public void setTypeOfResidence(String typeOfResidence) {
		this.typeOfResidence = typeOfResidence;
	}

	public String getHouseNo() {
		return houseNo;
	}

	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}

	public String getAddressId()
	{
		return addressId;
	}

	public void setAddressId(final String addressId)
	{
		this.addressId = addressId;
	}

	@Size(max = 255, message = "{address.title.invalid}")
	public String getTitleCode()
	{
		return titleCode;
	}

	public void setTitleCode(final String titleCode)
	{
		this.titleCode = titleCode;
	}

	@NotNull(message = "{address.firstName.invalid}")
	@Size(min = 1, max = 255, message = "{address.firstName.invalid}")
	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	@NotNull(message = "{address.lastName.invalid}")
	@Size(min = 1, max = 255, message = "{address.lastName.invalid}")
	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	@NotNull(message = "{address.line1.invalid}")
	@Size(min = 1, max = 255, message = "{address.line1.invalid}")
	public String getLine1()
	{
		return line1;
	}

	public void setLine1(final String line1)
	{
		this.line1 = line1;
	}

	public String getLine2()
	{
		return line2;
	}

	public void setLine2(final String line2)
	{
		this.line2 = line2;
	}

	@NotNull(message = "{address.townCity.invalid}")
	@Size(min = 1, max = 255, message = "{address.townCity.invalid}")
	public String getTownCity()
	{
		return townCity;
	}

	public void setTownCity(final String townCity)
	{
		this.townCity = townCity;
	}

	public String getRegionIso()
	{
		return regionIso;
	}

	public void setRegionIso(final String regionIso)
	{
		this.regionIso = regionIso;
	}

	@NotNull(message = "{address.postcode.invalid}")
	@Size(min = 1, max = 10, message = "{address.postcode.invalid}")
	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(final String postcode)
	{
		this.postcode = postcode;
	}

	@NotNull(message = "{address.country.invalid}")
	@Size(min = 1, max = 255, message = "{address.country.invalid}")
	public String getCountryIso()
	{
		return countryIso;
	}

	public void setCountryIso(final String countryIso)
	{
		this.countryIso = countryIso;
	}

	public Boolean getSaveInAddressBook()
	{
		return saveInAddressBook;
	}

	public void setSaveInAddressBook(final Boolean saveInAddressBook)
	{
		this.saveInAddressBook = saveInAddressBook;
	}

	public Boolean getDefaultAddress()
	{
		return defaultAddress;
	}

	public void setDefaultAddress(final Boolean defaultAddress)
	{
		this.defaultAddress = defaultAddress;
	}

	public Boolean getShippingAddress()
	{
		return shippingAddress;
	}

	public void setShippingAddress(final Boolean shippingAddress)
	{
		this.shippingAddress = shippingAddress;
	}

	public Boolean getBillingAddress()
	{
		return billingAddress;
	}

	public void setBillingAddress(final Boolean billingAddress)
	{
		this.billingAddress = billingAddress;
	}

	public Boolean getEditAddress()
	{
		return editAddress;
	}

	public void setEditAddress(final Boolean editAddress)
	{
		this.editAddress = editAddress;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(final String value)
	{
		phone = value;
	}

	@NotNull(message = "{address.area.invalid}")
    @Size(min = 1, max = 100, message = "{address.area.invalid}")
    public String getArea() {
        return area;
    }

    public void setArea(final String area) {
        this.area = area;
    }

	    /**
 	 * @return the appartment
 	 */

 	public String getAppartment()
 	{
 		return appartment;
 	}
 	/**
 	 * @param appartment the appartment to set
 	 */
 	public void setAppartment(final String appartment)
 	{
 		this.appartment = appartment;
 	}
 	/**
 	 * @return the building
 	 */
	@NotNull(message = "{address.building.invalid}")
	@Size(min = 1, max = 255, message = "{address.building.invalid}")
 	public String getBuilding()
 	{
 		return building;
 	}
 	/**
 	 * @param building the building to set
 	 */
 	public void setBuilding(final String building)
 	{
 		this.building = building;
 	}
 	/**
 	 * @return the streetname
 	 */
 	@NotNull(message = "{address.streetname.invalid}")
  	@Size(min = 1, max = 255, message = "{address.streetname.invalid}")
 	public String getStreetname()
 	{
 		return streetname;
 	}
 	/**
 	 * @param streetname the streetname to set
 	 */
 	public void setStreetname(final String streetname)
 	{
 		this.streetname = streetname;
 	}
 	/**
 	 * @return the province
 	 */
 	@NotNull(message = "{address.province.invalid}")
 	@Size(min = 1, max = 255, message = "{address.province.invalid}")
 	public String getProvince()
 	{
 		return province;
 	}
 	/**
 	 * @param province the province to set
 	 */
 	public void setProvince(final String province)
 	{
 		this.province = province;
 	}
 	/**
 	 * @return the district
 	 */

 	public String getDistrict()
 	{
 		return district;
 	}
 	/**
 	 * @param district the district to set
 	 */
 	public void setDistrict(final String district)
 	{
 		this.district = district;
 	}

 	@NotNull(message = "{address.neighbourhood.invalid}")
	@Size(min = 1, max = 255, message = "{address.neighbourhood.invalid}")
 	public String getNeighbourhood()
 	{
 		return neighbourhood;
 	}
 	/**
 	 * @param neighbourhood the neighbourhood to set
 	 */
 	public void setNeighbourhood(final String neighbourhood)
 	{
 		this.neighbourhood = neighbourhood;
 	}

		/**
	 * @return the isServiceable
	 */
	public Boolean getIsServiceable()
	{
		return isServiceable;
	}

	/**
	 * @param isServiceable
	 *           the isServiceable to set
	 */
	public void setIsServiceable(final Boolean isServiceable)
	{
		this.isServiceable = isServiceable;
	}
	public String getDob()
	{
		return dob;
	}

	public void setDob(final String dob)
	{
		this.dob = dob;
	}


	public String getTrnNumber()
	{
		return trnNumber;
	}

	public void setTrnNumber(final String trnNumber)
	{
		this.trnNumber = trnNumber;
	}
    
	public String getPhone2()
	{
		return phone2;
	}

	public void setPhone2(final String phone2)
	{
		this.phone2 = phone2;
	}

	public String getLandMark()
	{
		return landMark;
	}

	public void setLandMark(final String landMark)
	{
		this.landMark = landMark;
	}


}
