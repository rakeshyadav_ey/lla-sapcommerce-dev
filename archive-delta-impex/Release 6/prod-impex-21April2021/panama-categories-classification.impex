# ---------------------------------------------------------------------------
# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# ---------------------------------------------------------------------------

# ImpEx for Importing Category Classifications into B2C Telco Store

# Macros / Replacement Parameter definitions
$productCatalog = panamaProductCatalog
$catalogVersion = catalogversion(catalog(id[default = $productCatalog]), version[default = 'Staged'])[unique = true, default = $productCatalog:Staged]
$classCatalogVersion = catalogversion(catalog(id[default = 'panamaClassification']), version[default = '1.0'])[unique = true, default = 'panamaClassification:1.0']
$classSystemVersion = systemVersion(catalog(id[default = 'panamaClassification']), version[default = '1.0'])[unique = true]
$class = classificationClass(ClassificationClass.code, $classCatalogVersion)[unique = true]
$supercategories = source(code, $classCatalogVersion)[unique = true]
$categories = target(code, $catalogVersion)[unique = true]
$attribute = classificationAttribute(code, $classSystemVersion)[unique = true]
$unit = unit(code, $classSystemVersion)
$lang = es

#% impex.enableCodeExecution(true);
#% if: "true".equals(Config.getParameter("import.sample.data"));
# Insert Classifications
INSERT_UPDATE ClassificationClass; $classCatalogVersion; code[unique = true]; allowedPrincipals(uid)[default = 'customergroup']
; ; compatibilityclassification
; ; mobilephoneclassification
; ; accessoryclassification
; ; serviceplanclassification



#categories-classification impex

INSERT_UPDATE ClassificationAttribute                                                                  ; $classSystemVersion ; code[unique = true]
                                                                                                       ;                     ; storage
                                                                                                       ;                     ; camera
                                                                                                       ;                     ; front_camera
                                                                                                       ;                     ; displayresolution
                                                                                                       ;                     ; displaysize
                                                                                                       ;                     ; color
                                                                                                       ;                     ; ram
                                                                                                       ;                     ; microsd
                                                                                                       ;                     ; processor
                                                                                                       ;                     ; battery_power
                                                                                                       ;                     ; non_removable_battery
                                                                                                       ;                     ; connectivity
                                                                                                       ;                     ; mobileplatform


INSERT_UPDATE ClassAttributeAssignment                                                                 ; $class                    ; $attribute            ; position ; $unit      ; attributeType(code[default = string]) ; multiValued[default = false] ; range[default = false] ; localized[default = false] ; attributeValues(code, $classSystemVersion)
                                                                                                       ; mobilephoneclassification ; storage               ; 0        ; gb         ; string
                                                                                                       ; mobilephoneclassification ; ram                   ; 1        ; gb         ; number
                                                                                                       ; mobilephoneclassification ; microsd               ; 2        ;            ; string                                ;                              ;                        ;                            ;
                                                                                                       ; mobilephoneclassification ; processor             ; 3        ;            ; string
                                                                                                       ; mobilephoneclassification ; displayresolution     ; 4        ;            ; string
                                                                                                       ; mobilephoneclassification ; displaysize           ; 5        ;            ; string
                                                                                                       ; mobilephoneclassification ; color                 ; 6        ;            ;
                                                                                                       ; mobilephoneclassification ; camera                ; 7        ;            ; string
                                                                                                       ; mobilephoneclassification ; front_camera          ; 8        ;            ; string
                                                                                                       ; mobilephoneclassification ; battery_power         ; 9        ;            ; string
                                                                                                       ; mobilephoneclassification ; non_removable_battery ; 10       ;            ; string                                ;                              ;                        ;                            ; feature_non_removable_battery, feature_removable_battery
                                                                                                       ; mobilephoneclassification ; connectivity          ; 11       ;            ; string                                ;                              ;                        ;                            ; band_wifi, band_lte,band_bluetooth, band_gps, band_radio, band_hotspot
                                                                                                       ; mobilephoneclassification ; mobileplatform        ; 12       ;            ; enum                                  ;                              ;                        ;                            ; platform_ios,platform_android,platform_rim,platform_symbian,platform_ms


INSERT_UPDATE ClassificationAttributeValue                                                             ; code[unique = true] ; $classSystemVersion
                                                                                                       ; feature_removable_battery
                                                                                                       ; feature_non_removable_battery
                                                                                                       ; band_wifi
                                                                                                       ; band_lte
                                                                                                       ; band_bluetooth
                                                                                                       ; band_gps
                                                                                                       ; band_radio
                                                                                                       ; band_hotspot
                                                                                                       ; platform_ios
                                                                                                       ; platform_android
                                                                                                       ; platform_rim
                                                                                                       ; platform_symbian
                                                                                                       ; platform_ms
#% endif:
