# ---------------------------------------------------------------------------
# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# ---------------------------------------------------------------------------

# Impex for Importing Product Media into B2C Telco Store

# Macros / Replacement Parameter definitions
$productCatalog = cableticaProductCatalog
$catalogVersion = catalogversion(catalog(id[default = $productCatalog]), version[default = 'Staged'])[unique = true, default = $productCatalog:Staged]
$thumbnail = thumbnail(code, $catalogVersion)
$picture = picture(code, $catalogVersion)
$thumbnails = thumbnails(code, $catalogVersion)
$detail = detail(code, $catalogVersion)
$normal = normal(code, $catalogVersion)
$others = others(code, $catalogVersion)
$data_sheet = data_sheet(code, $catalogVersion)
$medias = medias(code, $catalogVersion)
$galleryImages = galleryImages(qualifier, $catalogVersion)
$siteResource = jar:com.lla.telcostore.constants.LlatelcostoreConstants&/llatelcostore/import/sampledata/productCatalogs/$productCatalog

# ImPex for Importing TV-Channel Product Media

# Macros / Replacement Parameter definitions

################Double play and triple play###########################

INSERT_UPDATE Media             ; mediaFormat(qualifier)                ; code[unique = true]                               ; @media[translator = de.hybris.platform.impex.jalo.media.MediaDataTranslator][forceWrite = true]             ; realfilename                     ; mime[default = 'image/jpeg'] ; $catalogVersion ; folder(qualifier)[default = images]

                                ; 30Wx30H                               ; 30Wx30H/dobleplay_DIAMANTE_30.jpg                      ; $siteResource/images/30Wx30H/dobleplay_MEGA_30.jpg                                                           ; dobleplay_MEGA_30.jpg             ;
                                ; 65Wx65H                               ; 65Wx65H/dobleplay_DIAMANTE_30.jpg                      ; $siteResource/images/65Wx65H/dobleplay_MEGA_30.jpg                                                           ; dobleplay_MEGA_30.jpg             ;
                                ; 96Wx96H                               ; 96Wx96H/dobleplay_DIAMANTE_30.jpg                      ; $siteResource/images/96Wx96H/dobleplay_MEGA_30.jpg                                                           ; dobleplay_MEGA_30.jpg             ;
                                ; 300Wx300H                             ; 300Wx300H/dobleplay_DIAMANTE_30.jpg                    ; $siteResource/images/300Wx300H/dobleplay_MEGA_30.jpg                                                         ; dobleplay_MEGA_30.jpg             ;
                                ; 515Wx515H                             ; 515Wx515H/dobleplay_DIAMANTE_30.jpg                    ; $siteResource/images/515Wx515H/dobleplay_MEGA_30.jpg                                                         ; dobleplay_MEGA_30.jpg             ;
                                ; 1200Wx1200H                           ; 1200Wx1200H/dobleplay_DIAMANTE_30.jpg                  ; $siteResource/images/1200Wx1200H/dobleplay_MEGA_30.jpg                                                       ; dobleplay_MEGA_30.jpg             ;

                                ; 30Wx30H                               ; 30Wx30H/dobleplay_DIAMANTE_100.jpg                      ; $siteResource/images/30Wx30H/dobleplay_MEGA_100.jpg                                                           ; dobleplay_MEGA_30.jpg             ;
                                ; 65Wx65H                               ; 65Wx65H/dobleplay_DIAMANTE_100.jpg                      ; $siteResource/images/65Wx65H/dobleplay_MEGA_100.jpg                                                           ; dobleplay_MEGA_30.jpg             ;
                                ; 96Wx96H                               ; 96Wx96H/dobleplay_DIAMANTE_100.jpg                      ; $siteResource/images/96Wx96H/dobleplay_MEGA_100.jpg                                                           ; dobleplay_MEGA_30.jpg             ;
                                ; 300Wx300H                             ; 300Wx300H/dobleplay_DIAMANTE_100.jpg                    ; $siteResource/images/300Wx300H/dobleplay_MEGA_100.jpg                                                         ; dobleplay_MEGA_30.jpg             ;
                                ; 515Wx515H                             ; 515Wx515H/dobleplay_DIAMANTE_100.jpg                    ; $siteResource/images/515Wx515H/dobleplay_MEGA_100.jpg                                                         ; dobleplay_MEGA_30.jpg             ;
                                ; 1200Wx1200H                           ; 1200Wx1200H/dobleplay_DIAMANTE_100.jpg                  ; $siteResource/images/1200Wx1200H/dobleplay_MEGA_100.jpg                                                       ; dobleplay_MEGA_30.jpg             ;

                                ; 30Wx30H                               ; 30Wx30H/dobleplay_DIAMANTE_200.jpg                      ; $siteResource/images/30Wx30H/dobleplay_MEGA_200.jpg                                                           ; dobleplay_MEGA_30.jpg             ;
                                ; 65Wx65H                               ; 65Wx65H/dobleplay_DIAMANTE_200.jpg                      ; $siteResource/images/65Wx65H/dobleplay_MEGA_200.jpg                                                           ; dobleplay_MEGA_30.jpg             ;
                                ; 96Wx96H                               ; 96Wx96H/dobleplay_DIAMANTE_200.jpg                      ; $siteResource/images/96Wx96H/dobleplay_MEGA_200.jpg                                                           ; dobleplay_MEGA_30.jpg             ;
                                ; 300Wx300H                             ; 300Wx300H/dobleplay_DIAMANTE_200.jpg                    ; $siteResource/images/300Wx300H/dobleplay_MEGA_200.jpg                                                         ; dobleplay_MEGA_30.jpg             ;
                                ; 515Wx515H                             ; 515Wx515H/dobleplay_DIAMANTE_200.jpg                    ; $siteResource/images/515Wx515H/dobleplay_MEGA_200.jpg                                                         ; dobleplay_MEGA_30.jpg             ;
                                ; 1200Wx1200H                           ; 1200Wx1200H/dobleplay_DIAMANTE_200.jpg                  ; $siteResource/images/1200Wx1200H/dobleplay_MEGA_200.jpg                                                       ; dobleplay_MEGA_30.jpg             ;

                                ; 30Wx30H                               ; 30Wx30H/tripleplay_DIAMANTE_30.jpg                      ; $siteResource/images/30Wx30H/tripleplay_MEGA_30.jpg                                                           ; tripleplay_MEGA_30.jpg             ;
                                ; 65Wx65H                               ; 65Wx65H/tripleplay_DIAMANTE_30.jpg                      ; $siteResource/images/65Wx65H/tripleplay_MEGA_30.jpg                                                           ; tripleplay_MEGA_30.jpg             ;
                                ; 96Wx96H                               ; 96Wx96H/tripleplay_DIAMANTE_30.jpg                      ; $siteResource/images/96Wx96H/tripleplay_MEGA_30.jpg                                                           ; tripleplay_MEGA_30.jpg             ;
                                ; 300Wx300H                             ; 300Wx300H/tripleplay_DIAMANTE_30.jpg                    ; $siteResource/images/300Wx300H/tripleplay_MEGA_30.jpg                                                         ; tripleplay_MEGA_30.jpg             ;
                                ; 515Wx515H                             ; 515Wx515H/tripleplay_DIAMANTE_30.jpg                    ; $siteResource/images/515Wx515H/tripleplay_MEGA_30.jpg                                                         ; tripleplay_MEGA_30.jpg             ;
                                ; 1200Wx1200H                           ; 1200Wx1200H/tripleplay_DIAMANTE_30.jpg                  ; $siteResource/images/1200Wx1200H/tripleplay_MEGA_30.jpg                                                       ; tripleplay_MEGA_30.jpg             ;

                                ; 30Wx30H                               ; 30Wx30H/tripleplay_DIAMANTE_100.jpg                      ; $siteResource/images/30Wx30H/tripleplay_MEGA_100.jpg                                                           ; tripleplay_MEGA_100.jpg             ;
                                ; 65Wx65H                               ; 65Wx65H/tripleplay_DIAMANTE_100.jpg                      ; $siteResource/images/65Wx65H/tripleplay_MEGA_100.jpg                                                           ; tripleplay_MEGA_100.jpg             ;
                                ; 96Wx96H                               ; 96Wx96H/tripleplay_DIAMANTE_100.jpg                      ; $siteResource/images/96Wx96H/tripleplay_MEGA_100.jpg                                                           ; tripleplay_MEGA_100.jpg             ;
                                ; 300Wx300H                             ; 300Wx300H/tripleplay_DIAMANTE_100.jpg                    ; $siteResource/images/300Wx300H/tripleplay_MEGA_100.jpg                                                         ; tripleplay_MEGA_100.jpg             ;
                                ; 515Wx515H                             ; 515Wx515H/tripleplay_DIAMANTE_100.jpg                    ; $siteResource/images/515Wx515H/tripleplay_MEGA_100.jpg                                                         ; tripleplay_MEGA_100.jpg             ;
                                ; 1200Wx1200H                           ; 1200Wx1200H/tripleplay_DIAMANTE_100.jpg                  ; $siteResource/images/1200Wx1200H/tripleplay_MEGA_100.jpg                                                       ; tripleplay_MEGA_100.jpg             ;    

                                ; 30Wx30H                               ; 30Wx30H/tripleplay_DIAMANTE_200.jpg                      ; $siteResource/images/30Wx30H/tripleplay_MEGA_200.jpg                                                           ; tripleplay_MEGA_200.jpg             ;
                                ; 65Wx65H                               ; 65Wx65H/tripleplay_DIAMANTE_200.jpg                      ; $siteResource/images/65Wx65H/tripleplay_MEGA_200.jpg                                                           ; tripleplay_MEGA_200.jpg             ;
                                ; 96Wx96H                               ; 96Wx96H/tripleplay_DIAMANTE_200.jpg                      ; $siteResource/images/96Wx96H/tripleplay_MEGA_200.jpg                                                           ; tripleplay_MEGA_200.jpg             ;
                                ; 300Wx300H                             ; 300Wx300H/tripleplay_DIAMANTE_200.jpg                    ; $siteResource/images/300Wx300H/tripleplay_MEGA_200.jpg                                                         ; tripleplay_MEGA_200.jpg             ;
                                ; 515Wx515H                             ; 515Wx515H/tripleplay_DIAMANTE_200.jpg                    ; $siteResource/images/515Wx515H/tripleplay_MEGA_200.jpg                                                         ; tripleplay_MEGA_200.jpg             ;
                                ; 1200Wx1200H                           ; 1200Wx1200H/tripleplay_DIAMANTE_200.jpg                  ; $siteResource/images/1200Wx1200H/tripleplay_MEGA_200.jpg                                                       ; tripleplay_MEGA_200.jpg             ;    


INSERT_UPDATE MediaContainer    ; qualifier[unique = true]                         ; $medias                                                                                                                                                                                                                      ; $catalogVersion

                                ; dobleplay_DIAMANTE_30-Container                      ; 30Wx30H/dobleplay_MEGA_30.jpg,65Wx65H/dobleplay_MEGA_30.jpg,96Wx96H/dobleplay_MEGA_30.jpg,300Wx300H/dobleplay_MEGA_30.jpg,515Wx515H/dobleplay_MEGA_30.jpg,1200Wx1200H/dobleplay_MEGA_30.jpg
                                ; dobleplay_DIAMANTE_100-Container                     ; 30Wx30H/dobleplay_MEGA_100.jpg,65Wx65H/dobleplay_MEGA_100.jpg,96Wx96H/dobleplay_MEGA_100.jpg,300Wx300H/dobleplay_MEGA_100.jpg,515Wx515H/dobleplay_MEGA_100.jpg,1200Wx1200H/dobleplay_MEGA_100.jpg
                                ; dobleplay_DIAMANTE_200-Container                     ; 30Wx30H/dobleplay_MEGA_200.jpg,65Wx65H/dobleplay_MEGA_200.jpg,96Wx96H/dobleplay_MEGA_200.jpg,300Wx300H/dobleplay_MEGA_200.jpg,515Wx515H/dobleplay_MEGA_200.jpg,1200Wx1200H/dobleplay_MEGA_200.jpg
                                ; tripleplay_DIAMANTE_30-Container                     ; 30Wx30H/tripleplay_MEGA_30.jpg,65Wx65H/tripleplay_MEGA_30.jpg,96Wx96H/tripleplay_MEGA_30.jpg,300Wx300H/tripleplay_MEGA_30.jpg,515Wx515H/tripleplay_MEGA_30.jpg,1200Wx1200H/tripleplay_MEGA_30.jpg
                                ; tripleplay_DIAMANTE_100-Container                    ; 30Wx30H/tripleplay_MEGA_100.jpg,65Wx65H/tripleplay_MEGA_100.jpg,96Wx96H/tripleplay_MEGA_100.jpg,300Wx300H/tripleplay_MEGA_100.jpg,515Wx515H/tripleplay_MEGA_100.jpg,1200Wx1200H/tripleplay_MEGA_100.jpg
                                ; tripleplay_DIAMANTE_200-Container                     ; 30Wx30H/tripleplay_MEGA_200.jpg,65Wx65H/tripleplay_MEGA_200.jpg,96Wx96H/tripleplay_MEGA_200.jpg,300Wx300H/tripleplay_MEGA_200.jpg,515Wx515H/tripleplay_MEGA_200.jpg,1200Wx1200H/tripleplay_MEGA_200.jpg


UPDATE Media                    ; code[unique = true]                   ; mediaContainer(qualifier, $catalogVersion) ; $catalogVersion

								; 30Wx30H/dobleplay_DIAMANTE_30.jpg       ; dobleplay_DIAMANTE_30-Container
                                ; 65Wx65H/dobleplay_DIAMANTE_30.jpg       ; dobleplay_DIAMANTE_30-Container
                                ; 96Wx96H/dobleplay_DIAMANTE_30.jpg       ; dobleplay_DIAMANTE_30-Container
                                ; 300Wx300H/dobleplay_DIAMANTE_30.jpg     ; dobleplay_DIAMANTE_30-Container
                                ; 515Wx515H/dobleplay_DIAMANTE_30.jpg     ; dobleplay_DIAMANTE_30-Container
                                ; 1200Wx1200H/dobleplay_DIAMANTE_30.jpg   ; dobleplay_DIAMANTE_30-Container

								; 30Wx30H/dobleplay_DIAMANTE_100.jpg       ; dobleplay_DIAMANTE_100-Container
                                ; 65Wx65H/dobleplay_DIAMANTE_100.jpg       ; dobleplay_DIAMANTE_100-Container
                                ; 96Wx96H/dobleplay_DIAMANTE_100.jpg       ; dobleplay_DIAMANTE_100-Container
                                ; 300Wx300H/dobleplay_DIAMANTE_100.jpg     ; dobleplay_DIAMANTE_100-Container
                                ; 515Wx515H/dobleplay_DIAMANTE_100.jpg     ; dobleplay_DIAMANTE_100-Container
                                ; 1200Wx1200H/dobleplay_DIAMANTE_100.jpg   ; dobleplay_DIAMANTE_100-Container

								; 30Wx30H/dobleplay_DIAMANTE_200.jpg       ; dobleplay_DIAMANTE_200-Container
                                ; 65Wx65H/dobleplay_DIAMANTE_200.jpg       ; dobleplay_DIAMANTE_200-Container
                                ; 96Wx96H/dobleplay_DIAMANTE_200.jpg       ; dobleplay_DIAMANTE_200-Container
                                ; 300Wx300H/dobleplay_DIAMANTE_200.jpg     ; dobleplay_DIAMANTE_200-Container
                                ; 515Wx515H/dobleplay_DIAMANTE_200.jpg     ; dobleplay_DIAMANTE_200-Container
                                ; 1200Wx1200H/dobleplay_DIAMANTE_200.jpg   ; dobleplay_DIAMANTE_200-Container

                                ; 30Wx30H/tripleplay_DIAMANTE_30.jpg          ; tripleplay_DIAMANTE_30-Container
                                ; 65Wx65H/tripleplay_DIAMANTE_30.jpg          ; tripleplay_DIAMANTE_30-Container
                                ; 96Wx96H/tripleplay_DIAMANTE_30.jpg          ; tripleplay_DIAMANTE_30-Container
                                ; 300Wx300H/tripleplay_DIAMANTE_30.jpg        ; tripleplay_DIAMANTE_30-Container
                                ; 515Wx515H/tripleplay_DIAMANTE_30.jpg        ; tripleplay_DIAMANTE_30-Container
                                ; 1200Wx1200H/tripleplay_DIAMANTE_30.jpg      ; tripleplay_DIAMANTE_30-Container
								
                                ; 30Wx30H/tripleplay_DIAMANTE_100.jpg          ; tripleplay_DIAMANTE_100-Container
                                ; 65Wx65H/tripleplay_DIAMANTE_100.jpg          ; tripleplay_DIAMANTE_100-Container
                                ; 96Wx96H/tripleplay_DIAMANTE_100.jpg          ; tripleplay_DIAMANTE_100-Container
                                ; 300Wx300H/tripleplay_DIAMANTE_100.jpg        ; tripleplay_DIAMANTE_100-Container
                                ; 515Wx515H/tripleplay_DIAMANTE_100.jpg        ; tripleplay_DIAMANTE_100-Container
                                ; 1200Wx1200H/tripleplay_DIAMANTE_100.jpg      ; tripleplay_DIAMANTE_100-Container 
								
                                ; 30Wx30H/tripleplay_DIAMANTE_200.jpg          ; tripleplay_DIAMANTE_200-Container
                                ; 65Wx65H/tripleplay_DIAMANTE_200.jpg          ; tripleplay_DIAMANTE_200-Container
                                ; 96Wx96H/tripleplay_DIAMANTE_200.jpg          ; tripleplay_DIAMANTE_200-Container
                                ; 300Wx300H/tripleplay_DIAMANTE_200.jpg        ; tripleplay_DIAMANTE_200-Container
                                ; 515Wx515H/tripleplay_DIAMANTE_200.jpg        ; tripleplay_DIAMANTE_200-Container
                                ; 1200Wx1200H/tripleplay_DIAMANTE_200.jpg      ; tripleplay_DIAMANTE_200-Container


UPDATE TmaSimpleProductOffering ; code[unique = true]                    ; $picture                                   ; $thumbnail                           ; $galleryImages                  ; $catalogVersion

                                ; dobleplay_DIAMANTE_30                      ; 300Wx300H/dobleplay_MEGA_30.jpg            ; 96Wx96H/dobleplay_MEGA_30.jpg        ; dobleplay_DIAMANTE_30-Container
                                ; dobleplay_DIAMANTE_100                     ; 300Wx300H/dobleplay_MEGA_100.jpg           ; 96Wx96H/dobleplay_MEGA_100.jpg       ; dobleplay_DIAMANTE_100-Container
                                ; dobleplay_DIAMANTE_200                     ; 300Wx300H/dobleplay_MEGA_200.jpg           ; 96Wx96H/dobleplay_MEGA_200.jpg       ; dobleplay_DIAMANTE_200-Container
                                ; tripleplay_DIAMANTE_30                     ; 300Wx300H/tripleplay_MEGA_30.jpg           ; 96Wx96H/tripleplay_MEGA_30.jpg       ; tripleplay_DIAMANTE_30-Container
                                ; tripleplay_DIAMANTE_100                    ; 300Wx300H/tripleplay_MEGA_100.jpg          ; 96Wx96H/tripleplay_MEGA_100.jpg      ; tripleplay_DIAMANTE_100-Container
                                ; tripleplay_DIAMANTE_200                    ; 300Wx300H/tripleplay_MEGA_200.jpg          ; 96Wx96H/tripleplay_MEGA_200.jpg      ; tripleplay_DIAMANTE_200-Container
