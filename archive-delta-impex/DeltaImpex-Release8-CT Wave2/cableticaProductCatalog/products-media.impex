# ---------------------------------------------------------------------------
# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# ---------------------------------------------------------------------------

# Impex for Importing Product Media into B2C Telco Store

# Macros / Replacement Parameter definitions
$productCatalog = cableticaProductCatalog
$catalogVersion = catalogversion(catalog(id[default = $productCatalog]), version[default = 'Staged'])[unique = true, default = $productCatalog:Staged]
$thumbnail = thumbnail(code, $catalogVersion)
$picture = picture(code, $catalogVersion)
$thumbnails = thumbnails(code, $catalogVersion)
$detail = detail(code, $catalogVersion)
$normal = normal(code, $catalogVersion)
$others = others(code, $catalogVersion)
$data_sheet = data_sheet(code, $catalogVersion)
$medias = medias(code, $catalogVersion)
$galleryImages = galleryImages(qualifier, $catalogVersion)
$siteResource = jar:com.lla.telcostore.constants.LlatelcostoreConstants&/llatelcostore/import/sampledata/productCatalogs/$productCatalog

# ImPex for Importing TV-Channel Product Media

# Macros / Replacement Parameter definitions


############icon image############

INSERT_UPDATE Media             ; mediaFormat(qualifier)                ; code[unique = true]                               ; @media[translator = de.hybris.platform.impex.jalo.media.MediaDataTranslator][forceWrite = true]             ; realfilename                     ; mime[default = 'image/jpeg'] ; $catalogVersion ; folder(qualifier)[default = images]

                                ; 30Wx30H                               ; 30Wx30H/tv_PACK_BASICO_DIGITAL.jpg                 ; $siteResource/images/30Wx30H/tv_PACK_BASICO_DIGITAL.jpg                                                      ; tv_PACK_BASICO_DIGITAL.jpg             ;
                                ; 65Wx65H                               ; 65Wx65H/tv_PACK_BASICO_DIGITAL.jpg                 ; $siteResource/images/65Wx65H/tv_PACK_BASICO_DIGITAL.jpg                                                      ; tv_PACK_BASICO_DIGITAL.jpg             ;
                                ; 96Wx96H                               ; 96Wx96H/tv_PACK_BASICO_DIGITAL.jpg                 ; $siteResource/images/96Wx96H/tv_PACK_BASICO_DIGITAL.jpg                                                      ; tv_PACK_BASICO_DIGITAL.jpg             ;
                                ; 300Wx300H                             ; 300Wx300H/tv_PACK_BASICO_DIGITAL.jpg               ; $siteResource/images/300Wx300H/tv_PACK_BASICO_DIGITAL.jpg                                                    ; tv_PACK_BASICO_DIGITAL.jpg             ;
                                ; 515Wx515H                             ; 515Wx515H/tv_PACK_BASICO_DIGITAL.jpg               ; $siteResource/images/515Wx515H/tv_PACK_BASICO_DIGITAL.jpg                                                    ; tv_PACK_BASICO_DIGITAL.jpg             ;
                                ; 1200Wx1200H                           ; 1200Wx1200H/tv_PACK_BASICO_DIGITAL.jpg             ; $siteResource/images/1200Wx1200H/tv_PACK_BASICO_DIGITAL.jpg                                                  ; tv_PACK_BASICO_DIGITAL.jpg             ;								

                                ; 30Wx30H                               ; 30Wx30H/tv_PACK_DIAMANTE.jpg                 ; $siteResource/images/30Wx30H/tv_PACK_DIAMANTE.jpg                                                      ; tv_PACK_DIAMANTE.jpg             ;
                                ; 65Wx65H                               ; 65Wx65H/tv_PACK_DIAMANTE.jpg                 ; $siteResource/images/65Wx65H/tv_PACK_DIAMANTE.jpg                                                      ; tv_PACK_DIAMANTE.jpg             ;
                                ; 96Wx96H                               ; 96Wx96H/tv_PACK_DIAMANTE.jpg                 ; $siteResource/images/96Wx96H/tv_PACK_DIAMANTE.jpg                                                      ; tv_PACK_DIAMANTE.jpg             ;
                                ; 300Wx300H                             ; 300Wx300H/tv_PACK_DIAMANTE.jpg               ; $siteResource/images/300Wx300H/tv_PACK_DIAMANTE.jpg                                                    ; tv_PACK_DIAMANTE.jpg             ;
                                ; 515Wx515H                             ; 515Wx515H/tv_PACK_DIAMANTE.jpg               ; $siteResource/images/515Wx515H/tv_PACK_DIAMANTE.jpg                                                    ; tv_PACK_DIAMANTE.jpg             ;
                                ; 1200Wx1200H                           ; 1200Wx1200H/tv_PACK_DIAMANTE.jpg             ; $siteResource/images/1200Wx1200H/tv_PACK_DIAMANTE.jpg                                                  ; tv_PACK_DIAMANTE.jpg             ;



INSERT_UPDATE MediaContainer    ; qualifier[unique = true]                         ; $medias                                                                                                                                                                                                                      ; $catalogVersion

                                ; tv_PACK_BASICO_DIGITAL-Container                 ; 30Wx30H/tv_PACK_BASICO_DIGITAL.jpg,65Wx65H/tv_PACK_BASICO_DIGITAL.jpg,96Wx96H/tv_PACK_BASICO_DIGITAL.jpg,300Wx300H/tv_PACK_BASICO_DIGITAL.jpg,515Wx515H/tv_PACK_BASICO_DIGITAL.jpg,1200Wx1200H/tv_PACK_BASICO_DIGITAL.jpg
                                ; tv_PACK_DIAMANTE-Container                       ; 30Wx30H/tv_PACK_DIAMANTE.jpg,65Wx65H/tv_PACK_DIAMANTE.jpg,96Wx96H/tv_PACK_DIAMANTE.jpg,300Wx300H/tv_PACK_DIAMANTE.jpg,515Wx515H/tv_PACK_DIAMANTE.jpg,1200Wx1200H/tv_PACK_DIAMANTE.jpg

								
								
								
								
UPDATE Media                    ; code[unique = true]                   ; mediaContainer(qualifier, $catalogVersion) ; $catalogVersion						
								
                                ; 30Wx30H/tv_PACK_BASICO_DIGITAL.jpg          ; tv_PACK_BASICO_DIGITAL-Container
                                ; 65Wx65H/tv_PACK_BASICO_DIGITAL.jpg          ; tv_PACK_BASICO_DIGITAL-Container
                                ; 96Wx96H/tv_PACK_BASICO_DIGITAL.jpg          ; tv_PACK_BASICO_DIGITAL-Container
                                ; 300Wx300H/tv_PACK_BASICO_DIGITAL.jpg        ; tv_PACK_BASICO_DIGITAL-Container
                                ; 515Wx515H/tv_PACK_BASICO_DIGITAL.jpg        ; tv_PACK_BASICO_DIGITAL-Container
                                ; 1200Wx1200H/tv_PACK_BASICO_DIGITAL.jpg      ; tv_PACK_BASICO_DIGITAL-Container
								
                                ; 30Wx30H/tv_PACK_DIAMANTE.jpg          ; tv_PACK_DIAMANTE-Container
                                ; 65Wx65H/tv_PACK_DIAMANTE.jpg          ; tv_PACK_DIAMANTE-Container
                                ; 96Wx96H/tv_PACK_DIAMANTE.jpg          ; tv_PACK_DIAMANTE-Container
                                ; 300Wx300H/tv_PACK_DIAMANTE.jpg        ; tv_PACK_DIAMANTE-Container
                                ; 515Wx515H/tv_PACK_DIAMANTE.jpg        ; tv_PACK_DIAMANTE-Container
                                ; 1200Wx1200H/tv_PACK_DIAMANTE.jpg      ; tv_PACK_DIAMANTE-Container
								
						
								
UPDATE TmaSimpleProductOffering ; code[unique = true]                    ; $picture                                   ; $thumbnail                           ; $galleryImages                  ; $catalogVersion

                                ; tv_PACK_BASICO_DIGITAL                 ; 300Wx300H/tv_PACK_BASICO_DIGITAL.jpg       ; 96Wx96H/tv_PACK_BASICO_DIGITAL.jpg   ; tv_PACK_BASICO_DIGITAL-Container
                                ; tv_PACK_DIAMANTE                       ; 300Wx300H/tv_PACK_DIAMANTE.jpg             ; 96Wx96H/tv_PACK_DIAMANTE.jpg         ; tv_PACK_DIAMANTE-Container
                                
								