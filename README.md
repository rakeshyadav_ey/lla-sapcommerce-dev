Install Commerce Cloud and Telco Accelerator

Install JDK 11 or SAP Machine 11 (Link here)

Set JDK11 as Java_Home

Download the following files a. CXCOMM190500P_13-70004140.ZIP b. CXTELPACK2003_0-70005063.ZIP

Extract CXCOMM190500P_13-70004140.ZIP file using 7-zip (windows) or keka (mac OS) from root folder. Change the extracted directory name to ‘hybris’.

Extract CXTELPACK2003_0-70005063.zip file and copy the bin directory to Hybris bin directory.

Open Git Bash
If you have not setup GIT-SSH on your system, please follow these steps, a. Run command ssh-keygen -t rsa -b 4096 -C "your_email@example.com" b. The key generation program will ask you where you want to save the key. Type in LLAgit_rsa. c. You can leave the passcode as blank. d. To view to SSH key, use the command i. cat LLAgit_rsa.pub e. Now you need to add the key to GitHub. f. Go to Git Hub (https://github.com/LLA-DIgital/lla-sapcommerce-dev.git) g. Click Settings h. Click SSH and GPG keys i. Create new SSH key j. Give any name and copy the SSH key generated in the above step.
Follow the following steps to do a git pull, a. cd $HYBRIS_HOME/hybris b. git init c. git remote add origin https://github.com/LLA-DIgital/lla-sapcommerce-dev.git d. git pull origin master
If you get overwrite error, follow these steps, a. git reset --hard origin/ci b. git pull origin ci
Now, you should have the latest code on your system.

Run ant clean all

Copy the local.properties and localextensions.xml file to config folder

Create Symbolic link for starting Commerce cloud with custom extensions

	For Mac

		ln -s /Users/to/original /path/to/link
		Replace original to /Users/ashwin.sarathi@ey.com/documents/hybris/hybris/core-customize/lla
		Replace link to /Users/ashwin.sarathi@ey.com/documents/hybris/hybris/bin/custom

	For Windows
		mklink /J <Hybris Bin Directory> <Git Checkout custom folder>
		example : mklink /J C:\LLA\hybris\bin\custom C:\LLA\Clone\llaclone1\lla-sapcommerce-dev\core-customize\lla

run below command for addon install :

ant addoninstall -Daddonnames="assistedservicestorefront,llacommonaddon,llatelcoaddon" -DaddonStorefront.yacceleratorstorefront="llacostaricastorefront,llajamaicastorefront,llapuertoricostorefront,llapanamastorefront" 

Run ant initialize
Start the Server > hybrisserver.bat. (./hybrisserver.sh for mac)

Please create a feature branch for your changes and merge to ci

Open Git Bash
git checkout -b ‘’
Make your git changes from your IDE
git status
Add your changed files a. git add ‘’
Commit the changes a. git commit -m ‘’
If you need to push to your feature branch for the time being, a. git push -u origin
Now to merge this commit with ci a. git checkout ci b. git merge –no-ff --no-commit c. Resolve any conflicts d. Perform git add for the files modified e. Perform commit again
Now to push the changes a. git push

Reference Links:

Installing Commerce Cloud on local machine https://help.sap.com/viewer/a74589c3a81a4a95bf51d87258c0ab15/1905/en-US/4e00fd71f0dd4dcab248f022a8c98d3d.html

Installing Commerce cloud with spartacus https://sap.github.io/spartacus-docs/installing-sap-commerce-cloud/
Note: Add below host entry 127.0.0.1 jamaica.local

